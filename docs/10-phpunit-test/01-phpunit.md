- установка phpunit
- знакомство с тем какие бывают тесты и как они пишутся
- напишем две заготовки тестов для нашего проекта

## Зачем нужны тесты

- phplint - проверка корретности php-кода
- PHP_CodeSniff - проверка code-style
- psalm - статический анализатор кода - для более детальной и глубокой проверки.

Статический анализатор кода позволяет проверять
- везде ли используется строгая типизация
- везде ли проставлены используемые типы (и классы) для переменных
- выявление недачётов и ошибок из-за невнимательности.
  например если передаём в функцию или метод переменную не корретного типа
  то есть типа с которой данная функция работать не сможет и упадёт с ошибкой.

Всё это позволяет выявлять ошибки до того как приложение будет использоваться.
То есть проверять то, что код с технической точки зрения написан корректно.
И что даже из-за частых слияний и ребейзов в единую кодовую базу не попадёт
ошибка или опечатка ломающая код.

Следующий тип возможных ошибок в приложнии - на уровне логики.
Если в приложении нарушена логика его работы то тут уже не имеет значения
язык программирования с какой типизацией использутеся. В таких случаях
ни динамическая ни статическая типизация не спасут от логических ошибок в коде.

Другими словами даже строгая типизаци не спасёт от логических ошибок допущеных
программистом.

Единственный способ проверки логической правильности кода - написать рядом
другой код, который будет проверять первый.
Т.е. для имеющихся классов отдельные пишутся классы, проверяющие правильность
работы первых. Это позволит быть уверенным что код работает так как надо и так
как от него ожидается.
Такой подход позволяет развеить иллюзи того что приложение написано правильно
и без ошибок. Выявить ошибки и исправить их до того как их обнаружат в процессе
эксплуатации.

## В каком виде стиле писать тесты?
Можно взять готовый инструмен и установить в свой проект.


[PHPUnit](https://phpunit.de/getting-started/phpunit-8.html) - самый популярный
фреймворк для тестирования в экосистеме php.

Фреймворком его можно назват по аналогии с фреймворками slim symfony/console
- slim - http-фреймворк позволяющий запускать web controller-ы(action-ы)
- symfony/console - фреймворк для запуска консольных команд

аналогично и phpunit - фреймворк который будет запускать и управлять нашими
тестами. Тестами разных компонентов нашей системы.
Он ставится так же как и все остальные пакеты

`--dev` Установка в dev-окружении (В проде тесты не нужны)
```sh
docker compose run --rm api-php-cli composer require --dev phpunit/phpunit
```
[output](./docs/10-phpunit-test/logs/phpunit-install.log)

```sh
du -hcd 1 api/vendor
580K  api/vendor/slim
40K   api/vendor/fig
372K  api/vendor/phar-io
528K  api/vendor/php-di
852K  api/vendor/overtrue
48K   api/vendor/bin
508K  api/vendor/phpstan
14M   api/vendor/vimeo
132K  api/vendor/fidry
2.1M  api/vendor/nikic
456K  api/vendor/amphp
60K   api/vendor/spatie
156K  api/vendor/laravel
6.5M  api/vendor/phpunit
28K   api/vendor/ralouphie
896K  api/vendor/felixfbecker
1004K api/vendor/sebastian
380K  api/vendor/psr
988K  api/vendor/composer
3.4M  api/vendor/symfony
9.4M  api/vendor/squizlabs
92K   api/vendor/netresearch
68K   api/vendor/doctrine
624K  api/vendor/phpdocumentor
28K   api/vendor/dnoegel
76K   api/vendor/theseer
200K  api/vendor/myclabs
224K  api/vendor/webmozart
44M   api/vendor
44M   total
```

на офф сайте приведены разные версии phpunit-a т.к. для старых версий самого
php-языка нужно брать соответствующую версию phpunit. PhpUnit свежей версии
не получится поставить и заставить корректно работать на старом php.


## Работа с PhpUnit

https://docs.phpunit.de/en/10.3/installation.html
https://docs.phpunit.de/en/10.3/installation.html#configuring-php-for-development
https://docs.phpunit.de/en/10.3/installation.html#composer
https://docs.phpunit.de/en/10.3/configuration.html
https://phpunit.de/announcements/phpunit-10.html

```sh
docker compose run --rm api-php-cli vendor/bin/phpunit --help
```
- выводить ли покрытие кода
- --filter - применять ли фильтры и если да то какие
  например если надо запустить тест только для одного конкретного класса или
  даже метода этого класса либо, некого каталога)

- --group можно через аннатации помечать свои тесты разными именами групп, а
  затем по этим именам запускать только тесты из этой группы.
- --exclude-group - запускать все тесты кроме указанной группы.
  например если есть слишком медленные тесты их можно поместить "медленные"
  и затем сказать "запускай все кроме медленных"

- --stop-on-failure  - удобно когда нужно останавливать тесты сразу на первом
  падении и не ждать пока пройдёт все остальные тесты

Команда `vendor/bin/phpunit --help` показывает достаточно много опций
и при каждом запуске придётся указывать нужные опции запуска наших тестов.

Можно создать конфиг файл для phpunit на подобии phpcs.xm psalm.xml и настроить
его под наш проект. Чтобы это сделать можно использовать `--generate-configuration`


```sh
docker compose run --rm api-php-cli vendor/bin/phpunit --generate-configuration
```
При этом запуститься интерактивный режим герерации конфига
```
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.

Generating phpunit.xml in /app

Bootstrap script (relative to path shown above; default: vendor/autoload.php):
Tests directory (relative to path shown above; default: tests):
Source directory (relative to path shown above; default: src):
Cache directory (relative to path shown above; default: .phpunit.cache): var/.phpunit.cache

Generated phpunit.xml in /app.
Make sure to exclude the var/.phpunit.cache directory from version control.
```

vendor/autoload.php - скрипт автозагрузки классов
Можно создать свой скрипт автозагрузки например на основе api/public/index.php
или api/bin/app.php и в этом скрипте установить нужные константы.
Но в нашем проекте нам это не нужно и мы можем использовать дефолтный скрипт
vendor/autoload.php так как в нашем проекте:
- все нужные константы и параметры мы указываем через переменные окружения.
- и даже для тестов не надо никакой переконфигурации наших фреймворков.

Далее выбираем стандартные названия каталогов `tests` и `src`
`src` - будет использоваться для анализа покрытия кодовой базы тестами

Сгенерированный конфиг api/phpunit.xml
```html
<?xml version="1.0" encoding="UTF-8"?>
<phpunit xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:noNamespaceSchemaLocation="https://schema.phpunit.de/10.3/phpunit.xsd"
         bootstrap="vendor/autoload.php"
         cacheDirectory="var/.phpunit.cache"
         executionOrder="depends,defects"
         requireCoverageMetadata="true"
         beStrictAboutCoverageMetadata="true"
         beStrictAboutOutputDuringTests="true"
         failOnRisky="true"
         failOnWarning="true">
    <testsuites>
        <testsuite name="default">
            <directory>tests</directory>  # каталог где должны хранится тесты
        </testsuite>
    </testsuites>

    <source restrictDeprecations="true" restrictNotices="true" restrictWarnings="true">
        <include>
            <directory>src</directory>
        </include>
    </source>
</phpunit>
```


executionOrder="depends,defects"
порядок запуска тестов. Сначала запускать depends - зависимые тесты, затем
defects - сломанные в прошлый запуск( если включено кэширование результатов)

Добавляем суффикс чтобы тестами воспринимались только файлы оканчивающиеся
на 'Test.php' а все остальные php-скрипты воспринимались как скрипты-помошники
а не тесты.
```xml
<testsuite name="default">
    <directory suffix="Test.php">tests</directory>
</testsuite>
```

PhpUnit так же использует кэш для хранения промежуточных результов своей работы.
Например результаты как прошли тесты, для того чтобы в следующий раз изменить
порядок запуска на указанный в конфиге(defects)
`cacheDirectory="var/.phpunit.cache"` в 10 версии оно спрашивает при генерации
в версии [8.5](https://docs.phpunit.de/en/8.5/configuration.html)
было другое свойство под имя файла
`cacheResultFile="var/.phpunit.result.cache"`

сохранять ли результаты тестов в кэш зависят от свойства cacheResult(true|false)
если можно как прописать в xml конфиге так и задавать через опции запуска в cli

на 8.5 был флаг `--test-result` чтобы сохранять результаты тестов в кэш
на 10.3 по умолчанию уже сохраняет и для отметы исп `--no-results`
```
docker compose run --rm api-php-cli vendor/bin/phpunit --no-results
```
Это ускоряет разработку т.к. первыми будут запускаться те тесты которые в прошлый
раз упали с ошибкой.


атрибут был с phpunit v7+
[forceCoversAnnotation](https://docs.phpunit.de/en/8.5/configuration.html#the-forcecoversannotation-attribute)
Possible values: true or false (default: false)

This attribute configures whether a test will be marked as risky
(see Unintentionally Covered Code) when it does not have a `@covers` annotation.

Говорит если хотим запускать тесты с анализом покрытия то такие тесты нужно
помечать аннотацией `@covers` и в этой аннотации нужно указывать какой класс
и какой метод они тестируют т.е. собираемую статистику для каждого метода.
Этот атрибут заставляет для каждого теста добавлять эту аннотацию и указывать
что мы в нём тестируем. Это может быть не удобно для функциональных тестов.

10.3
[requireCoverageMetadata](https://docs.phpunit.de/en/10.3/configuration.html#the-requirecoveragemetadata-attribute)
Possible values: true or false (default: false)

This attribute configures whether a test will be marked as risky
([see Unintentionally Covered Code](https://docs.phpunit.de/en/10.3/risky-tests.html#risky-tests-unintentionally-covered-code))
when it does not indicate the code it intends to cover using an attribute in
code or an annotation in a code comment.


8.5 verbose - подробно выводить сведенья о тестах и ошибка
на новой версии не понятно на что это заменили
10.3 testdox displayDetailsOnIncompleteTests

Если тестов не много то можно и на них запускать линтеры и проверку кода, но
если тестов много то обычно их не добавляют такого рода проверки чтобы экономить
время. Т.е. поломенные тесты и так упадут с ошибками


## APP_ENV test

У нас есть простые параметры для phpunit
но еще надо сделать некоторые вещи для нашего кода
для того чтобы в тестах наш код запускался немного по другому в отличи от
прод или дев-окружения

Если мы запускаем наш public/index.php
- создаётся di-контейнер для нужного окружения по имени передаваемому через
переменную окружения `APP_ENV` и по этому имени окружения(dev|prod) di-контейнер
подгружает сответствующие файлы конфигураций вот таким образом:

./api/config/dependencies.php:
```php
$files = array_merge(
    glob(__DIR__ . '/common/*.php') ?: [],
    glob(__DIR__ . '/' . (getenv('APP_ENV') ?: 'prod') . '/*.php') ?: []
);
```
то есть берёт каталог "общих"(common) конфигураций и дополнительно каталог
по имени окружения передаваемый через `APP_ENV`.(Либо берёт prod по умолчанию)
Само же имя окружения мы прописываем в наших docker-compose файлах.

Для дева это:
docker-compose.yml:
```yml
  api-php-fpm:
    build:
      context: api/docker
      dockerfile: development/php-fpm/Dockerfile
    environment:
      APP_ENV: dev                                # << имя окружения "dev"
      APP_DEBUG: 1                                # выводить ли инфу об ошибках
      PHP_IDE_CONFIG: serverName=API
    volumes:
      - ./api:/app
```

Для продакшена это:
docker-compose-production.yml:
```yml
  api-php-fpm:
    image: ${REGISTRY}/auction-api-php-fpm:${IMAGE_TAG}
    restart: always
    environment:
      APP_ENV: prod                              # << имя окружения "prod"
      APP_DEBUG: 0
```

В общем для тестов нужно переопределить занчения перменных
`APP_ENV=test` и `APP_DEBUG=1` это можно сделать через phpunit.xml
добавив в конец блок
```xml
    <php>
        <env name="APP_ENV" value="test" force="true"/>
        <env name="APP_DEBUG" value="1" force="true"/>
    <php>
</phpunit>
```
force=true - всегда переопределять значения переменных окружения
без опции force="true" режим значение по умолчанию. то есть
переопределение будет "ленивое" -если переменная уже задана то использовать
старое значение не присваивая новое из конфига.
нам же надо строго новое поэтому foce=true

## Прописываем каталог tests в автозагрузку


Дополнительный автолоадер но только в дев-окружении под неймспейсом Test
composer.json
```yml
{

    "autoload": {
        "psr-4": {
            "App\\": "src/"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "Test\\": "tests/"
        }
    },
}
```

Пока у нас будет плоская структура - т.е. все тесты будут внутри одного каталога
tests. Если же код проекта разрастёться то можно будет разбить его на модули
и поместить тесты этого модуля в его подкаталог

Подтянуть прописанный для тестов неймспейс:
```sh
docker compose run --rm api-php-cli composer dump-autoload
```
- Generating autoload files
- Generated autoload files

## Какие тесты нужны для нашего кода. Что нужно проверять в нашем коде

- проверять надо логику работы нашего приложения
уже есть два место два уровня на которых уже нужно проверять логику нашего
приложения
- проверка кода конкретного класса и конкретного метода.
  (инстанцировать обьект класса вызывать методы передавая значания и проверять
  верные ли результаты он возвращает)
каждая единица кода в нашей системе - это юнит, выполняющий некую роль
отсюда и название такое юнит-тесты - тесты проверяющие работу юнитов.

Юнит тесты обычно маленькие и быстрые - ими удобно тестировать классы и методы
но не удобно тестировать контроллеры, маршрутизацию. Так как тот же контроллер
работает не сам по себе в а связке с другими зависимостями, с СУБД, с
маршрутизатором, навешенными на маршруты middleware и аутентификация.

отсюда и второй вид тестов - которые тестирую работу всего приложения в целом
где будут эмулироваться рабочие запросы приходящие от клиентов в наше приложение
Такого рода тесты называются Функциональными

Т.к. мы делаем API то можно сделать тест эмулирующий json-запрос на наш API и
дальше проверить какой json-ответ вернулся по запросу и всё ли в нём правильно
все ли ожидаемые параметры и их значения такие как мы ожидаем.
Такие тесты запускают всё наше приложение и работают напрямую с СУБД и со всеми
имеющимися в приложении зависимостями.
Такие тесты медленные, писать их сложнее.


## Пирамида тестировани. Количество и типы тестов

- Unit-test-ы на каждый метод каждого класса можно написать свой отдельный юниттест
  таких тестов может быть много - отрабатывают быстро, запускаются на голом php

- Функциональные тесты проверяющие маршруты, контроллеры.
  их намного меньше чем юнит тестов хотябы потому что и контроллеров с маршрутами
  меньше чем разнообразных классов и их методов. К тому же эти тесты очень медленные
  и писать их тяжелее

- высокоуровневые тесты проверяющие работу всего нашего приложения.

В нашей системе кроме API есть ещё и Frontend и со временем может добавится
админка, личный кабинет и разнообразные дополнительные системы.
то есть такого рода тесты уже не поместить в каталог ./api их надо выносить на
уровень выше - в корень проекта.
Назначение таких тестов - приёмка и проверка нашей работы.
Отсюда и название - Приёмочные Тесты (Acceptance Tests)
Приёмочные тесты проверяют работу всей системы, у нас есть фронтенд на React
а значит наши приёмочные тесты должны будут эмулировать в браузере
"щёлканье по кнопкам/ссылка" в UI React страниц. Открываются ли и отправляются
ли формы и т.д. Такие тесты работают еще медленнее т.к. работают внтри реальных
браузерах, ждут выполнения "рендеринга" страниц. Но зато могут проверить всю
нашу систему насквозь целиком. Такого рода тесты пишутся уже по другому
подходу не так как функциональные и юнит-тесты. В каждом виде тестов используются
разные технологии, свои уникальные подходы и инструменты.


## Пишем первый тест для класса App\Http\JsonResponse.php

```php
<?php

declare(strict_types=1);

namespace Test\Unit\Http;

use App\Http\JsonResponse;
use PHPUnit\Framework\TestCase;

class JsonResponseTest extends TestCase
{
    public function testInt(): void
    {
        $response = new JsonResponse(12);
        self::assertEquals('12', $response->getBody()->getContents());
        self::assertEquals('200', $response->getStatusCode());
    }
}
```
название методов может начинаться и не со слова test, но тогда надо будет
добавлять аннотацию
```
/**
 * @test
 */
public function checkInt(): void { .. }
```

    self::assertEquals('12', $response->getBody()->getContents());

 тоже самое через привидение типов:

    self::assertEquals('12', (string)$response->getBody());

в http любой передаваемый контент воспринимается как текст

## Запуск теста
```sh
docker compose run --rm api-php-cli vendor/bin/phpunit
```

```
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.23
Configuration: /app/phpunit.xml

RR                                                                2 / 2 (100%)

Time: 00:00.007, Memory: 8.00 MB

There were 2 risky tests:

1) Test\Unit\Http\JsonResponseTest::testInt
This test does not define a code coverage target but is expected to do so

/app/tests/Unit/Http/JsonResponseTest.php:12

2) Test\Unit\Http\JsonResponseTest::testIntWithCode
This test does not define a code coverage target but is expected to do so

/app/tests/Unit/Http/JsonResponseTest.php:19

OK, but there were issues!
Tests: 2, Assertions: 4, Risky: 2.
```
- Tests: `2` - сколько всего тестов
- Assertions: `4` - сколько всего проверок было выполнено во всех тестах
- Risky: `2`. ?

Надпись
`This test does not define a code coverage target but is expected to do so`
убирается когда над тестом помещаешь аннотацию с указание что этот тест проверяет
Класс и метод
```
/**
 * @covers JsonResponse::__construct
 */
```

Вывод когда везде проставлены @covers

```sh
docker compose run --rm api-php-cli vendor/bin/phpunit
```
```
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.23
Configuration: /app/phpunit.xml

..                                                                  2 / 2 (100%)

Time: 00:00.007, Memory: 8.00 MB

OK (2 tests, 4 assertions)
```
две точки здесь это количетство отработавших тестов
2 tests - методов внутри одного тест-класса
4 -assertions - проверок типа self::assertEquals внутри тестовых методов.


Пишем разные варианты тестирования класса JsonResponse
и по традиции добавляем в Makefile короткую команду для запуска тестов:
алиас прописываем в composer.json секции scripts:

        "test": "phpunit --colors=always",

```sh
make api-test
docker compose run --rm api-php-cli composer test
```
```sh
> phpunit --colors=always
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.23
Configuration: /app/phpunit.xml

......                                                              6 / 6 (100%)

Time: 00:00.009, Memory: 8.00 MB

OK (6 tests, 13 assertions)
```

Пример вывода поломанного теста:
```
1) Test\Unit\Http\JsonResponseTest::testArray
Failed asserting that two strings are equal.
--- Expected
+++ Actual
@@ @@
-'{"str":"valuXe","int":1,"none":null}'
+'{"str":"value","int":1,"none":null}'

/app/tests/Unit/Http/JsonResponseTest.php:79

FAILURES!
Tests: 6, Assertions: 12, Failures: 1.
Script phpunit --colors=always handling the test event returned with error code 1
```


## Зачем писать тесты вообще и для JsonResponse в частности

- если перейдём с библиотери Slim\Psr7\Response на некую другую
то в нашем классе App\Http\JsonResponse достаточно будет только переопределить
расширяемый класс на новый из новой библиотеки, переписать конструктор.
И для того чтобы убедится что всё работает исправно - просто запустить уже
имеющиеся тесты. Если с новой библиотекой что-то сломается тесты сразу выявят
что именно да еще и выведут подробный отчёёт что конкретно и где.


## Параметризованный тест

В прошлой версии своего теста [JsonResponse](./api/tests/Unit/Http/JsonResponseTest.php)
тесты писали через копи-паст одной и той же заготовки меняя только передаваемые
в конструктор значения. Такой подход можно переделать на более функциональный.
В простом случае можно просто создать один массив всех подставляемых значений
и ожидаемых результатов и в цикле проходить и проверять каждое из них.

PhpUnit уже есть готовое решение для такого класса задач - Провайдеры данных

Параметризованный тест принимает входные параметры которые к нему идут от

```php
<?php
class JsonResponseTest extends TestCase
{
    /**
     * @dataProvider getCases    # указываем метод предаставляющие входные данные
     * @param mixed $source      # то что подаём на вход из очередных данных
     * @param mixed $expected    # то что ожидаем на выходе из тестируемого метода
     * @covers JsonResponse::__construct
     */
    public function testResponse($source, $expected): void
    {
        $response = new JsonResponse($source);

        self::assertEquals('application/json', $response->getHeaderLine('Content-Type'));
        self::assertEquals($expected, $response->getBody()->getContents());
        self::assertEquals('200', $response->getStatusCode());
    }

    /**
     * Провайдер Данных для testResponse
     * @return array<mixed>
     */
    public static function getCases(): array
    {

        $object = new stdClass();
        $object->str = 'value';
        $object->int = 1;
        $object->none = null;

        $array = ['str' => 'value', 'int' => 1, 'none' => null];
        return [
        // ключ -сообщение выводимое при провале теста
        // значение массив который будет подставлятся в аргументы функции
        // принимающией входные данные
            'null' => [null, 'null'],
            'empty' => ['', '""'],
            'number' => [12, '12'],
            'string' => ['16', '"16"'],
            'object' => [$object, '{"str":"value","int":1,"none":null}'],
            'array' => [$array, '{"str":"value","int":1,"none":null}'],
        ];
        // модно и так без доп обьясняющих сообщений,
        // но если что-то сломается то будет не понятно где и что сломалось
        return [
            [null, 'null'],
            ['', '""'],
        ];
    }

    // можно передавать сколько угодно параметров в тестирующие методы
    // для этого просто создай соответствущий выходной массив в dataProvider
    /** @dataProvider getCases3 */
    public function testThreeArgs($source, $expected, $extra): void
    {
    }
    public static function getCases3($source, $expected, $extra): void
    {
        return [
            [null, 'null', 'extra'],  // три элемента будут переданы в 3 аргум-та
            ['', '""', 'value'],
        ];
    }
```

Провайдер метод в тестовом классе должен быть статическим иначе выдаёт `D`:
```
D......                                                            7 / 7 (100%)
1) Test\Unit\Http\JsonResponseTest::testResponse
Data Provider method Test\Unit\Http\JsonResponseTest::getCases() is not static

OK, but there were issues!
Tests: 7, Assertions: 21, Deprecations: 1.
```

