## Синтаксические анализаторы кода

- эмулируют работу кода для выявления ошибок(в том числе скрытых)
- проверяют корректность типизации в связях нескольких компонентов(функций)
- позволяют выявлять более замудрёные ошибки

Пример зачем это надо:
В компилируемых языках со строгой типизацией (Например Java, C# и т.д)
сам язык заставляет чётко задавать типы переменных и если типы не сходятся
(например функция принимает аргумент типа string а ей пытаются передать int)
то сам компилятор просто выдаст ошибку в момент компиляции и не скомпилирует код.
То есть ошибка выявляется до того как код начнёт использоваться.

В динамических же языках, таких как php, js, lua ошибка может быть обнаружена
только в runtime т.е. в момент работы программы. т.к. такие языки не компилируются
Умные IDE имеют встроенную проверку соответсвия типов и подсвечивают ошибки и
нестыковки. Так же есть такие инструменты для запуска и напрямую из консоли.

Примеры синтаксических анализаторов кода для php:
- PHPStan,
- Psalm,
- Phan.


[Psalm](https://psalm.dev)
It’s easy to make great things in PHP, but bugs can creep in just as easily.
Psalm is a free & open-source static analysis tool that helps you identify
problems in your code, so you can sleep a little better.

```sh
docker compose run --rm api-php-cli composer require --dev vimeo/psalm
```
[output](./docs/09-lint-sniffer-psalm/logs/psalm.log)

```sh
du -hcd 1 api/vendor
580K  api/vendor/slim
40K   api/vendor/fig
528K  api/vendor/php-di
852K  api/vendor/overtrue
44K   api/vendor/bin
508K  api/vendor/phpstan
14M   api/vendor/vimeo
132K  api/vendor/fidry
2.1M  api/vendor/nikic
456K  api/vendor/amphp
60K   api/vendor/spatie
156K  api/vendor/laravel
28K   api/vendor/ralouphie
896K  api/vendor/felixfbecker
132K  api/vendor/sebastian
380K  api/vendor/psr
604K  api/vendor/composer
3.4M  api/vendor/symfony
9.4M  api/vendor/squizlabs
92K   api/vendor/netresearch
68K   api/vendor/doctrine
624K  api/vendor/phpdocumentor
28K   api/vendor/dnoegel
224K  api/vendor/webmozart
35M   api/vendor
35M   total
```

composer json
```json
{
    "require-dev": {
        "overtrue/phplint": "^9.0",
        "roave/security-advisories": "dev-master",
        "squizlabs/php_codesniffer": "^3.7",
        "vimeo/psalm": "^5.15"
    }
}
```
https://github.com/vimeo/psalm/blob/master/docs/running_psalm/configuration.md

сгенерировать файл конфигурации:
```sh
docker compose run --rm api-php-cli ./vendor/bin/psalm --init src 1
```
src - имя каталога с исходным кодом
1 - уровень строгости проверок( 1 самый строгий, 8 - самый не строгий)


```html
<?xml version="1.0"?>
<psalm
    totallyTyped="true"
    resolveFromConfigFile="true"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="https://getpsalm.org/schema/config"
    xsi:schemaLocation="https://getpsalm.org/schema/config vendor/vimeo/psalm/config.xsd"
    xmlns:xi="http://www.w3.org/2001/XInclude"
>
    <projectFiles>
        <directory name="bin" />
        <directory name="config" />
        <directory name="src" />
        <ignoreFiles>
            <directory name="vendor" />
        </ignoreFiles>
    </projectFiles>

    <issueHandler>
      <LessSpecificReturnType errorLevel="info" />
    </issueHandler>
</psalm>
```


totallyTyped="true" - абсолютно всё должно быть типизировано.

LessSpecificReturnType errorLevel="info" - переопеределяем уровень ошибки
для конкретной проверки
уровни ошибок бывают
- info
- warning
- error
- suppress - не выводить инфу об ошибке

Для 8го уровня строгости - все правила будут помечены как errorLevel="info"
т.е. будут только выводится предупреждения, но это не будет считаться ошибкой


```Makefile
analyze: api-analyze             # мето команда для запуска всех анализаторов
# ...
api-analyze:
	docker compose run --rm api-php-cli composer psalm
```

Здесь запуск статической проверки кода выносим в отдельную команду-таргет, а
не помещаем в внутрь `api-lint`-команды т.к. линтеры прогоняются быстро
(у них есть кэширование) а вот статические анализаторы кода (psalm, phpstan и др.)
как правило работают более медленно.
Проверка идёт дольше т.к. в такого типа проверках нужно всегда подгружать все
имеющиеся исходники т.к. проверяются все взаимосвязи всех функций методов и
классов. Потому как изменение кода в одном месте может повалиять на код в других
файлах. Поэтому здесь кэширование и не используют.

```sh
make anlyze
```
[output](./docs/09-lint-sniffer-psalm/logs/psalm-output.log)
В самом строгом (уровень=1) режиме сразу может быть выявлено очень много ошибок.
Особенно на старых - легаси проектах, где за этим не следили сразу от начала.
Но начинать новые проекты для надёжности лучше сразу с самой строгой типизацией.


## Разбор ошибок выданных статическим анализатором кода

```sh
ERROR: MixedAssignment - bin/app.php:13:1 -
  Unable to determine the type that $commands is being assigned to
 (see https://psalm.dev/032)
$commands = $container->get('config')['console']['commands'];
```
Здесь анализатор говорит: Я не понимаю что здесь дастаётся из такого вызова:
`$container->get('config')['console']['commands'];`
 сам метод get() контейнера уже возвращаетс тип `mixed` (user GotoDef)
`$container->get('config')['console']['commands'];`
к тому же дальше что-то там достаётся из некого неизвестного массива
тип "откдуча что брать? какие там типы хз"

[see src](./api/vendor/psr/container/src/ContainerInterface.php)

 Вот сам psalm об этом же говорит в примечании ниже:
```
  The type of $commands is sourced from here -
  vendor/psr/container/src/ContainerInterface.php:20:16
     * @return mixed Entry.
```
Вот что в исходнике обозначено в Doc-секции к методу get
```php
<?
interface ContainerInterface
{
    /**
     * @return mixed Entry.  <<<<
     */
    public function get(string $id);
```

`MixedAssignment` то есть он говорит, что ты в $commands задал тип mixed, а
должен быть array-масси т.к. дальше у нас в коде идёт итерация по массиву
вот он на это и указывает в следующей ошибке

```
ERROR: MixedAssignment - bin/app.php:14:23 -
  Unable to determine the type that $command is being assigned to
  (see https://psalm.dev/032)
foreach ($commands as $command) {            << итерация по массиву из нашего кода
```
Исправляется это указанием типа в коментации + подавлением ошибки MixedArrayAcess
```php
<?
/**
 * @var string[] $commands
 * @psalm-suppress MidexArrayAccess               << это подавляет вывод ошибки
 */
$commands = $container->get('config')['console']['commands'];
foreach ($commands as $command) { # теперь анализатор знает что здесь массив строк
  # ...
}
```


идёт попытка передать Mixed-тип внутрь метода который может принимать
только определённый тип(class) `ContainerInterface`
```
ERROR: MixedArgument - bin/app.php:15:31 -
  Argument 1 of Psr\Container\ContainerInterface::get cannot be mixed,
  expecting string (see https://psalm.dev/030)
    $cli->add($container->get($command));

  The type of $command is sourced from here -
  vendor/psr/container/src/ContainerInterface.php:20:16
     * @return mixed Entry.
```
Исправляется это так:
```php
<?
    /** @var Command $command */  # этим мы явно указываем класс(тип) переменной
    $command = $container->get($name); # по имени команды получаем саму команду
    $cli->add($command); # регистрируем эту команду в cli-приложение
```

Не может вывести тип возвращаемого значения
```
ERROR: MissingClosureReturnType - config/dependencies.php:12:5 -
Closure does not have a return type, expecting mixed (see https://psalm.dev/068)
    static function ($file) {
```
Вот это место в коде:
```php
<?
$configs = array_map(
    static function ($file) {
        return require $file;      # <<< вот это место будет в следующей ошибке
    },                             # это UnresolvableInclude
    $files
);
```
```
ERROR: UnresolvableInclude - config/dependencies.php:13:16 -
Cannot resolve the given expression to a file path (see https://psalm.dev/106)
        return require $file;
```
UnresolvableInclude - не могу понять какой файл здесь подключается(require)
а значит если статический анализатор не может понять какой файл подключается
то соответственно и не сможет понять что из этого файла будет возвращся в return

Исправляем вот так(указать тип принимаемого и возвращаемого значения):
```php
<?
$configs = array_map(

    static function (string $file): array {
        /**
         * @var array                           говорим тип переменной - массив
         * @noinspection PhpIncludeInspection        подавление для PhsStorm
         * @psalm-suppress UnresolvableInclude       подавление ошибки для Plasm
         */
        return require $file;
    },
    static function (string $file): array {
        return require $file;
    },
    $files
);
```



```
ERROR: MixedArgument - config/dependencies.php:18:33 -
Argument 1 of array_merge_recursive cannot be mixed,
expecting array<array-key, mixed> (see https://psalm.dev/030)
return array_merge_recursive(...$configs);
```
Здесь анализатор не может вывести тип переменной $configs там по задумке "список"
массивов наших конфигов для подключения


```
ERROR: MissingParamType - src/Http/JsonResponse.php:13:33 -
Parameter $data has no provided type (see https://psalm.dev/154)
    public function __construct($data, int $status = 200)
```
Говорит что не указан принимаемый тип перменной $data

```
ERROR: UnusedClass - src/Http/JsonResponse.php:11:7 -
Class App\Http\JsonResponse is never used (see https://psalm.dev/075)
class JsonResponse extends Response
```

```
ERROR: MixedArrayAccess - config/middleware.php:9:30 -
Cannot access array value on mixed variable  (see https://psalm.dev/051)
    $app->addErrorMiddleware($container->get('config')['debug'], true, true);

ERROR: MixedArgument - config/middleware.php:9:30 -
Argument 1 of Slim\App::addErrorMiddleware cannot be mixed,
expecting bool (see https://psalm.dev/030)
    $app->addErrorMiddleware($container->get('config')['debug'], true, true);

  The type of $container->get('config')['debug'] is sourced from here -
vendor/psr/container/src/ContainerInterface.php:20:16
     * @return mixed Entry.
```


Исправляем вот так явно указывая тип config-а ( было всё это одной строкой)
```php
<?
    /** psalm-var array{debug:bool} */
    $config = $container->get('config');
    $app->addErrorMiddleware($config['debug'], true, true);
```

psalm-var array{debug:bool} - этим мы говорим какие ключи и типы их значений
должны быть в массиве. т.е. ключ с именем debug и переменной типа bool
PHPDOC так не умеет в отличи от psalm-а

Вот эта ошибка не понятная. класс на деле используется а пишет что нет
```
ERROR: UnusedClass - src/Http/JsonResponse.php:11:7 -
Class App\Http\JsonResponse is never used (see https://psalm.dev/075)
class JsonResponse extends Response
```
нашел временное решение через `/** @psalm-api */`


после исправления всех ошибок
```sh
make analyze
docker compose run --rm api-php-cli composer psalm
> psalm --config=psalm.xml

Install the opcache extension to make use of JIT on PHP 8.0+ for a 20%+ performance boost!

Target PHP version: 8.1 (inferred from composer.json).
Scanning files...
Analyzing files...

------------------------------

       No errors found!

------------------------------

Checks took 0.49 seconds and used 104.281MB of memory
Psalm was able to infer types for 97.1014% of the codebase
```

## psalm + legacy projects

Для старого легаси проекта в котором не использовался статический анализ кода
и не указывались типы можно прописать просто информировать не считая это обшибкой

psalm.xml:
```html

    <issueHandlers>
      <MixedArrayAccess errorLevel="info" />
      <LessSpecificReturnType errorLevel="info" />
    </issueHandlers>
```

## Какой статический анализатор кода лучше?

phpstan будет ругаться на вот такой код:

```php
<?
$files = array_merge(
    glob(__DIR__ . '/common/*.php'),    #  мы у себя делали  ?: [],
    glob(__DIR__ . '/' . (getenv('APP_ENV') ?: 'prod') . '/*.php') ?: []
);
```
тогда как psalm будет пропускать такой код как корректный.
Подвох в этом коде в том, что функци `global` может возвращать `array|false`
т.е. либо массив либо false тогда как array_merge принимает только массивы!

конструкция `global() ?: []` - означает значение либо пустой массив.
то есть мы здесь страхуемся и если gloabl вернёт false - то вместо него берём
новый пустой массив.


В общем нет серебрянной пули и если нужна полная уверенность в коде и полная
его проверка обычно используют и psalm и phpstan вместе проверяя сначала одним
затем другим анализатором.

Здесь мы выбрали psalm потому что:
он поддерживает особые аннотации типа `psalm-suppress` у phpstan такого нет.

## Summary

На данном этапе мы установили линтер, проверку code-style и статический анализатор
кода. Это позволит при работе над проектом получать одназначный и единообразный
код даже в команде из программистов разных уровней. То есть даже если в команду
будет допущен Джуниор, то он не сможет сломать стиль кода и его корректность
просто уже потому что линтеры, код-стайл и статический анализатор не пропустит
плохой код в единую кодовую базу проекта.


