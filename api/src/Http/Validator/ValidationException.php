<?php

declare(strict_types=1);

namespace App\Http\Validator;

use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 *
 * 11-12-2023
 * @author Swarg
 */
class ValidationException extends \LogicException
{
    private ConstraintViolationListInterface $violations;

    public function __construct(
        // то ради чего всё это делаем - список ошибок из валидации обьекта
        ConstraintViolationListInterface $violations,
        string $message = 'Invalid input.',
        int $code = 0,
        \Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
        $this->violations = $violations;
    }

    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
