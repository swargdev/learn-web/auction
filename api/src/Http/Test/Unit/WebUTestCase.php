<?php

declare(strict_types=1);

namespace App\Http\Test\Unit;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\ServerRequestFactory;

/**
 * Unit test helper
 * 12-12-2023
 * @author Swarg
 */
class WebUTestCase extends TestCase
{
    public static function createRequest(
        string $method = 'POST',
        string $path = 'http://test'
    ): ServerRequestInterface {
        return (new ServerRequestFactory())->createServerRequest($method, $path);
    }

    public static function createResponse(): ResponseInterface
    {
        return (new ResponseFactory())->createResponse();
    }
}
