
План:
- через аттрибуты(аннотации) прописываем класс User как сущность,
- прописаваем таблицу с которой должна работать сущность User
- прописываем кастомные типы для своих классов обьектов-значений(Id,Email,Role)
- подключение Token-ов через вложенные обьекты Embedded
- через callback жизненного цикла сущности User создаём фильтр убирающий
  пустые доктриновские обьекты-заглушки пустых токенов
- реализация связи к нашим Network оставляем его классом обьект-значние,
  создаём сущность обёртку(носитель обьекта) для прописыванияя связи One-To-Many
- переписываем User на работу с Network через связующую сущность UserNetwork
- проверяем работоспособность кода через тесты и чеки кода.
- реализуем наш UserRepository на основе доктриновского EntityRepository  (04)
- реализуем Flusher на основе достриноского EntityManager
- проверим правильность написанного нами маппинга сущностей               (05)

<!-- (v21) -->

## Mapping сущностей на таблицы в БД

Теперь у нас уже есть установленная Doctrine-ORM
Есть настройки для работы с БД
[doctrine-config](./api/config/common/doctrine.php)

есть фабрика для контейнера внедрения зависимостей, создающая на основе наших
настроек доктриновский EntityManager

работа с Doctrine-ORM сводится к тому что мы получаем доступ к EntityManager
затем из него либо запрашиваем сущности(из БД), либо сохраняем сущности в БД

Здесь мы реализуем правила-настройки по которым наши сущности будут отображаться
(маппиться) в БазуДанных.

К примеру У нас есть сущность [User](./api/src/Auth/Entity/User/User.php)
по сути это большой и комплексный агрегат, поля которого содержат другие
сущности(классы обьектов-значений)

Теперь нам нужно настроить Doctrine-ORM так чтобы она могла сохранять такие
сущности как User в нашу БазуДанных. Точнее в таблицу с именем auth_users.
В этой таблице будут поля соответствующие полям класса User

Первое с чего начнём - упростим названия длинных классов

NetworkIdentity -> Network
NetworkIdentity.network -> Network.name

и соответственно имена полей тоже изменяем на более короткие названия
networkIdentity -> network


## Продумываем какие таблицы и с какими полями нужны
1) auth_users куда будем помещать пользователей (класс сущности User)
2) таблица для хранения соцсетей(network-ов)

В нешем коде у одного User-а может быть несколько соцсетей.
То есть связь один ко многим.

Как это можно решить.
1) сделать в БД json-поле в которое складывать все обьекты соцсетей в виде
json-массива. И при доставании из Бд восстанавливать json в обьекты network
2) Реализовать связь hasMany,
- сделать отдельную таблицу auth_user_networks
- помещать все соцсети в эту таблицу, привязывая их к User через связь
  один-ко-многим

## Создаём маппинг(отображение) сущностей на таблицы в базе данных
1) В [настройках доктрины](./api/config/common/doctrine.php)
прописываем пути к нашим сущностям в элемент конфиг-массива
config->doctrine->metadata_dirs
до этого он у нас был пустой.
```php
<?
return ['config' => ['doctrine' => ['metadata_dirs'=>[], ]]];
```
metadata_dirs - это масси всодержащий все наши имеющиеся в системе сущности
Значение массива metadata_dirs нами передаётся в фабрику

```
$config = ORMSetup::createAttributeMetadataConfiguration(
    $settings['metadata_dirs'],  // <<<<<
```
по этим путям внутри этой фабрики будет создан драйвер считывающий аттрибуты
из всех php-классов переданной ему директории.
Как уже помним для прода у нас уже настроен кэш для того, чтобы доктрина один
раз прошлась по каталогу с классами наших сущностей, распарсила их все
и на основе прописанных нами аттрибутов(Java-Аннотаций) построила из них кэш
и уже при повторном обращении использовала этот кэш, а не парсила каждый класс
каждый раз заново. То есть один раз парсятся сущности, строится кэш и схема БД
а дальше берётся уже готовая схема.

В Doctrine есть разные пути создания маппинга обьектов в поля таблиц:
- через xml
- через aннотации(синтанксис описываемый в docblock-ах, актуален до php 8
- через аттрибуты - это по сути копия Java Аннотаций, появились с php 8
  аттрибуты в php - это нативный способ навешивать на классы, поля и методы
  некую мето-дата информацию (такая же функциональность как у аннотаций в Java)

```php
<?
return ['config' => ['doctrine' => [
    'metadata_dirs' => [
        __DIR__ . '/../../src/Auth/Entity'  // путь к нашим сущностям
    ],
]]];
```
Пока на данный момент у нас только одна сущность - User, путь к этой сущности
мы и добавили в массив metadata_dirs

## Начинаем привязку нашей сущности User к БазеДанных через Аттрибуты

еще раз аттрибуты по сути это в понятии java аннтотации, видимо название такое
им дали т.к. до php 8 уже использовались аннотаци похожие на java-стиль писались
тоже через @ но только в docblock-ах т.е. внутри коментариев. То есть по сути
это был своего рода кастыль для поддержки подобной Java механики добавления
мето-данных к элементов класса. С php 8 уже была добавлена нативная поддержка
таких же как в java вещей, но т.к. название уже было занято взяли другое название
аттрибуты.

В доктрин есть три спобоса описания привязки
- через аннотации (устарел с php 8 и появление атрибутов)
- через xml-файл
- через yml-файлы (автор доктрин решил отказаться от yaml-файлов)
- через атрибуты - именно этот способ мы и будем использовать

Когда код пишется для себя - используются аннотации или аттрибуты
то есть методата назначается элементам класса прямо внутри исходника.
То есть аннотации и аттрибуты захломляют исходный код сущностей класса.

Описание маппинга в xml-файлах делают когда
пишут код для переиспользования другими людьми
нечто вроде бандлов для симфони, которые используют доктрин и мапят у себя
сущности. Это делается для того чтобы у другого разработчика была возможность
переопределить методанные в наших бандлах через свои xml-файлы.


## Аттрибут того к какой таблице привязываем сущность

Аттрибуты(Аннотации тоже) пишутся в виде неких классов.
В той же симфони много вещей делается через аннотации.

Для настройки доктриновского маппинга используются классы из неймспейса
[Doctrine\ORM\Mapping](./api/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/)

Table.php

[User](./api/src/Auth/Entity/User/User.php)
```php
<?
// ...
use Doctrine\ORM\Mapping as ORM;
// ...
#[ORM\Entity]                    // говорим этот класс сущность для БД
#[ORM\Table(name: 'auth_users')] // говорим в какую таблицу хотим маппить сущность
class User {
    // ...
}
```
Смотри здесь мы сначала прописываем алиас для слова ORM затем используем его
в аттрибуте так же как везде в коде используем расширение неймспейса до
конкретного имени класса. то есть полная запись будет вот такой
`#[ORM\Entity]` -> `#[Doctrine\ORM\Mapping\Entity]`
и на деле это просто php класс и каталоге vendor для него есть исходник который
всегда можно посмотреть и ознакомится как оно там внутри устроено.

[ORM\Entity](./api/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Entity.php)
[ORM\Table](./api/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Table.php)


Для примера как тоже самое делается через устаревшие php-аннотации
То же самое в устаревшем виде через php-Аннотации в docblock-ах
```php
<?
// ...
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name: 'auth_users')
 */
class User {
    // ...
}
```
Вообще в комплекте с Doctrine идёт библиотека для написания своих кастомных
аннотаций для своих целей.

Примр исходного кода доктриновского аттрибута
[ORM\Entity](./api/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Entity.php)
```php
<?php
declare(strict_types=1);
namespace Doctrine\ORM\Mapping;

use Attribute;
use Doctrine\Common\Annotations\Annotation\NamedArgumentConstructor;
use Doctrine\ORM\EntityRepository;

/**
 * @Annotation
 * @NamedArgumentConstructor()
 * @Target("CLASS")                                    // аннотация
 * @template T of object
 */
#[Attribute(Attribute::TARGET_CLASS)]                 // аттрибут
final class Entity implements MappingAttribute
{
    /**
     * @var string|null
     * @psalm-var class-string<EntityRepository<T>>|null
     * @readonly
     */
    public $repositoryClass;

    /**
     * @var bool
     * @readonly
     */
    public $readOnly = false;

    /** @psalm-param class-string<EntityRepository<T>>|null $repositoryClass */
    public function __construct(?string $repositoryClass = null, bool $readOnly = false)
    {
        $this->repositoryClass = $repositoryClass;
        $this->readOnly        = $readOnly;
    }
}
```

- $repositoryClass - через это поле можно переопеределить класс, который
доктрин будет автоматически привязывать в качестве репозитория к сущности над
которой используется этот аттрибут.
Речь здесь не о нашем репозитории UserRepository, а о внутреннем служебном
доктриновском репозитории.

- $readOnly - им можно задать флаг того что сущность не меняется, т.е. сущность
один раз записывается в бд и всё менять её больше нельзя.
при readOnly = true доктрин не будет следить за изменением значения сущности
для того чтобы автоматически обновлять это значение с тем что в БД
То есть readOnly = true - для работы с иммутабельными сущностями, которые один
раз кладутся в БД и больше не редачатся. Удобно для того чтобы снять с доктрин
проверки надо ли следить за состояние сущности для того чтобы обновлять это
состояние с БД. (при flush-ах)

Таким образом
`#[ORM\Entity]` - это с пустым repositoryClass и с дефолтным readOnly = false
для User нам надо следить и обновлять состояние сущности поэтому испо-ем false

## Аттрибут через который указываем в какую таблицу привязывать текущую сущность

`#[ORM\Table(name: 'auth_users')]`


[ORM\Table](./api/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Table.php)

```php
<?php
declare(strict_types=1);
namespace Doctrine\ORM\Mapping;

use Attribute;
use Doctrine\Common\Annotations\Annotation\NamedArgumentConstructor;
/**
 * @Annotation
 * @NamedArgumentConstructor
 * @Target("CLASS")
 */
#[Attribute(Attribute::TARGET_CLASS)]  // этот атрибут применим именно к класснну
final class Table implements MappingAttribute {
    public $name;//string                   - название таблицы
    public $schema;//string                 - схема в этой таблице
    public $indexes;// array<Index>|null    - список уникальных ключей таблицы
    public $uniqueConstraints; // array<UniqueConstraint>|null
    public $options = []; //array<string,mixed>
```

- `$name` - имя таблицы куда будет маппится сущность над которой данный аттрибит

- `$schema` в постгрессе например можно создавать несколько схем для одной бд
по умолчанию создаётся схема public
```
addp@localhost
  database
    app
      schemas
        public
    access methods
    extensions
  roles
```
через консоль глянуть на схемы можно так:

\dn - To list all schemas of the currently connected database

PGPASSWORD=secret psql -h localhost -p 54321 -U app -d app
```
app=# \dn
List of schemas
  Name  | Owner
--------+-------
 public | app
```
упрощенно схема - это как будто в одной базеданных несколько базданных
в нашем проекте будем использовать схему по умолчанию - public потому
поле schema оставляем пустым

- `$indexes`  array<Doctrine\ORM\Mapping\Index>
через это поле можно прописывать уникальные ключи для всей таблицы
это массив каждый элемент которого `Doctrine\ORM\Mapping\Index`

- `$uniqueConstraints`; // array<Doctrine\ORM\Mapping\UniqueConstraint>|null
   список уникальных ключей в нашей таблице
   Мы же не будем прописывать уникальные ключи используя поля этого аттрибута,
   Уникальные ключи будем проставлять для каждого поля (класса?) по отдельности.

- $options = [];  - дополнительные опции используемые для создания таблицы в БД
например кодировка

Заметь в этом исходном коде перед опеределением класса стоит аттрибут
`#[Attribute(Attribute::TARGET_CLASS)]`
TARGET_CLASS - говорит о том, что атрибут этого типа можно применять только к
классу. Будут еще и другие аттрибуты которые можно применять например к полям
класса.


## Прописываем связи для каждого поля класса User

делаем это постепенно через аттрибуты(аннотации) по алиасу ORM

Проще всего это сделать для полей тех типов, которые уже есть в доктрин
т.е. для стандартных полей.

Глянув на поля нашей сущности User
видим что большинство из них хранят наши собственные обьекты-значения
Id, Email, Status, Token, Role
```php
<?
//...
class User {
    private Id $id;
    private DateTimeImmutable $date;              // стандартный тип
    private Email $email;
    private Status $status;
    private ?string $passwordHash = null;         // стандартный тип string
    private ?Token $joinConfirmToken = null;
    private ?Token $passwordResetToken = null;
    private ?Token $newEmailToken = null;
    private ?Email $newEmail = null;
    private ArrayObject $networks;
    private Role $role;
```
В doctrine уже есть типы для поддержки строк и DateTimeImmutable
поэтому начнём с этих полей


```php
<?php class User {
    //...
    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $date;

    #[ORM\Column(type: 'string', name: 'password_hash', nullable: true)]
    private ?string $passwordHash = null; // поле класса - кэмелкейс
    // тогда как имя колонки этого поля в таблице должно быть в нижнем рег. + "_"
```
Аттрибут Column - именно для полей класса сущностей,
чтобы связать поле класса с колонкой в таблице БД.

- name - параметр для прописывания названия колонки имнно для этого поля.
По умолчанию для названия колонки берётся само имя поля класса
но т.к. здесь название составное и второе слово начинается с большой буквы - т.е
в стиле PSR то его надо преобразоваывать в ДБ-стиль.
Но так как мы уже прописали стратегию маппинга имён в конфиге для доктрин
./api/config/common/doctrine.php для конструктора `EntityManagerInterface`

        $config->setNamingStrategy(new UnderscoreNamingStrategy());

то имена полей классов сущностей указывать явно теперь не обязательно.
Иначе бы пришлось делать это над каждым полем класса всех сущностей маппящихся
в БД. То есть теперь маппинг по этой статегии будет делать сама доктрин.

- nullable - этим описываем может ли текущее поле колонки в бд быть пустым.
в нашем случае пароля может и не быть. если регистрация прошла через соцсеть.

[ORM\Column](./api/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Column.php)

```php
<?php
declare(strict_types=1);
namespace Doctrine\ORM\Mapping;
use Attribute;
use BackedEnum;
use Doctrine\Common\Annotations\Annotation\NamedArgumentConstructor;
/**
 * @Annotation
 * @NamedArgumentConstructor
 * @Target({"PROPERTY","ANNOTATION"})
 */
#[Attribute(Attribute::TARGET_PROPERTY)]
final class Column implements MappingAttribute
{
    public $name;    // string|null  - название колонки в таблице БД
    public $type;    // mixed
    public $length;  // int/null

    /* The precision for a decimal (exact numeric) column
      (Applies only for decimal column). */
    public $precision = 0; // int|null
    /* The scale for a decimal (exact numeric) column
      (Applies only for decimal column). */
    public $scale = 0; // int|null

    public $unique = false;    // bool
    public $nullable = false;  // bool Флаг того что поле в бд может быть NULL
    public $insertable = true; // bool
    public $updatable = true;  // bool

    public $enumType = null; // class-string<BackedEnum>|null

    public $options = [];  // array<string,mixed>

    public $columnDefinition; // string|null

    /* @psalm-var 'NEVER'|'INSERT'|'ALWAYS'|null
       @Enum({"NEVER", "INSERT", "ALWAYS"}) */
    public $generated; // string|null
```

дальше сталкиваемся с тем что остальные поля - это наши собственные классы
"подсущностей" обьектов-значений. Как прописать типы для них?


