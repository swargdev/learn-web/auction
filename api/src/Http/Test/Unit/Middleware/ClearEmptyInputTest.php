<?php

declare(strict_types=1);

namespace App\Http\Test\Unit\Middleware;

use App\Http\Middleware\ClearEmptyInput;
use App\Http\Test\Unit\WebUTestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Factory\UploadedFileFactory;

/**
 * @covers ClearEmptyInput
 */
class ClearEmptyInputTest extends WebUTestCase
{
    public function testParsedBody(): void
    {
        $middleware = new ClearEmptyInput();
        $request = self::createRequest()
        ->withParsedBody([
            'null' => null,
            'space' => ' ',
            'string' => 'String ',
            'int' => 42,
            'nested' => [
                'null' => null,
                'space' => ' ',
                'name' => ' Name',
            ],

        ]);

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->expects(self::once())->method('handle')
        ->willReturnCallback(
            static function (ServerRequestInterface $request): ResponseInterface {
                self::assertEquals([
                    'null' => null,
                    'space' => '',
                    'string' => 'String',
                    'int' => 42,
                    'nested' => [
                        'null' => null,
                        'space' => '',
                        'name' => 'Name',
                    ],
                ], $request->getParsedBody());
                return self::createResponse();
            }
        );

        $middleware->process($request, $handler);
    }

    public function testUploadedFiles(): void
    {
        $middleware = new ClearEmptyInput();

        $realFile = (new UploadedFileFactory())->createUploadedFile(
            (new StreamFactory())->createStream(''),
            0,
            UPLOAD_ERR_OK,
        );

        $noFile = (new UploadedFileFactory())->createUploadedFile(
            (new StreamFactory())->createStream(''),
            0,
            UPLOAD_ERR_NO_FILE,
        );

        $request = self::createRequest()
            ->withUploadedFiles([
                'real_file' => $realFile,
                'none_file' => $noFile,
                'files' => [$realFile, $noFile],
            ]);

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->expects(self::once())->method('handle')
            ->willReturnCallback(static function (ServerRequestInterface $request) use ($realFile): ResponseInterface {
                self::assertEquals([
                    'real_file' => $realFile,
                    'files' => [$realFile],
                ], $request->getUploadedFiles());
                return self::createResponse();
            });

        $middleware->process($request, $handler);
    }
}
