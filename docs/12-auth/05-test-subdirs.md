## Тест одного класса из нескольких файлов

Идея - есть один класс например App\Auth\Entity\User\Token.
Причём здесь Auth - имя модуля(src/Auth), тесты для этого модуля храним внутри
`src/Auth/Test/Unit/` -> `App\Auth\Test\Unit\`
Для класса Token изначально пишем тесты в однин класс `App\Auth\Test\Unit\Entity\User\TokenTest.php`
но со временем в этот класс добавляется новый функционал и тесты под него.
Что приводит к разростанию размера тестового класса.
Идея - разделять проверки в тесте на разные файлы по логике того что проверяем.


По умолчанию в java для быстрых прыжков от класса-исходника к классу-тесту
используется тот факт что пути чётко сотносятся как для классов-исходников так
и для тестов под них
наример:
src/main/java/my/pkg/SomeClass.java
src/test/java/my/pkg/SomeClassTest.java

сооветственно сделать механику быстрых прыжков-переключений достаточно просто

В php с phpunit да еще с модулями используется настройка из phpunit.xml
`src/Auth/Test/Unit/` а в composer.json прописан маппинг `src` -> `App`
т.е. каталог src является частью namespace класса (в java namespace==package)
Модули проекта храним в src/ а в самих модулях еще и тесты для его классов.

При таком подходе все тесты одного класса(Token) размещаются в соответствующем
тестом пути в одном тестовом классе (TokenTest) как бы "зеркалом"
```
./api/src/Auth/Test/Unit/Entity/User/
├── EmailTest.php
├── IdTest.php
├── TokenTest.php                     // << тесты для App\Auth\Entity\User\Test
...
```
Для наглядности и разнесения разной логики в тестах одного и того же класса
Можно вынести их в отдельные файлы-классов т.е. разнести TokenTest.php
на несколько файлов но перед этим добавив для них всех общий подкаталог Token:
(./api/src/Auth/Test/Unit/Entity/User/Token)
```
./api/src/Auth/Test/Unit/Entity/User/
├── EmailTest.php
├── IdTest.php
└── Token                             // вместо TokenTest.php
    ├── CreateTest.php                // разная логика проверки одного и тогоже
    └── ValidateTest.php              // класса может разносится в разные файлы
```
<!-- Notes: open_test_or_src toggle_jump -->

Но при таком подхоже нужно улучшать механизм отвечающий за быстрые прыжки от
исходников к тестам и от тестов к исходникам. Так как при прыжках от исходника
нужно будет еще проверять если TokenTest.php не найден но существует каталог
Token/ то прыгать например в первый файл из этого каталога или искать метод
под курсором в файлах c классами тестов каталоге Token и прыгать в нужный.

