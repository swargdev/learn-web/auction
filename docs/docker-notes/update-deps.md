## Обновление библиотек зависимостей (vendor)

Показать библиотеки из каталога vendor для которых вышли новые версии
(покажет вообще для всех библиотек в том числе и для их зависимостей из
composer.lock файла
```sh
docker compose run --rm api-php-cli composer outdated
```

Показать только те из библиотек, которые прописаны в composer.json
т.е. только основные без их зависимостей(более короткий список)
```sh
docker compose run --rm api-php-cli composer outdated --direct
```

Обновить сразу все библиотеки
```sh
docker compose run --rm api-php-cli composer update
```
