## Phinx Легковестная библиотека для создания миграций

Для работы без Доктрин, когда используются свои мапперы годится:

- библиотека [Phinx](https://book.cakephp.org/phinx/0/en/index.html)

Простая библиотека `для создания миграций` через php-код
через удобный построитель запросов
с возможностью как писать SQL-Запросы и Up-Down методы в классах миграций
так и описывывать php-кодом изменение одним методом change,
с автоматическим генерированием нужных SQL-Запросов
(на основе содержимого этого метода `change`)

Phinx makes it ridiculously easy to manage the database migrations for your
PHP app. In less than 5 minutes, you can install Phinx using Composer and create
your first database migration. Phinx is just about migrations without all the
bloat of a database ORM system or application framework.

https://book.cakephp.org/phinx/0/en/install.html
Подключается обычным образом через композер

```sh
php composer.phar require robmorgan/phinx
vendor/bin/phinx init
```
вторая команда для создания конфиг файла для этой либы, в которой можно было бы
прокинуть свой PDO, либо настройки хост-логин-пароль для подключения к БД

Создание миграций через библ-ку Phinx
https://book.cakephp.org/phinx/0/en/migrations.html

cli-команда куда передаём имя новой миграции, для которой нужно сгенерить класс
```sh
vendor/bin/phinx create MyNewMigration
```
формат создаваемого файла класса YYYYMMDDHHMMSS_my_new_migration.php

```php
<?php

use Phinx\Migration\AbstractMigration;

class MyNewMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     *
     */
    public function change()
    {

    }
}
```
Биб-ка Phinx позволяет писать один метод change вместо двух up + down
описываем нужное изменение(миграцию) в одном методе change и библ-ка сама
определяет изменения и делает для него два шага - вперёд и назад (up|down)

Так же есть постоитель запросов через пхп-код для того чтобы не писать голые
SQL-запросы руками:


```php
<?php

use Phinx\Migration\AbstractMigration;

class CreateUserLoginsTable extends AbstractMigration
{
    public function change()
    {
        // create the table
        $table = $this->table('user_logins');
        $table->addColumn('user_id', 'integer')
              ->addColumn('created', 'datetime')
              ->create();
    }
}
```

здесь создаётся таблица user_logins с двумя колонками user_id и created
И библ-ка Phinx сама просчитывает что должно быть делано для наката-отката
т.е. какие SQL-Запросы нужно формировать для шага вперёд и назад.

А можно выполнять SQL-запросы и напрямую `Executing Queries`


```php
<?php

use Phinx\Migration\AbstractMigration;

class MyNewMigration extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        // execute()
        $count = $this->execute('DELETE FROM users');
        // returns the number of affected rows

        // query()
        $stmt = $this->query('SELECT * FROM users'); // returns PDOStatement
        $rows = $stmt->fetchAll(); // returns the result as an array

        // using prepared queries
        $count = $this->execute('DELETE FROM users WHERE id = ?', [5]);
        $stmt = $this->query('SELECT * FROM users WHERE id > ?', [5]);
        // ^ returns PDOStatement
        $rows = $stmt->fetchAll();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
```


А вот пример того как можно было бы работать с этой бил-кой Phinx для того, чтобы
создать нужную нам таблицу пользователй:

Creating a Table

Creating a table is really easy using the Table object.
Let’s create a table to store a collection of users.

```php
<?php

use Phinx\Migration\AbstractMigration;

class MyNewMigration extends AbstractMigration
{
    public function change()
    {
        $users = $this->table('users');
        $users->addColumn('username', 'string', ['limit' => 20])
              ->addColumn('password', 'string', ['limit' => 40])
              ->addColumn('password_salt', 'string', ['limit' => 40])
              ->addColumn('email', 'string', ['limit' => 100])
              ->addColumn('first_name', 'string', ['limit' => 30])
              ->addColumn('last_name', 'string', ['limit' => 30])
              ->addColumn('created', 'datetime')
              ->addColumn('updated', 'datetime', ['null' => true])
              ->addIndex(['username', 'email'], ['unique' => true])
              ->create();
    }
}
```

т.е. здесь через построитель биб-ки Phinx идёт ручное прописываени всех нужных
полей которые должны быть созданы в БД-таблице.
Но при таком подходе надо всё это писать руками и постоянно следить на связью
сущностей с бд(маппингом сущностей на бд)

Но так как мы работаем с библ-кой Доктрин и связи у нас прописываются
автоматически через созданные нами аттрибуты-аннотации расставленные в коде
то использовать Phinx нет смысла, так как для этого есть более удобаная библ-ка
для самой доктрин


