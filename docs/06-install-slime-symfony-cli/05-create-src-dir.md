## Создание каталога src для исходного кодом нашего api

- прописываем автолоадинг классов
- выносим action-обработчик(контроллер) в отдельный класс
- используем стандарт psr для написания фреймворко-независимого кода
- простейшая настройка DI контейнера внедрения зависимостей
- продвинутые DI-Контейнеры с автовайрингом (Autowiring)
- выносим всю конфигурацию из index.php по отдельным файлам api/config
- Делаем файл конфигурации api/config/dependencies.php модульным
- Автозагрузка зависимостей api/config/dependencies.php
- Создаём систему разделения конфига по типу текущего окружения (APP_ENV)
- Выносим код по созданию приложения в отдельный конфиг-файл


Начинам разносить наш код из api/public/index.php в отдельные классы.
Для этого вначале прописывается автозагрузка классов через composer
Вообще можно сделать и свою собственную автозагрузку классов т.к. это вещь
не входит в сам язык php. Но здесь мы будем использовать стандартныую возможность
встроенную в composer.

### Прописываем автозагрузку классов нашего приложения в composer.json
Добавляем в api/composer.json:
```
"autoload": {
   "psr-4": {
        "App\\": "src/"
    }
}
```
этим мы говорим композеру чтобы он брали все классы с неймспейсом начинающимся
с `App\` с каталога src. Он это и будет делать при автозагрузке классов.
суть в том что в php изначально классы тоже надо было руками грузить через
всякие include, require('путь к классу') в composer-е эту штуку автоматизировали.
Но по сути в самом языке этой вещи нет!

После этого нужно перегерировать все автолоадеры в composer-е
```sh
docker compose run --rm api-php-cli composer dump-autoload
```
Эта генерация происхоит в каталоге vendor/composer
вот пример что он там у себя прописывает

./api/vendor/composer/autoload_psr4.php
```php
<?php
return array(
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Slim\\Psr7\\' => array($vendorDir . '/slim/psr7/src'),
    'Slim\\' => array($vendorDir . '/slim/slim/Slim'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/src'),
    'Psr\\Http\\Server\\' => array($vendorDir . '/psr/http-server-handler/src', $vendorDir . '/psr/http-server-middleware/src'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-factory/src', $vendorDir . '/psr/http-message/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Laravel\\SerializableClosure\\' => array($vendorDir . '/laravel/serializable-closure/src'),
    'Invoker\\' => array($vendorDir . '/php-di/invoker/src'),
    'Fig\\Http\\Message\\' => array($vendorDir . '/fig/http-message-util/src'),
    'FastRoute\\' => array($vendorDir . '/nikic/fast-route/src'),
    'DI\\' => array($vendorDir . '/php-di/php-di/src'),
    'App\\' => array($baseDir . '/src'),
);
```
Как видим здесь мапятся все нами используемые неймспейсы(имена пакетов в Java)
и каталоги из которых их брать
выше прописывали `App\\ -> src/` и как видем он здесь тоже есть.

## Выносим action-обработчик(контроллер) в отдельный класс

Выносим код создаём Invokable класс из одного метода __invoke()

Это будет Экшен(Контроллер) для вывода главной страницы
`./api/src/Http/Action/HomeAction.php`:
```php
<?php
// это особого рода класс называемый invokable, работающие как функции
// задаётся методом __invoke не требует конструктора - работает как функция.
// Slim поддерживает такого рода классы(инвокабл)
class HomeAction {
  public function __invoke(Request $request, Response $response, $args) {
    # .. сюда помещаем нашу функцию обработки запроса и заполнения ответа
    $response->getBody()->write('{}');
    return $response->withHeader('Content-Type', 'application/json');
  }
}
```
Теперь маппинг урл-маршрут -- обработчик записывается вот так
api/public/index.php:
```php
<?php
#...
$app->get('/', App\Http\Action\HomeAction::class);
$app->run();
```

Если же метод назвать не __invoke а как-то подругому, тогда надо будет
и хэндлер прописывать иначе
например если назвать метод `home` то прописывать машрут надо вот так:
```
$app->get('/', App\Http\Action\HomeAction::class . ":home");
```
(имя метода канкатенируем к классу через двоеточие)


## Добавляем php-шное расширение ext-json

```sh
docker compose run api-php-cli composer require ext-json
```
Оно нужно для работы с Php-шной функцией json_encode

внутри класса App\Http\Action\HomeAction

```
$response->getBody()->write('{}'); меняем
'{}' --> json_encode(new \stdClass());
```
```php
<?php
// внутри метода который будет обрабатывать запрос
$response->getBody()->write(
  json_encode(new \stdClass(), JSON_THROW_ON_ERROR, 512)
  // если что-то пошло не так кидать ошибку 512
);
return $response->withHeader('Content-Type', 'application/json');
```

сокращаем такой код для краткости т.к иначе будет очень много дублирования.
Для этого делаем хэлпер-класс для работы с нашим кодом
api/src/Http.php и в нём статический хэлпер-метод json
который затем уже можно будет вот так вот использовать

        return \App\Http::json($response, new stdClass());

stdClass - просто тандартный класс чтобы сгенерился json '{}'


## Используем стандарт psr для написания фреймворко-независимого кода

HomeAction с инвокабл классом - спецефичная вещь, которая поддерживается
фреймворком Slim. Фреймворк Slim умеет работь не только в таком формате.
Он может и в формате http-message хэндлеров, которые прописаны в стандарте PSR.

Поэтому если хочется писать фреймворко независимый код то надо отказаться от
простых инвокабл классов  в пользу http-message хэндлеров.

Для этого нужно использовать PSR-ровский интерфейс:

    use Psr\Http\Server\RequestHandlerInterface;

и для своих контроллеров делать классы реализующие этот интерфейс:

    class HomeAction implements RequestHandlerInterface;

а дальше просто переопределяем(override) метод `handle` который:
- принимает `Psr\Http\Message\ServerRequestInterface`;
- возвращает `Psr\Http\Message\ResponseInterface;`

внутри метода хэндлера можно напрямую создавать инстанс нужного Response-а

    $response = new \Slim\Psr7\Response();

но вместо этого будем использовать ResponseFactory реализующий интерфейс
`Psr\Http\Message\ResponseFactoryInterface`.
Это интерфейс ввели в стандарт PSR относительно недавно, ввели для того чтобы
http-фреймворки могли его реализовывать и их можно было использовать не напрямую,
а через интерфесы PSR-а, тем самым создавая более гибкий и независимый код.

Эти абстракции с интерфейсами для того, чтобы независить от одного конкретного
фреймворка - например Slim-а. Как это работает - есть стандарт - PSR, стандарт
описывает в себе интерфейсы которые могут реализовывать конктерные классы в
конкретных фреймворках. стандарт и его интерфейсы по сути это посредники.
можно поменять фреймворк на другой и если используюся интерфейсы, а не прямые
вызовы конкретных методов фрейворка тогда код не придётся переписывать.
Нужно только чтобы новый фреймворк поддерживал используемый в нашем коде
PSR-овский стандарт (тот самый набор интерфейсов).

Здесь можно посмотреть исходный код этого интерфейса
./api/vendor/psr/http-factory/src/ResponseFactoryInterface.php

а здесь интерфейса, который мы реализуем в своих Экшен-классах(контроллерах):
./api/vendor/psr/http-server-handler/src/RequestHandlerInterface.php


Фабрика же используется по нескольким причинам:

1) потому, что в самом Slim для создания обьекта типа
Response нужно передавать кучу параметров а это неудобно.

Руками создавать этот обьект вот так.
```
$response = new \Slim\Psr7\Response();
```
Если перейти в исходник через GotoDefinition по `Rsponse`, который лежит здесь:
`./api/vendor/slim/psr7/src/Response.php`
то увидим что в конструкторе Класса  не всё так просто

```php
<?php

class Response extends Message implements ResponseInterface
{
    public function __construct(
        int $status = StatusCodeInterface::STATUS_OK,
        ?HeadersInterface $headers = null,
        ?StreamInterface $body = null
    ) {
        $this->status = $this->filterStatus($status);
        $this->headers = $headers ?: new Headers([], []);
        $this->body = $body ?: (new StreamFactory())->createStream();
    }
}
```
Как можно заметить body ответа тут не просто текст а реализация интерфейса
`StreamInterface` что уже вносит доп трудности для использования.

вторая причина - это отойти от написания фрейморко зависимого кода.
Если есть хотябы даже одно использование специфичной для конкретного фреймворка
вещи - точнее конкретного класса фреймворка, то код уже становится зависимым от
этого фреймворка и не может использоваться с другими фреймворками.
Поэтому и нужно полностью отойти к PSR-овским интерфейсам во всех мелочах, только
тогда код будет действительно независим от фреймворка.
Речь естественно здесь идёт о Action классах они же Controllers

Перепишем наш HomeAction на полное использование PSR вообще не привязываясь
и не упоминая Slim классы: (./api/src/Http/Action/HomeAction.php)
```php
<?php

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class HomeAction implements RequestHandlerInterface
{
    private ResponseFactoryInterface $factory;

    public function __construct(ResponseFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        // throw new \Slim\Exception\HttpNotFoundException($request);
        $response = $this->factory->createResponse();
        return Http::json($response, new stdClass());
    }
}
```
заметь здесь в метод handle, который должен выдать ответ клиенту, поступает
только запрос от клиента (request) когда мы использовали инвокабл класс
то входил еще и ответ (response) внутрь функции передвал сам фреймворк Slim.

Здесь же теперь для создания response мы используем фабрику `$this->factory->createResponse();`. Конкретный обьект - инстанс фабрики реализующий нужный
нам psr-овский интерфейс должен быть передан в конструктор нашего класса
HomeAction при его инстанцировнии (new HomeAction(...)).

Конкретный класс реализующий нужную нам здесь фабрику (Реализация) идёт
в уже установленном нами пакете `slim/psr`, реализаций этим много разных например
есть и у zen-деактроса(?) и у других фреймворков и компонентов.

Передача инстанса фабрики в конструктор класса и называется на жаргоне
"инжект" зависимости (Dependency Injection) то есть говорят:

Фабрика "инжектится" через конструктор.
На деле это просто передача конкретного инстанса(фабрики) в приватное поле
нашего класса для дальнейшего его использования. при инстанцирования нужного
обьекта Response.

Теперь чтобы такой подход работал нужно настроить наш
`контейнер внедрения зависимостей(DI)` так, чтобы он мог "пробрасывать" нужную нам
фабрику внутрь конструктора нашего класса HomeAction.


## Простейшая настройка DI контейнера внедрения зависимостей.

Суть настройки DI-контейнера здесь в том, что мы мапим класс к функции замыкания
которая возвращает готовый к работе инстанс этого класса.
```php
<?php
$builder = new DI\ContainerBuilder();
$builder->addDefinitions([
    'config' => [
        'debug' => (bool) getenv('APP_DEBUG'),
    ],
    // вот здесь мы прописываем как конкретно инстанцировать наш экшен-класс
    App\Http\Action\HomeAction::class => function() {// это замыкание - closure
        // внутри функции говорим откуда брать фабрику которая будет отдавать Response
        return new App\Http\Action\HomeAction(new \Slim\Psr7\Factory\ResponseFactory());
        // Slim\Psr7\Factory\ResponseFactory - это и есть реализация стандарта PSR
    }
]);
$container = $builder->build();
// здесь мы передаём наш настроенный контейнер внедрения зависимостей (DI)
// (Dependencies Injections) внутрь нашего приложения($app)
$app = AppFactory::createFromContainer($container);

/* теперь при срабатывании маршрута для того, чтобы обработать входящий запрос.
будет инстанцироваться наш класс HomeAction (Контроллер)
При этом при его инстанцирование будет идти "из контейнера внедрения зависимостей"*/
$app->get('/', App\Http\Action\HomeAction::class);
```
Простым языком
Приложение(`$app`) при срабатывании маршрута '/' под капотом фреймворка будет
"дёргать" инстанс нашего класса из переданного ему контейнера(`$container`)
примерно таким образом:
```php
<?php
$action_instance = $container->get(App\Http\Action\HomeAction::class);
```
возращаемое значением будет инстанс нашего класса HomeAction в приватном поле
которого уже будет фабрика, которую мы прописали внутри метода addDefinitions()

То как мы сделали это описание - это простейший способ работы с di-контейнерами.
Но есть и более удобные и продвинутые способы.


## Продвинутые DI-Контейнеры с автовайрингом

При работае с простейшими di-контейнерами каждый класс пришлось нужно вот так вот
как мы это делали выше оборачивая в кэллбэк функцию(замыкание) :

    App\Http\Action\HomeAction::class => function() { return new HomeAction(...); },


Есть продвинутые di-контейнеры, котрые умеют сами парсить конструкторы классов
и подтягивать нужные элементы самостоятельно.
Такие di-контейнеры еще называют `контейнеры с автовайрингом`.

php-di контейнер имеет у себя автовайринг, его можно включать и выключать.

Суть работы продвинутых di-контейнеров в том, что они:
- принимают внешние запросы от других обьектов на получение инстанса конкретного класса
- при таком запросе di-контейнр парсит конструктор нужного класса до инстанцирования
- смотрит какие классы или интерфейсы используются в параметрах конструктора
- согласно настроенному маппингу или неким правилам создают обьекты и
  подставляют в конструктор нужного класса.

Если бы в этом конструкторе был не интерфейс а просто конкретный класс
```
    public function __construct(ResponseFactoryInterface $factory)
    {
        $this->factory = $factory;
    }
```
например вот такой `ResponseFactory`, то в момент запроса инстанса нужного класса
сам контейнер внутри себя создал бы new ResponseFactory() и подставил бы авто-
матически в конструктор нужного для инстанцирования класса.

Когда же в конструкторе указан интерфейс тут уже простые правила di-контейнера
не работают и нужен маппинг из чего создавать. и делается это вот так:

```php
<?php
$builder->addDefinitions([
    'config' => [
        'debug' => (bool) getenv('APP_DEBUG'),
    ],
    // обьявляем маппинг (алиас) означающий как-только у тебя попросят
    // инстанс вот этого вот интерфейса(класса) то
    ResponseFactoryInterface::class => DI\get( // верни ему ссылку Reference на
        Slim\Psr7\Factory\ResponseFactory::class // вот этот вот класс
    ),
]);
```

DI\get - это посути обёртка над new Reference() исходник здесь
./api/vendor/php-di/php-di/src/functions.php
```php
    function get(string $entryName): \DI\Definition\Reference;
    {
        return new Reference($entryName);
    }
```

Таким образом мы опеределям просто соответсвия какой интерфейс из какого
конкретно класса инстанцировать.
В частности мы указали что всякий раз как только у нашего di-контейнера
запросят инстанс класса(на деле интерфейса) `ResponseFactoryInterface`
то он должен будет вернуть запрашивающему ссылку на класс
`Slim\Psr7\Factory\ResponseFactory`, который далее и будет инстанцироваться
и подставляться т.е. "инжектиться" как зависимость. отсюда и такое название (DI)

То есть в момент срабатывания маршрута вызывается инстанцирование класса
HomeAction. причем запрос на инстанцирование отправляется в наш контейнер
т.к. мы его передали в приложение(`$app`). Далее di-контейнер идёт делать
new HomeAction() и видит что в его конструкторе нужен обьект него класса
он смотрит что за класс нужен - видит что это интерфейс ResponseFactoryInterface
т.к. интерфесы di-контейнер не может инстанцировать он идёт в "свои настройки"
которые мы ему прописали и ищет там нужный ему класс(интерфейс), находит
получает по нему ссылку на конкретный класс Slim\Psr7\Factory\ResponseFactory
делает  new Slim\Psr7\Factory\ResponseFactory() получившийся обьект подставляет
в конструктор нашего класса HomeAction и возвращает фреймворку инстанс.
далее фреймворк вызывает метод класса handle передавая в него параметр с запросом
от http-клиента.

Таким образом продвинутые di-контейнеры автоматически делают работу по
инстанцированию классов(созданию обьектов нужных классов) передавая в конструктор
параметры с обьектами (зависимостями) нужных типов(классов) делая это по
правилам(настройкам) нами прописанными.

DI-контейнеры для этого и были придуманы и используются - для внедрения
(подстановки) зависимостей нужных параметров в конструкторы классов при их
инстанцировании.

Не всегда даже продвинутый di-контейнер способен инстанцировать нужный класс.
Т.е. эта автоматика по созданию инстансов нужных классов работает не всегда.
Если например в конктрукторе нужного класса нужно передавать параметр не какого-то интерфейса, или класса а простого типа такого как string или array то для такого
класса нужно будет прописывать маппинг инстанцирования через функцию т.е.
вручную как мы это делали в начале.


## Делаем код более простым \App\Http\Response

недостаток класса HomeAction в том, что код класса получается громоздким
за счёт постоянного вызывания конструктора. С учётом того что это надо будет
делать во всех экшен классах стоит переписать это на более простое решение

Для этого создаём свой собственный Класс наследумый от \Slim\Psr7\Response
(Здесь наследоваться можно от класса любой реализации PSR-7 стандарта
так как у себя мы используем реализацию slim/psr7 то и наследуемся от неё)
./api/src/Http/JsonResponse.php
```php
<?php

declare(strict_types=1);

namespace App\Http;

use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Header;
use Slim\Psr7\Response;

class JsonResponse extends Response
{
    public function __construct($data, int $status = 200)
    {
        // здесь просто переопределяем конструктор родительского класса
        // напрямую задавая его содержимое на основе $data и $status
        parent::__construct(
            $status,
            new Header(['Content-Type' => 'application/json']),
            (new StreamFactory())->createStream(
                json_encode($data, JSON_THROW_ON_ERROR)
            )
        );
    }
}
```

После этого шак класс контроллера сокращается до такого вида:
```php
<?php
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use stdClass;

class HomeAction implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return new \App\Http\JsonResponse(new stdClass());
    }
}
```
Здесь полностью убираем все зависимости т.к нам более не нужна фабрика т.к.
наш собственный класс JsonResponse напрямую наследуется от `Slim\Psr7\Response`
который в свою очередь реализует стандартный интерфейс

    Psr\Http\Message\ResponseInterface

сама реализация PSR-7 стандарта не является частью фреймворка Slim поэтому и
зависимости от этого фреймворка не будет. т.к. slim/psr7 это просто реализация
стандарта(библиотека классов его реализующих) а не часть фреймворка.

Да этот способ вносит зависимость от slim/psr7, но если захочется поменять
реализацию psr7 со slim/psr7 на любую другую, то это можно будет сделать лего и
быстро - просто поменяв наследуемый класс в одном своём классе JsonResponse и всё.

Т.е. получается такой компромиссный вариант:
- да есть зависимость от одной конкретной slim/psr7-библиотеки, но и только
- всё остальное в свохи контроллерах используем psr-овское(интерфейсы)

Такая зависимость от библиотеки реализующий стандарт допустима потому,
что slim/psr7 - это небольшая библиотека, только набор классов, реализующих
стандартный psr7 интерфейс и не более того. эту библи-ку можно подключать в
любой проект через composer.json где уже используются другие библиотеки
реализующие PSR-7. И при этом конфликта между разными такими библиотеками
не будет.

Есть целый ряд библиотек, реализующих psr7-стандарт и его интерфейсы
найти их можно вот так:
https://packagist.org/packages/slim/psr7?query=PSR-7%20implementation
Вот часть из них и топа:

- guzzlehttp/psr7                 PSR-7 message implementation + common utilits
- nyholm/psr7                     A fast PHP7 implementation of PSR-7
- php-http/discovery              Finds and installs PSR-7,.. implementations
- laminas/laminas-diactoros       PSR HTTP Message implementations
- zendframework/zend-diactoros    PSR HTTP Message implementations
- ringcentral/psr7                PSR-7 message implementation
- slim/psr7                       Strict PSR-7 implementation

подписываются они примерно обычно как `PSR-7 message implementation`

На данном этапе у нас уже вынесены контроллеры (экшены) (HomeAction)
и переписаны на фреймворко-независимый вариант когда используем рекомандации
стандарта PSR7(использовать только его интерфейсы).
Далее так же эти интерфесы будет использовать и для наших мидлвере посредников.



## Выносим всю конфигурацию из index.php по отдельным файлам api/config

- обьявление зависимостей      -- api/config/dependencies.php (конфиг)
- код создания di-контейнера   -- api/config/container.php
- код регистрации маршрутов    -- api/config/routes.php
- код регистрации middleware-ов   api/config/middleware.php


```php
$builder = new DI\ContainerBuilder();
$builder->addDefinitions(require __DIR__ . '/../config/dependencies.php');
$container = $builder->build();
```

./api/config/dependencies.php: Просто возвращает массив
```php
<?php

return [
    'config' => [
        'debug' => (bool) getenv('APP_DEBUG'),
    ],
];
```
далее в метод addDefinitions() передаём этот массив подгружая через
require + путь к файлу


    addDefinitions(require __DIR__ . '/../config/dependencies.php');

метод так же поддерживает работу с файлами т.е. можно передавать и просто путь
к файлу а не сам массив из файла:

    addDefinitions( __DIR__ . '/../config/dependencies.php');

DI-Контейнер может понадобится в разных частях нашего кода не только в index.php
поэтому выносим его код в отдельный файл:

./api/config/container.php:
```php
<?php
declare(strict_types=1);
$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(require __DIR__ . '/dependencies.php');
$container = $builder->build();
return $container;
```
Заметь тут инклюд массива из файла уже по другому пути - т.к. инклюд идёт из
того же каталога где лежит и сам dependencies.php


Таким обрзом все конфигурации из api/public/index.php:
```php
<?php
declare(strict_types=1);
http_response_code(500);

require __DIR__ . '/../vendor/autoload.php';

use \Slim\Factory\AppFactory;

/** @var ContainerInterface $container */
$container = require __DIR__ . '/..config/container.php';

$app = AppFactory::createFromContainer($container);

(require __DIR__ . '/../config/middleware.php')($app, $container);
(require __DIR__ . '/../config/routes.php')($app);

$app->run();
```


вынесли в api/config:
```
api/config/
├── container.php
├── dependencies.php
├── middleware.php
└── routes.php
```

Здесь теперь будем прописывать все маршруты внутри своего приложения
api/config/routes.php
```php
<?php
declare(strict_types=1);
use Slim\App;
return static function (App $app): void {
    $app->get('/', \App\Http\Action\HomeAction::class);
};
```

api/config/middleware.php:
```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Slim\App;

return static function (App $app, ContainerInterface $container): void {
    $app->addErrorMiddleware($container->get('config')['debug'], true, true);
};
```


## Делаем файл конфигурации api/config/dependencies.php модульным

В этом файле пока у нас прописывает только конфиг-значение для debug, но сюда
же будем прописывать и маппинг интерфейсов к классам и классы к функции их
инстанцирования. И это могут быть десятки или даже сотни классов и параметров
для разных сущностей. Вот как это выглядит сейчас:

./api/config/dependencies.php:
```php
<?php

return [
    'config' => [
        'debug' => (bool) getenv('APP_DEBUG'),
        // сюда будем добавлять еще конфиг-параметры например пароли к бд
    ],
  /* сюда будем прописывать маппинг интерфейс-класс и класс-инстанс:

    ResponseFactoryInterface::class => DI\get(
        Slim\Psr7\Factory\ResponseFactory::class
    ),
   \App\Http\Action\HomeAction::class => function (...) {
        return new HomeAction(...);
   }

  */
];
```

Поэтому заранее продумываем модульную структуру формирования этого конфиг файла.
Так чтобы он мог собираться из нескольких отдельных файлов-конфигураций, каждый
из которых содержал бы в себе данные о конкретной области - единой ответственности.

Например db.php - для хранения параметров подключения, и описания для фабрики
собирающей ПДО.(тот самый маппинг интерфейс-класс, класс-инстанс для di)
Для шаблонизатора - свой отдельный конфиг файл и так далее.

Для этого можно сделать автозагрузку зависимостей.


## Автозагрузка зависимостей api/config/dependencies.php

Настройки дебага выносим в каталог common (Общее) в файл system.php:
./api/config/common/system.php:
```php
<?php

return [
    'config' => [
        'debug' => (bool) getenv('APP_DEBUG'),
        // сюда будем добавлять еще конфиг-параметры например пароли к бд
    ],
];
```

В самом же файле зависимостей делаем автозагрузку всех конфиг файлов из
каталога common:

./api/config/dependencies.php:
```php
<?php

declare(strict_types=1);
// сканируем указанный каталог для получения списка всех файлов в нём
$files = glob(__DIR__ . '/common/*.php');

// пробегаемся по всем файлам подгружая каждый из них через require и создавая
// список массивов из списка файлов
$configs = array_map(
    static function ($file) { // кэлбэк для вызова на каждый отдельный файл
        return require $file; //
    },
    $files // полный список всех найденых конфиг файлов
);

// "слеиваем" список отдельных массивов из отдельных файлов в единый массив
return array_merge_recursive(...$configs);
```
В итоге этот код будет работать так:
- проходит по каталогу api/config/common
- собирает все имена php-файлов в этом каталоге
- загружает каждый файл через require - добавляя в список configs
- затем список configs состоящий из отдельных массивов обьекдиняет в один общий
  массив - тот самый конфиг который будет использоваться в DI-контейнере.

Это очень удобно тем что теперь достаточно просто разместить в каталог
api/config/common/ например файл db.php прописать там все нужные параметры
и оно будет автоматически подтягиваться в общий конфиг.


## Создаём систему разделения конфига по типу текущего окружения

Чтобы можно было использовать разные настройки для разного окружения.

Зачем это надо:
Например для прода кэш должен быть включен, а для дев-окружения кэш будет мешать
поэтому его надо выключать, чтобы при каждом F5 в браузере файлы обновлялись.

Для создания такой системы достаточно будет создать подкаталоги в api/config:
туда где уже есть api/config/common  такие как dev, prod, test. И указывать
тип окружения через Системную Переменную Окружения(EnvVar) APP_ENV

передавать же эту EnvVar Будем через докер-комоуз

docker-compose.yml
```yml
  api-php-fpm:
    build:
      context: api/docker
      dockerfile: development/php-fpm/Dockerfile
    environment:
      APP_ENV: dev                                 # <<< api/config/dev/
      APP_DEBUG: 1
    volumes:
      - ./api:/app
```

docker-compose-production.yml
```yml
  api-php-fpm:
    image: ${REGISTRY}/auction-api-php-fpm:${IMAGE_TAG}
    restart: always
    environment:
      APP_ENV: prod                               # <<< api/config/prod/
      APP_DEBUG: 0
```

Далее просто добавим в атозагрузку зависимостей(конфиг файлов) обьединение(merge)
каталога `common` с каталогом заданным в `APP_ENV`

```php
<?php
$files = array_merge(
    glob(__DIR__ . '/common/*.php') ?: [],
    glob(__DIR__ . '/'.(getenv('APP_ENV') ?: 'prod') . '/*.php') ?: []
);
```
То есть здесь теперь мы собираем пути к файлам из двух каталогов и обьединяем
эти пути в один список, чтобы затем пройтись по нему и загрузить из каждого
файла в списке его содержимое - массив с конфиг-параметрами.
Так же здесь прописываем окружение по умолчанию - для prod-а это на случай
если APP_ENV не задано.


## Выносим код по созданию приложения в отдельный конфиг-файл

Вот этот вот код по создания приложения
api/public/index.php:
```php
<?
$app = AppFactory::createFromContainer($container);

(require __DIR__ . '/../config/middleware.php')($app, $container);
(require __DIR__ . '/../config/routes.php')($app);
```
в дальнейшем будет использоваться не только в файле index.php
Но и так же в например в feature-тестах. В подобного рода тестах так же будет
создаваться инстанс приложения - `$app` и вызываться для него метод handle
с передачей эмулируемого request-а.

То есть для тестов просто к коду выше добавится строка

    $response = $app->handle($request);


Поэтому выносим код для создания Приложения(\Slim\App) в отдельный конфиг-файл
(Такой файл еще называют bootstrap-приложения)
api/config/app.php:
```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use \Slim\Factory\AppFactory;
use \Slim\App;

return static function(ContainerInterface $container): App {
    $app = AppFactory::createFromContainer($container);
    (require __DIR__ . '/../config/middleware.php')($app, $container);
    (require __DIR__ . '/../config/routes.php')($app);
    return $app;
};
```

Подключение кода по созданию создания приложения будет идти так:

    $app = (require __DIR__ . '/../config/app.php')($container);



Итак на данном этапе мы выстроили чёткую структуру вынеся все настройки и
переиспользуемый код на свои места. и теперь index.php выглядит вот так:

api/public/index.php
```php
<?php

declare(strict_types=1);
http_response_code(500);
require __DIR__ . '/../vendor/autoload.php';

// создаём di-контейнер внедрения зависимостей
/** @var \Psr\Container\ContainerInterface $container */
$container = require __DIR__ . '/..config/container.php';

// создаём приложение передавая ему контейнер
/** @var \Slim\App $app */
$app = (require __DIR__ . '/../config/app.php')($container);
$app->run(); // сам запуск приложения
```

