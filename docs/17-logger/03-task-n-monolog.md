- Задача - сделать удобное логирование для test и dev окружений
- Psr\Container\ContainerInterface
- Библиотека логгирования Monolog
- Установка и конфигурирование логгера Monolog
- Тонкость при создании каталогов var/log
- Перенастравиваем кэши линтера и кодеснифера на с каталога var/ На var/cache/
- PROD-env: Установка правильных прав на каталог var/cache


## Задача - сделать удобное логирование для test и dev окружений

Устраняем недочёт тестового окружения,
добавляем возможность просмотра логов для завалившихся тестов.

Для того чтобы логирование работало и для prod и для test-окружения нужно
сделать запись логов в разные места:

- для Прод-окружения - писать логи в докер(StdErr).
- для Тест-окружения - в лог файл например в /var/log/cli/test.log

Это нужно для того, чтобы при работе с тестами иметь возможность зайти в
лог-файл и посмотреть в логах полные стектрейсы того, что пошло не так в тестах.

А значит нужно заменять стандартную php-функцию `error_log` просто пишущую
сообщения в StdErr("на экран"). Нужно более продвинутая система логирования.
Которая бы позволяла записывать еще и разную служебную информацию.
Например при создании своих HTTP-клиентов будет нужна возможность логирования
http-запросов-ответов между сервером и http-клиентами.
чтобы работая над своими http-клиентами было проще разбираться что не работает
и почему.


## Psr\Container\ContainerInterface

Подходим к использованию готовой библиотеки для логгирования.

Аналогично общепринятому интерфейсу `Psr\Container\ContainerInterface`
для DI-контейнеров в PSR есть и интерфейс для логеров:

[PSR-3: Logger Interface](https://www.php-fig.org/psr/psr-3/)
This document describes a common interface for logging libraries

The main goal is to allow libraries to receive a Psr\Log\LoggerInterface object
and write logs to it in a simple and universal way. Frameworks and CMSs that
have custom needs MAY extend the interface for their own purpose,
but SHOULD remain compatible with this document.
This ensures that the third-party libraries an application uses can
write to the centralized application logs.

Это стандартный общепризнаный и широко используемый интерфейс описывающий все
необходимые для логирования методы.

можно вызывать спец методы потипу info debug error и проч.

вот пример error-метода:
```php
<?

namespace Psr\Log;

interface LoggerInterface {
    # ...

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function error($message, array $context = array());
    # ...
    public function warning($message, array $context = array());
    public function notice($message, array $context = array());


    // этот же метод является общим для всех подобных error infor debug и проч:

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return void
     */
    public function log($level, $message, array $context = array());
}
```

либо общий метод log передавая в самое начала уровень лога в виде строки:

```php
<?php
namespace Psr\Log;
/* Describes log levels. */
class LogLevel
{
    const EMERGENCY = 'emergency';
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const ERROR     = 'error';
    const WARNING   = 'warning';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';
}
```

причем кроме самого сообщения `$message` можно еще передавать массив `$content`
с доп данными которые так же нужно записать в логи.

Интерфейс един и общепризнан. реализация интерфейса может быть разная.
и просто подключив библиотеку реализующую этот интерфейс получим рабочий логгер.

## Библиотека логгирования Monolog

Ныне наиболее широко используемый логгер - `monolog`

https://github.com/Seldaek/monolog
stars:20k forks:1.9k

используется почти во всех современных фреймворках
реализует PSR-овский LoggerInterface

## Как работать с Monolog:

```php
<?php

use Monolog\Level;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('name'); // имя приложения
// добавляем обработчик записывающий лог-сообщения
// куда писать и минимальный уровень с которого сообщени пишим в логи
$log->pushHandler(new StreamHandler('path/to/your.log', Level::Warning));

// теперь логгер готов к приёму и записи лог-сообщений в указанный файл
// add records to the log
$log->warning('Foo');
$log->error('Bar');
```

Monolog - достаточно зрелый продукт и уже накопил достаточно много разных
обработчиков логов, позволяющих отправлять логи в разные места хоть в телеграм бот
TelegramBotHandler.php. этот функционал содержат в себе хэндлеры(обработчики)

все доступные обработчики можно посмотреть в репозитории:
https://github.com/Seldaek/monolog/tree/main/src/Monolog/Handler

Для наших целей подходит `StreamHandler` - запись логов в один заданный файл
RotatingFileHandler - наследует StreamHandler позволяет делать ротацию логов,
создавая каждый день новый лог файл. Для того чтобы не писать всё в один огромный
файл. Можно указать сколько логов хранить - maxFiles (Тогда старые файлы будут
удаляться автоматически)


## Установка и конфигурирование логгера Monolog

```sh
composer require monolog/monolog
docker compose run --rm api-php-cli composer require monolog/monolog
```
```
./composer.json has been updated
Running composer update monolog/monolog
Loading composer repositories with package information
Updating dependencies
Lock file operations: 1 install, 0 updates, 0 removals
  - Locking monolog/monolog (3.5.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
  - Downloading monolog/monolog (3.5.0)
  - Installing monolog/monolog (3.5.0): Extracting archive
11 package suggestions were added by new dependencies, use `composer suggest` to see details.
Generating autoload files
No security vulnerability advisories found.
Using version ^3.5 for monolog/monolog
```

du -hcd 1 api/vendor/monolog/
1.5M  api/vendor/monolog/monolog
total 58M


Создаём новый файл под фабрику логгера по запросу `LoggerInterface`

./api/config/common/logger.php
```php
<?
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return [
    LoggerInterface::class => static function (ContainerInterface $container) {
        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{
         *     debug:bool,
         *     stderr:bool,
         *     file:string,
         *     processors:string[]
         * } $config
         */
        $config = $container->get('config')['logger'];

        // уровень логирования у нас будет зависить от режима работы
        // в режиме отладки пишем больше начиная от Debug-уровня.
        // уровень это число - минимальный уровень с которого делать логирование
        $level = $config['debug'] ? Level::Debug : Level::Info;
        // таким образов в обычном режиме не debug сообщения будут записываться
        // от уровня Info и выше, и НЕ будут записываться Debug-сообщения

        // создание логгера API - название подсистемы нашего проекта
        $log = new Logger('API');

        // дальше надо задавать два способа записи логов в StdErr и в файл:

        if ($config['stderr']) {// стандартный поток вывода - для докера
            $log->pushHandler(new StreamHandler('php://stderr', $level));
        }

        // дополнительный обработчик если нужно писать логи еще и в файл
        if (!empty($config['file'])) {
            $log->pushHandler(new StreamHandler($config['file'], $level));
        }

        return $log;
    },

    'config' => [
        'logger' => [
            'debug' => (bool)getenv('APP_DEBUG'),
            'file' => null,   // не писать логи в файл (это для prod-а)
            'stderr' => true, // писать логи в стандартный поток вывода ошибок
        ],
    ],
];
```

Для дев-окружения логи будут писаться как в файл так и стандартный вывод - т.е.
их можно будет смотреть еще и через `docker compose logs`

./api/config/dev/logger.php
```php
<?php

declare(strict_types=1);

return [
    'config' => [
        'logger' => [
            'file' => __DIR__ . '/../../var/log/' . PHP_SAPI . '/app.log',
            // записывать в файл в кталоге с названием режима под которым запущены
            // PHP_SAPI для php-cli будет `cli`
        ],
    ],
];
```

для тестового окружения надо будет еще добавить выключение вывода логов в StdErr
чтобы стектрейсы не вываливались на экран, а только писались в файл лога.
Это для того чтобы информация о работе самих тестов не перемешивалась с логами

```php
<?php

declare(strict_types=1);

return [
    'config' => [
        'logger' => [
            'file' => __DIR__ . '/../../var/log/' . PHP_SAPI . '/test.log',
            'stderr' => false, // не писать в StdErr             ^^^^^ файл
        ],
    ],
];
```

Дальше нужно настроить ErrorMidleware на запись логов без вывода.


## Тонкость при создании каталогов var/log

На данный момент каталога `./var/log` нет.
при первом запуске приложения сам `StreamHandler` создаст этот каталог.

Смотрим в исходник как `StreamHandler` будет создавать нужный ему каталог:

./api/vendor/monolog/monolog/src/Monolog/Handler/StreamHandler.php
```php
<?
namespace Monolog\Handler;
class StreamHandler extends AbstractProcessingHandler {
    # ...

    private function createDir(string $url): void
    {
        // Do not try to create dir if it has already been tried.
        if (true === $this->dirCreated) {
            return;
        }

        $dir = $this->getDirFromStream($url);
        if (null !== $dir && !is_dir($dir)) {
            $this->errorMessage = null;
            set_error_handler([$this, 'customErrorHandler']);

            // создать каталог с правами для всех, рекурсивно(true)
            $status = mkdir($dir, 0776, true);
            # ...
        }
        $this->dirCreated = true;
    }
```
Но здесь есть проблема в том, что при рекурсивном создании нескольких каталогов
правильные права назначатся только последнему каталогу из всего пути

т.е. для нашего случая если запустить в начале cli-приложение тогда
запуск пойдёт от имени администратора, создавать надо бдует путь `var/log/cli/`
и права правильными(776) будут только для каталога `cli` для каталога `log`
выставятся права по умолчанию (443) причем владелец назначится `root`
и войти в этот каталог от имени юзера под котором идёт разработка будет нельзя

Простейший вариант решения - руками создать `./var/log/.gitignore` в корне
проекта с нужными провами. в сам файл .gitignore: "игнорить всё кроме себя"
`*
!.gitignore
`

Вместо `chmod 776 var` прописываем два пути `var/cache` `var/log`:

```Makefile
api-permissions:
	docker run --rm -v ${PWD}/api:/app -w /app alpine chmod 776 var/cache var/log
# так же обновляем и команду для удаления всего "мусора" перед make-init
api-clear:
	docker run --rm -v ${PWD}/api:/app -w /app alpine sh -c 'rm -rf var/cache/* var/log/*'
```


## Перенастравиваем кэши линтера и кодеснифера на с каталога var/ На var/cache/

Теперь у нас в var появился каталог log и для того чтобы не валить всё в одну
кучу разделим кэши линтера и кодснифера в отдельный каталог cache:

./api/phpcs.xml
```html
<?xml version="0.0" encoding="utf-8"?>
<ruleset name="App coding standard">
    <arg value="p"/>
    <arg name="colors"/>
    <arg name="cache" value="var/cache/.phpcs.json"/>
                                ^^^^^^
```

./api/.phplint.yml
```yml
path: ./
jobs: 7
cache: var/cache/.phplint.json
          ^^^^^^
extensions:
  - php
exclude:
  - vendor
  - var
```

Для того чтобы кэши собирались не напрямую прямо в каталог var/ а в var/cache/



## PROD-env: Установка правильных прав на каталог var/cache

Prod работает через докер. поэтому нужно прописывать права на var/ в Dockerfile:

./api/docker/production/php-fpm/Dockerfile

php-fpm внутри этого контейнера работает не от `root` а от `www-data`
поэтому важно проставить права для доступа `php-fpm`,
иначе доступ к кэшу для него будет заблокирован.

```Dockerfile
# ...
FROM php:7.1-fpm-alpine
#...
COPY --from=build /app ./
COPY ./ ./

# в самый конец файл - рекурсивно проставить права для правильного владельца:
RUN chown www-data:www-data ./var -R
```

Для прод окружения будет использовать только каталог `var/cache`,
`var/log` не используется. т.к. логи собирает сам докер.
в кэш же будут кэшироваться данные от библиотеки Doctrine



