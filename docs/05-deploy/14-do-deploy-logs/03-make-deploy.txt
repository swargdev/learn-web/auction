ssh root@10.144.52.181 -p 22 -i ~/.ssh/keys/site-auction/id_rsa 'rm -rf site_1'
ssh root@10.144.52.181 -p 22 -i ~/.ssh/keys/site-auction/id_rsa 'mkdir site_1'

scp -P 22 -i ~/.ssh/keys/site-auction/id_rsa docker-compose-production.yml \
    root@10.144.52.181:site_1/docker-compose-production.yml
docker-compose-production.yml                     100%  586     6.4KB/s   00:00

ssh root@10.144.52.181 -p 22 -i ~/.ssh/keys/site-auction/id_rsa \
    'cd site_1 && echo "COMPOSE_PROJECT_NAME=auction" >> .env'

ssh root@10.144.52.181 -p 22 -i ~/.ssh/keys/site-auction/id_rsa \
    'cd site_1 && echo "REGISTRY=registry.gitlab.com/swargdev/learn-web/auction" >> .env'

ssh root@10.144.52.181 -p 22 -i ~/.ssh/keys/site-auction/id_rsa \
    'cd site_1 && echo "IMAGE_TAG=1" >> .env'

ssh root@10.144.52.181 -p 22 -i ~/.ssh/keys/site-auction/id_rsa \
    'cd site_1 && docker compose -f docker-compose-production.yml pull'

frontend Pulling
 api Pulling
 api-php-fpm Pulling
 gateway Pulling
 api-php-fpm Pulled
 api Pulled
 frontend Pulled
 gateway Pulled

ssh root@10.144.52.181 -p 22 -i ~/.ssh/keys/site-auction/id_rsa \
    'cd site_1 && docker compose -f docker-compose-production.yml up \
    --build --remove-orphans -d'
 Network auction_default  Creating
 Network auction_default  Created
 Container auction-api-php-fpm-1  Creating
 Container auction-frontend-1  Creating
 Container auction-frontend-1  Created
 Container auction-api-php-fpm-1  Created
 Container auction-api-1  Creating
 Container auction-api-1  Created
 Container auction-gateway-1  Creating
 Container auction-gateway-1  Created
 Container auction-frontend-1  Starting
 Container auction-api-php-fpm-1  Starting
 Container auction-api-php-fpm-1  Started
 Container auction-frontend-1  Started
 Container auction-api-1  Starting
 Container auction-api-1  Started
 Container auction-gateway-1  Starting
 Container auction-gateway-1  Started

ssh root@10.144.52.181 -p 22 -i ~/.ssh/keys/site-auction/id_rsa 'rm -f site'
ssh root@10.144.52.181 -p 22 -i ~/.ssh/keys/site-auction/id_rsa 'ln -sr site_1 site'
