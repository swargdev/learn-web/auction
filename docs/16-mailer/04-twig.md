- Подключение шаблонизатора twig
- Как использовать: Basic API Usage
- Установка twig-а
- Делаем шаблоны писем для рендеренга в Sender-e(Отправителе писем)
- Переписываем Sender на использование шаблонизатора Twig
- Рефакторинг JoinConfirmationSender-а своё расширение для twig и frontend_url
- Пишем своё расширение FrontendUrlTwigExtension
- Пишем тест для FrontendUrlTwigExtension
- Переписываем шаблон(представление) на использование функции frontend_url
- Реализуем остальные наши Sender-ы:



## Подключение шаблонизатора twig

Twig - часто используемый шаблонизатор для создания html-страниц
twig - это универсальный шаблонизатор, который можно использовать на любых
проектах с любыми фреймворками, устанавливается как отдельная библ-ка

https://twig.symfony.com
https://twig.symfony.com/doc/3.x/intro.html#installation


```sh
composer require "twig/twig:^3.0"
docker compose run --rm api-php-cli composer require "twig/twig:^3.0"
```

## Как использовать: Basic API Usage

This section gives you a brief introduction to the PHP API for Twig:

```php
<?
require_once '/path/to/vendor/autoload.php';

$loader = new \Twig\Loader\ArrayLoader([
    'index' => 'Hello {{ name }}!',
]);
$twig = new \Twig\Environment($loader);

echo $twig->render('index', ['name' => 'Fabien']);
```

Twig uses a loader (\Twig\Loader\ArrayLoader) to locate templates,
and an environment (\Twig\Environment) to store its configuration.

The render() method loads the template passed as a first argument and
renders it with the variables passed as a second argument.

As templates are generally stored on the filesystem,
Twig also comes with a filesystem loader:

```php
<?
$loader = new \Twig\Loader\FilesystemLoader('/path/to/templates');
$twig = new \Twig\Environment(
    $loader, // в шаблонизатор педаётся загрузчик шаблонов и
    [// указывается
     // куда кэшировать скомпиленные шаблоны, (где хранить их php-код)
    'cache' => '/path/to/compilation_cache',
    ]
);
// создание html- страницы на основе шаблона из файла index.html
echo $twig->render(
  'index.html',         // название представления которое рендерим
  ['name' => 'Fabien']  // параметры нужные для рендеринга этого представления
);
```

- Twig\Environment - сам шаблонизатор
- \Twig\Loader\FilesystemLoader - загрузчки шаблонов из файлов указанного каталога


## Установка twig-а

```sh
docker compose run --rm api-php-cli composer require "twig/twig:^3.0"

./composer.json has been updated
Running composer update twig/twig
Loading composer repositories with package information
Updating dependencies
Lock file operations: 1 install, 0 updates, 0 removals
  - Locking twig/twig (v3.8.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
  - Downloading twig/twig (v3.8.0)
  - Installing twig/twig (v3.8.0): Extracting archive
Generating autoload files
No security vulnerability advisories found.
```

du -hcd 1 api/vendor/twig
1.1M api/vendor/twig
total: (57M)

Добавляем фабрику собирающую обьект `Twig\Environment`
./api/config/common/twig.php:
```php
<?
return [
     // сами настройки в конфиг массиве
    'config' => [
        'twig' => [
            'debug' => (bool)getenv('APP_DEBUG', '0'),
            'template_dirs' => [
                // каталог где будем хранить шаблоны это ./api/templates
                FilesystemLoader::MAIN_NAMESPACE => __DIR__ . '/../../templates',
            ],
            'cache_dir' => __DIR__ . '/../../var/cache/twig',
            'extensions' => [], // для описания доп. плагинов-расширений
        ],
    ],
];
```

Описываем саму фабрику нужного нам Twig\Environment-а
```php
<?

return [
    Twig\Environment::class =>
    static function (ContainerInterface $container): Twig\Environment {
        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{
         *     debug:bool,
         *     template_dirs:array<string,string>,
         *     cache_dir:string,
         *     extensions:string[],
         * } $config
         */
        $config = $container->get('config')['twig'];

        // загрузчик шаблонов из каталогов, ему нужно указать где их искать.
        $loader = new FilesystemLoader();

        // созда загрузчик указываем откуда брать шаблоны
        // подгружаем все шаблоны из `./api/templates/`
        foreach ($config['template_dirs'] as $alias => $dir) {
            $loader->addPath($dir, $alias);
        }

        // Twig\Environment - Это и есть сам шаблонизатор
        $environment = new Twig\Environment($loader, [
            // включать ли кэширование шаблонов (выключать когда включен дебаг)
            // если дебаг не включе - подставлять каталог куда кэшировать
            'cache' => $config['debug'] ? false : $config['cache_dir'],
            'debug' => $config['debug'],  // режим отладки
            'strict_variables' => $config['debug'],
            'auto_reload' => $config['debug'],
        ]);

        if ($config['debug']) {
            // доп. расширение позволяющие просматривать передаваемые в шаблон
            $environment->addExtension(new DebugExtension()); // переменные
        }

        // подключение доп.расширений(плагинов для подключения)
        foreach ($config['extensions'] as $class) {
            /** @var ExtensionInterface $extension */
            $extension = $container->get($class);
            $environment->addExtension($extension);
        }

        return $environment;
    },
    #  config = ...
]
```
Всё такая фабрика будет создавать полностью готовый к работе шаблонизатор-twig

Так же как для доктрин переопределим для dev & test окружения каталог с кэшем

./api/config/test/dev.php:
./api/config/test/twig.php:

```php
<?php

declare(strict_types=1);

return [
    // 'config' => [
        'twig' => [
            'cache_dir' => __DIR__ . '/../../var/cache/' . PHP_SAPI . '/twig',
        ],
    // ],
];
```


## Делаем шаблоны писем для рендеренга в Sender-e(Отправителе писем)

Сначала создаём общий шаблон `./api/templates/mail.html.twig`
в нём делаем общую первоначальную разметку с логотипами, футерами, copyrigh и проч.
далее внутри этого общего шаблона обьявляем блок `body`:


```html
<!DOCTYPE ...>
<html>
<head>...</head>
<body>
    ...
{% block body %}{% endblock %}                   << вот этот блок для twig

</body>
</html>
```

его и будем переопределять в каждом конкретном своём письме

Теперь можно в конкретном шаблоне конкретно для писем подтверждения
переопределить содержание блока body
./api/templates/auth/join/confirm.html.twig

```html
{% extends 'mail.html.twig' %}

{% block body %}

<p style="margin: 0 0 10px 0">Confirm your email:</p>

<p style="margin: 0">
    <a style="color: #1a8aee; text-decoration: none" href="{{ url }}">
        <span style="color: #1a8aee; text-decoration: underline">{{ url }}</span>
    </a>
</p>

{% endblock %}
```

внутри брока простой html-вёрсткой делаем чтобы выводилось сообщение
`Confirm your email` и создавалась ссылка на подтверждение
Здесь ссылка {{ url }} оборачиваетмся в стандартный html-тег `<a>`
и дополнительно в `<span>` чтобы добавить ему доп цвета.

Вёрстка старниц для писем отличается от вёрстки простых страниц.
при верстке писем стили нужно прописывать напрямую в html-элементах
без использования отдельных таблиц стилей, классов стилей и тому подобного

Теперь этот шаблон приняв переменную url подставит в неё значение и сгенерит
полное html-тело письма


## Переписываем Sender на использование шаблонизатора Twig

```php
<?
class JoinConfirmationSender
{
    private MailerInterface $mailer;
    private FrontendUrlGenerator $frontend;
    private Environment $twig;

    public function __construct(
        MailerInterface $mailer,
        FrontendUrlGenerator $frontend,
        Environment $twig,
    ) {
        $this->mailer = $mailer;
        $this->frontend = $frontend;
        $this->twig = $twig;
    }

    public function send(Email $email, Token $token): void
    {
        $url = $this->frontend->generate(
            'join/confirm',
            [ 'token' => $token->getValue()]
        );

        $message = (new MimeEmail())
        ->subject('Join Confirmation')
        ->to($email->getValue())
        ->html(
            $this->twig->render('auth/join/confirm.html.twig', [ 'url' => $url ]),
            'text/html'
        );

        $this->mailer->send($message);
    }
}
```

Отправляем письмо через консольную команду и смотрим в MailHog:
```
Confirm your email:

http://localhost:8080/join/confirm?token=53b8e9ab-ed54-4735-99fe-1c612d1d3266
If it is an error just remove this mail.
```

Письмо сверсталось в Html страницу, тело которой можно глянуть в `Plain text`

А значит сама отправка токенов подтверждения регистрации работает,
хотя пока нет самого приёма этих токенов, но адреса уже генерируются с полным
абсолютным адресом( для dev-там у нас указан localhost:8080 но такой страницы
на самом фронтенде нет - поэтому буте выдан код 404 не найдено


## Рефакторинг JoinConfirmationSender-а своё расширение для twig и frontend_url

перенесём использование FrontendUrlGenerator внутрь twig шаблонов

в нашем классе JoinConfirmationSender отправляющего письма сам адрес- ссылка
генерируется напрямую через FrontendUrlGenerator, который мы пока что напрямую
передаём в конструктор класса. Это значит что его надо будет передвать во все
места где нам нужна будет генерация полной ссылки на наш сайт.

```php
<?
class JoinConfirmationSender
{
    public function __construct(
        MailerInterface $mailer,
        FrontendUrlGenerator $frontend, // обьект генерящий ссылки
        Environment $twig,
    ) {
        //
    }
    // в методе send его использование:  $url = $this->frontend->generate(...)
```

Вообще его можно переместить внутрь самого шаблона, прописав некую функцию
например `frontend_url` и в неё передавать относительный адрес и токен.
такая функция на деле будет просто вызывать обьект FrontendUrlGenerator который
и будет склеивать переданные значения в полную ссылку на фронтенд

Пример твиг шаблона с вызовом подобной функции:

```html
{% extends 'mail.html.twig' %}

{% block body %}

<p style="margin: 0 0 10px 0">Confirm your email:</p>

<p style="margin: 0">
    <a style="color: #1a8aee; text-decoration: none"
        href="{{ frontend_url('join/confirm', {'token': token}) }}">
        <span style="color: #1a8aee; text-decoration: underline">{{ url }}</span>
    </a>
</p>

{% endblock %}
```
Этим мы переносим генерирование ссылок из контроллеров, сервисов и подобного
прямо внутрь шаблонов.
Для этого и нужны расширения(extensions), которые поддерживает twig.
Можно создать своё расширение добавляющее новый функционал внутрь шаблона
например функции, фильтры и прочее.

Нам нужно написать расширение добавляющее в шаблон нашу функцию
Само добавление своих расширений мы уже прописали в конфиге:

```php
<?
        foreach ($config['extensions'] as $class) {
            /** @var ExtensionInterface $extension */
            $extension = $container->get($class);
            $environment->addExtension($extension);
        }
```

## Пишем своё расширение FrontendUrlTwigExtension

Рядом с генератором полных путей создаём твиг-расширение:

./api/src/Frontend/FrontendUrlTwigExtension.php
```php
<?php

declare(strict_types=1);

namespace App\Frontend;

// наш класс реализует абстрактный класс расширений твига
class FrontendUrlTwigExtension extends Twig\Extension\AbstractExtension
{
    // тот самый обьект собирающий полные адреса(ссылки)
    private FrontendUrlGenerator $url;

    public function __construct(FrontendUrlGenerator $url)
    {
        $this->url = $url;
    }

    // реализует-переопределяет абстактный метод базового класса
    // через этот метод мы и можем указать свои функции доступные из шаблонов
    public function getFunctions(): array
    {
        return [
            // название функции и откуда её вызывать
            new Twig\TwigFunction('frontend_url', [$this, 'url']),
            // в массиве задаётся своего рода callback на метод ниже:
        ];
    }

    // этот метод и будет вызываться из шаблона при доступке к фун frontend_url
    public function url(string $path, array $params = []): string
    {
        // здесь url - это инстанс FrontendUrlGenerator из DI-контейнера.
        return $this->url->generate($path, $params);
    }
}
```
В итоге FrontendUrlGenerator можно будет как передвать (inject-ить) внутрь наших
сервисов (автоматически через DI-контейнер) так и использовать внутри twig-
шаблонизатора написав для него специальное расширение FrontendUrlTwigExtension


## Пишем тест для FrontendUrlTwigExtension

Задача проверить как класс FrontendUrlTwigExtension будет работать именно
внутри самого twig. а для этого тест надо делать более интеграционным, чтобы
он тестировал не просто сам класс, а его сцепку с самим twig-ом.
А значит надо создать Twig\Environment, зарегать в нём своё расширение и вызвать
в шаблоне для рендеринга некого представления, содержащего тестируемую нами ф-ю
frontend_url.

Для этого в тесте нужно будет создать мок-заглушку класса FrontendUrlGenerator
эта заглушка позволит дополнительно проверить правильность генерации адресов:


```php
<?php

declare(strict_types=1);

namespace App\Frontend\Test;

use App\Frontend\FrontendUrlGenerator;
use App\Frontend\FrontendUrlTwigExtension;
use PHPUnit\Framework\TestCase;
use Twig\Environment;
use Twig\Loader\ArrayLoader;

/**
 * @covers FrontendUrlTwigExtension
 */
class FrontendUrlTwigExtensionTest extends TestCase
{
    public function testSuccess(): void
    {
        $frontend = $this->createMock(FrontendUrlGenerator::class);
        // метод generate должен быть вызван ровно один раз.
        $frontend->expects($this->once())->method('generate')->with(
            // именно вот с этими параметрами:
            $this->equalTo('path'),
            $this->equalTo(['a' => '1', 'b' => '2']),
        )
            // и должен этот метод вернуть вот это значение (адрес-ссылка):
        ->willReturn($url = 'http://site/path?a=1&b=2');

        // создаём сам twig-шаблонизатор
        $twig = new Twig\Environment(
            // но используем не файловый загрузчик реальных шаблонов из файлов
            // а загрузчик шаблонов из указанных массивов:
            new ArrayLoader([
                'page.html.twig' => // имя представления
                // значение имени это строка самого шаблона для рендеринга по нему
                "<p>{{ frontend_url('path', {'a':1, 'b': 2, }) }}</p>",
                //     ^^^^^^^^^^^^ вызов проверяемой функции с параметрами
            ]
            )
        );

        // регистрируем наше собтсвенное расширение с тестируемой функцией
        $twig->addExtension(new FrontendUrlTwigExtension($frontend));
        // для работы этому расширению нужен генератор ссылок ^
        // на его место и передаём наш мок-заглушку

        $exp = '<p>http://site/path?a=1&amp;b=2</p>';
        //                              ^^^ экранирование
        // вызываем рендеринг шаблона по имени его представления
        self::assertEquals($exp, $twig->render('page.html.twig'));
        // при рендеринге и должна отработать наша фукнция и выдать ожидаему
        // строку а именно вместо вызова функции должна подставится ссылка
    }
}
```

В итоге
- есть сам FrontendUrlGenerator и юнит-тест для него
- второй же тест именно для FrontendUrlTwigExtension "смотрим его работу в деле"


## Переписываем шаблон(представление) на использование функции frontend_url

```
{% extends 'mail.html.twig' %}

{% block body %}

    <p style="margin: 0 0 10px 0">Confirm your email:</p>

    {% set url = frontend_url('join/email', {'token': token.value} )%}
    <p style="margin: 0">
        <a style="color: #1a8aee; text-decoration: none" href="{{ url }}">
        <span style="color: #1a8aee; text-decoration: underline">{{ url }}</span>
        </a>
    </p>

{% endblock %}
```

Здесь мы прописываем `set`тер создающий новую переменную `url`
в эту переменную `url` будет присвоено значение возвращаемое нашей функцией
`frontend_url`:

            {% set url = frontend_url(...) %}

внутри вызова функции передаётся
- относительный путь `join/email`
- и токен подтверждения `{'token': token.value}`


Теперь можно переписать JoinConfirmationSender убрав из него FrontendUrlGenerator:

```php
<?
class JoinConfirmationSender
{
    private MailerInterface $mailer;
    private Environment $twig;

    public function __construct(
        MailerInterface $mailer,
        Environment $twig, // убрали FrontendUrlGenerator
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function send(Email $email, Token $token): void
    {
        $message = (new MimeEmail())
        ->subject('Join Confirmation')
        ->to($email->getValue())
        ->html(
            $this->twig->render(
                'auth/join/confirm.html.twig',
                // теперь передаётся не весь url а только токен подтверждения
                [ 'token' => $token ]
                // функция frontend_url сама на основе токена собирёт полный путь
                // сам же относительный путь прописывается в самом шаблоне
            )
        );

        $this->mailer->send($message);
    }
}
```

Такой подход позволяет намного упрощать код самих сендеров(JoinConfirmationSender)
дальше нужно переписать его тест:
в тесте нужно будет проверять
- правильное ли представление вызвалось
- наличие переданного токена
- правильность сформировных в email полей (кому,тема)

```php
<?
class JoinConfirmationSenderTest extends TestCase
{
    public function testSuccess(): void
    {
        $to = new Email('user@app.test');
        $token = new Token(Uuid::uuid4()->toString(), new DateTimeImmutable());
        $confirmUrl = 'http://site/join/confirm?token=' . $token->getValue();
        $body = '<a href="' . $confirmUrl . '">' . $confirmUrl . '</a>';

        // заглушка мок твига
        $twig = $this->createMock(Environment::class);
        // ожидаем один вызов метода render
        $twig->expects(self::once())->method('render')->with(
            // причём при вызове метода должно быть передано:
            self::equalTo('auth/join/confirm.html.twig'), // путь представления
            self::equalTo(['token' => $token]),           // токен для ссылки
        )->willReturn($body);       // задаём упрощенное тело ответа этого мока

        $mailer = $this->createMock(MailerInterface::class);
        $mailer->expects(self::once())->method('send')
        ->willReturnCallback(
            static function (MimeEmail $message) use ($to, $body): void {
                self::assertEquals([], $message->getFrom());
                self::assertEquals($to->getValue(), $message->getTo()[0]->getAddress());
                self::assertEquals('Join Confirmation', $message->getSubject());
                self::assertEquals($body, $message->getHtmlBody());
                // text/html
            }
        );

        $sender = new JoinConfirmationSender($mailer, $twig);
        //        уже не генератор ссылов а шаблонизатор ^

        $sender->send($to, $token);
    }
}
```


```sh
docker compose run --rm api-php-cli composer app mailer:check
> php bin/app.php --ansi 'mailer:check'
Sending

In confirm.html.twig line 7:

  Unknown "frontend_url" function.


mailer:check

Script php bin/app.php --ansi handling the app event returned with error code 1
```

Добавляем класс расширения в конфиге в массив extensions:

./api/config/common/twig.php
```php
<?
return [
    # ...
    'config' => [
        'twig' => [
            'debug' => (bool)getenv('APP_DEBUG'),
            'template_dirs' => [
                FilesystemLoader::MAIN_NAMESPACE => __DIR__ . '/../../templates',
            ],
            'cache_dir' => __DIR__ . '/../../var/cache/twig',
            'extensions' => [
                FrontendUrlTwigExtension::class, // <<< класс расширения
            ],
        ],
    ],
];
```

проверяем через Mailer:check и смотрим в MailHog:
```
Confirm your email:

http://localhost:8080/join/email?token=0906ccb1-6b37-4561-82b8-77320a1f6bb4
If it is an error just remove this mail.
```


## Реализуем остальные наши Sender-ы:

(просто копируя и изменяя код из JoinConfirmationSender)

- ./api/src/Auth/Service/NewEmailConfirmTokenSender.php
- ./api/src/Auth/Service/PasswordResetTokenSender.php

у каждого будет своё
- представление(view) (путь к шаблону)
- тема письма

в самих шаблонах меняется только заголовок и блок с сеттером

./api/templates/auth/join/confirm.html.twig
```
    <p style="margin: 0 0 10px 0">Confirm your email:</p>

    {% set url = frontend_url('join/confirm', {'token': token.value} )%}
```

./api/templates/auth/email/confirm.html.twig
```
    <p style="margin: 0 0 10px 0">Confirm your new email:</p>

    {% set url = frontend_url('email/confirm', {'token': token.value} )%}
```

./api/templates/auth/password/confirm.html.twig
```
    <p style="margin: 0 0 10px 0">Reset your password:</p>

    {% set url = frontend_url('password/confirm', {'token': token.value} )%}
```

```php
<?
class NewEmailConfirmTokenSender
{
    private MailerInterface $mailer;
    private Environment $twig;

    public function __construct(MailerInterface $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function send(Email $email, Token $token): void
    {
        $view = 'auth/email/confirm.html.twig'; // свой путь к шаблону

        $message = (new MimeEmail())
        ->subject('New Email Confirmation') // своя тема письма
        ->to($email->getValue())            // всё остальное один в один такое же
        ->html($this->twig->render($view, [ 'token' => $token ]));

        $this->mailer->send($message);
    }
}
```
так же и для PasswordResetTokenSender

