## Оптимизация размера образов api-php-{cli,php} pdo_pgsql

- Суть проблемы:
до установки pdo_pgsql из исходников размер образа api-php-cli 106 мб
после "наивной" установки - 589 мб.
- Причина: в образ запекается большое количество пакетов нужных для компиляции
  расширения pdo_pgsql.
- Решение: компиляция расширения с удалением пакетов нужных для компиляции
  через флаг `apk add --virtual`


Как вышел на решение данной проблемы:

Установака pdo_pgsql из исходников отдельным слоем - коммандой RUN

./api/docker/development/php-cli/Dockerfile
```Dockerfile
FROM php:8.1-cli-alpine

RUN apk update && apk add --no-cache --virtual .build-deps \
    autoconf g++ make linux-headers \
    && pecl install xdebug-3.2.2 \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable xdebug \
    && apk del -f .build-deps

# эта команда создат новый слой в который сильно раздует размер образа(в 5 раз)
RUN apk add --no-cache postgresql-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo_pgsql
# ...
````
Такой докерфайл даст размер образа в 589 мб (до того было 106мб)

Для того чтобы пересобрать образ из докерфайла можно использовать make init
оно положет всю дев-среду и пересобирёт все образы.
Если же нужно только пересобрать один образ api-php-cli то лучше использовать:
```sh
docker compose build --pull api-php-cli
```
Эта команда позволит собрать только один нужный образ api-php-cli
Сборка будет идти по настройкам из файла docker-compose.yml (Для dev-окружения)
при этом там прописано откуда брать докерфайл:
```
  api-php-cli:
    build:
      context: api/docker
      dockerfile: development/php-cli/Dockerfile
```

Смотрим на размер собранного образа:
```sh
docker images
```
```
REPOSITORY           TAG            IMAGE ID       CREATED          SIZE
site-api-php-cli     latest         2bccdb080727   12 minutes ago   589MB
```

## Оптимизация размера образа с pdo_pgsql

получаем список всех установленных пакетов внутри раздутого до 589мб образа

```sh
docker compose run --rm api-php-cli apk list -I > api-php-cli-pkgs-589mb.list
```
`-I|--installed` - показать все устанвленные пакеты
./docs/13-doctrine/logs/api-php-cli-pkgs-589mb.list

Теперь полностью откатим изменения убрав новый слой по установке pdo_pgsql.
Для этого убираем из Dockerfile команду на установку pdo_pgsql
(закомитить вот эти строки):

api/docker/development/php-cli/Dockerfile
```Dockerfile
# RUN ...
RUN apk add --no-cache postgresql-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo_pgsql
# ...
```

Снова пересобираем только нужный нам образ `api-php-cli` и смотрим размер образа
```sh
docker compose build --pull api-php-cli
docker images
```
Смотрим размер (docker images)
```
REPOSITORY           TAG            IMAGE ID       CREATED          SIZE
site-api-php-cli     latest         f85b38bd1dc4   8 seconds ago   106MB
```
589 Mb --> 106 Mb

Получаем список всех установленных в образе пакетов
```sh
docker compose run --rm api-php-cli apk list -I > api-php-cli-pkgs-106mb.list
```
./docs/13-doctrine/logs/api-php-cli-pkgs-106mb.list

```sh
docker compose run --rm api-php-cli apk stats
installed:
  packages: 38          <-- количество установленных пакетов в системе(образе)
  dirs: 119
  files: 367
  bytes: 19914752
  triggers: 2
available:
  names: 45170
  packages: 20071
atoms:
  num: 13676
```

## Получаем список пакетов которые установились в раздутый образ

как получить строки из file_2 которых нет в file_1:
```sh
diff file_1 file_2 | grep '^>' | cut -c 3-
```
This would print the entries in file_2 which are not in file_1.
For the opposite result one just has to replace '>' with '<'.
'cut' removes the first two characters added by 'diff',
that are not part of the original content.
The files don't even need to be sorted.

```sh
diff docs/13-doctrine/logs/api-php-cli-pkgs-106mb.list \
  docs/13-doctrine/logs/api-php-cli-pkgs-589mb.list | \
  grep '^>' | cut -c 3- > docs/13-doctrine/logs/pkgs-build_pdo_pgsgl.list
```
./docs/13-doctrine/logs/pkgs-build_pdo_pgsgl.list
Это и есть список тех пакетов которые нужны только для компиляции,
но не нужны в образе для его корректной работы.

Note: Если не указать версию то на данный момент устанавливается версия
`postgresql15-dev-15.4-r0`
Но при этом у себя в проекте пока что использую образ с postgresql 13.12

смотрим зависимости пакета `postgresql-dev`
```sh
docker compose exec api-php-fpm apk info --depends postgresql-dev
```
postgresql15-dev-15.4-r0 depends on:
clang14
icu-dev
libecpg-dev
libpq-dev
llvm14
lz4-dev
openssl-dev
so:libc.musl-x86_64.so.1
so:libpq.so.5                             <<< ****
zstd-dev

более детальный список зависимостей и зависимостей зависимостей здесь:
./docs/13-doctrine/logs/postgresql15-dev.log


## Первая попытка уменьшить размер итогового образа.

Пробуем собрать из исходников нужное расширение с флагом --virtual
Как это работает:
флаг apk add --virtual при установке пакета postgresql-dev запишет(запомнит)
все установившиеся пакеты в .pdo_pgsql-deps, то есть все те пакеты которые
установятся при установке самого пакета postgresql-dev. И дальше можно будет
удалить все эти пакеты через команду `apk del -f .pdo_pgsql-deps`.

Меняем установку слоя с компиляцией расширения pdo_pgsql на такую

api/docker/development/api-php-cli/Dockerfile:
```Dockerfile
# RUN ...
RUN apk add --no-cache --virtual .pdo_pgsql-deps \
    postgresql-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo_pgsql \
    && apk del -f .pdo_pgsql-deps
#...
```
Но такой Dockerfile выдаёт ошибку

Ошибка будет например при попытке запустить консольный скрипт api/bin/app
здесь он запускается по алиасу `app` в composer.json:
```sh
docker compose run --rm api-php-cli composer app
```
```
PHP Warning:
PHP Startup: Unable to load dynamic library 'pdo_pgsql.so'
(tried:
/usr/local/lib/php/extensions/no-debug-non-zts-20210902/pdo_pgsql.so
(Error loading shared library libpq.so.5: No such file or directory
(needed by /usr/local/lib/php/extensions/no-debug-non-zts-20210902/pdo_pgsql.so)),

/usr/local/lib/php/extensions/no-debug-non-zts-20210902/pdo_pgsql.so.so
(Error loading shared library
/usr/local/lib/php/extensions/no-debug-non-zts-20210902/pdo_pgsql.so.so:
No such file or directory)) in Unknown on line 0
```
Отсюда видим что для работы не хватает фала libpq.so.5

Если поискать в образе на 589мб где установлено всё, то он будет найден
```sh
docker compose run --rm api-php-cli find / -name pdo_pgsql.so
docker compose run --rm api-php-cli find / -name libpq.so
```
/usr/lib/libpq.so

Узнать по полному пути файла к какому пакету принадлежит этот файл так:

```sh
docker compose run --rm api-php-cli apk info --who-owns /usr/lib/libpq.so
```
/usr/lib/libpq.so is owned by libpq-dev-15.4-r0

Более точно
```sh
docker compose run --rm api-php-cli apk info -who-owns /usr/lib/libpq.so.5
/usr/lib/libpq.so.5 is owned by libpq-15.4-r0
```

Смотрим что входит в укзанный нами пакет libpq
```sh
docker compose run --rm api-php-cli apk info --contains libpq
```
```
libpq-15.4-r0 contains:
usr/lib/libpq.so.5
usr/lib/libpq.so.5.15
```
То есть видим что нужный файл входит в пакет `libpq`
но он же идёт и в пакете `so:libpq.so.5`, который прописан как зависимость
для `postgresql-dev`.

Причем `-L|--contains` на so:libpq.so.5 говорит что это пакет libpq-15.4-r0
```sh
docker compose run --rm api-php-cli apk info -L so:libpq.so.5
```
```
libpq-15.4-r0 contains:
usr/lib/libpq.so.5
usr/lib/libpq.so.5.15
```

```sh
docker compose run --rm api-php-cli apk info libpq
libpq-15.4-r0 description:
PostgreSQL client library

libpq-15.4-r0 webpage:
https://www.postgresql.org/

libpq-15.4-r0 installed size:
320 KiB
```


## Выход на исправление ошибки и решение проблемы  libpg (so:libpq.so.5)

Надо чтобы пакет libpq не удалялся после компиляции расширения pdo_pgsql,
а оставался в итоговом образе.

Напомню что пакет libpg входит в зависимость пакета postgresql-dev:
```sh
docker compose exec api-php-fpm apk info --depends postgresql-dev
```
```
postgresql15-dev-15.4-r0 depends on:
clang14 icu-dev libecpg-dev llvm14 lz4-dev openssl-dev zstd-dev
so:libc.musl-x86_64.so.1
libpq-dev
so:libpq.so.5       <<< вот этот пакет и нужно сохранить (он же libpq)
```

а значит можно libpq установить отдельно так, чтобы он не вошел в список
временных пакетов (.pdo_pgsql-deps), который формируется при
`apk add --virtual .pdo_pgsql-deps postgresql-dev`
а для этого просто ставлю его отдельным вызовом `apk add --no-cache libpq`:


```Dockerfile
FROM php:8.1-cli-alpine

RUN apk update && apk add --no-cache --virtual .build-deps \
    autoconf g++ make linux-headers \
    && pecl install xdebug-3.2.2 \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable xdebug \
    && apk del -f .build-deps

RUN apk add --no-cache libpq \
    && apk add --no-cache --virtual .pdo_pgsql-deps \
       postgresql-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo_pgsql \
    && apk del -f .pdo_pgsql-deps
# ....
```



Refs:
https://www.php.net/manual/en/pgsql.installation.php


