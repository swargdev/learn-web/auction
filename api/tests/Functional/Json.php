<?php

declare(strict_types=1);

namespace Test\Functional;

/**
 *
 * 09-12-2023
 * @author Swarg
 */
class Json
{
    /**
     * @param string $data
     * @return array
     */
    public static function decode(string $data): array
    {
        /** @var array */
        return json_decode($data, true, 512, JSON_THROW_ON_ERROR);
    }
}
