## Выносим настройки для playbook-а из site.yml в отдельный файл

```yml
- name: Provision Site
  hosts: all
  remote_user: root
  vars:                                           # указываем значения напрямую
    certbot_email: mail@my-domain.com
    certboot_hosts:
      - demo-auction.my-domain.pro
      - api.demo-auction.my-domain.pro
  tasks:
    - import_tasks: tasks/setup_docker.yml
    - import_tasks: tasks/setup_certbot.yml
    - import_tasks: tasks/generate_certificates.yml
  handlers:
    - name: Start docker on boot
      systemd:
        name: docker
        state: started
        enabled: yes
```

Выносим настройки для certbot-а в отдельный файл
./provisioning/envs/certbot.yml:
```yml
certbot_email: mail@my-domain.com
certboot_hosts:
  - demo-auction.my-domain.pro
  - api.demo-auction.my-domain.pro
```

И подключаем его в свой основной playbook через `vars_files` вместо `vars`:
```yml
- name: Provision Site
  hosts: all
  remote_user: root
  vars_files:
    - envs/certbot.yml                            # импортируем из файла
  tasks:
    - import_tasks: tasks/setup_docker.yml
    - import_tasks: tasks/setup_certbot.yml
    - import_tasks: tasks/generate_certificates.yml
  handlers:
    - name: Start docker on boot
      systemd:
        name: docker
        state: started
        enabled: yes
```

Теперь наш playbook site.yml
при первом запуске на чистом сервере:
- установит докер и настроит его для работы
  затем запустит хэндлер для проверки что он доступен `Start docker on boot`
- установит certbot
- сгенерирует список нужных сертификатов заданных в файле
  ./provisioning/envs/certbot.yml запустив для этого сервер apache в докер
  контейнере и удалив контейнер и его образ после генерации сертификатов.

Таким образом этот playbook подготовит всё что нужно для деплоя сайта на
удалённую машину (VMку)
Деплой при этом будет идти от пользователя root.
Доя более правильного подхода, лучше не деплоить от `root`-а, а создать для этого
отдельного пользователя на удалённой машине. Например под именем `deploy`

