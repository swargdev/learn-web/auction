## Настраиваем сохранение снимков экрана для неуспешных тестов.

При написании е2е тестов может возникнуть необходимость визуально посмотреть
что вообще происходит во время выполнения теста.
Ведь простого вывода сообщиений об ошибках в консоли может быть не достаточно.

Поэтому создадим каталог var в который настроим сохранение снимков экрана
которые будут создаваться автоматически в падающих тестах.

```Makefile
init: docker-down-clear \
  api-clear frontend-clear cucumber-clear \
  docker-pull docker-build docker-up \
	api-init frontend-init cucumber-init

test-e2e: api-fixtures cucumber-clear cucumber-e2e

cucumber-clear:
	docker run --rm -v ${PWD}/cucumber:/app -w /app alpine sh -c 'rm -rf var*'
```

Сохранение снимков экрана добавляем через хуки:


```js
const { Before, After, Status, setDefaultTimeout } = require('@cucumber/cucumber')
const puppeteer = require('puppeteer')

setDefaultTimeout(10 * 1000)

Before({ timeout: 30000 }, async function () {
  this.browser = await puppeteer.launch({
    headless: 'new',
    args: [
      '--disable-dev-shm-usage',
      '--no-sandbox'
    ]
  })
  this.page = await this.browser.newPage()
  await this.page.setViewport({ width: 1280, height: 720 })
})

// testCase - обьект с результатом выполнения конкретного сценария
After(async function (testCase) {
  // если тест сценария упал
  if (testCase.result.status === Status.FAILED) {
    // генерируем имя файла для скриншота заменяя слэша на подчёркивания
    const uri = testCase.pickle.uri
    const name = uri.replace(/^\/app\/features\//, '').replace(/\//g, '_') +
      '-' +
      testCase.pickle.name.toLowerCase().replace(/[^\w]/g, '_')
    await this.page.screenshot({ path: 'var/' + name + '.png', fullPage: true })
  }
  await this.page.close()
  await this.browser.close()
})
```

Проверяем что тесты до сих пор работают как надо, после этого ломаем наш тест
home.js: `expect(text).to.eql('Auc  tion')`
и проверяем генерацию картинки упавшего теста

```
yarn run v1.22.19
$ cucumber-js
...FF

Failures:

1) Scenario: View home page content # features/home.feature:6
   ✔ Before # features/support/hooks.js:6
   ✔ Given I am a guest user # features/support/steps/user.js:3
   ✔ When I open "/" page # features/support/steps/page.js:3
   ✖ Then I see welcome block # features/step_definition/home.js:4
       AssertionError
           + expected - actual

           -Auction
           +Auc tion

           at CustomWorld.<anonymous> (/app/features/step_definition/home.js:11:19)
           at process.processTicksAndRejections (node:internal/process/task_queues:95:5)
   ✔ After # features/support/hooks.js:18

1 scenario (1 failed)
3 steps (1 failed, 2 passed)
0m04.974s (executing steps: 0m04.952s)
```

Картинка сгенерируется по такому пути:
cucumber/var/features_home.feature-view_home_page_content.png


## Создание отчётов для автоматического анализа хода выполнения тестов

Для генерации отчёта в json формате достаточно добавить в команду запуска
cucumber-js флаг в котором указать формат отчёта(json) и путь куда сохранять
(var/report.json)

./cucumber/package.json
```json
  "scripts": {
    "e2e": "cucumber-js --format json:var/report.json",
     ...
   }
```


```sh
docker compose run --rm cucumber-node-cli yarn e2e
yarn run v1.22.19
$ cucumber-js --format json:var/report.json
.....

1 scenario (1 passed)
3 steps (3 passed)
0m08.318s (executing steps: 0m08.301s)
Done in 12.34s.
```

Пример такого отчёта:
cucumber/var/report.json
```json
[
  {
    "description": "  In order to check home page content\n  As a guest user\n  I want to be able to view home page",
    "elements": [
      {
        "description": "",
        "id": "view-home-page;view-home-page-content",
        "keyword": "Scenario",
        "line": 6,
        "name": "View home page content",
        "steps": [
          {
            "keyword": "Before",
            "hidden": true,
            "result": { "status": "passed", "duration": 5138968619 }
          },
          {
            "arguments": [],
            "keyword": "Given ",
            "line": 7,
            "name": "I am a guest user",
            "match": {
              "location": "features/support/steps/user.js:3"
            },
            "result": { "status": "passed", "duration": 168736 }
          },
          {
            "arguments": [],
            "keyword": "When ",
            "line": 8,
            "name": "I open \"/\" page",
            "match": {
              "location": "features/support/steps/page.js:3"
            },
            "result": { "status": "passed", "duration": 1251136300 }
          },
          {
            "arguments": [],
            "keyword": "Then ",
            "line": 9,
            "name": "I see welcome block",
            "match": {
              "location": "features/step_definition/home.js:4"
            },
            "result": { "status": "passed", "duration": 54688934 }
          },
          {
            "keyword": "After",
            "hidden": true,
            "result": { "status": "passed", "duration": 1856509083 }
          }
        ],
        "tags": [],
        "type": "scenario"
      }
    ],
    "id": "view-home-page",
    "line": 1,
    "keyword": "Feature",
    "name": "View home page",
    "tags": [],
    "uri": "features/home.feature"
  }
]
```

По умолчанию в этом отчёте описано то как прошло выполнение каждого шага в
каждом сценарии. Но вот для падающих тестов в этот отчёт не добавляется нужная
нам инфа - например скриншоты и доп информация

## Дополняем вывод отчёта Attach внутри cucumber

Мы скриншоты сохраняли как отдельные файлы картинок.
Можно используя метод attach добавлять скриншоты в отчёт вместо того, чтобы
сохранять их напрямую в отдельный файл. Это делается в том же хуке Affter:

```js
After(async function (testCase) {
  if (testCase.result.status === Status.FAILED) {
    const img = await this.page.screenshot({encoding: 'base64', fullPage: true})
    this.attach(img, 'image/png')
  }
  await this.page.close()
  await this.browser.close()
})
```
Такой код скриншот закодирует в формат base64 и добавит его в виде строки прямо
внутрь отчёта

Но для того чтобы метод attach заработал как надо нужно его принять в момент
создания world-а:

```js
const { setWorldConstructor } = require('@cucumber/cucumber')

function CustomWorld({ attach }) {
  this.attach = attach
  this.browser = null
  this.page = null
}

setWorldConstructor(CustomWorld)
```

Причем теперь метод this.attach будет доступен из любого места кода наших тестов
в том числе и внутри реализации тестов шагов проверяемых сценариев.


