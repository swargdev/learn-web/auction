<?php

declare(strict_types=1);

namespace App\Frontend;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 *
 * 07-12-2023
 * @author Swarg
 */
class FrontendUrlTwigExtension extends AbstractExtension
{
    private FrontendUrlGenerator $url;

    public function __construct(FrontendUrlGenerator $url)
    {
        $this->url = $url;
    }

    /**
     * @return array<int,TwigFunction>
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('frontend_url', [$this, 'url']),
        ];
    }

    /**
     * @param array<string,string> $params
     */
    public function url(string $path, array $params = []): string
    {
        return $this->url->generate($path, $params);
    }
}
