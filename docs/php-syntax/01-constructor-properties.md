## Constructor Properties or Constructor Property Promotion
https://php.watch/versions/8.0/constructor-property-promotion

Нововедение появившееся с версии php 8.0 (версия вышедшая на 25 летие языка)
суть в том, чтобы позволить задавать поля класса только лишь указывая их в
сигнатуре конструктора, при этом не "дублируя", как обычно эти же поля в теле
самого класса.
Для того, чтобы укоротить размер кода, поля класса описываеются один раз только
в конструкторе, после чего "само"(автоматически) идёт присвоение заданных
значений переданых в конструктор в сами поля класса,
при этом это чисто синтаксический сахар  так как под капотом, рефлексией всё
это доступно и видно, т.е. по сути это всё теже обычные поля класса.

`Constructor Property Promotion` is a new syntax in PHP 8 that allows class
property declaration and constructor assignment right from the constructor.

A typical class that declares a property,
and then assigns a value to it in the class constructor is quite verbose.

```php
<?
class User {
    private string $name;

    public function __construct(string $name) {
        $this->name = $name;
    }
}
```

With the Constructor Property Promotion syntax,
the class declaration above can be minimized to avoid boilerplate:

```php
<?
 class User {
   # private string $name;
   # public function __construct(string $name) {

   public function __construct(private string $name) {
   #    $this->name = $name
   }
}
```

This results in a much simplified and minimal syntax that is
functionally identical to the verbose class declaration:

```php
<?
class User {
    public function __construct(private string $name) {}
}
```



A new syntax to declare class properties right from the class constructor
(__construct magic method).

In the constructor, PHP 8.0 supports declaring the visibility
(public, private, or protected) and type.
Those properties will be registered as class properties with
same visibility and type they are declared in the constructor.

This backwards-incompatible feature can help
reduce boilerplate code when declaring value-object classes.


Еще пример такого способа обьявления полей класса:
```php
<?
class Point {
	public function __construct(
		public float $x = 0.0, // и обьявить поле класса и присвоить в $this->x
		public float $y = 0.0, // и так же задать дефолтное значение здесь 0.0
	) {
	}
}

# тоже самое до php 8.0:

class Point {
	public float $x;
	public float $y;

	public function __construct(
		float $x = 0.0,
		float $y = 0.0,
	) {
		$this->x = $x;
		$this->y = $y;
	}
}
```
