## Docker login Прописываем данные для доступа в registry

Для того чтобы новый пользователь на удалённой машине имел доступ к registry
и чтобы мог скачивать от туда образы с нашим сайтом нужно прописать данные для
авторизации.

```yml
---
- name: Log into private registry
  hosts: all
  remote_user: root
  become: yes             # переключиться на другого пользователя(по умолч root)
  become_user: deploy     # переключаться на пользователя deploy
  vars_prompt:            # Запросить в интерактивном режиме ввести данные
    - name: registry
      prompt: "Registry"
      private: no

    - name: username
      prompt: "Username"
      private: no

    - name: password
      prompt: "Password"
  tasks:
    - name: Log into private registry
      shell: "docker login -u '{{ username }}' -p '{{ password }}' {{ registry }}"
```

при запуске этого playbook-а ansible спросит у запустившего пользователя
ввести поочереди три значения Registry Username Password и подставит их затем
в shell команду, которая и запустится на удалённой машине.

обрати внимание `remote_user: root` - означает войти в систему от лица root
нам же для того чтобы хэшированный пароль записался не для root-a, а для нашего
юзера `deploy` в его каталог нужно

  become: yes
  become_user: deploy


`become: yes` - с пустым полем `become_user` - означает после входа на удалённый
сервер через sudo переключится на root-а


## Использование Ansible-плагина Docker для этих же целей

ак же вместо shell-ловской команды:
```yml
  shell: "docker login -u '{{ username }}' -p '{{ password }}' {{ registry }}"
```
Можно использовать такой подход:
```yml
  tasks:
    - name: Log into private registry
      docker_login:                  # для этого нужен доп плагин(модуль)
         username: ...
         registry: ...
```

Но так как мы особо с докером из ансибл работать не будем просито используем
шеловскую команду.

