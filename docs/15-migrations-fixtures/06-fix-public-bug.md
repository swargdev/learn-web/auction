## CREATE SCHEMA public

При работе с PostgreSQL через Doctrine-Migrations в методе `down`, генерируемых
миграций всегда добавляется строка:

        $this->addSql('CREATE SCHEMA public');

И это происходит из-за того в что в наших сущностях не прописано какую схему
в PostgreSQL использовать.
Вообще в PostgreSQL таблицы можно создавать в разных "схемах".
т.е. можно сделать несколько схем. назвать их по разному и в них уже создавать
таблицы.
доктрин видит что у сущности User прописана просто таблица и вообще не
прописана схема, а в самой бд уже есть схема под именем `public`

```php
<?
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'auth_users')]  // название таблицы есть, название схемы - нет
class User {
```

Из-за этого противоречия и будет постоянно генерится код по созданию схемы

        CREATE SCHEMA public

неудобство в том, что если будем вызвать метод down (откатывать миграцию)
то будет возникать ошибка из-за того что такая схема уже есть.
А значит после каждой генерации миграции надо удалять эту строку руками.

еще момент если попытаться это сделать то можно будет обнаружить что нет прав.
из-за того что генерация идёт изнутри докер-контейнера и т.к. там юзер не
прописан запуск идёт от имени root. И если работа с кодом идёт как и положено
не из под root-а а от имени обычного пользователя системы то и доступ нам как бы
закрыт. т.к. файл будет сгенерин изнутри контейнера с владельцем root.

Исправляем права для всего содержимого исходников
```sh
sudo chown $USER:$USER api/src -R
```
и удаляем злосчастную строку `CREATE SCHEMA public` из метода `down`


## Исправляем баг с созданием схемы public

есть issue на github-е которое висит уже очень давно и в нём ссылка на решение:
FixPostgreSQLDefaultSchemaListener.php
https://gist.github.com/vudaltsov/ec01012d3fe27c9eed59aa7fd9089cf7

как это работает:
- пишется слушатель(Listener) для Doctrine-Migrations
- этот слушатель берёт сгенеренную доктриной схему обходит все массивы
и добавляет везде где не прописано схему public

```php
<?
    foreach ($schemaManager->getExistingSchemaSearchPaths() as $namespace) {
        if (!$args->getSchema()->hasNamespace($namespace)) {
            $args->getSchema()->createNamespace($namespace);
        }
    }
```

навешиваем этот слушатель на наш EntityManager
для того чтобы в момент выполнения события postGenerateSchema вызывался наш код
в него приходил обьект который и будем исправлять

Для того чтобы руками в конфигах не указывать на какие собятия надо подписываться
реализуем это исправление через оформление не в виде слушателя а в виде
подписчика на событие (subscriber-а).

./api/src/Data/Doctrine/FixDefaultSchemaSubscriber.php


```php
<?php

declare(strict_types=1);

namespace App\Data\Doctrine;

use Doctrine\Common\EventSubscriber;               // интерфейс для реализации
use Doctrine\DBAL\Schema\PostgreSQLSchemaManager;
use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;
use Doctrine\ORM\Tools\ToolEvents;

// реализуем интерфейс "подписчика на событие"(субскрайбер)
final class FixDefaultSchemaSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        //описываем на какие события мы подписываемся
        return [
            // название события                какой метод из этого класса звать
            ToolEvents::postGenerateSchema => 'postGenerateSchema',
            // этим мы задаём вызов указанного метода именно в момент события
            // ToolEvents::postGenerateSchema
        ];
    }

    // тот самый код исправляющий проблему генерации публичной схемы
    public function postGenerateSchema(GenerateSchemaEventArgs $args): void
    {
        $schemaManager = $args
            ->getEntityManager()
            ->getConnection()
            ->createSchemaManager();

        /**
         * @psalm-suppress RedundantCondition
         */
        if (!$schemaManager instanceof PostgreSQLSchemaManager) {
            return;
        }

        /**
         * @psalm-suppress InternalMethod
         */
        foreach ($schemaManager->getExistingSchemaSearchPaths() as $namespace) {
            if (!$args->getSchema()->hasNamespace($namespace)) {
                $args->getSchema()->createNamespace($namespace);
            }
        }
    }
}
```

прописываем наш сабскрайбер в EntityManager:

./api/config/common/doctrine.php
```
    'config' => [
        'doctrine' => [
            'dev_mode' => false,
            'cache_dir' =>  ..
            'proxy_dir' =>  ..
            'connection' => [...],

            'subscribers' => [],                <===== добавляем новый массив

            'metadata_dirs' => [...],
            'types' => [... ]
        ],
    ],
```
Слущатель добавляем именно для DEV-окружения в проде массив остаётся пустой.
./api/config/dev/doctrine.php
```
return [
    'config' => [
        'doctrine' => [
            'dev_mode' => true,
            'cache_dir' => null,
            'proxy_dir' => ...
            'subscribers' => [
                App\Data\Doctrine\FixDefaultSchemaSubscriber::class,
            ],
        ],
    ],
```
Это доабвляется только для dev-окружения т.к. только из дев-окружения будут
генерятся новые миграции. Да и в проде даже этой команды не будет migrations:diff

Дальше уже достаём прописанные в конфиге подписчики в момент создания инстанса
EntityManager и добавляем своего подписчика на событие.

./api/config/common/doctrine.php
```php
<?
use ...;
return [
    EntityManagerInterface::class =>
    static function (ConstainerInterface $container): EntityManagerInterface {
        $settings = $container->get('config')['doctrine'];
        $config = ORMSetup::createAttributeMetadataConfiguration(...);
        // ...

        // создаём обьект менеджер событий
        $eventManager = new Doctrine\Common\EventManager();

        // добавляем в него инстансы обьектов созданных через DI-контейнер
        foreach ($settings['subsribers'] as $name) {
            /** @var EventSubsriber $subsciber */
            $subscriber = $container->get($name);
            $eventManager->addEventSubscriber($subscriber);
        }
        return new EntityManager($connection, $config, $eventManager);
        //                                             ^^^^^^^^^^^^^
    }
]
```

готово.

