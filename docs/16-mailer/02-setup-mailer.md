- Подключаем Symfony/Mailer к нашему проекту
- Создание фабрики по сборке Symfony/Mailer
- MAILER_FROM_EMAIL - Автоматическая подстановка отправителя для всех писем


## Подключаем Symfony/Mailer к нашему проекту

делаем на основе написанной в 01-intro.md cli-комманды CheckMailerCommand

настройки транспорта будем пробрасывать через перменные окружения
для dev-окружения задаём их просто через docker-compose.yml:
так же как задавали переменные для передачи данных для подключения к БД:


```yml
  api-php-fpm:
    build:
      context: api/docker
      dockerfile: development/php-fpm/Dockerfile
    environment:
      APP_ENV: dev
      APP_DEBUG: 1
      PHP_IDE_CONFIG: serverName=API
      DB_HOST: api-postgres
      DB_USER: app
      DB_PASSWORD: secret
      DB_NAME: app
      MAILER_HOST: mailer                     # новые переменные окружения для
      MAILER_PORT: 1025                       # настройки транспорта мейлера
      MAILER_USER: app                        # по сути это просто заглушки
      MAILER_PASSWORD: secret                 # для MailHog.
      MAILER_ENCRYPTION: tcp                  # не использовать tls шифрование
    volumes:
      - ./api:/app

  api-php-cli:
    build:
      context: api/docker
      dockerfile: development/php-cli/Dockerfile
    environment:
      APP_ENV: dev
      APP_DEBUG: 1
      DB_HOST: api-postgres
      DB_USER: app
      DB_PASSWORD: secret
      DB_NAME: app
      MAILER_HOST: mailer                          # <<<
      MAILER_PORT: 1025
      MAILER_USER: app
      MAILER_PASSWORD: secret
      MAILER_ENCRYPTION: tcp                       # <<<
    volumes:
      - ./api:/app
```

`MAILER_HOST: mailer` - это заглушка для dev-окружения, по сути это внутренее
dns-имя ведущее на контейнер с MailHog почтовиком(для тестов)


Для prod-а прописываем так, чтобы передавались реальные данные для "мейлера" -
почтового сервера через который будем слать письма.
(То есть Мейлер - это некий почтовый сервис, например яндекс или гул почты)
Эти данные будут задаваться через перменные окружения который будет подхватывать
сам docker compose при старте контейнеров (например через .env-файл)

./docker-compose-production.yml
```yml
  #...
  api-php-fpm:
    image: ${REGISTRY}/auction-api-php-fpm:${IMAGE_TAG}
    restart: always
    environment:
      APP_ENV: prod
      APP_DEBUG: 0
      DB_HOST: api-postgres
      DB_USER: app
      DB_PASSWORD: ${API_DB_PASSWORD}
      DB_NAME: app
      MAILER_HOST: ${API_MAILER_HOST}
      MAILER_PORT: ${API_MAILER_PORT}
      MAILER_USER: ${API_MAILER_USER}
      MAILER_PASSWORD: ${API_MAILER_PASSWORD}
      MAILER_ENCRYPTION: tls

  api-php-cli:
    image: ${REGISTRY}/auction-api-php-cli:${IMAGE_TAG}
    restart: always
    environment:
      APP_ENV: prod
      APP_DEBUG: 0
      DB_HOST: api-postgres
      DB_USER: app
      DB_PASSWORD: ${API_DB_PASSWORD}
      DB_NAME: app
      MAILER_HOST: ${API_MAILER_HOST}
      MAILER_PORT: ${API_MAILER_PORT}
      MAILER_USER: ${API_MAILER_USER}
      MAILER_PASSWORD: ${API_MAILER_PASSWORD}
      MAILER_ENCRYPTION: tls

  api-postgres:
      # ...
```




## Создание фабрики по сборке Symfony/Mailer

Теперь прописываем создание Symfony/Mailer для DI-контейнера:
делаем это так же как и со всеми другими классами-интерфейсами
описывая как зависимость которую должен уметь "доставать" из себя DI-контейнер.

Для этого создаём отдельный конфигурационный файл по аналогии с другими:

- ./api/config/common/mailer.php

Принцип прописывания сборки инстанса для DI-контейнера такой же как и везде

- сначала прописываем передачу значений переменных окружения в конфиг-массив:

```
    'config' => [
        'mailer' => [
            'host' => getenv('MAILER_HOST'),
            'port' => (int)getenv('MAILER_PORT'),
            'user' => getenv('MAILER_USERNAME'),
            'password' => getenv('MAILER_PASSWORD'),
            'encryption' => (getenv('MAILER_ENCRYPTION'),
        ],
    ],
```


Затем используем этот массив для заполнения данных при инстанцировании обьектов:
```php
<?

    // просто достаём массив со значениями взятых и переменных окружения
    $config = $container->get('config')['mailer'];

    // и на основе этих значений, лежащих в массиве, создаём нужные обьекты

    // транспорт для общения с мейлеров(почтовым сервером)
    $transport = (new EsmtpTransport(
        $config['host'],
        $config['port'],
        $config['encryption'] === 'tls', // здесь нужно передавать boolean
    ))
        ->setUsername($config['user'])
        ->setPassword($config['password']);

    return new Mailer($transport);
```

Всё вместе будет выглядить так:

./api/config/common/mailer.php
```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Mailer\EventListener\EnvelopeListener;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Component\Mime\Address;


return [
    MailerInterface::class =>
    static function (ContainerInterface $container): MailerInterface {
        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{
         *     host:string,
         *     port:int,
         *     user:string,
         *     password:string,
         *     encryption:string,
         * } $config
         */
        $config = $container->get('config')['mailer'];

        $transport = (new EsmtpTransport(
            $config['host'],
            $config['port'],
            $config['encryption'] === 'tls',
        ))
            ->setUsername($config['user'])
            ->setPassword($config['password']);

        return new Mailer($transport);
    },

    'config' => [
        'mailer' => [
            'host' => getenv('MAILER_HOST'),
            'port' => (int)getenv('MAILER_PORT'),
            'user' => getenv('MAILER_USERNAME'),
            'password' => getenv('MAILER_PASSWORD'),
            'encryption' => getenv('MAILER_ENCRYPTION'),
        ],
    ],
];
```
Понятия:
в данном конфигурационном файле ./api/config/common/mailer.php мы:
- прописали конкретные конфигурационные параметры и откуда их брать
- задали фабрику для создания конкретного обьекта Mailer.
  этот обьект будет выдавать DI-контейнер на запрос интерфейса `MailerInterface`
  (собирая при этом нужный обьект по заданным конфигурационным параметрам)


`MAILER_ENCRYPTION: tcp` - В локальной разработке отключаем шифрование
`MAILER_ENCRYPTION: tls` - Используем настоящее шифрование в продакшене


Инстанцирование обьекта `Symfony\Mailer` через DI-контейнер уже будет работать.
Теперь обьект мейлера можно напрямую использовать в своём коде в любом сервисе,
которые у нас распологаются в `./api/src/Auth/Service/`
например для  `JoinConfirmationSender`.


## MAILER_FROM_EMAIL - Автоматическая подстановка отправителя для всех писем

Пересмотрев класс MailerCheckCommand можно подметить что везде нужно будет
передавать адрес отправителя указывая его через метод from().

```php
<?
        $email = (new Email())
            ->from('mail@app.test') // << от имени кого отправляем письмо
            ->to('user@app.test')
            ->subject('Join Confirmation')
            ->text('Confirm');
```

И лучше не хардкодить значение адреса отправителя прямо в коде,
а передавать его тоже через конфиг и переменные окружения.

Для этого и используем еще один параметр:

      MAILER_FROM_EMAIL: ${API_MAILER_FROM_EMAIL}    - для prod

      MAILER_FROM_EMAIL: mail@app.test               - для локальной разработки

Для dev фиксированное значение, для prod - передаваемое из конфига.

Добавляем в конфиг подхват значения из перменной окружения для `from`:
./api/config/common/mailer.php
```
    'config' => [
        'mailer' => [
            'host' => getenv('MAILER_HOST'),
            'port' => (int)getenv('MAILER_PORT'),
            'user' => getenv('MAILER_USER'),
            'password' => getenv('MAILER_PASSWORD'),
            'encryption' => getenv('MAILER_ENCRYPTION'),

            'from' => [
                'email' => getenv('MAILER_FROM_EMAIL'),
                'name' => 'Auction',
            ],
        ],
    ],
```

Далее прописываем для инстанцирования Mailer автоматическую подстановку значения
отправителя используя для этого стандартный интерфейс диспетчера событий:

Принцип здесь простой.
- создаём подписку на событие до отправки письма (слушателя Listener события),
  чтобы вызывался наш код и прошла подстановка адреса отправителя.
- сам транспорт EsmtpTransport до самой отправки письма вызовет метод handle
  передав в него [MessageEvent](./api/vendor/symony/mailer/Event/MessageEvent.php)
- внутри MessageEvent будет передано само письмо "обёрнутое в конверт" `Envelope`

Но свой класс слушателя события писать уже не нужно т.к. есть готовый:
[EnvelopeListener](./api/vendor/symfony/mailer/EventListener/EnvelopeListener.php)
именно он и будет подставлять адрес отправителя(Sender) при срабатывании события.

```php
<?
class EnvelopeListener implements EventSubscriberInterface {
    public function __construct(
        Address|string $sender = null,  // отправитель кто шлёт
        array $recipients = null        // получатели
    ) { }
```

```php
<?
use Symfony\Component\EventDispatcher\EventDispatcher;
        // ...

        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{
         *     host:string,
         *     port:int,
         *     user:string,
         *     password:string,
         *     encryption:string,
         *     from:array{email:string, name:string}   // добавляем новое
         * } $config
         */
        $config = $container->get('config')['mailer'];
        $config = $container->get('config')['mailer'];

        $dispatcher = new EventDispatcher();

        $dispatcher->addSubscriber(new EnvelopeListener(new Address(
            $config['from']['email'],
            $config['from']['name']
        )));

        $transport = (new EsmtpTransport(
            $config['host'],
            $config['port'],
            $config['encryption'] === 'tls',
            $dispatcher                          // передаём свой диспетчер
        ))
            ->setUsername($config['user'])
            ->setPassword($config['password']);

```

То есть строкой

    $dispatcher->addSubscriber(new EnvelopeListener(new Address(email, name)));

мы создаём слушателя события которое будет вызываться перед отправкой каждого
письма. Слушатель уже есть готовый специально для этого EnvelopeListener,
который будет сам подставлять переданный в него адрес отправителя и получателей
нам нужно подставлять только отправителя поэтому указываем адрес (и имя адреса)
только отправителя, получатели null - и листенер их трогать изменять не будет.

