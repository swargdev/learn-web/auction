## Убираем дублирование конфиг файлов для nginx php-fpm

Выносим повторяющиейся код в одно место.
Тем самым исключаем случаи рассинхронизации конфигов для дева и прода.

Для этого делаем каталог `<module>/docker/common` и туда переносим
переиспользуемый код и для `docker/development` и для `docker/production`

Причем важная делать с путями которые будем прописывать в директиве `COPY`
т.к. для dev образы собираем через команду `make up`(Makefile) где идёт указание
контекста из которого образы собирать в файле `docker-compose.yml`(он сугубо для dev)
и т.к. нам не надо копировать код внутрь dev-контейнеров т.к. мы его напрямую
монтируем через volume-ы, то там указываем пути:
`api/docker` `frontend/docker` `gateway/docker`
это еще делается и для ускорения сборки образов, чтобы докер-композ не копировал
всё что есть в каталогах `api` `frontend` `gateway` для сборки во временный каталог.



Далее для dev-сборки указываем в docker-compose.yml базовый путь `api/docker`
а не `api/docker/development/nginx` поэтому и правим dev-Dockerfile с таких путей:

    COPY ./conf.d /etc/nginx/conf.d
На вот такие:

```Dockerfile
FROM ..
COPY ./common/nginx/conf.d /etc/nginx/conf.d
WORKDIR /app
```
(это в api/docker/nginx/Dockerfile)


в docker-compose.yml были такие базовые пути для сборки образа (build.contenxt)
```yml
  api:
    build:
      context: api/docker/development/nginx
    volumes:
      - ./api:/app
    depends_on:
      - api-php-fpm
```

меняем контекст на новый путь выше по дереву каталогов - `api/docker/`
Это для того, чтобы был доступ к новому каталогу `api/docker/common`
и для того, чтобы докер нашел нужный `Dockerfile` ипользуем директиву
`dockerfile: путь`:


docker-compose.yml
```yml
  api:
    build:
      context: api/docker
      dockerfile: development/nginx/Dockerfile
    volumes:
      - ./api:/app
    depends_on:
      - api-php-fpm
```

Для продакшена собираться образу будут через Makefile прописанной командой:
```sh
	docker --log-level=debug build --pull \
  --file=api/docker/production/nginx/Dockerfile \
  --tag=${REGISTRY}/auction-api:${IMAGE_TAG} api
```
И здесь последнее слово `api` - это имя подкаталога от корня проекта
который будет использоватся при сборке докер-образа по Dockerfile указанному
в значнии опции --file
поэтому в COPY у нас здесь не ./common/nginx а ./docker/common
т.е. базовым каталогом при сборке этого образа для прода будет api/ а не
api/docker как при сборке образов для dev-а


```Dockerfile
FROM nginx:1.18-alpine
COPY ./docker/common/nginx/conf.d /etc/nginx/conf.d
WORKDIR /app
COPY ./public ./public
```

для gateway мы прописываем базовый путь gateway/docker т.к. там никакой код
копировать для прода внутрь контейнера не надо.
Поэтому и для dev и для prod Dockerfile-ов при сборке образа базовым катлогом
будет `gateway/docker`:
./gateway/production/nginx/Dockerfile:
```Dockerfile
FROM nginx:1.18-alpine
COPY ./common/nginx/snippets /etc/nginx/snippets
COPY ./production/nginx/conf.d /etc/nginx/conf.d
WORKDIR /app
```

Еще раз для prod команда сборки образа прописана в MakeFile:

```sh
	docker --log-level=debug build --pull \
  --file=gateway/docker/production/nginx/Dockerfile \
  --tag=${REGISTRY}/auction-gateway:${IMAGE_TAG} \
  gateway/docker
```

а для сборки дев-образа команду указываем в docker-compose.yml
```yml
  gateway:
    build:
      context: gateway/docker
      dockerfile: development/nginx/Dockerfile
```
Ну и другие контейнеры в этом докер-компоуз-ям тоже собираются с базовым каталогом
api/docker frontend/docker поэтому и их докер файлы похожи на gateway это может
немного сбить с толку что считать базовым каталогом. Поэтому тут важна ясность
что откуда берёться и зачем. И чем отличается сборка образов для дева и для прода.

