<?php

declare(strict_types=1);

namespace App\Auth\Command\ChangePassword;

/**
 * 12-10-2023
 * @author Swarg
 */
class Command
{
    public string $id = '';
    public string $current = '';
    public string $new = '';
}
