## Реализуем обьекты через которые будем взаимодействовать с БД.

На данный момент наш `UserRepository` был просто интерфейсом-заглушкой.
Задумывался он как некий репозиторий, через который мы должны искать и доставать
из БД обьекты User-ов по разным значениям(полям таблиц)

Еще раз о том, как работать с EntityManager в своём коде

```php
<?
# найти и достать из БД Юзера по его первичному ключу (идишнику UUID)
$user = $em->find(User::class, $id);

// некое действие над этим юзером
$user->confirmJoin($token->getValue(), new DateTimeImmutable());

$em->flush(); // "сливаем" сохраняем обновлённое состояние юзера в БД
```

Для добавления нового User-а - нужно использовать метод `persist`

```php
<?
$user = User::requestJoinByEmail(
    $id = Id::generate(),
    $date = new DateTimeImmutable(),
    $email = new Email('mail@example.com'),
    $hash = 'hash',
    $token = new Token(Uuid::uuid4()->toString(), $date)
);

$em->persist($user); // добавляем нового юзера на сохранение в БД
$em->flush();        // сохранение в бд будет здесь
```

Удаление пользователя из БД
```php
<?
$repo = $em->getRepository(User:class);
$user = $repo->find($id);

$em->remove($user); // удаление Юзера из БД
$em->flush();
```

Убеждаемся что внутри сбея EntityManager работает через UnitOfWork:

[EntityManager](./api/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php)
```php
<? class EntityManager implements EntityManagerInterface {

    public function flush($entity = null)
    {
        if ($entity !== null) {
            Deprecation::trigger(/*...*/);
        }

        $this->errorIfClosed();

        $this->unitOfWork->commit($entity); // все изменения одной транзакцией
    }
```

`unitOfWork->commit()` - отпарвляет все изменения в обьектах наших сущностей в
бд одной транзакцией.
unitOfWork
- накапливает всю работу которую нужно делать,
- по вызову метода `commit` выполняет всю работу "одним махом"
  т.е. создаёт один большой SQL запрос, прописывая в нём одну транзакцию
  в которой описано всё что нужно сделать в бд для изменнеия состояния всех
  наших изменённых или новых добавленных обьектов.

В нашем коде нужно будет получать обьекты User не только по их первичным
ключам ($id) прямо через EntityManager, но и по другим колонкам.
Например по email или токенам и другим аттрибутам.

А так как мы уже осознали, что EntityManager внутри работает с бд через всякие
посредники типа UnitOfWork и другие. То логично предположить что и для поиска
есть специальный обьект. И Да в Доктрин для поиска сущностей в бд специально
существует такая вещь как Репозиторий (ObjectRepository|EntityRepository)
Назначение репозитория - предаставить возможность искать обьекты в бд по разным
критериям и даже сложным SQL запросам.

Для работы с такого рода репозиториями нужно сначала получить сам репозиторий
для конкретной нашей сущности:

        $repo = $em->getRepository(User::class);
Причём `$repo` - это инстанс встроенного, служебного доктриновского репозитория,
именно для наших сущностей класса User.

дальше, уже по этому репозиторию можно вести разного рода поиск например:

        $user = $repo->findOneBy(['email' => $email);

То есть место доставание сущностей из EntityManager, например вот так

        $user = $em->find(User::class, $id);

мы будем работать со встроенным доктриновским Репозиторием, и искать нужные нам
обьекты через него.


Исходник
[EntityManager](./api/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php)
```php
<? class EntityManager implements EntityManagerInterface {
    /**
     * Gets the repository for an entity class.
     *
     * @param string $entityName The name of the entity.
     * @psalm-param class-string<T> $entityName
     *
     * @return ObjectRepository|EntityRepository The repository class.
     * @psalm-return EntityRepository<T>
     *
     * @template T of object
     */
    public function getRepository($entityName)
    {
        if (strpos($entityName, ':') !== false) {
            if (class_exists(PersistentObject::class)) {
                Deprecation::trigger(
                    // ...
                );
            } else {
                throw NotSupported::createForPersistence3(sprintf(
                    // ...
                ));
            }
        }

        $repository = $this->repositoryFactory->getRepository($this, $entityName);
        if (! $repository instanceof EntityRepository) {
            //...
        }

        return $repository;
    }
```

Таким образом наш `UserRepository` должен реализовывать все свои методы именно
через работу с вот этим вот Служебным внутренним Доктриновским репозиторием,
получаемым через EntityManager.


## Реализуем свой UserRepository прерватив из интерфейса в полноценный класс

[UserRepository]./api/src/Auth/Entity/User/UserRepository.php
```php
<?
use Doctrine\ORM\EntityManagerInterface;

class UserRepository
{
    private EntityManagerInterface $em;

    // сам EntityManager будет подставлен сюда из нашего di-контейнера
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    // добавление нового Юзера для сохранения в бд
    public function add(User $user): void
    {
        $this->em->persist($user);
    }

    // Для реализации поиска Юзера по токену подтверждения регистрации:
    public function findByConfirmToken(string $token): ?User
    {
        return $this->em->getRepository(User::class)->findOneBy([
            // здесь мы передаём имя самого поля в классе User
            // и т.к. само поле хранящее Token в User прописано как Embedded
            // то можно именовать поля точно так же как они именуются в User
            // с той же вложенностью которая есть в самих наших классах
            'joinConfirmToken.value' => $token
            /* то есть этим мы говорим Доктрин - ищем по значению поля value
            из обьекта Token, которое хранится в поле класса User с именем
            joinConfirmToken. При этом доктрина сама разрулит мэппинг имён
            и вместо имён понятных в нашем коде подставит конкретное рельное имя
            колонки в таблице базыданных - т.е. в join_confirm_token_value
            */
        ]);
    }
}
```

Доктрина позволяет работать не только через прямую генерацию SQL запросов к бд,
а позволяет генерить запросы в DQL - Doctrine Query Language. При этом
конвертируя эти DQL запросы в SQL подставляя вместо удобным нам имён конкретные
имена полей и таблиц и отправля затем их в БД. Таким образом DQL избавляет
программиста от нужны постоянно думать о том как там всё на уровне БД и таблиц,
делая процесс разработки более гибким и простым.
То есть даже при поиске сущности(юзрера) в бд просто используем имена своих же
полей в классах искомых сущностей, не заморачиваясь в какие имена колонок оно
там преобразуется.

Дальше для удобства вынесем обьект репозитория для более удобного использования
в нашем коде.

```php
<?
use Doctrine\ORM\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
class UserRepository
{
    private ObjectRepository $repo; //общий интерфейс для всех репоизиториев
                             // это именно встроенный доктриновский репозиторий
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repo = $em->getRepository(User::class);// ret ObjectRepository
        $this->em = $em;
    }
    // ..
    public function findByConfirmToken(string $token): ?User
    {
        return $this->repo->findOneBy(['joinConfirmToken.value' => $token ]);
    }

    public function findByNewEmailToken(string $token): ?User
    {
        return $this->repo->findOneBy(['newEmailToken.value'=> $token]);
    }
    //...
```
Здесь приватное поле `$repo` после вызова конструктора будет хранить служебный
доктриноский репозиторий настроенный на работу с классом нашей сущности User
Причем сам метод `$em->getRepository()` выдаёт именно общий интерфейс
ObjectRepository, при этом у него ограниченый набор методов, тогда как на деле
там возвращается класс EntityRepository у которого есть очень полезные методы.
Один из приметор таких методов - QueryBuilder позволяющий строить SQL Запросы
Для того чтобы сохранять именно этот класс и получать доступ ко всем имеющимся
его методам можно сделать в конструкторе такую штуку:
При этом пропишем для статического анализатора кода чтобы не выдавал ошибку.

```php
<?
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;
class UserRepository
{
    private EntityRepository $repo; // конкретный класс, а не интерфейс
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        /** @var EntityRepository $repo */
        $repo = $em->getRepository(User::class);// возвращает ObjectRepository
        $this->repo = $repo; // отдельная переменная для указания реального типа
        $this->em = $em;
    }
```

Решаем проблему с возвращемым типом ?User
метод `findOneBy` возвращает `object|null` а мы прописали `?User`
чтобы psalm не выдавал ошибку прописываем аннотацию указывая конкретный тип
```php
<? class UserRepository {

    public function findByConfirmToken(string $token): ?User //<<<<
    {
        /** @psalm-var User|null */
        return $this->repo->findOneBy(['joinConfirmToken.value' => $token ]);
    }
```
Еще для PhpStorm можно сделать костыль чтобы исключить его ошибки типов

```php
<? class UserRepository {

    /**
     * @param string $token
     * @return User|object|null  <-  для PhpStorm кастуем findOneBy в наш тип
     * @psalm-return User|null   <-  для psalm перекрываем аннотацию для PhpStorm
     */
    public function findByConfirmToken(string $token): ?User {
        /** @psalm-var User|null */
        return $this->repo->findOneBy(['joinConfirmToken.value' => $token ]);
    }
```

Пример реализации метода проверки наличия юзера по Email
Для этого используем QueryBuilder - продвинутый построитель запросов к БД

```php
<? class UserRepository {

    public function hasByEmail(Email $email): bool
    {
        return $this->repo->createQueryBuilder('t') // собрать построитель запроса
            ->select('COUNT(t.id)')                 // нужный нам запрос
            ->andWhere('t.email = :email')
            ->setParameter(':email', $email->getValue())
            ->getQuery()->getSingleScalarResult() > 0;
    }
```
- `COUNT(t.id)` - ищем число указанных нами идентификаторов для записей где
- `t.email = :email` емейл в записе совпадает с искомым
и если число количества найденых совпадаений больше 0 - значит есть такой юзер
т.е. емейл уже кем-то используется

Note: Причем именно для получения доступа к методу createQueryBuilder и его
методу select и нужен тот кастинг ObjectRepository в EntityRepository. если
вместо EntityRepository поле будет типа ObjectRepository то статические
анализаторы выдадут ошибку дескоть нет такого метода select у `$repo`

Второй способ передавать обьект `EntityRepository` в конструктор нашего класса
`UserRepository` через di-container

```php
<? class UserRepository {
    private EntityRepository $repo;
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, EntityRepository $repo)
    {
        $this->repo = $repo;
        $this->em = $em;
    }
```
Но для такого подхода надо будет прописывать в конфиге нашего di-контейнера свою
фабрику создания EntityRepository в ./api/config/common/doctrine.php по аналоги
как мы сделали для интерфейса `EntityManagerInterface`.

Дальше пишем реализации всех остальных методов в нашем классе UserRepository
пример того как должен работать метод get - получение юзера по его идишнику
```php
<? class UserRepository {
    public function get(Id $id): User // данный метод всегда либо возвращает User
    {                                 // либо кидает исключение - не найден.
        $user = $this->repo->find($id->getValue());
        if ($user === null) {
            // если в базе данных нет такого юзера - кидать исключение
            throw new DomainException('User is not found.');
        }
        /* @var User $user */
        return $user;
    }
```

## Реализация нашего Flusher-a

```php
<?php
declare(strict_types=1);
namespace App;
use Doctrine\ORM\EntityManagerInterface;

class Flusher
{
    private EntityManagerInterface $em;

    // принимаем от di-контейнера EntityManager
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function flush(): void
    {
        $this->em->flush(); // просто вызаем одноимённый метод.
    }
}
```

## Зачем собственный репозиторий UserRepository и свой Flusher?

Почему мы не передаём EntityManager напрямую в наши хэндлеры и будущие
контроллеры? А делаем мы это для того чтобы убрать доступ ко множеству разных
методов в самом EntityManager. Таким например как createQueryBuilder.
Как показывает практика, если дать прямой доступ к EntityManager внутрь
контроллеров и хэндлеров, то не опытные и не дисциплинированные программисты в
коменде начнут писать код используя напряму такие вещи как createQueryBuilder.
писав код с построением запросов к бд через createQueryBuilder прямо внутри
контроллеров. Другими словами - выносим только то что реально нужно в коде
уберая возможность выстрелить себе в ногу, в том числе чужими руками по
неопытности. Вынося только нужный нам функционал через свои классы и не давая
прямого доступа ни к доктриноскому репозиторию ни к EntityManager-у

Note: наследоваться своим UserRepository от EntityRepository не вариант, т.к.
при наследовании все методы EntityRepository будут доступны в обьекте нашего
класса UserRepository, т.е. скрыть доступ к чрезмерно широкому функционалу не
выйдет. Поэтому и используется агрегирование обьектов вунтри класса
UserRepository.

Вот пример такого подхода и наследованием:
```php
<? class UserRepository extends EntityRepository {
    public function get(Id $id): User
    {
        $user = $this->find($id->getValue());
        //           ^^ короче без доступа к полю $this->repo-find(..)
    }
```
Да так удобнее, но всплывает проблема избыточного доступа к методам
EntityRepository. т.к. при наследовании дочерний класс наследует все родительские
методы, а значит любой новичек в коменде сможет дёрнуть createQueryBuilder в любом
месте кода в том числе в контроллере или хэндлере, создавая говнокод.


