<?php

declare(strict_types=1);

use App\Console;
use App\Console\FixturesLoadCommand;
use Doctrine\Migrations;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Command;
use Doctrine\ORM\Tools\Console\Command\SchemaTool;
use Psr\Container\ContainerInterface;

return [
    FixturesLoadCommand::class => static function (ContainerInterface $c) {
        /**
         * @psalm-suppress MixedArrayAccess
         * @psalm-var array{fixture_paths:string[]} $config
         */
        $config = $c->get('config')['console'];

        /** @var EntityManagerInterface $em */
        $em = $c->get(EntityManagerInterface::class);

        return new FixturesLoadCommand($em, $config['fixture_paths']);
    },

    'config' => [
        'console' => [
            'commands' => [
                Console\FixturesLoadCommand::class,

                Console\MailerCheckCommand::class,
                Console\DICheckCommand::class,
                SchemaTool\DropCommand::class,
                Command\InfoCommand::class,

                Migrations\Tools\Console\Command\DiffCommand::class,
                Migrations\Tools\Console\Command\GenerateCommand::class,
            ],
            'fixture_paths' => [
                __DIR__ . '/../../src/Auth/Fixture',
            ],
        ],
    ],

];
