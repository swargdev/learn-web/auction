## Добавим команду для ручной перегенерации сертификатов

Для этого кроме playbook-а который у нас называется site.yml
добавим файл в котором создадим свою команду `renew`

./provisioning/renew-certificates.yml:
```yml
---
- name: Renew Certificates
  hosts: all
  remote_user: root
  tasks:
    - name: Renew Certificates
      shell: "certbot renew"
```

Используем так:
cd ./provisioning
ansible-playbook -i hosts.yml renew-certificates.yml

Работает достаточно просто - заходит на все переданные хосты
(файл hosts.yml) и выполняет на них shell-команду `certbot renew` которая
и вызовет перегенерацию сертификатов

