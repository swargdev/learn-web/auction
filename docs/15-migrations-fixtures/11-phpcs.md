## Добавляем игнорирование длинны строк для сгенерированных миграций


```sh
docker compose run --rm api-php-cli composer cs-check
> phpcs
............................................................ 60 / 88 (68%)
...................W........                                 88 / 88 (100%)



FILE: /app/src/Data/Migration/Version20231203143514.php
----------------------------------------------------------------------
FOUND 0 ERRORS AND 4 WARNINGS AFFECTING 4 LINES
----------------------------------------------------------------------
 23 | WARNING | Line exceeds 120 characters; contains 190 characters
 25 | WARNING | Line exceeds 120 characters; contains 130 characters
 27 | WARNING | Line exceeds 120 characters; contains 664 characters
 38 | WARNING | Line exceeds 120 characters; contains 193 characters
----------------------------------------------------------------------

Time: 72ms; Memory: 8MB

Script phpcs handling the cs-check event returned with error code 1
```

Чтобы скрыть эти ошибки о длинне строк просто добавляем исключение:

./api/phpcs.xml
```html
    <rule ref="Generic.Files.LineLength">
        <exclude-pattern>/app/src/Data/Migration/*.php</exclude-pattern>
    </rule>
```

Путь внутри контейнера т.к. каталоги с исходником прокидываются через волюм
внутрь контейнера api-php-cli и в нём проект лежит в `/app/`

Взято отсюда:

https://www.doctrine-project.org/projects/doctrine-coding-standard/en/11.0/reference/customizing.html

t's also possible to exclude either a rule or an error for specific files/directories:

```html
<rule ref="Doctrine"/>

<!-- allow long lines in src/FileWithLongLines.php and in tests/ -->
<rule ref="Generic.Files.LineLength">
    <exclude-pattern>src/FileWithLongLines.php</exclude-pattern>
    <exclude-pattern>tests/*</exclude-pattern>
</rule>
```


Другой способ через опции командной строки:
```sh
vendor/bin/phpcs --standard=PSR2  --exclude=Generic.Files.LineLength app/
```

это же поведение прописывается через файл phpcs.xml в корне проекта:

```html
<?xml version="1.0"?>
<ruleset name="PHP_CodeSniffer">

    <rule ref="PSR2" /> <!-- ruleset standard -->
    <rule ref="Generic.Files.LineLength"> <!-- rule to override -->
        <properties>
            <property name="lineLimit" value="150"/> <!-- maximum line length -->
        </properties>
    </rule>
    <file>app</file> <!-- directory you want to analyze -->
    <arg name="encoding" value="utf-8"/>
</ruleset>
```
