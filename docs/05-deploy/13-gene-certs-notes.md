## Заметки по поводу генерации сетрификатов certbot-отм через Ансибл роли

Если неправильно задать DNS-запись для поддомена api например вот таким образом:
```
AAAA 	auction.v6.rocks 	2a00:...:1bb
AAAA  api.auction.v6.rocks 2a00:...:1bb
```

Тогда при генерации сертификата в логе Ансибл вылетит такая ошибка:

`TASK [site : Generate new certificate]`

`RED` - `FAIL!` - это для auction.v6.rocks
failed: [server]

```ansible
(item={'changed': False, 'stat': {'exists': False}, 'invocation': {'module_args': {'path': '/etc/letsencrypt/live/auction.v6.rocks/cert.pem', 'follow': False, 'get_md5': False, 'get_checksum': True, 'get_mime': True, 'get_attributes': True, 'checksum_algorithm': 'sha1'}}, 'failed': False, 'item': 'auction.v6.rocks', 'ansible_loop_var': 'item'}) =>
```

```json
{
  "ansible_loop_var": "item",
  "changed": true,
  "cmd": "certbot certonly --noninteractive --agree-tos --email admin@auction.v6.rocks -d auction.v6.rocks",
  "delta": "0:00:03.684828",
  "end": "2023-09-10 12:12:40.359858",
  "item": {
    "ansible_loop_var": "item",
    "changed": false,
    "failed": false,
    "invocation": {
      "module_args": {
        "checksum_algorithm": "sha1",
        "follow": false,
        "get_attributes": true,
        "get_checksum": true,
        "get_md5": false,
        "get_mime": true,
        "path": "/etc/letsencrypt/live/auction.v6.rocks/cert.pem"
      }
    },
    "item": "auction.v6.rocks",
    "stat": {"exists": false}
  },
  "msg": "non-zero return code",
  "rc": 1,
  "start": "2023-09-10 12:12:36.675030",
  "stderr": "тоже самое что ниже\n но одной строкой\n убрал для наглядности",
  "stderr_lines": [
    "Saving debug log to /var/log/letsencrypt/letsencrypt.log",
    "Plugins selected: Authenticator webroot, Installer None",
    "Performing the following challenges:",
    "http-01 challenge for auction.v6.rocks",
    "Using the webroot path /var/www/html for all unmatched domains.",
    "Waiting for verification...",
    "Challenge failed for domain auction.v6.rocks",
    "http-01 challenge for auction.v6.rocks",
    "Cleaning up challenges", "Some challenges have failed."
  ],
  "stdout": "...\n...",
  "stdout_lines": [
    "Requesting a certificate for auction.v6.rocks",
    "IMPORTANT NOTES:",
    " - The following errors were reported by the server:",
    "",
    "   Domain: auction.v6.rocks",
    "   Type:   dns",
    "   Detail: no valid A records found for auction.v6.rocks; no valid",
    "   AAAA records found for auction.v6.rocks"]
}
```

`GREEN` -  `OK!` - это для api.auction.v6.rocks
changed: [server] => (item=
{'changed': False, 'stat': {'exists': False}, 'invocation': {'module_args': {'path': '/etc/letsencrypt/live/api.auction.v6.rocks/cert.pem', 'follow': False, 'get_md5': False, 'get_checksum': True, 'get_mime': True, 'get_attributes': True, 'checksum_algorithm': 'sha1'}}, 'failed': False, 'item': 'api.auction.v6.rocks', 'ansible_loop_var': 'item'})

Т.к. в файле ./provisioning/roles/site/defaults/main.yml
у меня задано два доменя для генерации сертификата:
```yml
certbot_hosts:
  - auction.v6.rocks
  - api.auction.v6.rocks
```
то в этой задаче `TASK [site : Generate new certificate]` и идёт две итерации
циклом по списку с именем `certbot_hosts` из двух элементов - строк

Здесь ошибка в том, что неправильно указав DNS-запись для поддомена получаем
недоступным сам основной домен. поэтому вторая итерация проходит успешно а
первая падает и останавливает выполнение всех последующих задач - в том числе
остановку временного сервера apache.
останавливал руками зайдя на удалённую машину через `docker rm -f apache`

`Note` если задача по генерации ключа завалится на полпути, то поднятый
контейнер с апачем останется работать!

## Правильно укажем поддомен
CNAME 	api.auction.v6.rocks


Правильное заполнение DNS-Записей для IPv6 вот так:
```
AAAA        auction.v6.rocks  2a00:..:1bb
CNAME 	api.auction.v6.rocks 	not set
```

у CNAME на сайте dynv6.com DNS-record Заполняется вот так:
CNAME name='api' data='' (поле `data` оставлем пустым)

Получаем полностью успешную генерацию сертификатов

`TASK [site : Generate new certificate]`
`Создался сертификат для auction.v6.rocks`
changed: [server] => (item={'changed': False, 'stat': {'exists': False}, 'invocation': {'module_args': {'path': '/etc/letsencrypt/live/auction.v6.rocks/cert.pem', 'follow': False, 'get_md5': False, 'get_checksum': True, 'get_mime': True, 'get_attributes': True, 'checksum_algorithm': 'sha1'}}, 'failed': False, 'item': 'auction.v6.rocks', 'ansible_loop_var': 'item'})
`для api.auction.v6.rocks уже был создан поэтому здесь пропуск под-задачи(в цикле)`
skipping: [server] => (item={'changed': False, 'stat': {'exists': True, 'path': '/etc/letsencrypt/live/api.auction.v6.rocks/cert.pem', 'mode': '0777', 'isdir': False, 'ischr': False, 'isblk': False, 'isreg': False, 'isfifo': False, 'islnk': True, 'issock': False, 'uid': 0, 'gid': 0, 'size': 44, 'inode': 2622529, 'dev': 2050, 'nlink': 1, 'atime': 1694337167.6337955, 'mtime': 1694337167.6257956, 'ctime': 1694337167.6257956, 'wusr': True, 'rusr': True, 'xusr': True, 'wgrp': True, 'rgrp': True, 'xgrp': True, 'woth': True, 'roth': True, 'xoth': True, 'isuid': False, 'isgid': False, 'blocks': 0, 'block_size': 4096, 'device_type': 0, 'readable': True, 'writeable': True, 'executable': False, 'lnk_source': '/etc/letsencrypt/archive/api.auction.v6.rocks/cert1.pem', 'lnk_target': '../../archive/api.auction.v6.rocks/cert1.pem', 'pw_name': 'root', 'gr_name': 'root', 'mimetype': 'inode/symlink', 'charset': 'binary', 'version': None, 'attributes': [], 'attr_flags': ''}, 'invocation': {'module_args': {'path': '/etc/letsencrypt/live/api.auction.v6.rocks/cert.pem', 'follow': False, 'get_md5': False, 'get_checksum': True, 'get_mime': True, 'get_attributes': True, 'checksum_algorithm': 'sha1'}}, 'failed': False, 'item': 'api.auction.v6.rocks', 'ansible_loop_var': 'item'})


