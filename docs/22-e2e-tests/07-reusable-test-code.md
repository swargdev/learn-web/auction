## Выносим повторяющийся код E2E-тестов

Здесь мы вынесем код, который может быть переиспользуем при написании
наших е2е-тестов. Для уменьшения его дублирования

На данный момент у нас есть

```
cucumber/features/
├── home.feature       - описание "фичи" - поведенческих харак-тик для е2е теста
├── step_definition/
│   └── home.js        - "дефинишен" реализация фичи описанной в home.feature
└── support/
    ├── hooks.js
    └── world.js
```

Дальше нам нужно будет описывать новые фичи, а значит например будет нужно
переиспользовать однотипные шаги сценариев.
например

Вот этот шаг может понадобится в разных сценариях, не только для home.features
```js
Given('I am a guest user', function () { });
```

Вынесем этот код для переиспользования:
Для этого создадим каталог `steps` в `features/support`

./cucumber/features/support/steps/user.js
```js
const { Given } = require('@cucumber/cucumber');

Given('I am a guest user', function () {});
```

Удалив этот же код из home.js:

./cucumber/features/step_definition/home.js
```js
const { When, Then } = require('@cucumber/cucumber');
const { expect } = require('chai');

When('I open home page', async function () {
  return await this.page.goto('http://gateway:8080');
});

Then('I see welcome block', async function () {
  await this.page.waitForSelector('[data-test=welcome]')
  const callbackGetText = function(el) { return el.textContent }
  const text = await this.page.$eval('[data-test=welcome] h1', callbackGetText)
  expect(text).to.eql('Auction')
});
```

Вообще регистрировать новый файл `./cucumber/features/support/steps/user.js`
не нужно просто потому, что cucumber импортирует абсолютно все js-файлы из
каталога `features` и всех его дочерних подкаталогов. Поэтому структура внутри
`features/` может быть любая.


## Адреса страниц вместо имён страниц

```
  Scenario: View home page content
    Given I am a guest user
    When I open home page
    Then I see welcome block
```

Для тестов более удобно будет именовать страницы не по имени а по адресу:
`When I open home page` -> `When I open "/" page`

Переписываем step_definition на использование параметров (placeholder)
```js
When('I open {string} page', async function (uri) {
  return await this.page.goto('http://gateway:8080' + uri);
})
```

В итоге наш шаг теперь может открывать в браузере любую страницу по её адресу
поэтмоу выносим этот код в отдельный файл support/steps/page.js и удаляя его
из home.js

./cucumber/features/support/steps/page.js
```js
const { When } = require('@cucumber/cucumber');

When('I open {string} page', async function (uri) {
  return await this.page.goto('http://gateway:8080' + uri);
});
```

В общем принцип простой
- общие шаги мы выносим в отдельные файлы откуда cucumber и будет их вызывать
  support/steps/
- специфичные вещи для конкретной фичи мы размещаем в соответствующем файле
  `home.featrure` -> `step_definition/home.js`


Убеждаемся что обновление кода не сломало е2е тест:

```sh
docker compose run --rm cucumber-node-cli yarn e2e
yarn run v1.22.19
$ cucumber-js
.....
1 scenario (1 passed)
3 steps (3 passed)
0m07.372s (executing steps: 0m07.357s)
Done in 11.32s.
```


