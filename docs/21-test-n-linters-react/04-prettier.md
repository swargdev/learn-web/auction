

## Prettier дополнительные файлы для фронтенда

Итак мы установили линтеры `eslint` и `stylelint`
- `eslint` может работать не только как простой линтер, обнаруживающий
  синтаксические ошибки в коде(не там зяпятая) и нарушения стиля кода,
  но и как статический анализатор js-кода т.е. может находить
  семантические(смысловые) ошибки в коде (не обявленная перменная)
  (как psalm в php)

`eslint` - умеет исправлять ошибки в js-коде `make eslint-fix`, a
`stylelint` - не умеет исправлять ошибки в css, умеет только находить их

Но в файлах нашего фронтенда будут не только js, jsx и css-файлы
например большинство конфигураций у наc хранится в формате json
так же документацию мы можем хранить в формате md (Markdown)
И по-хорошему и для этих форматов тоже хорошо бы иметь инструмент,
проверяющий их на наличие ошибок. и такой иснтрумент есть - `prettier`

Этот интсрумент можно использовать как вообще полностью отдельно от других
линтеров и статических анализаторов кода так и совместно с ними.
Конкретно в нашем случае его можно интегрировать с `eslint` и `stylelint`.


http://prettier.io - Библиотека умеющая исправлять форматирования кода
для многих языков программирования, используемых в основном во фтронтенд
разработке.

What is Prettier?
- An opinionated code formatter
- Supports many languages
- Integrates with most editors
- Has few options »

Why?
- Your code is formatted on save
- No need to discuss style in code review
- Saves you time and energy
- And more »

Works with the Tools You Use:
- JavaScript: JSX Flow TypeScript JSON
- HTML:       Vue Angular Ember / Handlebars
- CSS:        Less SCSS styled-components styled-jsx
- GraphQL:    GraphQL Schemas
- Markdown:   CommonMark GitHub-Flavored Markdown MDX v1
- YAML:
- Community Plugins: Apex Java PHP Ruby Rust TOML XML And more...


## Устанавливаем Prettier интегрируя его с eslint и stylelint

задача не просто поставить биб-ку prettier, а интегрировать на совместную
работу с уже установлеными линтерами для js и для css
иначе они могут работать в разнобой и конфликтовать выдавая разную диагностику

https://prettier.io/docs/en/integrating-with-linters

Для интеграции с другими линтерами prettier-у нужны доп плагины, которые
ставятся в виде отдельных биб-к-пакетов

для совместной работы prettier с eslint нужно будет поставить два плагина:

- eslint-config-prettier  - это плагин для синхронизации проверки code-style
- eslint-plugin-prettier  - плагин для запуска prettier как правила в eslint


> 1й плагин - `eslint-config-prettier` - настройки форматирования и код-стайл
https://github.com/prettier/eslint-config-prettier
задача этого плагина интегрировать работу prettier и eslint

ставится он в секцию dev: вот так
```sh
npm install --save-dev eslint-config-prettier
```
после установки плагина нужно будет прописать его в конфиг файл .eslintrc.json:
```json
{
  "extends": [
    "some-other-config-you-use",
    "prettier",
  ]
}
```
Note:
Но не спеши сразу это прописывать - это только на случай когда используем
только один плагин. для нас же важно поставить разом всю сборку из двух плагинов


Как помним секция `extends` - это про подключение внешних конфиг-файлов,
поэтому при подключении prettier произойдёт подключение конфига prettier-а
который переопределит плавила `rules` с которыми он обычно конфликтует.
например prettier не добавляет пробел перед параметрами функции `App ()`
поэтому при добавлении prettier в extends произойдёт автодобавление правила
которое мы сами прописали явно в rules:space-before-function-paren:off

> 2й плагин `eslint-plugin-prettier` - запуск Prettier через ESLint
это доп.-ый плагин для eslint, который добавит запуск Prettier-а при
запуске самого ESLint:

https://github.com/prettier/eslint-plugin-prettier#recommended-configuration

Runs Prettier as an ESLint rule and reports differences as individual
ESLint issues.

Установка такая: т.е. разом ставятся сразу два плагина
```sh
npm install --save-dev eslint-config-prettier eslint-plugin-prettier
# or
yarn add --dev eslint-config-prettier eslint-plugin-prettier
```

Установка этих плагинов должна идти на уже установленные Prettier и ESLint:
`eslint-plugin-prettier` does not install `Prettier` or `ESLint` for you.
You must install these yourself.

> Configuration через `.eslintrc`

For legacy configuration, this plugin ships with a `plugin:prettier/recommended`
config that sets up both `eslint-plugin-prettier` and `eslint-config-prettier`
in one go.

Add `plugin:prettier/recommended` as the last item in the extends array in your
`.eslintrc*` config file, so that `eslint-config-prettier` has the opportunity
to override other configs:

.eslintrc.json
```json
{
  "extends": [
    "plugin:prettier/recommended",
  ]
}
```
Заметь: этой строкой подлючаем сразу оба плагина и
- eslint-config-prettier
- eslint-plugin-prettier
И делается это здесь вместо простой строки `prettier` как было в конфиге
только для одного `eslint-config-prettier`

This will:(Такое прописывание extends в .eslintrc сделает):
- Enable the `prettier/prettier` rule.
- Disable the arrow-body-style and prefer-arrow-callback rules
  which are problematic with this plugin - see the below for why.
- Enable the `eslint-config-prettier` config which will turn off `ESLint` rules
  that conflict with `Prettier`.

Так же нужно прописать Prettier в секцию plugin:
./frontend/.eslintrc.json
```json
 "plugins": ["prettier"],
```


## Prettier+Stylelint
https://github.com/prettier/eslint-config-prettier

https://github.com/prettier/stylelint-prettier

Runs `Prettier` as a `Stylelint` rule and reports differences as individual
Stylelint issues.

stylelint-config-prettier


> Installation

```sh
npm install --save-dev stylelint-prettier prettier
```

stylelint-prettier does not install Prettier or Stylelint for you.
You must install these yourself.

.stylelintrc:
```json
{
  "plugins": ["stylelint-prettier"],
  "rules": {
    "prettier/prettier": true
  }
}
```

Здесь так же есть 2й плагин для запуска проверок Prettier при запуске Stylelint
Use Stylelint to run Prettier
stylelint-prettier

```sh
yarn add --dev stylelint-prettier
```

В общем prettier умеет исправлять ошибки но не умеет(??) выводить отчёты об их
обнаружении, поэтому мы и ставим вторые плагины для eslint & stylelint которые
будут запускать prettier как rules и выодить его диагностику о найденых обишках

В итоге конфиг файл для ESLint будет выглядеть так:

./frontend/.eslintrc.json
```json
{
  "env": {
    "browser": true,
    "es2021": true,
    "node": true,
    "jest": true
  },
  "extends": [
    "standard",
    "plugin:react/recommended",
    "plugin:prettier/recommended"     // +
  ],
  "parserOptions": {
    "ecmaVersion": "latest",
    "sourceType": "module"
  },
  "plugins": [
    "react",
    "prettier"                       // +
  ],
  "settings": {
    "react": {
      "version": "18"
    }
  },
  "rules": {
    "semi": "off",
    "prettier/prettier": "error"     // +
  }
}
```

добавив в rules `prettier/prettier: error` можно убирать правило:
 "space-before-function-paren": "off"


Конфиг для Stylelint будет такой:
./frontend/.stylelintrc.json
```json
{
  "extends": [
    "stylelint-config-standard",
    "stylelint-prettier/recommended"
  ],
  "plugins": ["stylelint-prettier"],
  "rules": {
    "prettier/prettier": true
  }
}
```

В нём до этого было только `"extends": "stylelint-config-standard"`

Здесь так же важен порядок подключения конфигов. в секиции extends
следующий конфиг переопеределяет настройки предыдущего поэтому у нас здесь
stylelint-prettier/recommended нужно обязательно помещать ниже тех что у нас
уже были ранее `stylelint-config-standard`. Иначе станадртные перезапишут
правила prettier-а

`rule prettier/prettier: true` - для того чтобы ошибки prettier выводились в
диагностике найденых ошибок при работе самого Stylelint



## Конфиг для самого Prettier

```json
{
  "printWidth": 100,
  "arrowParens": "always",
  "semi": false,
  "tabWidth": 2,
  "singleQuote": true
}
```
все эти параметры описаны в документации
printWidth - это шинира строки, если текст выступает - переносить на новую строку
arrowParens - это для анонимных функций:

```js
new Promise()
  .then(registration => {registration.unreg()})
  .catch(error => {console.error(error.message)})
```
Заметь здесь аргументы анонимных функций registration и error без скобок,
да в js так можно но это не лучший подход, да и если надо будет указать
второй аргумент для аноним функции то всё равно надо будет добавлять скобки

```js
new Promise()
  .then((registration) => {registration.unreg()})
  .catch((error) => {console.error(error.message)})
```

arrowParens always - говорит Prettier-у всегда оборачивать параметры
анонимной функции в скобки

- "semi": false, - не использовать ; в конце строк
- "tabWidth": 2, - ширина отступа(ident) - два пробела
- "singleQuote": true  - строки обрамлять именно в одинарные кавычки а не двойн.


## Установка библиотеки Prettier и всех нужных плагинов:

```sh
docker compose run --rm frontend-node-cli yarn add --dev prettier \
eslint-config-prettier eslint-plugin-prettier \
stylelint-config-prettier stylelint-prettier
```

```
yarn add v1.22.19
[1/4] Resolving packages...
[2/4] Fetching packages...
[3/4] Linking dependencies...
warning " > stylelint-config-prettier@9.0.5" has incorrect peer dependency "stylelint@>= 11.x < 15".
[4/4] Building fresh packages...
success Saved lockfile.
success Saved 17 new dependencies.
info Direct dependencies
├─ eslint-config-prettier@9.1.0
├─ eslint-plugin-prettier@5.1.2
├─ prettier@3.1.1
├─ stylelint-config-prettier@9.0.5
└─ stylelint-prettier@5.0.0
info All dependencies
├─ @pkgr/utils@2.4.2
├─ big-integer@1.6.52
├─ bplist-parser@0.2.0
├─ bundle-name@3.0.0
├─ default-browser-id@3.0.0
├─ default-browser@4.0.0
├─ eslint-config-prettier@9.1.0
├─ eslint-plugin-prettier@5.1.2
├─ fast-diff@1.3.0
├─ is-inside-container@1.0.0
├─ prettier@3.1.1
├─ run-applescript@5.0.0
├─ stylelint-config-prettier@9.0.5
├─ stylelint-prettier@5.0.0
├─ synckit@0.8.7
├─ titleize@3.0.0
└─ untildify@4.0.0
Done in 108.20s.
```
Пока нет пакета `stylelint-config-prettier` для 16й версии stylelint
поэтому выдаётся такое предупреждение:
warning " > stylelint-config-prettier@9.0.5"
has incorrect peer dependency "stylelint@>= 11.x < 15".
прока просто игнорирую это - со временем должен будет выйти - тогда обновлю


## Проверяем успешность установки и интеграции Prettier с ESLint Stylelint

```sh
make frontend-lint
```

```
docker compose run --rm frontend-node-cli yarn eslint
yarn run v1.22.19
$ eslint --ext .js,.jsx src

/app/src/App.test.jsx
  5:11  error  Replace `'./components/Welcome',·()·=>` with `⏎··'./components/Welcome',⏎··()·=>⏎···`                                prettier/prettier
  6:3   error  Replace `return·<span·data-testid='WelcomeComponent'·/>` with `····return·<span·data-testid="WelcomeComponent"·/>;`  prettier/prettier
  7:1   error  Replace `}` with `····},⏎`                                                                                           prettier/prettier

/app/src/index.jsx
  11:22  error  Insert `,`  prettier/prettier

/app/src/reportWebVitals.js
  1:25  error  Replace `onPerfEntry` with `(onPerfEntry)`  prettier/prettier

✖ 5 problems (5 errors, 0 warnings)
  5 errors and 0 warnings potentially fixable with the `--fix` option.

error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
```

Ругается на этот файл ./frontend/src/App.test.jsx
```js
import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

jest.mock('./components/Welcome', () => function mockWelcome(/* props */) {
  return <span data-testid='WelcomeComponent' />
});

test('renders app', () => {
  render(<App />);
  expect(screen.getByTestId('WelcomeComponent')).toBeInTheDocument();
  // screen.debug()
});
```

## Добавляем консольные команды в package.json для запуска prettier-а

Через эту команду мы будем вызывать сам prettier - для автоматического
исправления найденных ошибок форматирования
```json
"scripts": {
  ...
  "prettier": "prettier --write \"**/*.+(js|ts|jsx|tsx|json|css|md)\""
}
```

добавляем в Makefile команду для запуска `prettier`
```Makefile
frontend-eslint-fix:
	docker compose run --rm frontend-node-cli yarn eslint-fix

frontend-pretty:
	docker compose run --rm frontend-node-cli yarn prettier
```

Пробуем исправить ошибки в коде:

```sh
make frontend-pretty
docker compose run --rm frontend-node-cli yarn prettier
yarn run v1.22.19
$ prettier --write "**/*.+(js|ts|jsx|tsx|json|css|md)"
.eslintrc.json 56ms
.stylelintrc.json 4ms
package.json 6ms (unchanged)
public/manifest.json 5ms (unchanged)
README.md 55ms (unchanged)
src/App.css 30ms
src/App.jsx 12ms (unchanged)
src/App.test.jsx 12ms
src/components/Welcome.jsx 4ms (unchanged)
src/components/Welcome.module.css 9ms (unchanged)
src/components/Welcome.test.jsx 5ms (unchanged)
src/index.css 3ms
src/index.jsx 6ms
src/reportWebVitals.js 8ms
src/setupTests.js 1ms (unchanged)
Done in 0.65s.
```

unchanged - файлы полностью исправны и без ошибок - "не изменились"
как видно prettier исправил и json файлы тоже:
- .eslintrc.json 56ms
- .stylelintrc.json 4ms


После запуска prettier-а теперь запуск линтера проходит успешно и без ошибок
```sh
make frontend-lint
docker compose run --rm frontend-node-cli yarn eslint
yarn run v1.22.19
$ eslint --ext .js,.jsx src
Done in 1.47s.
docker compose run --rm frontend-node-cli yarn stylelint
yarn run v1.22.19
$ stylelint "src/**/*.css"
Done in 0.81s.
```

App.test.jsx исправило так:
```js
jest.mock(
  './components/Welcome',
  () =>
    function mockWelcome(/* props */) {
      return <span data-testid="WelcomeComponent" />;
    },
);

// было так:

jest.mock('./components/Welcome', () => function mockWelcome(/* props */) {
  return <span data-testid='WelcomeComponent' />
});
```

То есть после установки prettier для форматирования и исправления js-кода
теперь можно использовать только его и не использовать eslint-fix
Но в то же время prettier только про форматирование кода, он не умеет так же
как eslint статически анализировать код на семантические ошибки.
поэтому eslint-fix всё же иногда может быть полезен поэтому его оставляем

```Makefile
frontend-lint:
	docker compose run --rm frontend-node-cli yarn eslint
	docker compose run --rm frontend-node-cli yarn stylelint

frontend-eslint-fix:
	docker compose run --rm frontend-node-cli yarn eslint-fix

frontend-pretty:
	docker compose run --rm frontend-node-cli yarn prettier
```
Поэтому и создаём для них разные команды чтобы было более понятно
- frontend-pretty - используем чаще, для исправления форматирования кода
- frontend-eslint-fix - используем реже, для исправления семантических ошибок.



## REFS
[Stylelint](https://github.com/stylelint/stylelint)
[.prettierrc](https://prettier.io/docs/en/options.html)
