Просмотр содержимого таблиц в PostgreSQL через консольную утилиту psql

Смотрим через консоль что накатилось после применения миграций:

```sh
PGPASSWORD=secret psql -h localhost -p 54321 -U app -d app
```


```
psql (13.13 (Debian 13.13-0+deb11u1), server 13.12)
Type "help" for help.

app=# \dt
              List of relations
 Schema |        Name        | Type  | Owner
--------+--------------------+-------+-------
 public | auth_user_networks | table | app
 public | auth_users         | table | app
 public | migrations         | table | app
(3 rows)
```
здесь показывается что у нас в "схеме" public есть три таблицы (Name)

посмотреть всё содержимое таблицы migrations можно вот так:
```
app=# select * from migrations;

                 version                  |     executed_at     | execution_time
------------------------------------------+---------------------+----------------
 App\Data\Migration\Version20231203143514 | 2023-12-03 15:38:42 |            496
```

вот так можно просмотреть поля и настройки таблицы с миграциями:
```
app=# \d+ migrations

                                                             Table "public.migrations"

     Column     |              Type              | Collation | Nullable |              Default              | Storage  | Stats target | Description
----------------+--------------------------------+-----------+----------+-----------------------------------+----------+--------------+-------------
 version        | character varying(191)         |           | not null |                                   | extended |              |
 executed_at    | timestamp(0) without time zone |           |          | NULL::timestamp without time zone | plain    |              |
 execution_time | integer                        |           |          |                                   | plain    |              |
Indexes:

    "migrations_pkey" PRIMARY KEY, btree (version)
```




посмотреть на все колонки таблицы `auth_users`

```
\d+ auth_users

            Column            |              Type              | Nullable |              Default              | Storage  |         Description
------------------------------+--------------------------------+----------+-----------------------------------+----------+------------------------------
 id                           | uuid                           | not null |                                   | plain    | (DC2Type:auth_user_id)
 date                         | timestamp(0) without time zone | not null |                                   | plain    | (DC2Type:datetime_immutable)
 email                        | character varying(255)         | not null |                                   | extended | (DC2Type:auth_user_email)
 status                       | character varying(16)          | not null |                                   | extended | (DC2Type:auth_user_status)
 password_hash                | character varying(255)         |          | NULL::character varying           | extended |
 new_email                    | character varying(255)         |          | NULL::character varying           | extended | (DC2Type:auth_user_email)
 role                         | character varying(16)          | not null |                                   | extended | (DC2Type:auth_user_role)
 join_confirm_token_value     | character varying(255)         |          | NULL::character varying           | extended |
 join_confirm_token_expires   | timestamp(0) without time zone |          | NULL::timestamp without time zone | plain    | (DC2Type:datetime_immutable)
 password_reset_token_value   | character varying(255)         |          | NULL::character varying           | extended |
 password_reset_token_expires | timestamp(0) without time zone |          | NULL::timestamp without time zone | plain    | (DC2Type:datetime_immutable)
 new_email_token_value        | character varying(255)         |          | NULL::character varying           | extended |
 new_email_token_expires      | timestamp(0) without time zone |          | NULL::timestamp without time zone | plain    | (DC2Type:datetime_immutable)

Indexes:
    "auth_users_pkey" PRIMARY KEY, btree (id)
    "uniq_d8a1f49ce7927c74" UNIQUE, btree (email)

Referenced by:
    TABLE "auth_user_networks" CONSTRAINT "fk_3ea78c3ba76ed395" FOREIGN KEY (user_id) REFERENCES auth_users(id) ON DELETE CASCADE
```



```
app=# \d+ auth_user_networks

                                              Table "public.auth_user_networks"

      Column      |         Type          | Collation | Nullable | Default | Storage  | Stats target |      Description
------------------+-----------------------+-----------+----------+---------+----------+--------------+------------------------
 id               | uuid                  |           | not null |         | plain    |              |
 user_id          | uuid                  |           | not null |         | plain    |              | (DC2Type:auth_user_id)
 network_name     | character varying(16) |           | not null |         | extended |              |
 network_identity | character varying(16) |           | not null |         | extended |              |

Indexes:
    "auth_user_networks_pkey" PRIMARY KEY, btree (id)
    "idx_3ea78c3ba76ed395" btree (user_id)
    "uniq_3ea78c3b257ebd71c756d255" UNIQUE, btree (network_name, network_identity)

Foreign-key constraints:
    "fk_3ea78c3ba76ed395" FOREIGN KEY (user_id) REFERENCES auth_users(id) ON DELETE CASCADE
```


