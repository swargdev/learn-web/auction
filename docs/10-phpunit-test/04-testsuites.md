## Разделение запуска функциональных и юнит-тестов (testsuites)

- разделяем быстрые и медленные тесты на разные testsuites

Зачем:
Разделение может сыкономить много времени при разработке проекта.
Например когда надо запускать только юыстрые юнит тесты и не ждать
пока отработают еще и все медленные функциональные тесты.

Новые команды:
- make test-unit
- make test-functional
- make api-test-unit
- make api-test-functional


## О том как разделять тесты разных типов и запускать по отдельности

Пример полной команды для запуска только функциональных тестов
```sh
docker compose run --rm api-php-cli vendor/bin/phpunit --filter=Functional
```
```
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.
Runtime:       PHP 8.1.23
Configuration: /app/phpunit.xml
...                                                                 3 / 3 (100%)
Time: 00:00.023, Memory: 10.00 MB
OK (3 tests, 5 assertions)
```
Это работает за счёт того, что функ и юнит тесты у нас в разных каталогах
```
api/tests/
├── Functional
│   ├── HomeTest.php
│   ├── NotFoundTest.php
│   └── WebTestCase.php
└── Unit
    └── Http
        └── JsonResponseTest.php
```

## Как передать опцию --filter=Functional в vendor/bin/phpunit
если сделать вот так
```sh
docker compose run --rm api-php-cli composer test --filter=Functional
```
то нашу опцию обработает сам composer и не передаст ей в vendor/bin/phpunit

`--` - говорит composer-у что флаги для него самого закончились и всё остальное
предназначено для передачи в vendor/bin/phpunit
```sh
docker compose run --rm api-php-cli composer test -- --filter=Functional
```

Ну а сам маппинг на слово test для cmposer-а мы уже прописали вот здесь:
composer.json:

        "test": "phpunit --colors=always",

Более удобный способ запускать разного типа тесты
- настроить два testsuite через phpunit.xml

Вот как у нас был прописан только один testsuite:
```xml
    <testsuites>
        <testsuite name="default">                          // имя секции
            <directory suffix="Test.php">tests</directory>
        </testsuite>
    </testsuites>
```
здесь default - имя секции. дальше описано что брать их из каталога tests

Прописываем две секции unit и functional: из разных каталогов
```xml
    <testsuites>
        <testsuite name="unit">
            <directory suffix="Test.php">tests/Unit/</directory>
        </testsuite>
        <testsuite name="functional">
            <directory suffix="Test.php">tests/Functional</directory>
        </testsuite>
    </testsuites>
```
Вот так запустятся все тесты из всех описанных testsuites
```sh
make test
docker compose run --rm api-php-cli composer test
```
```
> phpunit --colors=always
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.
Runtime:       PHP 8.1.23
Configuration: /app/phpunit.xml
..........                                                        10 / 10 (100%)
Time: 00:00.026, Memory: 10.00 MB
OK (10 tests, 26 assertions)
```

Для того чтобы запустить только один конкретный testsuite по его имени:
```sh
docker compose run --rm api-php-cli composer test -- --testsuite=functional
```
```
> phpunit --colors=always '--testsuite=functional'
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.
Runtime:       PHP 8.1.23
Configuration: /app/phpunit.xml
...                                                                 3 / 3 (100%)
Time: 00:00.020, Memory: 10.00 MB
OK (3 tests, 5 assertions)
```

алиасы для полных команд в самом начале для всего проекта(но пока только api)
и отдельно только для api. все, юнит, функциональные:
```Makefile
test: api-test                                        # уже было
test-unit: api-test-unit
test-functional: api-test-functional

# ...

api-test:                                              # уже было
	docker compose run --rm api-php-cli composer test

api-test-unit:
	docker compose run --rm api-php-cli composer test -- --testsuite=unit

api-test-functional:
	docker compose run --rm api-php-cli composer test -- --testsuite=functional
```


