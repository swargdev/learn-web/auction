<?php

declare(strict_types=1);

namespace App\Http\Test\Unit\Middleware;

use App\Http\Middleware\ContentLanguage;
use App\Http\Test\Unit\WebUTestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @covers \App\Http\Middleware\ContentLanguage;
 * 12-12-2023
 * @author Swarg
 */
class ContentLanguageTest extends WebUTestCase
{
    // create Controller stab what wait specific locale (Accept-Language asserts)
    private function mkControllerExpLocale(string $exp): RequestHandlerInterface
    {
        $handler = $this->createStub(RequestHandlerInterface::class);

        $handler->method('handle')->willReturnCallback(
            static function (ServerRequestInterface $request) use ($exp): ResponseInterface {
                $actual = $request->getHeaderLine('Accept-Language');

                self::assertEquals($exp, $actual); // workload

                return self::createResponse();
            }
        );
        return $handler;
    }


    public function testDefault(): void
    {
        $middleware = new ContentLanguage(['en', 'ru']);
        $handler = $this->mkControllerExpLocale('');

        $middleware->process(self::createRequest(), $handler);
    }

    public function testAccepted(): void
    {
        $middleware = new ContentLanguage(['en', 'ru']);

        $request = self::createRequest()->withHeader('Accept-Language', 'ru');
        $handler = $this->mkControllerExpLocale('ru');

        $middleware->process($request, $handler);
    }

    public function testMulti(): void
    {
        $middleware = new ContentLanguage(['en', 'ru']);

        $request = self::createRequest()
            ->withHeader('Accept-Language', 'es;q=0.9, ru;q=0.8, *;q=0.5');

        $handler = $this->mkControllerExpLocale('ru');

        $middleware->process($request, $handler);
    }

    public function testOther(): void
    {
        $middleware = new ContentLanguage(['en', 'ru']);

        $request = self::createRequest()->withHeader('Accept-Language', 'es');
        $handler = $this->mkControllerExpLocale('en');

        $middleware->process($request, $handler);
    }
}
