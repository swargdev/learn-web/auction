## Doctrine Data Fixtures Extension
https://www.doctrine-project.org/projects/data-fixtures.html
https://github.com/doctrine/data-fixtures

Библиотека для автоматизированного заполнения таблиц БД dev-данными.

```sh
composer require --dev doctrine/data-fixtures
```

```sh
docker compose run --rm api-php-cli composer require --dev doctrine/data-fixtures
```

флаг --dev для того чтобы установилась библиотека только для локального
использования и не попала в прод. Если "забыть" этот флаг и установить эту либу
то можно испортить и потерять данные в базеданных прода.

Подключив эту библиотеку можно будет по аналогии с миграциями писать и фикстуры.

https://www.doctrine-project.org/projects/doctrine-data-fixtures/en/1.7/how-to/sharing-objects-between-fixtures.html

Пример из документации того, как писать фикстуры
```php
<?php

namespace MyDataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;

class UserDataLoader extends AbstractFixture
{
    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setUsername('jwage');
        $user->setPassword('test');
        $user->setRole(
            $this->getReference('admin-role', Role::class) // load the stored reference
        );

        $manager->persist($user);
        $manager->flush();

        // store reference of admin-user for other Fixtures
        $this->addReference('admin-user', $user);
    }
}
```

Создаём свою первую фикстуру ./api/src/Auth/Fixture/UserFixture.php

```php
<?php

declare(strict_types=1);

namespace App\Auth\Fixture;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\Id;
use App\Auth\Entity\User\Token;
use App\Auth\Entity\User\User;
use DateTimeImmutable;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

final class UserFixture extends AbstractFixture // наш класс наследуется от абстартного
{
    // 'password' уже захэшированный пароль в виде константы
    // для хэширования можно использовать и свой хэшер, но генерация хэша - затратная процедура
    // и будет каждый раз грузить CPU. поэтому лучше один раз захэшировать и переиспользовать
    // пароль вынесен в константу для переиспользования при создании еще других юзеров
    private const PASSWORD_HASH = '$2y$12$qwnND33o8DGWvFoepotSju7eTAQ6gzLD/zy6W8NCVtiHPbkybz.w6';

    // здесь $manager - должен быть наш EntityManager настроенный на работу с БД
    public function load(ObjectManager $manager): void
    {
        // создаём нового пользователя именно с фиксированными данными.
        $user = User::requestJoinByEmail(
            new Id('00000000-0000-0000-0000-000000000001'),
            $date = new DateTimeImmutable('-30 days'),
            new Email('user@app.test'),
            self::PASSWORD_HASH,
            // токен здесь просто случайный для подтверждения регистрации по емейлу
            // после подтверждения токен просто будет отброшен
            new Token($value = Uuid::uuid4()->toString(), $date->modify('+1 day'))
        );

        $user->confirmJoin($value, $date); // сразу подтверждаем регистрацию

        $manager->persist($user); // отправляем на сохранение в БД

        $manager->flush(); // само сохранение изменений в БД
    }
}

```

Для генерации нескольких юзеров можно использвать библиотеку Faker.
он позволит генерить юзеров со случайными данными. например разные имена и емейлы.

В нашем случае для нас важен именно конкретный id и email чтобы под этим юзером
можно было бы хводить в систему из локального окружения.

если нужно будет создать wait@app.test юзера который не прошел валидацию по емейлу
то можно бдует создать фиксированное значение токена. чтобы проверять по нему.
например чтобы пройти по ссылки подтверждения регистрации и проверить как это
работает из внешних тестов.

Поэтому здесь и делаем фиксированные данные, чтобы их же забивать во внешние
тесты и по ним ходить в наш API, который будет обращаться к бд.

Пример если будет внешний тест,лезущий в админку или профиль юзера и пытается
удалить из админки другого юзера то надо как минимум проверить не пытается ли
юзер удалить самого себя.



## Консольная команда для загрузки данных из фикстуры в БД

- нет готовой консольной команды для загрузки фикстур в doctrine/data-fixtures
- но в документации показано как это можно сделать.

https://www.doctrine-project.org/projects/doctrine-data-fixtures/en/1.7/how-to/loading-fixtures.html

Есть два класса для загрузки фикстур в БД. Loader - первый из них

- Doctrine\Common\DataFixtures\Loader
- позволяет получить список фикстур из разных каталогов и файлов.
- через него только добавляем, подготавливаем фикстуры для загрузки в БД.


Пример из документации:

### Loading fixtures
To load a fixture, you can call Loader::addFixture():

```php
<?php

use Doctrine\Common\DataFixtures\Loader;
use MyDataFixtures\UserDataLoader;

$loader = new Loader();
$loader->addFixture(new UserDataLoader());
```

It is also possible to load a fixture by providing its path:

```php
<?php
$loader->loadFromFile('/path/to/MyDataFixtures/MyFixture1.php');
```

If you have many fixtures, this can get old pretty fast,
and you might want to load a whole directory of fixtures
instead of making one call per fixture.

То есть можно загрузить одним махом все фикстуры из целой директории

```php
<?php
$loader->loadFromDirectory('/path/to/MyDataFixtures');
```

You can get the added fixtures using the getFixtures() method:
Для получения списка фикстур которые передали выше

```php
<?php
$fixtures = $loader->getFixtures();
```

### Executing fixtures

Непосредтсвенно загрузка фикстур в саму БД

To load the fixtures in your data store, you need to execute them.
This is when you need to pick different classes depending on the type of store
you are using. For example, if you are using ORM, you should do the following:

```php
<?php
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

$executor = new ORMExecutor($entityManager, new ORMPurger());
$executor->execute($loader->getFixtures());
```

- как и везде здесь передаётся EntityManager настроенный на работу с нашей БД

- `ORMPurger` - обьект занимающийся очисткой дб от всех остальных данных

Когда применяются Фикстуры
- сначала очищается всё содержимое из бд
- накатываются в бд сами фикстуры

но можно подгружать новые фикстыры не затерая и не удаляя старые значения.


Each executor class provided by this package
comes with a purger class that will be used to empty your database
unless you explicitly disable it.
(Здесь о том, что по умолчанию старые данные удаляются перед накаткой фикстур)

If you want to append the fixtures instead of purging before loading
then pass append: true to the execute() method:

Если нужно только добавить и не удалять старое то делается это так:
```php
<?php
$executor->execute($loader->getFixtures(), append: true);
```

By default the ORMExecutor will wrap the purge and the load of fixtures
in a single transaction, which is the recommended way,
but in some cases
(for example if loading your fixtures is too slow and causes timeouts)
you may want to wrap the purge and the load of every fixture in
its own transaction. To do so, you can use MultipleTransactionORMExecutor.

```php
<?php
$executor = new MultipleTransactionORMExecutor($entityManager, new ORMPurger());
```

Пишем свою консольную команду для загрузки указанных фикстур в бд.
./api/src/Console/FixturesLoadCommand.php

регистрируем эту консольную команду в dev-окружении
./api/config/dev/console.php

```php
<?php

declare(strict_types=1);

use App\Console;
use App\Console\FixturesLoadCommand;
use Doctrine\Migrations;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Command;
use Doctrine\ORM\Tools\Console\Command\SchemaTool;
use Psr\Container\ContainerInterface;

return [
    // для DI-контейнера то как собирать обьект этого класса
    // нужно для передачи EntityManager и каталог фикстур в саму команду
    FixturesLoadCommand::class => static function (ContainerInterface $c) {
        /**
         * @psalm-suppress MixedArrayAccess
         * @psalm-var array{fixture_paths:string[]} $config
         */
        $config = $c->get('config')['console'];

        /** @var EntityManagerInterface $em */
        $em = $c->get(EntityManagerInterface::class);

        return new FixturesLoadCommand($em, $config['fixture_paths']);
    },

    'config' => [
        'console' => [
            'commands' => [
                Console\FixturesLoadCommand::class, // новая команда под фикстуры

                SchemaTool\DropCommand::class,

                Migrations\Tools\Console\Command\DiffCommand::class,
                Migrations\Tools\Console\Command\GenerateCommand::class,
            ],

            // перечисляем каталоги со своими фикстурами:
            'fixture_paths' => [
                __DIR__ . '/../../src/Auth/Fixture',
            ],
        ],
    ],
];
```


```sh
docker compose run --rm api-php-cli composer app
```

Убеждаемся что команда добавлена и видна в списке доступных:

```
 fixtures
  fixtures:load          Load fixtures
```

```sh
docker compose run --rm api-php-cli composer app fixtures:load
```

```
> php bin/app.php --ansi 'fixtures:load'
Loading fixtures
purging database                          << строка из нашего логера(очистил бд)
loading App\Auth\Fixture\UserFixture      << имя подгруженной фикстуры
Done.
```

Смотрим в саму бд через консоль
```sh
PGPASSWORD=secret psql -h localhost -p 54321 -U app -d app
select * from auth_users;
```

Получить все строки в виде Json-а
```sh
select row_to_json(auth_users) as json from auth_users;
```
```json
{
    "id":"00000000-0000-0000-0000-000000000001",
    "date":"2023-11-05T17:48:02",
    "email":"user@app.test",
    "status":"active",
    "password_hash":"$2y$12$qwnND33o8DGWvFoepotSju7eTAQ6gzLD/zy6W8NCVtiHPbkybz.w6",
    "new_email":null,
    "role":"user",
    "join_confirm_token_value":null,
    "join_confirm_token_expires":null,
    "password_reset_token_value":null,
    "password_reset_token_expires":null,
    "new_email_token_value":null,
    "new_email_token_expires":null
}
```

Добавляем команду в Makefile для того чтобы после накатывания миграций
накатывалась и фикстура, добавляющего прописанного нами юзера user@app.test

```Makefile
api-init: api-permissions api-composer-install api-wait-db api-migrations api-fixtures
...

api-migrations:
	docker compose run --rm api-php-cli composer app migrations:migrate

api-migrations:
	docker compose run --rm api-php-cli composer app fixtures:load
```

Теперь команда `make init`:
Позволит поднять и полностью подготовить всё локальное dev-окружение из любого
состояни до полностью готового последнего актуального состояния, причем с
подготовленной и заполненной нужными данными локальной БД.


- ход выполнения make init:

docker-down-clear:
   - удалит и остановит все контейнеры,
   - удалит все сети(network-и)
   - удалит все волюмы т.к. есть флаг -v (это полностью очистит содержимое БД)

docker-down-clear:
docker compose down -v --remove-orphans
                     ^удалять volume (на нём хранится состояние нашей БД)

api-clear
- очищает все локи и весь кэш:

```sh
docker run --rm -v ~/dev0/php/auction/api:/app -w /app alpine sh -c 'rm -rf var/*'
```

docker-pull - проверяет и если надо вытягивает новые образы
docker-build - проверяет и пересобирает обновлённые докер-образы
docker-up    - поднимает из свежих образов свежие контейнеры
api-init:
 - права доступа к каталогу var
 - запуск установки зависимостей через композер
   `docker compose run --rm api-php-cli composer install`
 - стартанул запуск ожидания готовности БД
   `docker compose run --rm api-php-cli wait4open -t 30 api-postgres:5432`
 - запуск миграций
   `docker compose run --rm api-php-cli composer app migrations:migrate`
 - запуск фикстур
   `docker compose run --rm api-php-cli composer app fixtures:load`


Зачем постоянно затерать бд-таблицы постоянно перезатерая и пересоздавая данные?
- если фикстуры не используются, в проекте несколько программистов работающих
на разных машинах возможна ситуация:
- один программист делающий регистрацию, заполнил только таблицу регистрации
- другой делающий блок на сайте, заполнил в админке статьи в блоге
- третий заполнил лоты в аукционе для своей работы
получается у каждого программиста свои данные в их локальных базах данных
и поделится этими данными с другими участниками команды можно разве что через
пересылку дампов бд через месенжеры или эл.почту. Это может быть не удобно.
Использование фикстур позволяет каждому программисту работающему над своим
функционалом заполнить фикстуру для создания данных в бд нужных для его работы.
Ну а так как фикстуры в проекте по сути это просто php-классы и их можно под
систему контроля версий и в общий репозиторий то у каждого из программиста
команды в итоге будет такие же данные в бд.
То есть фикстуры подобны миграциям, но только миграции изменяют саму бд под
модели данные, тогда как фикстуры позволяют заполнять бд данными для разработки.

Еще одно достоинство фикстур
Можно заполнять бд данными в том числе и на сервере именно для его тестирования.
Так для разных видов тестов именно через фикстуры бд заполняется данными, нужными
для конкретного типа тестов. например для функционального.
либо полностью заплнить бд всеми необходимыми для приёмочного теста данными,
когда взаимодействие с нашим api идёт снаружи.

Фикстыры - это удобный и быстрый способ передавать и обмениваться данными для БД.
используя фикстуры не нужно обмениваться дампами данных из бд.


Если резюмировать то в общем всё что нужно для работы с БД это:
- подключить некую БД к проекту, например PostgreSQL или MySQL
- написать миграции для создания таблиц
- написать фикстуры для заполнения таблиц dev/test-данными


На данном этапе вся необходимая работа с БД реализована
осталось:
- реализовать отправку токенов на email
для этого нужно подключить мейлер и эмулятор отправки писем.
