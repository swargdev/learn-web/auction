﻿## Устранение проблем с работой и сборкой образов

при настройке xdebug-а и пересборке всего дев-окружения make init  с
пересозданием своих докер-образов столкнулся с такой проблемой.
make init проходит успешно никаких ошибок не показывает, но и окружение
не запускается `docker ps` не показывает никаких запущенных контейнеров.

Как выяснилось контейнеры запускались но один из них api-php-fpm падал в
переопределённом мной entrypoint.sh скрипте и из-за этого докер компоуз валил
всю связку локальных дев-образов.
Визуально никаких ошибок при этом не выводится. можно посмотреть лог самого
докер-демона:

```sh
sudo journalctl -u docker
```
```
Sep 13 09:29:09 xdeb11 dockerd[172177]: time="2023-09-13T09:29:09.445598248+03:00" level=error msg="Error setting up exec command in container site-api-php-fpm-1: Container 38f19e6e95b8832d6d0c1469d6263739631c783049d70f8b967fadafe7c6beed is not running"
```


В основном Makefile в таргите build прописана сборка только для прод-образов
дев-образы собираются по данным которые прописаны в docker-compose.yml.

этот файл используется для локальной разработки т.е. для dev-а.
Так чтобы собрать руками один из нужных dev-образов можно вот так:

```sh
docker --log-level=debug build --file=api/docker/development/php-fpm/Dockerfile \
  --tag=auction-api-php-fpm api/docker
```
- `api/docker` - каталог из которого собирать образ
полное имя образа будет назначено как `docker.io/library/auction-api-php-fpm`
т.е. в tag-e укано только базовое имя без полного пути с репозиторием.
по умолчанию берётся докеровский репозиторий `docker.io/library`

Смотрим не сбился ли ENTRYPOINT и СMD
```sh
docker images inspect <image-has-id> | grep Cmd -A 8
```
```
"Cmd": ["php-fpm"],
"Image": "",
"Volumes": null,
"WorkingDir": "/app",
"Entrypoint": ["docker-php-entrypoint"],
```

Наблюдения:
Если в entrypoint.sh скрипте одна из команд "падает" то завершается выполнение
как самого этого скрипта, так и запуск контейнера тоже "падает".
Так например если использовать в скрипите
```sh
ping -q -c1 $HOST_DOMAIN > /dev/null 2>&1       # пингуем есть ли такой хост
if [ $? -ne 0 ]; then                           # если не пингуется тогда:
# ...
```
То в итогде контейнер api-php-fpm упадёт из-за ошибки
```
ping: bad address 'host.docker.internal'
```
при этом если это запускать через `make init` то это будет запускаться через
docker-compose.yml т.е. докер компоузером который ведёт себя так:
- когда падает один из контейнеров(сервис) в стаке (одном файле docker-compose.yml)
  тогда завершается все контейнеры этой "связки"(стака)

поэтому если в entrypoint скрипте ошибка - то dev-окружение тупо не развернётся
и никаких сообщений об ошибках не будет.

Как отловил причину -
раставил echo на строчках в скрипте ./api/docker/development/php-fpm/entrypoint.sh
затем собирал образ api-php-fmp отдельно и отдельно для него запускал контейнер

```sh
docker --log-level=debug build \
  --file=api/docker/development/php-fpm/Dockerfile \
  --tag=auction-api-php-fpm api/docker \
  && docker run --rm auction-api-php-fpm:latest
```

Так же помни что при частых сборках образуются "мусорные образы"

```
REPOSITORY           TAG               IMAGE ID       CREATED          SIZE
auction-api-php-fpm  latest            cb5c27d7b978   8 minutes ago    358MB
<none>               <none>            13b7f584ae5f   9 minutes ago    358MB
<none>               <none>            4f5ad2ab8605   11 minutes ago   358MB
<none>               <none>            e0cf2e20d16d   12 minutes ago   358MB
<none>               <none>            c44440ae05d5   13 minutes ago   358MB
<none>               <none>            3560ebdb3538   14 minutes ago   358MB
```
удалить их руками можно вот так:
```sh
docker rmi 3560ebdb3538 c44440ae05d5 e0cf2e20d16d 4f5ad2ab8605 13b7f584ae5f cb5c27d7b978
```

Посмотреть не удалённые остановленные контейнеры - через флаг -a (покажи всё)
```sh
docker ps -a
```
Без флага -a показывает только запущенные контейнеры

Ошибку исправил таким кодом (ping поместить прямо в if)
```sh
HOST_DOMAIN="host.docker.internal"
if ! ping -q -c1 $HOST_DOMAIN > /dev/null 2>&1; then
  HOST_IP=$(ip route | awk 'NR==1 {print $3}')
  echo -e "$HOST_IP\t$HOST_DOMAIN" >> /etc/hosts
fi
```

Как оказалось всё сломалось из-за одной строчки в этом entrypoint.sh скрипте
ping выдавал код возврата с ошибкой и выполнение скрипта останавливалось на
строчке
```sh
ping -q -c1 $HOST_DOMAIN > /dev/null 2>&1
```
То есть контейнер стартовал, запускался скрипт - скрипт "падал" и падал контейнер
а дальше докер-компоуз увидев это валил и все остальные контейнеры в связке(стаке)

