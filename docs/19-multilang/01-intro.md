- Http-заголовок Accept-Language
- Добавление мультиязычности.

02-translator
- Перевод ошибок валидации для Symfony/Validator
- Symfony/Translator
- подключаем компонент Symfony/Config
- Регистрируем файл с русским переводом
- Добавляем Symfony/Translator в Symfony/Validator
- Делаем автоматическое переключение языков на основе Accept-Language
- Создаём Middleware\TranslatorLocale для автоматического переключения Языка
- Приступаем к реализации парсера списка Accept-Language

- Библиотека willdurand/negotiation для парсинга Accept-Language
- Пишем свой Middleware/ContentLanguage на основе willdurand/negotiation


## Добавление мультиязычности.

Задача:
Возможность получать вывод валидации и доменных исключений на разных языках.

При написании простых классических сайтов язык обычно указывают в url
```
http://localhost:8081/ru/auth/join
```
Такой подход должен использоваться на фронтенде, для того чтобы поисковики
могли индексировать разные версии сайта по конкретным языкам.

На API обычно языки в адрес не добавляют. Для этого Используют http заголовки.


## Http-заголовок Accept-Language

```http
POST http://localhost:8081/v1/auth/join
Accept: application/json
Accept-Language: ru
Content-Type: application/json

{
    "email": "new-user@app.test",
    "password": ""
}
```

Так если сервер поддерживаем многоязычность то приняв в заголовке-запросе
`Accept-Language: ru` сервер должен выдать ответ на запрашиваемом языке.

В заголовке Accept-Language можно передавать не просто язык, а целый
список да еще и с индексами приоритетов:

`Accept-Language: ru_RU;q=0.9, ru;q=0.8, *;q=0.5`
`ru_RU` - код языка
`q` - это приоритет описываемого языка.
`*` - любой другой язык

Таким образом первый шаг для поддержки мультиязычности - уметь парсить такие
Accept-Language запросы с приоритетами. Причем из запрашиваемых языков должны
браться только поддерживаемые нами языки. Чтобы если просят на испанском, а
у нас поддерживается только русский и английский - то выводить на англ.

Опишем спеку в которой запрашивается es-испансикй язык но т.к. наш проект
поддерживает только ru и en то выводить более приоритетный из заданного списка
то есть русский(0.8)

Если язык не указан - должны выводиться на англ.

./api/tests/Functional/V1/Auth/Join/RequestTest.php
```php
<?
    public function testNotValidLang(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join', [
            'email' => 'not-email',
            'password' => '',
        ])->withHeader('Accept-Language', 'es;q=0.9, ru;q=0.8, *;q=0.5'));


        self::assertEquals(422, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        self::assertEquals([
            'errors' => [
                'email' => 'This value is not a valid email address.',
                'password' => 'This value should not be blank.',
            ],
        ], Json::decode($body));
    }
```

