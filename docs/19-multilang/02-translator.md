- Перевод ошибок валидации для Symfony/Validator
- Symfony/Translator
- подключаем компонент Symfony/Config
- Регистрируем файл с русским переводом

- Добавляем Symfony/Translator в Symfony/Validator
- Делаем автоматическое переключение языков на основе Accept-Language
- Создаём Middleware\TranslatorLocale для автоматического переключения Языка
- Приступаем к реализации парсера списка Accept-Language



## Перевод ошибок валидации для Symfony/Validator

В Symfony/Validator есть возможность использовать отдельный компонет для перевода

Зайдём в исходник Symfony/Validator в фабрику его создания
Быстро это сделать можно через свой исходник - через фабрику для DI-контейнера:
./api/config/common/validator.php
```php
<?
return [
    ValidatorInterface::class =>
    static function (ContainerInterface $container): ValidatorInterface {

        return Validation::createValidatorBuilder()
                          // ^^^^^^ goto-definition
    },
];
```

./api/vendor/symfony/validator/Validation.php
```php
<?
namespace Symfony\Component\Validator;
class Validation {
    // ...
    public static function createValidatorBuilder(): ValidatorBuilder
    {
        return new ValidatorBuilder();
        //         ^^^ goto-definition
    }
}

## ....
class ValidatorBuilder {

    private array $initializers = [];
    private array $loaders = [];
    private array $xmlMappings = [];
    // ...
    private ?TranslatorInterface $translator = null;
    //        ^                   ^^^^^^^^^ интересующее нас поле
    //         ` - Symfony\Contracts\Translation\TranslatorInterface
    private ?string $translationDomain = null;
    //...
}
```
То есть в построителе(Билдере) валидатора можно передать интерфейс переводчика,
а в этом самом интерфейсе указан рабочий метод trans - для перевода

./api/vendor/symfony/translation-contracts/TranslatorInterface.php
```php
<?
namespace Symfony\Contracts\Translation;

interface TranslatorInterface
{
    /*
     * $id         The message id (may also be an object that can be cast to string)
     * $parameters An array of parameters for the message
     * $domain     The domain for the message or null to use the default
     * $locale     The locale or null to use the default
     */
    public function trans(
        string $id,               // что нужно перевести
        array $parameters = [],
        string $domain = null,
        string $locale = null
    ): string;

    /** Returns the default locale. */
    public function getLocale(): string;
```

`Symfony/Validator` может использовать любой обьект реализующий этот интерфейс
а значит можно либо написать переводчик самому, либо поставить готовый.
Например в симфони есть Symfony/translation:


## Symfony/Translator

https://github.com/symfony/translation
Forks: 87 Stars: 6.5k

Provides tools to internationalize your application
Как и обычная php-библ-ка подключается через
```sh
composer require symfony/translation
docker compose run --rm api-php-cli composer require symfony/translation
```

```php
<?
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;

$translator = new Translator('fr_FR');
//                            ^^^ язык по умолчанию
$translator->addLoader('array', new ArrayLoader());

//дальше задаём ресурсы по которому и будет происходить перевод
$translator->addResource('array', [
    // ключ
    'Hello World!' => 'Bonjour !',
    // оригинал        перевод
], 'fr_FR');

echo $translator->trans('Hello World!'); // outputs « Bonjour ! »
//  вызываем "перевод" и по умолчанию будет переводить на дефолтный язык
```


Он просто делает маппинг строк по чётко заданному шаблону
т.е. весь перевод надо делать заранее самому.


язык по умолчанию задаётся в конструкторе, но можно сменить язык или указать
его прямо в методе trans()



## установка Symfony/Translator

```
docker compose run --rm api-php-cli composer require symfony/translation
```

```
Cannot use symfony/translation's latest version v7.0.0 as it requires php >=8.2 which is not satisfied by your platform.
./composer.json has been updated
Running composer update symfony/translation
Loading composer repositories with package information
Updating dependencies
Lock file operations: 1 install, 0 updates, 0 removals
  - Locking symfony/translation (v6.4.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
  - Downloading symfony/translation (v6.4.0)
  - Installing symfony/translation (v6.4.0): Extracting archive
Generating autoload files
No security vulnerability advisories found.
Using version ^6.4 for symfony/translation


api/vendor/symfony/translation   744K
api/vendor/                       63M
```

Создаём конфиг-фабрику для DI-контейнера
./api/config/common/translator.php
```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Contracts\Translation\TranslatorInterface;

return [
    TranslatorInterface::class =>
    static function (ContainerInterface $container): Translator {
        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{lang:string,resources:array<string[]>} $config
         */
        $config = $container->get('config')['translator'];

        $translator = new Translator($config['lang']);

        return $translator;
    },


    'config' => [
        'translator' => [
            'lang' => 'en', // язык по умолчанию
        ],
    ],
];
```

Это минимальный код по созданию переводчика. Дальше надо добавить ресурсы
по которым и будет идти перевод.
А значит если хотим подключить к переводчику наш валидатор, чтобы все сообщения
из валидации могли переводится нужно подключать список со всеми возможными
переводами.

С `Symfony/Validator` преводы уже идут из коробки (57 языковых файла на 1.6Mb):
1.6M	api/vendor/symfony/validator/Resources/translations/

И эти переводы можно подключить к `Symfony/Translator`

переводы сделаны в формате xlf - это своего рода xml

Таким образом если нам нужны переводы сообщений валидации то можно просто
добавить готовый файл:
./api/vendor/symfony/validator/Resources/translations/validators.ru.xlf
```html
    <trans-unit id="1">
        <source>This value should be false.</source>
        <target>Значение должно быть ложным.</target>
    </trans-unit>
```

Так же есть поддержка подстановки параметров `{{ name }}`
```html
    <trans-unit id="3">
        <source>This value should be of type {{ type }}.</source>
        <target>Тип значения должен быть {{ type }}.</target>
    </trans-unit>
```
Именно они и используются для подстановки в результирующую строку перевода при
вызове метода trans


## Подключаем компонент Symfony/Config

Для подключения xlm нужен компонент `Symfony/Config`
этот компонет позволит читать xlm формат, который нужен для `Symfony/Translator`

```sh
docker compose run --rm api-php-cli composer require symfony/config

Cannot use symfony/config's latest version v7.0.0 as it requires php >=8.2 which is not satisfied by your platform.
./composer.json has been updated
Running composer update symfony/config
Loading composer repositories with package information
Updating dependencies
Lock file operations: 1 install, 0 updates, 0 removals
  - Locking symfony/config (v6.4.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
  - Downloading symfony/config (v6.4.0)
  - Installing symfony/config (v6.4.0): Extracting archive
Generating autoload files
No security vulnerability advisories found.
Using version ^6.4 for symfony/config


api/vendor/symfony/config/  536K
api/vendor/                  64M
```

## Регистрируем файл с русским переводом

Теперь добавляем загрузчики и прописываем откуда брать файлы переводов.
```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Symfony\Component\Translation\Loader\PhpFileLoader;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Contracts\Translation\TranslatorInterface;

$symfony_validator_res = __DIR__ . '/../../vendor/symfony/validator/Resources/';

return [
    TranslatorInterface::class =>
    static function (ContainerInterface $container): Translator {
        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{lang:string,resources:array<string[]>} $config
         */
        $config = $container->get('config')['translator'];

        $translator = new Translator($config['lang']);
        $translator->addLoader('php', new PhpFileLoader()); // для php-массивов
        $translator->addLoader('xlf', new XliffFileLoader()); // для xlm

        foreach ($config['resources'] as $resource) {
            $translator->addResource(...$resource);
        }

        return $translator;
    },

    config => ... //см ниже
];
```

Вообще переводы для Symfony/Translator можно писать и в виде простых ассоциативных
php-массивов и складывать их в простом конфиг-файле(php-шном). Для этого лоадер:

        $translator->addLoader('php', new PhpFileLoader());

Смотрим на метод addResource
```php
<?
namespace Symfony\Component\Translation;
class Translator implements TranslatorInterface {
    /**
     * Adds a Resource.
     * @throws InvalidArgumentException If the locale contains invalid characters
     */
    public function addResource(
        string $format,  // The name of the loader (@see addLoader())
        mixed $resource,  // The resource name - путь к ресурсу откуда читать
        string $locale,   // локаль - код языка
        string $domain = null // имя категории этого ресурса
    ) {}
```

domain - Задаёт своего рода категорию для ресурса перевода. чтобы можно было
переводы держать не все в одной куче а раздельно.
Например переводы для валидации в домене поместь в validations, а
переводы для доменных исключений в exceptions и доставть для исключений переводы
только из конкретной категории(экономит время поиска на перевод и позволяет
избегать конфликтов для одинаковых ключей)



```php
<?
return [
    TranslatorInterface::class => {},
    // ..
    'config' => [
        'translator' => [
            'lang' => 'en',
            'resources' => [
                [
                    'xlf',        // format
                    $symfony_validator_res . 'translations/validators.ru.xlf',
                    // ^ resource полное имя файл откуда грузить ресурс
                    'ru',         // locale язык на который переводит данный рес
                    'validators'  // domain "имя категории" куда этот ресурс
                                  //добавлять
                ],
            ],
        ],
    ],
];
```


```php
<?
    foreach ($config['resources'] as $resource) {
        $translator->addResource(...$resource);
       //                        ^^^ просто "распаковывает" массив подставляя
       // все элементы в соответствующие по порядку агрументы для передачи в
       // метод
    }
```


## Добавляем Symfony/Translator в Symfony/Validator

```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

return [
    ValidatorInterface::class =>
    static function (ContainerInterface $container): ValidatorInterface {
        /** @var TranslatorInterface $translator */
        $translator = $container->get(TranslatorInterface::class);

        return Validation::createValidatorBuilder()
            ->enableAttributeMapping()
            ->setTranslator($translator)         // прокидываем переводчик
            ->setTranslationDomain('validators') // имя категории откуда брать
            ->getValidator();                    // переводы
    },
];
```

Пробуем запросить на русском
```sh
curl -X POST http://localhost:8081/v1/auth/join \
-H "Accept: application/json" \
-H "Content-Type: application/json" \
-H "Accept-Language: ru" \
-d '{"email": "new-userapp.test", "password": "" }' | jq
```
это не сработает пока руками в конфиге ./api/config/common/translator.php
не перебить язык по умолчанию на русский:

```php
<?
return [
    'config' => [
        'translator' => [
            // 'lang' => 'en',
            'lang' => 'ru',                # <<<< временно чисто проверить
            'resources' => ..]]];
```

После того как сделал русский язык дефолтным должно переводиться на русский:

```json
{
  "errors": {
    "email": "Значение адреса электронной почты недопустимо.",
    "password": "Значение не должно быть пустым."
  }
}
```

NOTE:
чтобы отформатировать json в удобочитаемый вид используй `curl ... | jq`
без этого русский тектс будет в виде:
`{"errors":{"email":"\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 ..`


## Делаем автоматическое переключение языков на основе Accept-Language

Подумаем как это можено сделать.
Первый способ как можно автоматически переключать языки:

прямо в контроллере например в нашем `Join/RequestAction`
- сначала передвать в конструктор через DI-контейнер и затем
  сохранять в приватное поле обьект переводчика (Symfony/Translator)
- достать из request заголовок Accept-Language понять какой там язык просят
- и перед вызовом самой валидации сменить локаль в переводчике на нужную
- запустить метод валидации.

Но как мы уже понимаем делать это прямо в конструкторах нет смысла,
когда есть возможность написать свой middleware.
(Middleware-Посредник работающий в цепочке вызовов до вызова самого контроллера)


## Создаём Middleware\TranslatorLocale для автоматического переключения Языка

Задача данного миддлвера будет на основе пришедшего заголовка Accept-Language
переключать локаль(язык) до вызова самого конструктора.

Вообще мы уже обозначили, что в `Accept-Language` может быть не просто язык, а
целый список предпочтений, который к тому же нужно еще парсить, чтобы получить
из него список языков. А затем либо выбрать самый приоритетный либо вернуть
дефолтный поддерживаемый. Если среди запрашиваемых языков нет поддерживаемого.

Но парсить язык из Accept-Language в этом миддлвере мы не будем. т.к. это уже
будет нарушением принципа единой ответственности.
Для самого парсинга языка из заголовка сразу создадим отдельный миддлвер.
Который и будет парсить `Accept-Language` доставая из него нужный поддерживаемый
язык и размещать в этот же заголовок в виде простой строки, которую наш
`Middleware\TranslatorLocale` и будет брать для переключения языка.
Пока займёмся реализацией миддлвера самого переводчика.


```php
<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Translation\Translator;

class TranslatorLocale implements MiddlewareInterface
{
    private Translator $translator;

    // принимаем переводчик именно по реализующему классу а не интерфейсу
    // т.к. через интерфейс доступен только метод trans() и getLocale()
    // а нам нужна возможность настравивать наш переводчик (вызывать setLocale)
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        // здась уже должна быть подготовленная локаль в виде простой строки
        $locale = $request->getHeaderLine('Accept-Language');

        // переключение с дефолтного языка делать только когда запрашивают другой
        if (!empty($locale)) {  // переключение локали на нужный язык
            $this->translator->setLocale($locale);
        }

        return $handler->handle($request);
    }
}
```

Но т.к. в этом миддлвере переводчик нам нужен именно по классу а не по интерфесу
то нужно будет подкорректировать фабрику DI-контейнера.
Так чтобы можно было получить переводчик и по его классу и по интерфесу.

Делается это через добавление псевдонима (интерфейс=>класс, класс =>):

```php
<?
return [
    TranslatorInterface::class => DI\get(Translator::class), // псевдоним

    Translator::class =>  // был TranslatorInterface
    static function (ContainerInterface $container): Translator {
        // .. фабрика
    },
]
```

Такая запись говрит, кто при запросе `TranslatorInterface` нужно инстанцировать
из класса `Translator`. И уже ниже регаем не интерфейс как было а именно класс.

Это позволит получать один и тот же обьект из DI-контейнера и при запросе из
него и интерфейса `TranslatorInterface` и при запросе класса `Translator`.
То есть пересоздаваться новый инстанс переводчика НЕ будет, а будет
использоваться один и тот же уже созданный инстанс.

Это важно, потому что нам нужно чтобы переводчик во всей нашей системы был один.
И был настроен при этом правильным образом, а не два настроенных по разному.

./api/config/common/translator.php
```php
<?
return [
    TranslatorInterface::class => DI\get(Translator::class),

    Translator::class =>
    static function (ContainerInterface $container): Translator {
        //... тоже самое без изменений
    },

    // делаем фабрику для миддлвера
    TranslatorLocale::class =>
    static function (ContainerInterface $container): Translator {
        /** @var Translator $translator */
        $translator = $container->get(Translator::class);
        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{allowed:string[]} $config
         */
        $config = $container->get('config')['locales'];
        // $config['allowed']);

        return new TranslatorLocale($translator);
    },

    'config' => [
        'translator' => [
            // тоже самое без измнений
        ],
        'locales' => [ // добавляем новое - разрешенные языки
            'allowed' => ['en', 'ru'], // поддерживаемый нашим приложением
            // по умолчанию будет использоваться 1й т.е. en - англ
        ],
    ],
];
```

Хотя у нас пока что нет парсинга списка языков из Accept-Language, но если
в Accept-Language указан просто язык, то сообщения валидации переводчик уже
сможет переводить.

Прописываем новый миддлвер

./api/config/middleware.php
```php
<?
use App\Http\Middleware;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;

return static function (App $app): void {
    $app->add(Middleware\DomainExceptionHandler::class);
    $app->add(Middleware\ValidationMiddleware::class);
    $app->add(Middleware\ClearEmptyInput::class);
    $app->add(Middleware\TranslatorLocale::class);     // << регистрируем новый
    $app->addBodyParsingMiddleware();
    $app->add(ErrorMiddleware::class);
};
```


В юнит тесте для этого мидлвера будет два случая
- переключение языка не запрашивается миддлвер ничего не делает
- идёт запрос на переключение - переключить локаль и передать управление дальше

./api/src/Http/Test/Unit/Middleware/TranslatorLocaleTest.php
```php
<?
namespace App\Http\Test\Unit\Middleware;

use App\Http\Middleware\TranslatorLocale;
use App\Http\Test\Unit\WebUTestCase;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Translation\Translator;

class TranslatorLocaleTest extends WebUTestCase
{
    // когда вообще нет заголовка Accept-Language то и пререключения на язык нет
    public function testDefault(): void
    {
        $translator = $this->createMock(Translator::class);
        // не должно быть никакого переключения - работа с языком по умолчанию
        $translator->expects(self::never())->method('setLocale');

        $middleware = new TranslatorLocale($translator);

        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn($source = self::createResponse());

        $response = $middleware->process(self::createRequest(), $handler);

        self::assertEquals($source, $response);
    }

    public function testAccepted(): void
    {
        // вся проверка здесь внутри мока-заглушки
        $translator = $this->createMock(Translator::class);
        // говорим что должно произовйти переключение на ru-локаль
        $translator->expects(self::once())->method('setLocale')->with(
            self::equalTo('ru') // значение первого аргумента при вызове метода
        );                      // setLocale

        $middleware = new TranslatorLocale($translator);

        // Заглушка контроллера
        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn(self::createResponse());

        // эмулируем запрос с запросом на русский язык
        $request = self::createRequest()->withHeader('Accept-Language', 'ru');

        // передача управления - внутри миддлвер должен будет вызвать
        // Symfony/Translator.setLocale('ru') для переключение на русский язык
        $middleware->process($request, $handler);
    }
}
```

Временно упростим функциональный тест где идёт проверка сообщений валидации,
заменив список языков на просто язык и проверим работу нового миддлвера:
```php
<? class RequestTest extends WebTestCase {

    public function testNotValidLang(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join', [
            'email' => 'not-email',
            'password' => '',
        // ])->withHeader('Accept-Language', 'es;q=0.9, ru;q=0.8, *;q=0.5'));
        ])->withHeader('Accept-Language', 'ru')); // << просто язык а не список

        self::assertEquals(422, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        self::assertEquals([
            'errors' => [
                'email' => 'Значение адреса электронной почты недопустимо.',
                'password' => 'Значение не должно быть пустым.',
            ],
        ], Json::decode($body));
    }
```
Функциональный тест прохидит - перевод сообщений валидации работает.


## Приступаем к реализации парсера списка Accept-Language

Теперь пора реализовать парсинг заголовка Accept-Language на получение
одного из поддерживаемых языков из заданного спика.


`config.locales.allowed` - это список поддерживаемых нашим приложением языков.

```php
<?
return [
    // фабрики Translator TranslatorInterface и Middleware\TranslatorLocale

    'config' => [
        'translator' => [
            // тоже самое без измнений
        ],
        // новое:
        'locales' => [ // языки
            'allowed' => ['en', 'ru'], // поддерживаемые нашим приложением
            // по умолчанию будет использоваться 1й т.е. en - англ
        ],
    ],
];
```
