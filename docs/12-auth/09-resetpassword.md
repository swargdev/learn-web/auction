##  Сброс пароля пользователя ResetPassword (v16)

Reset Password (Сброс пароля) - полезен когда пользователь забыл свой пароль и
хочет его сбросить, заменив на новый.
- Он запрашивает сброс, вбивая свой email,
- ему на введённый им email отправляется ссылка на форму,
  где нужно вести новый пароль(В этой ссылке будет вшит токен сброса пароля)
- дальше в форме ввода нового пароля, открывшейся по ссылке из письма,
  юзер вводит новый пароль и отправляет его на сайт где и происходит замена
  старого пароля на новый (точнее на хэш нового пароля) При условии что токен
  валиден и срок его действия еще не истёк

Продумываем как будем реализовывать.

UseCase(Command) Из двух шагов.
```
Reset Password            - сброс пароля
  Request                    1й шаг(эпат)
    email + token
  Reset                   - 2й шаг -по переходу по токену смена пароля на новый
```
Шаги сброса пароля:
1й шаг: запрос сброса по емейлу:
- есть форма сброса пароля где нужно указать свой email под которым зареган
- система по е-мейлу должна найти юзера в БД
- если для такого е-мейла юзер существует - отправить ему на почту токен
для сброса пароля(ссылка перейдя по которой можно будет ввести новый пароль)

2й шаг переход по токену сброса пароля
- отослать форму ввода нового пароля и его подтверждения


## При реализации главное учесть необходимость защиты от атак и спама

Если не сделать защиту, то атакующий сможет спамить через наш сайт
А это в конечном итоге приведёт к блокировке технического адреса эл почты сайта
различными почтовыми службами.


## Реализуем UseCase Сброса пароля (ResetPassword)

как обычно в каталоге [команд](.api/src/Auth/Command/ResetPassword/)
создаём уже привычную структуру - "команда-обёртка над данными" + обработчик:

```
api/src/Auth/Command/ResetPassword/
├── Request           1й шаг
│   ├── Command.php   передача email-а куда надо отправить токен сброса пароля
│   └── Handler.php   обработчик отправляющий письмо с токеном на почту
└── Reset             2й шаг
    ├── Command.php
    └── Handler.php
```

## Реализация первого шага ResetPassword\Request - Запрос на сброс пароля

логика ResetPassword\Request\Handler
- пришел адрес почты куда надо отправить токен сброса пароля
- строку адреса преобразуем в обьект Email - с валидацией адреса почты


Кстати валидацию для почтового адреса в классе Email:
```php
<?
//class Email
  if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
      throw new \InvalidArgumentException('Incorrect email.');
  }
```
можно в новой либе Webmozart заменить на более короткую

```php
<?
  Assert::email($value); // use Webmozart\Assert\Assert;
  // причем внутри под капотом там тоже самое - через filter_var см исходники
```

Реализацию начинам "сверху" - с обработчика

```php
<?
class Handler // App\Auth\Command\ResetPassword\Request\Handler
{
    private UserRepository $users;
    private Tokenizer $tokenizer;
    private Flusher $flusher;
    private PasswordResetTokenSender $sender;  //пока просто класс-заглушка

    public function __construct(
        UserRepository $users,
        Tokenizer $tokenizer,
        Flusher $flusher,
        PasswordResetTokenSender $sender     // пока это просто класс-загрушка
    ) {                                      // с одним пустым методом send()
        $this->users = $users;
        $this->tokenizer = $tokenizer;
        $this->flusher = $flusher;
        $this->sender = $sender;
    }

    public function handle(Command $command): void
    {
        $email = new Email($command->email);      // внутри встроена валидация

        $user = $this->users->getByEmail($email); // найти из БД по адресу почты
        // getByEmail - это новый метод, который добавляем с UserRepository
        // если юзера с таким емейлом нет - кинет исключение DomainException

        $date = new DateTimeImmutable();
        // теперь найдя нужного юзера нужно изменить его состояние для этого
        // вызываем метод переключающий состояние обьекта User
        // причем в этом методе должны быть проверки для защиты от спама
        $user->requestPasswordReset(
            $token = $this->tokenizer->generate($date),
            $date
        );

        $this->flusher->flush();                   // сохранение состояния в БД

        $this->sender->send($email, $token);       // отправка письма с токеном
    }
}
```

Важно сделать защиту от попыток использовать сайт для спама, когда атакующий
указывает некий email и постоянно запрашивая сброс пароля вызывает многократную
отправку писем на указанный им адрес. Если такого не сделать то почтовые системы
могут добавить наш технический адрес почты в чёрный список спама.
Защита от этого - сделать так, чтобы на один и тот же адрес нельзя было
запрашивать письма c токеном сброса пароля слишком часто.
А именно, не чаще времени действия токена сброса пароля (скажем 1 час)

        $this->sender->send($email, $token);

Здесь у нас отправка письма идёт в синхронном режиме прямо в коде обработчика.
Т.к. пока что у нас нет никаких доменных событий.
Дальше еще будем вводить использование событий. перепишем методы и тогда
операции, подобные отправке писем, можно будет делать уже изнутри слушателей
событий, а не синхронно, напрямую внутри кода хэндлеров.

Алгоритм разработки такой же
- сначала пишем методы которые нам нужны "сверху" начиная с обработчика
- затем пишем тесты для этого нового метода, продумывая его поведение
- затем продумав и описав в тестах все возможные случаи его использования
  переходим к написанию самого метода

здесь такой новый метод это `requestPasswordReset()`
его вводим первоначально внутри хэндлера, затем пишем под него тесты:
./api/src/Auth/Test/Unit/Entity/User/User/ResetPassword/RequestTest.php

Причем новый хэндлер сброса пароля вручную пока что проверить никак не можем
т.к. еще не подключена ни БД, да и сами контроллеры еще не написаны.
И вообще, проверить работу нашего нового кода, эмулируя запросы к нашему
приложению мы пока, на данном этапе вообще никак не можем.
единственное что можно сделать на данном этапе - написать юнит-тесты для этого
нового метода `requestPasswordReset`.

Причем обрати внимание. Опять идёт разделение на подкаталоги внутри Auth/Test/:
тест проверки нашего нового метода размещаем сюда:
./api/src/Auth/Test/Unit/Entity/User/User/ResetPassword/RequestTest.php
тем самым поддерживая чёткую и понятную структуру тестом.
и разбивая различного род проверки разных частей одного и того-же класса User
на разные тест-файлы.
т.е все тесты для класса `App\Auth\Entity\User\User` размещаются не в одном
огромном файле `App\Auth\Test\Unit\Entity\User\User.php`, а создаётся вместо
файла UserTest.php каталог User и в нём подкаталоги по тематике:
`App\Auth\Test\Unit\Entity\User\User\ResetPassword\RequestTest.php`

И в этом тестовом файле мы описываем все возможные случаи использования
нового еще не написанного метода `requestPasswordReset` правильные и неправильные.

```php
<?php
namespace App\Auth\Test\Unit\Entity\User\User\ResetRequest;
// use ...
class RequestTest extends TestCase
{
    // проверка того что передаваемый токен реально передаётся и хранится в User
    public function testSuccess(): void
    {
        $user = (new UserBuilder())->active()->build();

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 hour'));

        $user->requestPasswordReset($token, $now);

        self::assertNotNull($user->getPasswordResetToken());
        self::assertEquals($token, $user->getPasswordResetToken());
        /* по сути вот эти геттеры getPasswordResetToken пишутся чисто для тестов
         т.к. пока что мы работаем напрямую без событий
         когда же перейдём на использование событий, которые будут генерятся
         внутри User то проверка будет идти по сгенереным событиям а не через
         геттеры */
    }

    // остальные методы - это проверка тонкостей работы метода и возможное
    // пеправильное его использование

    // что будет если метод вызвать несколько раз
    public function testAlready(): void
    {
        $user = (new UserBuilder())->active()->build();

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 hour'));

        $user->requestPasswordReset($token, $now);

        // здесь должна возникнуть ошибка - запрос на сброс пароля уже отправлен
        $this->expectExceptionMessage('Resetting is already requested.');
        $user->requestPasswordReset($token, $now);
    }

    public function testExpired(): void
    {
        $user = (new UserBuilder())->active()->build();

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 hour'));
        $user->requestPasswordReset($token, $now);

        // время жизни токена - 1 час. создав новый токен он должен подхватиться
        $newDate = $now->modify('+2 hours');
        $newToken = $this->createToken($newDate->modify('+1 hour'));
        $user->requestPasswordReset($newToken, $newDate);

        self::assertEquals($newToken, $user->getPasswordResetToken());
    }

    // попытка регистрации по чужому емейлу с последующим запросом на сброс пароля
    // т.е. попытка спама на чужой или не существующий email
    public function testNotActive(): void
    {
        $user = (new UserBuilder())->build(); //  не подтверждённый - не активный
        // т.е. юзер еще не прошел подтверждение регистрации

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 hour'));

        // ошибка защищающая от попыток сбросить пароль еще не активированному юзеру
        $this->expectExceptionMessage('User is not active.');
        $user->requestPasswordReset($token, $now);
    }

    // простой хэлпер для более удобного создания токена со сроком истечения
    private function createToken(DateTimeImmutable $date): Token
    {
        return new Token(Uuid::uuid4()->toString(), $date);
    }
}
```

Когда пишем тесты - продумываем все возможные варианты использования метода
как он должен и не должен работать и как его могут пытаться ломать.
Написав все эти тесты идём писать сам метод под уже продуманное поведение


```php
<?
class User {
    public function requestPasswordReset(Token $token, DateTimeImmutable $date): void
    {
        if (!$this->isActive()) {        // только для подтверживших свой email
            throw new DomainException("User is not active.");   // иначе ошибка
        }
        // если уже запрошен токен сброса пароля и этот токен еще не истёк
        if (
            $this->passwordResetToken !== null &&
            !$this->passwordResetToken->isExpiredTo($date)
        ) {  // тогда инициируем ошибку - защита от частого запроса - спама
            throw new DomainException('Resetting is already requested.');
        }
        // если юзер подтверждён и токена либо еще нет либо уже просрочен
        $this->passwordResetToken = $token;
    }
```
// у нас метод `Token.isExpiredTo` был изначально приватным.
Теперь же он должен стать публичным - поэтому для него можно написать тест:

`./api/src/Auth/Test/Unit/Entity/User/Token/ExpiresTest.php`

```php
<?php
namespace App\Auth\Test\Unit\Entity\User\Token;

// use ..
class ExpiresTest extends TestCase
{
    public function testNot(): void
    {
        $token = new Token(
            $value = Uuid::uuid4()->toString(),
            $expires = new DateTimeImmutable() // дата истечения этого токена
        );

        // за одну секунду до истечения - токен еще рабочий - false не исчёк
        self::assertFalse($token->isExpiredTo($expires->modify('-1 secs')));
        // true - токен уже не действителен т.е. срок его действия уже истёк
        self::assertTrue($token->isExpiredTo($expires));
        self::assertTrue($token->isExpiredTo($expires->modify('+1 secs')));
    }
}
```
Убеждаемся что тесты проходят, код корректен как стилистически так и самантически
make test
make check

Таким образом приходим к тому, что простая команда сброса пароля обрасла доп
проверками, кажущимися не опытным чрезмерными и раздувающими код класса User.
Новичкам может показаться что в User уже слишком много кода, со слишком
большим числом проверок - дескоть ненужная избыточность и оверхед внтутри одного
класса User. Но с другой стороны даже если делать все проверки не в User, а в
самом  контроллере, то проверки всё равно делать будет надо.
т.е. просто изменится разположение кода с проверками из User-а в контроллеры.
И кода меньше не будет он просто может быть в другом месте.

В нашем же подхоже мы просто размещаем код с проверками и валидацией как можно
ближе к тем полям где хранятся проверяемые данные. Т.е. проверка значение для
токена - внутри обьекта его хранящего - Token и т.д. Так же и с User - надо
сделать проверку по приватным полям класса User - размещаем проверки внутри
самого этого класса.

В архитектурных принципах разделения ответственности `GRASP`
первый принцип называется `Information Expert` говорит:

- логику работы с данными размещай как можно ближе к самим этим данным.
то есть, если надо проверить поле класса User значит и проверку размещаем
внутри это класса User - в одном из его методов. Так же и с Email Token и т.д.
Если надо проверить валидность адреса почты - размещаем валидацию в сам класс
где хранится этот адрес почты.

Для проверок которым нужды доп. данные снаружи самого класса эти данные либо
можно передавать в проверяющий метод через входной параметр. Либо производить
проверку "выше" вне самого класса сущности(например юзер или токен)
Пример: проверку на истечение срока жизни токена - размещаем внутри токена, а
более продвинутые проверки выносим в User, который работает с обьектом Token

Когда же надо делать проверку с несколькими User-ами или коллекцией юзеров, то
такую проверку мы уже размещаем в наш Хэндлер. - Пример таких проверок - это
проверка в БД на уникальность неких строк, например адреса эл.почты.



## Реализация второго шага - User передаёт токен и новый пароль

Поведение:
- найти по токену смены пароля User-а
- сгенерить хэш нового пароля и записать его в User

Возможные проверки:
- активен ли токен

```
api/src/Auth/Command/ResetPassword/
├── Request           1й шаг (Уже реализовали)
│   ├── Command.php   email
│   └── Handler.php
└── Reset             2й шаг  реализуем
    ├── Command.php   token, password  значение токена и нового пароля
    └── Handler.php
```

./api/src/Auth/Command/ResetPassword/Reset/Command.php
```php
<?
namespace App\Auth\Command\ResetPassword\Reset;
class Command
{
    public string $token = '';
    public string $password = '';
}
```

./api/src/Auth/Command/ResetPassword/Reset/Handler.php
```php
<?
class Handler
{
    private UserRepository $users;
    private PasswordHasher $hasher;
    private Flusher $flusher;

    public function __construct(
        UserRepository $users,
        PasswordHasher $hasher,
        Flusher $flusher,
    ) {
        $this->users = $users;
        $this->hasher = $hasher;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        // для поиска User-а по токену сброса пароля новый метод:
        if (!$user = $this->users->findByPasswordResetToken($command->token)) {
            throw new DomainException('Token is not found.');
        }

        $user->resetPassword( // вызов метода меняющего состояние обьакта User
            $command->token,
            new DateTimeImmutable(),
            $this->hasher->hash($command->password)
        );

        $this->flusher->flush();
    }
}
```
 Опять таки нужный нам новый метод `findByPasswordResetToken` просто прописываем
в пока что интерфейс `UserRepository`:

        public function findByPasswordResetToken(string $token): ?User;

Причем обрати внимание префикс `find` говорит о том что метод либо возвращает
юзера либо null(?User) если такого нет, но исключение не кидает,
тогда как методы с префиксом `get` обязаны вернуть значение либо кинуть
исключение(это просто такая договорённость в именовании методов)

в нагшем случае мы ищем юзера по токену сброса пароля, если бы мы писали
метод getByPasswordResetToken то кидалось бы исключение "юзер не найден",
тогда как по нашей логике нам здесь нужна проверка валидности токена и
в случае ошибки уже совсем другая ошибка - токен не валиден или не найден.

Если юзер находится по заданному токену - получаем из бд обьект этого User-a
дальше нам надо проверить токен и изменить состояние User-а - на новый пароль
поэтому в метод resetPassword мы и передаём и токен и текущую дату и хэш
нового пароля. Только если токен валиден и еще не истёк только тогда меняем
пароль на новый.

Тонкость в передачи хэша пароля:
- Генерация хэша сделана так чтобы гружить железо - и быть медленной
(защита от брутфорса - грубого перебора пароля по словарю)
у нас же при таком коде сначала идёт генерация хэша пароля а потом проверки
можно назначать новый пароль либо нет.
Сайт с таким кодом можно будет пегружать просто отправкой огромного кол-ва
левых запросов на сброс пароля. Перегрузка будет возможно потому что
даже при невалидном токене будут происходить "тяжелое" хэширование пароля.
- запросить восстановление пароля.
- подождать час или два (срок жизни токена)
- массивно запрашивать сброс пароля по этому токену.

Второй способ защищающий от возможности ложить сайт неправильными запросамы:
```php
<?
    $user->resetPassword( // вызов метода меняющего состояние обьакта User
        $command->token,
        new DateTimeImmutable(),
        $command->password, // сам пароль
        $this->hasher       // обьект доменной службы PasswordHasher
    );
```
здесь мы просто передаём сам пароль и обьект-хэшера который можно бдует просто
вызывать только если проверки проходят и токен валиден. Это исключит возможность
ложить сервер огромным количеством запросов на сброс пароля.

Такую защиту-оптимизацию можно и не делать если у сайта есть Fail2Ban, суть
которого блокировка клиента при слишком частых запросах к сайту.
То есть если у приложения есть некий ограничитель трафика то можно и не писать
доп защиту в самом доменном коде. Если же подобных внешних защит нет - знай
и учитывай что хэширование требует большого кол-ва ресурсов кома и если не сделать
защиту то через него можно будет ложить сайт.
Суть защиты - сначало проверка на валидность и только после неё само тяжелое
вычисление хэша.

До написания самого тела метода пишем тесты:
./api/src/Auth/Test/Unit/Entity/User/User/ResetPassword/ResetTest.php

```php
<?php
namespace App\Auth\Test\Unit\Entity\User\User\ResetRequest;
// use ..
class ResetTest extends TestCase
{
    public function testSuccess(): void
    {
        $user = (new UserBuilder())->active()->build();

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 hour'));
        // сначало запрос на сброс пароля
        $user->requestPasswordReset($token, $now);

        self::assertNotNull($user->getPasswordResetToken());

        // сам сброс пароля на новый "хэш-пароля"
        $user->resetPassword($token->getValue(), $now, $hash = 'hash');

        // проверка что токен сброса обнулён и что хэш пароля обновлён
        self::assertNull($user->getPasswordResetToken());
        self::assertEquals($hash, $user->getPasswordHash());
    }

    // проверка когда пытаются впихнуть левый токен
    public function testInvalidToken(): void
    {
        $user = (new UserBuilder())->active()->build();

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 hour'));

        $user->requestPasswordReset($token, $now);

        // ожидаем ошибку - не правильный токен
        $this->expectExceptionMessage('Token is invalid.');
        $wrongToken = Uuid::uuid4()->toString();
        $user->resetPassword($wrongToken, $now, 'hash');
    }
    // ситуация токен истёк
    public function testExpiredToken(): void
    {
        $user = (new UserBuilder())->active()->build();

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 hour'));

        $user->requestPasswordReset($token, $now);
        // должна возникнуть ошибка - токен истёк
        $this->expectExceptionMessage('Token is expired.');
        $user->resetPassword($token->getValue(), $now->modify('+1 day'), 'hash');
    }
    // попытка вызвать метод resetPassword до запроса на сброс пароля
    public function testNotRequested(): void
    {
        $user = (new UserBuilder())->active()->build();

        $now = new DateTimeImmutable();

        $this->expectExceptionMessage('Resetting is not requested.');
        $user->resetPassword(Uuid::uuid4()->toString(), $now, 'hash');
    }

    private function createToken(DateTimeImmutable $date): Token
    {
        return new Token(Uuid::uuid4()->toString(), $date);
    }
}
```

Сам метод обновления хэша пароля внутри класса User

```php
<?
class User {
    public function resetPassword(
        string $token,
        DateTimeImmutable $date,
        string $hash // хэш уже сгенеренного нового пароля
    ): void {
        // сначала валидация что запрос на сброс уже был, если нет - ошибка
        if ($this->passwordResetToken === null) {
            throw new DomainException('Resetting is not requested.');
        }
        // валидация токена - его значение и время жизни - может кидать иключения
        $this->passwordResetToken->validate($token, $date);
        // здесь будем только если токен прошел проверки и правильный
        $this->passwordResetToken = null;       // убираем токен сброса пароля
        $this->passwordHash = $hash;         // обновляем новое значение пароля
        // this->passwordHash = $hasher->hash($password);
    }
```

Таким образов в классе [User](./api/src/Auth/Entity/User/User.php)

- requestPasswordReset - переключает User-а в состояние запроса нового пароля

- resetPassword - прямая передача и установка хэша нового пароля

таким образом вся бизнес логика работы с токенами находится в этих двух методах
в классе User, тогда как обработчик ResetPassword\Reset\Handler - просто своего
рода обёртка:
- просто принимает команду с данными
- запрашивает у репозитория по этим данным обьект User
- если находит передаёт данные из команды внутрь обьекта User, внутри которого
 и происходят проверки и изменение состояния самого обьекта User
- сохраняет изменения User-обьекта в БД


Реализация казалось бы примитивной операции по восстановлению пароля на практике
оказалась не такой уж и примитивной - разраслась кодом, бизнес-логикой и
проверками на безопасность, защитой от спама и возможных атак на сайт.

Новичкам может показаться что здесь мы написали слишком много "ненужного" кода
внутри класса User. Но опять таки на деле если не размещать код с проверками
внутри юзера его всё равно надо будет размещать в другом месте.


