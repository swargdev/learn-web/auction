## Тесты для React-прилоежния

Договорённость здесь такая:
- сами тесты размещаем в тех же каталогах что и исходники, но просто добавляем
к тестовым файла суффикс `.test.jsx`:


```
frontend/src/
├── App.css
├── App.jsx
├── App.test.jsx                <<<
├── components
│   ├── Welcome.jsx
│   ├── Welcome.test.jsx        <<<
│   └── Welcome.module.css
├── index.css
├── index.jsx
├── reportWebVitals.js
└── setupTests.js
```

Смотрим что там за тесты такие на примере App.test.jsx

./frontend/src/App.test.jsx
```js
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
```



> Документация раздел тестирование
https://create-react-app.dev/docs/running-tests

Create React App uses [Jest](https://jestjs.io/) as its test runner.
To prepare for this integration, we did a major revamp of Jest
so if you heard bad things about it years ago, give it another try.

В общем работает это всё через библ-ку `Jest`

javascript - это браузреный язык, пержде всего расчитанный на работу именно
внутри браузера. Для взаимодействия с ним через такие обьекты как `window`
`document` и проч. `nodejs` появился намного позже и по сути это способ
запускать js вне бразуреа - т.е. из-под консоли. Соответствено запуская js
через консоль т.е. через `nodejs` в ней таких стандартных для работы в браузера
вещей как `window` и `document` просто не будет.
Поэтому для эмуляции работы как буд-то в браузере есть спец библ-ки в частности:

`Jest` is a Node-based runner. This means that the tests always run in a Node
environment and not in a real browser.
This lets us enable fast iteration speed and prevent flakiness.

While Jest provides browser globals such as window thanks to
[jsdom](https://github.com/tmpvar/jsdom)
they are only approximations of the real browser behavior.
Jest is intended to be used for unit tests of your logic and your components
rather than the DOM quirks.

We recommend that you use a separate tool for browser end-to-end tests
if you need them. They are beyond the scope of Create React App.

Дальше в этой доке пишут о том как писать свои тестовые файлы

1й способ - создавать отдельный каталог `__tests__` и в него помещать js-файлы:
2й способ - прямо рядом со своими компонентами размещать файл с суффиксом
либо `.test.js` либо с `.spec.js`

> Filename Conventions

Jest will look for test files with any of the following popular naming conventions:

    Files with .js suffix in __tests__ folders.
    Files with .test.js suffix.
    Files with .spec.js suffix.

The .test.js / .spec.js files (or the `__tests__` folders) can be located at
any depth under the `src` top level folder.

We recommend to put the test files (or `__tests__` folders) next to the code
they are testing so that relative imports appear shorter.
For example, if `App.test.js` and `App.js` are in the same folder,
the test only needs to import App from './App' instead of a long relative path.
Collocation also helps find tests more quickly in larger projects.

При установке CRA в package.json в секцию scripts было добавлена команда
`test: react-scripts test` - это команда для запуска наших тестов

./frontend/src/App.test.jsx
```js
import React from 'react'; // для возможности использовать JSX-синтаксис
import { render, _screen } from '@testing-library/react'; // (RTL)
import App from './App';

test('renders app', () => {
  const { getByText } = render(<App />);
  const h1Element =  getByText(/Auction/i);
  expect(h1Element).toBeInTheDocument();
});
```

`/Auction/i` - это регулярное выражение, `i` - игнорировать регистр

Запускаем тесты
```sh
docker compose run --rm frontend-node-cli yarn test
```
```
 PASS  src/App.test.jsx
  ✓ renders app (34 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        1.469 s
Ran all test suites.

Watch Usage
 › Press f to run only failed tests.
 › Press o to only run tests related to changed files.
 › Press q to quit watch mode.
 › Press p to filter by a filename regex pattern.
 › Press t to filter by a test name regex pattern.
 › Press Enter to trigger a test run.
```
как видим тест он нашел, но не прекратил свою работу, а остался висеть в ожидании
то есть тесты вызываются в интерактивном режиме ожидая пользовательского ввода.
Причем при таком режиме работы идёт слежение за изменениями в файлах проекта
и как только файл для которого есть тесты измненится - автоматически будут
вызываться тесты для этого изменённого файла. Идея в том, чтобы при активной
разработке сразу видить проходят тесты или нет, без ручного их перезапуска
Это удобно для ручной разработке, но не для CI систем и автоматизации

> Запуск тестов без интерактивного режима
```sh
docker compose run --rm frontend-node-cli yarn test --watchAll=false
```
```
yarn run v1.22.19
$ react-scripts test --watchAll=false
 PASS  src/App.test.jsx
  ✓ renders app (36 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        1.595 s
Ran all test suites.
Done in 2.36s.
```
Просто запустил и вышел


## Прописываем алиасы для тестов в Makefile

```js
frontend-test:
	docker compose run --rm frontend-node-cli yarn test --watchAll=false

frontend-test-watch:
	docker compose run --rm frontend-node-cli yarn test
```

whatch - Для локальной разработки когда надо чтобы тесты постоянно сами
запускались в интерактивном режиме

проверяем алиас

```sh
make frontend-test
docker compose run --rm frontend-node-cli yarn test --watchAll=false
yarn run v1.22.19
$ react-scripts test --watchAll=false
 PASS  src/App.test.jsx
  ✓ renders app (37 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        1.706 s
Ran all test suites.
Done in 2.52s.
```

В документации так же много чего интересного есть

[как писать тесты](https://create-react-app.dev/docs/running-tests#writing-tests)

> Writing Tests

To create tests, add `it()` (or `test()`) blocks with the name of the test and
its code. You may optionally wrap them in `describe()` blocks for logical
grouping but this is neither required nor recommended.

Jest provides a built-in `expect()` global function for making assertions.
A basic test could look like this:
```js
import sum from './sum'; // импорт своей проверяемой фунции

// можно вместо it использовать слово test
it('sums numbers', () => { // название теста, аннонимная функция с самой проверкой
  expect(sum(1, 2)).toEqual(3);
  expect(sum(2, 2)).toEqual(4);
});
```

All expect() matchers supported by Jest are extensively documented [here]
(https://jestjs.io/docs/expect#content)


### Как тестировать наши UI-компоненты
https://create-react-app.dev/docs/running-tests#testing-components

пример из доки
```js
import React from 'react';        // импорт самой библ-ки React
import ReactDOM from 'react-dom'; // импорт биб-ки ReactDOM
import App from './App';        // импорт наш UI-Компонент App из файла App.jsx

it('renders without crashing', () => {
  const div = document.createElement('div');
  // здесь идёт стандартный рендеринг через биб-ку React
  ReactDOM.render(<App />, div);
});
```

Еще один способ, используя спец биб-ки React для тестов (RTL):
https://create-react-app.dev/docs/running-tests#react-testing-library

> React Testing Library(RTL)

If you’d like to test components in isolation from the child components they
render, we recommend using `react-testing-library`.
`react-testing-library` is a library for testing React components in a way that
resembles the way the components are used by end users.
It is well suited for unit, integration, and end-to-end testing of React
components and applications.
It works more directly with DOM nodes, and therefore it's recommended to use
with jest-dom for improved assertions.

To install react-testing-library and jest-dom, you can run:


```sh
npm install --save @testing-library/react @testing-library/jest-dom
```
or
```sh
yarn add @testing-library/react @testing-library/jest-dom
```

Причем его нам уже поставило `CRA`(create-react-app)

Пример использования (новый) (React-18):
```js
import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

it('renders welcome message', () => {
  render(<App />);
  expect(screen.getByText('Learn React')).toBeInTheDocument();
});
```

Здесь вместо рендеринга через вызов самого React идёт вызов именно тестовой либы
`@testing-library/react`(RTL)


Тоже самое года два назад (React-16):
```js
import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

it('renders welcome message', () => {
  const { getByText } = render(<App />); // запускаем рендеринг нашего приложения
  // getByText - позволит проверить результат
  expect(getByText('Learn React')).toBeInTheDocument();
  // ищем текст в нашем компоненте
  // "ожидаем", что здананный текст "будет в документе"
});
```


разбираем как это работает на нашем примере UI-Компонент App

в этом тесте идёт проверка того как рендерится наш App
./frontend/src/App.test.jsx
```js
import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders app', () => {
  const { getByText } = render(<App />);
  // getByText - это поиск на отредеренной странице по слову
  const h1Element = getByText(/Auction/i);
  expect(h1Element).toBeInTheDocument();
});
```

это же можно переписать так:
```js
import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders app', () => {
  render(<App />);
  const h1Element = screen.getByText(/Auction/i);
  expect(h1Element).toBeInTheDocument();
});
```

наш компонент App задан вот так,
(внутри у себя он содержит наш UI-компонент `Welcome`)
т.е. в этом тесте будет происходить рендеринг всей нашей страницы (App)
```js
import React from 'react';
import './App.css';
import Welcome from './components/Welcome';

function App() {
  return (
    <div className="app">
      <Welcome />
    </div>
  );
}

export default App;
```

и после этого мы задаём текст который хотим найти для того, чтобы
убедится что он render-ится на html-странице


## Пишем тест для компонента Welcome

./frontend/src/components/Welcome.text.jsx
```js
import React from 'react';
import { render, screen } from '@testing-library/react';
import Welcome from './Welcome';

test('renders app', () => {
  render(<Welcome />);
  const h1Element = screen.getByText(/Auction/i);
  expect(h1Element).toBeInTheDocument();
});
```

По сути проверка наличия того же самого текста, т.к рендеринг слова Auction
у нас прописана именно внутри компонента Welcome. Всё что пока делает App:

```js
function App() {
  return (
    <div className="app">
      <Welcome />
    </div>
  );
}
```
это просто оборачивание вёрстки которую делает компонент `Welcome` в `div` c
css-классом `app`

и здесь мы подходим к необходимости раздельного и независимого тестирования кода
наших UI-компонентов.


## Shallow Rendering (Поверхностный рендеринг)

В итоге Welcome.test.jsx - это юнит тест проверяющий только компонент `Welcome`
а вот App.test.jsx - это у же скорее больше интеграционный тест, т.к. проверяет
работу двух компонентов App и Welcome в связке, App внутри себя содержит Welcome

И писать тесты для компонента App в таком подходе будет крайне не удобно т.к.
App по сути будет содержать всё наше приложение - все наши UI-компоненты.
в итоге при таком подходе тест для самого App со временем разрастётся до слишком
больших размеров, описывая по сути проверку всех наших компонентов.
при этом любое измнение в дочерних UI-компонентов содержащихся внутри App
будет ломать тесты самого App.

А идея юнит тестов в том, чтобы тесты были именно юнит-текстами.
Чтобы тестировать отдельные части, юниты всего нашего приложения, по отдельности

Когда мы разрабатывали API на php то там мы активно использовали заглушки
компонентов - используя для этого mock-и и stub-ы. То есть когда проверяли
конкретный компонент, то на место его внутренних компонентов подставляли
заглушки.

Такая же идея применима и к тестам UI-компонентов, написанных для React.
т.е. можно вместо конкретных дочерних UI-компонентов внутри их родительского
UI-компонента подставлять заглушки, чтобы проверять только один компонент не
зависимо от компонентов используемых внутри проверяемого компонента.

## React-18 vs React-16

для 16й и 17й версии React для написания юнит-тестов под React-компоненты
активно использовали библиотеку `Enzyme` https://github.com/enzymejs/enzyme

Но в библиотеку `React 18` внесли изменения, которые по сути
сделали невозможным обновление Enzyme до 18 версии React-a
подробнее смотри здесь ./docs/21-test-n-liners.react/03-use-rtl-react18.md


## Переписываем App.test.jsx с интеграционного на юнит-тест

Сам исходник для которого нужно сделать юнит-тест
```js
import React from 'react';
import './App.css';
import Welcome from './components/Welcome';

function App() {
  return (
    <div className="app">
      <Welcome />
    </div>
  );
}

export default App;
```

Переписываем с интеграционного на юнит тест создавая заглушку для Welcome

Идея здесь в том, чтобы не вызывать рендеринг дочерних элементов в App
а только проверять рендеринг самого тестируемого компонента App,
поэтому для этого здесь подменяем внутренний-дочерний компонент `Welcome`
заглушкой через `jest.mock`


./frontend/src/App.test.jsx
```js
import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

// jest.mock('./components/Welcome', () => (props) => <span {...props} />);

// создаём заглушку компонента Welcome (подставится внутрь App вместо оригинала)
jest.mock('./components/Welcome', () => (_props) => {
  // преобразуя его в простой span с доп полем data-testid для screen.getByTestId
  return <span data-testid="WelcomeComponent" />
});

test('renders app', () => {
  render(<App />);
  // проверяю что в результате рендеринга внутри был отображен компонент
  // Welcome который мы выше переопределили мок-заглушкой
  expect(screen.getByTestId("WelcomeComponent")).toBeInTheDocument();
  // screen.debug() эта вещь выведет все отрендеренные html-теги в консоль
  // при запуске теста
});
```

Для React 16-17 и Enzyme примерно тоже самое могло бы выглядеть так:
```js
import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

it('render app', () => {
  shallow(<App />);
})
```
`shallow` - это метод из библ-ки Enzyme который отрисовывает компонент без
отрисовки его дочерник компонентов т.е. без вызова Welcome
в общем такой тест будет просто проверять рендерится ли вообще компонент App
и нет ли в нём неких грубых ошибок.

Вообще здесь у меня всё же возникают вопросы как правильнее проверять рендеринг
App без зависимостей от внутренних компонентов, т.к. мой подход с созданием
заглушки для Welcome через jest.mock - это по сути всё равно внедрение в детали
реализации самого компонента App - т.е. надо идти и смотреть что он там у себя
за компоненты использует.


## Пробуем сломать компонент чтобы убедится что и тест завалится
Сначала запускаем наши тесты,
```sh
make frontend-test
docker compose run --rm frontend-node-cli yarn test --watchAll=false
yarn run v1.22.19
$ react-scripts test --watchAll=false
 PASS  src/components/Welcome.test.jsx
 PASS  src/App.test.jsx

Test Suites: 2 passed, 2 total
Tests:       2 passed, 2 total
Snapshots:   0 total
Time:        2.205 s
Ran all test suites.
Done in 3.00s.
```

Затем в Welcome заменяем строку `Auction` -> `Auc tion` и запускаем тесты снова


```sh
make frontend-test
docker compose run --rm frontend-node-cli yarn test --watchAll=false
yarn run v1.22.19
$ react-scripts test --watchAll=false
 PASS  src/App.test.jsx
 FAIL  src/components/Welcome.test.jsx
  ● renders app

TestingLibraryElementError: Unable to find an element with the text: /Auc tion/i.
This could be because the text is broken up by multiple elements.
In this case, you can provide a function for your text matcher
to make your matcher more flexible.

    Ignored nodes: comments, script, style
    <body>
      <div>
        <div
          class="welcome"
        >
          <h1>
            Auction
          </h1>
          <p>
            We will be here soon
          </p>
        </div>
      </div>
    </body>

       5 | test('renders app', () => {
       6 |   render(<Welcome />);
    >  7 |   const h1Element = screen.getByText(/Auc tion/i);
         |                            ^
       8 |   expect(h1Element).toBeInTheDocument();
       9 | });
      10 |

      at Object.getElementError (node_modules/@testing-library/dom/dist/config.js:37:19)
      at node_modules/@testing-library/dom/dist/query-helpers.js:76:38
      at node_modules/@testing-library/dom/dist/query-helpers.js:52:17
      at getByText (node_modules/@testing-library/dom/dist/query-helpers.js:95:19)
      at Object.<anonymous> (src/components/Welcome.test.jsx:7:28)

Test Suites: 1 failed, 1 passed, 2 total
Tests:       1 failed, 1 passed, 2 total
Snapshots:   0 total
Time:        2.171 s
Ran all test suites.
error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
```

Возращаем обрано  и проверяем снова - оба теста работают
