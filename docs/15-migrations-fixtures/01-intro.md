Содержание раздела:

- Обзор что уже сделано и зачем вызод на миграции
- Миграции и продвинутые инструменты для заполнения БД-таблиц. (02-migrations.md)
- Что такое миграции
- Phinx - легковестная библиотека для создания миграций             03-phinx.md

## 04-doctrine-migrations.md

Doctrine-Migrations:
- Как использовать и запускать Doctrine-Migrations в нашем коде                 :49
- Простейший способ прокинуть команды из Doctrine-Migrations в свой bin/app.php :88
- Краткий обзор команд Doctrine-Migrations какие есть и для чего нужны          :150
- Проброс EntityManager-а через DependencyFactory                               :176
- Как настраивать миграции                                                      :258
- Более продвинутый способ настройки миграций прямо через php-код               :293
- Настройка миграций для нашего проекта (DependencyFactory)                     :368
- Пример настройка миграций с HelperSet для старых версий библиотеки            :454
- Разделение команд Doctrine-Migrations для разных окружений(dev|prod)          :521
- run migrations:diff - создание первой миграции изучаем сгенерированный код    :694
- Коментарии кастомных типов сохраняемые в саму БД. что это и зачем             :790
- CREATE SCHEMA public исправление бага (06-fix-public-bug)                     :859
- Накатываем сгенерированную миграцию migrations:migrate                        :872
- изучаем состояние бд после накатывание миграции                               :932
- 07-migrations-dev-cmds Настройка Makefile под новые команды (+ wait4open)
- 08-migrations-prod-cmds
- 09-summary-orm+migrations Краткий итог того что даёт ORM+Migrations
- 10-fixtures.md Фикстуры что это такое, зачем и как делается



## Обзор что уже сделано и зачем

У нас уже есть
- подключенная к проекту базад данных (PostgreSQL)
- подключенная библиотека Doctrine-ORM для работы с сущностями
- через Doctrine-ORM аттрибуты(аннотации) прописали связи сущностей и их частей
  с базой данных.(связи - как сохранять и загружать сущности в БД)
- для всех полей сущности User прописали то как, и в каком формате перекладывать
  каждое из этих полей в базу данных и в каком виде извлекать из бд.
  То есть по аттрибутам(аннотациям) доктрин уже сама автоматически будет
  преобразовывать данные из обьектов в поля таблицы бд и обратно из полей таблицы
  собирать инстансы обьектов наших классов с заполненными значениями полями классов
- создали для своих собтвенных классов обьектов-значений Id,Email,Role..
  свои собственные "доктриновские типы" - специальные классы:
  IdType, EmailType, RoleType,
  в которых прописали то, как должна происходить конвертация значений в и из бд.
  На основе этих типов Доктрин сама будет инстанцировать из полей таблиц обьекты,
  заполняя приватные поля соответствующими данными из полей таблицы в бд.
- добавили в своё консольное php-приложение нужные нам cli команды Doctrine
- научились проверять корректность прописанного нами маппинга
  (валидировать схему таблицы БД) все ли аттрибуты(аннотации) прописаны правильно
- подошли к тому, чтобы начать знакомство с миграциями - созданием и заполнением
  таблиц некими данными.


## Выход на задачу создания самих БД-таблиц. Подробнее о валидации схем и маппинга

Например для сущности [User](./api/src/Auth/Entity/User/User.php)
прописали что все сущности этого класса должны сохранятся в таблицу `auth_users`

```php
<?
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'auth_users')] // в какой таблице в бд должна быть эта сущность
class User {
  //...
```

В момент доставания из репозитория сущности User, Doctrine:
- сделает запрос в БД
- получит ассоциативный массив с полями
- на основе полученной из БД инфы воссоздат обьект класса User (сущность)

При воссоздании обьекта класса сущности по данным из бд Доктрин создаёт его
через рефлексию, а не через конструктор. Т.е. значения всех полей (и приватных
тоже) будут заполнены значениями напрямую, в обход созданного нами конструктора.

К своему консольному приложению ./api/bin/app.php мы прописали некоторые cli
команды идущие в составе библиотеки Doctrine-ORM.

смотрим на все наши cli команды
```sh
docker compose run --rm api-php-cli composer app
```
```
# ...
Available commands:
  di-check                Check DI-Container
  hello                   Hello command
  help                    Display help for a command
  list                    List commands
 orm
  orm:info                Show basic information about all mapped entities
  orm:schema-tool:create  Processes the schema and either create it directly on
                          EntityManager Storage Connection or generate the SQL output

  orm:schema-tool:drop    Drop the complete database schema of EntityManager Storage
                          Connection or generate the corresponding SQL output

  orm:validate-schema     Validate the mapping files
```

`orm:validate-schema` -
 эта одна из полезнейших команд для валидации схемы mapping->database

```sh
docker compose run --rm api-php-cli composer app orm:validate-schema
```
[Детально об этом здесь](./docs/14-entity2db-mapping/05-check-mapping.md)

## Задача: создать сами таблицы для созданных нами связей(маппинга сущностей)

## Как можно создавать БД-таблицы

Способ первый на локальной машине через команду `orm:schema-tool:create`
Эта команда создаст таблицы на основе написанного нами маппинга
то есть на основе нами написанных аттрибутов-аннотаций расставленных в прямо в
исходном коде классов наших сущностей и их "запчастей"


```sh
docker compose run --rm api-php-cli composer app orm:schema-tool:create
> php bin/app.php --ansi 'orm:schema-tool:create'
 !
 ! [CAUTION] This operation should not be executed in a production environment!
 !

 Creating database schema...
 [OK] Database schema created successfully!
```
Предупреждает что эту команду не надо использовать в проде.
Так же видим что схема в бд успешно создана

проверим содержимое самой бд:
```sh
PGPASSWORD=secret psql -h localhost -p 54321 -U app -d app
```

```
app=# \dt                                     < покажи все таблицы в текущей бд
              List of relations
 Schema |        Name        | Type  | Owner
--------+--------------------+-------+-------
 public | auth_user_networks | table | app
 public | auth_users         | table | app
(2 rows)
```

```
app=# \d+
                                  List of relations
 Schema |        Name        | Type  | Owner | Persistence |    Size    | Description
--------+--------------------+-------+-------+-------------+------------+-------------
 public | auth_user_networks | table | app   | permanent   | 0 bytes    |
 public | auth_users         | table | app   | permanent   | 8192 bytes |
(2 rows)
```

Смотрим данные по конкретным именам наших таблиц
```
app=# \d auth_user_networks
                     Table "public.auth_user_networks"
      Column      |         Type          | Collation | Nullable | Default
------------------+-----------------------+-----------+----------+---------
 id               | uuid                  |           | not null |
 user_id          | uuid                  |           | not null |
 network_name     | character varying(16) |           | not null |
 network_identity | character varying(16) |           | not null |
Indexes:
    "auth_user_networks_pkey" PRIMARY KEY, btree (id)
    "idx_3ea78c3ba76ed395" btree (user_id)
    "uniq_3ea78c3b257ebd71c756d255" UNIQUE, btree (network_name, network_identity)
Foreign-key constraints:
    "fk_3ea78c3ba76ed395"
              FOREIGN KEY (user_id) REFERENCES auth_users(id) ON DELETE CASCADE
```

```
app=# \d auth_users
                                                Table "public.auth_users"
            Column            |              Type              |  Nullable |              Default
------------------------------+--------------------------------+-----------+-----------------------------------
 id                           | uuid                           |  not null |
 date                         | timestamp(0) without time zone |  not null |
 email                        | character varying(255)         |  not null |
 status                       | character varying(16)          |  not null |
 password_hash                | character varying(255)         |           | NULL::character varying
 new_email                    | character varying(255)         |           | NULL::character varying
 role                         | character varying(16)          |  not null |
 join_confirm_token_value     | character varying(255)         |           | NULL::character varying
 join_confirm_token_expires   | timestamp(0) without time zone |           | NULL::timestamp without time zone
 password_reset_token_value   | character varying(255)         |           | NULL::character varying
 password_reset_token_expires | timestamp(0) without time zone |           | NULL::timestamp without time zone
 new_email_token_value        | character varying(255)         |           | NULL::character varying
 new_email_token_expires      | timestamp(0) without time zone |           | NULL::timestamp without time zone
Indexes:
    "auth_users_pkey" PRIMARY KEY, btree (id)
    "uniq_d8a1f49ce7927c74" UNIQUE, btree (email)
Referenced by:
    TABLE "auth_user_networks"
          CONSTRAINT "fk_3ea78c3ba76ed395"
          FOREIGN KEY (user_id)
          REFERENCES auth_users(id)
          ON DELETE CASCADE
```

Как видим все нужное для работы создано:
- все поля и их настройки
- все уникальные первичные ключи
- все внешние ключи для связей записей между несколькими таблицами
- для типов Embedded(Token,Network) создались парные колонки(поля в таблице):
   - `join_confirm_token_value` и `join_confirm_token_expires`
   оба этих поля и будут собираться в обьект нашего класса Token
   то есть это то конкретное место в бд-таблице, куда доктрин при сохранении
   в БД самого обьекта User, с имеющимися валидными токенами
   будет сохранять сами значения из полей класса Token.
   Так же и в обратку. по этим полям Доктрин и будет собирать обьеты из БД,
   возвращая их уже с заполненными значениями полями.


## Выход на проблему изменения таблиц в ходе разработки

Когда мы разрабатываем код своего приложения невозможно один раз создать
таблицу и вообще никогда больше её не изменять.
Изменения придётся делать постоянно:
- при создании новых сущностей,
- при добавлении новых полей в сущностях,
- при переименовании полей и соответственно колонок в таблицах,
- при удалении или добавлении новых полей - а значит и колонок в таблицах.

А это значит что и бд со временем нужно будет изменять. Так например если мы
в своём коде изменим в сущности имя поля, добавим новое или удалим старое
то нужно будет выполнить некие SQL команды по изменению бд чтобы и в самой бд
применились наши новые измнения.

Тут мы выхоми на вопрос о том,

## Как делать синхронизацию изменения кода в сущностях и имеющихся таблиц.

Есть разные способы как это можно делать.


Способ первый через `orm:schema-tool:update`(доктриноская cli команда)
(Годится только для локальной разработки, по сути одноразовый)

Можно добавить из доктриновских комад еще и команду `orm:schema-tool:update`

`orm:schema-tool:update` - так же как и `orm:validate-schema`: создаёт две схемы
одну по маппингу в нашем коде, вторую по текущему состоянию бд, затем сравнивает
эти две схемы и генерирует SQL запрос для измнения таблиц в бд под новые поля в
наших сущностях, прописанных аттрибутами(аннотациями).

Другими словами если в классе сущности User добавить новое поле, прописать для
него маппинг через аттрибуты доктрин, а затем выполнить эту команду, то она
создат такой SQL-Запрос, который добавит для этого, нашего нового поля, новую
колонку(поле в таблице бд) и сделает это в соответствии нашего маппинга.

Недостатки работы `orm:schema-tool:update`
Но у этой команды есть и недочёты - она не такая умная, и не всегда может
правильно понять наши намеренья.
- при удалении или добавлении полей эта команда справится и сделает свою работу.
- при переименовании старого поля эта команда тупо удалит старое и добавит новое,
  а не просто переименует его как следовало бы сделать. Почему так:
  - увидит что одно поле добавилось а другое поле исчезло,
  - сгенерит SQL запрос удаляющий старое поле и добавляющий новое поле,
    вмето того чтобы догадаться что надо просто сгенерить запрос на
    переименование старого поля(колонки) в новое имя.

## Выход на продвинутые специализированные иснтрументы создания БД-таблиц

Отсюда приходит понимание, что такие команды как
- `orm:schema-tool:create`
- `orm:schema-tool:update`
- `orm:schema-tool:drop`
можно использовать только для локальной разработки, и всего один раз. И для
рабочего решения дла прода это не годится.


Для работы с бд лучше будет найти более продвинутые и специализированные
иснтрументы нежели эти команды.

Начём с того что вообще удалим эту команду `orm:schema-tool:create` из нашего
bin/app т.к. будем работать без неё

Удаляем её из конфигурации dev-окружения:(просто убрав обьявление класса из массива)
- ./api/config/dev/doctrine.php
- ./api/config/dev/console.php


Убеждаемся что схемы в норме( Плосле отработки команды `orm:schema-tool:create`
```sh
docker compose run --rm api-php-cli composer app orm:validate-schema
```
```
Mapping   [OK] The mapping files are correct.
Database  [OK] The database schema is in sync with the mapping files.
```

Проверим реально ли это работает - изменим длинну поля-колонки сущности User

```php
<? class User {
    #[ORM\Column(type: 'auth_user_status', length: 32)]
    private Status $status;                    //  ^^
```

И перезапустив команду получим ошибку по базеданных
```
Database
[ERROR] The database schema is not in sync with the current mapping file.
```

Удалим все наши таблицы. Для того чтобы освоить другие инструменты рабоыт с бд.
```
docker compose run --rm api-php-cli composer app orm:schema-tool:drop -- --force
```
`[OK] Database schema dropped successfully!`

--force - именно удалить, а не просто сгенерировать SQL-запрос на удаление таблиц


Убеждаемся что таблицы реально были удалены

```sh
PGPASSWORD=secret psql -h localhost -p 54321 -U app -d app
```
```
app=# \d auth_users
Did not find any relation named "auth_users".
app=# \d auth_user_networks
Did not find any relation named "auth_user_networks".

app=# \dt                            << покажи все имеющиеся таблицы в этой бд
Did not find any relations.          << не найдено ни одной таблицы - дб пустая
```

Теперь освоим более продвинутые способы создания таблиц БД через миграции.





