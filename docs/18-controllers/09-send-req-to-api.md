## Ручное взаимодействие со сдеалнным API маршрутом

В нашем коде все проверки работы контроллеров на взаимдействие с API
сделаны в виде функциональных тестов:

```
api/tests/Functional/
├── HomeTest.php                << тест открытия "главной страницы" API
├── Json.php
├── MailerClient.php
├── NotFoundTest.php            << тест попытки запросить не существующий адрес
├── V1
│   └── Auth
│       └── Join
│           ├── RequestFixture.php
│           ├── RequestTest.php      -- для контроллера Join/RequestAction
│           ├── ConfirmFixture.php
│           └── ConfirmTest.php      -- для контроллера Join/ConfirmAction
└── WebTestCase.php
```

Но посмотреть "вживую" на то, как работает сделанное апи тоже можно

Вручную шлем json-запрос на наш сделнный API маршрут, чтобы посмотреть на ответ
контроллера и работу миддлверов при невалидном вводе

```http
POST http://localhost:8081/v1/auth/join
Accept: application/json
Content-Type: application/json

{
    "email": "new-user@app.test",
    "password": ""
}
```

Тоже самое через curl - послать json
```sh
curl -X POST http://localhost:8081/v1/auth/join \
-H "Accept: application/json" \
-H "Content-Type: application/json" \
-d '{"email": "new-user@app.test", "password": "" }'
```
ответ:
```json
{"errors":{"password":"This value should not be blank."}}
```

шлём невалидный email-адресс
```sh
curl -X POST http://localhost:8081/v1/auth/join \
-H "Accept: application/json" \
-H "Content-Type: application/json" \
-d '{"email": "new-user", "password": "qwertyuiop" }'
```

```json
{"errors":{"email":"This value is not a valid email address."}}
```

