<?php

namespace App\Auth\Test\Builder;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\Id;
use App\Auth\Entity\User\Network;
use App\Auth\Entity\User\Token;
use App\Auth\Entity\User\User;
use DateTimeImmutable;
use Ramsey\Uuid\Uuid;

class UserBuilder
{
    private Id $id;
    private DateTimeImmutable $date;
    private Email $email;
    private string $passwordHash;
    private Token $joinConfirmToken;
    private bool $active = false;
    private ?Network $network = null;

    public function __construct()
    {
        $this->id = Id::generate();
        $this->date = new DateTimeImmutable();
        $this->email = new Email('mail@example.com');
        $this->passwordHash = 'hash';
        $this->joinConfirmToken = new Token(
            Uuid::uuid4()->toString(),
            $this->date->modify('+1 day')
        );
    }

    public function withEmail(Email $email): UserBuilder
    {
        $clone = clone $this;
        $clone->email = $email;
        return $clone;
    }

    public function withJoinConfirmToken(Token $token): UserBuilder
    {
        $clone = clone $this;
        $clone->joinConfirmToken = $token;
        return $clone;
    }

    public function viaNetwork(Network $identity = null): UserBuilder
    {
        $clone = clone $this;
        $clone->network = $identity ?? new Network('vk', '0001');
        return $clone;
    }

    public function active(): UserBuilder
    {
        $clone = clone $this;
        $clone->active = true;
        return $clone;
    }

    public function build(): User
    {
        if ($this->network !== null) {
            return  User::joinByNetwork(
                $this->id,
                $this->date,
                $this->email,
                $this->network
            );
        }

        $user = User::requestJoinByEmail(
            $this->id,
            $this->date,
            $this->email,
            $this->passwordHash,
            $this->joinConfirmToken
        );

        if ($this->active) {
            $user->confirmJoin(
                $this->joinConfirmToken->getValue(),
                $this->joinConfirmToken->getExpires()->modify('-1 day'),
            );
        }

        return $user;
    }
}
