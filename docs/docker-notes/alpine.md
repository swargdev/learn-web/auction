
## Посмотреть список установленых в образ Alpine-пакетов

```sh
apk info --depends
docker compose run --rm cucumber-node-cli apk info --depends
```

Смотрим инормацию о конкретном пакете `apk info chromium`
флаг --size - узнать размер пакета

```sh
docker compose run --rm cucumber-node-cli apk info chromium
chromium-120.0.6099.129-r0 description:
Chromium web browser

chromium-120.0.6099.129-r0 webpage:
https://www.chromium.org/Home

chromium-120.0.6099.129-r0 installed size:
219 MiB
```


redirect everything into the file:
```sh
docker build --no-cache --progress=plain  . &> build.log
```

