#!/usr/bin/env php
<?php

declare(strict_types=1);

// use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;

require __DIR__ . '/../vendor/autoload.php';

/** @var \Psr\Container\ContainerInterface $container */
$container = require __DIR__ . '/../config/container.php';

$cli = new \Symfony\Component\Console\Application('Console');

/**
 * @var string[] $commands
 * @psalm-suppress MixedArrayAccess
 */
$commands = $container->get('config')['console']['commands'];

/** @var Doctrine\ORM\EntityManagerInterface $entityManager */
$entityManager = $container->get(Doctrine\ORM\EntityManagerInterface::class);

// this is code a deprecated code for old versions:
// $cli->getHelperSet()->set(new EntityManagerHelper($entityManager), 'em');
// now to pass a deps into Doctrine/Migrations used a DependencyFactory class
// See DI-container setup in: ./api/config/common/migrations.php


foreach ($commands as $name) {
    /** @var Symfony\Component\Console\Command\Command $command */
    $command = $container->get($name);
    $cli->add($command);
}

$cli->run();
