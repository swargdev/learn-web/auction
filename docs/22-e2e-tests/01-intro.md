## Вводная

На текущий момент нами сделаны первоначальные версии api и frontend:

```
├── api/                             << backend на php и Slim
├── frontend/                        << frontend на js и React
├── gateway/
├── docs/
├── Makefile
├── docker-compose-production.yml
├── docker-compose.yml
├── provisioning
├── LICENCE.md
```

Заглушка нашего фронтенда работает и доступна на локальном адресе
http://localhost:8080/

Страница ею выводимая визуально точно такая же как и раньше,
но теперь эта страница рендерится не на строне сервера, а через React

посмотрев код страницы в браузере через Inspect увидим:
```html
<html>
  <head>
    ...
    <title>Auction</title>
  <script defer="" src="/static/js/bundle.js"></script>
    ...
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
      <div class="app">
        <div class="Welcome_welcome__tQMjo">
          <h1>Auction</h1>
          <p>We will be here soon</p>
        </div>
      </div>
    </div>
  </body>
</html>
```

На данный момент к нашим api и frontend мы написали тесты и добавили разного рода
линтеры позволяющие проверять качество написанного кода и отлавливать ошибки.

Теперь имея отдельно протестированные api и frontend встаёт задача написать
общие тесты, которые будут проверять работу нашего приложения как единого
целого. И даже если со временем мы будет разделять наш api на некие подсистемы
и вынесим часть как отдельные компоненты - подсистемы. но общие тесты всё
равно будут проверять работу и взаимодействие всех подсистем(компонентов) как
единого целого.

Для начала нужно определится с понятиями.
Что за обобщающие тесты и как их называть.


Первые нами написанные тесты - это юнит-тесты(модульные тесты),
с помощью которых мы тестировали наши классы в API
- части код - юниты мы тестировали изалированно от других частей кода
- тестируя некий компонент зависящий от других компонентов все зависимости
  мы подменяли заглушками-муляжами(mock-ами)
  Так например, если тестируемый сервис, для своей работы требовал другой сервис
  то тот другой мы подменяли mock-ом эмулируя его наличие и некое его поведение

Превый наш выход на интеграционные тесты так же был в api
Для тестирования взаимодействия нескольких юнитов(частей кода) друг с другом
- например когда в api мы писали ./api/src/Frontend/FrontendUrlGenerator.php (1)
  то написали для него и юнит-тест проверяющий работу самого этого генератора
  ./api/src/Frontend/Test/FrontendUrlGeneratorTest.php
  и суть проверки в этом тесте в том чтобы создать обьект FrontendUrlGenerator
  затем вызвать нужный метод а затем проверить что этот метод возвращает
  правильные значения

```
./api/src/Frontend/
├── FrontendUrlGenerator.php     << (1)
├── FrontendUrlTwigExtension.php
└── Test
    ├── FrontendUrlGeneratorTest.php         < Unit-Test
    └── FrontendUrlTwigExtensionTest.php     < Integration test
```

Дальее мы написали ./api/src/Frontend/FrontendUrlTwigExtension.php
обьект для генерации адресов на фронтенд, который внутри себя использует обьект
FrontendUrlGenerator.
Оба этих класса решали задачу по генерации писем подтверждения регистрации
внутри которых должны генерироваться ссылки на подтверждение регистрации с
полными абсолютными url путями на наш фронтенд.
И для тестирования FrontendUrlTwigExtension мы уже написали не просто юнит-тест
а интеграционный тест проверяющий совместную работу двух компонентов:
FrontendUrlTwigExtension и FrontendUrlGenerator
и для этого мы создавали twig-овское окружение (Environment):

./api/src/Frontend/Test/FrontendUrlTwigExtensionTest.php
```php
<?php
class FrontendUrlTwigExtensionTest extends TestCase {
    public function testSuccess(): void {
        // Заглушка
        $frontend = $this->createMock(FrontendUrlGenerator::class);
        $frontend->expects($this->once())->method('generate')->with(
            $this->equalTo('path'),
            $this->equalTo(['a' => '1', 'b' => '2']),
        )->willReturn($url = 'http://site/path?a=1&b=2');

        // создание твиговского окружения для рендеринга страницы
        $twig = new Environment(new ArrayLoader([
            'page.html.twig' =>
             "<p>{{ frontend_url('path', {'a': 1, 'b': 2}) }}</p>",
        ]));

        // подключение нашего плагина FrontendUrlTwigExtension
        $twig->addExtension(new FrontendUrlTwigExtension($frontend));

        $exp = '<p>http://site/path?a=1&amp;b=2</p>';
        // проверяем результат рендеринга всей страницы в целом
        self::assertEquals($exp, $twig->render('page.html.twig'));
    }
```
И здесь мы уже проверяли как будет рендерится наше представление через наш
плагин FrontendUrlTwigExtension регистрируя его в Environment twig-а
То есть это уже полноценный интеграционный тест, в котором идёт проверка работы
сразу нескольких обьектов(компонентов-юнитов) друг с другом.
Причем обычно интеграционные тесты обычно пишутся только по нужде. т.к.
- их написание тяжелее чем юнит-тестов
- выполняются они дольше чем юнит-тесты

Что даёт написание Юнит-тестов.
Когда начиная проект тесты пишутся сразу, одновременно или даже прямо до
написания самих компонентов то в итоге код получается намного более чистый и
удобный и для тестирования и для дальнейшей работы с ним и поддержки.
Т.е. подход с написанием юнит-тестов одновременно или даже до момента
написания самих компонентов, которые эти юнит-тесты тестируют подталикивают к
написанию хорошо тестируюемого кода, с меньшим числом сильных зависимостей от
других компонентов. Просто потому, что писать юнит-тесты когда надо для проверки
одного компонента в него передавать кучу других крайне не удобно и подталкивает
к написанию более осмысленного кода, с меньшим числом сильных зависимостей.

С другой стороны бывают ситуации когда нужно написать тесты для старого легаси
проекта в котором тесты вообще не писались. И как правило код в таких проектах
тяжело тестируем из-за сильной связности компонентов.
Такой код еще называют "ком грязи"
В таком случае написать юнит-тесты бывает либо слишком трудно или затратно либо
вообще не возможно.
Поэтому чаще для такого кода пишут именно интеграционные тесты.
Которые проверяют работу сразу целого набора из нескольких компонентов, а не
компоненты по отдельности. И делается это просто потому, что все компоненты
настолько связаны что "оторвать" один компонент от других, чтобы проверить его
функциональность, бывает просто не возможно либо слишком долго и трудоёмко.
Далее как правило такого рода легаси-код, когда уже есть интеграционные тесты
рефакторят выделяя отдельные компоненты с одновременным написанием юнит-тестов
для них.


Функциональные тесты.
у нас функциональные тесты занимались эмуляцией отправкой json-запросов на наши
контроллеры. При этом происходило
- формирование приложения App
- формирование тестого json-запроса
- отправка созданного запроса в приложение App

И суть таких тестов была в том чтобы проверить конкретную функциональность
приложения:
- какой ответ(response) прислало App
- детальная проверка содержимого присланного ответа - поля и их значения
  проверяли соответствует ли ответ и его поля нашим ожиданиям

В общем картина такая:
- юнит тесты - для тестирования по отдельности каждого класса-компонента
- интеграционные тесты - для тестирования набора из нескольких компонентов
- функциональные тесты - для тестирования отдельных функциональных возможностей
  функциональных частей всего приложения в целом.

Таким образом переход от юнит тестов к функциональным - это тенденция ухода от
мелких деталей к более обобщённой укрупнённой картине


Тоже самое применимо и для фронтенда
юнит-тесты - для тестирования отдельных UI-комонентов
интеграционные тесты - это уже рендеринт UI-компонента состоящего из множества
других UI-компонентов

Здесь приведены виды тестов и краткое их описание

https://www.atlassian.com/continuous-delivery/software-testing/types-of-software-testing

1. Unit tests
Unit tests are very low level and close to the source of an application.
They consist in testing individual methods and functions of the classes,
components, or modules used by your software.
Unit tests are generally quite cheap to automate and can run very quickly by
a continuous integration server.

2. Integration tests
Integration tests verify that different modules or services used by your
application work well together. For example, it can be testing the interaction
with the database or making sure that microservices work together as expected.
These types of tests are more expensive to run as they require multiple parts
of the application to be up and running.

3. Functional tests
Functional tests focus on the business requirements of an application.
They only verify the output of an action and do not check the intermediate
states of the system when performing that action.

There is sometimes a confusion between integration tests and functional tests
as they both require multiple components to interact with each other.
The difference is that an integration test may simply verify that you can query
the database while a functional test would expect to get a specific value from
the database as defined by the product requirements.


## И вот мы преходим к новому виду тестов E2E:
E2E тесты и Приёмочное(Acceptance) тестирование

Приёмочное тестирование - это когда готовое приложение, показывается заказчику
с целью проверить его работу и готовность к работе.
Показываться приложение может например через загрузку на dev-сервер. На который
уже заходит сам заказчик и своими силами проверяет работу всего приложения в
целом. Например для сайта: заполняет формы кликает по кнопкам. И в итоге либо
принимает работу либо отправляет проект на доработку.
Обычно приёмочное тестирование это больше про ручную проверку, про ручные клики
и прямое взаимодействие человека и созданной системы.


5. Acceptance testing (Приёмочное тестирование)

Acceptance tests are formal tests that verify if a system satisfies business
requirements. They require the entire application to be running while testing
and focus on replicating user behaviors. But they can also go further and
measure the performance of the system and reject changes if certain goals are
not met.

из этого описания понятно что это скорее про более ручной способ проверки.


Когда же мы пишем свой проект независимо - без заказчиков, то по сути мы сами и
являемся заказчиком и нам самим и придётся принимать работу нашей системы.
То есть нужна будет приёмочная проверка перед деплоем проекта на рабочий сервер

А так как мы программисты то логично будет проверять всё это не руками а
автоматизированными инструментами. Другим словами даже для приёмочного
тестирования можно написать свои автоматизированные тесты которые и будут сами
автоматичечски по описанной нами логике проверять работу всего приложения в
целом. Эмулируя при этом поведение человека-заказчика.(клики, заполнение форм)

Идея такая:
- берём некий инструмент(фреймворк или либу) которая умеет эмулировать
работу браузера. (Уже есть такие готовые инструменты, один из них Selenium)
https://www.selenium.dev/
- пишем тесты эмулирующие поведение пользователя на нашем сайте
- в итоге получаем возможность автоматизированно проверять работу всего сайта
  backend+frontend как единого целого. Особенно актуально для интерактивных
  приложений(сайтов), как у нас на React или на любом другом фронтенд-фреймворке

Какие бывают тестовые фреймворки для этих целей?
- https://codeception.com/ - в этом фреймворке сразу из коробки при его установке
и инициализации проекта создаются три каталога: `unit` `function` и `acceptance`
т.е. сразу создаются каталоги для написания трёх видов тестов:
- юнит(модульных) тестов, функциональных и приёмочных тестов

Но в рассматриваемой нами статье есть и вот такой вот вид тестов:

4. End-to-end tests

Сквозное тестирование(E2E) воспроизводит(эмулирует) поведение пользователя с
программным обеспечением в полной среде приложения. И проверяет, что различные
варианты использования приложения работают как надо. E2E тесты могут быть такими
же простыми, как загрузка веб-страницы или вход в систему, или гораздо более
сложными сценариями, проверяющие уведомления по электронной почте,
онлайн-платежи и т. д.

Сквозные тесты очень полезны, но они более затратны и их может быть сложно
поддерживать, если они автоматизированы.
Рекомендуется провести несколько ключевых сквозных тестов и больше полагаться
на типы тестирования более низкого уровня (модульные и интеграционные тесты),
чтобы иметь возможность быстро выявлять критические изменения.

End-to-end testing replicates a user behavior with the software in a complete
application environment. It verifies that various user flows work as expected
and can be as simple as loading a web page or logging in or much more complex
scenarios verifying email notifications, online payments, etc...

End-to-end tests are very useful, but they're expensive to perform and
can be hard to maintain when they're automated.
It is recommended to have a few key end-to-end tests and
rely more on lower level types of testing (unit and integration tests)
to be able to quickly identify breaking changes.

В общем идея здесь в полноценной эмуляции поведения пользователя на нашем сайте
- открытие страниц сайта
- заполнение форм
- клики по кнопкам для отправки данных на серверную часть приложения(сайта)
- проверка корретности отображаемых пользователю данных

В общем End-to-End (сквозные) тесты - это про работу пользователя с нашим сайтом

Вопрос поиска фреймворка позволяющего писать E2E-тесты
- если эти тесты размещать в каталог api в нашем проекте то надо искать фреймворк
на php
- но они скорее больше про фронтенд а значит место им в каталоге frontend
  тогда просто нужно будет доустановить некую js библ-ку и уже через тот же Jest
  запускать тесты в которых будет происходить открытие эмулируемого бразуера
  с открытием страниц и эмуляцией нажатий клавиш и вводом данных

другими словами E2E-тесты можно писать хоть на php и разместить их в api/
хоть на js и разместить в кталог frontend/
но размещать е2е тесты во фронденде означает загромождать код самого фронтенда
т.к. там будут как тесты самого фронтенда так еще и тесты всего нашего
приложения в целом. Поэтому более правильно держать E2E тесты отдельно в
отдельном каталоге например либо с именем `e2e` либо по названию фреймворка.

Плюс размещения E2E тестов в отдельном кталоге вне api/ и frontend/ заключается
в том что можно будет выбрать для их написания любой язык программирования и
любой удобный тестовый фреймворк. То есть это по сути уже будет отдельный
подпроект проверяющий работу нашего проекта-сайта как единого целого

E2E тесты эмулируют поведение юзера на нашем сайте
и от способа восприятия слова "поведение" открываются два пути:
- писать тесты в виде привычного программного кода по типу того как мы это уже
делали на PHPUnit и Jest. когда идёт создание обьект браузер вызов методов по
заполнению разных полей и разных действий.
- более продвинутые тесты behavior-driven development


Какие виды driven development мы уже знаем?
- domain driven development - анализируем предметную область, пишем свой
исходный код под сущности и понятия встречаемые в жизни - а конкретно в области
кторую мы автоматизируем через свой проект(свою программу)

- test driven development - подход когда перед написанием самого компонента
(класса или метода) сначало пишутся сами тесты, описывающие как именно должен
вести себя тестируемый компонент, а уже затем на основе тестов пишется код
самого компонента

- behavior-driven development
 это чем-то напоминает test driven development(TDD),
 но в BDD пишутся тесты проверяющие поведение
 - используется описание необходимого нам поведения на специальном доменно-
 специфическом языке. По-простому говоря поведение нужно описывать на простом
 человеко-понятном языке. Языке спицифичном для конкретной предметной(domain)
 области которую собственно у себя и автоматизируем

Статья из Вики о Behavior-driven_development(BDD)

https://en.wikipedia.org/wiki/Behavior-driven_development
In software engineering, behavior-driven development (BDD) is a software
development process that encourages collaboration among developers,
quality assurance experts, and customer representatives in a software project.
It encourages teams to use conversation and concrete examples to formalize
a shared understanding of how the application should behave.
It emerged from test-driven development (TDD) and
can work alongside an agile software development process.
BDD combines the general techniques and principles of TDD
with ideas from domain-driven design and object-oriented analysis and design
to provide software development and management teams with shared tools and
a shared process to collaborate on software development.

В этой же статье есть о принципах построения этих спецификаций "поведения".

идея здесь в том, чтобы проверяемое поведение описывать в виде текста на обычном
человеческом языке, внедряя в него специальные ключевые слова(Об это еще будет)


> Principles of BDD

Test-driven development is a software-development methodology which
essentially states that for each unit of software, a software developer must:

- define a test set for the unit first;
- make the tests fail;
- then implement the unit;
- finally verify that the implementation of the unit makes the tests succeed.

This definition is rather non-specific in that
it allows tests in terms of high-level software requirements,
low-level technical details or anything in between.
One way of looking at BDD therefore, is that it is a continued development of
TDD which makes more specific choices than TDD.

Behavior-driven development specifies that tests of any unit of software should
be specified in terms of the desired behavior of the unit.
Borrowing from agile software development the "desired behavior" in this case
consists of the requirements set by the business - that is,
the desired behavior that has business value for whatever entity commissioned
the software unit under construction.
Within BDD practice, this is referred to as BDD being an "outside-in" activity.

BDD говорит о том, что тесты любой программной единицы(software unit) должны
быть описаны(специфицированы) с точки зрения желаемого поведения этой единицы.
"Желаемое поведение" состоит из требований предъявляемых бизнесом,
то есть "желаемое поведение" - это нечто что может иметь комерческую ценность
для любой организации, заказавшей разрабатываемую программную единицу.

## Структура описания поведенческих характеристик - Behavioral specifications

Following this fundamental choice, a second choice made by BDD relates to
how the desired behavior should be specified.
In this area BDD chooses to use a semi-formal format for behavioral specification
which is borrowed from user story specifications
from the field of object-oriented analysis and design.
The scenario aspect of this format may be regarded as an application of
Hoare logic to behavioral specification of software units using the
domain-specific language of the situation.

BDD specifies that business analysts and developers should collaborate in
this area and should specify behavior in terms of user stories,
which are each explicitly written down in a dedicated document.
Each user story should, in some way, follow the following structure:

В общем описанием поведенческих характеристик выбрали полу-формальный формат
заимствованный из спецификаций user story(пользовательских историй) из области
Обьектно-ориентированного анализа и проектирования.
Аспект сценария описывает логику поведенческих характеристик программных единиц
используя язык с особенностями конкретной предметной логики.
BDD предполагает что бизнес-аналитики и разработчики должны сотрудничать в этой
области и должны описывать поведение в понятиях историй пользователя(user stories)
каждая из которых явно прописанала в специальном отдельном документе.
И каждая такая user story в свою очередь должна следовать такой структуре:

(это конкретный пример структуры для описания поведенческих характеристики)

`Title`( Заголовок)
  An explicit title.

`Narrative` (Повествование)
  A short introductory section with the following structure:

  - `As a`: the person or role who will benefit from the feature;
  - `I want`: the feature;
  - `so that`: the benefit or value of the feature.

`Acceptance criteria` (Критерии приемки)
  A description of each specific scenario of the
  narrative with the following structure:

  - `Given`: the initial context at the beginning of the scenario,
             in one or more clauses;
  - `When`: the event that triggers the scenario;
  - `Then`: the expected outcome, in one or more clauses.

Конкретный пример
- нужно прверить вход юзера на сайт
  соответственно взяли некую функциональность и нам нужно описать как это должно
  работать.

`Title` // ( Заголовок)
  вход юзера на сайт
`Narrative` // (Повествование)  повествовательно описываем желаемое поведение
                             // как должна работать конкретно эта функциональность
 нужно проверить функциональность по входу на сайт

// в виде похожего на коментарий на обычном человеческом языке деля на абзацы
  `As a`: (`как`) обычный пользователь
  `I want`: (я хочу) войти на сайт
  `so that`: (так чтобы) меня пустило в мой личный кабинет

// А дальше идёт описание разных сценариев в на человеческом языке
// причем начинать предложения нужно с ключевых слов `Given` `When` `Then`
//given - описываем "дано" ситуацию с которой начинаем
`Given`: открыв страницу сайта я неавторизованный гость
`When`: (когда) я запоняю форму авторизации своими данными и отправляю форму..
`Then`: (тогда) меня запускает в личный кабинет, либо показывает ошибку ввода

В общем идея простая
- писать на чётком, однозначном и понятном любому человеку языке описывая все
  возможные варианты, для того чтобы потом такой документ можно было отдать
  разработчику-программисту для реализации в виде кода
- дать возможность заказчикам писать свои требования на обычном чел-ком языке
  в виде своего рода спецификаций, которые так же яв-ся документов на равне с
  техническим заданием и могут быть размещатся в репозитории проекта
- дальше под каждое предложение и каждый сценарий пишутся тесты, которые
  делают описанное на обычном человеческом языке.


В итоге к чему мы приходим
- у нас должен быть отдельный каталог где будут сохранены все эти проверяемые
  поведенческие характеристики кратко называемые `features`
- где-то рядом будет каталог с самим исходным кодом, который будет парсить
  описанный человеческий текст и на их основе исполнять провеки в виде тестов
- в итоге по сути наш код будет просто своего рода прослойкой между тестами
  описанными на обычном человеческом языке в виде вот таких вот описаний как
  приведены выше

Для заказчиков плюс такого подхода в том, что можно сразу просто написать
"желаемое поведение"(поведенческие характеристики) на обычном человеческом языке,
а не программировать их на одном из ЯП (языке программирования)

И уже дальше эти поведенческие характеристики сохранить в *.feature-файлы и
добавить их как e2e тесты

Вот пример одной из такой "фичи" `feature` которую требуется проверить:


`Title`: Returns and exchanges go to inventory.  // название фичи

// человеко-понятный коментарий того чтобы был понятнее контекст того что мы
// здесь вообще собираемся проверять
`As a` store owner,
`I want` to add items back to inventory when they are returned or exchanged,
`so that` I can sell them again.

// дальше идут разные сценарии работы - поведенческие характеристики проекта
`Scenario 1`: Items returned for refund should be added to inventory.
`Given` that a customer previously bought a black sweater from me
`and` I have three black sweaters in inventory,
`when` they return the black sweater for a refund,
`then` I should have four black sweaters in inventory.

`Scenario 2`: Exchanged items should be returned to inventory.
`Given` that a customer previously bought a blue garment from me
`and` I have two blue garments in inventory
`and` three black garments in inventory,
`when` they exchange the blue garment for a black garment,
`then` I should have three blue garments in inventory
`and` two black garments in inventory.

В идеале сценарий формулируется декларативно а не на имепративно - то есть
на деловом языке, без ссылок на конкретные UI-элементы через которые должно
происходить взаимодействие.

И такой формат описания сценариев назван Gherkin language

This format is referred to as the `Gherkin language`, which has a syntax similar
to the above example. The term `Gherkin`, however, is specific to the Cucumber,
JBehave, Lettuce, behave and Behat software tools.

[Спецификация Gherkin Syntax](https://cucumber.netlify.app/docs/gherkin/)
Идём в спеку чтобы посмотреть из чего состоит язык Gherkin
[Reference](https://cucumber.netlify.app/docs/gherkin/reference/)
https://github.com/cucumber/docs

Вот пример от туда того как сценарий может быть записан в виде обычного
текстового файла

```
Feature: Guess the word                                        // название фичи

  # The first example has two steps
  Scenario: Maker starts a game                                // 1й сценарий
    When the Maker starts a game
    Then the Maker waits for a Breaker to join

  # The second example has three steps
  Scenario: Breaker joins a game                               // 2й сценарий
    Given the Maker has started a game with the word "silky"
    When the Breaker joins the Maker's game
    Then the Breaker must guess a word with 5 characters
```

дальше описываются ключевые слова:

> Keywords

Each line that isn’t a blank line has to start with a Gherkin keyword,
followed by any text you like.
The only exceptions are the free-form descriptions placed underneath
Example/Scenario, Background, Scenario Outline and Rule lines.

The primary keywords are: (главные ключевые слова)

- `Feature`
- `Rule` (as of Gherkin 6)
- `Example` (or `Scenario`)    для описания сценария
- `Given`, `When`, `Then`, `And`, `But` for steps (or *) для определения шагов
- `Background` - для описания общих элементов для нескольких сценариев
   что-то вроде переиспользования кода, для обьединения нескольких в одно  для
   дальнейшего переиспользования
- `Scenario Outline` (or `Scenario Template`) - шаблон с плейсхолдерами см ниже
- `Examples` (or `Scenarios`) списки значений для шаблона см ниже

There are a few secondary keywords as well:

- `"""` (Doc Strings)
- `|` (Data Tables)
- `@` (Tags)
- `#` (Comments)

Так же для тестирования несколько однотиных сценариев(калькулятор) можно
написать один общий сценарий разместить в нём placeholder-ы и затем извне
подставлять туда через цикл наборы значений
и такой сценарий называют `Scenario Outline` или `Scenario Template`
а подставляемые значения называют как `Examples` или `Scenarios`


Вообще подобные сценарии можно писать не только на английском но и на русском
тоже. Это может пригодится когда заказчик русскоязычный - тогда задача по
описанию требований - поведенческих характеристик может быть возложена на него
чтобы фичи были описаны в виде простых файлов на русском языке, а дальше их
просто поместить в проект и реализовать в виде тестов.



## Выбираем фреймворк для работы c Gherkin Syntax
Мы у себя для E2E тестов будем использовать фреймворк Cucumber

https://cucumber.netlify.app/docs/installation/

Данный фреймворк достаточно зрелый и имеет свои реализации на разных языках

- Cucumber-JVM Java official
- Cucumber.js Node.js and browsers official
- Cucumber.rb Ruby, Ruby on Rails official
- Cucumber.ml OCaml official
- Cucumber.cpp C++ unmaintained
- Cucumber-Lua Lua official
- Android™ Java official
- Kotlin Cucumber-JVM with Kotlin official
- Cucumber-Scala Scala official
- Cucumber-Tcl Tcl unmaintained
- Godog Go official
- Xunit.Gherkin.Quick C#, F#, VB with .NET Core semi-official
- Behat `PHP` semi-official
- Behave Python semi-official
- SpecFlow C#, F#, VB.NET semi-official
- Cucumberish iOS, Swift, ObjC semi-official
- Test::BDD-Cucumber Perl semi-official
- gocuke Go semi-official
- Cucumber-Rust Rust unofficial
- GoBDD Go unofficial
- unencumbered D unofficial
- Cucumber-Clojure Clojure unmaintained
- Cucumber-Gosu Gosu unmaintained
- Cucumber-Groovy Groovy unmaintained
- Cucumber-JRuby JRuby unmaintained
- Cucumber-Jython Jython unmaintained

Как видно этот Gherkin синтаксис поддерживается многими современными языками

можно взять [Behat](https://docs.behat.org/en/latest/) и писать тесты на PHP
т.е. это фреймворк позволяющий парсить Gherkin-синтаксиси и под сценарии писать
программный код для тестов именно на php, дальше еще нужно было бы еще одну
доп библиотеку работающую с Selenium сервером

Selenium - сервер для управления поведением браузера
https://github.com/seleniumhq/selenium

Например когда проект пишут на Java часто бирут Behave и пишут на Python

## Почему не берём Behat + php .

Не факт что новые backend & api проекты так же будут создаваться на языке php.
И если изучить написание E2E тестов на php то это будем менее актуальным опытом
т.к. вносить язык php в проекты которые написаны не на php не получится.
Поэтому лучше сразу брать более популярный и более широкораспостранённый язык.

В частности наш фронтенд пришем на JavaScript, поэтому можно взять `Cucumber.js`
и писать тесты на js.

Достоинства-Преимущества выбора написания E2E тестов с Cucumber на JavaScript:

- Опыт написания E2E тестов для Cucumber на js более актуален и переносим
Даже если со временем мы для написания своего бэкенда с php перейдём на любой
другой язык, хоть на java, хоть на go то опыт с написанием тестов на js
останется актуальным и можно будет писать e2e тесты на js.
Подключить js в другой проект в котором путь даже не используется сам js намного
проще чем подключать php в проект где язык php не используется.

- Возожность работы с браузерами напрямую без нужны ставить посредника в виде
  Selenium-серверара
- при работе с js можно будет использовать асинхронные вещи из js

