## Инициализируем composer.json файл для своего проекта

- Подключение composer.json к нашему проекту(автолоадинг классов)
- Установка фреймворка Slim
- Кратко знакомимся с тем как работает HTTP-Framework и Slim в частости
- Создаём заглушку своего API уже на основе Slim-фреймворка


```sh
docker compose run --rm api-php-cli composer init
```
```
This command will guide you through creating your composer.json config.

Package name (<vendor>/<name>) [root/app]: swarg/auction
Description []:
Author [n to skip]: swarg
Minimum Stability []:
Package Type (e.g. library, project, metapackage, composer-plugin) []: project
License []: BSD-3-Clause

Define your dependencies.

Would you like to define your dependencies (require) interactively [yes]? n
Would you like to define your dev dependencies (require-dev) interactively [yes]? n
Add PSR-4 autoload mapping? Maps namespace "Swarg\Auction" to the entered relative path. [src/, n to skip]: n

{
    "name": "swarg/auction",           # название пакета
    "type": "project",                 # тип исходников - проект
    "license": "BSD-3-Clause",
    "authors": [
        {
            "name": "swarg"
        }
    ],
    "require": {}
}
```

Чтобы добавить в зависимости конкретную версию php можно сделать так:
```sh
docker compose run --rm api-php-cli composer require php ^8.1
```

```json
{
  // ...
  "require": {
    "php": "^8.1"
  }

```

опция конфиг
```json
{
  "config": {
    "process-timeout": 0,
    "sort-packages": true  // автоматически сортировать названия пакетов
  }
}
```

Дополнительная зависимость для dev(разработки)
Нужна для того чтобы выявлять использование "дырявых пакетов" с уязвимостями

это список пакетов которые не должны использоваться и обнаружив которые
композер должен ругаться. По сути это список перечислением таких не надёжных библиотек и компонет
```
"require-dev": {
    "roave/security-advisories": "dev-master"
}
```

## Подключаем библиотеки и фреймворк slim

Сам фреймворк slim и реализация протокола psr7 для php
```sh
docker compose run --rm api-php-cli composer require slim/slim slim/psr7
```
т.е. такая команда говторит композеру установи в проект две библиотеки
- slim/slim - это библ-ка с самим фреймворком
- slim/psr7 - пакет с реализацией протокола

```
./composer.json has been updated
Running composer update slim/slim slim/psr7
Loading composer repositories with package information
Updating dependencies
Lock file operations: 13 installs, 0 updates, 0 removals
  - Locking fig/http-message-util (1.1.5)
  - Locking nikic/fast-route (v1.3.0)
  - Locking psr/container (2.0.2)
  - Locking psr/http-factory (1.0.2)
  - Locking psr/http-message (1.1)
  - Locking psr/http-server-handler (1.0.2)
  - Locking psr/http-server-middleware (1.0.2)
  - Locking psr/log (3.0.0)
  - Locking ralouphie/getallheaders (3.0.3)
  - Locking roave/security-advisories (dev-master 00de3fc)
  - Locking slim/psr7 (1.6.1)
  - Locking slim/slim (4.12.0)
  - Locking symfony/polyfill-php80 (v1.28.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 13 installs, 0 updates, 0 removals
  - Downloading symfony/polyfill-php80 (v1.28.0)
  - Downloading ralouphie/getallheaders (3.0.3)
  - Downloading psr/http-message (1.1)
  - Downloading psr/http-factory (1.0.2)
  - Downloading fig/http-message-util (1.1.5)
  - Downloading slim/psr7 (1.6.1)
  - Downloading psr/log (3.0.0)
  - Downloading psr/http-server-handler (1.0.2)
  - Downloading psr/http-server-middleware (1.0.2)
  - Downloading psr/container (2.0.2)
  - Downloading nikic/fast-route (v1.3.0)
  - Downloading slim/slim (4.12.0)
  - Installing roave/security-advisories (dev-master 00de3fc)
  - Installing symfony/polyfill-php80 (v1.28.0): Extracting archive
  - Installing ralouphie/getallheaders (3.0.3): Extracting archive
  - Installing psr/http-message (1.1): Extracting archive
  - Installing psr/http-factory (1.0.2): Extracting archive
  - Installing fig/http-message-util (1.1.5): Extracting archive
  - Installing slim/psr7 (1.6.1): Extracting archive
  - Installing psr/log (3.0.0): Extracting archive
  - Installing psr/http-server-handler (1.0.2): Extracting archive
  - Installing psr/http-server-middleware (1.0.2): Extracting archive
  - Installing psr/container (2.0.2): Extracting archive
  - Installing nikic/fast-route (v1.3.0): Extracting archive
  - Installing slim/slim (4.12.0): Extracting archive
1 package suggestions were added by new dependencies, use `composer suggest` to see details.
Generating autoload files
3 packages you are using are looking for funding.
Use the `composer fund` command to find out more!
No security vulnerability advisories found
Using version ^4.12 for slim/slim
Using version ^1.6 for slim/psr7
```

И после этого указанные пакеты добавятся в composer.json в require:
```
    "require": {
        "php": "^8.1",
        "slim/psr7": "^1.6",
        "slim/slim": "^4.12"
    },
```

Так же composer все нужные пакеты скачал и установил в каталог `./api/vendor`

```sh
du -hcd 1 api/vendor/
```

```
580K  api/vendor/slim
      api/vendor/slim/slim      11 directories, 76 files (Всё серце фреймворка)
      api/vendor/slim/psr7       3 directories, 21 files

40K   api/vendor/fig
236K  api/vendor/nikic
28K   api/vendor/ralouphie
296K  api/vendor/psr
144K  api/vendor/composer
60K   api/vendor/symfony
1.4M  api/vendor/
1.4M  total
```

Как видим сам фрейморк минимальный всего 76 файла  `./api/vendor/slim/slim/`

`Slim/App.php` - основной класс "Приложение"

./api/vendor/slim/slim/Slim/Exception - иключения которыми можно кидаться
  например HttpNotFoundException который можно исп-ть у себя в коде например для
  того чтобы выбрасывать его в своём контроллере если надо вывести страницу 404

./api/vendor/slim/slim/Slim/Handlers - обработчки ошибок


./api/vendor/slim/psr7 - это реализация fig-овского http-message, которыйл лёг сюда:
./api/vendor/fig/http-message-util

как я понял сам протокол psr здесь:
```
./api/vendor/psr/
├── container
├── http-factory
├── http-message
├── http-server-handler
├── http-server-middleware
└── log
```
эти вещи реализует и использует у себя сам slim/psr7



### Подключаем фреймворк в своём public/index.php

старая заглушка api-шки
```php
<?php
declare(strict_types=1);
header('Content-Type: application/json');
echo '{}';
```

Простейший код из документации slim-a, о том как с ним работать в примитивном виде.
```php
<?php

declare(strict_types=1);

use Slim\Factory\AppFactory;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

require __DIR__ . '../vendor/autoload.php';

$app = AppFactory::create(); // используя Slim-овское Application
/* создаём новый инстанс класса App с настройками по умолчанию
исходник смотри здесь (GotoDefinition) там просто new App(...);
./api/vendor/slim/slim/Slim/Factory/AppFactory.php
create() - создание минимально необходимой рабочей комплектации для App обьекта
*/

// прописываем маршрутизацию
// путь в упле - функция с обработчиком на этот урл
$app->get('/', function (Request $request, Response $response, $args) {
    /* по сути эта функция - это контроллер или action
     в неё будут передаваться интерфейсы:
     Psr\Http\Message\ServerRequestInterface   - вся инфа о текущем запросе
     Psr\Http\Message\ResponseInterface        - черновая версия ответа сервера
     они просто мапяться к более коротким именам в начале через use .. as ..;
     args - это аргументы которые будут парсится из урл адреса
     например если вместо '/' указать '/{id}' то этот id попадёт в args
    */
    $response->getBody()->write('{}');
    // Здесь просто записываем конкретный текст внутрь тела отвера
    // и далее добавляем нужный header-заготово говоря что ответ это json
    return $response->withHeader('Content-Type', 'application/json');

});

$app->run(); // здесь и пойдёт запуск проекта с хождением "по описанным урлам"
```

Psr\Http\Message\ServerRequestInterface - информация о текущем запросе
это http-метод query get-параметры и т.д.

Psr\Http\Message\ResponseInterface        - черновая версия ответа сервера
которая в конце концов пойдёт клиенту

В приметивном случае так и работает slim-фреймворк
прописываем урлы и если надо аргументы в этом урле далее мапим к ним обработчики
здесь это делается прямым указанием функции которая принимает три параметра
первых два интерфейсы и 3й это аргументы из урла. Так же задаём http-метод
разрешенного http-соединения для урла get или post прямо в названии функции
создании мапинга урл-хэндлер


Теперь наша заглушка АПИ работает через slim-фреймворк, а не через echo

Любой http-фреймворк - имеет основной класс Application или Kernel, который
содержит в себе маршрутизатор что позволяет работать с маршрутами
(маршрут в смысле разные пути в урле которые мапятся к конкретным обработчикам)

Здесь в коде выше $app - и есть тот самый Kernel или Application. т.е. центральное
ядро куда регистрирум все свои маршруты для того чтобы при запуске приложения
и обработке запросов сам фреймворк разруливал какой из обработчиков вызывать
для обработки запроса, передав в этот обработчик обьекты текущего запроса и
будущего ответа клиенту + аргументы из урла.
Так же внутри этого $app может быть встроен обработчик ошибок.
Это и есть минимальный набор, который предоставляет любой из микрофреймворков.
Slim - и есть микрофреймворк и в нём как раз такой набор функционала и содержится.

## Как идёт запуск приложения $app->run()

Вот этот вот метод класса `Slim\App` и производит всю обработку запроса клиента.

./api/vendor/slim/slim/Slim/App.php:
```php
<?
class App {
    # ... $app->run():
    public function run(?ServerRequestInterface $request = null): void
    {
        if (!$request) {
            // здесь создаётся фабрика создающая обьект обёртку вокруг всех
            // стандартных php массивов: $_SERVER $_GET $_POST $_FILES и т.д.
            $serverRequestCreator = ServerRequestCreatorFactory::create();
            // здесь создание обьекта request-а и заполнение значениями
            $request = $serverRequestCreator->createServerRequestFromGlobals();
        }

        $response = $this->handle($request); // передаётся request на обработку
        // создаётся эммитер который будет отдавать ответ клиенту
        $responseEmitter = new ResponseEmitter();
        $responseEmitter->emit($response);  // здесь идёт вывод данных клиенту(echo)
    }
}
```

как работает ResponseEmitter: смотри здесь
./api/vendor/slim/slim/Slim/ResponseEmitter.php
там идёт просто добавление "печеать" header-заготовков http-ответа в
- private func `emitHeaders`(...)  через std фун-ю header($header_name, $value)
- private func `emitBody` через стандартную функцию `echo $data`

То есть по факту работа фрейморка базируется на тех же простых вещах - стандартных
функциях php таких как header, echo и стандартных массивов с данными о запросе.
просто это оборачивается в слои абстракции для ООП использования

На данном этапе установка фреймворка и начальных зависимостей завершена.
дальше надо будет добавить команду в Makefile для автоматической установки
всех зависимостей подпроетка `api`


### Update Makefile: Автоустановка всех зависимостей для composer.json в api

Теперь для того, чтобы при выполнении локального развёртывания проекта через
`make init` у нас полностью разворачивался весь проект со всеми нужными для
работы зависиммостями нужно
- сделать автоматическую установку composerа
- автоматическое подтягивание им всех зависимостей в каталог api/vendor

./Makefile:
```
init: docker-down-clear docker-pull docker-build docker-up api-init
...

api-init: api-composer-install

api-composer-install:
docker compose run --rm api-php-cli composer install
```
Так как у нас проект состоит из множества модулей(подпроектов).
таких как `api` `frontend` `gateway` то для удобвства и наглядности эти же
имена используем и для команд в Makefile чтобы иметь чёткую и понятную струкруту

Здесь так же сразу выносим отведльную мето-команду `api-init` для того чтобы
дальше можно было в неё добавлять дополнительные "шаги инициализации",
первый из которых который мы сейчас добавили - `compose install`

таким образом теперь при выполнении команды `make init` будет происходить
- остановка дев-окружения со всеми контейнерами приложения если они уже запущены
- полная пересборка всех образов с самого начала( естественно используя кэш докера)
- запуск всех контейнеров описанных в docker-compose.yml (как единого целого приложения)
- запуск команды `composer install` внутри контейнера api-php-cli для устновки
  всех зависимостей.

То есть как и задумано - одна команда make init приводит проект в рабочее состояние
из любой точки, либо когда ты только скачал репозиторий ну другую машину, либо
когда продолжаешь разработку и нужно просто пересобрать образы.


