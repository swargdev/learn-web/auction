## Создаём конфиги для сборки докер-образов под продакшен

делаем на основе конфига внутри каталога подмодуля, например `api`

копируем всё что есть из /docker/development в /docker/production
а далее уже внутри production вносим правки для того чтобы этот образ был уже
для боевого сервера а не для сервера разработки

получаем такую структуру для каталога api

```
api/
├── docker
│   ├── development
│   │   ├── nginx
│   │   │   ├── conf.d
│   │   │   │   └── default.conf
│   │   │   └── Dockerfile
│   │   └── php-fpm
│   │       ├── conf.d
│   │       │   └── security.ini
│   │       └── Dockerfile
│   └── production
│       ├── nginx
│       │   ├── conf.d
│       │   │   └── default.conf
│       │   └── Dockerfile
│       └── php-fpm
│           ├── conf.d
│           │   └── security.ini
│           └── Dockerfile
└── public
    └── index.php
```

Рассмотрим еще раз как используется NGINX для dev-а(разработки)

Докер файл для сборки дев-версии образа с nginx-ом:
[Nginx dev Dockerfile](./api/docker/development/nginx/Dockerfile)

```Dockerfile
FROM nginx:1.18-alpine

COPY ./conf.d /etc/nginx/conf.d

WORKDIR /app
```

в docker-compose.yml он используется так:

```yml
  api:
    build:
      context: api/docker/development/nginx
    volumes:
      - ./api:/app
    depends_on:
      - api-php-fpm
```
build.context - это директива "собери из указанного каталога"
как раз в нём и лежит Dockerfile для dev-nginx-а

`volumes: ./api:/app` - это монтирование каталога с приложением поверх образа
так чтобы изменения в коде приложения сразу отображались без пересборки образа.
поэтому здесь в докер файле для NGINX и нет никаких COPY с каталогом public
этот каталог попадает в образ через подключение(монтирование) volume-а

Для прода нужно каталог с кодом копировать прямо внутрь образа
Проблема в том что каталог `api/public` относительно пути который указываем
docker-compose для сборки `api/docker/development/nginx` на три уровня выше.
Использовать же такой переход на три уровня выше прямо в докерфайле не выйдет

    COPY ../../../public ./public

т.е. докер скажет что не может найти каталог public.
Это происходит потому что при сборке образов Докер собирает не из самого каталога
контектса который указан нами в docker-compose.yml:
build.context:  `api/docker/development/nginx` а путём копирования всего
содержимого этого каталога во временный каталог с рандомным именем вроде:

    /var/lib/docker/tmp/xxxx/

т.е. будет попытка обратится по пути

    /var/lib/public

Таким образом при сборке докерфайла нельзя ссылаться на ресурсы Файловой системы
которые лежат выше каталога из которого говорим собирать образ.
Решается это сменой корневого каталога откуда собираем образы.
В нашем случае здесь корневой надо указать директорию `api` и тогда во временный
каталог под сборку образа попадёт всё что в этом каталоге:


    /var/lib/docker/tmp/xxxx/docker/production/nginx/conf.d
    /var/lib/docker/tmp/xxxx/docker/production/nginx/Dockerfile
    /var/lib/docker/tmp/xxxx/docker/production/nginx/Dockerfile
    /var/lib/docker/tmp/xxxx/public

ну и соответственно надо будет переписать Dockerfile под новые пути к конфигам

```
WORKDIR /app              -- аналог mkdir /app && cd /app

COPY ./public ./public    -- копируем из текущего корня а это api/
                             внутрь текущего каталога контейнера т.е. /app/public
т.е. такая конструкция копирует весь код из api/public внутрь контейнера
```

Для PHP-FPM уже используем php.ini-production а не php.ini-development:

    RUN mv $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini

По аналогии так же сделать и для frontend

Для gateway настройка nginx-конфигов будет уже другая т.к. нужно использовать
разделение не по портам(dev) а по доменам(prod)
Т.е. здесь уже реальные конфигурации которые пойдут на рабочий сервер.

и по аналогии сделали копию затем вносим изменения. структура такая же
[dev frontend](./gateway/docker/development/conf.d/frontend.conf)
[prod frontend](./gateway/docker/production/conf.d/frontend.conf)

в этом конфиге уже надо настраивать ssl и сертификаты
делать будем это через certbot
получать сертификат с рабочего сервера, и затем будем примонтировать его внутрь
контейнера через волюмы по путям `/etc/letsencrypt`

```nginx
# redirect from www.domain into domain
server {
    listen 443 ssl http2;
    server_name www.demo-auction.my-domain.pro;
    server_tokens off;

    ssl_certificate /etc/letsencrypt/live/demo-auction.my-domain.pro/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/demo-auction.my-domain.pro/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/demo-auction.my-domain.pro/chain.pem;
    ...

    rewrite ^(.*) https://demo-auction.my-domain.pro$1 permanent;
}
```

в блоке server где идёт www. перед доменом как я понял это сделано для того,
чтобы перенаправлять запросы с поддомена на основной.
??
Походу если это не сделать то так могут сделать злоумышленники подделав сайт?


Resolver - здесь прописываем внутренний DNS Докера.
Эта штука нужна чтобы указать nginx-у куда ходить для проверки сертификатов.
без неё будет ругаться на сертификаты.

    resolver 127.0.0.11 ipv6=off;

Вспомогательные заголовки.
Всегда ипользуй https для этого сайта и не пытайся открывать по http
и запомни это на весь год: 31536000

    add_header Strict-Transport-Security "max-age=31536000";

Запрещаем открывать картинки css и js у которых вместо https прописан http

    add_header Content-Security-Policy "block-all-mixed-content";



## Letsencrypt постоянно автопродление сертификатов

Безплатный сертификат даётся на непродолжительный срок 90 дней примерно.
Нужно еще будет сделать автопродление сертификата
для этого надо будет предоставить возможность показывать спец каталог летсэнкрипта


    location /.well-known/acme-challenge {
        root /var/www/html;
    }

её разместим сюда. При каждом обновлении сертификата сдесь будет создаваться
сертботом(ему надо будет дать сюда доступ) новый случайный файл для проверки извне.
т.е по пути

    /var/www/html/.well-known/acme-challenge/xxx

Это для подтверждения владения сайтом.
Так же работают Google|Yandex Web Master Tools. нужно разместить некий файл у себя.


## Далее добавим команды для сборки прод-образов в Makefile

Для сборки образа нужно выполнить команду собрать и указать откуда собирать

    docker build api

здесь 3е слово `api` - имя каталого откуда собираем образ.

Но! т.к. Dockerfile находится не самом каталоге api, а внутри подкаталогов
api/docker/production то нужно указать явно путь к докер-файлу иначе не найдёт:

    docker build --file api/docker/production/Dockerfile api

Такая команда сгенерить образ со случайным именем. Зададим явное имя

    docker build --file api/docker/production/nginx/Dockerfile \
                 --tag auction-nginx api

При таком подходе не указав конкретную версию докер подставит `latest`:

    --tag auction-nginx:latest

Для того чтобы отличать разные сборки и нужны версии(Для отката например)

    docker build --file api/docker/production/nginx/Dockerfile \
                 --tag auction-nginx:1 api

    docker build --file api/docker/production/nginx/Dockerfile \
                 --tag auction-nginx:2 api

Ну и далее можно поднимать в проде эти образу под конкретной версией вот так:
docker-compose.yml:
```
varsion: '3.7'
services:
  gateway:
    image: auction_nginx:3
    depends_on: [frontend, api]
```

Ну и версию можно настроить через переменную окружения (EnvVar)

Далее может возникнуть нужда поднимать доп образы например для дев-стенда:

    --tag auction-nginx:master-1
    --tag auction-nginx:master-2
    --tag auction-nginx:feature-rbac-1

т.е. добавить префиксы для версий. Для того чтобы можно было хранить в одном
реестре множество разных образов без конфликтов имён

Для того чтобы запушить образ к себе в репозиторий команда


    docker push <название-образа-который-пушим>

Например при такой команде с таким именем докер попытается запушить образ на
DockerHub. Так как по умолчанию все образы с простыми именами идут туда.

    docker push auction_nginx:master-1

Если надо отправить(запушить-залить) образ в свой собственный рееджестри:
(selfhosted registry)

    docker push registry.my-domain.pro/auction_nginx:master-1

Поэтому при сборке образа нужно и имя указывать для образа полное:

    docker build --file ... --tag registry.mu-domain.pro/auction-nginx:master-1

Для пуша в приватный реджестри нужна аутентификация перед docker push:

    docker login registry.mu-domain.pro

Далее в интерактивном режиме реджестри запросит логин и пароль

    Username: ...
    Password:...

При успешном логине отпишет и сохранит логин и хэш пароля для последующего
повторного использования при работе с этим же реджестри.

На проде же при деплои для скачивания образа команда:

    docker pull registry.mu-domain.pro/auction-nginx:master-1

Ну и в лучших традициях - вместо хардкодинга конкретных имён испо-ем EnvVar:
указывая их везде во всех командах сборки, пуша и пула, пример:

    docker pull $REGISTRY/auction-nginx:$IMAGE_TAG

В команде build флаг `--pull` - говорит всегда скачивай обновлёённые версии
используемых базовых образов(FROM в Dockerfile) перед сборкой нашего образа.

    docker build --pull

Таким образом получаем такую команду для сборки образа `auction-gateway`:
```sh
docker --log-level=debug build --pull \
  --file=gateway/docker/production/nginx/Dockerfile \
  --tag=${REGISTRY}/auction-gateway:${IMAGE_TAG} \
  gateway/docker/production/nginx
```

Здесь:
- `gateway/docker/production/nginx` - путь к каталогу для сборки образа от корня
проекта. здесь входим глубже т.к. в этот образ идёт только nginx и не надо
копировать в образ public-каталог с кодом(не нужно переходить выше по каталогам).
- --file=`gateway/docker/production/nginx/Dockerfile` что собирать из этой дир-ии


если используем полный путь к nginx при билде

    docker build ... gateway/docker/production/nginx

тода докер файл такой же как для dev:
```Dockerfile
FROM nginx:1.18-alpine
COPY ./conf.d /etc/nginx/conf.d
WORKDIR /app
```

Для однотипности с api и frontend укажу для билда только каталог подпроекта:

    docker build ... gateway

```Dockerfile
FROM nginx:1.18-alpine
COPY ./docker/production/nginx/conf.d /etc/nginx/conf.d
WORKDIR /app
```




```Makefile
...
build: build-gateway build-frontend build-api

build-gateway:
	docker --log-level=debug build --pull --file=gateway/docker/production/nginx/Dockerfile --tag=${REGISTRY}/auction-gateway:${IMAGE_TAG} gateway/docker

build-frontend:
	docker --log-level=debug build --pull --file=frontend/docker/production/nginx/Dockerfile --tag=${REGISTRY}/auction-frontend:${IMAGE_TAG} frontend

build-api:
	docker --log-level=debug build --pull --file=api/docker/production/nginx/Dockerfile --tag=${REGISTRY}/auction-api:${IMAGE_TAG} api
	docker --log-level=debug build --pull --file=api/docker/production/php-fpm/Dockerfile --tag=${REGISTRY}/auction-api-php-fpm:${IMAGE_TAG} api
```

Так же добавим дев-команду для проверки билда всех этих образов
здесь в ней прописываются переменные окружения для подстановки в команды

```Makefile
try-build:
  REGISTRY=localhost IMAGE_TAG=0 make build
```

Пробуем собрать через команду

```sh
make try-build
```

Смотрим что получилось:
```sh
docker images
```
```
REPOSITORY                      TAG       IMAGE ID       CREATED          SIZE
localhost/auction-api           0         04f34e92ae2b   17 seconds ago   21.9MB
localhost/auction-api-php-fpm   0         2be61fb5c59f   24 seconds ago   77.3MB
localhost/auction-frontend      0         4526603c6129   35 seconds ago   21.9MB
localhost/auction-gateway       0         6d540287e0f2   42 seconds ago   21.9MB
...
```

Теперь для сборки готовых образов для прода нужно только указать переменные
окружения куда заливать и какой таг(имя) для образа назначать:

```sh
make REGISTRY=registry.my-domain.pro IMAGE_TAG=1 make build
```

ну и далее уже можно как обычно запушить образ в реджестри
docker push <подставить полное имя из вывода после сборки>

Здесь прямая аналогия с гитом. Код мы пушим в совой гит-репозиторий
так и здесь готовый образ заливается в свой реджестри - докерхаб или свой например

Теперь надо еще убрать мусор из готовых образов.
например PHPStorm создает в под-каталогах проекта свои временные данные
которые по умолчанию так же попадут в образы - это надо исправить.
.dockerignore - спец файл для этого по аналогии с .gitignore

```sh
echo '/.idea' > api/.dockerignore
echo '/.idea' > frontend/.dockerignore
echo '/.idea' > gateway/.dockerignore
```

далее в эти файлы будем добавлять исключения такие как логи vendor и проч.



