<?php

declare(strict_types=1);

use Doctrine\Migrations;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\Migrations\Tools\Console\Command;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

return [
    DependencyFactory::class => static function (ContainerInterface $container) {
        /** @var Doctrine\ORM\EntityManagerInterface $em */
        $em = $container->get(EntityManagerInterface::class);

        $conf = new Doctrine\Migrations\Configuration\Configuration();
        $conf->addMigrationsDirectory(
            'App\Data\Migration',
            __DIR__ . '/../../src/Data/Migration'
        );
        $conf->setAllOrNothing(true);
        $conf->setCheckDatabasePlatform(false);

        $sc = new Migrations\Metadata\Storage\TableMetadataStorageConfiguration();
        $sc->setTableName('migrations');

        $conf->setMetadataStorageConfiguration($sc);

        return DependencyFactory::fromEntityManager(
            new Migrations\Configuration\Migration\ExistingConfiguration($conf),
            new Migrations\Configuration\EntityManager\ExistingEntityManager($em)
        );
    },

    Command\ExecuteCommand::class => static function (ContainerInterface $ci) {
        /** @var Doctrine\Migrations\DependencyFactory $factory */
        $factory = $ci->get(DependencyFactory::class);
        return new Command\ExecuteCommand($factory);
    },
    Command\MigrateCommand::class => static function (ContainerInterface $ci) {
        /** @var Doctrine\Migrations\DependencyFactory $factory */
        $factory = $ci->get(DependencyFactory::class);
        return new Command\MigrateCommand($factory);
    },
    Command\LatestCommand::class => static function (ContainerInterface $ci) {
        /** @var Doctrine\Migrations\DependencyFactory $factory */
        $factory = $ci->get(DependencyFactory::class);
        return new Command\LatestCommand($factory);
    },
    Command\ListCommand::class => static function (ContainerInterface $ci) {
        /** @var Doctrine\Migrations\DependencyFactory $factory */
        $factory = $ci->get(DependencyFactory::class);
        return new Command\ListCommand($factory);
    },
    Command\StatusCommand::class => static function (ContainerInterface $ci) {
        /** @var Doctrine\Migrations\DependencyFactory $factory */
        $factory = $ci->get(DependencyFactory::class);
        return new Command\StatusCommand($factory);
    },
    Command\UpToDateCommand::class => static function (ContainerInterface $ci) {
        /** @var Doctrine\Migrations\DependencyFactory $factory */
        $factory = $ci->get(DependencyFactory::class);
        return new Command\UpToDateCommand($factory);
    },
    Command\DiffCommand::class => static function (ContainerInterface $ci) {
        /** @var Doctrine\Migrations\DependencyFactory $factory */
        $factory = $ci->get(DependencyFactory::class);
        return new Command\DiffCommand($factory);
    },
    Command\GenerateCommand::class => static function (ContainerInterface $ci) {
        /** @var Doctrine\Migrations\DependencyFactory $factory */
        $factory = $ci->get(DependencyFactory::class);
        return new Command\GenerateCommand($factory);
    },
];
