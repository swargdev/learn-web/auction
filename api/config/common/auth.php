<?php

declare(strict_types=1);

use App\Auth\Service\Tokenizer;
use Psr\Container\ContainerInterface;

return [

    Tokenizer::class => static function (ContainerInterface $container): Tokenizer {
        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{token_ttl:string} $config
         */
        $config = $container->get('config')['auth'];

        return new Tokenizer(new DateInterval($config['token_ttl']));
    },

    'config' => [
        'auth' => [
            'token_ttl' => 'PT1H',
        ],
    ],

    // JoinConfirmationSender::class =>
    // static function (ContainerInterface $container): JoinConfirmationSender {
    //     /** @var MailerInterface $mailer */
    //     $mailer = $container->get(MailerInterface::class);
    //
    //     /** @var FrontendUrlGenerator $frontend */
    //     $frontend = $container->get(FrontendUrlGenerator::class);
    //
    //     return new JoinConfirmationSender($mailer, $frontend, $twigEnv);
    // },
];
