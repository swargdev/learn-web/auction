## Возвращаем свою вёрстку уберая код сгенерированный через CRA

Теперь можно вернуть ту вёрстку которая у нас была первоночально до CRA
(CreateReactApp)

Убираем всё лишнее из шаблона
./frontend/public/index.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="icon" href="%PUBLIC_URL%/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta name="description" content="Auction" />
    <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />
    <title>Auction</title>
  </head>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"></div>
  </body>
</html>
```

Возвращаем свой логотип logo192.png и favicon.ico

Теперь вернём ту вёрстку которая у нас была раньше на нашей заглушке

- здесь главное осозновать что здесь теги похожие на html-ловские это
на самом деле jsx синтаксис, а не обычный html-код.
jsx - это собый синтаксис для быстрого и удобного создания React-овских
UI-комонентов и дерева вёрстки из них.

./frontend/src/App.jsx
```js
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="app">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
```

на свою вёрстку
./frontend/src/App.jsx
```js
import React from 'react';
import './App.css';

function App() {
  return (
    <div className="app">
      <div className="welcome">
        <h1>Auction</h1>
        <p>We will be here soon</p>
      </div>
    </div>
  );
}

export default App;
```

тоже самое делаем и для App.css заменив стили на свои из app.css

Сюда размещаем только стили для того кода который описываем в App.jsx
./frontend/src/App.css
```css
.app {
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
}

.welcone {
  display: block;
  vertical-align: middle;
  padding: 30px;
  background: #fff;
  box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.05);
  border-radius: 6px;
}

.welcome h1 {
  text-align: center;
  color: #444;
  font-weight: normal;
  font-size: 1.8rem;
  line-height: 1;
  margin: 0 0 20px 0;
}

.welcome p {
  text-align: center;
  color: #444;
  font-size: 0.9rem;
  line-height: 1;
  margin: 0;
}
```

А всю общую вёрстку для всего нашего шаблона (index.html?) помещаем отдельно
./frontend/src/index.css
```css
* {
  box-sizing: border-box;
}

html, body {
  height: 100vh;
  margin: 0;
  padding: 0;
}

html {
  line-height: 1.5;
  -webkit-text-size-adjust: 100%;
  font-size: 16px;
}

body {
  background-color: #f6f6f6;
  font-family: Arial, sans-serif;
}
```

Таким образом файл App.css будет описывать стили для кода в App.jsx
а index.css для шаблона public/index.html

```
frontend/src/
├── App.css
├── App.jsx
├── App.test.jsx
├── index.css
├── index.jsx
├── reportWebVitals.js
└── setupTests.js
```
