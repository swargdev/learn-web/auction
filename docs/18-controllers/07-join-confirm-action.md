## Реализуем Контроллер Join/ConfirmAction "Подтверждение регистрации"

Начинаем с фикстуры для теста.
Делаем не одного а сразу несколько юзеров:
- с валидным действующим токеном
- с истёкшим токеном

Для RequestTest токен генерили случайно т.к. он нам был не важен и использовали
в самих тестах только email. То вот при подтверждении регистрации значение
токена уже важно. именно поэтому их и делаем фиксированными - чтобы их же
потом использовать в запросе.


```php
<?
class ConfirmFixture extends AbstractFixture
{
    //константы для токенов подтверждения регистрации
    public const VALID = '00000000-0000-0000-0000-000000000001';
    public const EXPIRED = '00000000-0000-0000-0000-000000000002';

    public function load(ObjectManager $manager): void
    {
        // Valid  валидный - может подтвержить регистрацию

        $user = User::requestJoinByEmail(
            Id::generate(),
            $date = new DateTimeImmutable(),
            new Email('valid@app.test'),
            'password-hash',
            // валидный действующий и не истёкший токен
            new Token($value = self::VALID, $date->modify('+1 hour'))
        );

        $manager->persist($user);

        // Expired - токен истёк 2 часа назад

        $user = User::requestJoinByEmail(
            Id::generate(),
            $date = new DateTimeImmutable(),
            new Email('expired@app.test'),
            'password-hash',
            new Token($value = self::EXPIRED, $date->modify('-2 hours'))
        );

        $manager->persist($user);

        $manager->flush();
    }
}
```

Теперь пишем тест использующий эти фикстуры:


```php
<?php

declare(strict_types=1);

namespace Test\Functional\V1\Auth\Join;

use Ramsey\Uuid\Uuid;
use Test\Functional\Json;
use Test\Functional\WebTestCase;

class ConfirmTest extends WebTestCase
{
    // прописываем перед началом каждого тестовго метода
    protected function setUp(): void
    {
        parent::setUp();

        // подкгружать нашу новую фикстуру именно для этого теста
        $this->loadFixtures([
            ConfirmFixture::class, // в этой фикстуре два тестовых пользователя
        ]);
    }

    // дальше принимаем решение что адрес подтверждения у нас в API будет:
    // '/v1/auth/join/confirm'

    public function testMethod(): void
    {
        $response = $this->app()->handle(self::json('GET', '/v1/auth/join/confirm'));
        // для GET-запроса 405 - метод не поддерживается
        self::assertEquals(405, $response->getStatusCode());
    }

    // во всех других тестах уже будем передавать конкретные токены

    // при успехе возврат код 200 и пустая строка. - всё ок
    public function testSuccess(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join/confirm', [
            'token' => ConfirmFixture::VALID,// значение токена из константы
        ]));

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('', (string)$response->getBody());
    }

    public function testExpired(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join/confirm', [
            'token' => ConfirmFixture::EXPIRED,
        ]));

        // токен истёк вылетет  DomainException
        self::assertEquals(409, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        self::assertEquals([
            'message' => 'Token is expired.',// сообщение из DomainException
        ], Json::decode($body));
    }

    // "забыли передать токен"
    public function testEmpty(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join/confirm', []));

        self::assertEquals(409, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        self::assertEquals([
            'message' => 'Incorrect token.'
        ], Json::decode($body));
    }

    // при попытке передать случайный токен
    public function testNotExisting(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join/confirm', [
            'token' => Uuid::uuid4()->toString(),
        ]));

        self::assertEquals(409, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        self::assertEquals([
            'message' => 'Incorrect token.', // бракует и ничего не делает
        ], Json::decode($body));
    }
}
```


Спека готова реализуем Контроллер


Код контроллера Join/ConfirmAction будет очень похож на первый контроллер
Join/RequestAction т.к. их суть такая же просто меняются вызываемые обработчики
// это видно по неймспейсу классов Command и Handler

```php
<?
namespace App\Http\Action\V1\Auth\Join;

use App\Auth\Command\JoinByEmail\Confirm\Command;
use App\Auth\Command\JoinByEmail\Confirm\Handler;
//                               ^^^^^^^
use App\Http\EmptyResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ConfirmAction implements RequestHandlerInterface
{
    private Handler $handler;

    public function __construct(Handler $handler)
    {
        $this->handler = $handler;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @psalm-var array{token:?string} $data */
        $data = $request->getParsedBody();

        $command = new Command();
        // в команде должен быть только токен по которому должна пройти
        // подтверждение регистрации.
        $command->token = $data['token'] ?? '';
        // у прошлого контроллера здесь был емейл и пароль.

        $this->handler->handle($command);

        return new EmptyResponse(200); // код 200 - "ok"
                                       // у RequestAction был код 201 - "Created"
    }
}
```


## Регистрация адресов API к контроллера

- v1/auth/join
- v1/auth/join/confirm

В маршрутизаторе адреса API можно записать в виде простого списка:

```php
<?
$app->post('/v1/auth/join', \App\Http\Action\V1\Auth\Join\RequestAction::class);
$app->post('/v1/auth/join/confirm', \App\Http\Action\V1\Auth\Join\ConfirmAction::class);
```


Slim позволяет прописывать их еще и в виде групп:

./api/config/routes.php
```php
<?php

declare(strict_types=1);

use App\Http\Action;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return static function (App $app): void {
    $app->get('/', \App\Http\Action\HomeAction::class);

    $app->group('/v1', function (RouteCollectorProxy $group): void {
        $group->group('/auth', function (RouteCollectorProxy $group): void {
            $group->post('/join', Action\V1\Auth\Join\RequestAction::class);
            $group->post('/join/confirm', Action\V1\Auth\Join\ConfirmAction::class);
        });
    });
};
```

- `v1` - родительская группа для других поддгрупп
   сначала идёт имя затем функция-кэллбэк с подгруппами и подэлементами

```php
<?
namespace Slim\Routing;
class RouteCollectorProxy implements RouteCollectorProxyInterface {
public function group(string $pattern, $callable): RouteGroupInterface {}
```


Контроллеры(Экшены) для регистрации говотвы

Все остальные контроллеры, для всех остальных, оставшихся комманд модуля Auth
(аутентификации), можно дописать по такой же аналогии.

Но на данный момент у на нет никакой валидации воода юзеров.
Все попытки ввести неправильные данные в контроллер просто вызывают исключения
которые прилетают из обьектов-обёрток выдвая просто InvalidArgumentException.
При этом самому юзеру возвращается 500-я ошибка Internal Server Error и всё
без каких либо подсказок что он делалает не так. В общем нам нужно добавить
валидацию ввода, для отображения конечным пользователям.
- Например пароль не должен быть пустым,
- email должен быть валидным и т.д

Такую валидацию ввода можно как писать самому, так и использовать для этого
готовую библиотеку. Дальше будем заниматься подключением валидации
