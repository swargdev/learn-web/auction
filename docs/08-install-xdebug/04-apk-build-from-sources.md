﻿# Оптимальный способ в Apline собирать пакеты из исходников для докер-образов

- Оптимизируем размер наших dev-образов.(Установка Xdebug из исходников)
- Что такое `apk add --virtual .build-deps ... && apk del -f .build-deps`
- что такое `apk add --update ...`

## До оптимизации просто через 'apk add --no-cache':

```
REPOSITORY         TAG     IMAGE ID       CREATED         SIZE
site-api           latest  0686fbe205de   2 minutes ago   21.9MB
site-api-php-fpm   latest  4daef9657e90   2 minutes ago   358MB        <<
site-api-php-cli   latest  8d6da56f569e   4 hours ago     382MB        <<
site-gateway       latest  4968567c1740   2 days ago      21.9MB
site-frontend      latest  5aef2a6f33fa   2 days ago      21.9MB
```

## apk add --no-cache --virtual .build-deps

Оптимизированное создание образа используя `--virtual .build-deps`:

```Dockerfile
RUN apk update && apk add --no-cache --virtual .build-deps \
    autoconf g++ make linux-headers \
    && pecl install xdebug-3.2.2 \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable xdebug \
    && apk del -f .build-deps

# ...
```

Размер образов стал в несколько раз меньше!
```
REPOSITORY         TAG     IMAGE ID       CREATED          SIZE
site-api           latest  d756d0d7b4ae   28 seconds ago   21.9MB
site-api-php-fpm   latest  ecacaf11b0f4   29 seconds ago   81.3MB     <<
site-api-php-cli   latest  bf2adae80c19   28 seconds ago   103MB      <<
site-gateway       latest  4968567c1740   2 days ago       21.9MB
site-frontend      latest  5aef2a6f33fa   2 days ago       21.9MB
```

Как это работает:
 - для инсталляции пакетов из исходников нужен компилятор и его зависимости
   поэтому до сборки из исходников нужно скачать эти пакеты с компилятором
 - как правило в итоговом образе сам компилятор и его зависимости не нужны,
   но при обычной установке через apk add --no-cache они остаются в слоях
   итогового докер-образа.
 - чтобы убрать все эти ненужные пакеты и используется флаг --virtual.
 - до установки пакетов которые затем будем удалять указываем
   `apk add --virtual .build_deps ...packages`
 - в конце после сборки из исходников просто удаляем более не нужные пакеты
   `apk del -f .build-deps`
 - делать одной командой инчае лишние файлы запекутся в слои образа


Способ найден здесь:

https://stackoverflow.com/questions/73097246/how-to-add-xdebug-to-php8-1-fpm-alpine-docker-container

Пример кода из указанной статьи
```Dockerfile
RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS \
    && pecl install xdebug-3.0.0 \
    && docker-php-ext-enable xdebug \
    && apk del -f .build-deps
```


## Что такое apk add --virtual .build-deps ... && apk del -f .build-deps

[What is .build-deps for apk add --virtual command?](https://stackoverflow.com/questions/46221063/what-is-build-deps-for-apk-add-virtual-command)

В общем эта штука позволяет облегчать образы так, чтобы в них не запекались
на веки вечные сами компиляторы, которые используются при компиляции нужных
вещей в момент создания докер-образа по Dockerfile-у.
И так же важно чтобы удаление этого .build-deps шло в одной команде RUN иначе
оно "запечётся" в слой на постоянку. Ведь команда удаления из отдельного RUN
просто наложится поверх предыдущего слоя где уже будут впечатаны ненужные в
образе файлы

Пример использования `--virtual`
```Dockerfile
RUN apk add --no-cache --virtual .build-deps \
    gcc freetype-dev musl-dev \
    && pip install --no-cache-dir <packages_that_require_gcc...> \
    && apk del .build-deps
```

What that means is when you install packages, those packages are not added to
global packages. And this change can be easily reverted. So if I need gcc to
compile a program, but once the program is compiled I no more need gcc.

I can install gcc, and other required packages in a virtual package and all of
its dependencies and everything can be removed this virtual package name. Below
is an example usage
```Dockerfile
RUN apk add --virtual mypacks gcc vim \
 && apk del mypacks
```

The next command will delete all 18 packages installed with the first command.
In docker these must be executed as a single `RUN` command (as shown above),
otherwise it will not reduce the image size.

Also worth mentioning that it's important to use a unique name for a virtual
package, not existing in currently configured repositories, otherwise
installing packages succeeds but doesn't install what you need.

Note: you must execute it in one RUN command, else it cannot be deleted from
the previous Docker image layer


`.build-deps` is an arbitrary name to call a "virtual package" in Alpine,
where you will add packages.

It creates an extra 'world' of packages, that you will need for a limited
period of time (e.g. compilers for building other things).

Its main purpose is to keep your image as lean and light as possible,
because you can easily get rid of it once those packages were used.

Please remember that it should be included in the same RUN if you want to
achieve the main purpose of lightweight.


## Что такое "apk --update add .." (Alpine Linux)
## Explanation of the "apk --update add" command for Alpine Linux
(Объяснение того что значит флаг --update)

`apk --update add some_pkg` === `apk update && apk add some_pkg`

To get the latest list of available packages, use the `update` command.
it is similar to the Debian `apt-get update`
that you do before `apt-get install my_package`.

https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management#Update_the_Package_list

Adding the `--update-cache`, or for short `-U` switch to another apk command,
as in `apk --update-cache upgrade` or `apk -U add ...`,
the command has the same effect as first running `apk update`
before the other apk command
[Source](https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management#Update_the_Package_list)


