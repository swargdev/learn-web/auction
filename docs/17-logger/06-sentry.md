
- Внешний Агрегатор логов
- Способы как можно сделать оповещение об ошибках
- Продвинутая система отправки логов - сервис "Агрегатор логов"
- Подключение Sentry.io к своему проекту
- Создаём класс-декоратор SentryDecorator
- Почему наследовались от ErrorHandler и создали LogErrorHandler


## Внешний Агрегатор логов

Для того чтобы глянуть логи на прод-сервере надо заходить на прод-сервер
и либо запускать команду `docker compose logs` либо идти в `/var/lib/docker/`
туда куда докер сохраняет логи в json формате. Искать CONTAINER_ID для нужного
контейнера и открывать и смотреть файл с логом.

Проблема еще в том, что если на прод-сервере возникнет ошибка, то узнаем мы об
этом не сразу, а только когда пойдём смотреть логи.
Нужно оповещение об ошибках.


## Способы как можно сделать оповещение об ошибках

1) Используя возможности monolog-логгера настроить доп хэндлер так,
чтобы либоги с ошибками он отправлял например нам в телегармм.
Чтобы оповещение об возникшей ошибке прилетало в меседджер сразу.
Но такой подход не оч хорош, потому что такой хэндлер обычно просто будет слать
сообщение о каждой возникшей ошибке, сколько бы их небыло.
Так если ошибка будет долбить слишком часто. Скажем на сервер зашло 100 человек
и у всех посыпались ошибки.
То при этом обычный хэндлер просто пришлёт все эти 100 ошибок как есть.


## Продвинутая система отправки логов - сервис "Агрегатор логов"

Вместо ручной отправки логов в некий меседжер  используем готовый агрегатор,
которому так же можно отправлять сообщения с ошибками и исключениями.
Но реагирование на эти исключения более разумное и продвинутое.

Можно написать такую систему самому, а можно использовать уже готовый сервис

https://sentry.io/ - один из подобных сервисов.
https://github.com/getsentry/sentry

Это сервис с удобной панелькой куда можно передавать исключения из своего кода.
Кроме самих исключений можно передавать и некие события происходящие на сайте.

Два способа как им можно пользоваться:
- напрямую с сайта, есть бесплатный тариф (на месяц?) до 300 исключений в день.
- запустить локально у себя на машине.
  есть готовый докер-образ. запустил у себя этот докер образ на виртуалке
  и у тебя self-hosted собственная копия сервиса `sentry.io`:

https://develop.sentry.dev/self-hosted/
https://github.com/getsentry/self-hosted

Так для своего высоконагруженного сайта можно сразу ставить себе селф-хостет
вариант через докер-контейнер и пользоваться на всю катушку без лимитов и
платной подписки.


## Подключение Sentry.io к своему проекту
v24 00:41:00

- В UI Project-> New Project -> PHP -> give project name
- composer require sentry/sdk:2.1.0
- в самом коде `Sentry\init(['dsn'=>'https://HASH/@sentry.io/ID'])`
  здесь передаётся секретный dsn для инициализации библиотеки
- инициализацию размещать в два места public/index.php и bin/app.php
  сразу после автозагрузчика классов `vendor/autoload.php`
- после этого все исключения будут отправляться на сервер `sentry.io`
- смотреть прилетевшие сообщения об ошибках нужно на вкладке `issues`
- еще надо будет настроить отправку исключений из ErrorMiddleware
  т.к. он так же перехватывает исключения и до сентри они не долетят
  то есть настроить ErrorMiddleware так чтобы он еще и в Sentry Их слал.
- Symfony\Component\Console\Application - (симвоневское cli-приложение)
  так же у себя внутри отлавливает все исключения и они тоже не попадут в sentry.
  есть параметр `catchExceptions` с дефолтным true это для того чтобы
  каждый вызов команды оборачивать в try-catch и ошибки выводить в оформленном
  красивом виде.
  То есть для прода в bin/app.php нужно отключать отлов исключений:

```php
<?
  Sentry\init([/*передача секретного dsn */]);
  $cli = new Symfony\Component\Console\Application('Console');
  $cli->setCatchExceptions(false);
```

Так же лучшим вариантом не хардкодить секретный dsn а передавать его через ENVVAR:

api/bin/app.php
```php
<?
require __DIR__ . '/../vendor/autoload.php';

if ($dsn = env('SENTRY_DSN')) {    // будет включаться только для прода
    Sentry\init(['dsn' => $dsn]);  // когда будет передаваться не пустое ENV_VAR
}

$cli = new Application('Console');

if (getenv('SENTRY_DSN')) { // выключать отлов исключений самим приложением
    $cli->setCatchExceptions(false); // только если Sentry есть и работает
}
```
переменную окружения `SENTRY_DSN` - при этом надо будет добавлять в docker-compose

Для того чтобы отправлять исключения в sentry Из LogErrorHandler
достаточно просто добавить передачу исключения из writeToErrorLog()

```php
<?php
namesapece App\ErrorHandler;
#...
use function Sentry\captureException;  // короткий алиас для вызова функиии

class LogErrorHandler extends ErrorHandler
{
    protected function writeToErrorLog(): void
    {
        captureException($this->exception); // передача исключения в sentry

        $this->logger->error($this->exception->getMessage(), [
            'exception' => $this->exception,
            'url' => (string)$this->request->getUri(),
        ]);
    }
}
```
Sentry\init - это функция создающая Singletone Sentry обьекта,
captureException будет работать через этот синглтон.

Но есть более правильный способ как это можно сделать без захломления
класса LogErrorHandler.

## Создаём класс-декоратор SentryDecorator

этот класс будет реализовывать тот же интерефейс что и сам ErrorHandler -
ErrorHandlerInterface.
При этом суть этого класса - просто быть посредником, принимающим в своём
конструкторе следующий по цепочке ErrorHandler, который он должен будет вызывать
после совей работы.

```php
<?php

declare(strict_types=1);

namespace App\ErrorHandler;

// класс декоратор
final class SentryDecorator implements ErrorHandlerInterface
{
    // здесь новый синтаксис php 8.0 поля класса обьявляются в конструкторе
    // и идёт присваивание значений переданных из конструктора в сами поля
    public function __construct(
        // первый параметр - это хэндлер который этот класс декоратор оборачивает
        private readonly Slim\Interfaces\ErrorHandlerInterface $next
    ) {
    }

    // класс с единственной функцией при вызове будет вызваться именно этот метод:
    public function __invoke(
        Psr\Http\Message\ServerRequestInterface $request,
        \Throwable $exception,
        bool $displayErrorDetails,
        bool $logErrors,
        bool $logErrorDetails
    ): Psr\Http\Message\ResponseInterface {
        // ради этой строки и создаём этот класс-декоратор:
        Sentry\captureException($exception);// послать исключение на сервер

        // next - это следующий по цепочке хэндлер который мы здесь и оборачиваем
        // "декарируем". здесь идёт его вызов с передачей тех же параметров
        return ($this->next)(
            $request,
            $exception,
            $displayErrorDetails,
            $logErrors,
            $logErrorDetails
        );
    }
}
```
Суть этот класса-декоратора - быть посредником-прослойкой между кодом
который его вызывает и настоящим хэндлером который этот код должен вызывать.

Как это работает:
- принять параметры вызова (здесь это через метод `__invoke`)
- выполнить свою работу на основе принятых данных
- вызвать следующего(next) по цепочке обработчика
  (хранится в поле класса под типом интерфейса - принимается из конструктора)

api/config/erorrs.php
```php
<?
    $middleware->setDefaultErrorHandler(
        new SentryDecorator( // оборачиваем в посредник-класс-декоратор
            // наш обработчик ошибок
            new LogErrorHandler($callableResolver, $responseFactory, $logger)
        )
    );
```

Зачем такое оборачивание в класс-декоратор?
- сохраняется принцип одной ответственности
- такой код проще и удобнее тестировать.
  тесты для обоих классов можно писать отдельно, т.к функционал разделён на классы
  и не перемешивается.
- просто создавать моки-заглушки в тестах SentryDecorator вокруг ErrorHandler
  чтобы проверять правильно возвращаемых значений


Другой способ решения этой же задачи - не создавать класс декоратор
SentryDecorator а унаследоваться от своего LogErrorHandler.
Но наследование слеплевает функционал во едино в классе наследнике, а значет
лишвает возможность написания отдельных независимых тестов.
Так если мы наследуем SentryErrorHandler от LogErrorHandler и в LogErrorHandler
пишем тесты для обоих, а потом со временем нужно изменить внутреннее поведение
LogErrorHandler то придется переписывать тесты для обоих классов.
Поэтому наследование принято использовать только в редких случаях, когда
нет других вариантов как переопределить нужное поведение.


## Почему наследовались от ErrorHandler и создали LogErrorHandler

Потому что другого выхода как переопределить поведение чтобы в лог кроме
сообщения попадали доп параметры на данный момент не было(?).
(что мы переопределили в методе writeToErrorLog)

Если бы Slim\Handlers\ErrorHandler поддерживал бы некий ErrorLogger класс
и мог принимать его в своём конструкторе, то можно было бы не наследоваться
от ErrorHandler а использовать "композицию" то есть написать свой собственный
ErrorLogger-класс и передвать уже его в конструктор стандартного ErrorHandler-а

