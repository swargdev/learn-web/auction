## К чему пришли

Полностью реализовали написание наших контроллеров(экшенов):
- добавли к ним валидацию входных данных
- переводы ошибок валидации и доменных исключений на другие языки
- научились искать и использовать готовые библиотеки и готовый код
  (подключение Middleware\ContentLanguage из сторонней библиотеки)

Таким образом когда пишем код по рекомендациям PSR:
- можем использовать наработки сторонних разработчиков, реализующие стандартные
  PSR-овские интерфейсы
- можем переиспользовать свой код в разных проектах,
  которые поддерживают насивание Middleware и Handler-ов в формате PSR

То есть написав все эти миддлверы:
```
api/src/Http/Middleware/
├── ClearEmptyInput.php
├── ContentLanguage.php          <<
├── DomainExceptionHandler.php   <<
├── TranslatorLocale.php         <<
└── ValidationMiddleware.php
```
их можно будет переиспользовать в других своих проектах на других фреймворках
или микрофреймворках.

Например если взять любой фреймворк или микрофрейморк такие как
Zend-Expression, Lumen(из мира Laravel), Laravel то и в них тоже можно будет
использовать эти свои миддлверы. Можно взять свои написанные классы под PSR
стандарт и опубликовать в отдельном репозитории в виде отдельных пакетов.
Причем важно наличие тестов - т.к. не нужно будет переписывать каждый раз
тесты под них т.к. в этих пакетах они уже будут.
То есть можно создавать и переиспользовать свои пакеты в других своих проектах.
либо если один большой проект разбили на несколько микросервисов и вся общая
кодавая база разнесена на несколько репозиториев.
Это позволит переиспользовать частоповторяющийся код, без копипасты.
Таким образом мидлверы написанные в универсальном проекто независимом виде,
реализующие стандартный PSRовские интерфесы - это дверь к переиспользованию,
а значит экономии времени и сил.

Если присмотреться к ./api/src/Http/Middleware/DomainExceptionHandler.php
то можно заметить что этот мидлвер пока не является проектно-независимым
т.к. внутри себя использует наш собственный JsonResponse из нашего проекта
а значит выложить этот класс в виде независимого пакет не получится.
Т.к. здесь внутри него идёт зависимость от JsonResponse который в свою очередь
зависит(наследуется от) SlimResponse.
Таким образом чтобы можно было сделать класс DomainExceptionHandler независимым
чтобы его можно было отдельно выкложить на гитхаб нужно его переписать убрав
зависимость.

Как это обычно делается:

Перепишим DomainExceptionHandler так чтобы его можно было в виде независимого
пакета опубликовать в отдельном репозитории и на packagist.org/
```php
<?
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class DomainExceptionHandler implements MiddlewareInterface
{
    private LoggerInterface $logger;
    private TranslatorInterface $translator;
    // фабрика через которую будем создавать response
    private Psr\Http\Message\ResponseFactoryInterface $factory;

    public function __construct(
        LoggerInterface $logger,
        TranslatorInterface $translator,
        ResponseFactoryInterface $factory
    ) {
        $this->logger = $logger;
        $this->translator = $translator;
        $this->factory = $factory;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        try {
            return $handler->handle($request);
        } catch (DomainException $e) {
            $this->logger->warning($e->getMessage(), [
                'exception' => $e,
                'url' => (string)$request->getUri(),
            ]);

            $data = [ 'message' =>
                $this->translator->trans($e->getMessage(), [], 'exceptions')
            ];

            // создаём response через стандартную фабрику
            $response = $this->factory->createResponce(409)
                ->withHeader('Content-Type', 'application/json');

            // "ручное" записывание с преобразованием обьекта в json-строку
            $response->getBody()
                ->write(json_encode($data, JSON_THROW_ON_ERROR, 512));

            return $response;
        }
    }
```
Принцип здесь простой - не использовать классы из своего проекта,
у себя мы использовали в частности `App\Http\JsonResponse`

а использовать только стандартные широкоиспользуемые PSR-овские:
`Psr\Http\Message\ResponseFactoryInterface`

так же можно использовать и интерфейсы из симфони-комонентов
`Symfony\Contracts\Translation\TranslatorInterface`
так как наборы подобных интерфейсов не привязывает код именно к самому
симфони-фреймоврку, это просто отдельный компанент не более того.

Таким образом выложив свой код на packagist.org позволит подключать этот код
как отдельный компонент, ныне уже стандартным способом через `composer requeire`

Дальше уже в своём новом коде, после подключения своего же пакета нужно будет
только прописать фабрику для DI-контейнера чтобы указать какой конкретно
класс использовать для создания инстансов `ResponseFactoryInterface`:

Пример кода обьявления фабрики в своём новом конфиге
```php
<?
use Psr\Http\Message\ResponseFactoryInterface;
use Slim\Psr7\Factory\ResponseFactory;// конкретная фабрика из фреймворка проекта

return [
    ResponseFactoryInterface::class => // универсальный PSR-овский интерфейс
    static function (ContainerInterface $container) {
        // здесь использвуется конкретная фабрика того фреймворка, что используется
        // в проекте для которого и прописывается этот конфиг-файл
        return new ResponseFactory(); // Слимовский класс конкретная реализация
    }
];
```

Так же еще нужно будет переписать тесты для DomainExceptionHandler - для
передачи в него третьего параметра - фабрики ResponseFactory

Вывод.
Теперь ты знаешь как писать миддлверы так чтобы их можно было переиспользовать
в других своих проектах не через копи-пасту а как надо через подключение
пакетов(библиотек-компонентов) с помощью composer require

Можно даже прописать в composer.json чтобы пакеты подгружались из конкретного
своего репозитория.


Дальше нужно будет по аналогии реализовать все остальные контроллеры для Auth
модуля и функциональные тесты для них. (и нужными для тестов фикстурами)
В итоге получим полноценный готовый и протестированный API для модуля Auth.
Ну и дальше уже можно будет добавлять новые модули, новые контроллеры, тесты.
