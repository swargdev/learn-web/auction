<?php

declare(strict_types=1);

namespace App\Http\Test\Unit\Middleware;

use App\Http\Middleware\ValidationMiddleware;
use App\Http\Test\Unit\WebUTestCase;
use App\Http\Validator\ValidationException;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * @covers ValidationMiddleware
 */
class ValidationMiddlewareTest extends WebUTestCase
{
    public function testNormal(): void
    {
        $middleware = new ValidationMiddleware();

        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn($source = self::createResponse());

        $response = $middleware->process(self::createRequest(), $handler);

        self::assertEquals($source, $response);
    }

    public function testException(): void
    {
        $middleware = new ValidationMiddleware();

        $violations = new ConstraintViolationList([
            new ConstraintViolation('Incorrect Email', null, [], null, 'email', 'not-email'),
            new ConstraintViolation('Empty Password', null, [], null, 'password', ''),
        ]);

        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')
            ->willThrowException(new ValidationException($violations));

        $response = $middleware->process(self::createRequest(), $handler);

        self::assertEquals(422, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        /** @var array $data */
        $data = json_decode($body, true, 512, JSON_THROW_ON_ERROR);

        self::assertEquals([
            'errors' => [
                'email' => 'Incorrect Email',
                'password' => 'Empty Password',
            ],
        ], $data);
    }
}
