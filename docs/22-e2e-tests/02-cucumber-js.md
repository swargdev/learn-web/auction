## Cucumber-js Установка
https://github.com/cucumber/cucumber-js

Cucumber is a tool for running automated tests written in plain language.
Because they're written in plain language, they can be read by anyone on your
team. Because they can be read by anyone, you can use them to help improve
communication, collaboration and trust on your team.


> Install
https://github.com/cucumber/cucumber-js/blob/main/docs/installation.md
- With npm:
```sh
npm install @cucumber/cucumber
```
- With Yarn:
```sh
yarn add @cucumber/cucumber
```

Работать он у нас будет на nodejs через докер контейнер по тому же принципу что
и наш фронтенд. Поэтому сначала создаём докер-образ с nodejs:

./cucumber/docker/development/node/Dockerfile
```Dockerfile
FROM node:18-alpine

WORKDIR /app
```

Теперь добавляем новый контейнер-сервис в `./docker-compose.yml`
```yaml
  cucumber-node-cli:
    build:
      context: cucumber/docker/development/node
    volumes:
      - ./cucumber:/app

```


```sh
docker compose run --rm cucumber-node-cli yarn add @cucumber/cucumber
```
./docs/22-e2e-tests/log/install-cucumberjs.log
du -hcd 1 cucumber/node_modules/ - `34Mb`

```sh
docker images
REPOSITORY                 TAG            IMAGE ID       CREATED        SIZE
site-gateway               latest         f383566f8572   2 days ago     21.9MB
site-frontend              latest         2ae8f9cb7e63   2 days ago     21.9MB
site-api-php-cli           latest         c8982f3ea8e3   2 days ago     105MB
site-api-php-fpm           latest         a734c351eba4   2 days ago     82.7MB
site-frontend-node         latest         48b243e28d98   3 days ago     132MB
site-frontend-node-cli     latest         4926de67dec6   3 days ago     132MB
site-cucumber-node-cli     latest         1b4be5094bf9   3 days ago     132MB
```
Похоже для образа site-cucumber-node-cli используется слой из другого образа
site-cucumber-node-cli

```
[cucumber-node-cli internal] load metadata for docker.io/library/node:18-alpine
[cucumber-node-cli 1/2] FROM docker.io/library/node:18-alpine@sha256:
                                 b1a0356f7d6b86c958a06949d3db3f7fb27f95f627aa61
CACHED [cucumber-node-cli 2/2] WORKDIR /app
```

Т.е идёт переиспользование образа и не создаётся еще дополнительных 132 мб

После установки package.json выглядет так:
```json
{
  "license": "MIT",
  "dependencies": {
    "@cucumber/cucumber": "^10.1.0"
  }
}
```

## Makefile добавляем команды для запуска

Прописываем команду которая будет инициализировать на чистом git clone
все наши e2e тесты на cucumber
```Makefile
init: docker-down-clear \
  api-clear frontend-clear \
  docker-pull docker-build docker-up \
	api-init frontend-init cucumber-init

cucumber-init: cucumber-yarn-install

cucumber-yarn-install:
	docker compose run --rm cucumber-node-cli yarn install
```


Добавляем команду для запуска наших e2e тестов

```json
{
  "license": "MIT",
  "scripts": {
    "e2e": "cucumber-js"
  },
  "dependencies": {
    "@cucumber/cucumber": "^10.1.0"
  }
}
```

Теперь добавляем test-e2e и перед запуском самого cucumber-e2e запускаем
накатку фикстур на бд(чтобы API было в правильном состоянии с имеющимся
зареганы юзером)
```Makefile
init: docker-down-clear \
  api-clear frontend-clear \
  docker-pull docker-build docker-up \
	api-init frontend-init cucumber-init

test-e2e: api-fixtures cucumber-e2e

# ...

cucumber-e2e:
	docker compose run --rm cucumber-node-cli yarn e2e
```

Пробуем запустить новую команду
```sh
make test-e2e
docker compose run --rm api-php-cli composer app fixtures:load
> php bin/app.php --ansi 'fixtures:load'
Loading fixtures
purging database
loading App\Auth\Fixture\UserFixture
Done.
docker compose run --rm cucumber-node-cli yarn e2e
yarn run v1.22.19
$ cucumber-js


0 scenarios                                                   # (1)
0 steps                                                       # (2)
0m00.000s (executing steps: 0m00.000s)
Done in 0.95s.
```
Всё отработало как надо но отписало что 0 сценариев (1) и 0 шагов(2)

Теперь можно приступить к написанию e2e тестов

Для nvim можно поставить cucumber-language-server (lsp)
https://github.com/cucumber/language-server


