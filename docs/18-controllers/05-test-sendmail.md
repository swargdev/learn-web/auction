## Проверка отправки писем на мейлер

### MailHog API и что с ним можно делать

Наш Обработчик(Handler) JoinByEmail, отвечающий за регистрацию по почте,
должен отправлять на указанный адрас письмо с подтверждением регистрации.

Зайдя в MailHog можем убедится что письма действительно отправлялись
и что содержимое этих писем было правильное:

http://localhost:8082/:
```
Auction new-user@app.test   Join Confirmation   17 minutes ago   3.51 kB
Auction new-user@app.test   Join Confirmation   23 minutes ago   3.51 kB
Auction new-user@app.test   Join Confirmation  ...
...
```

```
Confirm your email:

http://localhost:8080/join/confirm?token=c37b34ed-bfec-48e6-bb1e-a0a70dd8dd5b
```

Но пока в наших тестах нет автоматизации проверки их отправления.
И если что-то по отправке писем поломается невозможно будет сразу об этом узнать
А значит нужна проверка идёт ли отправка писем на адрес или нет.


## Добавляем в тесты проверку отправки писем на мейлер (MailHog)

Для такого рода проверок в MailHog есть API через которое можно с ним работать:

HTTP API to list, retrieve and delete messages

    See APIv1 and APIv2 documentation for more information

- https://github.com/mailhog/MailHog/blob/master/docs/APIv1.md
- https://github.com/mailhog/MailHog/blob/master/docs/APIv2.md

Смотрим что там есть в APIv1:

### MailHog API v1

The v1 API is a RESTful HTTP JSON API.

`GET /api/v1/events` Streams new messages using EventSource and chunked encoding

`GET /api/v1/messages` Lists all messages excluding message content

`DELETE /api/v1/messages` Deletes all messages
Returns a 200 response code if message deletion was successful.

`GET /api/v1/messages/{ message_id }`
Returns an individual message including message content


`DELETE /api/v1/messages/{ message_id }` Delete an individual message
Returns a 200 response code if message deletion was successful.

`GET /api/v1/messages/{ message_id }/download` Download the complete message

`GET /api/v1/messages/{ message_id }/mime/part/{ part_index }/download`
Download a MIME part

`POST /api/v1/messages/{ message_id }/release` Release the message to an SMTP server

Send a JSON body specifying the recipient, SMTP hostname and port number:

```json
{
	"Host": "mail.example.com",
	"Post": "25",
	"Email": "someone@example.com"
}
```
Returns a 200 response code if message delivery was successful.



То есть есть возможность как получать содержимое мейлера так и очищать собранные
им письма

Если нужно искать нужное письмо по отправителю нужно использовать APIv2

Вторая версия API описана в виде swagger-а
https://github.com/mailhog/MailHog/blob/master/docs/APIv2/swagger-2.0.yaml
APIv2 содержит в себе только методы:

- /api/v2/messages
- /api/v2/search     - для поиска нужного элемента

Чтобы что-то найти можно передать путь `/api/v2/search`
и к нему в query в под параметрах передать что хотим искать

Пример как это работает:
К локальному адресу на котором висит установленный MailHog добавим api/v2/messages
Либо в браузере либо через curl
```
http://localhost:8082/api/v2/messages
```
в ответ на это получим список всех сообщений в json-формате.
их можно распарсить в своём коде и как-то обработать.

Чтобы запрашивать конкретное письмо
смотрим описание:

```yaml
swagger: '2.0'

info:
  version: "2.0.0"
  title: MailHog API

paths:
  /api/v2/messages:
    get:
        ...
  /api/v2/search:                                # << нужный нам метод API
    get:
      description: |
        Search messages
      parameters:
        -
          name: kind                             # < тип по какому полю искать
          in: query
          description: Kind of search
          required: true
          type: string
          enum: [ from, to, containing ]         # варианты типа
        -
          name: query                            # указать строкой что скать
          in: query
          description: Search parameter
          required: true
          type: string
        -
          name: start
          in: query
          description: Start index
          required: false
          type: number
          format: int64
          default: 0
        -
          name: limit
          in: query
          description: Number of messages
          required: false
          type: number
          format: int64
          default: 50
      responses:
        200:
          # ...
```

Таким образом чтобы у себя в тесте экшена проверить идёт ли отпрака письма:

по API идёт на MailHog и запрашиваем найти письмо по адресу получателя
```php
<?
$query = '?query=new-user@app.test@kind=to'; // найти по адресу получателя
$json = file_get_contents('http://mailer:8025/api/v2/search' . $query);
$data = Json::decode($json);

// ответом будет список найденых сообщений, где total - число найденых писем
self::assertGreaterThan(0, $data['total']); // письмо отправилось - т.е найдено
//письмо с заданным адресом отправителя
```

Проверяем работает ли этот код в тесте:
убедившить что работает изменяем адрес теста и проверяем что тест упадёт
показав что значение найденых писем равно 0

```sh
docker compose run --rm api-php-cli composer test -- --testsuite=functional
> phpunit --colors=always '--testsuite=functional'
PHPUnit 10.4.1

There was 1 failure:

1) Test\Functional\V1\Auth\Join\RequestTest::testSuccess
Failed asserting that 0 is greater than 0.                            # <<<<
                      ^
/app/tests/Functional/V1/Auth/Join/RequestTest.php:44
```

А в начале теста нужно очищать все письма в MailHog чтобы он удалял уже имеющиеся
пришедшие из прошлых тестов:

Но для удобства использования мейлера в тестах сделаем свой класс для него:
Свой собственный мейлер-клиент.
При этом ходить по Http адресам можно как через стандартные функции php
file_get_contents
так и через более удобные http-клиенты, из дополнительных библиотек.

Рассматривали библиотеку Sentry у неё есть зависимость от http-клиента

`guzzlehttp/guzzle`
https://github.com/guzzle/guzzle

Если sentry не ставили но хотим использовать эту библиотеку либо если ставили
но хотим быть уверены что в будущем наш код не сломается если вдруг уберём
библ-ку sentry тогда ставим её явно через:

```sh
composer require --dev guzzlehttp/guzzle
docker compose run --rm api-php-cli composer require --dev guzzlehttp/guzzle
```

--dev т.к. она будет нужна только в dev-test-окружениях


```
./composer.json has been updated
Running composer update guzzlehttp/guzzle
Loading composer repositories with package information
Updating dependencies
Lock file operations: 4 installs, 0 updates, 0 removals
  - Locking guzzlehttp/guzzle (7.8.1)
  - Locking guzzlehttp/promises (2.0.2)
  - Locking guzzlehttp/psr7 (2.6.2)
  - Locking psr/http-client (1.0.3)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 4 installs, 0 updates, 0 removals
  - Downloading psr/http-client (1.0.3)
  - Downloading guzzlehttp/psr7 (2.6.2)
  - Downloading guzzlehttp/promises (2.0.2)
  - Downloading guzzlehttp/guzzle (7.8.1)
  - Installing psr/http-client (1.0.3): Extracting archive
  - Installing guzzlehttp/psr7 (2.6.2): Extracting archive
  - Installing guzzlehttp/promises (2.0.2): Extracting archive
  - Installing guzzlehttp/guzzle (7.8.1): Extracting archive
2 package suggestions were added by new dependencies, use `composer suggest` to see details.
Generating autoload files
Using version ^7.8 for guzzlehttp/guzzle

api/vendor/  59M

api/vendor/guzzlehttp/
128K  api/vendor/guzzlehttp/promises
496K  api/vendor/guzzlehttp/guzzle
348K  api/vendor/guzzlehttp/psr7
976K  api/vendor/guzzlehttp/
```


./api/tests/Functional/MailerClient.php
```php
<?php

declare(strict_types=1);

namespace Test\Functional;

use GuzzleHttp\Client;

final class MailerClient
{
    private Client $client;

    public function __construct()
    {
        $this->client = new Client([
             // базовый адрес Http-клиента задаём с адресом нашего мейлера
            'base_uri' => 'http://mailer:8025',
        ]);
    }

    // метод для удаления всех сообщений из мейлера
    public function clear(): void
    {
        $this->client->delete('/api/v1/messages');
    }

    // метод для проверки наличия письма по адресу получателя
    public function hasEmailSentTo(string $to): bool
    {
        // методом GET к базовому адресу т.е. http://mailer:8025 вызывам
        // поиск письма по полю получателя
        $response = $this->client->get('/api/v2/search?kind=to&query=' . urlencode($to));
        /** @var array{total:int} $data */
        $data = json_decode((string)$response->getBody(), true, 512, JSON_THROW_ON_ERROR);
        return $data['total'] > 0; // хотябы одно письмо найдено
    }
}
```

Написав свой MailerClient добавим его в свой WebTestCase:


```php
<?php
class WebTestCase extends TestCase
{
    private ?App $app = null;
    private ?MailerClient $mailer = null;
    // ...

    protected function mailer(): MailerClient // геттер по аналогии с app()
    {
        if ($this->mailer === null) {
            $this->mailer = new MailerClient();
        }
        return $this->mailer;
    }
```

Теперь наш собтсвенный клиент готов к использованию в коде тестах


```php
<?
class RequestTest extends WebTestCase {

    public function testSuccess(): void
    {
        $this->mailer()->clear(); // очищаем всю почту до самой проверки

        $new_mail = 'new-user@app.test';
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join', [
            'email' => $new_mail,
            'password' => 'new-password',
        ]));

        self::assertEquals(201, $response->getStatusCode());
        self::assertEquals('', (string)$response->getBody());

        // проверяем что письмо было отправлено
        self::assertTrue($this->mailer()->hasEmailSentTo($new_mail));
    }
```

в итоге низкоуровневый код
```php
<?php
        $query = '?query=new-user@aXpp.test&kind=to';
        $json = file_get_contents('http://mailer:8025/api/v2/search' . $query);
        $data = Json::decode($json);

        self::assertGreaterThan(0, $data['total']);

        // заменили на краткий и лаконичный вызов:
        self::assertTrue($this->mailer()->hasEmailSentTo($new_mail));
```

Убедится воочию что апи действительно удаляет письма перед тестом можно
зайдя в WEB-UI MailHog и увидить что будет только одно письмо - последнее из
теста.




