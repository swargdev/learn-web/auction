## Попытка использовать транзакции в тестах провалилась

Думал до введения очередей запускать тесты обёрнутые в транзакции
чтобы после каждого теста шел откат до изначального состояния.
Но заставить работать транзакции пока не удалось:

```
1) Test\Functional\V1\Auth\Join\RequestTest::testMethod
Doctrine\DBAL\ConnectionException: There is no active transaction.

/app/vendor/doctrine/dbal/src/ConnectionException.php:17
/app/vendor/doctrine/dbal/src/Connection.php:1503
/app/tests/Functional/V1/Auth/Join/RequestTest.php:28
```

```
There were 5 failures:

1) Test\Functional\V1\Auth\Join\RequestTest::testMethod
Failed asserting that false is true.

/app/tests/Functional/V1/Auth/Join/RequestTest.php:21
```

```php
<?
class RequestTest extends WebTestCase {

    protected function setUp(): void
    {
        parent::setUp();

        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        // $em->getConnection()->setAutoCommit(false);

/* 21 */self::assertTrue($em->getConnection()->isTransactionActive()); // fail

        $this->loadFixtures([
            RequestFixture::class,
        ]);
    }

    protected function tearDown(): void
    {
/* 28 */$this->getEntityManager()->getConnection()->rollBack();// error:
        // Doctrine\DBAL\ConnectionException: There is no active transaction.

        parent::tearDown();
    }
```


