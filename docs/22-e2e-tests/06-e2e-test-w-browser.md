## Реализуем наши E2E тесты

После того как установили chromium-бразузер и биб-ку puppeteer
всё готово для дальшейшей реализации е2е тестов:

На данный момент у нас есть:
```sh
cucumber/features/
├── home.feature
└── step_definition
    └── home.js
```
- одна спека `cucumber/features/home.features` описывающая один сценарий
- заготовка самой реализации под эту спеку (поведенческую характеристику)
  `cucumber/features/step_definition/home.js`

./cucumber/features/home.features:
```feature
Feature: View home page
  In order to check home page content
  As a guest user
  I want to be able to view home page

  Scenario: View home page content
    Given I am a guest user
    When I open home page
    Then I see welcome block
```

./cucumber/features/step_definition/home.js
```js
const { Given, When, Then } = require('@cucumber/cucumber');

Given('I am a guest user', function () {
  return 'pending';
});

When('I open home page', function () {
  return 'pending';
});

Then('I see welcome block', function () {
  return 'pending';
});
```

Здесь у нас каждый шаг(step) имеет своё имя(строка-предложение) к которому
мы соотносим анонимную функцию внутри которой и нужно реализовать тест

- Given('I am a guest user', ..
 здесь описаывается начальное состояние входа в сценарий.
 но при обычном открытии страницы пользователь и так будет считаться гостем
 поэтому можно просто убрать весь код из анан ф-и т.к. делать ничего не надо:

```js
Given('I am a guest user', function () {});
```


```js
// захожу на главную страницу
When('I open home page', function () {
  return 'pending';
});

// и должен увидить welcome-блок на этой странице
Then('I see welcome block', function () {
  return 'pending';
});
```

Запуская наш е2е тест мы передаём управление тестовому фреймворку Cucumber
у него есть чёткие правила по которым он работает.
Так при обработке сценария одной конктерной фичи
(фича - файла с описанием поведенческих характеристик на Gherkin-языке)
cucumber бдует поэтапно друг за другом вызывать сценарии, а внутри сценариев шаги
Причем шаги имеют четкую последовательность вызовов такую же как описана в
фича-файле: Given, When, Then:
```
  Scenario: View home page content
    Given I am a guest user
    When I open home page
    Then I see welcome block
```

у себя в реализации е2е тесты мы для каждого этого шага передаём анонимную ф-ю
которая и будет вызвана тестовым фреймворком cucumber-ом.

Причем между шагами одного сценария cucumber позволяет иметь общее состояние
```
    When I open home page    - здесь мы должны открыть страницу
    Then I see welcome block - а здесь проверить на на этой странице что-то есть
```

Cucumber выполняет анонимные функции в контексте одного некого обьекта,
этот обьект в понятиях Cucumber обычно называют `world` - мир
world - это просто обьект для хранения состояния(значений в полях этого обьекта.
состояния, которое нужно использовать между шагами(step-ами) в одном сценарии.

Вот пример того зачем нужно хранить состояние между шагами:
```js
When('I open home page', function () {
  this.page = '/';  // this - это и есть ссылка на этот обьект world
  ...
});

Then('I see welcome block', function () {
  this.page ... // доступ к полю обьекта world заданному в прошлом шаге
});
```

Упрощенно это можно изобрать так:
```js
const world = {} //обьект для хранения промежуточного состояния

When('I open home page', function () { world.page = '/'; });
Then('I see welcome block', function () { console.log(word.page) });
```

и вызовы наших анон фун-й которые мы передаём в Given When Then происходят в
контексте этого обьекта `world`. Поэтому и доступна возможность обращаться к
обьекту world через ключевое слово `this` как мы делаем в примере выше.

Note:
Как помним в js ООП не было реализовано сразу с выходом языка, а было добавлено
относительно недавно, причем со своими специфичными вещами.
одна из таких ключевых особенностей js - `потяря this`
когда внутри функций this меняется на обьект внутри которой эта функция
запускается. (Поэтому даже добавили метод func.bind(this) для привязки
конкретного обьекта чтобы "не терять this")

В общем тестовом фреймворке cucumber написанные нами анон. ф-и для шагов:
Given, When, Then будет вызывать внутри обьекта world, это обьект позволяет
в его полях хранить промежуточное состояние доступное в разных шагах одного
сценария. Причем для каждого нового сценария создаётся новый инстанс world.


Вообще Cucumber позволяет описывать обьект world заранее, до запуска e2e тестов
в отдельном js-файле по конкретному пути `features/support/world.js`

./cucumber/features/support/world.js
```js
import { setWorldConstructor, World } from '@cucumber/cucumber'

function CustomWorld() { // описываем функцию для создания нашего world
  this.browser = null
}

// прописываем нашу функцию по созданию обьекта world в фреймворке Cucumber
// для того чтобы сам фреймворк по этой функции создавал обьект world с нужными
// нам свойствами (полями и значениями в них
setWorldConstructor(CustomWorld)
```

После этого сам Cucumber перед запуском каждого сценария будет создавать новый
обьект world по нашему описанию

Т.к. мы используем `puppeteer` то перед запуском каждого сценария нам нужно
создавать браузер чтобы запускать в нём наши е2е тесты.

Писать создание лаунчера можно где угодно, хоть в шагах сценария т.е.

```js
When('I open home page', function () {
  this.browser = await puppeteer.launch();
  ...
});
```
Но более разумно браузер "создавать" перед каждым сценарием т.к. он будет
нужен во всех шагах и пересоздавать(переподключаться) к браузеру на каждом
шаге - лишний расход ресурсов и времени.

Для этих целей в Cucumber есть "хуки", которые можно навесить для выполнения
в нужный момент времени. Например до выполнения сценария Before или после After

./cucumber/features/support/hooks.js

```js
const puppeteer = require('puppeteer')
const { Before, After } = require('@cucumber/cucumber')

Before(async function () { // срабатывать перед каждым сцерарием
  // создать обьект браузер (через подключение к установленному браузеру)
  this.browser = await puppeteer.launch({
    // параметры запуска браузера
    args: [
      '--disable-dev-shm-usage', // для параллельного запуска тестов
      '--no-sandbox'             // смотри ссылку
    ]
  })
})

After(async function () { // после каждого сценария
  await this.browser.close() // закрывать открытый браузер
})
```
закрытие браузера нужно для того чтобы состояние браузера полностью сбрасывалось
т.е. очищались куки, история и прочие вещи.
Для того чтобы каждый вход на страницу сайта проходил с чистого листа,
например для того чтобы после авторизации браузер не запоминал логин и пароль
и не вволдил запомненые данные для автовхода на сайт


Описав создание world и создав хук для подключения к браузеру через puppeteer
можно написать открытие страницы для шага When('I open home page'

Для этого добавляем новое поле `page`(страницы) в `world`
./cucumber/features/support/world.js
```js
import { setWorldConstructor, World } from '@cucumber/cucumber'

function CustomWorld() {
  this.browser = null
  this.page = null
}
```
Теперь это поле можно использовать в шагах сценариев
```js
When('I open home page', async function () {
  this.page = await this.browser.newPage() // создаём новую страницу
  await this.page.setViewport({ width: 1280, height: 720 }) // размер экрана
  return await this.page.goto('http://gateway:8080') // переход на адрес сайта
});
```
Здесь мы входим на сайт через обращение к имени сервиса `gateway`
Эти тесты будут проходить внутри докер контейнера а значит внутри докер-сети
а при подьёме "сервисов"-контейнеров через `docker cmpose up` все они начинают
работать в одной общей сети, где имя каждого такого сервиса, которые описаны
в нашем docker-compose.yml, внутри докер-сети прописываются как dns-имена к
которым можно обращаться как к полноценным именам обычных сайтов.
Так вот т.к. работа идёт внутри докер-сети вообще можно сразу идти не на
gateway а на frontend на 80й порт, но мы у себя проверяем именно всю связку
всего нашего приложения. И gateway(шлюз) является одной из её частей которую
тоже надо проверять.


Вообще создание страниц будет использвоться у нас достаточно часто
поэтому лучше переместить этот код в хук перед запуском шагов нового сценария:

./cucumber/features/support/hooks.js
```js
Before(async function () {
  this.browser = await puppeteer.launch({
    args: [
      '--disable-dev-shm-usage',
      '--no-sandbox'
    ]
  })
  // создание вкладки и указание её размера
  this.page = await this.browser.newPage()
  await this.page.setViewport({ width: 1280, height: 720 })
})
```
home.js
```js
When('I open home page', async function () {
  // теперь вкладка в браузере будут создавать в хуке перед запуском сценария
  // а здесь мы просто открываем нужный адрес сайта
  return await this.page.goto('http://gateway:8080');
});
```


## Реализуем тест для шага "Then I see welcome block"

Первое что нам понадобится - это библиотека для реализации утверждений в тестах.
Например в нашем frontend-е мы для тестов используем либу Jest и внутри неё
в основном используется функция `expect`.
Для Cucumber мы же уже установили биб-ку `chai` которая так же будет
предоставлять нам функционал для написания тестовых утверждений через вызов
функции expect.

Вообще на 2023 год в README.md на гитхабе проекта выложен такой пример
https://github.com/cucumber/cucumber-js
```js
const assert = require('assert')                     // биб-ка для утверждений
const { When, Then } = require('@cucumber/cucumber')
const { Greeter } = require('../../src')

When('the greeter says hello', function () {
  this.whatIHeard = new Greeter().sayHello()
});

Then('I should have heard {string}', function (expectedResponse) {
  assert.equal(this.whatIHeard, expectedResponse)
});
```

Дописаываем шаг для Then
```js
const { expect } = require('chai'); // !

When('I open home page', async function () {
  return await this.page.goto('http://gateway:8080');
});

Then('I see welcome block', async function () {
  // берём открытую в прошлом шаге страницу this.page
  // и ожидаем появление нужного нам селектора(по имени html-класса)
  // по умолчанию ожидание идёт 5 секунд.
  await this.page.waitForSelector('.welcome')
  // после получения селектора можем выполнить любой js код для того чтобы
  // достать сам текст из нужного нам селектора
  const callbackGetText = function(el) { return el.textContent }
  // (el) = > el.textContent
  const text = await this.page.$eval('.welcome h1', callbackGetText)
  // вытащив значение поля из нужного html-тега проверяем что текст равен тому
  // что мы ожидаем
  expect(text).to.eql('Auction')
});
```


и пробуем запустить е2е тест
```sh
make test-e2e
...
docker compose run --rm cucumber-node-cli yarn e2e

1) Scenario: View home page content # features/home.feature:6
   ✖ Before # features/support/hooks.js:4
       Error: function timed out, ensure the promise resolves within 5000 milliseconds
           at Timeout.<anonymous> (/app/node_modules/@cucumber/cucumber/lib/time.js:54:20)
           at listOnTimeout (node:internal/timers:569:17)
           at process.processTimers (node:internal/timers:512:7)
```

Эта ошибка возникает когда истекает таймаут а браузер не успевает поднятся:

Cucumber takes by default 5000 ms for asynchronous calls.
We can modify this in the below-mentioned manner.
This should go under the support directory in your framework for better practice.
Try using this:

```js
const { setDefaultTimeout } = require('@cucumber/cucumber');
setDefaultTimeout(parseInt(process.env.DEFAULT_TIMEOUT) || 60000);
```

В общем нужно переопределить дефотный тайм-аут в 5 секунд
```js
const puppeteer = require('puppeteer')
const { Before, After, setDefaultTimeout } = require('@cucumber/cucumber')

setDefaultTimeout(10 * 1000) // ждать до 10 секунд (тайм-аут по умолчанию - 5 сек)

// на "создание" открытие браузера максимум давать 30 секунд вместо 5 дефолтных
Before({ timeout: 30000 }, async function () {
  this.browser = await puppeteer.launch({
  args: [
    '--disable-dev-shm-usage',
      '--no-sandbox'
    ]
  })
  this.page = await this.browser.newPage()
  await this.page.setViewport({ width: 1280, height: 720 })
})

After(async function () {
  await this.browser.close()
})
```

## Первый запуск E2E теста. Проблема динамических css-классов в React

```sh
docker compose run --rm cucumber-node-cli yarn e2e
yarn run v1.22.19
$ cucumber-js

...F.

Failures:

1) Scenario: View home page content # features/home.feature:6
   ✔ Before # features/support/hooks.js:6
   ✔ Given I am a guest user # features/step_definition/home.js:3
   ✔ When I open home page # features/step_definition/home.js:6
   ✖ Then I see welcome block # features/step_definition/home.js:10
       Error: function timed out, ensure the promise resolves within 10000 milliseconds
           at Timeout.<anonymous> (/app/node_modules/@cucumber/cucumber/lib/time.js:54:20)
           at listOnTimeout (node:internal/timers:569:17)
           at process.processTimers (node:internal/timers:512:7)
   ✔ After # features/support/hooks.js:17

1 scenario (1 failed)
3 steps (1 failed, 2 passed)
0m18.656s (executing steps: 0m18.641s)
error Command failed with exit code 1.
```

Тест упал на шаге `Then` где идёт поиск нужного текста на странице
- `Then I see welcome block` # features/step_definition/home.js:10

Происходит это потому, что мы ищем по css-классу `welcome`, а на деле в нашем
фронтенде идёт использование динамически генерируемых css-классов:

```js
import React from 'react';
import styles from './Welcome.module.css';
//     ^^^^^^ подключается в виде некого модуля

function Welcome() {
  return (
    //              vvvvvvvvvvvvvv вместо простого css-класса 'welcome'
    <div className={styles.welcome}>
      <h1>Auction</h1>
      <p>We will be here soon</p>
    </div>
  );
}
export default Welcome;
```
Но при этом хотя в самом Welcome.mode.css css-класс описан как `.welcome`
но при "компиляции" точнее при сборке React исходников для прода генерируется
своего рода css-имя-класс+неймспейс+хэш

```html
<div class="app">
  <div class="Welcome_welcome__tQMjo">
    <h1>Auction</h1>
    <p>We will be here soon</p>
  </div>
</div>
```
В данном случае имя css-класса будет Welcome_welcome__tQMjo,
причём по мере разработке такое имя css-класса будет меняться.
Мы уже это обсуждали при написании фронтенда на React. И говорили о том, что
такой подход упрощает разработку большого числа компонентов, решая задачу
конфикта имён для UI-компонентов и их css-классов.
Когда такой подход не используются и прописываются полные и статические имена
классов - тогда и в е2е тестат используют эти же статические имена css-классов.
Но для подхода разработки при использовании динамической генерации css-классов
нужен некий другой способ обращения к html-элементам на странице.

И решается это либо добавлением к элементу еще одного css-класса(один html-тег
может иметь список из нескольких css-классов разделённых пробелами)
Либо добавлением "своего собственного атрибута". - `data-test`
и часто для e2e тестов и используют именно такой подход:

Добавляем "якорь" для e2e теста `data-test="welcome"`
./frontend/src/components/Welcome.jsx
```js
import React from 'react';
import styles from './Welcome.module.css';

function Welcome() {
  return (
    <div data-test="welcome" className={styles.welcome}>
      <h1>Auction</h1>
      <p>We will be here soon</p>
    </div>
  );
}

export default Welcome;
```
И в сам Е2Е тест поиск в селекторе делают уже по новому атрибуту:

```js
Then('I see welcome block', async function () {
  // await this.page.waitForSelector('.welcome') by css-class
  await this.page.waitForSelector('[data-test=welcome]')
  const callbackGetText = function(el) { return el.textContent }
  // const text = await this.page.$eval('.welcome h1', callbackGetText)
  const text = await this.page.$eval('[data-test=welcome] h1', callbackGetText)
  expect(text).to.eql('Auction')
});
```

Теперь е2е тест проходит
```js
docker compose run --rm cucumber-node-cli yarn e2e
yarn run v1.22.19
$ cucumber-js
.....
1 scenario (1 passed)
3 steps (3 passed)
0m01.965s (executing steps: 0m01.952s)
Done in 3.15s.
```

## Атрибут data-test в html-тегах
- используется когда css-классы в html-тегах генерятся динамически с разными
  названиями, что делает невозможным их использование
- может быть полезен при измнении дизайна и вёрстки фронтенд-сайта
  так же делает е2е тесты более независимыми от названий css-классов
  уберегая от поломок е2е тестов при работе над фронтендом и измнении css-классов

Поэтому в доках ко многим Е2Е системам есть рекомандация использовать не имена
css-классов, а создавать свои уникальные html-атрибуты и ориентировать на них.

При запуске тестов выводится вот такое вот уведомление

Puppeteer old Headless deprecation warning:
In the near future `headless: true` will default to the new Headless mode
for Chrome instead of the old Headless implementation. For more
information, please see https://developer.chrome.com/articles/new-headless/.
Consider opting in early by passing `headless: "new"` to `puppeteer.launch()`
If you encounter any bugs, please report them to
https://github.com/puppeteer/puppeteer/issues/new/choose.

Добавляем опцию в создание браузера, чтобы скрыть это уведомление:

./cucumber/features/support/hooks.js
```js
Before({ timeout: 30000 }, async function () {
  this.browser = await puppeteer.launch({
    headless: 'new', /* Скроет постоянный вывод ворнингов */
    args: [
      '--disable-dev-shm-usage',
      '--no-sandbox'
    ]
  })
  this.page = await this.browser.newPage()
  await this.page.setViewport({ width: 1280, height: 720 })
})
```


В итоге у нас есть один фича-файл и реализованный тест для него, так же
нас е2е тест успешно проходит. Причем сам тест фактически происходит внутри
запускаемого chromium-браузера который у нас стартует внутри контейнера в
headless режиме(без UI).
