- Настройка проекта на использование логгера monolog
- Варианты как прокинуть свой логгер в Slim\ErrorHandler
- Рефакторинг. Выносим ErrorMiddleware из addErrorMiddleware в DI-контейнер
- Создаём фабрику для ErrorMiddleware


## Выход на то как можно настроить логгер monolog для проекта

Наша задача сделать так, чтобы ошибки(стектрейсы) в тестах писались в файлы.
Для этого нужно подключить выбранный нами логгер monolog к Slim\ErrorHandler
так чтобы этот хэндлер ошибок использовал именно наш логгер настроенный так как
нам нужно.

Для этого просмотрим исходник самого `Slim`-фреймворка,
Чтобы понять как там всё устроено и как установить в него свой логгер.


Вот наш конфиг файл где мы настраиваем мидлвейр.

./api/config/middleware.php
```php
<?
return static function (App $app, ContainerInterface $container): void {
    /** @psalm-var array{debug:bool, env:string} */
    $config = $container->get('config');
    $app->addErrorMiddleware($config['debug'], $config['env'] != 'test', true);
    //    ^^^^^^^^^^^^^^^^^^ goto definition место добавления мидл-вейра
};
```
здесь и есть зацепка через которую можно быстро через goto-definition
добраться до нужного места в исходнике.

Еще раз находим нужный нам метод `writeToErrorLog`
(очередной пример того как исследовать исходники в каталоге vendor)

goto-definition track:
- `$errorMiddleware = new ErrorMiddleware`
- `\process()  return $this->handleException`
- `$handler = $this->getErrorHandler`
- `return $this->getDefaultErrorHandler`

есть метод:

./api/vendor/slim/slim/Slim/Handlers/ErrorHandler.php
```php
<?
namespace Slim\Handlers;

class ErrorHandler implements ErrorHandlerInterface {

    protected function writeToErrorLog(): void // вот интересующий нас метод
    {
        # ....
        $this->logError($error);               // $this->logger->error($error);
        //     ^^^^^^^^ здесь и идёт запись инфы в лог через ^
    }

    public function __construct(
        CallableResolverInterface $callableResolver,
        ResponseFactoryInterface $responseFactory,
        ?LoggerInterface $logger = null         // <<< вот как передаётся логер
    ) {
        $this->callableResolver = $callableResolver;
        $this->responseFactory = $responseFactory;
        $this->logger = $logger ?: $this->getDefaultLogger();
    }
```

Ради интереса можно посмотреть что за getDefaultLogger используется:

./api/vendor/slim/slim/Slim/Handlers/ErrorHandler.php
```php
<?
namespace Slim\Handlers;
class ErrorHandler implements ErrorHandlerInterface {
    protected function getDefaultLogger(): LoggerInterface
    {
        return new Logger(); // это Slim\Logger
        //         ^ goto-definition
    }
```
И вот стандартный дефолтный логгер встроенный в Slim-фреймворк
```php
<?
namespace Slim;
use function error_log;

class Logger extends Psr\Log\AbstractLogger
{
    public function log($level, $message, array $context = []): void
    {
        // тупо выводить все сообщения без учёта уровня логирования.
        error_log((string) $message);
        // ^ та самая стандартная php-функция которая просто пишет в StdErr
    }
}
```


## Варианты как прокинуть свой логгер в Slim\ErrorHandler

Нужно настроить стандартный служебный Slim-овский ErrorHandler-а
на использование нашего логгера monolog.
Для того чтобы переопередить то как ErroHandler записывает сообщения в лог.

Смотрим рецепт из документации
[Документация](https://slimframework.com/docs/v4/middleware/error-handling.html)

дока говорит о том что есть два пути как это можно сделать:
- через наследование от стандартного класса ErrorHandler для того,
  чтобы переопределить любой нужный protected метод(logError или writeToErrorLog)
- либо просто передав PSR-ровский `LoggerInterface`:

```php
<?php
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use MyApp\Handlers\MyErrorHandler;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

// Add Routing Middleware
$app->addRoutingMiddleware();

// Monolog Example
$logger = new Logger('app');
$streamHandler = new StreamHandler(__DIR__ . '/var/log', 100);
$logger->pushHandler($streamHandler);

// Add Error Middleware with Logger
$errorMiddleware = $app->addErrorMiddleware(true, true, true, $logger);
//                                     прямая передача логгера ^^^^^^     <<<<<

// ...

$app->run();
```

Но Будем наследоваться и переопределять метод `writeToErrorLog`.
для более тонкой и подвинутой возможности настроить логирование.


## Рефакторинг. Выносим ErrorMiddleware из addErrorMiddleware в DI-контейнер

А для того, чтобы можно было переопеределить ErrorHandler на свой класс
нужно переписать(отрефакторить) наш текущий конфиг - настройку DI-контейнера.
(`api/config/**/*.php`)


На данный момент ErrorMiddleware к нашему Slim-приложению подключается так:

./api/config/middleware.php
```php
<?
// здесь $app - это Slim\App т.е. инстанс нашего слимовского приложения
return static function (App $app, ContainerInterface $container): void {
    /** @psalm-var array{debug:bool, env:string} */
    $config = $container->get('config');
    $app->addErrorMiddleware($config['debug'], $config['env'] != 'test', true);
    // ^   ^ то как мы к своему слим-приложению подключаем ErrorMiddleware
    //  \наше Slim-приложение
};
```

Смотрим на метод addErrorMiddleware снова
./api/vendor/slim/slim/Slim/App.php
```php
<?
namespace Slim;
class App extends RouteCollectorProxy implements RequestHandlerInterface {

    // по сути это метод является неким хелпером создающим обьект ErrorMiddleware
    // добавляющим созданный обьект во внутренний список мидлвейров и
    // возвращающий созданный обьект
    public function addErrorMiddleware(
        bool $displayErrorDetails,
        bool $logErrors,
        bool $logErrorDetails,
        ?LoggerInterface $logger = null
    ): ErrorMiddleware {
        $errorMiddleware = new ErrorMiddleware(  // создание обьекта
            $this->getCallableResolver(),
            $this->getResponseFactory(),
            $displayErrorDetails,
            $logErrors,
            $logErrorDetails,
            $logger
        );
        // здесь идёт просто добавление созданного мидлвейра в список всех своих
        // мидлвейров
        $this->add($errorMiddleware);
        return $errorMiddleware;// ну и возврат созданного обьекта
    }
```

а значит если этот хелпер возращает сам обьект ErrorMiddleware то можно:

```php
<?
return static function (App $app, ContainerInterface $container): void {
    /** @psalm-var array{debug:bool, env:string} */
    $config = $container->get('config');
    // получить инстнас из нами используемого метода
    $errorMiddleware = $app->addErrorMiddleware( /* тоже самое */);
    // а дальше можно в этом errorMiddleware переопеределить ErrorHandler
    // смотрим в исходник класса ErrorMiddleware чтобы узнать как это сделать
};
```

Дальше можно обнаружить в исходнике сеттер на переопеределение ErrorHandler
./api/vendor/slim/slim/Slim/Middleware/ErrorMiddleware.php
```php
<?
class ErrorMiddleware implements MiddlewareInterface {
    # ...
    public function setDefaultErrorHandler($handler): self
    {
        $this->defaultErrorHandler = $handler;
        return $this;
    }
```

поэтому передать хэндлер можно так

./api/config/middleware.php
```php
<?
return static function (App $app, ContainerInterface $container): void {
    /** @psalm-var array{debug:bool, env:string} */
    $config = $container->get('config');
    //
    $errorMiddleware = $app->addErrorMiddleware( /* тоже самое */);
    // здесь мы создадим свой собственный класс LogErrorHandler наследник
    // и передаём его обьект внутрь errorMiddleware:
    $errorMiddleware->setDefaultErrorHandler(new LogErrorHandler($deps));
    // так же при создании инстанса нашему обьекту^ нужно передать ^ зависимости
}
```
Делать инстанцирование LogErrorHandler прямо в файле middleware.php не лучший
вариант. т.к. нужно будет из контейнера доставать `LoggerInterface` (Monolog)
и писать сюда кучу кода. А у нас другой подход.

Мы держим наши фабрики классов(используемые DI-контейнером) в отдельных файлах:

```
api/config/common/
 auth.php
 console.php
 doctrine.php
 frontend.php
 logger.php
 mailer.php
 migrations.php
 system.php
 twig.php
```

Поэтому по традиции вынесем код и сделаем отдельную фабрику для
интанцирования обьекта класса ErrorMiddleware.
И уже в этой фабрике опишем весь нужный для инстанцирования и настроки код
собираемого DI-контейнером обьекта ErrorMiddleware.

Другими словами вынесем создание обьекта ErrorMiddleware в
конфигурацию DI-контейнера.(api/config/common/ .. .php)

А как создавать сам обьект `ErrorMiddleware` и что нужно в него передавать?
Просто смотрим сам код создания middleware из `./api/vendor/slim/slim/Slim/App.php`
метода addErrorMiddleware:

```php
<?
    $errorMiddleware = new ErrorMiddleware(
        $this->getCallableResolver(),
        $this->getResponseFactory(),
        $displayErrorDetails,
        $logErrors,
        $logErrorDetails,
        $logger
    );
```

Ну а дальше само подключение обьекта этого middleware к приложению `$app`
уже будем делать напрямую, а НЕ через хелпер-метод `addErrorMiddleware()`.
так же, как это делается в этом же методе т.е. через метод `$app->add`

        $this->add($errorMiddleware); // из addErrorMiddleware


./api/config/middleware.php
```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;

return static function (App $app, ContainerInterface $container): void {
    /** @psalm-var array{debug:bool, env:string} */
    $config = $container->get('config');  // и это тоже больше не нужно
    // Эту старую строку удаляем:
    // $app->addErrorMiddleware($config['debug'], $config['env'] != 'test', true);

    $app->add(ErrorMiddleware::class); // новый код
    //(сам обьект ErrorMiddleware будет собираеть DI-контейнер)
};
```

В итоге убирая, весь теперь не нужный код, получаем:
./api/config/middleware.php
```php
<?php

declare(strict_types=1);

use Slim\App;
use Slim\Middleware\ErrorMiddleware;

return static function (App $app): void {
    $app->add(ErrorMiddleware::class);
};
```

Так же надо будет убрать $container из вызова данной функции в файле
./api/config/app.php
```php
<?
return static function (ContainerInterface $container): App {
    $app = AppFactory::createFromContainer($container);
    (require __DIR__ . '/../config/middleware.php')($app);// было ($app, $container)
    (require __DIR__ . '/../config/routes.php')($app);
    return $app;
};
```

Почему используем класс а не обьект?

Приложение `$app` может само достать инстанс по указанному классу в методе add()
так в само приложение предаётся DI-контейнер и оно умеет запрашивать по классам
инстанцирование их обьектов. поэтому мы здесь и указаваем просто класс

        $container->get(ErrorMiddleware::class);


Такой подход позволит более гибко настравивать ErrorMiddleware в нашем коде.

Смотрим что нужно передавать в конструктор ErrorMiddleware:


```php
<?
namespace Slim\Middleware;
class ErrorMiddleware implements MiddlewareInterface {

    public function __construct(
        CallableResolverInterface $callableResolver, // << некие интерфейсы
        ResponseFactoryInterface $responseFactory,   // нужные ему для работы
        bool $displayErrorDetails,
        bool $logErrors,
        bool $logErrorDetails,
        ?LoggerInterface $logger = null
    ) { /* .. */ }
```

Смотрим откуда берутся эти два интерфейса:
- CallableResolverInterface
- ResponseFactoryInterface

./api/vendor/slim/slim/Slim/App.php
```php
<?
namespace Slim;
class App extends RouteCollectorProxy implements RequestHandlerInterface {

    public function addErrorMiddleware(
        bool $displayErrorDetails,
        bool $logErrors,
        bool $logErrorDetails,
        ?LoggerInterface $logger = null
    ): ErrorMiddleware {
        $errorMiddleware = new ErrorMiddleware(
            $this->getCallableResolver(), // вот откуда они берутся эти
            $this->getResponseFactory(),  // интерфейсы - из самого приложения
            $displayErrorDetails,
            $logErrors,
            $logErrorDetails,
            $logger
        );
        $this->add($errorMiddleware);
        return $errorMiddleware;
    }
```

Причем они же передаются "вглубь" из ErrorMiddleware в ErrorHandler:
```php
<?
namespace Slim\Middleware;
class ErrorMiddleware implements MiddlewareInterface {

    public function getDefaultErrorHandler()
    {
        if ($this->defaultErrorHandler === null) {
            $this->defaultErrorHandler = new ErrorHandler(
                $this->callableResolver, // передача интрефейсов из
                $this->responseFactory,  // самого приложения
                $this->logger
            );
        }

        return $this->callableResolver->resolve($this->defaultErrorHandler);
    }
```

Откуда брать CallableResolverInterface ResponseFactoryInterface
если мы собираемся инстанцировать ErrorMiddleware через DI-контейнер?
Ведь при сборке обьекта из DI-контейнера получить доступ к самому приложению
и его полям мы не сможем.


```php
<?
    $errorMiddleware = new ErrorMiddleware(
        $this->getCallableResolver(), // как дотянутся до этих значений
        $this->getResponseFactory(),  // из DI-контейнера?
        #...
    );
```

Решение - через регистрацию обоих этих интерфейсов в фабриках для DI-контейнера
Для того чтобы и `$app` и инстанцируемый обьект ErrorMiddleware  использовали
именно эти нами зареганые в фабриках интерфейсы:
ResponseFactoryInterface и CallableResolverInterface


### Создаём фабрику для ErrorMiddleware

./api/config/common/errors.php
```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Log\LoggerInterface;
use Slim\Interfaces\CallableResolverInterface;

use Slim\Middleware\ErrorMiddleware;

return [
    ErrorMiddleware::class =>
    static function (ContainerInterface $container): ErrorMiddleware {
        // два нужных интерфейса как обычно достаём из контейнера

        /** @var CallableResolverInterface $callableResolver */
        $callableResolver = $container->get(CallableResolverInterface::class);
        /** @var ResponseFactoryInterface $responseFactory */
        $responseFactory = $container->get(ResponseFactoryInterface::class);

        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{display_details:bool} $config
         */
        $config = $container->get('config')['errors'];

        // напрямую создаём обьект передавая всё нужное в его конструктор
        // т.е. передаём нужные ему зависимости и так же доп параметры
        // которые мы можем теперь настраивать через конфиг
        $middleware = new ErrorMiddleware(
            $callableResolver, // те самые интревейсы
            $responseFactory,
            $config['display_details'], // доп параметры настройка через конифг
            $config['log'],
            true
        );

        return $middleware;
    },

    'config' => [
        'errors' => [
            // выводить детали только в отладочном режиме
            'display_details' => (bool)getenv('APP_DEBUG'),
            'log' => true, // отдельный параметр для включения и выключения
        ],
    ],
];
```

То есть теперь все настройки для ErrorMiddleware по его работе для конкретного
типа окружения переносятся из api/config/middlewar.php в api/config/errors.php

к тому же в этом новом конфиг-файле api/config/errors.php(фабрике ErrorMiddleware)
мы прописываем и тонкие настройки работы через массив `config`

Раньше логирование работало по принципу:
"нужно включать для прода и дев-окружений, но отключать для тестов"
теперь за этот функционал отвечает поле в конфиге `config.errors.log`

Таким образом чтобы отключить логирование для тестов просто переопределяем:

./api/config/test/errors.php
```php
<?php

declare(strict_types=1);

return [
    'config' => [
        'errors' => [
            'log' => false, // выключаем логирование
        ],
    ],
];
```

Прописываем фабрики для ResponseFactoryInterface CallableResolverInterface

./api/config/common/slim.php
```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Slim\CallableResolver;
use Slim\Interfaces\CallableResolverInterface;
use Slim\Psr7\Factory\ResponseFactory;

return [
    CallableResolverInterface::class =>
    static fn (ContainerInterface $container): CallableResolverInterface =>
    new CallableResolver($container),

    ResponseFactoryInterface::class =>
    static fn (): ResponseFactoryInterface => new ResponseFactory(),
];
```


Что мы получили этими действиями?
По сути сейчас мы просто провели рефакторинг старого кода и по сути никаких
существенных изменений в наш код пока не внесли.
- просто вынесли создание ErrorMiddleware в api/config/middleware.php
  из метода addErrorMiddleware в фабрику для DI-контейнера

А значит можно просто запустить тесты и посмотреть всё ли исправно.

```sh
make test
docker compose run --rm api-php-cli composer test
> phpunit --colors=always
PHPUnit 10.4.1 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.26
Configuration: /app/phpunit.xml

................................................................. 65 / 72 ( 90%)
.......                                                           72 / 72 (100%)

Time: 00:00.802, Memory: 16.00 MB

OK (72 tests, 129 assertions)
```

./api/src/Http/Action/HomeAction.php

Да все тесты проходят, но т.к. логгер еще не подключили сами логи еще не пишутся
