- Постановка задачи - реализовать валидацию входящих данных
- Откуда берётся 500я ошибка
- Продумываем как реализовывать валидацию
- markTestIncomplete пометка для незавершенных тестов.
- Думаем как лучше сделать валидацию
- Symfony/Validator
- Как использовать Symfony/Validator у себя в коде
- Установка Symfony/Validator
- Создаём фабрику для DI-контейнера
- Прописывание правил валидации через Аннотаций в старых версиях библ-ки
- Прописываем правила валидации для команд UseCase-обработчиков
- Прописываем Symfony/Validator в контроллер Join/RequestAction
- Рефакторим валидацию в контроллере Join/RequestAction
- Реализуем Свои классы Validator и ValidationException
- Создаём класс своего собственного исключения ValidationException
- Переписываем контроллер на исползование своих Validator и ValidationException
- Создаём свой ValidionMiddleware ловящий ValidationException
- Реализуем ValidationMiddleware по тесту для него
- Рефакторим код контроллеров под ValidationMiddleware

09-send-req-to-api
- Ручное взаимодействие со сдеалнным API маршрутом



## Постановка задачи - реализовать валидацию входящих данных

Наши первые контроллеры написаны
Контроллеры-экшены производящие регистрацию пользователя.

```
./api/src/Http/Action/V1/Auth/
└── Join
    ├── ConfirmAction.php
    └── RequestAction.php
```

Как они работают:
- берут данные из http-запроса от клиента (`$data = $request->getParsedBody()`)
- заполняют пришедшими данными команду(Command) для обработчика (Handler)
- отправляют заполненную данными команду в соответствующий обработчик:

Контролленр Join/ConfirmAction:
```php
<?
namespace App\Http\Action\V1\Auth\Join;

use App\Auth\Command\JoinByEmail\Confirm\Command;
use App\Auth\Command\JoinByEmail\Confirm\Handler;
use App\Http\EmptyResponse;

class ConfirmAction implements Psr\Http\Server\RequestHandlerInterface {

    public function __construct(
        // обработчик команды приходит сюда из DI-контейнера
        private Command\JoinByEmail\Confirm\Handler $handler)
    { }

    // ...
    public function handle(Psr\Http\Message\ServerRequestInterface $request)
    : Psr\Http\Message\ResponseInterface
    {
        // 1 - взять подготовленные пришедшие данные
        /** @psalm-var array{token:?string} $data */
        $data = $request->getParsedBody();

        // 2 - заполнить команду для обработчика конкретного UseCase-а
        $command = new Command\JoinByEmail\Confirm\Command();
        $command->token = $data['token'] ?? '';

        // 3 - передавть команду на выполнение обработчику
        $this->handler->handle($command);

        // если обработчик кидает исключение его подхватывает middleware
        // DomainExceptionHandler

        // если исключений нет - вернуть 200й-код успеха и пустой ответ
        return new EmptyResponse(200);
    }
}
```

Для двух этих контроллеров написали два соответствующих теста:
```
./api/tests/Functional/V1/Auth/
└── Join
    ├── ConfirmFixture.php
    ├── ConfirmTest.php
    ├── RequestFixture.php
    └── RequestTest.php
```

Оба этих теста эмулируют запросы к контроллерам(экшенам) и проверяют их ответы.

Впринципе наш API уже работает, но на данный момент вообще нет удобной валидации
входящих от пользователей данных т.е. нет валидации пользовательского ввода.
на данный момент при попытке юзера отправить либо недостающие поля,
либо неправильные данные просто вылетает 500я ошибка - "internal server error"

И именно такое поведение прописано и в наших тестат тоже:

./api/tests/Functional/V1/Auth/Join/RequestTest.php
```php
<? class RequestTest extends WebTestCase {

    public function testEmpty(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join',
            [] // - пустые невалидные данные ожидается email и password
        ));
        self::assertEquals(500, $response->getStatusCode());
        //                 ^^^ код ошибки
    }
```

## Откуда берётся 500я ошибка
На данный момент вся валидация происходит в конструкторах наших обьектов-значений

```php
<? class /*App\Auth\Entity\User\*/Email { // класс обьекта-значения

    public function __construct(string $value)  // конструктор
    {  // валидация вводимого значения
        Webmozart\Assert\Assert::notEmpty($value);          // < 1 >> (2)
        Webmozart\Assert\Assert::email($value);
        $this->value = mb_strtolower($value);
    }

// class Webmozart\Assert\Assert {

    public static function notEmpty($value, $message = '') // (2)
    {
        if (empty($value)) {
            static::reportInvalidArgument(\sprintf(              // < 2 >> (3)
                $message ?: 'Expected a non-empty value. Got: %s',
                static::valueToString($value)
            ));
        }
    }

    protected static function reportInvalidArgument($message)    // (3)
    {
        throw new InvalidArgumentException($message); // вот оно
    }
```

Таким образом на данный момент валидация вводимых значений идёт на уровне
констркуторов обьетов-значений. И происходит она в момент создания инстанса
обьект-значения.
То есть исключения кидаются при попытке создать объект для невалидных данных.

В нашем приложении есть middleware `DomainExceptionHandler`,
Это настроенный посредник, по сути оборачивающий вызов каждого контроллера в
блок try-catch так, чтобы если в контроллере вылетает исключение DomainException
то управление передавалось бы в этот middleware. И он вместо ответа контроллера
возвращал код возникшей ошибки и её текстовое сообщение.
При этом само сообщение берётся прямо из самого DomainException исключения.

Еще раз как это всё работает в связке:
- От клиента приходит http-запрос
- сервер nginx направляет запрос клиента в `public/index.php`
- там происходит маршрутизация(route) за которую отвечает фреймворк Slim
- по маршруту запроса(путь в uri адресе) опеределяется какой контроллер
  нужно вызвать.
  (отчасти это и называют API, соответствие адресов в uri контроллерам, которые
  должны запускать выполнение некой внутренней логики приложения)
- управление передаётся не напрямую в нужный контроллер, а в зарегестрированные
  middleware, образующие цепочку вызовов.
  Задача middleware например подготовка ввода к обработке контроллером,
  или перехват исключений происходящих внутри контроллера.
- В конце цепочки вызывается сам контроллер с передачей ему $request-а


Вот наш middleware DomainExceptionHandler создан он так, чтобы перехватывать
именно `DomainException` и на их основе генерировать некий удобный ответ:

./api/src/Http/Middleware/DomainExceptionHandler.php
```php
<?
class DomainExceptionHandler implements MiddlewareInterface {

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        try {

            return $handler->handle($request); // вызов контроллера
            // если всё в норме вернёт ответ из вызванного контроллера

        } catch (DomainException $exception) {
            // если есть ошибки ..
            // сохранить ошибку в лог
            $this->logger->warning($exception->getMessage(), [
                'exception' => $exception,
                'url' => (string)$request->getUri(),
            ]);

            // .. и вернуть сообщение об этой ошибке с 409 кодом ответа
            return new JsonResponse([
                'message' => $exception->getMessage(), // в ответе сообщение
            ], 409);
        }
    }
```

Если посмотреть на иерархию наследования исключений то можно увидить,
что и DomainException и InvalidArgumentException оба наследуются от LogicException

```php
<?
class DomainException extends LogicException {}
class InvalidArgumentException extends LogicException {}
```

А это значит что можно по аналогии с DomainExceptionHandler сделать еще один
middleware для отдельного перехвата в нём InvalidArgumentException для того чтобы
на их основе сделать валидацию ввода.

Но это не лучшее решение:
- можно будет получить только самое первое исключение а значит нельзя будет
получить полный список всех неверно заполненных полей.
- InvalidArgumentException могут выбрасывать не только обьекты-значения но и
  другой код. например PasswordHasher
  т.е. сервисы так же выполняющие некие проверки в своих конструкторах.
  а это уже будет мешать валидации ввода.

Другими словами если сделать валидацию ввода на перехвате именно исключений
`InvalidArgumentException` то юзеру смогут выводится и внутрении ошибки нашей
системы. например с неправильными параметрами подключений, паролями и т.д.
то есть потенциально это может служить источником утечки данных.

А значит нужно более продуманная система для валидации ввода.
- либо проверять входные данные до заполнения полей команды обработчика
- либо т.к. для передачи данных в обработчик используем обьекты Command то
  можно сначала просто заполнять поля команды пришедшими данными,
  и дальше до передачи заполненного обьекта-команды в обработчик производить её
  валидацию. точнее валидацию заполненных в обьекте-команде полей.


```php
<?
    // 1 - взять пришедшие в этот контроллер подготовленные данные
    $data = $request->getParsedBody();

    // 2 - заполнить команду для обработчика
    $command = new Command\JoinByEmail\Confirm\Command();
    $command->token = $data['token'] ?? '';

    // 3 - валидация заполненной команды
    //     если есть ошибки вернуть ответ со списком этих ошибок

    // 4 - если ошибок нет - передавть команду на выполнение обработчику
    $this->handler->handle($command);
```

Делаем валидацию обьекта-команды и если всё в пордяке, то команда отправляется в
обработчик, если же есть ошибки - то весь полный список со всем тем, что введено
с ошибками должно быть отправлено в ответе клиенту (в json-формате)


## Продумываем как реализовывать валидацию

Задача - реализовать валидацию ввода.
Как это сделать? В каком виде нужно возвращать ошибки?

Допустим нужно сделать так, чтобы возращать ошибки в виде подобного массива:

```php
<?
 ['message' => 'User already exists.'];
```

Вот как у нас это прописано на данный момент
```php
<? //RequestTest

    public function testExisting(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join',
            [ 'email' => 'existing@app.test', 'password' => 'new-password']
        ));

        // здесь в контроллере вылетает DomainException поэтому дальше идёт
        // возврат ответа с кодом 409 и оборачивание сообщения об ошибке в массив

        self::assertEquals(409, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        self::assertEquals([
            'message' => 'User already exists.',
        ], Json::decode($body));
    }
```
если вылетает DomainException тогда идёт возврат 409го кода и json с сообщением
об возникшей ошибке.
По такому же принципу и можно сделать валидацию данных:
- в контроллер приходит запрос
- проводим валидацию значений полей в обьекте-Command и если нашли ошибки,
  тогда выводим уже другой код кошибки и не просто сообщение а целый список того,
  что введено неправильно.

Пример как это можно сделать в тесте на пустой ввод.
здесь в этом тесте мы возвращали 500 ошибку (внутренняя ошибка сервера)
Но 422 код здесь подходит лучше, нежели расплывчатый 500й код
`422 Unprocessable Content`(Система не может обработать пришедшее значение)

```php
<?// RequestTest
    public function testEmpty(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join', []));

        //self::assertEquals(500, $response->getStatusCode()); // заменяем на:

        // новый вариант с валидацией
        self::assertEquals(422, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        self::assertEquals([
            'errors' => [ // дальше идёт список того что введено неправильно:
                'email' => 'This value should not be blank.',
                'password' => 'This value should not be blank.',
            ],
        ], Json::decode($body));
    }
```
И по аналогии если например передан невалидный email то сообщение можно поменять
на `This value is not a valid email address.`

А дальше уже сам фронтенд, получив эти ошибки из json, сможет выводить их у себя
прямо рядом с неправильно введёнными полями.

Так же и для ConfirmTest для testEmpty раньше была ошибка "409 невалидный токен"
теперь же с системой валидации нужно будет возвращать код 422
"токен не должен быть пустым"


## markTestIncomplete пометка для незавершенных тестов.
https://docs.phpunit.de/en/9.6/incomplete-and-skipped-tests.html

Допустим мы работаем над созданием системы валидации.
Начали как обычно с тестов. Написали сами тесты для нового функционала и хотим
закомитить их в репозиторий. Но не написав сам новый функционал под эти тесты
мы получим ситуацию, когда наш комит будет ломать прохождение тестов.
А значит будет нарушаться принцип, что "в ветке мастер всегда должен находится
полностью работающий код, проходящий все созданные тесты".

Как работать над внесением подобных измнений с возможностью их сохранения?
Чтобы можно было писать новые тесты и сохранять их в репозиторий не доведя всю
работу по до конца. То есть иметь возможность комитить промежуточное состояяние.

Можно это сделать - пометив новые тесты как незавершенные:
через `$this->markTestIncomplete('Сообщение чего не хватает')`

```php
<?// RequestTest
    public function testEmpty(): void
    {
        $this->markTestIncomplete('Waiting for validation.');// помечаем
        //     ^^^^^^^^^^^^^^^^^^ помечает этот тест как "не завершенный"

        $response = $this->app()->handle(self::json('POST', '/v1/auth/join', []));

        // новый вариант с валидацией сама новая механика еще не реализована
        // поэтому здесь тест будет падать
        self::assertEquals(422, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        self::assertEquals([
            'errors' => [ 'email' => '...', 'password' => '...' ],
        ], Json::decode($body));
    }
```

```sh
make test
docker compose run --rm api-php-cli composer test
> phpunit --colors=always
PHPUnit 10.4.1

................................................................. 65 / 88 ( 73%)
..........I...I...I....                                           88 / 88 (100%)

Time: 00:02.240, Memory: 26.00 MB

OK, but there were issues!
Tests: 88, Assertions: 162, Incomplete: 3.
                            ^^^^^^^^^^^^^
```
Видим что тесты до сих пор проходят даже несмотря на то, что есть незавершенный
функционал. Так же сразу видно, что есть Incomplete - незавершенные тесты которые
нужно доделалть.

Для того чтобы увидить какие именно тесты незавершенные и сообщения в них
используй запуск `vendor/bin/phpunit --display-incomplete`

```sh
docker compose run --rm api-php-cli vendor/bin/phpunit --display-incomplete
PHPUnit 10.4.1

Configuration: /app/phpunit.xml

................................................................. 65 / 88 ( 73%)
..............I......II                                           88 / 88 (100%)

Time: 00:02.170, Memory: 26.00 MB

There were 3 incomplete tests:

1) Test\Functional\V1\Auth\Join\ConfirmTest::testEmpty
Validation

/app/tests/Functional/V1/Auth/Join/ConfirmTest.php:55

2) Test\Functional\V1\Auth\Join\RequestTest::testEmpty
Waiting for validation.

/app/tests/Functional/V1/Auth/Join/RequestTest.php:63

3) Test\Functional\V1\Auth\Join\RequestTest::testNotValid
Validation

/app/tests/Functional/V1/Auth/Join/RequestTest.php:80

OK, but there were issues!
Tests: 88, Assertions: 162, Incomplete: 3.
```

Очень удобно когда работы много, сначала изменяем тесты на новое поведение
и отмечаем что данные тесты нужно доделать через markTestIncomplete и
еще можно дописать сообщение о том что надо доделать. Далее сохраняем изменения
делая фиксацию(commit) в репозиторий. При этом прохождение тестов не ломается
но и выводит сообщение о том что есть незавершенные тесты. и любой из команды
может зайти посмотреть что нужно доделать и приступить к реализации нового
функционала.
То есть например если команда раработчиков, кто-то один может сделать "спеку"
то есть изменить тесты под новый функционал, пометив их незавершенными далее
сделать коммит и пушь в репозиторий. другие из команды подтягивают из центр-го
репозитория последние измнения и приступают к реализации.



## Думаем как лучше сделать валидацию

Способы как её можно делать:
1) руками прямо в контроллере


```php
<? class RequestAction implements RequestHandlerInterface {
    // ..

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @psalm-var array{email:?string, password:?string} $data */
        $data = $request->getParsedBody();

        // берём и проверяем(валидируем) все введённые данные через if-ы
        if (is_empty($data['emali'])) { 'Email should not be blank.' }
        // .. и так для каждого поля

        $command = new Command();
        $command->email = $data['email'] ?? '';
        $command->password = $data['password'] ?? '';
        // ...
    }
```

Ручной подход не самый лучший, особенно когда контроллеров станет много и самих
этих проверок будет тоже много.
К тому же возникает необходимость следить за их консистентностью кода в разных
местах приложения.

Есть готовые решения(библиотеки), позволяющее валидировать php-массивы и обьекты

например RespectValidator, так же часто бывает что в фреймворке идёт встроенная
биб-ка для валидации, например в ларавел есть illuminate validator.
но подобного рода биб-ки именно для валидации массивов
А для валидации не массивов а именно обьектов точнее их полей они не удобны.
В частности мы в совём коде используем обьекты-Command для передачи данных в
обработчики.

К тому же если поместить код валидации в сам контроллер тогда придётся
копипастить это код валидации и в другие места нашего приложения.
Так как работа с нашими командами (UseCase-ами) может быть возможна не только
через контроллеры, но и через другие источники. Например через консоль или
через шину сообщений. Поэтому нужно сделать так, чтобы код валидации был не в
контроллере, а в неком другом более месте доступном не только контроллерам.

Именно поэтому валидировать будем не сам входной массив ($data),
а сам обьект команды($command), поля которого уже заполненную входными данными.

Если делать валидацию так, чтобы она напрямую проверяла сам обьект команды тогда
и правила валидации можно записать в сам класс Комманды.
Это и позволит держать код валидации общедоступным и независимым от контроллеров.
а значит выполнять валидацию в любом месте нашего кода:
- и внутри контроллера до вызова обработчика
- и внутри воркеров принимающих сообщения из очередей.
- и даже в самом GraphQL, если вдруг придётся переходить на другой формат


Поэтому использовать будем библиотеку позволяющую писать правила валидации
для обьектов(а не массивов) И одна из таких либ это


## Symfony/Validator

https://symfony.com/doc/current/components/validator.html
Это отдельный компонент Symfony, который можно использовать в любом php-проекте
просто подключив его через композер как обычную библиотеку.

```sh
composer require symfony/validator
docker compose run --rm api-php-cli composer require symfony/validator
```
## Как использовать Symfony/Validator у себя в коде

```php
<?
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

$validator = Validation::createValidator();  // Построитель валидарота
// создав валидатор используем его для валидации строкового значения
$violations = $validator->validate('Bernhard', [
    new Length(['min' => 10]), // длинна поля должна быть больше 10 символов
    new NotBlank(), // поле не должно быть пустым
]);
// $violations - это коллекция содержащая ошибки валидации

if (0 !== count($violations)) { // число ошибок валидации
    // there are errors, now you can show them
    foreach ($violations as $violation) {
        echo $violation->getMessage().'<br>'; // вывод обнаруженных ошибок
    }
}
```
В комплекте либы идёт Билдер для создания валидатора,
создав валидатор через него идёт проверка значения, например строки
валидация идёт через вызов метода validate далее указываются правила.
этот метод возвращает коллекцию(список) ошибок возникших при валидации.
count - это стандартная php функция, возвращающая чисто ошибок валидации
работает это через `Countable` интерфейс который реализует обьект коллекции
который возвращает `$validator->validate()`


Что будем делать
- установим либу
- зарегистрируем создание валидатора в DI-контейнере
- в контроллере вызывать метод validate но передавая в него уже обьект нашей
  команды а не просто строковое значение
- прописать правила валидации для каждой нашей команды UseCase-обработчика
- проверить есть ли нарушения - ошибки валидации и если есть вернуть их в json
  (полный список какие поля и что не так)


По поводу правил валидации для этой либы:
Прописывать правила валидации обьектов для данной бил-ки можно по разному
можно в отдельных файлах: yaml, xml, php а можно и через аттрибуты(аннотации)

Например мы уже использовали аттрибуты(аннотации) для создания мэппинга
сущностей на БД-таблицы.
По тому же принципу есть и аттрибуты для прописывания правил валидации

То есть можно просто зайти в класс команды например этой
./api/src/Auth/Command/JoinByEmail/Request/Command.php
```php
<?php

declare(strict_types=1);

namespace App\Auth\Command\JoinByEmail\Request;

class Command
{
    // и прямо здесь расставить нужные аттрибуты(аннотации) как именно
    // нужно валидировать это конкретное поле
    public string $email = '';
    public string $password = '';
}
```


## Установка Symfony/Validator

```sh
docker compose run --rm api-php-cli composer require symfony/validator
Cannot use symfony/validator's latest version v7.0.0 as it requires php >=8.2 which is not satisfied by your platform.
./composer.json has been updated
Running composer update symfony/validator
Loading composer repositories with package information
Updating dependencies
Lock file operations: 3 installs, 0 updates, 0 removals
  - Locking symfony/polyfill-php83 (v1.28.0)
  - Locking symfony/translation-contracts (v3.4.0)
  - Locking symfony/validator (v6.4.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 3 installs, 0 updates, 0 removals
  - Downloading symfony/translation-contracts (v3.4.0)
  - Downloading symfony/polyfill-php83 (v1.28.0)
  - Downloading symfony/validator (v6.4.0)
  - Installing symfony/translation-contracts (v3.4.0): Extracting archive
  - Installing symfony/polyfill-php83 (v1.28.0): Extracting archive
  - Installing symfony/validator (v6.4.0): Extracting archive
Generating autoload files
No security vulnerability advisories found.
Using version ^6.4 for symfony/validator


api/vendor/symfony/validator    2.8M
api/vendor                      62M
```


## Создаём фабрику для DI-контейнера

`Symfony\Component\Validator\Validator\ValidatorInterface` - нужный нам
интерфейс из установленной либы
https://symfony.com/doc/current/components/validator/resources.html


./api/config/common/validator.php
```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

return [
    ValidatorInterface::class =>
    static function (ContainerInterface $container): ValidatorInterface {

        return Validation::createValidatorBuilder() // построитель валидатора
            ->enableAttributeMapping() // говорим как будем задавать правила
            // для валидации нашего кода - через атрибуты в php-коде
            ->getValidator();
    },
];
```
Теперь в любом контроллере можно через DI-контейнер доставать валидатор и через
него валидировать обьекты команд.




## Прописывание правил валидации через Аннотаций в старых версиях библ-ки

Note: в старых версиях библиотеки и php ~2020 год вместо аттрибутов использовали
аннотации - те самые в докблоках. Так как по сути нативного способа навешать
методанные на элементы классов в php не было и спользовали костыли через докблоки.
С новой версией php этот способ появился и его назвали аттрибуты т.к. название
аннотация уже была занята(суть такая же как в java) И в частности в старых
версиях та же библиотека Doctrine, с помощью которой мы мапим сущности в БД,
были какие-то свои штуки для работы с аннотациями(парсинг их из док-блоков?)
поэтому там и для того чтобы "подружить" аннотации Symfony/Validator надо было
прописывать костыль:
```php
<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

return [
    ValidatorInterface::class =>
    static function (ContainerInterface $container): ValidatorInterface {
        /** @psalm-suppress DeprecatedMethd */
        \Doctrine\Common\Annotations\AnnotationRegistry:: // способ подружить
             registerLoader('class_exists');           // с аннотациями Доктрин

        return Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();
    },
];
```


## Прописываем правила валидации для команд UseCase-обработчиков

Note:
Еще раз аннотации - через докблоки `/** @annotation */`
Аттрибуты - более новый способ сделать по сути тоже самое через `#[Attribute]`
php-шные аттрибуты по сути полный аналог java-аннотаций


Пример из документации как это делать через аттрибуты
https://symfony.com/doc/current/components/validator/resources.html#the-attributeloader
```php
<?
use Symfony\Component\Validator\Constraints as Assert;
// ...                                         ^^^^^^ просто алиас для краткости

class User
{
    #[Assert\NotBlank] // этим мы и задаём правило для валидации - означающее
    protected string $name; // "это поле не должно быть пустым"
}
```


./api/src/Auth/Command/JoinByEmail/Request/Command.php
```php
<?php

declare(strict_types=1);

namespace App\Auth\Command\JoinByEmail\Request;

// класс атрибутов для прописывания правил валидации
use Symfony\Component\Validator\Constraints;

class Command
{
    #[Constraints\NotBlank]   // этими аттрибутами и прописывем правила
    #[Constraints\Email]      // валидации этого конкретного поля
    public string $email = '';

    #[Constraints\NotBlank]
    #[Constraints\Length(min: 6)]
    public string $password = '';
}
```


## Прописываем Symfony/Validator в контроллер Join/RequestAction

```php
<?
namespace App\Http\Action\V1\Auth\Join;

use App\Auth\Command\JoinByEmail\Request\Command;
use App\Auth\Command\JoinByEmail\Request\Handler;
use App\Http\EmptyResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestAction implements RequestHandlerInterface
{
    private Handler $handler;
    private ValidatorInterface $validator;

    // дописываем валидатор, чтобы di-контейнер предоставлял его в конструктор
    public function __construct(Handler $handler, ValidatorInterface $validator)
    {
        $this->handler = $handler;
        $this->validator = $validator;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @psalm-var array{email:?string, password:?string} $data */
        $data = $request->getParsedBody(); // входные данные

        // заполняем команду входными данными
        $command = new Command();
        $command->email = $data['email'] ?? '';
        $command->password = $data['password'] ?? '';

        //  ================================
        //запускаем валидацию обьекта нашей команды
        $violations = $this->validator->validate($command);

        // после валидации создаётся писок с нарушениями заданных правил
        if ($violations->count() > 0) { // если есть ошибки
            $errors = [];
            /** @var ConstraintViolationInterface $violation */
            foreach ($violations as $violation) {
                $field_name = $violation->getPropertyPath(); //название поля
                $errors[$field_name] = $violation->getMessage();
            }
            // собранные ошибки отправляем клиенту в json-ответе
            return new JsonResponse(['error' => $errors], 422);
        }
        //  ================================

        $this->handler->handle($command);

        return new EmptyResponse(201);
    }
}
```

Про getPropertyPath - Имя поля может быть полным путем внутри обьекта с точками.
Полезно когда у команды могут быть вложенные обьекты так же содержащие свои поля.
То есть валидатор умеет валидировать еще и вложенные поля обьектов и при этом
при нарушении заданных правил генерирует полный путь к этому полю собирая его
на основе названий полей в обьекте. Удобно при вложенных обьетках

Поэтому и берём именно getPropertyPath() а не просто "плоское" имя поля


## Рефакторим валидацию в контроллере Join/RequestAction

валидация самого обьекта команнды, когда правила валидации прописаны в самом
классе команды, позволяют вызывать валидацию через один и тот же валидатор
причем в разных местах нашего приложения.

Причем даже само API может изменится например на GraphQL. но код валидации при
этом не сломается просто вначале пришедшие данные преобразуются в обычный
php-массив и не важно как они пришли через json xml или GraphQL а дальше идёт
заполнение полей обьекта команды и вызов валидации.
То есть если пришел json - в php массив его и из массива заполняем поля обьекта
Command затем вызвается валидация этой конманды.
Если пришел GraphQL то значения берутся из него и ими заполняеются поля Команды
и опять вызвается валидация всего обьекта Комманда.

Этот развесистый код по валидации надо будет повторять во всех контроллерах
и хотя здесь вызов самой валидации это одна строчка

```php
<?
    $violations = $this->validator->validate($command);
```

Но вот формирование списка неправельно введёных полей несколько строк:

```php
<?
    if ($violations->count() > 0) {
        $errors = [];
        /** @var ConstraintViolationInterface $violation */
        foreach ($violations as $violation) {
            $field_name = $violation->getPropertyPath();
            $errors[$field_name] = $violation->getMessage();
        }
        return new JsonResponse(['error' => $errors], 422);
    }
```
И весь этот вод будет воторяться один в один во всех контроллерах.


Можно напримет сделать статический метод toErrors который бы формировал бы
список ошибок:
```php
<?
    $violations = $this->validator->validate($command);
    if ($violations->count() > 0) {
        return new JsonResponse(['error' => toErrors($violations)], 422);
    }
```
Но можно еще лучше, через middleware.
Мы уже сделали свой middleware DomainExceptionHandler который по сути
оборачивает вызов контроллера в try-catch для отлова всех доменных исключений.
Можно по аналогии сделать такой же и для валидации, но ловить своё уникальное
исключение

В таком случае валидация займёт ровно одну строку:
```php
<?
class RequestAction implements RequestHandlerInterface
{
    // ..

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @psalm-var array{email:?string, password:?string} $data */
        $data = $request->getParsedBody();

        $command = new Command();
        $command->email = $data['email'] ?? '';
        $command->password = $data['password'] ?? '';

        $this->validator->validate($command); // валидация кидающая исключение

        $this->handler->handle($command);

        return new EmptyResponse(201);
    }
}
```

вот как это можно сделать внутри контроллера:
```php
<?
    $command = new Command();
    // .. Заполнение полей команды

    $violations = $this->validator->validate($command);
    //                   ^^^ симфонивский валидатор

    if ($violations->count() > 0) {
        throw new ValidationException($violations); // кидаем исключение
        // в которое помещаем коллекцию со списком нарушений
    }

    $this->handler->handle($command);
```
А дальше написать сам middleware, который и будет перехватывать именно только
эти исключения и никакие другие.


## Реализуем Свои классы Validator и ValidationException

А для того чтобы не копипастить эти 4 строки с валидацией и киданием исключений
во всех своих контроллерах можно создать свой собственный, отдельный класс
`Validate` обьект, которого при вызове его метода `validate` и будет внутри себя
содержать вышеописанный код и кидать уникальное ValidationException при ошибках.


```php
<?php

declare(strict_types=1);

namespace App\Http\Validator;

use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validator
{
    private ValidatorInterface $validator;

    // принимаем synmofy-вский валидатор
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    // метод для валидации обьекта комманды
    public function validate(object $object): void
    {
        // вызываем симфоиниский валидор
        $violations = $this->validator->validate($object);

        // и если есть ошибки нарушающие правила описанные в обьекте то кидаем
        if ($violations->count() > 0) {
            throw new ValidationException($violations); // наше же исключение
        }
    }
}
```


## Создаём класс своего собственного исключения ValidationException
Оно нам будет нужно для того чтобы ловить в будущем middleware именно его
и чтобы в нём передавать список нарушений которые не прошли валидацию обьекта

```php
<?
namespace App\Http\Validator;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \LogicException
{
    private ConstraintViolationListInterface $violations;

    public function __construct(
        // то ради чего всё это делаем - список ошибок из валидации обьекта
        ConstraintViolationListInterface $violations,
        string $message = 'Invalid input.',
        int $code = 0,
        \Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
        $this->violations = $violations;
    }

    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
```

./api/src/Http/Test/Unit/Validator/ValidationExceptionTest.php
```php
<?
namespace App\Http\Test\Unit\Validator;

use App\Http\Validator\ValidationException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * @covers ValidationException
 */
class ValidationExceptionTest extends TestCase
{
    public function testValid(): void
    {
        $exception = new ValidationException(
            // создаём инстанс исключения передавая в него свой список
            $violations = new ConstraintViolationList()
        );

        self::assertEquals('Invalid input.', $exception->getMessage());
        self::assertEquals($violations, $exception->getViolations());
        // проверяем что возвращает именно наш переданный список
    }
}
```

Тест самого валидатора
./api/src/Http/Test/Unit/Validator/ValidatorTest.php

```php
<?
use App\Http\Validator\ValidationException;
use App\Http\Validator\Validator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidatorTest extends TestCase
{
    // случай при успешно проходящей валидации

    // наш валидатор - это обьект, для работы которого нужен симфонивский
    // валидатор поэтому проверка в этом тесте основана на описании мока
    public function testValid(): void
    {
        $command = new \stdClass();

        // заглушка эмулирующая симфорниский валидатор который нужен нашему
        $origin = $this->createMock(ValidatorInterface::class);
        //                          ^^^ симфонивский валидатор
        // должен вызваться один раз метод validate
        $origin->expects(self::once())->method('validate')
            ->with(self::equalTo($command)) // с таким параметров
            ->willReturn(new ConstraintViolationList());// что должен вернуть

        $validator = new \App\Http\Validator\Validator($origin);
        // ^^^ наш валидатор

        $validator->validate($command);// вызываем метод нашего обьекта
        // внутри него пойдёт вызов мока-заглушки симфонивского валидотора
        // при вызове которого и пройдут проверки которые мы прописали при
        // создании мока - а именно что у обьекта-мока внутри нашего обьекта-
        // валидатора будет вызван метод validate с передачей в него того же
        // обьекта что мы передаём в метод validate своего обьекта Validator
    }

    // случай когда валидация не проходит из-за нарушения правила
    public function testNotValid(): void
    {
        $command = new stdClass();

        $origin = $this->createMock(ValidatorInterface::class);
        $origin->expects(self::once())->method('validate')
            ->with(self::equalTo($command))
        // здесь уже мок-заглушка должен передать список полей которые не валидны
            ->willReturn($violations = new ConstraintViolationList([
                // элемент в списке тоже мок-заглушка
                $this->createMock(ConstraintViolation::class),
            ]));

        // создаём инстанс нашего собственного валидатора
        $validator = new Validator($origin);

        try {
            // внутри должно быть кинуто исключение т.к. список невалидных
            // элементов не пустой.
            $validator->validate($command);
            // т.к. исключение должно быть кинуто полюбому то прописываем кидать
            // ошибку если вдруг исключение не будет кинуто и управление дойдёт
            // до этой строчки
            self::fail('Expected exception is not thrown');
            //
        } catch (Exception $exception) {// именно базовый класс Exception
            // проверяем что кинуто нужное исключение
            self::assertInstanceOf(ValidationException::class, $exception);
            // проверяем список нарушений внутри исключения
            self::assertEquals($violations, $exception->getViolations());
        }

    }
}
```

здесь мы не используем стандартную возможность phpunit по указанию какое
исключение должно быть кинуто. т.к. нам нужно кроме самого наличия исключения
еще и проверить переданную внутрь исключение обьект-коллекцию, ради которого
ми и кидаем исключение.

```php
<?
    $this->expectException(InvalidArgumentException::class);
```

Так же здесь мы не указываем явно тип исключения которого ловим:

```php
<?
    try {
       // ...
    } catch (ValidationException $exception) {
        self::assertEquals($violations, $exception->getViolations());
    }
```
А используем базовый класс `Expeption` для того, чтобы если вдруг класс
кидаемого нашим валидатором исключения поменяется в коде то чтобы это было сразу
видно при запуске тестов. Если этого не сделать то при такой ситуации тест
просто сломается, исключение нового класса вырвится наружу теста поломав его.
и не будет сразу понятно что пошло не так и что надо исправить.


Проверяем сделанное запуская только юнит тесты
```sh
make test-unit
docker compose run --rm api-php-cli composer test -- --testsuite=unit
> phpunit --colors=always '--testsuite=unit'
PHPUnit 10.4.1

Runtime:       PHP 8.1.26
Configuration: /app/phpunit.xml

................................................................. 65 / 78 ( 83%)
.............                                                     78 / 78 (100%)

Time: 00:00.105, Memory: 14.00 MB

OK (78 tests, 145 assertions)
```


## Переписываем контроллер на исползование своих Validator и ValidationException


```php
<?php

declare(strict_types=1);

namespace App\Http\Action\V1\Auth\Join;

use App\Auth\Command\JoinByEmail\Request\Command;
use App\Auth\Command\JoinByEmail\Request\Handler;
use App\Http\EmptyResponse;
use App\Http\JsonResponse;
use App\Http\Validator\Validator;                   //<< наш собственный
use App\Http\Validator\ValidatorException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestAction implements RequestHandlerInterface
{
    private Handler $handler;
    private Validator $validator; // меняем симфонивский на наш собственный

    public function __construct(Handler $handler, Validator $validator)
    {
        $this->handler = $handler;
        $this->validator = $validator;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @psalm-var array{email:?string, password:?string} $data */
        $data = $request->getParsedBody();

        $command = new Command();
        $command->email = $data['email'] ?? '';
        $command->password = $data['password'] ?? '';

        // переписываем на блок try-catch уже именно под наш валидатор
        try {
            $this->validator->validate($command); // это уже наш а не симфонивский
            // теперь наш валидар при ошибках валидации будет кидать исключение
            // в котором и будет передаваться список что введено не правильно
        } catch (ValidatorException $exception) {
            $errors = [];
            /** @var ConstraintViolationInterface $violation */
            foreach ($exception->getViolations() as $violation) {
                $field_name = $violation->getPropertyPath();
                $errors[$field_name] = $violation->getMessage();
            }
            return new JsonResponse(['errors' => $errors], 422);
        }

        $this->handler->handle($command);

        return new EmptyResponse(201);
    }
}
```

Такой же код пишем и для второго контроллера Join/ConfirmAction
И приходим к тому что готовы писать свой миддлвейр для этого:



## Создаём свой ValidionMiddleware ловящий ValidationException

Начинаем со spec-и теста, описывающего как должен работать наш middleware

обьект middleware должен поддерживать два случая
- валидация прошла успешно - без исключения ValidatorException
- валидация провались перехвачен ValidatorException нужно вывести json


./api/src/Http/Test/Unit/Middleware/ValidationMiddlewareTest.php
```php
<?
namespace App\Http\Test\Unit\Middleware;

use App\Http\Middleware\ValidationMiddleware;
use PHPUnit\Framework\TestCase;

/**
 * @covers ValidationMiddleware
 */
class ValidationMiddlewareTest extends TestCase
{
    private static function createRequest(): ServerRequestInterface
    {
        return (new ServerRequestFactory())
            ->createServerRequest('POST', 'http://test');
    }

    private static function createResponse(): ResponseInterface
    {
        return (new ResponseFactory())->createResponse();
    }

    public function testNormal(): void
    {
        // сам тестируемый миддлвейр
        $middleware = new ValidationMiddleware();

        // заглушка контроллера, просто возвращающего пустой ответ(response)
        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn($source = self::createResponse());

        $response = $middleware->process(self::createRequest(), $handler);

        // ошибок нет значит из эмулируемого контроллера должен вернутся
        // ответ в нём и сформированный без каких либо измнений
        self::assertEquals($source, $response);
    }

    // Случай когда контроллер кидает ошибку валидации
    public function testException(): void
    {
        $middleware = new ValidationExceptionHandler();

        // эмулируем ошибки валидации
        $violations = new ConstraintViolationList([
            // используем оригинальные симфонивские классы ошибок
            new ConstraintViolation(
                // прям руками заполняем все необходимые поля ошибки-нарушения
                'Incorrect Email', null, [], null, 'email', 'not-email'
            ),
            new ConstraintViolation(
                'Empty Password', null, [], null, 'password', ''
            ),
        ]);

        // прописываем что заглушка внутри себя должна кидать исключение
        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')->willThrowException(
            new ValidationException($violations) // внутри метода будет кинуто
        );

        // внутри заглушкой $handler будет кинуто исключение ValidatorException
        // поэтому должен быть сформирован новый ответ от нашего миддлвейра
        $response = $middleware->process(self::createRequest(), $handler);

        // говорим что ожидаем json с кодом ответа 422 (невозможно обработать)
        self::assertEquals(422, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        /** @var array $data */
        $data = json_decode($body, true, 512, JSON_THROW_ON_ERROR);

        // что именно было "заполнено неправильно"
        self::assertEquals([
            'errors' => [
                'email' => 'Incorrect Email',
                'password' => 'Empty Password',
            ],
        ], $data);
    }
}
```

Теперь у нас чёткая картина того как должен работать и как реализовывать сам
ValidationMiddleware:


## Реализуем ValidationMiddleware по тесту для него
Задача этого middleware ловить ValidatorException и на их основе формировать
json-ответ со списком того, что в пришедших данных не прошло валидацию

Сюда мы выносим тот самый try-catch блок из контроллера

```php
<?
namespace App\Http\Middleware;

use App\Http\JsonResponse;
use App\Http\Validator\ValidationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

class ValidationExceptionHandler implements MiddlewareInterface
{
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        // оборачиваем вызов контроллера в try-catch и ловим ошибки валидации
        try {
            return $handler->handle($request);
        } catch (ValidationException $exception) {
            // если возникают ошибки валидации - формируем на их основе ответ
            return new JsonResponse([
                'errors' => self::errorsArray($exception->getViolations()),
            ], 422);
        }
    }

    // вспомогательный метод для формирования списка нарушений валидации
    private static function errorsArray(
        ConstraintViolationListInterface $violations
    ): array {
        $errors = [];
        /** @var ConstraintViolationInterface $violation */
        foreach ($violations as $violation) {
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }
        return $errors;
    }
}
```

Регистрируем новый `ValidationMiddleware`
```php
<?php

declare(strict_types=1);

use App\Http\Middleware;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;

return static function (App $app): void {
    $app->add(Middleware\DomainExceptionHandler::class);
    $app->add(Middleware\ValidationMiddleware::class);
    $app->add(Middleware\ClearEmptyInput::class);
    $app->addBodyParsingMiddleware();
    $app->add(ErrorMiddleware::class);
};
```

## Рефакторим код контроллеров под ValidationMiddleware

В тоге можем переписать код нашего контроллера на такой:
(убираем блок try-catch т.к. он теперь прописан в ValidationMiddleware)
```php
<?
namespace App\Http\Action\V1\Auth\Join;

use App\Auth\Command\JoinByEmail\Request\Command;
use App\Auth\Command\JoinByEmail\Request\Handler;
use App\Http\EmptyResponse;
use App\Http\Validator\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RequestAction implements RequestHandlerInterface
{
    private Handler $handler;
    private Validator $validator;

    public function __construct(Handler $handler, Validator $validator)
    {
        $this->handler = $handler;
        $this->validator = $validator;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @psalm-var array{email:?string, password:?string} $data */
        $data = $request->getParsedBody();

        $command = new Command();
        $command->email = $data['email'] ?? '';
        $command->password = $data['password'] ?? '';

        $this->validator->validate($command); // <<<< всё что нужно оставить
        // кидаемые из этого метода исключения будет ловить ValidationMiddleware

        $this->handler->handle($command);

        return new EmptyResponse(201);
    }
}
```

В итоге получаем что метод validator->validate() может кидать исключения
ValidatorException и их по той же аналогии будет ловить специально для этого
созданный ValidationMiddleware.
По той же аналогии работает и сам вызов контроллера:
метод `handler->handle($command)` тоже может кидать исклюение но уже
`DomainException` и его ловит и обрабатывает middleware `DomainExceptionHandler`


```sh
make test
docker compose run --rm api-php-cli composer test
> phpunit --colors=always
PHPUnit

................................................................. 65 / 93 ( 69%)
............................                                      93 / 93 (100%)

Time: 00:00.699, Memory: 28.00 MB

OK (93 tests, 181 assertions)
```


