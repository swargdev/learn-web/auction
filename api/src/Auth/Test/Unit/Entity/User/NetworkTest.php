<?php

declare(strict_types=1);

namespace App\Auth\Test\Unit\Entity\User;

use App\Auth\Entity\User\Network;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Auth\Entity\User\Network
 * 29-09-2023
 * @author Swarg
 */
class NetworkTest extends TestCase
{
    public function testSuccess(): void
    {
        $network = new Network($name = 'google', $identity = 'google-1');

        self::assertEquals($name, $network->getName());
        self::assertEquals($identity, $network->getIdentity());
    }

    public function testEmptyName(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Network($name = '', $identity = 'google-1');
    }

    public function testEmptyIdentity(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Network($name = 'google', $identity = '');
    }

    public function testEqual(): void
    {
        $network = new Network($name = 'google', $identity = 'g-1');
        self::assertTrue($network->isEqualTo(new Network($name, 'g-1')));
        self::assertFalse($network->isEqualTo(new Network($name, 'g-2')));
        self::assertFalse($network->isEqualTo(new Network('vk', 'vk-1')));
    }
}
