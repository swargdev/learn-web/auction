## Deploy to Production

Все эти действия идут с локальной машины руками выполняя команды подставляя EnvVar

## Кратко. Последовательность команд для развёртывания сайта

Из корня проекта где лежит основной Makefile
```sh
REGISTRY=registry.gitlab.com/swargdev/learn-web/auction IMAGE_TAG=1 make build
docker login -u a registry.gitlab.com/swargdev/learn-web/auction
Password: # здесь надо будет руками ввести токен доступа
REGISTRY=registry.gitlab.com/swargdev/learn-web/auction IMAGE_TAG=1 make push

HOST=root@10.144.52.181 PORT=22 OPT='-i ~/.ssh/keys/site-auction/id_rsa' \
  REGISTRY='registry.gitlab.com/swargdev/learn-web/auction' \
  BUILD_NUMBER=1 IMAGE_TAG=1 \
  make deploy
```


### Собираю образы для гитлаб-репозитория:

```sh
REGISTRY=registry.gitlab.com/swargdev/learn-web/auction IMAGE_TAG=1 make build
```
[See Output](./docs/05-deploy/14-do-deploy-logs/01-make-build.md)


### Отправляю(push) собранные образы в registry.gitlab.com

Перед пушем логинюсь в registry чтобы был доступ пушить образы
```sh
docker login -u a registry.gitlab.com/swargdev/learn-web/auction
```
```
Password:
WARNING! Your password will be stored unencrypted in /home/user/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

```sh
REGISTRY='registry.gitlab.com/swargdev/learn-web/auction' IMAGE_TAG=1 make push
docker login -u token -p ${ACCESS_TOKEN} registry.gitlab.com
```

[See Output](./docs/05-deploy/14-do-deploy-logs/02-make-push.txt)



```sh
HOST=root@10.144.52.181 PORT=22 OPT='-i ~/.ssh/keys/site-auction/id_rsa' \
REGISTRY='registry.gitlab.com/swargdev/learn-web/auction' \
BUILD_NUMBER=1 IMAGE_TAG=1 make deploy
```
[See Output] (./docs/05-deploy/14-do-deploy-logs/03-make-deploy.txt)

Проверка работоспособности и доступности сайта:
```sh
curl https://auction.v6.rocks
```
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon* href=" /favicon.ico">
    <meta name="description" content="Auction">
    <link href="/app.css" rel="stylesheet" />
    <title>Auction</title>
  </head>

  <body>
    <div id="app" class="app">
      <div class="welcome">
        <hi>Auction</hi>
        <p>We will be here soon</p>
      </div>
    </div>
  </body>
</html>
```

API как и положено возвращает json т.е. `content-type: application/json`:
```sh
curl -I https://api.auction.v6.rocks
```

```
HTTP/2 200
server: nginx
date: Sun, 10 Sep 2023 15:07:39 GMT
content-type: application/json
strict-transport-security: max-age=31536000
content-security-policy: block-all-mixed-content

{}
```

Можно посмотреть с удалённой машины на все запущенные и работающие контейнеры
```sh
docker ps
```
У всех IMAGE начало будет начинаться в заданного репозитория(сократил)
`registry.gitlab.com/swargdev/learn-web/auction/`
```
ID     IMAGE                    CREATED     STATUS     PORTS                 NAMES
28d0  ./auction-gateway:1      3 min ago   Up 3 min   0.0.0.0:80->80/tcp,   auction-gateway-1
                                                      :::80->80/tcp,
                                                      0.0.0.0:443->443/tcp,
                                                      :::443->443/tcp

f56a  ./auction-api:1          3 min ago   Up 3 min   80/tcp                auction-api-1
e696  ./auction-api-php-fpm:1  3 min ago   Up 3 min   9000/tcp              auction-api-php-fpm-1
611d  ./auction-frontend:1     3 min ago   Up 3 min   80/tcp                auction-frontend-1
```

### Troubleshooting. Как посмотреть логи Ningx-а внутри контейнера

```sh
ssh remotehost ..
docker ps   # найти имя нужного контейнера например auction-gateway-1
docker logs auction-gateway-1
```
эта команда отобразит все логи которые перенаправляются из stdout-а контейнра
так просиходит т.к. nginx внутри контейнера работает не в режиме демона и не
пишет логи в стандартный каталог /var/logs/nginx/ а пишет их в StdOut StdErr
которые и собират docker docker-compose и даёт возможность посмотреть их.


### Остановить сайт руками с удалённой машины
Для этого нужны два файла docker-compose-production.yml и .env
```sh
cat .env
COMPOSE_PROJECT_NAME=auction
REGISTRY=registry.gitlab.com/swargdev/learn-web/auction
IMAGE_TAG=1
```

```
user@xdeb11:~/v/site_src$ docker compose -f docker-compose-production.yml down
[+] Running 5/5
 ✔ Container auction-gateway-1      Removed                                0.4s
 ✔ Container auction-frontend-1     Removed                                0.3s
 ✔ Container auction-api-1          Removed                                0.3s
 ✔ Container auction-api-php-fpm-1  Removed                                0.2s
 ✔ Network auction_default          Removed                                0.1s
```

Теперь всё готово к разработке сайта.

### Зачем Deploy юзер?

Для этого деплоя использовал открытый репозиторий.
Поэтому образы без проблем выкачивались от лица root-a без ввода паролей.
Таким образом созданный юзер `deploy` да данном этане не понадобился.
Создавался же этот пользователь для того, чтобы от его имени качать образы из
закрытого реджестри. А для этого нужно хранить аутентификационные данные в кэше
Докера. Эти данные автоматически сохраняются после успешно пройденной команды
`docker login`. Поэтому и споздавали отдельный плейбук
`./provisioning/docker-login.yml`.


Пользователь `deploy` создаётся автоматически при выполнении Ансибл-Роли
[site](./provisioning/roles/site/)
т.к. файл [create_user.yml](./provisioning/roles/site/tasks/create_user.yml)
инклюдится в [главный файл роли](./provisioning/roles/site/tasks/main.yml).

само же копирование ssh ключа идёт отдельным плейбуком -
сделанло это потому что в нём идёт прямое копирование публичного ssh ключа с
машины того кто этот плейбук запускает. И ssh-ключ не хранится в git-репозитории
ssh-ключ берётся по этому пути:

        key: "{{ lookup('file', '~/.ssh/keys/site-auction/id_rsa.pub') }}"

это прописано в плейбуке `./provisioning/authorize.yml`


### Копирование ssh ключа для доступа от лица юзера `deploy`

```sh
cd provisioning/
make authorize
ansible-playbook -i hosts.yml authorize.yml
```
```
PLAY [Add authorized key] *****************************************************

TASK [Gathering Facts] ********************************************************
ok: [server]

TASK [Add user authorized key] ************************************************
changed: [server]

PLAY RECAP ********************************************************************
server: ok=2  changed=1  unreachable=0  failed=0  skipped=0 rescued=0 ignored=0
```


Можно легко проверить что пользователь `deploy` действительн осоздался
заблокированным для входа по ssh:
```sh
ssh deploy@10.144.52.181 -p 22 -i ~/.ssh/keys/site-auction/id_rsa 'php -v'
deploy@10.144.52.181: Permission denied (publickey).
```
лог авторзации на строне удалённого сервера:

/var/log/auth.log:
```
User deploy not allowed because account is locked
```
