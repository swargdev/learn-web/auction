## Заметки по использованию Makefile

### Установка значений для EnvVar для конкретной цели

Способ как можно установить переменные окружения для конкретного target-a
На примере того как можно укоротить строку вызова ssh
```Makefile
check: export HOST=root@10.144.52.181
check: export PORT=22
check: export OPT=-i ~/.ssh/keys/site-auction/id_rsa

# set EnvVar for specific target
check: export OPTS=${HOST} -p ${PORT} ${OPT}
check:
	ssh ${OPTS} 'php -v'
```

### Установка значений переменных окружения для всех целей внутри Makefile-а

```Makefile
# set EnvVar for all targets
export HOST=root@10.144.52.181
export PORT=22
export OPT=-i ~/.ssh/keys/site-auction/id_rsa
export OPTS=${HOST} -p ${PORT} ${OPT}
export BUILD_NUMBER=1
export REGISTRY=registry.gitlab.com/swargdev/learn-web/auction
export IMAGE_TAG=1

check:
	ssh ${OPTS} 'php -v'
```
