## Полезные команды для работы с пакетным менеджером apk в alpine

### Установка временных пакетов с их дальнейшем удалением
Например для компиляции нужных пакетов из исходников, с последующим удалением
всех нужных для компиляции пакетов:
```sh
apk add --virtual .build-deps postgresql-dev
... compilation
apk del -f .build-deps
```

Конкретные примеры использования:

./docs/08-install-xdebug/01-add-xdebug-to-images.md
```Dockerfile
FROM ...
RUN apk update && apk add --no-cache --virtual .build-deps \
    autoconf g++ make linux-headers \
    && pecl install xdebug-3.2.2 \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable xdebug \
    && apk del -f .build-deps
```

./docs/13-doctrine/02-pdo_pgsql-opti.md
```Dockerfile
# ...
RUN apk add --no-cache so:libpq.so.5 libpq-dev \
    && apk add --no-cache --virtual .pdo_pgsql-deps \
       postgresql-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo_pgsql \
    && apk del -f .pdo_pgsql-deps
# ...
```

## Получить список зависимостей конкретного пакета
```sh
apk info --depends <package>
```

docker compose run --rm api-php-cli apk info --help

...

-R, --depends         List the dependencies of the package
-W, --who-owns        Print the package which owns the specified file

-L --contains - список файлов в данном пакете

## Сформировать граф зависимостей для заданного пакета apk dot X --installed
docker compose run --rm cucumber-node-cli apk dot chromium --installed
