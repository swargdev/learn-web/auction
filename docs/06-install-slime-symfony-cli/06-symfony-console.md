## Фреймворк для работы с консолью symfony/console

- установим библиотеку symfony/console
--Входной скрипт для запуска консольных команд api/bin/app.php
- напишем и запустим свою первую консольную команду HelloCommand
- Пропишем консольную команду через composer.json
- Тихий режим без запроса подтверждения


## Устанока библиотеки symfony/console
https://github.com/symfony/console
https://packagist.org/packages/symfony/console

## Что на данный момент по установленным пакетам:

slim/slim - http-framework - содержащий функционал для работы с http запросами
это такие вещи как:
- сам класс приложения \Slim\App
- Request Response
- middleware (у нас используется пока для обработки http-ошибок)
- маршрутизация

slim/psr7 - реализация стандарта PSR7 для http-message

Но работать будем не только с http-framework-ом,
так же нам надо будет работать в консоли - то есть запускать консольные команды.
Для этого можно использовать пакет symfony/console

установка как обычно:
```sh
docker compose run --rm api-php-cli composer require symfony/console
```

```
./composer.json has been updated
Running composer update symfony/console
Loading composer repositories with package information
Updating dependencies
Lock file operations: 8 installs, 0 updates, 0 removals
  - Locking symfony/console (v6.3.4)
  - Locking symfony/deprecation-contracts (v3.3.0)
  - Locking symfony/polyfill-ctype (v1.28.0)
  - Locking symfony/polyfill-intl-grapheme (v1.28.0)
  - Locking symfony/polyfill-intl-normalizer (v1.28.0)
  - Locking symfony/polyfill-mbstring (v1.28.0)
  - Locking symfony/service-contracts (v3.3.0)
  - Locking symfony/string (v6.3.2)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 8 installs, 0 updates, 0 removals
  - Downloading symfony/polyfill-mbstring (v1.28.0)
  - Downloading symfony/polyfill-intl-normalizer (v1.28.0)
  - Downloading symfony/polyfill-intl-grapheme (v1.28.0)
  - Downloading symfony/polyfill-ctype (v1.28.0)
  - Downloading symfony/string (v6.3.2)
  - Downloading symfony/service-contracts (v3.3.0)
  - Downloading symfony/deprecation-contracts (v3.3.0)
  - Downloading symfony/console (v6.3.4)
  - Installing symfony/polyfill-mbstring (v1.28.0): Extracting archive
  - Installing symfony/polyfill-intl-normalizer (v1.28.0): Extracting archive
  - Installing symfony/polyfill-intl-grapheme (v1.28.0): Extracting archive
  - Installing symfony/polyfill-ctype (v1.28.0): Extracting archive
  - Installing symfony/string (v6.3.2): Extracting archive
  - Installing symfony/service-contracts (v3.3.0): Extracting archive
  - Installing symfony/deprecation-contracts (v3.3.0): Extracting archive
  - Installing symfony/console (v6.3.4): Extracting archive
2 package suggestions were added by new dependencies, use `composer suggest` to see details.
Generating autoload files
No security vulnerability advisories found
  Using version ^6.3 for symfony/console
```

После этого автоматически обновятся зависимости в файле composer.json:
```json
{
    "require": {
        "php": "^8.1",
        "ext-json": "*",
        "php-di/php-di": "^7.0",
        "slim/psr7": "^1.6",
        "slim/slim": "^4.12",
        "symfony/console": "^6.3"       // << новая зависимость
    }
}
```

Смотрим что по размеру
```sh
du -hcd 1 api/vendor/
580K  api/vendor/slim
40K   api/vendor/fig
528K  api/vendor/php-di
236K  api/vendor/nikic
156K  api/vendor/laravel
28K   api/vendor/ralouphie
296K  api/vendor/psr
184K  api/vendor/composer
1.7M  api/vendor/symfony             ----
3.7M  api/vendor/
3.7M	total

du -hcd 1 api/vendor/symfony/
32K   api/vendor/symfony/polyfill-ctype
64K	  api/vendor/symfony/service-contracts
140K  api/vendor/symfony/polyfill-mbstring
36K   api/vendor/symfony/polyfill-intl-grapheme
56K   api/vendor/symfony/polyfill-php80
196K  api/vendor/symfony/polyfill-intl-normalizer
24K   api/vendor/symfony/deprecation-contracts
224K  api/vendor/symfony/string
888K  api/vendor/symfony/console
1.7M  api/vendor/symfony/
```


## Входной скрипт для запуска консольных команд

Пример простейшего консольного приложения на оснвое symfony/console
```php
#!/usr/bin/env php
<?php

use Symfony\Component\Console\Application;

require __DIR__ . '/../vendor/autoload.php';

$cli = new Application('Console');
// здесь нужно добавить обрабатываемые консольные команды и их обработчики
$cli->run(); // по аналогии с http-фреймворком - запуск приложения
```


Сделаем свою первую простую команду(контроллер для cli-приложения)

src/Console/HelloCommand.php
```php
<?php
declare(strict_types=1);
namespace App\Console;

use Symfony\Component\Console\Command\Command ;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HelloCommand extends Command // наследуемся от симфони-класса
{
    protected function configure():void
    {
       // задаём название нашей команды и её описание
        $this
            ->setName('hello')
            ->setDefinition('Hello command');
    }

    // точка входа для данной команды по аналогии с handle для http-framework-а
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // <info> - обернёт выводимый в консоль текст в зелёный цвет
        $output->writeln('<info>Hello!</info>');  // пишем в StdOut

        return 0; // консольный код возврата, 0 - нет ошибок)
    }
}
```

Подключение класса скоей команды в симфони-cli-приложение
```php
<?php
// ...
$cli = new Application('Console');
$cli->add(new \App\Console\HelloCommand()); // добавляем свою конманду
$cli->run();
```

Запуск консольного приложения из консоли
```sh
docker compose run --rm api-php-cli php bin/app.php
```
покажет справку самого симфони + список имеющихся команд
```
Available commands:
  completion  Dump the shell completion script
  hello       Hello command                        # <<< наша команда
  help        Display help for a command
  list        List commands
```

запуск нашей команды внутри консольного приложения
```sh
docker compose run --rm api-php-cli php bin/app.php hello
Hello!
```
Ответ выведется зелёным цветом. заданное нами слово `Hello!`


Для команд так же будет нужен di-контейнер.
То есть надо будет инстанцировать классы консольных команд тоже через DI
делается это вот так:

```php
#!/usr/bin/env php
<?php
declare(strict_types=1);
use Symfony\Component\Console\Application;
require __DIR__ . '/../vendor/autoload.php';

// создание DI-контейнера такое же как для http-framework-а
/** @var \Psr\Container\ContainerInterface $container */
$container = require __DIR__ . '/../config/container.php';

$cli = new Application('Console');
// инстанцируем через контейнер а не напрямую как до этого
$cli->add($container->get(\App\Console\HelloCommand()); // with DI
$cli->run();
```

Так же дальше разумно будет перенести добавление своих команд в отдельный конфиг
./api/config/common/console.php
```php
<?php

declare(strict_types=1);
use \App\Console;

return [
    'config' => [
        'console' => [
            'commands' => [
                // здесь просто перечисляем классы наших контроллеров для cli
                Console\HelloCommand::class,
            ]
        ]
    ],
];
```

Подтягиваем все описанные в конфиге команды в cli-приложение

```php
#!/usr/bin/env php
<?php
declare(strict_types=1);
use Symfony\Component\Console\Application;
require __DIR__ . '/../vendor/autoload.php';

/** @var \Psr\Container\ContainerInterface $container */
$container = require __DIR__ . '/../config/container.php';

$cli = new Application('Console');

// проходим по массиву из конфига и добавляем все описанные там команды
$commands = $container->get('config')['console']['commands'];
foreach ($commands as $command) {
    $cli->add($container->get($command));
}
$cli->run();
```

## cli-app Summary

console-фреймфорк позволяет делать те же вещи как и http-фреймворк,
но не через http-запросы, а напрямую через консольные команды.

По аналогии как и для http-фреймворка можно писать контроллеры(экшены) уже
не для http-запросов по урлам, а для команд из командной строки(консоли)

api/bin/app.php - Приложение для запуска консольных команд (точка входа)
запускается оно вот так:

```sh
docker compose run --rm api-php-cli \ # запустить команду внутри контейнера
    php bin/app.php hello             # команда для запуска
```
При этом будет подниматься контейнер с именем `api-php-cli` и в нём запускаться
прописанная нами консольная команда `php bin/app.php hello`
где последнее слово - зареганая нами команда внутри консольного symfony/console
приложения.

Еще раз о том как писать контроллер для команд:
расширяем(extends симфонивсикй класс Command)
- метод `configure` - задаём имя своей команды и её описание для help-а
- метод `execute` - точка входа при вызове нашей команды

- InputInterface $input - по аналогии с http-запросом  (ввод StdIn?)
- OutputInterface $output -  по аналогии с http-ответом (вывод в StdOut)


## Прописываем консольную команду через composer
Для того чтобы сделать их запуск более удобным при разработке

Композер позволяет прописывать скрипты для их запуска через псевдонимы(aliase)
```json
{
    "scripts": {
        "app" : "php bin/app.php --ansi"
    }
}
```
- `app` - это название команды(алиас - псевдоним)
- "php bin/app.php" - что исполнять при вызове `app`
- `--ansi` - использовать цветной режим(по умолчанию чернобелый)


Теперь запустить ту же команду можно вот так
```sh
docker compose run --rm api-php-cli composer app hello
```
Output:
```
> php bin/app.php --ansi 'hello'
Hello!
```

## Тихий режим без запроса подтверждения

Симфони может работать с StdIn для интерактивного общения с юзером
И если в команде прописан код спрашивать что-либо у пользователя то
выполнение команды приостановится пока юзер не введёт данные.

есть флаг `-n --no-interaction` для того чтобы симфони не спрашивал ничего
Например дальше у нас будут миграции БД. и симфони будет постоянно спрашивать
применить ли миграции Y/N и чтобы избежать этого используем этот флаг.

Зачем консольные команды:
- миграции бд
- воркеры очередей



