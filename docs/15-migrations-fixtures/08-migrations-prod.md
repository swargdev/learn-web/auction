## Добавляем выполнение миграций для PROD-окружения

- No Interaction
- Рабочее решение. Двухшаговый деплой

- Дополнительные заметки
- Прод сервер и деплой на него


В проде из исходников сами ничего не собираем,
Хотя в создании слоя для opcache происходит установка компилятора:
```sh
RUN docker-php-ext-install opcache
```
т.е. в этом шаге где-то вшито устанавливать пакеты с компилятором и биб-кой Си.
вообще посмотреть на содержимое скрипта docker-php-ext-install можно так:

```sh
docker run --rm auction-api-php-cli:1 which docker-php-ext-install
docker run --rm auction-api-php-cli:1 cat `which docker-php-ext-install`
```


Пока же прописал компиляцию wait4open отдельным этапом мультистадийной сборки:

[PROD-php-cli](./api/docker/production/php-cli/Dockerfile)

```Dockerfile
FROM php:8.1-cli-alpine AS build
#... composer ...


### wait4open ###

FROM php:8.1-cli-alpine AS build-w4o

COPY ./docker/common/wait4open.c /tmp/w4o.c

RUN apk update && apk add --no-cache \
  gcc libc-dev \
  && gcc -pedantic -ansi -O2 /tmp/w4o.c -o /usr/local/bin/wait4open \
  && chmod +x /usr/local/bin/wait4open


#### CLI ####

FROM php:8.1-cli-alpine

# install wait4open
COPY --from=build-w4o /usr/local/bin/wait4open /usr/local/bin/wait4open

RUN docker-php-ext-install opcache
#...
COPY --from=build /app ./
COPY ./ ./

```

На данный момент диплой на прод-сервер описан через Makefile вот так

```Makefile
# set EnvVar for specific target
deploy: export OPTS=${HOST} -p ${PORT} ${OPT}

deploy:
	ssh ${OPTS} 'rm -rf site_${BUILD_NUMBER}'
	ssh ${OPTS} 'mkdir site_${BUILD_NUMBER}'

	scp -P ${PORT} ${OPT} docker-compose-production.yml ${HOST}:site_${BUILD_NUMBER}/docker-compose-production.yml
	ssh ${OPTS} 'cd site_${BUILD_NUMBER} && echo "COMPOSE_PROJECT_NAME=auction" >> .env'
	ssh ${OPTS} 'cd site_${BUILD_NUMBER} && echo "REGISTRY=${REGISTRY}" >> .env'
	ssh ${OPTS} 'cd site_${BUILD_NUMBER} && echo "IMAGE_TAG=${IMAGE_TAG}" >> .env'
	ssh ${OPTS} 'cd site_${BUILD_NUMBER} && docker compose -f docker-compose-production.yml pull'
	ssh ${OPTS} 'cd site_${BUILD_NUMBER} && docker compose -f docker-compose-production.yml up --build --remove-orphans -d'

	ssh ${OPTS} 'rm -f site'
	ssh ${OPTS} 'ln -sr site_${BUILD_NUMBER} site'
```

Это просто набор команд запускаемых на удалённом (прод)сервере через ssh.
Что здесь происходит:
- подключились к серверу по ssh
-	docker compose -f docker.. .yml pull         -- скачивает образы
-	docker compose -f docker.. .yml up --build   -- поднимает все контейнеры,
  обновляя образы на новые

Обновляем это так:


```Makefile
	ssh ${O} 'cd site_${N} && docker compose -f docker-compose-production.yml pull'
	ssh ${O} 'cd site_${N} && docker compose -f docker-compose-production.yml up --build --remove-orphans -d'
	ssh ${O} 'cd site_${N} && docker compose run api-php-cli wait4open -t 60 api-postgres:5432'
	ssh ${O} 'cd site_${N} && docker compose run api-php-cli php bin/app.php migrations:migrate --no-interaction'
```
здесь выше в ssh-командах сокращения для кратности
${O} это ${OPTS}
${N} это ${BUILD_NUMBER}

- ждём пока поднимится БД (wait4open -t 60 api-postgres:5432)
- накатываем миграции в неинтерактивном режиме(без задавания вопросов)

## No Interaction

Обрати внимание. здесь запуск команды внутри образа api-php-cli идёт через
- `php bin/app.php migrations:migrate --no-interaction`
а не через то как это делаем в дев-окружении:
- `composer app migrations:migrate --no-interaction`
потому что в проде у нас вообще нет композера, да он там не нужен.
(А зависимости? Т.е. используется мультистадийная сборка - то подтягиваются
только сами пакеты зависимостей - нужные библиотеки, а сам копозер - нет)
по сути запуск через композер это просто алиас на деле запуск cli-комманд
происходит всё равно через интерпретатор php и указывая оригинальный файл и
полный путь к нему: bin/app.php (внутри контейнера и его текущего каталога)

`migrations:migrate --no-interaction` - флаг для того чтобы не команда не
задавала уточняющий вопрос, и сразу отрабатывала

вообще флаг `--no-interaction` - универсальный флаг для всех synfony-вских команд
(пакет synfony-console) указывая этот флаг все команды отрабатывают без задавания
вопросов.


Еще то как можно прямо в коде убрать запрос на подтверждение применение миграций

```php
<?php
$input = new Symfony\Components\Console\Input\ArrayInput(
    array(
        'migrations:migrate',
        '--configuration' => APPLICATION_PATH . '/../doctrine/migrations.xml',
    )
);
$input->setInteractive(false); // просто тихо без вопросов применить и всё.
?>
```



Теперь ход обновления сервера происходит так
- выкачиваются обновлённые образы для раскатки(деплоя)
- происходит их поднятие через docker compose (флаги up и --build)
- идёт ожидание поднятие базы данных wait4open -t 60 api-postgres:5432
- и как только бд поднялась - накатываются миграции.

Недостаток такого подхода:
миграции при большом количестве данных в БД и сложных SQL-Запросах могут
выполняться достаточно долго. В итоге после `docker compose up` уже заработает
обновлённый код ожидающий от базыданных нового поведения, которое еще не
накатилось т.е. миграция еще выполняется. Т.е. новый код уже запустился и допустим
пытается лесть к новым еще не добавленных миграцией колонкам в таблике БД.
Это происходит потому, что сначала накатываются образы а лишь потом накатываются
сами миграции. В итоге все запросы на обновлённый код будет вызывать ошибки
т.к. бд не готова


## Рабочее решение. Двухшаговый деплой

Решение - создание двухшагового деплоя. Сначала миграции, потом обновление кода.
- сначала обновляем/поднимаем наш api-postgres
- дожидаемся его готовности бд (wait4open)
- и только после наката миграций производим переключение всех остальных образов


```Makefile
	ssh ${O} 'cd site_${N} && docker compose pull'
	ssh ${O} 'cd site_${N} && docker compose up --build -d api-postgres'
	ssh ${O} 'cd site_${N} && docker compose run api-php-cli wait4open -t 60 api-postgres:5432'
	ssh ${O} 'cd site_${N} && docker compose run api-php-cli php bin/app.php migrations:migrate --no-interaction'
	ssh ${O} 'cd site_${N} && docker compose up --build --remove-orphans -d'
```


В итоге теперь подключив пакет Doctrine-Migration
- генерируем новые миграции в dev-окружении (в локальной разработке)
  причем с больше именно автоматически
- написали cli-команду применяющую миграции на проде




## Дополнительные заметки

### Прод сервер и деплой на него

как делается текущий настроенный деплой подробно описано здесь:
./docs/05-deploy/14-do-deploy.md

```sh
HOST=root@10.144.52.181 PORT=22 OPT='-i ~/.ssh/keys/site-auction/id_rsa' \
  REGISTRY='registry.gitlab.com/swargdev/learn-web/auction' \
  BUILD_NUMBER=1 IMAGE_TAG=1 \
  make deploy
```

```sh
ssh root@10.144.52.181 -i ~/.ssh/keys/site-auction/id_rsa
```

пароль от юзера deploy создаётся в плейбуке ансибл случайный.
вход от этого юзера через ssh заблокирован


здесь 10.144.52.181 - это vpn адрес моего "прод" сервера для тестового деплоя.

как проверить правильность сборки прод-докер-образа (с добавленым wait4open)

Команда сборки образа из докер-файла для прода взята из Makefile-а `build-api`
```sh
docker --log-level=debug build --pull --file=api/docker/production/php-cli/Dockerfile --tag=auc/api-php-cli:1 api
```

здесь явно указываю имя образа `auc/api-php-cli` и таг `1` чисто для проверки

После сборки запускаем собраный бинарник изнутри собранного докер-образа
```sh
docker run --rm auc/api-php-cli:1 wait4open -h
```

Основное предназначение этого образа - запуск консольных команд:
```sh
docker run --rm auc/api-php-cli:1 php bin/app.php
```
```sh
docker images
REPOSITORY       TAG               IMAGE ID       CREATED          SIZE
auc/api-php-cli  1                 b5fc18cc059a   10 minutes ago   107MB
...
```

удалить образ после проверки
```sh
docker rmi auc/api-php-cli
```

