## Взаимодействие с базой данных PostgreSQL стоящей в докер-контейнере
./docs/docker-notes/psql-notes/01-cli.md

установка клиента на локальную машину для подключения к базе через cli
```sh
sudo apt install postgresql-client
```

Подключиться к бд
```sh
psql -h localhost -p 54321 -U app -d app
PGPASSWORD=secret psql -h localhost -p 54321 -U app -d app
```

Команды:

\l  - показать список всех доступных базданных
\dt - To list all tables in the current database
\dn - To list all schemas of the currently connected database
\du - To list all users and their assign roles
      список пользователей и их права доступа
\g  - повторить прошлую команду
\s  - история введённых команд

\s filename   - сохранить историю введённых команд в файл
\i filename   - выполнить команды из файла
\?  - получить помощь
\h ALTER TABLE  - помощь по конкретному разделу(изменить таблицу)

\e  - открыть редактор для ввода команды, и пыполнить её при выходе из него
\q  - выйти из соединения с СУБД

Пример команд на чистом постгресе с пустой базой данных `app`
```
Password for user app:
psql (13.11 (Debian 13.11-0+deb11u1), server 13.12)
Type "help" for help.

app=# \l

                             List of databases
   Name    | Owner | Encoding |  Collate   |   Ctype    | Access privileges
-----------+-------+----------+------------+------------+-------------------
 app       | app   | UTF8     | en_US.utf8 | en_US.utf8 |
 postgres  | app   | UTF8     | en_US.utf8 | en_US.utf8 |
 template0 | app   | UTF8     | en_US.utf8 | en_US.utf8 | =c/app           +
           |       |          |            |            | app=CTc/app
 template1 | app   | UTF8     | en_US.utf8 | en_US.utf8 | =c/app           +
           |       |          |            |            | app=CTc/app
(4 rows)


app=# \dt                            << покажи все имеющиеся таблицы в этой бд
Did not find any relations.          << когда бд пустая



app=# \dt                                            << когда в бд есть таблицы
              List of relations
 Schema |        Name        | Type  | Owner
--------+--------------------+-------+-------
 public | auth_user_networks | table | app
 public | auth_users         | table | app
(2 rows)


app=# \dn
List of schemas
  Name  | Owner
--------+-------
 public | app
(1 row)


app=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of
-----------+------------------------------------------------------------+-----------
 app       | Superuser, Create role, Create DB, Replication, Bypass RLS | {}



app=# select version();
                                                      version
-------------------------------------------------------------------------------
 PostgreSQL 13.12 on x86_64-pc-linux-musl, compiled by gcc (Alpine 12.2.1_git20220924-r10) 12.2.1 20220924, 64-bit
(1 row)
```



Refs:

https://wiki.debian.org/PostgreSql
https://www.postgresqltutorial.com/postgresql-administration/psql-commands/


показать содержимое таблицы в виде json строк
```sh
PGPASSWORD=secret psql -h localhost -p 54321 -U app -d app
select row_to_json(auth_users) as json from auth_users;

# удалить все строки кроме той в которой есть указанный емейл
DELETE FROM auth_users WHERE email <> 'user@app.test';
```

