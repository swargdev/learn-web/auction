- Разбираемся откуда появляются docker-овские логги
- Смотрим на Dockerfile образа nginx
- Смотрим на Dockerfile образа php-fpm:
- Как еще можно смотреть логи собранные Докером
- Исправляем автоматическое удаление логов в Prod-е для api-php-cli

## Разбираемся откуда появляются docker-овские логги

Теперь смотрим сохраняются ли при текущих настройках где-нибудь какие-нибудь логи

При классической разработке без использования Docker логи программ обычно пишутся
в отдельный каталог, например в `var/log`.
Например в нашем случа это может позволить зайти в логи даже на проде и
посмотреть есть ли какие ошибки,
и узнать есть ли что-то что работает не так как надо.

В нашем проекте мы работаем через Docker, а не на голом железе или виртуалках.
У докера своя филлософия того как и куда надо писать логи.

Можно посмотреть все накопленные нашим приложением на текущий момент логи:

```sh
docker compose logs
```

Выводить логи в реальном времени: -f follows(следовать за выводом)

```sh
docker compose logs -f api-php-fpm
```
```
site-api-php-fpm-1  | [06-Dec-2023 15:33:36] NOTICE: fpm is running, pid 1
site-api-php-fpm-1  | [06-Dec-2023 15:33:36] NOTICE: ready to handle connections
```
При такой команде будет выведены все имеющиеся лог-сообщения и в реальном времени
выводится все приходящие новые сообщения.

Так зайдя на наш API http://localhost:8081/ например через браузер или curl
И нескольк раз обновив страницу можно будет увидель это в логе:

```sh
docker compose logs -f api-php-fpm
site-api-php-fpm-1  | [06-Dec-2023 15:33:36] NOTICE: fpm is running, pid 1
site-api-php-fpm-1  | [06-Dec-2023 15:33:36] NOTICE: ready to handle connections
site-api-php-fpm-1  | 172.26.0.3 -  08/Dec/2023:05:51:16 +0000 "GET /index.php" 200
site-api-php-fpm-1  | 172.26.0.3 -  08/Dec/2023:05:51:19 +0000 "GET /index.php" 200
```

То есть в реальном времени можно видить заходы на страницу с api
здесь выводятся логи именно api-php-fpm.
php-fpm работает через nginx,
и запрос на "GET /index.php" прилетает к php-fpm от nginx

пробуем запросить не существующую страницу:

http://localhost:8081/not-exists
```
site-api-php-fpm-1  | 172.26.0.3 -  08/Dec/2023:05:56:14 +0000 "GET /index.php" 404
site-api-php-fpm-1  | 172.26.0.3 -  08/Dec/2023:05:56:15 +0000 "GET /index.php" 404
```
вместо 200 выводится код 404 - страница не найдена
при этом в браузере отобразился красивый вывод об ошибке:
```
404 Not Found

The application could not run because of the following error:
Details
Type: Slim\Exception\HttpNotFoundException
Code: 404
Message: Not found.
File: /app/vendor/slim/slim/Slim/Middleware/RoutingMiddleware.php
Line: 76
Trace

#0 /app/vendor/slim/slim/Slim/Routing/RouteRunner.php(56): ..
#1 /app/vendor/slim/slim/Slim/Middleware/ErrorMiddleware.php(76):  ..
#2 /app/vendor/slim/slim/Slim/MiddlewareDispatcher.php(121): ..
#3 /app/vendor/slim/slim/Slim/MiddlewareDispatcher.php(65): ..
#4 /app/vendor/slim/slim/Slim/App.php(199):
#5 /app/vendor/slim/slim/Slim/App.php(183):
#6 /app/public/index.php(15):
#7 {main}
```
так же трейс может отображаться прямо в локе показываемым docker compose
если соответственно настроен `ErrorMiddleware` (стандартный Slim-овский)

./api/vendor/slim/slim/Slim/App.php

```php
<?
namespace Slim;
class App extends RouteCollectorProxy implements RequestHandlerInterface {
    # ...
    public function addErrorMiddleware(
        bool $displayErrorDetails,
        bool $logErrors,
        bool $logErrorDetails,
        ?LoggerInterface $logger = null
    ): ErrorMiddleware {
        $errorMiddleware = new ErrorMiddleware(
            $this->getCallableResolver(),
            $this->getResponseFactory(),
            $displayErrorDetails,
            $logErrors,
            $logErrorDetails,
            $logger
        );
        $this->add($errorMiddleware);
        return $errorMiddleware;
    }
```

Смотрим в исходник что из себя представляет этот `ErrorMiddleware`:
./api/vendor/slim/slim/Slim/Middleware/ErrorMiddleware.php


```php
<?
namespace Slim\Middleware;

class ErrorMiddleware implements MiddlewareInterface {
    # ...

    // Этот ErrorMiddleware - это посредник, и здесь видно что запуск нашего
    // action-а просто оборачивает в блок try-catch:
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        try {
            return $handler->handle($request); // здесь идёт запуск нашего action
        } catch (Throwable $e) {
            return $this->handleException($request, $e);
        }
    }
}
```

А значит если при запуске либо нашего экшена либо другого middleware вылетит
исключение то отработает метод handleException() для обработки
- и запроса `$request`
- и исключения `$e`

Смотрим как работает этот метод
```php
<?
class ErrorMiddleware implements MiddlewareInterface {
    # ...

    public function handleException(
        ServerRequestInterface $request,
        Throwable $exception
    ): ResponseInterface {
        if ($exception instanceof HttpException) {
            $request = $exception->getRequest();
        }

        $exceptionType = get_class($exception);
        // достаёт обработчик для класса возникшего исключения
        $handler = $this->getErrorHandler($exceptionType);

        // и передаёт ему данные для обработки
        return $handler(
            $request,
            $exception,
            $this->displayErrorDetails,
            $this->logErrors,
            $this->logErrorDetails
        );
    }
```

```php
<?
    public function getErrorHandler(string $type)
    {
    // здесь идёт поиск настроенного хэндлера под конкретный класс исключения
        if (isset($this->handlers[$type])) {
            return $this->callableResolver->resolve($this->handlers[$type]);
        }

        if (isset($this->subClassHandlers[$type])) {
            return $this->callableResolver->resolve($this->subClassHandlers[$type]);
        }

        foreach ($this->subClassHandlers as $class => $handler) {
            if (is_subclass_of($type, $class)) {
                return $this->callableResolver->resolve($handler);
            }
        }
    // если не находит конкретный - создаётся новый по умолчанию
        return $this->getDefaultErrorHandler();
    }

    // вот то как идёт создание дефолтного обработчика ошибок:
    public function getDefaultErrorHandler()
    {
        if ($this->defaultErrorHandler === null) {
            $this->defaultErrorHandler = new ErrorHandler(
                $this->callableResolver,   //     ^ goto definition
                $this->responseFactory,
                $this->logger
            );
        }

        return $this->callableResolver->resolve($this->defaultErrorHandler);
    }
```

Смотрим на сам ErrorHandler (через goto-definition):

./api/vendor/slim/slim/Slim/Handlers/ErrorHandler.php

```php
<?
namespace Slim\Handlers;

class ErrorHandler implements ErrorHandlerInterface {
    # ...
    public function __invoke(
        ServerRequestInterface $request,
        Throwable $exception,
        bool $displayErrorDetails,
        bool $logErrors, // логировать ошибки
        bool $logErrorDetails
    ): ResponseInterface {
        # ...
        $this->logErrors = $logErrors;
        # ...

        if ($logErrors) {
            $this->writeToErrorLog();  # вызвать если логирование включено
        }

        return $this->respond();
    }

    # ...

    /**
     * Write to the error log if $logErrors has been set to true
     */
    protected function writeToErrorLog(): void
    {
        $renderer = $this->callableResolver->resolve($this->logErrorRenderer);
        // ^ обьект для создания отображения стектрейса в строку
        $error = $renderer($this->exception, $this->logErrorDetails);
        // ^ полное сообщение обшики для передачи в лог
        if (!$this->displayErrorDetails) {
            $error .= "\nTips: To display error details in HTTP response ";
            $error .= 'set "displayErrorDetails" to true in the ErrorHandler constructor.';
        }
        $this->logError($error); // запись сообщение в поток выводa
    }

    protected function logError(string $error): void
    {
        $this->logger->error($error);
        // в старых версия здесь шла стандартная процедура error_log($error)
        // error_log - просто пишет в StdErr
    }
```

Видимо поэтому на новой версии Slim по умолчанию и не видно стектрейса в логе
докер-компоуза. т.к. пишет по умолчанию не в StdErr а сразу в обьект logger

Чтобы удебиться что в лог докер-компоуза котрый мы смотрим через
`docker compose -f api-php-fpm` реально попадает всё что пишется в StdErr
можно добавить в свой код такую проверку:
/home/swarg/dev0/php/auction/api/src/Http/Action/HomeAction.php

```php
<?
class HomeAction implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
/*20*/  file_put_contents('php://stderr', 'Test'); // напрямую пишем в StdErr
        return new JsonResponse(new stdClass());
    }
}
```

Проверяем запрашивая главную страницу с api
```sh
curl localhost:8081
```

```
site-api-php-fpm-1  | 172.26.0.3 -  08/Dec/2023:06:24:34 +0000 "GET /index.php" 200
site-api-php-fpm-1  | Test
```
Да видим что в лог докера реально попадает всё что пишется прямо в StdErr

```php
<?
        file_put_contents('php://stdout', 'Success');
        file_put_contents('php://stderr', 'Error');
```

Да и StdOut тоже пишется в логи показываемые докером.
```
site-api-php-fpm-1  | Success
site-api-php-fpm-1  | 172.26.0.3 -  08/Dec/2023:06:27:37 +0000 "GET /index.php" 200
site-api-php-fpm-1  | Error
```

Таким образом по философии докера - все процессы в нём зупущенные для логирования
их сообщений должны просто выводить свои логи в StdOut или в StdErr.
То есть процессы запущенные в докер-контейнере должны выводить все свои логи
именно в станадартные потоки StdOut StdErr, а не куда-то там в файлы логов.

Как это работает.
При установке php-fpm и ningx напрямую "на железо" без докера то по настройкам
эти процессы будут сохранять свои локи в файлы в каталоге /var/logs

при установке php-fpm и nginx внутрь докер-контейра в их настройках прописывается
вывод лог-сообщений напрямую в StdOut StdErr. Чтобы это проверить:


## Смотрим на Dockerfile образа nginx

https://github.com/nginxinc/docker-nginx/

находим Dockerfile нужного нам образа(apline)

https://github.com/nginxinc/docker-nginx/blob/master/stable/alpine/Dockerfile

```Dockerfile
FROM nginx:1.24.0-alpine-slim
...
```

https://github.com/nginxinc/docker-nginx/blob/master/stable/alpine-slim/Dockerfile
```Dockerfile
FROM alpine:3.18
# ...
RUN  \ # ...
# forward request and error logs to docker log collector
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
```
Через символические ссылки идёт перенаправление вывода из файлов в stdout/err
/dev/stdout - это стандартный интерфейс в Linux чтобы каждый процесс обращаясь
по такому имени файла мог писать в свои стандартные потоки вывода и ошибок

И это делается специально для того, чтобы докер мог логировать сообщения
попадающие в stdout|stderr в свои логи, а не искать их гдето в файлах контейнера.


## Смотрим на Dockerfile образа php-fpm:

https://github.com/docker-library/php/

https://github.com/docker-library/php/blob/master/8.1/alpine3.17/fpm/Dockerfile
```Dockerfile
FROM alpine:3.17
# ...
{ \
		echo '[global]'; \
		echo 'error_log = /proc/self/fd/2'; \
		echo '[www]'; \
		echo 'access.log = /proc/self/fd/2'; \
		echo 'clear_env = no'; \
		echo; \
		echo '; Ensure worker stdout and stderr are sent to the main error log.'; \
		echo 'catch_workers_output = yes'; \
		echo 'decorate_workers_output = no'; \
	} | tee php-fpm.d/docker.conf; \
```
Здесь для каждого запускаемого пула прописывается опция
catch_workers_output = yes
При запуске процесс php-fpm порождает несколько дочерних процессов worker-ов
эти воркеры запускаются как самостоятельные отдельные процессы. и вот эта вот
опция говорит о том чтобы главный процесс перехватывал выводы своих воркеров и
выводил в свой(т.е. общий) StdErr StdOut который и будет логировать докер.

Теперь стало понятно откуда берутся докеровские логи
возникает еще один вопрос:
Нужно ли всегда запускать `docker compose logs` чтобы вывести логи на экран?


## Как еще можно смотреть логи собранные Докером

Докер сохраняет ход своей работы в свою служебную директорию `/var/lib/docker`
- buildkit
- containers - здесь хранятся данные о текущих запущенных в системе контейнерах
- engine-id
- image
- network
- overlay2
- plugins
- runtimes
- swarm
- tmp
- volumes   - Здесь хранятся все волюмы подключаемые к контейнерам

Что происходит при выполнении команды `docker compose up`:
- если явно флагом -f не указано откуда брать docker-compose.yml то он ищется
  в текущей работчей директории.
- парсится docker-compose.yml и из него берутся сервисы и их настройки которые
  нужно запустить.
- для каждого сервиса докером запускается свой контейнер,
- контейнер запускается именно на основе прописанного в docker-compose.yml
  image-а(образа)
- при запуске каждого контейнера в каталоге /var/lib/docker/containers
  создаётся поддиректория с именем полного хэша созданного контейнера

Узнать конкретный хэш контейнера(CONTAINER ID) можно через команду `docker ps` -
"покажи все запущенные контейнеры":
```sh
docker ps
CONTAINER ID   IMAGE                    NAMES
e59677998ae6   site-gateway             site-gateway-1
8c2871230937   site-api                 site-api-1
46ad068debf4   mailhog/mailhog          site-mailer-1
5a8088117bc5   site-api-php-fpm         site-api-php-fpm-1
c294a788dacc   site-frontend            site-frontend-1
3b548a6145dd   postgres:13.12-alpine    site-api-postgres-1
```

Далее по краткому CONTAINER ID можно получить полный путь:
и посмотреть что там докер хранит для конкретного контейнера
```sh
sudo ls -l /var/lib/docker/containers/5a8088117bc51a303ad957864fe75664b7f3763a03a6a36d1dc24326435a0ff3
```
-rw-r----- 1 root root 1704 Dec  8 09:27 5a8088117bc51a303ad957864fe75664b7f3763a03a6a36d1dc24326435a0ff3-json.log
drwx------ 2 root root 4096 Dec  6 18:33 checkpoints
-rw------- 1 root root 4687 Dec  6 18:33 config.v2.json
-rw------- 1 root root 1520 Dec  6 18:33 hostconfig.json
-rw-r--r-- 1 root root   13 Dec  6 18:33 hostname
-rw-r--r-- 1 root root  206 Dec  6 18:33 hosts
drwx--x--- 2 root root 4096 Dec  6 18:33 mounts
-rw-r--r-- 1 root root   38 Dec  6 18:33 resolv.conf
-rw-r--r-- 1 root root   71 Dec  6 18:33 resolv.conf.hash

Смотрим содержимое

```
sudo tail -n 3 /var/lib/docker/containers/5a8088117bc51a303ad957864fe75664b7f3763a03a6a36d1dc24326435a0ff3/5a8088117bc51a303ad957864fe75664b7f3763a03a6a36d1dc24326435a0ff3-json.log
```
```json
{"log":"Success\n","stream":"stderr","time":"2023-12-08T06:27:37.230843186Z"}
{"log":"172.26.0.3 -  08/Dec/2023:06:27:37 +0000 \"GET /index.php\" 200\n","stream":"stderr","time":"2023-12-08T06:27:37.231349547Z"}
{"log":"Error\n","stream":"stderr","time":"2023-12-08T06:27:37.232226075Z"}
```
это и есть место куда складываются логи из работающих докер-контейнеров

Причем формат удобный для машинной обработки - каждая строка лога - json
```json
{
    "log":"Error\n",
    "stream":"stderr",                         < даже поток вывода указывается
    "time":"2023-12-08T06:27:37.232226075Z"
}
```

Это специально сделано для систем атоматизации сбора, обработки и анализирования
логов. В будущем будут расмотрены некие системы сбора и агрерации логов. И эти
системы сами логи будут брать именно из этих файлов, считывая текстовые сообещния
именно из файлов этих логов.
Напишем парсер, проходящий по всем каталогам докера для работающих контейнеров,
читающий и отправляющий логи в некий лог-агрегатор.

Разабрались как работает логирование в докере.
Если проект работает через докер, то логи становится не обязательно писать
в отдельные файлы - достаточно выводить их в StdOut StdErr. Так как докер сам
автоматически логирует весь этот вывод в специальное место у себя в
`/var/lib/docker/containers/CONTAINER_ID/CONTAINER_ID-json.log`.

Поэтому для того чтобый зайти на прод сервер и посмотреть журнал ошибок(лог)
даже не нужно делать какой-то специальный логгер, сохраняющий сообщения
в файл. Просто выводим в StdErr StdOut и докер сам сохраняет это для нас.


## Исправляем автоматическое удаление логов в Prod-е для api-php-cli

Проблема.
В проде при запуске контейнера c php-cli докер каждый раз создаёт для него новый
CONTAINER_ID и всю конфигнурацию пишит в соответствующую директорию.
Но у нас на данный момент в скрипте деплоя на прод код запуска контейнеров такой:

```sh
# ...
ssh ${OPTS} 'cd site_${N} && docker compose up --build -d api-postgres'
ssh ${OPTS} 'cd site_${N} && docker compose run --rm api-php-cli wait4open -t 60 api-postgres:5432'
ssh ${OPTS} 'cd site_${N} && docker compose run --rm api-php-cli php bin/app.php migrations:migrate --no-interaction'
ssh ${OPTS} 'cd site_${N} && docker compose up --build --remove-orphans -d'
```

флаг `run --rm` автоматически удаляет контейнер после остановки.
То есть сначала создаст каталог по CONTAINER_ID, запишет туда логи
но после останова контейнера сразу же удалит весь этот каталог со всем содержимым.
Таким образом логи из докер-контейнеров `api-php-cli` будут постоянно удалятся
и посмотреть их будет невозможно.

Для того чтобы логи сохранялись и для запуска консольных команд из `api-php-cli`
тогда надо убрать `--rm` флаг, но при этом указать чтобы образ персобирался
при обновлениях:

`docker compose up --build -d api-postgres api-php-cli`


```sh
ssh ${OPTS} 'cd site_${N} && docker compose up --build -d api-postgres api-php-cli'
#                                                                      ^^^^^^^^^^^
ssh ${OPTS} 'cd site_${N} && docker compose run api-php-cli wait4open -t 60 api-postgres:5432'
ssh ${OPTS} 'cd site_${N} && docker compose run api-php-cli php bin/app.php migrations:migrate --no-interaction'
```
После этого в прод-окружении каталог CONTAINER_ID больше удалятся не будет и логи
будут всегда доступны для просмотра.
А значит можно будут собирать логи агрегатором логов для их надёжного хранения.



