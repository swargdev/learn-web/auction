<?php

declare(strict_types=1);

namespace App\Auth\Service;

use RuntimeException;
use Webmozart\Assert\Assert;

class PasswordHasher
{
    private int $memoryCost;

    /** @psalm-suppress PossiblyUnusedMethod */
    public function __construct(int $memoryCost = PASSWORD_ARGON2_DEFAULT_MEMORY_COST)
    {
        $this->memoryCost = $memoryCost;
    }

    public function hash(string $password): string
    {
        Assert::notEmpty($password);
        $options = ['memory_cost' => $this->memoryCost];
        /** @var string|false|null $hash */
        $hash = password_hash($password, PASSWORD_ARGON2I, $options);
        if ($hash == null) {
            throw new RuntimeException('Invalid hash algorithm');
        }
        /** @psalm-suppress TypeDoesNotContainType */
        if ($hash == false) {
            throw new RuntimeException('Unable to generate hash');
        }
        return $hash;
    }

    /** @psalm-suppress PossiblyUnusedMethod */
    public function validate(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}
