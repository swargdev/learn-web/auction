## Подготовка к разработке

- правильный код ответа, при ошибках
- исключения из своего кода
- удобный вывод ошибок в виде html\json
- разная настройка вывода ошибок для разных окружений
- Simple Config Based EnvVar
- как сделать систему конфигурации. Выход на библиотеку php-di
- библиотека php-di - система конфигурации


### Правильная обработка ошибок и http код ответа
### Решаем проблему 200-го кода для php-fpm

На данный момент если даже мы в коде api допустим ошибку например путь
для автолоудинга укажем не тот то

```sh
curl -D - localhost:8081  # адрес локального api
```
```
HTTP/1.1 200 OK
Server: nginx
Date: Mon, 11 Sep 2023 12:22:10 GMT
Content-Type: text/html; charset=UTF-8
Transfer-Encoding: chunked
Connection: keep-alive

<br />
<b>Warning</b>:  require(/app/public/..1/vendor/autoload.php):
Failed to open stream:
No such file or directory in <b>/app/public/index.php</b> on line <b>9</b><br />
<br />
<b>Fatal error</b>:
Uncaught Error: Failed opening required '/app/public/..XX/vendor/autoload.php'
(include_path='.:/usr/local/lib/php') in /app/public/index.php:9
Stack trace:
#0 {main}
  thrown in <b>/app/public/index.php</b> on line <b>9</b><br />
```

Как видем код у нас даже при ошибке всё равно 200тый
исправить это можно разными способами

#### 1й Простейший способ - установить код 500 по умолчанию
в самом начале index.php прописываем код возврата 500.
и если приложение где-то упадёт и не дойдёт до конца выведется 500 код а не 200

```php
<?php
  # use ...
  http_response_code(500);
  require __DIR__ # .. ;
```

```sh
curl -I localhost:8081
```
Теперь как видно всё правильно при ошибке вадаётся 500 код
```
HTTP/1.1 500 Internal Server Error
Server: nginx
Date: Mon, 11 Sep 2023 12:26:08 GMT
Content-Type: text/html; charset=UTF-8
Transfer-Encoding: chunked
Connection: keep-alive
```

```sh
curl -D - localhost:8081
```
Убрав ошибку - всё как надо - эммиттер внутри Slim фреймворка ставит 200 код сам.
```
HTTP/1.1 200 OK
Server: nginx
Date: Mon, 11 Sep 2023 12:27:05 GMT
Content-Type: application/json
Transfer-Encoding: chunked
Connection: keep-alive

{}
```

#### Другой Способ слать правильные коды http-ответа - Обработчики

 зарегать свой обработчик ошибок который будет перехватывать все httpи
и php ошибки: исключения и error-ы и устанавливать нужный 500 код.
но пока оставим по простому.


### Исключения из своего кода

Если из своего кода кинуть исключение
index.php
```php
<?php
  $app->get('/', function (Request $request, Response $response, $args) {
      throw new \Slim\Exception\HttpNotFoundException($request);
  });
```

То стектрейс с ошибкой выведется в сам http-ответ api-сервера
```sh
curl -D - localhost:8081
```
```
HTTP/1.1 500 Internal Server Error
Server: nginx
Date: Mon, 11 Sep 2023 12:33:38 GMT
Content-Type: text/html; charset=UTF-8
Transfer-Encoding: chunked
Connection: keep-alive

<br />
<b>Fatal error</b>:  Uncaught Slim\Exception\HttpNotFoundException:
              Not found. in /app/public/index.php:16
Stack trace:
#0 /app/vendor/slim/slim/Slim/Handlers/Strategies/RequestResponse.php(38): ...
#1 /app/vendor/slim/slim/Slim/Routing/Route.php(358):
---- полотно стектрейса ----
#10 {main}
  thrown in <b>/app/public/index.php</b> on line <b>16</b><br />
```
Если смотреть на это из консоли то еще норм но в браузере это никак не форматируется
и представляет из себя мешанину букв

Это не удобно для отладки хорошо и уже есть инструменты для удобного
форматирования ошибкой и трейсов.
Для этого в Slim-е есть свой обработчик Middleware способный в красивом виде
выводить трейсы с ошибками в html-e
```
$app = ...::create()
$app->addErrorMiddleware(false, true, true);
```
значение аргументов можно увидеть в исходники перейдя по GotoDefinition:
```
bool $displayErrorDetails,   # выводить инфу об ошибке подробно
  для dev удобно true - выводить, для prod - крайне рекомендуется false
bool $logErrors,             # писать ли ошибки в лог
bool $logErrorDetails,       # писать в лог об ошибках подробно
```

Теперь в браузере выведет вот такой вот отформатированный текст:
```
404 Not Found

The requested resource could not be found. Please verify the URI and try again.
```

причем если запрос на api придет от клиента "дай мне json" то и ответ об ошибке
будет не в html а в json обёртке.

Первое значение в создаваемой Middleware-обработчике ошибок - это флаг настроки
того, как выводить инфу об ошибке
```
$app->addErrorMiddleware(false, true, true);
```

для dev удобно true - что значит выводить подробно со стектрейсом,
для prod - крайне рекомендуется false - просто текст что возникла ошибка + код 500

### Simple Config Based EnvVar

Здесь уже возникает потребность иметь некую конфигурацию для гибкой настройки
этого значения через некие конфигурационные файлы для разных окружений в которых
будет работать приложение - прод и дев.

Когда работаем с докером вместо то конфиг файла удобнее использовать переменные
окружения(EnvVar). Можно прямо через Dockerfile передавать переменные окружения
задавая им нужные значения. При этом эти EnvVar будут просто передаваться внутрь
контейнера и будут доступны в нём так же как доступны EnvVar в обычной ОС.

По такому же принципу мы делали настройки для композера в
./api/docker/development/php-cli/Dockerfile:
```Dockerfile
ENV COMPOSER_ALLOW_SUPERUSER 1
```

Можно делать это и не только в Dockerfile но и в docker-compose.yml вот так:
./docker-compose.yml
```yml
  api-php-fpm:
    build:
      context: api/docker
      dockerfile: development/php-fpm/Dockerfile
    environment:
      APP_DEBUG: 1                                   # << EnvVar
    volumes:
      - ./api:/app
```

Для прода дебаг выключаем:
./docker-compose-production.yml
```yml
  api-php-fpm:
    image: ${REGISTRY}/auction-api-php-fpm:${IMAGE_TAG}
    restart: always
    environment:
      APP_DEBUG: 0
```

Таким образом можно полностью уйти от использования конфиг-файлов и полностью
использовать только переменные окружения напрямую передавая нужные параметры
внутрь контейнера либо при его сборке - запекать внутрь образа, либо при поднятии
контейнера из образа через docker-compose.yml файл или даже через команду
запуска `docker compose`.
В общем работая с докером намного удобнее передавать из через EnvVar и вообще
отказать от конфигов в файлах.

Использовать перменные окружения в php вот так:
```
$app->addErrorMiddleware((bool)getenv('APP_DEBUG'), true, true);
```
(bool) к 1 вернёт true, если переменной нет или она например 0 тогда - false
(bool)null -> false

Детальный вывод об ошибке выглядит так:
```
404 Not Found

The application could not run because of the following error:
Details
Type: Slim\Exception\HttpNotFoundException
Code: 404
Message: Not found.
File: /app/public/index.php
Line: 19
Trace
...
```

### Системы конфигурации. Выход на библиотеку php-di

Более удобным и правильным способом работы с конфигурационными данными будет
их вынос в отдельные файлы. Даже если получаем мы их через переменные окружения.

Выносим часто используемые параметры в отдельные конфиг файлы,
чтобы прямо внутри кода не хардкодить всякое там `(bool)getenv('APP_DEBUG')`

Вынеся конфиги в отдельные файлы можно использовать контейнеры или точнее
системы по управлению конфигурацией. Которые позволяют вставлять
свои конфигурационные данные в разные места кода, например в конструкторы
классов.

Для этого и используются такие вещи как контейнеры внедрения зависимостей.

### Библиотека php-di - система конфигурации
```sh
docker compose run --rm api-php-cli composer require php-di/php-di
```

```
./composer.json has been updated
Running composer update php-di/php-di
Loading composer repositories with package information
Updating dependencies
Lock file operations: 3 installs, 0 updates, 0 removals
  - Locking laravel/serializable-closure (v1.3.1)
  - Locking php-di/invoker (2.3.4)
  - Locking php-di/php-di (7.0.5)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 3 installs, 0 updates, 0 removals
  - Downloading php-di/invoker (2.3.4)
  - Downloading laravel/serializable-closure (v1.3.1)
  - Downloading php-di/php-di (7.0.5)
  - Installing php-di/invoker (2.3.4): Extracting archive
  - Installing laravel/serializable-closure (v1.3.1): Extracting archive
  - Installing php-di/php-di (7.0.5): Extracting archive
1 package suggestions were added by new dependencies, use `composer suggest` to see details.
Generating autoload files
5 packages you are using are looking for funding.
Use the `composer fund` command to find out more!
No security vulnerability advisories found
Using version ^7.0 for php-di/php-di
```

```
"require": {
    "php": "^8.1",
    "php-di/php-di": "^7.0",
    "slim/psr7": "^1.6",
    "slim/slim": "^4.12"
},
```

api/public/index.php:
```php
<?php
use DI\Container;
# ...
$container = new Container();
```

Теперь добавив к себе php-di и создав обьект container можно все настройки
перенести в этот наш контейнер.
Так же теперь можно создавать Slim\App уже используя контейнер для этого
вместо обычного статического метода `create(.. ):App` нужно использовать метод
`createFromContainer(ContainerInterface $container): App`

ContainerInterface  - это интерфейс из стандарта PSR сюда можно передавать
любой инстанс Класса который реализует этот интерфейс.
Сделав так создание приложения будет идти уже на основе настроек хранимых внутри
нашего `$continar`.

```php
<?php
$container = new Container();

$app = AppFactory::createFromContainer($container);
```

Конфигурация внутри контейнера задётся через использоание DI\ContainerBuilder-а:
вместо прямого создания через new Container();
```php
<?php

$builder = new DI\ContainerBuilder();

$builder->addDefinitions([    # массив с "определениями"-definition-ами
  'config' => [               # параметрс с именем config
    'debug' => (bool) getenv('APP_DEBUG'), # одна из насроек внутри config-а
    # сюда дальше сможем добавлять все нужные нам настройки - пароли к бд и проч.
  ],
]);
$container = $builder->build(); # создать контейнер из билдера

$app = AppFactory::createFromContainer($container);
# вот таким вот образом идёт обращение к конкретному параметру внутри контейнера
$app->addErrorMiddleware($container->get('config')['debug'], true, true);
```


