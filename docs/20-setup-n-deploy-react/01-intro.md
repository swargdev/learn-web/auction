## Вводаная

Первоначальная версия API готова. Переходим к фронденду.

На данный момент весь наш фронтенд по сути это один файл-заглушкa

./frontend/public/index.html
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon* href=" /favicon.ico">
    <meta name="description" content="Auction">
    <link href="/app.css" rel="stylesheet" />
    <title>Auction</title>
  </head>

  <body>
    <div id="app" class="app">
      <div class="welcome">
        <hi>Auction</hi>
        <p>We will be here soon</p>
      </div>
    </div>
  </body>
</html>
```

## Задача: Переписать заглушку на ReactJS

## Что такое ReactJS

В своём API мы используем `Slim`
`Slim` - это микрофреймворк.
Фреймворк - это про управление потоком управления в приложении
и постороение готового полноценного приложения через встраивание в каркас
(framework) своих компонентов-частей со своим прикладным кодом.
Фреймворк своего рода готовый стержень, каркас к которому мы можем прикриплять
свой код в виде контроллеров и других компонентов. При этом сам фреймворк
маршрутизирует запросы вызывая наш код(из написанных нами контроллеров)


[React](https://reactjs.org) - это не фреймворк и не микрофреймворк, а просто
библиотека для построения UI пользовательских интерфейсов.
Реакт можно сравнить с twig-ом(шаблонизатором html-страниц)

Другими словами библиотека ReactJS не является ни фреймворком ни микрофреймворком
а значит возникает вопрос как эту библиотеку использовать.
Причем в оффициальной документации https://reactjs.org описано только то,
как строить сами интерфейсы то есть сами `React`-овские UI-компоненты,
но нет ничего про то как именно создавать проект и организовывать его структуру,
как писать и запускать тесты и выполнять разного рода действия.


В эко системе реакт есть построитель приложений-проектов [Create-React-App]
(http://create-react-app.dev)
Это инструмент позволяет создавать готовый настроенный проект для написания
React-приложений, с уже установленным и настроенным React-ом и доп. библ-ками.
Так же в этом готовом проекте будет создана структура каталогов, будет входной
скрипт запускающий наше UI приложение.

В общем Для того чтобы превратить библиотеку React в полноценный фреймворк нужно
будет еще доустановить дополнительные библиотеки такие как Router(маршрутизатор),
биб-ки для работы с бизненс логикой, для отправки запросов к API и т.д.

В общем в отличии от фреймворка Slim, чтобы писать фронтенд приложение на React
нужно еще из разных компонентов собрать сам полноценный фреймворк.


В общем ReactJS - просто биб-ка для построения UI
документация по ReactJS http://reactjs.org описывает то, как можно использовать
саму эту библиотеку.

а уже на сайте create-react-app.dev описана документация по построению
полноценного React-проекта - т.е. как создать на основе библ-ки React полнойенный
фреймворк, что для этого нужно подключить и как именно


## Create-React-App.dev

На сайте пишут что для того чтобы создать приложение нужно

To create a project called my-app, run this command:

```sh
npx create-react-app my-app
```

my-app - каталог будущего проекта
npx - пакетный менеджер экосистемы js.(в php такую же роль играет composer)

в js есть несколько пакетных менеджеров, кроме npx: `yarn` `npm` и т.д
причем для работы в js нужно установить `nodejs` - это среда выполнения js-кода
на самой машине без участия браузера(Как известно javascript разрабатывался
как простой язык для выполнения только в браузере. со временем создали
интерпретатор, который может выполнять инструкции этого языка и вне браузера
"прямо на железе" - это и есть nodejs - интерпретатор языка) Дальше со временем
понасоздавали кучу разных инструментов - в том числе пакетных менеджеров для
ускорения и упрощения разработки и они тоже как правило написаны на js и для их
работы и нужен nodejs.




