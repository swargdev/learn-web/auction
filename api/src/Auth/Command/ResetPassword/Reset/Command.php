<?php

declare(strict_types=1);

namespace App\Auth\Command\ResetPassword\Reset;

/**
 *
 * 12-10-2023
 * @author Swarg
 */
class Command
{
    public string $token = '';
    public string $password = '';
}
