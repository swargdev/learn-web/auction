## Обновление Ansible скриптов до актуального состояния

## Role:docker

Что потребовало обновления:
- id gpg ключа для докер-репозитория + явное указание куда сохранять
  (новый идшник ключа получил из ошибки при команде `apt update`)
- добавил gnupg в зависимости
- вместо docker-compose(V1) ставлю плагин V2
- по умолчанию выключил автоочистку докер-обьектов


Добавил чтобы задача по добавлению автоочистки старых докер-обрзов запускалась
только когда задана переменная окружения `AUTO_PRUNE`:
Теперь если надо добавлять запись в crontab(включить автоочистку) запускай так:
```sh
AUTO_PRUNE=1 ansible-playbook -i hosts.yml site.yml
```
сама запись для cron-а пишется сюда: `/var/spool/cron/crontab/root`

### Старый docker-compose V1

Old Deprecated way to install docker-compose V1:
./provisioning/roles/docker/tasks/main.yml
```
 - name: Install Docker Compose # install file through simpe wget
   get-url:
     url: https://github.com/docker/compose/releases/download/1.25.0/docker-compose-{{ absible_system }}-{{ ansible_us}}
     dest: /usr/local/bin/docker-compose   # where to save downloaded file
     group: docker
     mode: 'u+x,g+x'
```
Это кусок кода из оригинала, где шла установка старой версии DockerCompose V1
здесь идёт установка через прямое скачивание файла и назначения ему нужных прав


### Обновление верификации gpg ключа docker-репозитория

старый ключ был `OEBFCD88` новый `7EA0A9C3F273FCD8`
Старый даёт такую ошибку:
```
TASK [docker : Verify fingerprint]
fatal: [server]: FAILED! =>
                  {"changed": false, "id": "OEBFCD88", "msg": "Invalid key_id"}
```

https://docs.docker.com/engine/install/debian/#install-using-the-repository
Команда из документации для установки gpg-ключа для докер-репозитория:
```sh
curl -fsSL https://download.docker.com/linux/debian/gpg | \
  sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

Альтернативный способ добавлять gpg-ключ вручную через скачивание файла(сломан)
вот такой код для добавления ключа ломает `apt update`:
```yml
- name: Add Docker GPG apt key.
  ansible.builtin.get_url:
    url: "https://download.docker.com/linux/debian/gpg"
    dest: /etc/apt/keyrings/docker.gpg
    mode: '0644'
    force: false
    checksum: "sha256:1500c1f56fa9e26b9b8f42452a553675796ade0807cdce11975eb98170b3a570"
  register: add_repository_key
  ignore_errors: false
```
причем хэш сумму надо брать от самого ключа по url ссылке
но команда cat gpg | sudo gpg --dearmor -o .../docker.pgp
как-то изменяет файл с сключём(хэш уже будет другого)
Ошибка здесь из-за того что похоже здесь идёт просто сохранение файла как есть
а нужно какое-то доп приобразование.

`man gpg` говорит:
- --dearmor
 Pack or unpack an arbitrary input into/from an OpenPGP ASCII armor.
 This is a GnuPG extension to OpenPGP and in general not very useful.


после задачи "Add Docker GPG apt key" по "ручному" добавлению ключа следующая
задача будет падать с ошибкой:
```
TASK [docker : Update apt packages]
fatal: [server]: FAILED! =>
      {"changed": false, "msg": "Failed to update apt cache: unknown reason"}
```

Реальную причину можно узнать запустив на самом сервере руками `sudo apt update`:
```
An error occurred during the signature verification.
The repository is not updated and the previous index files will be used.
GPG error: https://download.docker.com/linux/debian bullseye InRelease:
The following signatures couldn't be verified
because the public key is not available: NO_PUBKEY 7EA0A9C3F273FCD8

W: Failed to fetch https://download.docker.com/linux/debian/dists/bullseye/InRelease
The following signatures couldn't be verified
because the public key is not available: NO_PUBKEY 7EA0A9C3F273FCD8

W: Some index files failed to download.
They have been ignored, or old ones used instead.
```
отсюда кстати и взял новый id ключа

Вот так Ансибл ругается когда вместо корретного нового идишника указал значение:
`9DC858229FC7DD38854AE2D88D81803C0EBFCD88`:

```
TASK [docker : Verify fingerprint]
fatal: [server]: FAILED! => {
  "before": ["B7C5D7D6350947F8", "6ED0E7B82643E131", "254CF3B5AEC0A8F0", "BDE6D2B9216EC7A8", "F8D2585B8783D481", "73A4F27B8DD47936", "0E98404D386FA1D9", "A48449044AAD5C5D", "54404762BBB6E853", "605C66F00D6C9793", "DC30D7C23CBBABEE", "648ACFD622F3D138", "4DFAB270CAA96DFA", "112695A0E562B32A", "DCC9EFBF77E11517", "B188E2B695BD4743", "B214EAC28059B8AC", "1657198823E52A61", "5A4011580999FBE0"],
"changed": true,
"fp": "8D81803C0EBFCD88",
"id": "9DC858229FC7DD38854AE2D88D81803C0EBFCD88",
"key_id": "9DC858229FC7DD38854AE2D88D81803C0EBFCD88",
"msg": "No key to add ... how did i get here?!?!", "short_id": "0EBFCD88"}
```

Вот такая ошибка когда gpg ключ реально не сохраняется в нужное место:
```
fatal: [server]: FAILED! =>
  {"changed": false, "msg": "Failed to update apt cache:
W:GPG error: https://download.docker.com/linux/debian bullseye InRelease:
The following signatures couldn't be verified
because the public key is not available: NO_PUBKEY 7EA0A9C3F273FCD8,
E:The repository 'https://download.docker.com/linux/debian bullseye InRelease'
is not signed."}
```
нужное место задаётся в строке с репозиторием вот так:
```
deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://... .. stable
                          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
```
по пути заданому в `signed-by` и должен лежать pgp-ключ
и если его там нет то будет подобного рода ошибка.

Исправил так: добавил в задачу секцию `keyring` для явного указания куда сохранять
```
- name: Add GPG key
  apt_key:
    url: https://download.docker.com/linux/debian/gpg
    keyring: /etc/apt/keyrings/docker.gpg                 # куда сохранять ключ
    state: present
```



Альтернативный кусок кода для проверки запущена ли служба докера без хэндлера
(не проверял работает ли)
```yml
- name: Ensure Docker is started and enabled at boot.
  service:
    name: docker
    state: started
    enabled: true
  ignore_errors: "{{ ansible_check_mode }}"
```

