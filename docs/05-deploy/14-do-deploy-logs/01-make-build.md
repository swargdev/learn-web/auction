# Это лог сборки образов в более читаемом виде

make[1]: Entering directory '/home/swarg/dev0/php/auction'
```sh
docker --log-level=debug build --pull \
  --file=gateway/docker/production/nginx/Dockerfile \
  --tag=registry.gitlab.com/swargdev/learn-web/auction/auction-gateway:1 \
    gateway/docker
```
[+] Building 4.0s (9/9) FINISHED                                docker:default
 => [internal] load .dockerignore                                          0.5s
 => [internal] load build definition from Dockerfile 0.4s
 => [internal] load metadata for docker.io/library/nginx:1.18-alpine
 => [1/4] FROM docker.io/library/nginx:1.18-alpine@sha256:93baf2ec1bfefd04d29eb070900dd5d79b0f79863653453397e55a5b663a6
 => [internal] load build context
 => => transferring context:
 => CACHED [2/4] COPY ./common/nginx/snippets /etc/nginx/snippets
 => CACHED [3/4] COPY ./production/nginx/conf.d /etc/nginx/conf.d
 => CACHED [4/4] WORKDIR /app
 => exporting to image
 => => exporting layers
 => => writing image sha256:ed903ef825031c3544f27314e7c50efed8becfac2db5590e33d8124b4e026c69
 => => naming to registry.gitlab.com/swargdev/learn-web/auction/auction-gateway:

```sh
docker --log-level=debug build --pull \
       --file=frontend/docker/production/nginx/Dockerfile \
       --tag=registry.gitlab.com/swargdev/learn-web/auction/auction-frontend:1 \
         frontend
```
[+] Building 2.4s (9/9) FINISHED                                 docker:default
 => [internal] load .dockerignore
 => => transferring context:
 => [internal] load build definition from
 => => transferring dockerfile:
 => [internal] load metadata for docker.io/library/nginx:1.18-alpine
 => [1/4] FROM docker.io/library/nginx:1.18-alpine@sha256:93baf2ec1bfefd04d29eb070900dd5d79b0f79863653453397e55a5b663a6
 => [internal] load build context
 => => transferring context:
 => CACHED [2/4] COPY ./docker/common/nginx/conf.d /etc/nginx/conf.d
 => CACHED [3/4] WORKDIR /app
 => CACHED [4/4] COPY ./public ./public
 => exporting to
 => => exporting
 => => writing image sha256:7ad520353dbd5ef44598e18e2d3411f8d1e189676cd54810dc45646c250cf935
 => => naming to registry.gitlab.com/swargdev/learn-web/auction/auction-frontend:1

```sh
docker --log-level=debug build --pull \
  --file=api/docker/production/php-fpm/Dockerfile \
  --tag=registry.gitlab.com/swargdev/learn-web/auction/auction-api-php-fpm:1 \
    api
```
[+] Building 3.5s (10/10) FINISHED                               docker:default
 => [internal] load .dockerignore
 => => transferring context:
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile:
 => [internal] load metadata for docker.io/library/php:8.1-fpm-alpine
 => [1/5] FROM docker.io/library/php:8.1-fpm-alpine@sha256:0055323b74a1e4380ff850c9943eecfead5185c3eff33c4107972404ee00
 => [internal] load build
 => => transferring context: 1.67kB
 => CACHED [2/5] RUN mv /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
 => CACHED [3/5] COPY ./docker/common/php/conf.d /usr/local/etc/php/conf.d
 => CACHED [4/5] WORKDIR /app
 => CACHED [5/5] COPY ./ ./
 => exporting to image
 => => exporting layers
 => => writing image sha256:aa2b5cf1b769eb774b06cf8f8257e408ae5603af310cf9da614455c5d032b22a
 => => naming to registry.gitlab.com/swargdev/learn-web/auction/auction-api-php-fpm:1

```sh
docker --log-level=debug build --pull \
  --file=api/docker/production/nginx/Dockerfile \
  --tag=registry.gitlab.com/swargdev/learn-web/auction/auction-api:1 \
    api
```
[+] Building 2.7s (9/9) FINISHED                                 docker:default
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 151B
 => [internal] load .dockerignore
 => => transferring context: 47B
 => [internal] load metadata for docker.io/library/nginx:1.18-alpine
 => [1/4] FROM docker.io/library/nginx:1.18-alpine@sha256:93baf2ec1bfefd04d29eb070900dd5d79b0f79863653453397e55a5b663a6
 => [internal] load build context
 => => transferring context: 266B
 => CACHED [2/4] COPY ./docker/common/nginx/conf.d /etc/nginx/conf.d
 => CACHED [3/4] WORKDIR /app
 => CACHED [4/4] COPY ./public ./public
 => exporting to image
 => => exporting layers
 => => writing image sha256:809bd8ab54719b97bddf0dc9cc09effc53cb058656d345268ec0b464e1d38c50
 => => naming to registry.gitlab.com/swargdev/learn-web/auction/auction-api:1
make[1]: Leaving directory '/home/swarg/dev0/php/auction'
