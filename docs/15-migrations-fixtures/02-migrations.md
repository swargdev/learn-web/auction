## Миграции и продвинутые инструменты для заполнения БД-таблиц.

<!-- v22 9:32-->
## Что такое миграции

- исходники храним в некой системе контроля версий(например git)
- коммиты(фиксации изменений в коде) и пуши в удалённый репозиторий позволяют
  держать синхронизованную кодовую базу, так чтобы у всех участников команды
  программистов всегда был свежий актуальный код со всеми новыми изменениями.

- при работы с БД нельзя просто взять и сделать полный дамп всех смех таблиц БД,
  для того чтобы потом можно было править его в коде. Это не будет работать
  Т.к. сама рабочая БД под вносимые изменения сама изменятся не будет.
  В этих случаях используют подход называемый "миграции".

Миграции "вручную" без посторонних инструментов(Принцип):
(просто на основе файлов и каталогов)

```
migrations/                   < каталог для наших миграций
  0001.sql  CREATE TABLE ...  < файл с SQL-запросом на создание нужной таблицы
  0002.sql  ALTER TABLE ...   < другой файл с SQL-запросом на изменение таблицы
  0003.sql  ...
```

Принцип простой - в отдельном каталоге создаются файлы с sql-запросами на
формирование нужного состояния бд. Например самый первый из таких файлов будет
содержать SQL-запрос на полное создание бд всех нужных таблиц с полным описанием
всех полей типов первичных ключей и прочего.
Дальше по мере разработки когда надо будет внести измненеие в БД создаётся новый
файл 0002.sql в котором прописывается новый SQL-запрос меняющий состояние БД на
нужное. т.е. например добавляющий колонку(поле) в конкретную таблицу БД

То есть вместо хранения полного дампа со схемой всех таблиц БД
хранятся отдельные элементы(файлы) в которых происходит поэтапная модификация БД
под новые возникающие требования.
То есть хранятся элементы происводящие пошаговые-поэтапные изменения(миграции)
состояния БД из одного состояния в другое.
Это позволит каждому из команды подогнать свою копию локальной БД под текущее
актуальное (нужное состояние) просто через пошаговое применение новых,
недостающих у него "этапов" элементов с миграциями - т.е. в нашем примере
файлов, с модифицирующими SQL-Запросами.
То есть таким образом можно с нуля либо с любого промежуточного шага получить
последнее актуальное состояние БД на другой машине и даже создать нужную БД
с нуля на новой свежей машине с только что установленной СУБД.
Суть - синхронизировать состояние БД в любом месте из любой точки, и как
у программистов команды на их локальных машинах так и в проде на рабочих серверах


Такой простой подход с ручным применением файлов с SQL-запросами автоматизируется
в консольное приложение или команду. например с именем `migrate`

Задача такой cli-команды будет:
- поэтапное применение файлов из нашего каталога migrations
- избегать повторное применение одних и тех же миграций на той же БД

Для того чтобы сделать защиту от повторного запуска ранее применённых миграций:
- для этого обычно в самой бд создаётся отдельная таблица с именем `migrations`
- в этой таблице храниться список уже примененённых миграций.
В нашем примере это могут быть просто номера файлов с SQL-запросами из каталога
со всеми имеющимися миграциями: 0001 0002 0003 соответствующие применённым
файлам 0001.sql 0002.sql 0003.sql

таким образом когда запускаем нашу cli утилиту накатывающую миграции она:
- смотрит что уже накатано и накатывает только новые файлы,
- сохраняет имена накатанных внутрь самой БД в таблицу migrations.

Так например если подтянув обновления кода из гит-репозитория приходит новый
0004.sql файл с миграцией то применив нашу cli-команду применится только этот
новый файл-миграции, тем самым изменит состояние БД до текущего актуального
и для последующего обновления запишет имя новой применённой миграции(0004)
в саму бд.

При разработке и работе с несколькими ветками в git-репозитории часто может
возникнуть нужда вернутся назад к прошлому состоянию, а значит нужен способ
откатить и состояние БД до некого придедущего. То есть нужно чтобы миграции
можно было производить не только вперед но и еще и назад.
А значит нужна возможность откатывать миграции и на шаг назад,а не только вперед
Для этого при таком ручном подходе с sql-файлами нужно будет писать sql-запросы
в две стороны:
- накатывающий изменение `up`
- откатывающий изменения обратно `down`

        0001_up.sql    CREATE TABLE ...
        0001_down.sql  DROP TABLE ...

Для каждого шага(миграции) делаем по два файла с накатом и откатом и это
позволит еще и возвращать актуальное состояние бд к старому состоянию через
обратное применение поэтапные откаты `down` шагов с миграциями(_down.sql файлов)

То есть нам нужно сделать checkout на состояние кода недельной давности.
делаем откат БД по шагам назад, затем откатываем код и исследуем проблему.


Таким образом нужна консольная команда, помогающая управлять состоянием БД
на основе описанных поэтапных шагов изменения её состояний(миграций)

В данном случае код пишем на PHP, поэтому желательно чтобы требуемая команда
умела работать с php кодом а не только с чистыми SQL запросами.
Ну и была бы более продвинутой, с возможностью выполнять PHP-код

А значит лучше задавать миграции не в ввиде SQL-запросов, а в виде php-классов

```php
<?
class M20231203084700 extends AbstractMigration
{
  public function up(): void
  {   // здесь описываем нужные SQL-запросы для "шага вперёд"
      $this->execute('CREATE TABLE users ...');
  }

  public function down(): void
  {   // SQL-запросы для отката назад
      $this->execute('DROP TABLE users');
  }
}
```

В имени класса указывается текущая дата(на момент создания миграции)
Уже есть готовые наборы библиотек для подобного подхода.

Для работы без Доктрин, когда используются свои мапперы годится:
- библиотека [Phinx](./docs/15-migrations-fixtures/03-phinx.md)
Простая библиотека для создания миграций через php-код через удобный
построитель запросов с возможностью как писать SQL-Запросы и Up-Down методы
в классах миграций так и описывывать php-кодом изменение методом change с
автоматическим генерированием нужных SQL-Запросов.

В нашем проекте мы используем `Doctrine/DBAL` `Doctrine/ORM` а поэтому и
создавать миграции (т.е. таблицы в бд) будем через её же библиотеку
[Doctrine-Migrations](./docs/15-migrations-fixtures/04-doctrine-migrations.md)


