import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

jest.mock(
  './components/Welcome',
  () =>
    function mockWelcome(/* props */) {
      return <span data-testid="WelcomeComponent" />;
    }
);

test('renders app', () => {
  render(<App />);
  expect(screen.getByTestId('WelcomeComponent')).toBeInTheDocument();
  // screen.debug()
});
