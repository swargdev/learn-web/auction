## Делаем перевод доменных исключений

## Добавляем файла с переводом наших доменных исключений

Файл с простым ассоциативным массиво всех имеющихся у нас исключений
./api/translations/exceptions.ru.php

```php
<?php

declare(strict_types=1);

return [
    'User is not found.' => 'Пользователь не найден.',
    'Confirmation is not required.' => 'Подтверждение не нужно.',
    'Network is already attached.' => 'Соцсеть уже привязана.',
    'User is not active.' => 'Пользователь не активен.',
    'Incorrect token.' => 'Неверный токен.',
    'User already exists.' => 'Пользователь уже существует.',
    'Token is not found.' => 'Токен не найден.',
    'Token is expired.' => 'Время действия токена истекло.',
    'User does not have an old password.' => 'У пользователя нет старого пароля.',
    'Resetting is already requested.' => 'Восстановление уже запрошено.',
    'Resetting is not requested.' => 'Восстановление не запрошено.',
    'Incorrect current password.' => 'Неверный текущий пароль.',
    'Email is already same.' => 'Email совпадает с текущим.',
    'Changing is already requested.' => 'Смена уже запрошена.',
    'Changing is not requested.' => 'Смена не запрошена.',
    'Unable to remove active user.' => 'Нельзя удалить активного пользователя.'
];
```

./api/config/common/translator.php
```php
<?
return [
    'config' => [
        'translator' => [
            'lang' => 'en',
            // 'lang' => 'ru',
            'resources' => [
                [
                    'xlf',
                    $symfony_validator_res . 'translations/validators.ru.xlf',
                    'ru',
                    'validators',
                ],
                // добавляем свой php-файл с переводами
                [
                    'php',
                    __DIR__ . '/../../translations/exceptions.ru.php',
                    'ru',
                    'exceptions',
                ],
            ],
        ],
        'locales' => [
            'allowed' => ['en', 'ru'],
        ],
    ],
];
```

## Добавление переводчика в Middleware/DomainExceptionHandler


```php
<?
namespace App\Http\Middleware;

use App\Http\JsonResponse;
use DomainException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class DomainExceptionHandler implements MiddlewareInterface
{
    private LoggerInterface $logger;
    private TranslatorInterface $translator;

    public function __construct(
        LoggerInterface $logger,
        TranslatorInterface $translator
    ) {
        $this->logger = $logger;
        $this->translator = $translator;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        try {
            return $handler->handle($request);
        } catch (DomainException $exception) {
            $this->logger->warning($exception->getMessage(), [
                'exception' => $exception,
                'url' => (string)$request->getUri(),
            ]);
            $message = $this->translator->trans(
                $exception->getMessage(), [], 'exceptions'
                // второй агрумент пустой ^ массив т.к. здесь у нас нет
                // placeholder-ов значений для подстановки в сообщение
                // exceptions - название категории перевода(domain)
            );

            return new JsonResponse([ 'message' => $message ], 409);
        }
    }
}
```

Обновляем тесты для DomainExceptionHandler добавляя заглушку переводчика

./api/src/Http/Test/Unit/Middleware/DomainExceptionHandlerTest.php
```php
<?
class DomainExceptionHandlerTest extends WebUTestCase {

    // случай успеха исключение не кидается переводить нечего
    public function testNormal(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->never())->method('warning');

        // новая заглушка ничего не далает т.к. не будет вызываться
        $translator = $this->createMock(TranslatorInterface::class);

        $middleware = new DomainExceptionHandler($logger, $translator);

        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn($source = self::createResponse());

        $response = $middleware->process(self::createRequest(), $handler);

        self::assertEquals($source, $response);
    }

    // случай срабатывания исключения
    public function testException(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('warning');

        // заглушка перемодчика
        $translator = $this->createMock(TranslatorInterface::class);
        // должен вызваться один раз метод trans с конкретными параметрами:
        $translator->expects($this->once())->method('trans')->with(
            $this->equalTo('Some error.'), // первый параметр при вызове
            $this->equalTo([]), // 2й параметрв при вызове trans
            $this->equalTo('exceptions') // 3й - domain
        // задаём значение, которое должен вернуть вызов метода trans
        )->willReturn('Ошибка.');

        $middleware = new DomainExceptionHandler($logger);

        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')
            ->willThrowException(new DomainException('Some error.'));

        $response = $middleware->process(self::createRequest(), $handler);

        self::assertEquals(409, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        /** @var array $data */
        $data = json_decode($body, true, 512, JSON_THROW_ON_ERROR);

        // из миддлвера должен прийти уже переведённый текст ошибки
        self::assertEquals([ 'message' => 'Ошибка.' ], $data);
    }
```

так же убираем пометку "незавершенный тест" из `testExistingLang()` в тесте
./api/tests/Functional/V1/Auth/Join/RequestTest.php

Запускаем полную проверку всего кода
```sh
make check
```

прогоняет весь код через:
- линтеры (php code-sniffer)
  проверка кодстайла и синтаксических ошибок
- статический анализатор кода(psalm)
  проверка типов и аннотаций с типами
- прогоняет проверку состояния БД, все ли миграции накатаны
- запускает все тесты и юнит и функциональные
- накатывает фикстуры для дев-окружения
  (заполняет бд данными нужными для локально разработки)


Таким образом задача по переводу сообщений валидации и исключений выполнена.
