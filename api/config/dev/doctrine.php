<?php

declare(strict_types=1);

use Doctrine\ORM\Tools\Console\Command;
use Doctrine\ORM\Tools\Console\Command\SchemaTool;
use Doctrine\ORM\Tools\Console\EntityManagerProvider\SingleManagerProvider;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

return [
    'config' => [
        'doctrine' => [
            'dev_mode' => true,
            'cache_dir' => null,
            'proxy_dir' => __DIR__ . '/../../var/cache/' . PHP_SAPI . '/doctrine/proxy',
            'subscribers' => [
                App\Data\Doctrine\FixDefaultSchemaSubscriber::class,
            ],
        ],
    ],


    // --------------------------------------------------------------------- \\
    //                   CLI Commands for bin/app.php
    // --------------------------------------------------------------------- \\

    Command\InfoCommand::class =>
    static function (ContainerInterface $c): SymfonyCommand {
        /** @var Doctrine\ORM\Tools\Console\EntityManagerProvider emp */
        $emp = $c->get(SingleManagerProvider::class);
        return new Command\InfoCommand($emp);
    },

    SchemaTool\DropCommand::class =>
    static function (ContainerInterface $c): SymfonyCommand {
        /** @var Doctrine\ORM\Tools\Console\EntityManagerProvider emp */
        $emp = $c->get(SingleManagerProvider::class);
        return new SchemaTool\DropCommand($emp);
    },
];
