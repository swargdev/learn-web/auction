<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Negotiation\LanguageNegotiator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 *
 * 12-12-2023
 * @author Swarg
 */
class ContentLanguage implements MiddlewareInterface
{
    /**
     * @var string[] Allowed languages
     */
    private array $languages = [];

    /**
     * @param string[] $languages
     */
    public function __construct(array $languages = [])
    {
        $this->languages = $languages;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $locale = $this->detectFromHeader($request);

        if (!empty($locale)) {
            $request = $request->withHeader('Accept-Language', $locale);
        }

        return $handler->handle($request);
    }

    /**
     * Returns the local using the Accept-Language header.
     */
    private function detectFromHeader(ServerRequestInterface $request): ?string
    {
        $accept = $request->getHeaderLine('Accept-Language');

        if (empty($accept)) {
            return null;
        }

        $default = $this->languages[0] ?? null;
        $locale = null;

        try {
            $negotiator = new LanguageNegotiator();
            $best = $negotiator->getBest($accept, $this->languages);
            if ($best instanceof \Negotiation\AcceptLanguage) {
                $locale = $best->getType();
            }
        } catch (\Exception $exception) {
            // ignore
        }

        if (empty($locale) || !in_array($locale, $this->languages, true)) {
            $locale = $default;
        }

        return $locale;
    }
}
