## UseCase ChangeEmail - Двухэтапная смена email-a


ChangeEmail так же делим на два этапа
  - Request    1й - юзер присылает новый адрес почты, отправка письма с токеном
  - Confirm    2й - приход токена смены почты изменяем email на новый

Так как у нас регистрация с подтверждением email-a то и при смене email тоже
надо будет подтверждать владение новым email-ом.

То есть при запросе на смену на новый email его надо не сразу менять, а:
- сначала поместить в приватное поле сущности User,
- проверить владение этим новым указанным email через переход по ссылки с письма,
- затем проверить что новый емейл еще не зареган на другого юзера
- и только потом делать текущим email-ом новый указанный, прошедший подтверждение.

Так же как при подтверждении регистрации
- отправляется письмо с токеном,
- юзер переходит по ссылке из письма с токеном,
- токен приходит от юзера и идёт валидация этого токена(смены емейла)
- при успехе идёт установка нового email-а как основного

Поэтому команда и разбивается на две части Request и Confirm
То есть из-за необходимости наличия подтверждения это уже не просто
редактирование некого поля свойства User-а в его личном кабинете.

Структура UseCase-а такая:
```
./api/src/Auth/Command/ChangeEmail/
├── Request             1й шаг - запрос смены email-а
│   ├── Command.php     id email  - в команде идишник юзера и новый емейл
│   └── Handler.php
└── Confirm             2й шаг - подтверждение смены через токен
    ├── Command.php     token
    └── Handler.php
```

Логика обработчика ChangeEmail\Request\Handler
./api/src/Auth/Command/ChangeEmail/Request/Handler.php

- ищем юзера в бд по его идишнику  `users->get(new Id($commane->id))`
- если такой User есть получаем его обьект иначе - ошибка и выход
- проверяем что новый емейл не используется у других юзеров на сайте
- генерируем стандартный токен с временем жизни и отправляем его в письме
  `$this->tokenizer->generate($date)` - генератор токенов получает текущую дату
  и на основе переданных ему настроек создаёт дату истечения нового токена
- делаем защиту от слишком частого спама запросов на смену емейла, через
  блокировку попыток послать новый запрос на смену адреса, которая приводит к
  посылке письма на указанную почту. Делаем так же как раньше - пока время жизни
  токена не истекло повторная отсылка запроса на смену емейла - запрещена.

- в самом методе $user->requestEmailChanging(Token, Date, Email) делаем
  проверки на валидацию токена и защиты от спама, и если проверки проходят то
  этот метод сохранит новый токен на смену email и сам новый email почты внутрь
  сущности User и передаст данные в БД, для последующей смены при подтверждении

введя в обработчике новый  метод `requestEmailChanging` пишем для него тесты:
./api/src/Auth/Test/Unit/Entity/User/User/ChangeEmail/RequestTest.php

RequestTest:
- testSuccess:
  - создаёт активного юзера с указанным емейлом
  - создаёт валидный, еще живой(не истёкший) токен
  - запрашивает смену email через requestEmailChanging
    передавая в этот метод токен, время и новый email
  - здесь и проверяем что новый емейл и токен смены емейла реально сохранились
    и оба равены переданным в метод и при этом старый-текущий емейл не изменён

- testSame  - проверка попытки сменить адрес на тот же самый - текущий
  должна быть ошибка - "емейл тот же"

- testAlready - проверка попытки повторного запроса письма с токеном на смену
  когда токен на смену уже выдан и еще действует - время его не истекло
  при таких случаях выдаётся ошибка - "смена уже запрошена" (защита от спама)

- testExpired - когда прошлый токен на смену емейла уже истёк
   - сначала делаем уже истёкший токен(срок жизни 1 час) и назначаем его в User
   - через 2 часа генерим новый свежий токен и снова передаём его в User
   - новый токен должен подхватиться т.к. старый уже истёк

- testNotActive - проверка попытки запросить смену емейла у еще не активного
  т.е. не прошедшего подтверждение емайла, указанному им при регистрации.

написав тесты для метода `requestEmailChanging` пишем в User сам этот метод
этот метод будет очень похож на метод `requestPasswordReset`

./api/src/Auth/Entity/User/User.php
```php
<?
class User {
    // ...
    private ?Token $newEmailToken = null;
    private ?Email $newEmail = null;
    // ...
    public function requestEmailChanging(
        Token $token,
        DateTimeImmutable $date,
        Email $email
    ): void {
        if (!$this->isActive()) {
            throw new DomainException("User is not active.");
        }
        // защита от попытки сменить емейл на тот же самый
        if ($this->email->isEqualTo($email)) {
            throw new DomainException('Email is already same.');
        }
        // защита от попыток спамить через запросы на смену емейла
        // если уже есть токен на смену и он еще не протух - запрет через ошибку
        if (
            $this->newEmailToken !== null &&
            !$this->newEmailToken->isExpiredTo($date)
        ) {
            // запрос уже запрошен ранее - токен еще валиден
            // если письмо с токеном не дошло - запроси смену еще раз но позже
            throw new DomainException('Changing is already requested.');
        }
        // запоминаем в поле класса новый емейл и токен - только в случае успеха
        // когда все проверки прошли и защита пропустила
        $this->newEmail = $email;
        $this->newEmailToken = $token;
    }
```

Дальше в хендлере при успехе метода requestEmailChanging будет идти
- записть измнений User в БД
- отправка письма с новым токеном через службу(service) NewEmailTokenSender

NewEmailTokenSender - пока что просто интерфейс-заглушка с одним методом send()
т.к. пока что у нас нет "мейлера" (системы отправки писем)


## Второй шаг - подтверждение смены емейла. ChangeEmail\Confirm

Работа подтверждения смены email-а по сути как в JoinByEmail\Confirm\Handler
./api/src/Auth/Command/JoinByEmail/Confirm/Handler.php

в JoinByEmail\Confirm\Handler мы
- сначала ищем юзера в БД по токену подтверждения(регистрации)
- и если находим то вызываем метод User.confirmJoin()

        users->findByConfirmToken($command->token))
        $user->confirmJoin($command->token, new DateTimeImmutable());


В нашем же текущем UseCase `ChangeEmail\Confirm\Handler`
./api/src/Auth/Command/ChangeEmail/Confirm/Handler.php нужно:

- искать по токену смены email     `$this->users->findByNewEmailToken()`
  (вводим такой метод в UserRepository)
- и вызывать подтверждение смены   `$user->confirmEmailChanging()`
  (будем вводить после тестов в User.confirmEmailChanging)

Если же юзер по токену смены емейла в бд не найден - выдаём ошибку "плохой токен"

Note: Причем здесь в ChangeEmail\Confirm\Command присылается только токен смены
email-а и ничего больше ни id ни email.

Дальше пишем тесты для метода `User.confirmEmailChanging(string, Date)`
./api/src/Auth/Test/Unit/Entity/User/User/ChangeEmail/ConfirmTest.php

- testSuccess описывает правильную логику смены емейла
  - делаем активного юзера
  - создаём свежий токен  смены емейла
  - запрос requestEmailChanging с указанием токена, времени и нового емейла
  - убеждаемся что токен смены сохраняется в поле через User.getNewEmailToken
  - триггерим подтверждение передавая свежий токен в confirmEmailChanging
  - убеждаемся что токен и newEmail сбросились и емейл поменялся на новый

- testInvalidToken - проверка попытки прислать левый токен
- testExpiredToken - пришел истекший токен - ошибка
- testNotRequested - попытка подтверждения когда запроса вообще не было - ошибка

дальше пишем User.confirmEmailChanging удовлетворяющий написанным тестам


```php
<?
class User {

    public function confirmEmailChanging(
        string $token,
        DateTimeImmutable $date
    ): void {
        // случай когда смена емейла еще не была запрошена - пустые поля
        if ($this->newEmail === null || $this->newEmailToken === null) {
            throw new DomainException('Changing is not requested.');
        }
        // валидация токена - что он вообще правильный и еще не протух
        $this->newEmailToken->validate($token, $date);

        $this->email = $this->newEmail;
        // сброс полей хранящих временные промежуточные состояния для смены:
        $this->newEmail = null;
        $this->newEmailToken = null;
    }
```

make test
make check

Дополнительные правки:
- ./api/src/Auth/Test/Builder/UserBuilder.php              withEmail
- ./api/src/Auth/Entity/User/Email.php                     isEqualTo
- ./api/src/Auth/Service/NewEmailConfirmTokenSender.php    send

