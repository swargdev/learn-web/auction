﻿# Как работать с Xdebug

- Общие принципы
- Связь Xdebug <-> IDE под капотом
- Устранение проблемы: debugging не работает (ошибок не пишет)

### Как диагностировать что вообще происходит с Xdebug-ом
- Запуск консольных скриптов в режиме отладки для образа api-php-cli


## Общие принципы работы с Xdebug:

- Xdebug - это расширение для php точнее для php-fpm и php-cli,
  это расширение установили только в dev-докер-образы api-php-fpm api-php-cli.

- Xdebug конфигурирутеся через файл:
  `./api/docker/development/php/conf.d/xdebug.ini`
  docker compose run --rm api-php-fpm cat /usr/local/etc/php/conf.d/xdebug.ini

- обычно режим отладки(debugging) начинается когда Xdebug в запросе от клиента
  видит Cookies с именем `XDEBUG_TRIGGER` или `XDEBUG_SESSION`(старое)
  [Подробно Docs](https://xdebug.org/docs/step_debug#activate_debugger)
  [Еще Docs]https://xdebug.org/docs/all_settings#start_with_request

- если в xdebug.ini прописан `xdebug.start_with_request=yes` тогда в режим
  дебага будет выполнятся каждый запускаемый php-скрипт.
  т.е. каждое обращение к нашему API серверу будет происходить в режиме отладки,
  а это значительно снижает скорость работы php интерпретатора.
  лучше триггерить дебаг-режим через куки.

- начиная сессию отладки Xdebug 3.x подключается к слушающему сокету по адресу
  прописываемому в xdebug.ini `xdebug.client_host`/`xdebug.client_port`
  при этом идёт установление связи с IDE или редактором кода в котором должна
  происходить отладка.
  по умолчанию для Xdebug 3.x порт 9003, для Xdebug 2.x был 9000
  `Note`: по умолчанию php-образы конфигурируют php-fpm работать на порту 9000

- Чтобы можно было дебажить в IDE должен быть включён `режим прослушивания`.
  Обычно это делают в виде кнопки `Start Listening` в PhpStorm и VSCode.
  под капотом при этом:
  для VSCode - поднимается отдельный процесс (например раширение vscode-php-debug)
  этот процесс:
     - слушает входящие соединения на локальном порту 9003
     - работает с IDE или редактором кода через стандартной протокол DAP.
  либо может напрямую быть вшит в само IDE
  Но суть в том, что открывается прослушивающий серверный сокет к которому
  должен присоединиться Xdebug чтобы образовалась связь php-fpm->Xdebug->IDE

- как только связь устанавливается можно из IDE управлять ходом отладки
  т.е. общение Xdebug-IDE идёт через tcp-сокет на локальном 9003 порту.

- Чтобы стригерить режим отладки при запросе страницы сайта
  можно использовать разные инструменты.
  Суть всех таких инструментов в том, чтобы посылать c http-заголовками
  специальную Cookie с именем `XDEBUG_TRIGGER` или `XDEBUG_SESSION`
  значение которой обычно содержит имя IDE например `PHPSTORM`
  - Это можно делать напрямую через сurl консольной командой
  - Либо можно использовать расширения для браузера например `Xdebug Helper`
  - Есть [сайт](http://php.babo.ist/xtest/) для проверки "включен" ли debugging

- [ВАЖНОЕ](#) Для того чтобы дебажить изнутри докер-образов нужно:
  настроить `Path mapping` - соответствие путей внутри докер образа и машины,
  где находятся исходники самого проекта.
  Выявлена тонкость при попытке войти в дебаг режим на связке
  `nvim + nvim-dap + vscode-php-debug`. Если не прописать или прописать ошибочный
  Path mapping тогда отладка просто не будет работать без вывода каких-либо
  сообщений.
  Чтобы узнать причину поломки можно:
  - включить запись логов в Xdebug(xdebug.ini) например в /tmp/xdebug.log
  - использовать специальную функцию xdebug_info(); прямо в своём php-коде.

## Про Лог Xdebug-а

Когда задан путь к логу - логи записываются в заданный файл.
(помни про права доступа т.к. запись идёт от лица под которым работает php)
В логи пишется всё общение которое Xdebug ведёт с IDE слушающей на 9003 порту.

## Связь Xdebug <-> IDE под капотом
Во время включения режима отладки Xdebug сам пытается подключится на дефолтный
или заданный в конфиге адрес и порт. Поэтому до начала отладки нужно в IDE
включить режим прослушивания что-то вроде нажать кнопку `Start Listening`.

На деле IDE может не само слушать 9003 порт, а использовать для этого некого
"посредника" - отдельный процесс. Например, в vscode дебаг так и происходит
- устанавливается отдельное расширение например `vscode-php-debug`
- при входе в режим прослушивания - запускается отдельно взятый процесс
  `node /path/to/vscode-php-debug/out/phpDebug.js`
  который открывает и слушает 9003 порт. к этому процессу и будет подключаться
  сам xdebug причем по протоколу [DBGp](https://xdebug.org/docs/dbgp)
  общение же меду VSCode и phpDebug.js будет идти по DAP протоколу


### Вывод функции xdebug_info()

Через эту php-функцию можно посмотреть имена всех настроек для Xdebug
и из значения.

Как это сделать на данном проекте.

можно временно добавить в `./api/bin/app.php` вызов `echo xdebug_info();`
так чтобы при запуске этого скрипта он сразу сработывал.
Далее просто запустить на выполнение этот php-скрипт:

команда запуска через композер изнутри нашего докер-контейнера `api-php-cli`
```sh
docker compose run --rm api-php-cli composer app
```
[Output here](./docs/08-install-xdebug/logs/xdebug_info.log)

В разделе Settings покажет список всех конфиг файлов. Один из них вот такой:
`/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini`,
чтобы узнать его содержимое (файл внутри контейнера api-php-cli)
```sh
docker compose run --rm api-php-cli cat /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
```
Output: `zend_extension=xdebug`

Updated: Для удобства разработки добавил команду app xdebug выводящую xdebug_info()


## Как узнал конкретное имя Cookie с которой работает Xdebug

Узнал через php-код `print_r($_COOKIE);`
расширение для браузера при "активированном жуке" в режим Debugging
добавляет в запрос такую Cookie(куку):

     [XDEBUG_SESSION] => PHPSTORM


## Запустить режим отладки при получении страницы сайта(API) через curl

* через curl шлю запрос к API с кукой включения дебаггинга (команда выше)
```sh
curl --cookie "XDEBUG_SESSION=PHPSTORM;path=/;" localhost:8081
```
(Note почему-то расширение для Firefox отказалось включатся для url-a к API)

Если надо слать пост-данные
```sh
curl -i -X POST -d '{"some":"data"}' http://localhost:8081 -b XDEBUG_SESSION=PHPSTORM
```

Убедится в том что такой запрос через curl Действительно включает в Xdebug
режим отладки можно вот так, через внешний сайт:
```sh
curl --cookie "XDEBUG_SESSION=PHPSTORM;path=/;" http://php.babo.ist/xtest/
```
Resonse:
```html
Checking for cookies... (this can take some time)<br>
...ready!<br>
<br><img src="debug_active.png"> Debugging is enabled<br>      <!-- ENABLED -->
<img src="trace_inactive.png"> Tracing is disabled<br>
<img src="profile_inactive.png"> Profiling is disabled<br>
```



## Устранение проблемы: debugging не работает (ошибок не пишет)

Во время настройки nvim + nvim-dap + vscode-php-debug возникла проблема:
- ставлю точку останова в public/index.php (:DapToggleBreakpoint)
- запускаю через :DapContinue `Start Listening for Xdebug` (мною настроенный)
- появлется dapui но без контекста и без локальных переменных - как будто сломано
- триггерю дебаг-запрос к api(localhost:8081) через curl с кукой `XDEBUG_SESSION`
- nvim-dap в nvim выдаёт предупреждение:
  "сессия(debugging) активна, но останов в брейкпоинте не проишел. что делать?"

        Session active, but not stopped at breakpoint>
        1: Terminate session
        2: Pause a thread
        3: Restart session
        4: Disconnect (terminate = true)
        5: Disconnect (terminate = false)
        6: Start additional session
        7: Do nothing


Как устранял проблему:
- проверяю запущен ли локально "слушатель" 9003 порта и кто его слушает
- проверяю пингуется ли 9003 порт локальной машины изнутри докер-контейнера
- Смотрим логи Xdebug что пошло не так


### Проверяю запущен ли локально "слушатель" 9003 порта и кто его слушает


```sh
nc -vz 127.0.0.1 9003
```
Когда нет "слушателя"

    nc: connect to 127.0.0.1 port 9003 (tcp) failed: Connection refused

Когад Start Listening включён

    Connection to 127.0.0.1 9003 port [tcp/*] succeeded!

При такой проверке в dapui(dap-repl) отобразится предупреждение:

    Failed initializing connection 1: connection closed (on close)

Это видимо просиходит потому что `nc` команда подключается и разрывает соединение


Еще один способ проверить открыт ли порт через Curl.(Закрыть руками через Ctrl-D)
```
Connection to 127.0.0.1 9003 port [tcp/*] succeeded!
```
проверить открыт ли порт (через telnet соединение)  будет висеть подключенным
```sh
curl -v telnet://127.0.0.1:9003
```
```
*   Trying 127.0.0.1:9003...
* Connected to 127.0.0.1 (127.0.0.1) port 9003 (#0)
```

Проверяю прослушивается ли серверный порт 9003
```sh
sudo netstat -tlp | grep 9003
tcp6     0    0    [::]:9003    LISTEN    262560/node
```
Здесь 262560 - это PID процесса, который слушает порт

Узнать полную команду запуска процесса со всеми переданными аргументами
```sh
cat /proc/262560/cmdline
```
```
node ~/.local/share/nvim/mason/packages/php-debug-adapter/extension/out/phpDebug.js
```
Этим убеждаемся что расширение vscode-php-debug запущено и ожидает подключений
от Xdebug


### Проверяю пингуется ли 9003 порт локальной машины изнутри докер-контейнера

```sh
docker compose exec api-php-fpm nc -vz host.docker.internal 9003
```
```
host.docker.internal (172.24.0.1:9003) open
```
`nc -vz` - проверить открыт ли порт но не устанавливать соединение

Смотрим список прописанных хостов для надёжности:
```sh
docker compose exec api-php-fpm cat /etc/hosts
```
```
127.0.0.1	localhost
::1	localhost ip6-localhost ip6-loopback
172.24.0.3	1f4bf488c543
172.24.0.1	host.docker.internal                  << адрес шлюза подсети
```

[Заметка](#) На Xdebug 2.x по умолчанию порт был 9000 на Xdebug 3.0 сделали 9003
проверяя доступность порта заметил такую вещь:
9000 порт в докер-образе api-php-fpm слушает сам php-fpm
```sh
docker compose exec api-php-fpm netstat -tnpl | grep 9000
```
```
tcp  0  0  :::9000  :::*   LISTEN   1/php-fpm.conf)
```


### Смотрим логи Xdebug что пошло не так

Прописал в настройки xdebug ./api/docker/development/php/conf.d/xdebug.ini
путь к логу `xdebug.log=/tmp/xdebug.log` (Внутри контейнера api-php-fpm)
Судя по [выводу xdebug_index()](./docs/08-install-xdebug/logs/xdebug_info.log)
чтобы включить лог нужно просто прописать путь к файлу (по умолчанию путь пустой)
(уровень логирования не менял)

Проверяю поведение xdebug-a в разных условиях(что пишется в его логе):

1) Режим `Start Listening` выключен (в моём случае расширение vscode-php-debug)
  то есть локальный порт 9003 никто не слушает
  (в nvim завершить начатую debugging сессию можно через `:DapTerminate`)

* через curl шлю запрос к API с кукой включения дебаггинга (команда выше)

* смотрим лог Xdebug:
```sh
docker compose exec api-php-fpm cat /tmp/xdebug.log
```
```
[12] Log opened at 2023-09-13 17:59:01.047938
[12] [Step Debug] INFO: Connecting to configured address/port:
                        host.docker.internal:9003.

[12] [Step Debug] WARN: Creating socket for 'host.docker.internal:9003',
                        poll success, but error: Operation in progress (29).

[12] [Step Debug] ERR: Could not connect to debugging client.
                       Tried: host.docker.internal:9003
                       (through xdebug.client_host/xdebug.client_port).
```

2) Режим `Start Listening` включен. (через связку nvim+nvim-dap+vscode-php-debug)
  то есть расшинение vscode-php-debug запущено отдельным процессом в локальной
  машине и слущает 9003 порт.
  На nvim debugging сессию включаю так:
  `:DapContinue` -> `Start Listening for Xdebug`
  (пункт выбирается из списка. само имя и конкретные его детали прописаны
  [здесь](~/.dotfiles/nvim/.config/nvim/lua/user/dap/vscode-php-debug.lua)
  [смотри настройки nvim-a](./docs/08-install-xdebug/06-nvim.md)

* через curl шлю запрос к API с кукой включения дебаггинга

* смотрим лог Xdebug
Часть вывода из [лога](./docs/08-install-xdebug/logs/try-debug-on-listening.log)
```sh
[11] [Step Debug] <- breakpoint_set -i 6 -t line
  -f file:///app/api/src/Http/Action/HomeAction.php -n 21
[11] [Step Debug] WARN: Breakpoint file name does not exist:
/app/api/src/Http/Action/HomeAction.php (No such file or directory).
[11] [Step Debug] ->
<response xmlns="urn:debugger_protocol_v1"
  xmlns:xdebug="https://xdebug.org/dbgp/xdebug"
  command="breakpoint_set"
  transaction_id="6"
  id="110002"
  resolved="unresolved">
</response>
```
`<-` то что приходит из-вне - т.е. это запросы от DAP
`->` то что отправляется в ответ.

Здесь можно увидеть неправильно настроенный маппинг:

    -f file:///app/api/src/Http/Action/HomeAction.php -n 21

    WARN: Breakpoint file name does not exist:
    /app/api/src/Http/Action/HomeAction.php (No such file or directory).

Правильно вот так (localSourceRoot):
```lua
dap.configurations.php = {
  {
    name = 'Start Listening for Xdebug',
    type = 'php',
    request = 'launch',
    port = 9003,

    localSourceRoot = '~/dev/php/auction/api',  -- <<<< Add this field to fix issue

    pathMappings = {
      ['/app/'] = "~/dev/php/auction/api"
    },
  },
```

`Note`:
Почему то этот ответ от Xdebug об ошибке маппинга не отобразился в nvim в DapUI
TODO сделать чтобы выводилось предупреждение об неправильно настроенном маппинге


Посмотреть что в настройках xdebug.ini внутри образа
```sh
docker compose run --rm api-php-fpm cat /usr/local/etc/php/conf.d/xdebug.ini
```



## Запуск консольных скриптов в режиме отладки для образа api-php-cli

- разбираюсь как вообще дебажить php-cli скрирты запускаемые из консоли внутри
  докер-образа
- чиню debuggig для образа api-php-cli.

### Как дебажить php-cli

Для того, чтобы запустить режим отладки консольного php-скрита нужно:
- установленное в php-cli расширение xdebug (+)
- настроенный xdebug (+)
- передать одну из спец. переменных окружения(триггерящих режим отладки)
  внутрь контейнера с php-cli:

Передать переменную и проверить реально ли она задана внутри контейнера:
```sh
docker compose run --rm -e XDEBUG_TRIGGER=1 api-php-cli env | grep XDEBUG
XDEBUG_TRIGGER=1        # << Output
```

Команда для запуска скрипта в режиме отладки изнутри контейнера:
```sh
docker compose run --rm -e XDEBUG_TRIGGER=1 api-php-cli php bin/app.php --ansi
```
то же самое через настроенный в composer.json маппинг алиас -> полная команда
```sh
docker compose run --rm -e XDEBUG_TRIGGER=1 api-php-cli composer app
```

### Как диагностировать что вообще происходит с Xdebug-ом

Внутри ./api/bin/app.php для диагностики состояния xdebug разместил вывод
echo xdebug_info(); вот вывод этой функции:

Для диагностики того что происходит можно:
- либо смотреть вывод в лог файл /tmp/xdebug.log (настройка в xdebug.ini)
- либо использовать вызов функции xdebug_info(); прямо в отлаживаемом php-коде.

Временно помещаю строку `echo xdebug_info();` внутрь скрипта `./api/bin/app.php`


Шаги для проверки дебага:
- nvim включаю режим `Start Listening for xdebug`
- ставлю точку останова внутри срипта `./api/bin/app.php`
- в консоли запускаю образ api-php-cli на выполнение php-скрипта изнутри образа:
```sh
docker compose run --rm -e XDEBUG_TRIGGER=1 api-php-cli composer app
```

Выводит xdebug_info() когда host.docker.internal не настроен правильно:
(Это часть вывода api/bin/app.php прямо в консоль:)
```
Diagnostic Log
[Step Debug] WARN: Creating socket for 'host.docker.internal:9003', getaddrinfo: Resource temporarily unavailable.
[Step Debug] ERR: Could not connect to debugging client. Tried: host.docker.internal:9003 (through xdebug.client_host/xdebug.client_port).

Step Debugging
Debugger is not active
```
Всё сразу понятно - xdebug не смог подключится к слушающему сокету на локальной
машине(vscode-php-debug-у) т.к. в api-php-cli небыло добавлена австоустановка
адреса шлюза под dns host.docker.internal
То есть ошибка была в том, что и для api-php-cli нужно так же пробрасывать
такой же entrypoint.sh как и для api-php-fpm

Как исправил(Починил дебаг для php-cli):

- добавил в докерфайл образа api-php-cli такое же переопределение entrypoint.sh
  как уже было сделано в докерфайле для api-php-fpm
- пересобрал дев-окружение `make init`

Те же действия - триггерем консольной командой запуск скрипта из контейнера:
```
Diagnostic Log
No messages
                              Step Debugging
Debugger is active
Connected Client => host.docker.internal:9003

DBGp Settings
Max Children => 100
Max Data => 1024
Max Depth => 1
Show Hidden Properties => No
Extended Properties => Yes
Notifications => Yes
Resolved Breakpoints => Yes
Breakpoint: Details => No
Breakpoint: Include Return Values => Yes
```

Теперь всё работает как надо и остановка в заданном брейкпоинте работает.

docker compose run --rm -e XDEBUG_TRIGGER=1 api-php-cli php -r "xdebug_info();"

Для удобства просмотра диагностического вывода xdebug_info();
добавил консольную команду `app xdebug` и её обработчик:
`./api/src/Console/XDebugCommand.php`
```sh
docker compose run --rm -e XDEBUG_TRIGGER=1 api-php-cli composer app xdebug
```
полная команда без прописанного алиаса:
```sh
docker compose run --rm -e XDEBUG_TRIGGER=1 api-php-cli php bin/app.php --ansi xdebug
```

[передача в докер-контейнер своих EnvVar-ов](./docs/docker-notes/index.md)





## Полезные ссылки:

https://xdebug.org/docs/step_debug
https://xdebug.org/docs/dbgp              DBGp protocol.
https://github.com/vim-vdebug/vdebug      vim плагин под DBGp-протокол

https://xdebug.org/docs/step_debug#activate_debugger
https://xdebug.org/docs/step_debug#troubleshoot
https://xdebug.org/docs/all_settings#start_with_request

