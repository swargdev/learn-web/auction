
## Выход на Smoke testing

E2E тесты отличаются от юнит тестов(модульных) и интерграционных тестов тем,
что они более тяжелые, выполняются дольше и для своей работы требуют больше
ресурсов. Дальше мы будет делать свой CI-CD Pipeline. Который будет полностью
автоматически запускать все имеющиеся у нас тесты в том числе и E2E-тесты.
Но возможна ситуация когда мы где-то напортачили например в Docker-файле
фронтенда или api, и в результате сайт вообще не будет работать так как после
этого, например nginx сервер даже не поднимается, а у нас уже например есть
целая пачка E2E тестов которые ресурсоёмки и требуют вычислительных мощностей.
и если как-то не предусмотреть такой расклад, то даже если у нас сервер упал
и вообще ничего не работает - сайт не открывается то всё равно в пайплайне
будут долбить до конца все имеющиеся е2е тесты, расходуя ресурс и время.
А по итогу в отчете покажет что все тесты красные(завалены). И причина например
не в ошибках в коде - а просто сервер не стартанул.
Вот чтобы так не делать и заверашать тестирование преждевренеменно с диагнозом -
"система полностью сломана" и существуют Smoke-тесты. Т.е. можно сделать
несколько ключевых тестов которые можно использовать по-быстрому для того, чтобы
понять работает вообще система в целом или нет. Например сделать один тест для
проверки открытия главной страницы сайта. - если страница отркрылась - норм
сервера подняты, система работает - можно тестировать дальше.

https://www.atlassian.com/continuous-delivery/software-testing/types-of-software-testing
7. Smoke testing

Smoke tests are basic tests that check the basic functionality of an application.
They are meant to be quick to execute, and their goal is to give you the
assurance that the major features of your system are working as expected.

Smoke tests can be useful right after a new build is made to decide whether or
not you can run more expensive tests, or right after a deployment to make sure
that they application is running properly in the newly deployed environment.

Задача этих тестов побыстрому проверить запустилась ли система как надо или нет.

Поэтому для дальнейшей автоматизации мы выделим минимальный набор е2е тестов
которые и будут проверять запустился сервер или нет.
При этом когда мы будем развёртывать stating-сервер то можно будет запустить
наши Smoke-тесты чтобы понять поднаялась система или нет, и если поднялся то уже
тогда запускать полноценный набор е2е-тестов.
Ну а если будет ошибка в smoke-тестах то просто выводить отчёт об этом и дальше
всю пачку тяжелых e2e тестов не запускать - смысла то нет - все и так завалятся.


## Создаём smoke-тесты

Вообще под это можно даже отдельный каталог выделить по аналогии с
- api, frontend, cucumber

А можно просто испрользовать подход с тегированием тестов
То есть помечать некоторые сценарии своими уникальными тегами.
Например выделяем ключевые е2е-тесты помечая их сценарии аннтотацией `@smoke`:

./cucumber/features/home.feature
```
Feature: View home page
  In order to check home page content
  As a guest user
  I want to be able to view home page

  @smoke                                           <<<<<<<<<<
  Scenario: View home page content
    Given I am a guest user
    When I open "/" page
    Then I see welcome block
```

Дальше просто добавляем команду для запуска только smoke-тестов(по тегу):
```sh
cucumber-js --tags @smoke --fail-fast --parallel 2
```

./cucumber/package.json
```json
  "scripts": {
    "smoke": "cucumber-js --tags @smoke --fail-fast --parallel 2",
    "e2e": "cucumber-js --parallel 2",
    "lint": "eslint --ext .js,.jsx features",
    "lint-fix": "eslint --fix --ext .js,.jsx features"
  },
```

- `fail-fast` - сразу завершать тестирование на первой ошибке

Алиас в Makefile для быстрого запуска
```Makefile
test-smoke: api-fixtures cucumber-clear cucumber-smoke
test-e2e: api-fixtures cucumber-clear cucumber-e2e
```
