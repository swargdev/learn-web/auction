- Рефакторим код контроллера Join/RequestAction
- Первое что здесь нужно исправить то декодирование тело запроса в json
- Детально об Middleware и как это использовать
- Middleware in Slim
- Обработка строк в массиве через trim. Свой middleware ClearEmptyInput
- Добавление очистки от пустых файлов в ClearEmptyInput
- Переносим обработку DomainException в Middleware DomainExceptionHandler


## Рефакторим код контроллера Join/RequestAction

смотрим на нами уже написанный RequestAction и думаем как его улучшить

./api/src/Http/Action/V1/Auth/Join/RequestAction.php
```php
<?
final class RequestAction implements RequestHandlerInterface {

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @psalm-var array{email:?string, password:?string} $data */
        $data = json_decode((string)$request->getBody(), true);

        $command = new Command();
        $command->email = trim($data['email'] ?? '');
        $command->password = trim($data['password'] ?? '');

        try {
            $this->handler->handle($command);
            return new EmptyResponse(201);
        } catch (DomainException $e) {
            return new JsonResponse(['message' => $e->getMessage()], 409);
        }
    }
```

## Первое что здесь нужно исправить то декодирование тело запроса в json

```php
<?
        /** @psalm-var array{email:?string, password:?string} $data */
        $data = json_decode((string)$request->getBody(), true);
```
Здесь мы декодируем тело пришедшего запроса на получение из него json-а
специально для псалма нужно явно казать что ожидаем строку `(string)`
т.к. из getBody() может вылетать и null, в то время как json_decode работает
только со строкой но не с NULL

Этот код можно вынести в отдельный метод например в `Json::decode()` из
./api/tests/Functional/Json.php


Но можно сделать и по другому.
Сделать автоматический парсинг json-строки пришедшей в запросе клиента.
Причем размещать этот пришедший json в виде готового к работе массива
в удобный стандартный метод `$request->getParsedBody()`
Причем делать это автоматически только когда приходит
`Content-Type: application/json` тем самым сделав наше API универсальным,
т.е. способным принимать запросы разных типов.
Наример если будет прилетать запрос не в json а в xml то тоже самое делать с
xml - парсить и размещать в виде массива в метод $request->getParsedBody()
Если так сделать то наше API будет способно принимать запросы в разных форматах
и при этом отвечать одинаково что для json что для xml


Как это можно сделать:

`$request->getParsedBody()` - возвращает массив с полями стандартной http-формы
из тела запроса пришедшего от клиента.
То есть этот метод парсит POST-запрос с Content-Type =
`application/x-www-form-urlencoded` и раскладывает его в обычный php массив.

Но это не будет работать для запросов с типом `application/json`
т.е. для пришедшего json-а, метод `$request->getParsedBody()` вернёт пустой массив

Поэтому для доступа к пришедшему json-у, как к обычному php-массиву его
- сначала нужно запрасить в виде пришедшей строки `$request->getBody()`
- затем эту строку содержащую json распарсить в php-массив


можно для удобства автоматизировать этот процесс так, чтобы под капотом
Json  парсился из строки и помещался так же в `$request->getParsedBody()`
чтобы и через этот метод можно было так же удобно обращаться к пришедшему Json
(как будто к нам пришли данные из обычной http-формы.)

для того, чтобы поместить распершенный Json в getParsedBody в виде массива нужно:
использовать метод withParsedBody который присвоит заданный массив (json)

```php
<?
// берём строку достаём из неё json в виде php-массива и помещаем в ParsedBody
$request = $request->withParsedBody(json_decode((string)$request->getBody()));

$request->getParsedBody(); // теперь возращает массив с пришедшем Json-ом
```

## Детально об Middleware и как это использовать

Чтобы автоматизировать этот процесс распознования что там за формат пришел
и его конвертации в php-массив с выкладкой в getParsedBody() можно использовать
Middleware.
Например мы уже используем ErrorMiddleware для того чтобы наш ErrorHandler
оборачивал все запросы наших экшенов в блок try-catch с отловом исключений.

есть экшен


``` php
<?
    joinAction($request) { // наш экшен-контроллер
        return new Response(); // возвращающий некий ответ например
       // EmptyResponse JsonResponce и это может быть любой другой
    }
```

в таких фреймворках как symfony и yii код обработки приходящих запросов можно
размещать только в контроллерах(экшенах) потому что приходящий request из
index.php пройдя маршрутизацию передаётся напрямую именно в контроллер.
и невозможно поместить между контроллером и маршрутизатором никаких посредников.

В PSR-овских фреймворках, а к ним относятся например такие как `laravel` и `slim`
можно использовать подход с использованием посредников называемых `middleware`.

То есть кроме контроллеров(экшенов) можно размещать свой код в неких посредниках,
которые будут вызываться перед нашими контроллерами.

То есть упрощенно чисто на уровне функций:
у нас есть функция нашего контроллера(экшена) `joinAction`
мы пишем еще одну функцию оборачивающую наш экшен:

К примеру мы можем назвать эту функцию `bodyParamsMiddleware`:
```php
<?
function bodyParamsMiddleware(ServerRequestInterface $request) {
    // в этой функции посреднике мы размещаем свой полезный код
    // полезная работа до вызова нашего экшена(контроллера)
    $request = $request->withParsedBody(json_decode((string)$request->getBody()));

    return joinAction($request);//вызов нашего экшена с подготовленными данными
}
```
Дальше когда приходит запрос от клиента он проходит файл `public/index.php`
в нём прописана маршрутизация(API) в которой опеределяется какой именно экшен
должен быть вызван для конкретного маршнута. Марштур отпределился и поток
управления передаёся дальше именно в нашу функцию посредник `bodyParamsMiddleware`

Ну и сама эта функция-посредник `bodyParamsMiddleware` просто делает полезную
дополнительную, нужную нам работу, а именно преобразовывает json-строку в массив
и помещает его для нас в метод `$request->getParsedBody()`  ну и дальше
а вызывает код нашего контроллера(экшена) т.е. функцию `joinAction`

Другими словами:
Работая с PSR-овскими фреймворками можем писать свои посредники(`middleware`)
которые будут вызываться перед вызовом наших контроллеров(экшенов)

таких middleware(посредников) в нашем коде может быть несколько. Например
Один посредник производит первоначальную подготовку пришедших данных
(например как в нашем случае - перобразует json-строку в удобный php-массив)
Сделующий посредник(middleware) вычищает ненужные поля из этого массива.
причем эти посредники образуют цепочку вызовов, в конце которой идёт вызов
нашего контроллера(экшена) который и должен быть вызван.
(который должен быть вызван определяется по прописанной в API маршрутизации).

Цепочка вызовов задаётся передачей в посредник дополнительного параметра `$next`
который по сути и есть интерфейс с заведомо известным методом для точки входа
контроллера или middleware.

Таким образом образуется цепочка переходов по которой будет проходить пришедший
от клиента запрос(request) проходя все посредники дойдя до контроллера
в котором сформируется ответ(response). и пошел бы возращаться обратно по той
же самой цепочке в обратном порядке. Причем и в обратном направлении ответ
respose может в посредниках обрабатываться неким образом например дополняясь
дополнительными http-заголовками.

Поэтому нашу задумку по декодированию json строки в массив мы можем реализовать
через psr-овский посредник называемый middleware.

Но в комплекте фреймворка Slim уже есть такой готовый middleware-класс:
[Slim\Middleware\BodyParsingMiddleware](./api/vendor/slim/slim/Slim/Middleware/BodyParsingMiddleware.php)

Подключаем:

./api/config/middleware.php
```php
<?php

declare(strict_types=1);

use Slim\App;
use Slim\Middleware\ErrorMiddleware;

return static function (App $app): void {
    $app->add(ErrorMiddleware::class);
    $app->addBodyParsingMiddleware();        // <<<
};
```

Глянув исходники (стантарнув с хэлпер-метода addBodyParsingMiddleware) можно
узнать, что этот миддлвейр уже умеет парсить такие входные данные как:
- `application/json`
- `application/x-www-form-urlencoded`
- `application/xml`
- `text/xml`

```php
<?
class BodyParsingMiddleware implements MiddlewareInterface {

    protected function registerDefaultBodyParsers(): void
    {
        $this->registerBodyParser('application/json', static function ($input) {
            $result = json_decode($input, true);

            if (!is_array($result)) {
                return null;
            }

            return $result;
        });

        $this->registerBodyParser('application/x-www-form-urlencoded',
            static function ($input) {
                parse_str($input, $data);
                return $data;
            }
        );

        $xmlCallable = static function ($input) {
            $backup = self::disableXmlEntityLoader(true);
            $backup_errors = libxml_use_internal_errors(true);
            $result = simplexml_load_string($input);

            self::disableXmlEntityLoader($backup);
            libxml_clear_errors();
            libxml_use_internal_errors($backup_errors);

            if ($result === false) {
                return null;
            }

            return $result;
        };

        $this->registerBodyParser('application/xml', $xmlCallable);
        $this->registerBodyParser('text/xml', $xmlCallable);
    }
```

Это означает что Slim в себе уже содержит готовое решение нашей проблемы
можно использовать стандартный middleware из самого фреймворка Slim.

## Middleware in Slim

Но есть одна тонкость того, как идёт опеределение миддлвейров в Slim.

Вообще Middleware должны вызываться по цепочке.
То есть сначала выполнятся один миддлвейр потом другой и в конце наш контроллер

И Обычно в других микрофреймворках потипу старого ZendExpressio в отличии от
Slim оборачивание идёт предсказуемым образом - каждый последующий добавляемый
миддлрейр становится посредником который вызывается после того который был
уже добавлен.
То есть в других не Smil-микрофреймворках такая запись создат цепочку
```php
<?
return static function (App $app): void {
    $app->add(ErrorMiddleware::class);
    $app->addBodyParsingMiddleware();
};
```

```
ErrorMiddleware
   BodyParsingMiddleware
      Action
```

В Slim же добавление миддлвейров происходит наоборот. каждый новый прописывается
как "родительский" над всеми уже прописанными(добавленными) ранее миддлвейрами
т.е. добавление идёт в голову списка.
Если миддлвейры в Slim прописать добавление вот таким образом:
Поэтому при коде выше для Slim миддлвейры запишутся вот так:

```
BodyParsingMiddleware   - будет вызываться первым
    ErrorMiddleware     - вторым
      Action
```

То есть нужно учитывать это и для того чтобы ErrorMiddleware быть первым среди
всех, а именно это нам и надо т.к. он оборачивает в try-catch всё остальное
чтобы писать ошибки в логи. то надо указывать его самым последним в списке

Таким образом прописав стандартный миддлвейр BodyParsingMiddleware:
```php
<?
return static function (App $app): void {
    $app->addBodyParsingMiddleware();
    $app->add(ErrorMiddleware::class); // последний при обьявлении - первый при вызове
};
```

можно упростить код Join/RequestAction:



```php
<?
final class RequestAction implements RequestHandlerInterface{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @psalm-var array{email:?string, password:?string} $data */
        $data = $request->getParsedBody();                              // <<<<
        // $data = json_decode((string)$request->getBody(), true);     было так

        $command = new Command();
        $command->email = trim($data['email'] ?? '');
        $command->password = trim($data['password'] ?? '');

        try {
            $this->handler->handle($command);
            return new EmptyResponse(201);
        } catch (DomainException $e) {
            return new JsonResponse(['message' => $e->getMessage()], 409);
        }
    }
```

Причем важно понимать что такая простая строка `$request->getParsedBody()`
теперь позволяет работать нашему контроллеру не только с json но и с xml

После обновления кода - прогоняем тесты чтобы убедиться что всё работает.
`make test`


## Обработка строк в массиве через trim. Свой middleware ClearEmptyInput

Следующее что брасается в глаза в коде контроллера:

```php
<?
        $command = new Command();
        $command->email = trim($data['email'] ?? '');
        $command->password = trim($data['password'] ?? '');
```
функция `trim` отсекает пробелы с обоих боков строки.
чтобы отсечь пробелы при вводе данных из полей форм

Нужен как-то способ автоматического trim-инаг всех значений пришедшего в
контроллер массива из пришедшего массива.
Для этого уже напишим свой миддлвейр.

Его работа:
- брать массив из $request->getParsedBody()
- проходить циклом по полям массива и вызывать для строковых значений `trim()`
- причем делать это рекурсивно для всего массива.

Назовём наш новый класс ClearEmptyInput и разместим его и его тест в
- ./api/src/Http/Middleware/ClearEmptyInput.php
- ./api/src/Http/Test/Unit/Middleware/ClearEmptyInputTest.php

В тесте формируем новый запрос(request) а именно
`Psr\Http\Message\ServerRequestInterface`
содержащий в себе вложенный массив преобразование которого и будем проверять.

```php
<?
class ClearEmptyInputTest extends TestCase
{
    public function testParsedBody(): void
    {
        $middleware = new ClearEmptyInput();
        $request = (new ServerRequestFactory())
        ->createServerRequest('POST', 'http://site')
        ->withParsedBody([  // задаём массив для $request->getParsedBody()
            'null' => null, // его наш миддлвейр и должен бдует обработать
            'space' => ' ',       // применив ко всем строкам trim
            'string' => 'String ',
            'int' => 42,
            'nested' => [
                'null' => null,
                'space' => ' ',
                'name' => ' Name',
            ],

        ]);
        # ... дальше надо будет как-то делать проверку
    }
```

Знакомимся с внутренней структуры Middleware, чтобы понять как его проверять:

./api/src/Http/Middleware/ClearEmptyInput.php
```php
<?
namespace App\Http\Middleware;

use Psr\Http\Message\ResponseInterface;

class ClearEmptyInput implements Psr\Http\Message\MiddlewareInterface
{
    public function process(
        Psr\Http\Message\ServerRequestInterface $request, // запрос от клиента
        Psr\Http\Message\SRequestHandlerInterface $handler // следующий в цепочке
    ): ResponseInterface {
        // полезная работа здесь

        return $handler->handle($request); // передать управление следующему
    }
}
```
Middleware - это класс реализующий стандартный prs-овский `MiddlewareInterface`

в метод process принимаются:
- $request - это сам запрос пришедший к серверу обёрнутый в станадартный psr
             ServerRequestInterface
- $handler - это либо следующий middleware в цепочке вызовов либо оконечный
             контроллер(экшен)

Принцип всех middleware очень простой - по сути это цепочка вызовов где каждый
middleware - это ячейка-звено в цепочке, в которой можно производить некие
действия над переданным в эту цепочку $request-ом и затем передать управление
следующему звену - другому миддлвейру или контроллеру.


это может быть реализовано примерно так

```php
<?
namespace App\Http\Middleware;

use Psr\Http\Message\ResponseInterface;

class ClearEmptyInput implements Psr\Http\Message\MiddlewareInterface
{
    public function process(
        Psr\Http\Message\ServerRequestInterface $request,
        Psr\Http\Message\SRequestHandlerInterface $handler
    ): ResponseInterface {

        $request = $request
        // очитка массива и обновление его внутри переменной $request
            ->withParsedBody(self::filterStrings($request->getParsedBody()));

        // передача обновлённого $request дальше по цепочке.
        return $handler->handle($request); // передать управление следующему
    }

    private static function filterStrings(array $array): array
    {
        // здесь должна быть обработка $array
        return $array;
    }
}
```

Теперь понятно как нужно писать тестовый метод для этого класса.

Сам $request в тесте мы уже создали. дальше нужно создавать заглушку-мок для
$handler и именно в нём проверять какой именно $request будет возвращён.
Должен быть очищенный обрботанный через нашу функцию filterStrings,
а не оригинальный.

```php
<?
class ClearEmptyInputTest extends TestCase
{
    public function testParsedBody(): void
    {
        $middleware = new ClearEmptyInput();
        $request = (new ServerRequestFactory())
        ->createServerRequest('POST', 'http://site')
        ->withParsedBody([
            // тот же самый вложенный массив с разного типа данными
            // "грязный" массив с пробелами (см выше)
        ]);

        // создаём обычным образом мок-заглушнку нужного интерфейса
        $handler = $this->createMock(RequestHandlerInterface::class);
        // говорим что обязательно должен быть вызван метод(handler) ровно 1 раз
        $handler->expects(self::once())->method('handle')
            ->willReturnCallback(
            // это функция где мы проверяем $request входящий в метод handler
            static function (ServerRequestInterface $request): ResponseInterface {
                self::assertEquals([
                    // очищенный от пробелов $request который ожидаем
                    'null' => null,
                    'space' => '',
                    'string' => 'String',
                    'int' => 42,
                    'nested' => [
                        'null' => null,
                        'space' => '',
                        'name' => 'Name',
                    ],
                ], $request->getParsedBody());
                return (new ResponseFactory())->createResponse();
            });

        // вот здесь и идёт вызов очистки "грязного"  реквеста и передача в
        // мок-заглушку $handler уже очищенного реквеста.
        $middleware->process($request, $handler);
    }
```


## Добавление очистки от пустых файлов в ClearEmptyInput

Вообще очистка может быть нужна не только для строк в массиве
$request->getParsedBody() но так же и при загрузке файлов на сайт.

$request->getUploadedFiles()
может быть ситуация когда юзер загружает файл, случается сбой и файл доходит
пустым.


создание реального загруженного файла со статусов ERR_OK
```php
<?
    $realFile = (new UploadedFileFactory())->createUploadedFile(
        (new StreamFactory())->createStream(''), // пустой
        0,
        UPLOAD_ERR_OK,
    );
```

Ситуация когда форма отправлена, но файл прикрепить забыли
запрос с формы для отправки отправился, но в суперглальной $_FILES файл записан
с пустым значением
```php
<?
    $noFile = (new UploadedFileFactory())->createUploadedFile(
        (new StreamFactory())->createStream(''),
        0,
        UPLOAD_ERR_NO_FILE,
    );
```

```php
<?
    $request = (new ServerRequestFactory())
        ->createServerRequest('POST', 'http://test')
        ->withUploadedFiles([
            'real_file' => $realFile,  // реальный
            'none_file' => $noFile,    // не существующий
            'files' => [$realFile, $noFile], // т.к. файлы можно загружать и
             // вложенным массивом тоже.
        ]);

    // в моке нам надо проверить что остались только реальные файлы а пустышки
    // удалились
    $handler = $this->createMock(RequestHandlerInterface::class);
    $handler->expects(self::once())->method('handle')
    ->willReturnCallback(
        static function (ServerRequestInterface $request) use ($realFile)
        : ResponseInterface
        {
            self::assertEquals([
                'real_file' => $realFile,
                'files' => [$realFile],
            ], $request->getUploadedFiles());
            return (new ResponseFactory())->createResponse();
        }
    );

    $middleware->process($request, $handler);
```

реализация фильтра ошибочных(пустых) файлов:
```php
<?
class ClearEmptyInput implements MiddlewareInterface {

    /**
     * @param array<string,array|UploadedFileInterface> $items
     * @return array<string,array|UploadedFileInterface>
     */
    private static function filterFiles(array $items): array
    {
        $result = [];

        /**
         * @var string $key
         * @var array|UploadedFileInterface $item
         */
        foreach ($items as $key => $item) {
            if ($item instanceof UploadedFileInterface) {
                if ($item->getError() !== UPLOAD_ERR_NO_FILE) {
                    $result[$key] = $item;
                }
            } else {
                $result[$key] = self::filterFiles($item);
            }
        }

        return $result;
    }
```

просто отбрасывает все те файлы у которых есть ошибки

Регистрируем свой Middleware\ClearEmptyInput:


```php
<?
use App\Http\Middleware;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;

return static function (App $app): void {
    $app->add(Middleware\ClearEmptyInput::class);     // новый миддлвейр
    $app->addBodyParsingMiddleware();
    $app->add(ErrorMiddleware::class);
};
```
Т.к. он в самом начале списка значит вызыватся он будет последним из всех
мидлвейров прямо до вызова контроллера.

теперь можно удалить trim из RequestAction:

```php
<?
final class RequestAction implements RequestHandlerInterface
{
    private Handler $handler;

    public function __construct(Handler $handler)
    {
        $this->handler = $handler;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @psalm-var array{email:?string, password:?string} $data */
        $data = $request->getParsedBody();

        $command = new Command();
        $command->email = $data['email'] ?? '';         // убрали trim()
        $command->password = $data['password'] ?? '';

        # ...
    }
```
Проверяем проходят ли тесты - проходят.

проверим новый миддлвейр добавив пробел скраю email в тесте регистрации нового


```php
<?
    public function testSuccess(): void
    {
        $this->mailer()->clear();

        $new_mail = 'new-user@app.test ';
        //                            ^

        $response = $this->app()->handle(self::json('POST', '/v1/auth/join', [
            'email' => $new_mail,
            'password' => 'new-password',
        ]));
    # ...
    }
```

Автоматическое вычищение пробелов через middleware готово.


## Переносим обработку DomainException в Middleware

Во всех наших контроллерах нужно будет оборачивать вызов хэндлера вызываемой
команды в блок try-catch для того чтобы при возникновении доменной ошибки
выдавать её в json-ответе

```php
<?
    try {
        $this->handler->handle($command);
        return new EmptyResponse(201);
    } catch (DomainException $e) {
        return new JsonResponse(['message' => $e->getMessage()], 409);
    }
```
Сделаем так чтобы не нужно было постоянно копипастить этот try-catch блок.
ну и еще однин недостаток такого бойлер-кода с try-catch прямо в контроллере
в том, что если надо будет изменить структуру ответа, хотябы просто изменить
имя поля ответа придётся менять это во всех контроллерах.


Реализацию начинаем со "спеки" то есть с тестов.
```php
<?php

declare(strict_types=1);

namespace App\Http\Test\Unit\Middleware;

use App\Http\Middleware\DomainExceptionHandler;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\ServerRequestFactory;

/**
 * @covers DomainExceptionHandler
 */
class DomainExceptionHandlerTest extends TestCase
{
    // при нормальном поведении когда DomainException не кидаются
    // он просто должен вернуть ответ контроллера стоящего за ним
    public function testNormal(): void
    {
        $middleware = new DomainExceptionHandler();

        //эмулируем ответ контроллера который вернутся из handle
        $source = (new ResponseFactory())->createResponse();

        // создаём простую заглушку которая просто возвращает заданное значение
        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn($source);

        // эмулируемый запрос который будет ходить по цепочке миддлвейров
        $request = (new ServerRequestFactory())
            ->createServerRequest('POST', 'http://test');

        // эмулируем передачу управления в наш миддлвейр
        $response = $middleware->process($request, $handler);

        // вот здесь и убеждаемся что ответ контроллера доходит как есть.
        self::assertEquals($source, $response);
    }

    // при исключениях метод должен вернуть 409 код и json с сообщением об
    // причине возникновения ошибки
    public function testException(): void
    {
        $middleware = new DomainExceptionHandler();

        $handler = $this->createStub(RequestHandlerInterface::class);
        // эмулируем чтобы вызываемый метод кидал исключение DomainException
        $handler->method('handle')
            ->willThrowException(new DomainException('Some error.'));

        $request = (new ServerRequestFactory())
            ->createServerRequest('POST', 'http://test');

        // запуск где внутри handler кидает исключение
        $response = $middleware->process($request, $handler);

        // ответ уже совсем другой - код 409
        self::assertEquals(409, $response->getStatusCode());
        // есть тело ответа и это json
        self::assertJson($body = (string)$response->getBody());

        /** @var array $data */
        $data = json_decode($body, true, 512, JSON_THROW_ON_ERROR);

        // декодированный json должен содержать сообщение об ошибке
        self::assertEquals([ 'message' => 'Some error.' ], $data);
    }
}
```

Реализуем сам миддлвейр
```php
<?
class DomainExceptionHandler implements MiddlewareInterface
{
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        try {
            // если всё нормельно исключений нет - просто вернуть ответ
            return $handler->handle($request);
        } catch (DomainException $exception) {
            // если возникло исключение - вернуть json с кодом 409 и сообщением
            return new JsonResponse([ // об возникшей ошибке
                'message' => $exception->getMessage(),
            ], 409);
        }
    }
}
```

регистрация миддлвейра так чтобы он был как можно ближе к контроллеру т.е.
как бы оборачивал его try-catch
```php
<?
return static function (App $app): void {
    $app->add(Middleware\DomainExceptionHandler::class);
    $app->add(Middleware\ClearEmptyInput::class);
    $app->addBodyParsingMiddleware();
    $app->add(ErrorMiddleware::class);
};
```

В итоге класс контроллера упрощается до
```php
<?
class RequestAction implements RequestHandlerInterface
{
    // ...

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @psalm-var array{email:?string, password:?string} $data */
        $data = $request->getParsedBody();

        $command = new Command();
        $command->email = $data['email'] ?? '';
        $command->password = $data['password'] ?? '';

        $this->handler->handle($command);
        return new EmptyResponse(201);
    }
}
```

Проверяем прохождение тестов - проходят

```sh
docker compose run --rm api-php-cli composer test
> phpunit --colors=always
PHPUnit 10.4.1

................................................................. 65 / 83 ( 78%)
..................                                                83 / 83 (100%)

Time: 00:00.555, Memory: 26.00 MB

OK (83 tests, 153 assertions)
```


## Добавление DomainException в лог

Может быть полезно при активной разработке своего API чтобы узнать какие ошибки
возникают у клиентов.

```php
<?
class DomainExceptionHandler implements MiddlewareInterface
{
    private LoggerInterface $logger;

    // как обычно через DI-контейнер сюда будет передаваться логгер
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        try {
            return $handler->handle($request);
        } catch (DomainException $exception) {
            // отправляем сообщение в лог.  (вообще можно и под уровнем info)
            $this->logger->warning($exception->getMessage(), [
                'exception' => $exception,
                'url' => (string)$request->getUri(),
            ]);

            return new JsonResponse([
                'message' => $exception->getMessage(),
            ], 409);
        }
    }
}
```

Логирование доменных исключений позволит отлавливать всякие аномалии.
Например можно бдует зайти в логи на проде и увидить что у некого юзера
слишком часто кидается исключение типа нехватает денег на счёте.
а это может происходить из-за того что в мобильном приложении забыли скрыть
конопку купить или что-то подобное, а юзер всё упорно тыкает её и не понимает
почему не работает. То есть логирование доменных исключений порой может помочь
через анализирование логов найти упущение в коде. например о том что забыли
спрятать кнопку при нулевом балансе.

Обновляем тесты
```php
<?
class DomainExceptionHandlerTest extends TestCase
{
    public function testNormal(): void
    {
        // заглушка для логгера
        $logger = $this->createMock(LoggerInterface::class);
        // never - метод не должен быть вызван
        $logger->expects($this->never())->method('warning');

        $middleware = new DomainExceptionHandler($logger);
        # ...
    }

    public function testException(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        // при исключении метод должен вызваться ровно один раз
        $logger->expects($this->one())->method('warning');

        $middleware = new DomainExceptionHandler($logger);
        # ...
    }
```

