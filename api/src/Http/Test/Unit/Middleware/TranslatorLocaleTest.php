<?php

declare(strict_types=1);

namespace App\Http\Test\Unit\Middleware;

use App\Http\Middleware\TranslatorLocale;
use App\Http\Test\Unit\WebUTestCase;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Translation\Translator;

/**
 * @covers TranslatorLocale
 */
class TranslatorLocaleTest extends WebUTestCase
{
    public function testDefault(): void
    {
        $translator = $this->createMock(Translator::class);
        $translator->expects(self::never())->method('setLocale');

        $middleware = new TranslatorLocale($translator);

        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn($source = self::createResponse());

        $response = $middleware->process(self::createRequest(), $handler);

        self::assertEquals($source, $response);
    }

    public function testAccepted(): void
    {
        $translator = $this->createMock(Translator::class);
        $translator->expects(self::once())->method('setLocale')->with(
            self::equalTo('ru')
        );

        $middleware = new TranslatorLocale($translator);

        $handler = $this->createStub(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn(self::createResponse());

        $request = self::createRequest()->withHeader('Accept-Language', 'ru');

        $middleware->process($request, $handler);
    }
}
