init: docker-down-clear \
  api-clear frontend-clear cucumber-clear \
  docker-pull docker-build docker-up \
	api-init frontend-init cucumber-init
up: docker-up
down: docker-down
restart: down up
check: lint analyze validate-schema test
lint: api-lint frontend-lint cucumber-lint
analyze: api-analyze
validate-schema: api-validate-schema
test: api-test api-fixtures frontend-test
test-unit: api-test-unit
test-unit-coverage: api-test-unit-coverage
test-functional: api-test-functional api-fixtures
test-functional-coverage: api-test-functional-coverage api-fixtures
test-smoke: api-fixtures cucumber-clear cucumber-smoke
test-e2e: api-fixtures cucumber-clear cucumber-e2e

docker-up:
	docker compose up -d

docker-down:
	docker compose down --remove-orphans

docker-down-clear:
	docker compose down -v --remove-orphans

docker-pull:
	docker compose pull

docker-build:
	docker compose build --pull

api-clear:
	docker run --rm -v ${PWD}/api:/app -w /app alpine sh -c 'rm -rf var/cache/* var/log/*'

api-init: api-permissions api-composer-install api-wait-db api-migrations api-fixtures

api-permissions:
	docker run --rm -v ${PWD}/api:/app -w /app alpine chmod 777 var/cache var/log

api-composer-install:
	docker compose run --rm api-php-cli composer install

api-wait-db:
	docker compose run --rm api-php-cli wait4open -t 30 api-postgres:5432

api-migrations:
	docker compose run --rm api-php-cli composer app migrations:migrate --no-interaction

api-fixtures:
	docker compose run --rm api-php-cli composer app fixtures:load

api-validate-schema:
	docker compose run --rm api-php-cli composer app orm:validate-schema

api-lint:
	docker compose run --rm api-php-cli composer lint
	docker compose run --rm api-php-cli composer cs-check

api-analyze:
	docker compose run --rm api-php-cli composer psalm

api-test:
	docker compose run --rm api-php-cli composer test

api-test-unit:
	docker compose run --rm api-php-cli composer test -- --testsuite=unit

api-test-unit-coverage:
	docker compose run -e XDEBUG_MODE=coverage --rm api-php-cli composer test-coverage -- --testsuite=unit

api-test-functional:
	docker compose run --rm api-php-cli composer test -- --testsuite=functional

api-test-functional-coverage:
	docker compose run -e XDEBUG_MODE=coverage --rm api-php-cli composer test-coverage -- --testsuite=functional


frontend-clear:
	docker run --rm -v ${PWD}/frontend:/app -w /app alpine sh -c 'rm -rf .ready build'

frontend-init: frontend-yarn-install frontend-ready

frontend-yarn-install:
	docker compose run --rm frontend-node-cli yarn install

frontend-ready:
  docker run --rm -v ${PWD}/frontend:/app -w /app alpine touch .ready

frontend-lint:
	docker compose run --rm frontend-node-cli yarn eslint
	docker compose run --rm frontend-node-cli yarn stylelint

frontend-eslint-fix:
	docker compose run --rm frontend-node-cli yarn eslint-fix

frontend-pretty:
	docker compose run --rm frontend-node-cli yarn prettier

frontend-test:
	docker compose run --rm frontend-node-cli yarn test --watchAll=false

frontend-test-watch:
	docker compose run --rm frontend-node-cli yarn test


cucumber-clear:
	docker run --rm -v ${PWD}/cucumber:/app -w /app alpine sh -c 'rm -rf var*'

cucumber-init: cucumber-yarn-install

cucumber-yarn-install:
	docker compose run --rm cucumber-node-cli yarn install

cucumber-e2e:
	docker compose run --rm cucumber-node-cli yarn e2e

cucumber-lint:
	docker compose run --rm cucumber-node-cli yarn lint

cucumber-lint-fix:
	docker compose run --rm cucumber-node-cli yarn lint-fix

build: build-gateway build-frontend build-api

build-gateway:
	docker --log-level=debug build --pull --file=gateway/docker/production/nginx/Dockerfile --tag=${REGISTRY}/auction-gateway:${IMAGE_TAG} gateway/docker

build-frontend:
	docker --log-level=debug build --pull --file=frontend/docker/production/nginx/Dockerfile --tag=${REGISTRY}/auction-frontend:${IMAGE_TAG} frontend

build-api:
	docker --log-level=debug build --pull --file=api/docker/production/nginx/Dockerfile --tag=${REGISTRY}/auction-api:${IMAGE_TAG} api
	docker --log-level=debug build --pull --file=api/docker/production/php-fpm/Dockerfile --tag=${REGISTRY}/auction-api-php-fpm:${IMAGE_TAG} api
	docker --log-level=debug build --pull --file=api/docker/production/php-cli/Dockerfile --tag=${REGISTRY}/auction-api-php-cli:${IMAGE_TAG} api

try-build:
	REGISTRY=localhost IMAGE_TAG=0 make build


push: push-gateway push-frontend push-api

push-gateway:
	docker push ${REGISTRY}/auction-gateway:${IMAGE_TAG}

push-frontend:
	docker push ${REGISTRY}/auction-frontend:${IMAGE_TAG}

push-api:
	docker push ${REGISTRY}/auction-api:${IMAGE_TAG}
	docker push ${REGISTRY}/auction-api-php-fpm:${IMAGE_TAG}
	docker push ${REGISTRY}/auction-api-php-cli:${IMAGE_TAG}


# set EnvVar for specific target
deploy: export OPTS=${HOST} -p ${PORT} ${OPT}
deploy: export N=${BUILD_NUMBER}

# file docker-compose-production.yml copied into server as docker-compose.yml
# so you don't need to specify -f docker-compose-production.yml in commands

# how to use make deploy see here: docs/05-deploy/14-do-deploy.md
deploy:
	ssh ${OPTS} 'rm -rf site_${N} && mkdir site_${N}'

	scp -P ${PORT} ${OPT} docker-compose-production.yml ${HOST}:site_${N}/docker-compose.yml

	ssh ${OPTS} 'cd site_${N} && echo "COMPOSE_PROJECT_NAME=auction" >> .env'
	ssh ${OPTS} 'cd site_${N} && echo "REGISTRY=${REGISTRY}" >> .env'
	ssh ${OPTS} 'cd site_${N} && echo "IMAGE_TAG=${IMAGE_TAG}" >> .env'

	ssh ${OPTS} 'cd site_${N} && docker compose pull'
	ssh ${OPTS} 'cd site_${N} && docker compose up --build -d api-postgres api-php-cli'
	ssh ${OPTS} 'cd site_${N} && docker compose run api-php-cli wait4open -t 60 api-postgres:5432'
	ssh ${OPTS} 'cd site_${N} && docker compose run api-php-cli php bin/app.php migrations:migrate --no-interaction'
	ssh ${OPTS} 'cd site_${N} && docker compose up --build --remove-orphans -d'

	ssh ${OPTS} 'rm -f site'
	ssh ${OPTS} 'ln -sr site_${BUILD_NUMBER} site'


rollback:
	ssh ${OPTS} 'cd site_${N} && docker compose pull'
	ssh ${OPTS} 'cd site_${N} && docker compose up --build --remove-orphans -d'
	ssh ${OPTS} 'rm -f site'
	ssh ${OPTS} 'ln -sr site_${N} site'


# Trigger debug sessions Before call this apply the StartListening mode in the IDE

# Trigger debug requests for php-fpm (API)
debug-api:
	curl --cookie "XDEBUG_TRIGGER=1;" localhost:8081

# Trigger debug requests for php-cli
# Usage example: make debug-api-cli cmd=hello [arg1=name]
debug-api-cli:
	docker compose run --rm -e XDEBUG_TRIGGER=1 api-php-cli composer app $(cmd) $(arg1)

# Note: the file xdebug.ini must have a setting: `xdebug.log=/tmp/xdebug.log`
debug-xdebug-logs:
	docker compose exec api-php-fpm tail -n 16 /tmp/xdebug.log

