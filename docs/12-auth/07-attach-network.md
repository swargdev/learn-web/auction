## UseCase AttachNetwork

Второй метод(UseCase) - Добавление новой соцсети к User-у
Например когда юзер хочет через личный кабинет привязать к своему аккаунту
новую соцсеть

- Перед добавлением новой соцсети проверять на уникальность и отсутствие данной
соцсети у юзера

Создаём этот UseCase в отдельном нейспейсе
`./api/src/Auth/Command/AttachNetwork/`
Handler.php

В команде будут приходить данные кому и какую сеть нужно добавить:
`./api/src/Auth/Command/AttachNetwork/Command.php`
```php
<?php

declare(strict_types=1);

namespace App\Auth\Command\AttachNetwork;

class Command
{
    public string $id;       // идишник юзера
    public string $network;  // имя соцсети
    public string $identity; // идишник этого юзера в этой соцсети
}
```

Обработчик этого юз-кейса
`./api/src/Auth/Command/AttachNetwork/Handler.php`
в своём единственном и основном методе Handle



```php
<?
namespace App\Auth\Command\AttachNetwork;

//use ...

class Handler
{
    // ...
    public function handle(Command $command): void
    {
        // данные из пришедшей команды оборачивает в обьект значение NetworkIdentity
        $identity = new NetworkIdentity($command->network, $command->identity);

        // проверяем есть ли в системе уже юзер с такой же соцсетью
        if ($this->users->hasByNetwork($identity)) {
            //если есть кидаем исключение чтобы не позволить дублировать данные
            throw new \DomainException('User with this network already exists.');
        }

        // по идишнику юзера достаём его обьект из бд через репозиторий
        $user = $this->user->get(new Id($command->id));
        // метод get - это уже новый метод, нужный нам для реализации этой логики
        // его прописываем в App\Auth\Entity\User\UserRepository

        // ... вводим в UserRepository метод get:
}
```

```php
<?
namespace App\Auth\Entity\User;

use DomainException;

interface UserRepository
{
    public function hasByEmail(Email $email): bool;
    public function hasByNetwork(NetworkIdentity $identity): ?User;
    public function findByConfirmToken(string $token): ?User;

    /**                         <-- это док блок для внесения ясности по методу
     * @param Id $id
     * @return User
     * @throws DomainException  <-- явно указываем какое может кинуть иключение
     *                              данный метод.
     */
    public function get(Id $id): User; // << наш новый метод для получения юзера
                                       // из БД по его идишнику

    public function add(User $user): void;
```

php не позволяет указывать исключения в сигнатуре как это можно делать например
в Java. поэтому единственный путь указать возможные кидаемые исключения - это
через док-блок(комментарий над методом)

Далее в методе handle класса AttachNetwork\Handler:
```php
<?php

        $user = $this->user->get(new Id($command->id));
        // у полученного из БД юзера вызывается метод добавить соцсеть:
        // здесь $identity - это и есть обьект-значение хранящее соцсеть
        $user->attachNetwork($identity);
        // сохранение изменений в БД
        $this->flusher->flush();
```

Сначала пишем тесты для проверки выполнения операции attachNetwork
./api/src/Auth/Test/Unit/Entity/User/AttachNetworkTest.php


```php
<?
class AttachNetworkTest extends TestCase
{

    public function testSuccess(): void
    {
        // создание нового активного юзера через хэлпер
        $user = (new UserBuilder())
            ->active()  // уже зареганный и подтверждённый email
            ->build();

        $network = new NetworkIdentity('vk', '000001');
        // производим операцию добавления соцсети
        $user->attachNetwork($network);

        // проверяем что соцсеть реально добавилась к этому юзеру
        self::assertCount(1, $networks = $user->getNetworks());
        self::assertEquals($network, $networks[0] ?? null);
    }

    // второй тест  проверяем защиту от попытки дублировать уже добавленную сеть
    public function testAlready(): void
    {
        $user = (new UserBuilder())
            ->active()
            ->build();

        $network = new NetworkIdentity('vk', '000001');

        $user->attachNetwork($network);
        // здесь говорим что должно кинутся исключение
        $this->expectExceptionMessage('Network is already attached.');
        // этот метод должен кинуть исключение - говоря нельзя добавить соцсеть
        $user->attachNetwork($network);
    }
```

Имея этот тест можно приступить к реализации самого метода attachNetwork в User:
```php
<?
class User {
    public function attachNetwork(NetworkIdentity $identity): void
    {
        // делаем проверку на уникальность, проверяя есть ли уже такая же соцсеть
        /** @var NetworkIdentity $existing */
        foreach ($this->networks as $existing) {
            if ($existing->isEqualTo($identity)) {
                // если такая соцсеть уже есть - кидаем исключение
                throw new DomainException('Network is already attached.');
            }
        }
        // добавляем новую соцсеть в список всех соцсетей этого юзера
        $this->networks->append($identity);
    }
```
в php вот эту строку

            if ($existing->isEqualTo($identity))

можно переписать и вот так вот

            if ($existing == $identity)

Такое нестрогое сравнение будет проверять на равентсво по всем существующим полям
но здесь мы вводим свой метод isEqualTo для надёжности и возможности переопределить
поведение сравнения(например если надо будет какие-то поля пропускать)

строгое сравнение === здесь использовать нельзя - т.к. оно проверяет не поля,
двух обьектов а то что эти две переменные содержат ссылки на один и тот же обьект
в памяти


