01-intro
- Что сделано и что дальше
- Зачем Логирование
- Эмитируем ситуацию поломанного кода после обновы библиотеки-зависимости:

02-docker-logs
- Разбираемся откуда появляются docker-овские логги
- Смотрим на Dockerfile образа nginx
- Смотрим на Dockerfile образа php-fpm:
- Как еще можно смотреть логи собранные Докером
- Исправляем автоматическое удаление логов в Prod-е для api-php-cli

03-task-n-monolog
- Задача - сделать удобное логирование для test и dev окружений
- Psr\Container\ContainerInterface
- Библиотека логгирования Monolog
- Установка и конфигурирование логгера Monolog
- Тонкость при создании каталогов var/log
- Перенастравиваем кэши линтера и кодеснифера на с каталога var/ На var/cache/
- PROD-env: Установка правильных прав на каталог var/cache

04-error-middleware
- Выход на то как можно настроить логгер monolog для проекта
- Варианты как прокинуть свой логгер в Slim\ErrorHandler
- Рефакторинг. Выносим ErrorMiddleware из addErrorMiddleware в DI-контейнер
- Создаём фабрику для ErrorMiddleware

05-setup-logger
- Подключение логера к проекту, переопеределение Slim\ErrorHandler
- Создаём свой LogErrorHandler вместо стандартного Slim\Handlers\ErrorHandler
- Проверяем запись лог-сообщений в /var/log/cli/test.log
- Логирование готово. Что это даёт
- Запись дополнительных параметров(context) в лог

06-sentry
- Внешний Агрегатор логов
- Способы как можно сделать оповещение об ошибках
- Продвинутая система отправки логов - сервис "Агрегатор логов"
- Подключение Sentry.io к своему проекту
- Создаём класс-декоратор SentryDecorator
- Почему наследовались от ErrorHandler и создали LogErrorHandler



## Что сделано и что дальше

- База данных подключена,
- отправка писем настроена.
- подключен эмулятор почтового сервера (для локальной разработки)

Чтобы код API стал полноценным осталось написать контроллеры и функ тесты для них.

Но перед написанием самих контроллеров нужно настроить полноценное логгирование.

## Зачем Логирование

При написании тестов возможны случа когда без логирования узнать место где
возникла ошибка будет крайне затруднительно.
Так как полный стектрейс с ошибкой просто не будет выводиться.

На данный момент написано только два функц-ых теста
```
./api/tests/Functional/
├── HomeTest.php             тест для главной страницы API
├── NotFoundTest.php         тест на правильность отдачи 404 ошибки
└── WebTestCase.php          базовый класс для наследования
```

Так наши тесты заходят на разные страницы, наше веб-приложение генерит ошибки
но посмотреть сами эти ошибки (полные их стектрейсы в отчётах о тестировании)
пока никак нельзя.

Ситуация: обновили библиотку, в ней что-то поменялось и сломало код.
Чтобы было быстрее и легче отловить где именно и что именно сломалось
так же смогут помочь логи.

## Эмитируем ситуацию поломанного кода после обновы библиотеки-зависимости:

Для примера посмотрим на то как будет отображаться ошибка при отработке нашего
экшена HomeAction. Для этого просто добавляем кидание исключени в handle

./api/src/Http/Action/HomeAction.php
```php
<?
class HomeAction implements RequestHandlerInterface {

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
/* 20 */throw new \RuntimeException('ErrorExc'); // добавляем исключение
        return new JsonResponse(new stdClass());
    }
```

Теперь запускаем тесты чтобы посмотреть как будут отображаться ошибки
```sh
make test
docker compose run --rm api-php-cli composer test
> phpunit --colors=always
PHPUnit 10.4.1 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.26
Configuration: /app/phpunit.xml

................................................................. 65 / 72 ( 90%)
....F..                                                           72 / 72 (100%)

Time: 00:00.138, Memory: 16.00 MB

There was 1 failure:

1) Test\Functional\HomeTest::testSuccess
Failed asserting that 500 matches expected 200.             # <<<<<<<<<<<<<<

/app/tests/Functional/HomeTest.php:13

FAILURES!
Tests: 72, Assertions: 127, Failures: 1.
Script phpunit --colors=always handling the test event returned with error code 1
```

```php
<?
class HomeTest extends WebTestCase {
    public function testSuccess(): void
    {
        $response = $this->app()->handle(self::json('GET', '/'));

/* 13 */self::assertEquals(200, $response->getStatusCode()); // выдало 500 а не 200
        self::assertEquals('application/json', $response->getHeaderLine('Content-Type'));
        self::assertEquals('{}', (string)$response->getBody());
    }
```

Из отчётов теста видно только что assert на 13 строке провалился
при этом никаких дополнительных деталей (полного стектрейса) нет.

Это потому что этом саму ошибку перехватывает настроенный нами Middleware:
./api/config/middleware.php
```php
<?
return static function (App $app, ContainerInterface $container): void {
    /** @psalm-var array{debug:bool, env:string} */
    $config = $container->get('config');
    $app->addErrorMiddleware($config['debug'], $config['env'] != 'test', true);
};
```
Именно этот мидлвейр вместо стектрейса генерирует человеко-читаемое сообщение
"страница 500 - внутрянняя ошибка сервера":

      Failed asserting that 500 matches expected 200.

и выдаёт сгенеренную html страницу на экран браузера в dev-окружении.

В тестах же дальше просто проваливается assert,
в котором ждали не 500й, а 200й код ответа.

Таким образом в самих тестах у нас нет возможности посмотреть на полный
стектрейс, чтобы быстро разобраться где именно и что пошло не так и сломалось.

Так же в конфиге сделано чтобы middleware не запускался при запуске тестов:
```php
<?
    $app->addErrorMiddleware($config['debug'], $config['env'] != 'test', true);
   //                                          ^^^^^^^^^^^^^^^^^^^^^^^^
```

Проверим что бы было, если бы логирование было включено и для тестов тоже
временно заменим на true:

```php
<?
    $app->addErrorMiddleware($config['debug'], true, true);
    //                                         ^^^^^
```

смотрим что выдаст теперь
```sh
make test
docker compose run --rm api-php-cli composer test
```
```
> phpunit --colors=always
PHPUnit 10.4.1 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.26
Configuration: /app/phpunit.xml

................................................................. 65 / 72 ( 90%)
....Slim Application Error
                       ----- 1е исключение:
Type: RuntimeException                        << нами кинутое исключение
Code: 0
Message: ErrorExc                             << то самое сообщение об ошибке
File: /app/src/Http/Action/HomeAction.php     << файл в котором оно сработало
Line: 20                                      << Номер строки в этом файле
Trace: #0 /app/vendor/slim/slim/Slim/Handlers/Strategies/RequestHandler.php(46): ..
... (длинный стектрейс)

#9 /app/vendor/slim/slim/Slim/App.php(199): ..
#10 /app/tests/Functional/HomeTest.php(11): Slim\App->handle(Slim\Psr7\Request)
                         ^^^^^^^^^^^^^ ^^ вот место в тесте где шел первый вызов
...
#20 /app/vendor/phpunit/phpunit/phpunit(99): PHPUnit\TextUI\Application->run(Array)
#21 /app/vendor/bin/phpunit(122): include('/app/vendor/php...')
#22 {main}

                       ----- 2е исключение:
F405 Method Not Allowed
Type: Slim\Exception\HttpMethodNotAllowedException
Code: 405
Message: Method not allowed. Must be one of: GET
File: /app/vendor/slim/slim/Slim/Middleware/RoutingMiddleware.php
Line: 79
Trace: #0 /app/vendor/slim/slim/Slim/Routing/RouteRunner.php(56): ..
...
#4 /app/vendor/slim/slim/Slim/App.php(199): ...
#5 /app/tests/Functional/HomeTest.php(20): Slim\App->handle(Slim\Psr7\Request)
...
#17 {main}

                       ----- 3е исключение:
.404 Not Found
Type: Slim\Exception\HttpNotFoundException
Code: 404
Message: Not found.
File: /app/vendor/slim/slim/Slim/Middleware/RoutingMiddleware.php
Line: 76
Trace: #0 /app/vendor/slim/slim/Slim/Routing/RouteRunner.php(56): ..
...
#4 /app/vendor/slim/slim/Slim/App.php(199):
#5 /app/tests/Functional/NotFoundTest.php(11): <<<
....
#16 /app/vendor/bin/phpunit(122): include('/app/vendor/php...')
#17 {main}
.                                                           72 / 72 (100%)

Time: 00:00.129, Memory: 16.00 MB

There was 1 failure:

1) Test\Functional\HomeTest::testSuccess
Failed asserting that 500 matches expected 200.

/app/tests/Functional/HomeTest.php:13
```

2е исключение
возникло при обращении на главную страницу API через метод POST
тогда как разрешено только через GET:
```
Type: Slim\Exception\HttpMethodNotAllowedException
Code: 405
Message: Method not allowed. Must be one of: GET
File: /app/vendor/slim/slim/Slim/Middleware/RoutingMiddleware.php
Line: 79
...
#5 /app/tests/Functional/HomeTest.php(20): Slim\App->handle(Object(Slim\Psr7\Request))
...                      ^^^^^^^^^^^^^^^^
```

Вот это место в коде теста: оно в стектрейсе под #5 номером:
```php
<?
class HomeTest extends WebTestCase {
    public function testMethod(): void
    {
/*20*/  $response = $this->app()->handle(self::json('POST', '/')); // <<<<

        self::assertEquals(405, $response->getStatusCode());
    }
```
При этой ошибки сам маршрутизатор Slim-а сгенерил исключение с "типом":
`Slim\Exception\HttpMethodNotAllowedException`


3е исключение прошло при тесте
./api/tests/Functional/NotFoundTest.php

```php
<?
class NotFoundTest extends WebTestCase
{
    public function testNotFound(): void
    {
/*11*/  $response = $this->app()->handle(self::json('GET', '/not-found'));

        self::assertEquals(404, $response->getStatusCode());
    }
}
```

в выводе тестов это 3е исключение:
```
.404 Not Found
Type: Slim\Exception\HttpNotFoundException
Code: 404
Message: Not found.
File: /app/vendor/slim/slim/Slim/Middleware/RoutingMiddleware.php
...
#5 /app/tests/Functional/NotFoundTest.php(11): <<< наш файл 11 строка
                         ^^^^^^^^^^^^^^^^ ^^
```


Теперь оставляем вывод всех ошибок в лог тестов,

    $app->addErrorMiddleware($config['debug'], true, true);

но убираем строку из HomeAction где кидали исключение для эмитации ошибки:

    /* 20 */ throw new \RuntimeException('ErrorExc');


Запустив тесты теперь видим что из 3х исключений по прежнему выводятся 2:

- "405 Method Not Allowed"  Slim\Exception\HttpMethodNotAllowedException
- "404 Not Found"           Slim\Exception\HttpNotFoundException

Такой вариант нас не устраивает т.к. по сути у нас тест успешный хотя мы и
проверяем именно наличие ошибки, И при этом вывод теста засоряется стектрейсами.
Т.е. нужен другой способ доступа к стектрейсам, причем именно когда это нужно.

Возвращаем конфигурацию middleware как оно было:
./api/config/middleware.php:

    $app->addErrorMiddleware($config['debug'], $config['env'] != 'test', true);



