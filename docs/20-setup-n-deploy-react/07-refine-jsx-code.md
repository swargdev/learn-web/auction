## Создаём структуру будущего React-App

Входной скрипт нашего React-App это вот этот вот файл:

./frontend/src/index.jsx
```js
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />                   {/* рендеринт нашего UI-компонента */}
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
```

Здесь идёт рендеринг нашего UI-компонента `App`, который описан в файле
`./frontend/src/App.jsx` и импортируется извне через `import App from './App';`
(этот файл App.jsx подключает еще свои стили App.css)

Сам этот сгенерированный [App компонент](/frontend/src/App.jsx) выглядит так

./frontend/src/App.jsx
```js
import React from 'react';
import './App.css';                       // импорт стилей

function App() {
  return (
    <div className="app">
      <div className="welcome">
        <h1>Auction</h1>
        <p>We will be here soon</p>
      </div>
    </div>
  );
}

export default App;
```

Видим что этот файл, описывающий UI-компонент React-а `App` по сути содержит в
себе весь код нашего приложения.

Вообще в компоненте App по итогу должно быть всё наше приложение в том числе
переключение маршрутов в общем слишком много всего, чтобы держать это всё в
одном файле. поэтому сразу создадим удобную для разработки структуру каталогов
для хранения UI-компонентов.


## Создаём свой первыфй UI-компонент Welcome

для начала создаём каталог в котором будем размещать наши UI-компоненты
`./frontend/src/components/`


./frontend/src/components/Welcome.jsx
```js
import React from 'react';
import './Welcome.css';

function Welcome() {
  return (
    <div className="welcome">
      <h1>Auction</h1>
      <p>We will be here soon</p>
    </div>
  );
}

export default Welcome;
```

Переносим css код касающийся Welcome в Welcome.css

./frontend/src/components/Welcome.css
```css
.app {
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
}
```


./frontend/src/App.jsx
```js
import React from 'react';
import './App.css';
// импортируем наш UI-компонент из каталога components
import Welcome from './components/Welcome'; // импортируем наш UI-компонент
//     ^^^ на деле это js-функция из ./frontend/components/src/Welcome.jsx

function App() {
  // помни что это JSX-семантика, а не обычные html-теги
  return (
    <div className="app">
      <Welcome />          {/* подключаем наш UI-компонент в JSX-нотации */}
    </div>
  );
}

export default App;
```

после разделения кода на два компонента обновляем страницу фронтенда в барузере
и смотрим её исходный код прямо в нём через Ctrl-U
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta name="description" content="Auction" />
    <link rel="apple-touch-icon" href="/logo192.png" />
    <title>Auction</title>
  <script defer src="/static/js/bundle.js"></script></head>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"></div>
  </body>
</html>
```

Видим опять "иллюзию пустоты".
в исходнике видно только body и пустой div id="root" и казалось бы нет того
что реально на ней сайчас отображается в виде надписи

`Auction We will be here soon`
Это потому что так работает React приложение - оно монтирует виртуальное DOM-
дерево прямо внутрь элемента div с id=root и после такого монтирования браузер
отображает нам картинку SPA(SinglePageApplication) приложения.
Другими словами React-приложение генерирует страницу прямо в браузере вместо
привычной классической генерации html-страниц на стороне сервера.
Вообще дальше мы еще расмотрим такую технологию как SSR(ServerSideRender)
которая позволяет рендерить React-приложение(вёрстку страниц) и на строне
сервера для дальнейшей её раздачи клиентам в браузер с последующей интерактивной
её модификацией.

А вот если через Inspect просмотреть на выводимый элемент то увидим нашу
вёрстку:

```html
<div class="app">
  <div class="welcome">
    <h1>Auction</h1>
    <p>We will be here soon</p>
  </div>
</div>
```


## React css-стили привязанные к конкретному UI-компоненту

При рендеринге компонента Welcome с таким кодом:

./frontend/src/components/Welcome.jsx
```js
import React from 'react';
import './Welcome.css';

function Welcome() {
  return (
    <div className="welcome">
      <h1>Auction</h1>
      <p>We will be here soon</p>
    </div>
  );
}
```

в результате своей работы React для данного компонента на странице создаст
такую вёрстку:
```html
<div class="welcome">
  <h1>Auction</h1>
  <p>We will be here soon</p>
</div>
```

То есть данный компонент создаст `div` с css-классом `welcome` и вложенными
эл-тами которые мы у себя описали.
Внутри себя UI-компонент Welcome напрямую импортирует `import './Welcome.css';`
стили из одноимённого css-файла, поэтому в корень нашей страницы будут
смонтированы всё что описано внутри Welcome.css.
При этом если мы создадим несколько компонентов `Welcome` т.е. несколько
компонентов с одним названием, и которые так же используют css-стили то нам надо
будет следить за тем, чтобы имена css-html-классов внутри css-файлов были
уникальными и не конфликтовали с css-файлами из других компонентов.
Т.е. возникает проблема конфликта имён css-html-классов для css-стилей.

если посмотреть на страницу через Inspect в блоке `<head>` можно увидить наши
подключенные стили в блоках `<style><style>`
на данный момент их там будет три - для index.css App.css и Welcome.css

```html
<style>
.welcone {
  display: block;
  /* vertical-align: middle; */
  padding: 30px;
  background: #fff;
  box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.05);
  border-radius: 6px;
}

.welcome h1 { }  /* сократил для наглядности код здесь тот же*/
.welcome p { }
</style>
```

т.е. фактически все подключаемые из UI-компонентов css файлы попадают на страницу
в виде одной простыни, хотя и обёрнуты в разные `<stylе>`-блоки.
То есть ни о каком пространстве имён речь даже не идёт, а значит неизбежен
конфликт имён для css-html-классов с одинаковыми названиями.

Для решения этой проблемы
React позволяет писать стили привязывая их к конкретному компонент

```
frontend/src/
├── App.css
├── App.jsx
├── App.test.jsx
├── components
│   ├── Welcome.css
│   └── Welcome.jsx
├── index.css
├── index.jsx
├── reportWebVitals.js
└── setupTests.js
```

Для этого переименовываем Welcome.css в Welcome.module.css

```js
import React from 'react';
// подключаем уже как некий модуль со стилями
import styles from './Welcome.module.css';
// теперь можно будет обращаться к styles как к обьекту с полями
// доставая из него нужные имена классов: style.welcome

function Welcome() {
  //               VVVVVVVVVVVVVVV
  return (
    <div className={style.welcome}">
      <h1>Auction</h1>
      <p>We will be here soon</p>
    </div>
  );
}

export default Welcome;
```

После таких измнений смотрим через Inspect
```html
<div class="Welcome_welcome__tQMjo">
  <h1>Auction</h1>
  <p>We will be here soon</p>
</div>
```

Т.е. видим что название css-класса изменилось на Welcome_welcome__tQMjo

Причем и в самих подключённых стилях имена классов теперь тоже под такими
же именами
```html
<head>
  ...
<style>
.Welcome_welcone__\+wyVl { }

.Welcome_welcome__tQMjo h1 { }

.Welcome_welcome__tQMjo p { }

</style>
```

Т.е. теперь мы знаем как писать свои UI-компоненты и css-стили для них так,
чтобы не переживать за уникальноть имён классов - всё это забота React.


Теперь настало время подключить дополнительные вещи к ReactJS
на подобии того как мы это сделали для php
- линтеры и проверка код-стайла
- статические анализаторы кода и проверка на ошибки

задача которых вылавливать ошибки и следить за правильностью нашего кода
