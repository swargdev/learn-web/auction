
- Установка ORM (Doctrine)
- EntityManager
- Добавляем Doctrine в наш конфиг di-контейнера
- Делаем Dev версию настройки Doctrine
- CLI комманды Doctrine
- Добавляем cli команды doctrine в наш api/bin/app.php

## Установка ORM (Doctrine)

Doctine - это готовое решение позволяющее связывать свои сущности с базой данных.
Данная библиотека часто используется в проектах использующих Symfony

https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/configuration.html

Установка пакета doctrine/orm через консоль (обновит composer.json
```sh
docker compose run --rm api-php-cli composer require doctrine/orm
```
[log](./docs/13-doctrine/logs/doctrine-orm.log)
```sh
du -hcd 1 api/vendor
580K  api/vendor/slim
40K   api/vendor/fig
372K  api/vendor/phar-io
528K  api/vendor/php-di
852K  api/vendor/overtrue
56K   api/vendor/bin
508K  api/vendor/phpstan
14M   api/vendor/vimeo
216K  api/vendor/brick
132K  api/vendor/fidry
2.1M  api/vendor/nikic
456K  api/vendor/amphp
60K   api/vendor/spatie
156K  api/vendor/laravel
6.6M  api/vendor/phpunit
28K   api/vendor/ralouphie
896K  api/vendor/felixfbecker
1004K api/vendor/sebastian
380K	api/vendor/psr
1.1M	api/vendor/composer
3.5M	api/vendor/symfony
9.4M	api/vendor/squizlabs
92K   api/vendor/netresearch
7.3M	api/vendor/doctrine
624K	api/vendor/phpdocumentor
28K   api/vendor/dnoegel
76K   api/vendor/theseer
200K	api/vendor/myclabs
224K	api/vendor/webmozart
900K	api/vendor/ramsey
52M   api/vendor
52M	  total
```


## Obtaining an EntityManager
Простейший пример того как можно подключить doctrine к нашему проекту

```php
<?php
// bootstrap.php
require_once "vendor/autoload.php";

use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;

$paths = ['/path/to/entity-files'];
$isDevMode = false;

// the connection configuration
$dbParams = [
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => '',
    'dbname'   => 'foo',
];

$config = ORMSetup::createAttributeMetadataConfiguration($paths, $isDevMode);
$connection = DriverManager::getConnection($dbParams, $config);
$entityManager = new EntityManager($connection, $config);
```
$entityManager - это ключевой обьект doctrine-ы,
Вся работа с этой ORM идёт именно через него, через EntityManager (em)
через него будем
- сохранять сущности в бд
- искать сущности в бд

Для того чтобы entityManager мог корректно работать - его нужно настроить.
Т.е прописать все параметры подключения к бд, её имя, имя пользователя и пароль
и так же драйвер(driver) через который нужно работать с бд.
в коде выше это делается через массив $dbParams


Пример кода для работы с доктриной в нашем проекте.


```php
<?
// настойка подключения к бд
// 1й способ:
$connection = [
  'driver' => 'pdo_pgsql',
  'host' => 'api-postgres',
  'port' => 5432,
  'user’ => ‘app',
  'password' => 'secret',
  'dbname' => 'app',
  'charset' => 'utf-8',
];

// 2й через url в котором все параметры подключения идут одной строкой
// $connection2 = [
//   'driver' => 'pdo_pgsql',
//   'url' => 'pgsgl://app:secret@api-postgres:5432/app',
//              БД     user passwd      ^       ^порт ^-имя нашей БД
//                               с какого host-а
//   'charset' => 'utf-8',
// ];

// 3й способ через pdo
// $pdo = new \PDO('pgsgl:...', 'username', 'password');
// $connection3 = [
//   'pdo'=> $pdo, # этот обьект и передавать дальше в em
// ];


// указываем параметры для работы с нашими сущностями

// указываем место хранения наших сущностей, которые будут маппится через
// аннотации.
$path = [
  'src/Auth/Entity',
];
// При этом будет произведён парсинг аннотаций из всех классов в этих каталогах
// и по найденным аннотациям будет настроен маппинг для всех наших сущностей
// Можно вместо аннотаций прямо в коде использовать маппинг через xml файлы
// тогда нужно бдует использовать не createAnnotationMetadataConfiguration
// а createXMLMetadataConfiguration

// режим работы - флаг определяющий как будет происходить кэширование и создание
// прокси-классов для маппинга сущностей.
$isDevMode = getenv('APP_ENV') !== 'prod';
/* в прод режиме для оптимизации работы доктрин проходит по указанным каталогам
с сущностями парсит, создаёт настройки маппинга и кэширует результат для последу
ющего переиспользования - чтобы не парсить каждый раз. То есть один раз кэширует
сущности а дальше использует кэш.
В дев-режиме такое поведение не подходит т.к. по ходу разработки постоянно будем
менять код и нужно будет чтобы каждое изменение подхватывалось сразу, а не
бралось старое из кэша.
*/

$config = ORMSetup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$connection = DriverManager::getConnection($dbParams, $config);
$entityManager = new EntityManager($connection, $config);
```
исходник нужного нам метода-фабрики createAnnotationMetadataConfiguration

./api/vendor/doctrine/orm/lib/Doctrine/ORM/ORMSetup.php

```php
<?php
namespace Doctrine\ORM;
final class ORMSetup
{
    /**
     * Creates a configuration with an annotation metadata driver.
     *
     * @deprecated Use another mapping driver.
     *
     * @param string[] $paths
     */
    public static function createAnnotationMetadataConfiguration(
        array $paths,
        bool $isDevMode = false,
        ?string $proxyDir = null,
        ?CacheItemPoolInterface $cache = null
    ): Configuration {
        Deprecation::trigger(
            'doctrine/orm',
            'https://github.com/doctrine/orm/issues/10098',
            '%s is deprecated and will be removed in Doctrine ORM 3.0',
            __METHOD__
        );
        $config = self::createConfiguration($isDevMode, $proxyDir, $cache);
        $config->setMetadataDriverImpl(self::createDefaultAnnotationDriver($paths));

        return $config;
    }
```

В php 8.0+ появились атрибуты - это нативная поддержка для добавления неких
мето-данных для полей, классов и методов. Аналог Аннотакий в Java.
До этого использовались php-"аннотации" которые размещали в docs-блоках

https://www.php.net/manual/en/language.attributes.overview.php
Поэтоу `createAnnotationMetadataConfiguration` ныне для php > 8 версии считается
deprecated(устаревшим) и нужно использовать createAttributeMetadataConfiguration


```php
<?php
namespace Doctrine\ORM;
final class ORMSetup
{
    /**
     * Creates a configuration with an attribute metadata driver.
     *
     * @param string[] $paths
     */
    public static function createAttributeMetadataConfiguration(
        array $paths,
        bool $isDevMode = false,
        ?string $proxyDir = null,
        ?CacheItemPoolInterface $cache = null,
        bool $reportFieldsWhereDeclared = false
    ): Configuration {
        $config = self::createConfiguration($isDevMode, $proxyDir, $cache);
        $config->setMetadataDriverImpl(new AttributeDriver($paths, $reportFieldsWhereDeclared));

        return $config;
    }
```

Есть разные способы для того, чтобы настаивать маппинг наших классов сущностей.
и для каждого способа есть свой "драйвер" в этом можно убедиться глянув исходники
так например для `createAttributeMetadataConfiguration` это `AttributeDriver`
есть еще и `XmlDriver` он нужен для того чтобы прописывать маппинг через xml -
файлы.



```php
<?
// настройка маппинга наших сущностей - т.е. этим опеределяем как доктрин
// сможет определять какие наши классы являются сущностями а какие нет
$config = ORMSetup::createAttributeMetadataConfiguration($paths, $isDevMode);
//
$connection = DriverManager::getConnection($dbParams, $config);
// инстанцирование самого обьекта em через который и будем работать с бд
$em = new EntityManager($connection, $config); // em -- entityManager

///////////////////////////////
// Имея доступ к em уже можно работать с бд через наши сущности:
// код по сути не меняется - здесь создаём юзера для регистрации по почте
$user = User::requestJoinByEmail(
  $id = Id::generate(),
  $date = new DateTimeImmutable(),
  new Email('mail@example.com'),
  'hash',
  $token = new Token(UUID::uuid4()->toString(), $date->modify('+1 hour'))
);

$em->persist($user); // здесь говорим - "сохрани в бд"
$em->flush();        // отправка сформированных запросов в БД
///////////////////////////////
```

Особенность EntityManager в том, что у него есть обьект UnitOfWork, реализующий
одноименный паттерн. Суть его втом, что паттер UnitOfWork сначала накапливает
всю работу которую нужно будет произвести.

```php
<?
$em->persist($user);
$em->persist($user2);
$em->persist($user3);
$em->flush();
```

то есть сначала мы добавляем всякие
разные обьекты, передавая их в EntityManager(em), оно там собирается.
и только при вызове метода flush(commit) происходит отправка данных и изменений
в БД

Под капотом `$em->persist()` помещает обьект в приватный массив `entitiesForInsert`
`$rm->remove()` в `entitiesDelites`
и уже дальше при общении с БД будут сформированы SQL запросы на вставку и удаление

Когда же надо просто обновить поля нашей сущности, то не нужно будет вызывать
никаких методов:

```php
<?
// достали сущность юзера из бд и поместили данные из таблиц в обьект
$user = $em->find(User::class, $id);
// при этом под капотом этот обьект добавляется в приватный массив identityMap
// в этом массиве указывается что по идишнику $id хранится сущность $user
// и есть вызывать find для этого же $id несколько раз то будет возвращаться уже
// сформированный обьект $user вместо того чтобы лезть каждый раз в бд и
// персобирать его.
// Так же при формировании обьекта сущности происходит сохранение состояния
// которое было в момент доставания сущности из бд
$user->confirmJoin($token->getValue(), new DateTimeImmutable());

$em->flush() // здесь произойдёт обновление полей нашего обьекта $user
```
Как это работает - при flush идёт проверка того изменились ли поля обьекта.
То есть идёт сравнение полей значение которых были в момент доставанияя из бд
с теми значениями которые есть в данный момент. И если поля в сущностях изменились
то эти обьекты добавляются в массив на обновление. И при общении с БД создадутся
SQL запросы на обновление изменившихся сущностей.

Поэтому код обработчиков(Handler-ы) для наших команд не содержит никаких вызовов
update, а только flush. т.к. в нём и будет автоматически происходить обновление
изменённых полей обьектов.

Теперь имея представление как работает EntityManager и как с ним можно работать
его нужно подключить к проекту зарегистрировав в di-контейнере внедрения
зависимостей.


## Добавляем Doctrine в наш конфиг di-контейнера

Новый файл с описанием для di-контейнера того, как строить DoctrineORM обьекты
./api/config/common/doctrine.php
```php
<?php
return [
  // прописываем настройку для di-контейнера как инстанцировать EntityManager
  EntityManagerInterface::class =>
  static function (ContainerInterface $container): EntityManagerInterface {
    $settings = $container->get('config')['doctrine'];
    $cache =

    $config = ORMSetup::createAttributeMetadataConfiguration(
      $settings['metadata_dirs'], // 1 каталоги с классами сущностей
      $settings['dev_mode'],      // 2 режим работы dev или prod
      $settings['proxy_dir'],     // 3 каталог для генерируемых прокси классов
      $settings['cache_dir']      // 4
        ? new FilesystemAdapter('', 0, $settings['cache_dir'])
        : new ArrayAdapter()
    );
    //...
  },
  //...
];
```

`3 proxy_dir` - каталог где DoctrineORM будет хранить соданные прокси-классы для
классов наших сущностей. Что это такое и зачем:
DoctrineORM поддерживает ленивую загрузку связей. То есть когда
мы запрашиваем обьект сущности из БД, который может состоять из множества
разных полей класса. То эти все эти поля заполняются не все сразу в момент
создания самого обьекта сущности, а по мере необходимости - т.е. при доступе.
Это и есть ленивая загрузка связей - т.е. заполнение полей по данным из БД
идёт не сразу в момент инстанцирования класса, а только при обращении к этим
полям. И реализуется подобное самой Doctrine за счёт того, что она на лету
создаёт для наших классов сущностей свои proxy-классы(обёртки), в которых
содержится код с запросами к БД.
То есть это нечто похожее на stub и mock-и в тестах, когда мы переопределяем
нужные нам оригинальные классы задавая им нужное нам поведение через классы
посредники(proxy). Так же и Doctrine сама на лету умеет создавать proxy-классы
для классов наших сущностей, переопеределяя методы нашего класса так, чтобы
происходила подгрузка данных из бд. То есть обратились к геттеру - и он не
достаёт значение из приватного поля а идёт в бд достаёт от туда значение
записывает в поле класса и отдаёт запрашивающему, и при последующих вызовах
уже выдаёт значение которое уже хранится в поле класса, а не бегает каждый раз
в бд. Всё это Doctrine делает так же через рефлексию, и подобные proxy классы
нужно кэшировать и где-то хранить. именно для этого и используется proxy_dir

`2 dev_mode`
Режим проксирования определяет флаг dev_mode
false - ("работаем в проде") Doctrine один раз генерит прокси-классы, сохраняет
их в каталог proxy_dir и дальше всегда использует их, пока не очистить кэш.
true  - перегенировать прокси классы на каждый запрос. минимум кэша.


`4 cache_dir`
Здесь в метод-фабрику createAttributeMetadataConfiguration передаётся обьект
кэша, через который и будут кэшироваться все наши элементы.
Что это такое и как используется можно посмотреть в исходнике:

./api/vendor/doctrine/orm/lib/Doctrine/ORM/ORMSetup.php
```php
<?
class ORMSetup {
    // ...
    public static function createAttributeMetadataConfiguration(
        array $paths,                              // metadata_dirs
        bool $isDevMode = false,                   // dev_mode
        ?string $proxyDir = null,                  // proxy_dir
        ?CacheItemPoolInterface $cache = null,     // cache_dir  <<<<<<
        bool $reportFieldsWhereDeclared = false
    ): Configuration {
        $config = self::createConfiguration($isDevMode, $proxyDir, $cache);
        $config->setMetadataDriverImpl(new AttributeDriver($paths, $reportFieldsWhereDeclared));

        return $config;
    }

    // ...

    /**
     * Creates a configuration without a metadata driver.
     */
    public static function createConfiguration(
        bool $isDevMode = false,
        ?string $proxyDir = null,
        ?CacheItemPoolInterface $cache = null     // <<<< тот самый объект кэша
    ): Configuration {
        $proxyDir = $proxyDir ?: sys_get_temp_dir();

        $cache = self::createCacheInstance($isDevMode, $proxyDir, $cache);

        $config = new Configuration();

        $config->setMetadataCache($cache);  // << **
        $config->setQueryCache($cache);     // << **
        $config->setResultCache($cache);    // << **
        $config->setProxyDir($proxyDir);
        $config->setProxyNamespace('DoctrineProxies');
        $config->setAutoGenerateProxyClasses($isDevMode);

        return $config;
    }
```
Видим что при создании конфигурации через createConfiguration переданный нами
обьект кэша будет передаваться во все места где нужен кэш (**)

Обьект кэш может быть либо неким файловым кэшем, который будет сохранять всё в
файлы на диск(например в каталог var/cache)
А можно для dev-режима, чтобы кэш на диск не писался и не мешал разработке,
передавать своего рода заглушку где кэширование будет происходить только в
памяти обьекта в его приватные поля, без сохранения на диск

У себя так и делаем.
./api/config/common/doctrine.php
```php
<?
return [
    $settings = $container->get('config')['doctrine'],
    // ...                           |       |
    'config' => [        //       <--'       |
        'doctrine' => [  //               <--'
            'dev_mode' => false,
            'cache_dir' => __DIR__ . '/../../var/cache/doctrine/cache', // <<<
            'proxy_dir' => __DIR__ . '/../../var/cache/doctrine/proxy',
        ]
    ]
  ];
```
если в конфиге `cahce_dir` не пустой и в нём указан каталог: здесь это
var/cache/doctrine/cache

```php
<?
$settings['cache_dir']
  ? new FilesystemAdapter('', 0, $settings['cache_dir'])  // кэш в файлы на диск
  : new ArrayAdapter()                                    // кэш в память

```
то есть эта строчка кода работает так
если в `$settings['cache_dir']` не null то вызывается FilesystemAdapter
если же null то второе ArrayAdapter



```php
<?
  //.. конец анонимной функции по созданию инстанса EntityManager
  // стратегия именования и маппинга имён полей классов к полям таблиц в БД
  // joinConfirmToken -> join_confirm_token
  $config->setNamingStrategy(new UnderscoreNamingStrategy());

  $connection = DriverManager::getConnection(
      $settings['connection'], // прописываем тут же в конфиге чуть ниже
      $config // наш конфиг созданный через createAttributeMetadataConfiguration
  );
  // инстанцирование EntityManager на основе только что созданного конфига:
  return new EntityManager($connection, $config);
// }

```
здесь же чуть ниже идёт массив с доп настройками, значения берём из EnvVar
```
  'config' => [
      'doctrine' => [
          ...
          'connection' => [
              'driver' => 'pdo_pgsql',
              'host' => getenv('DB_HOST'),
              'user' => getenv('DB_USER'),
              'password' => getenv('DB_PASSWORD'),
              'dbname' => getenv('DB_NAME'),
              'charset' => 'utf-8',
          ],
      ],
  ],
```

`$config->setNamingStrategy(new UnderscoreNamingStrategy())`
Эту настройку здесь мы прописываем для того, чтобы руками в классе не указывать
соответствие имени поля класса имени колонке в ДБ. Дело в том что по PSR имена
полей в классах рекомендуется писать в верблюжей нотации, а имена полей в бд
таблицах идут в нижнем регистре с подчёркиванием.
Эта вещь сделает так, чтобы DoctrineORM сама мапила имена из кода в имена в БД.

то есть поля классов будут мапится joinConfirmToken -> join_confirm_token
если эту вещь не делать то придётся руками под каждым полем класса которое не
соответствует названию колонке в таблице указывать "табличное имя колонке"

### Как работает эта конфигурация для di-контейнера

- di-контейнер при первом запросе на инстанцирование обьекта класса
EntityManagerInterface вытащит нашу анонимную функцию по которой будет создан
инстанс со всеми описаными нами параметрами и настройками. далее этот обьект
будет сохранён в di-контейнере и будет возвращатся везде где он будет нужен.

```php
<?php
return [
  EntityManagerInterface::class =>
  static function (ContainerInterface $container): EntityManagerInterface {
    //.. тело анонимной функции инстанцирует обьект один раз далее он хранится
    // в di-контейнере и на возвращается уже готовый везде где он будет нужен

    $config->setNamingStrategy(new UnderscoreNamingStrategy());

    $connection = DriverManager::getConnection(
        $settings['connection'],
        $config
    );
    return new EntityManager($connection, $config);
  }
];
```

### Передача значений в php для подключения к БД
Здесь у нас значения для подключения к БД передаются через переменные окружения.
Вообще-то можно использовать и одну перменную DB_URL вместо
DB_HOST DB_USER DB_PASSWORD DB_NAME. Но у такого подхода есть минусы если
перейдём на передачу пароля от бд не через переменную окружения а через файл
секретов. (DockerSwarm Kubernetis)

```
  'connection' => [
      'driver' => 'pdo_pgsql',
      'host' => getenv('DB_HOST'),
      'user' => getenv('DB_USER'),
      'password' => getenv('DB_PASSWORD'),
      'dbname' => getenv('DB_NAME'),
      'charset' => 'utf-8',
  ],
```

Теперь нам нужно прописать эти EnvVar-ы в docker-compose файлы для dev & prod

./docker-compose-production.yml
```yml
version: '3.7'
services:
  # ...
  api-php-fpm:
    image: ${REGISTRY}/auction-api-php-fpm:${IMAGE_TAG}
    restart: always
    environment:
      APP_ENV: prod
      APP_DEBUG: 0
      DB_HOST: api-postgres
      DB_USER: app
      DB_PASSWORD: ${API_DB_PASSWORD}
      DB_NAME: app

  api-php-cli:
    image: ${REGISTRY}/auction-api-php-cli:${IMAGE_TAG}
    restart: always
    environment:
      APP_ENV: prod
      APP_DEBUG: 0
      DB_HOST: api-postgres
      DB_USER: app
      DB_PASSWORD: ${API_DB_PASSWORD}
      DB_NAME: app
  #...
```


./docker-compose-production.yml
```yml
version: '3.7'
services:
  #...
  api-php-fpm:
    build:
      context: api/docker
      dockerfile: development/php-fpm/Dockerfile
    environment:
      APP_ENV: dev
      APP_DEBUG: 1
      PHP_IDE_CONFIG: serverName=API
      DB_HOST: api-postgres
      DB_USER: app
      DB_PASSWORD: secret                         # просто заглушка
      #DB_PASSWORD_FILE: ... путь к файлу с паролем
      DB_NAME: app
    volumes:
      - ./api:/app

  api-php-cli:
    build:
      context: api/docker
      dockerfile: development/php-cli/Dockerfile
    environment:
      APP_ENV: dev
      APP_DEBUG: 1
      DB_HOST: api-postgres
      DB_USER: app
      DB_PASSWORD: secret
      DB_NAME: app
    volumes:
      - ./api:/app

  api-postgres:
    image: postgres:13.12-alpine
    environment:
      POSTGRES_USER: app
      POSTGRES_PASSWORD: secret
      POSTGRES_DB: app
    volumes:
      - api-postgres:/var/lib/postgresql/data
    ports:
      - "54321:5432"
```


## Делаем Dev версию настройки Doctrine

Отключаем кэш для разработки (dev-окружения)

./api/config/dependencies.php
У нас уже настроено разделение конфига на разные окружения.(prod,dev,test)
Есть каталог `api/config/common` с общими для всех базавыми настройками,
которовые на основе переменной `APP_ENV` могут быть переопределены на нужные.

```php
<?
$files = array_merge(
    glob(__DIR__ . '/common/*.php') ?: [], // настройки общие для всех окружений
    glob(__DIR__ . '/' . (getenv('APP_ENV') ?: 'prod') . '/*.php') ?: []
  //                             ^^^^^^^^
);
// Здесь все файлы собираются в один большой массив сначала грузятся все файлы
$configs = array_map(function($file):array{return require $file;}, files,);
// и из них собираются отдельные массивы и затем все эти массивы обьединяются в
//один большой
return array_replace_recursive(...$configs);
// эта функция позволяет склеивать множество массивов в один большой так,
// чтобы новые значения переопределяли(затерали) старые значения
```



в частности сейчас нам нужно переопределить поведение кэша для dev-окружения
для этого можно просто создать файл ./api/config/dev/doctrine.php

```php
<?php

declare(strict_types=1);

return [
    'config' => [
        'doctrine' => [
            'dev_mode' => true,
            'cache_dir' => null, // переопределяем common-настройку
        ],
    ],
];
```

тоже самое делаем и для тестов (test-окружение)
./api/config/test/doctrine.php



## Проблема конфликта доступа к кэшу для разных докер-контейнеров

из-за разных парав доступа - работа от разных user(системных)

У нас php код бегает в разных режимах под двумя разными докер-компоуз сервисами
api-php-fpm и api-php-cli

и все php-скрипты из под контейнера api-php-cli выполняются от лица root,
(т.к. user не настроен в development/php-cli/Dockerfile)
тогда как в api-php-fpm от лица www-data

В итоге если запустить в дев-окружении некую консольную команду, которая приведёт
к тому что будет сгенерен кэш в var/cache то он создасться с владельцем root
и в этот же каталог не сможет получать доступ api-php-fpm и будет выдавать ошибку
(т.е. работает не от root и прав доступа не имеет)

Решение: прописываем использование разных каталогов под кэш proxy_dir
Делаем это через php константу `PHP_SAPI` - содержит название текущего процесса

`'proxy_dir' => __DIR__ . '/../../var/cache/' . PHP_SAPI . '/doctrine/proxy',`

Для api-php-cli значением PHP_SAPI будет `cli`
Причем делаем это только для dev & test окружений, оставляя для прода как есть.


Далее настроим консольные команды для подключения к EntityManager


## CLI комманды Doctrine
https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/configuration.html#setting-up-the-commandline-tool

Doctrine идёт с набором готовых cli комманд позволяющих
- создавать БД и таблицы
- генерировать некую информацию, сущности
- делать некие преобразования и операции с БД

Способы подключения команд докстрины к нашему проекту
https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/tools.html

#### 1й способ через создание конфига и стандартной команды vendor/bin/doctrine

```sh
php vendor/bin/doctrine
```
Причем если не прописать конфиг файл для doctrine-cli то выдаст такую ошибку
cli-config.php config/cli-config.php
```
php api/vendor/bin/doctrine
[Warning] The use of this script is discouraged.
See https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/tools.html#doctrine-console
for instructions on bootstrapping the console runner.


You are missing a "cli-config.php" or "config/cli-config.php" file in your
project, which is required to get the Doctrine Console working. You can use the
following sample as a template:
```

```php
<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap
require_once 'bootstrap.php';

// replace with mechanism to retrieve EntityManager in your app
$entityManager = GetEntityManager();

return ConsoleRunner::createHelperSet($entityManager);

```

Суть здесь такая - сначала создаётся инстанс того самого EntityManager затем
идёт создание `ConsoleRunner` с передачей в него em

Мы у себя поступим иначе и будем использовать наш di-контейнер для того чтобыe
em инстанцировать из него на основе всех нами прописанных конфигов.

То есть подгружаем наш di-контейнер, из этого di-контейнера достаём настроенный
em и уже дальше создаем ConsoleRunner и HelperSet

#### 2й способ использовать комнады - Symfony\Component\Console\Application;
Т.к. cli команды доктрин написаны для symfony-console, и мы её тоже используем в
своём коде для api/bin/app.php, то можно взять все нужные нам команды доктрин
и добавить в свой консольный скрипт.


## Добавляем cli команды doctrine в наш api/bin/app.php
./api/bin/app.php

```php
<?
$commands = $container->get('config')['console']['commands'];
// добавление всех доктриновских команд к нашим командам
Doctrine\ORM\Tools\Console\ConsoleRunner::addCommands($cli); // <<<<

foreach ($commands as $name) {
    /** @var Symfony\Component\Console\Command\Command $command */
    $command = $container->get($name);
    $cli->add($command);
}
$cli->run();
```
глянув исходный код метода addCommands():
./api/vendor/doctrine/orm/lib/Doctrine/ORM/Tools/Console/ConsoleRunner.php

видим что это просто добавление всех имеющихся команд в наше консольное приложение
$cli
```php
<?
final class ConsoleRunner{
//..
  public static function addCommands(
    Application $cli,
    ?EntityManagerProvider $entityManagerProvider = null
    ): void {
      // ...
        $cli->addCommands(
            [
              // DBAL Commands
              new DBALConsole\Command\ReservedWordsCommand($connectionProvider),
              new DBALConsole\Command\RunSqlCommand($connectionProvider),

              // ORM Commands
              new Command\ClearCache\CollectionRegionCommand($entityManagerProvider),
              new Command\ClearCache\EntityRegionCommand($entityManagerProvider),
              new Command\ClearCache\MetadataCommand($entityManagerProvider),
              new Command\ClearCache\QueryCommand($entityManagerProvider)
              //...
      );
  //...
  }
}
```
То есть после вызова этого метода к нашему $cli - консольному приложению добавятся
все возможные команды. Проблема только в том как передать в них нашего $em
EntityManager-a

Для передачи нашего em из di-контейнера

```php
<?
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;

// ...

/** @var Doctrine\ORM\EntityManagerInterface $entityManager */
$entityManager = $container->get(Doctrine\ORM\EntityManagerInterface::class);
$cli->getHelperSet()->set(new EntityManagerHelper($entityManager), 'em');
// $cli->setHelperSet(ConsoleRunner::createHelperSet($entityManager));

Doctrine\ORM\Tools\Console\ConsoleRunner::addCommands($cli);
```
Как это работает:

$cli->getHelperSet() - достаём HelperSet из нашего консольного приложения $cli
->set(new EntityManagerHelper($entityManager), 'em');
здесь мы прописываем под именем 'em' передаём инстанс нашего EntityManager,
обёрнутый в класс обёртку EntityManagerHelper(deprecated)

У симфонивской консоли есть некий набор хэлперов
и в этот набор можно добавить свой хэлпер под нужным именем(здесь это `em`)
получить этот набор хэлперов можно через getHelperSet()
Дальше уже по коду все доктриновские команды будут обращаться к этому, нами
только что добавленому, хэлперу и через него получать инстанцированный через
di-контейнер EntityManager со всеми нужными нам настройками.

./api/vendor/doctrine/orm/UPGRADE.md
проверить список всех доступных консольных команд:
```sh
docker compose run --rm api-php-cli composer app
```

Видим что команд много и не все нам будут нужны, поэтому можно выбрать только
нужные. Для этого просто копируем нужные нам команды из
./api/vendor/doctrine/orm/lib/Doctrine/ORM/Tools/Console/ConsoleRunner.php
метода addCommands и добавляем их в наш конфиг где мы прописываем классы наших
команд т.е. в ./api/config/common/console.php

```php
<?php
declare(strict_types=1);
use App\Console;

return [
    'config' => [
        'console' => [
            'commands' => [
                Console\HelloCommand::class,
/*  >>>>>  */   Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand::class,
            ]
        ]
    ],
];
```
После этого закомитим строку: чтобы не добавлялись все команды
Doctrine\ORM\Tools\Console\ConsoleRunner::addCommands($cli);
и проверяем список доступных

```sh
docker compose run --rm api-php-cli composer app
```
```
Available commands:
  hello                Hello command
  help                 Display help for a command
  list                 List commands
 orm
  orm:validate-schema  Validate the mapping files             <--- есть!
```

Выбираем какие еще команды нам могут быть полезны:

orm:schema-tool:drop - удаление всех бд и всех таблиц
Эта команда может быть полезна для разработки когда нужно будет удалить имеющуюся
таблицу и проверить работу миграций. Но проблема в том, что эта команды не должна
быть доступна для prod-окружения только для dev.
Чтобы исключить такую возможность - случайного запуска этой команды и удаление
в проде всей БД можно скопировать файл api/config/common/console.php в
./api/config/dev/console.php и только в этом файле добавить нужную команду.

Ожидам что теперь для dev окружения у нас будет 3 команды
2 мы прописали для common и одну на удаление всего для dev
Проверяем видим:
```
Available commands:
  help                  Display help for a command
  list                  List commands
 orm
  orm:schema-tool:drop  Drop the complete database schema of EntityManager
                        Storage Connection or generate the corresponding SQL output
  orm:validate-schema   Validate the mapping files
```
Пропала наша команда `hello` и это происходит потому, что  мы склеиваем наши
массивы через функцию `array_replace_recursive`
а эта функция для массивов заменяет содержимое индексов, а не слеивает два
массива в один. Т.е. в массив commands значение ключа 0 где наша hello команда
подставляется значение из второго массива из api/config/dev/console.php

То есть функция `array_replace_recursive` хороша когда надо переопределять
значения, в том числе внутри массива по его числовым индексам, но она не будет
склеивать значения двух массивов в один общий массив как нам здесь это нужно

Простейшее решение - дублировать полный список команд в разных файла

./api/config/common/console.php
```php
<?
return [
    'config' => [
        'console' => [
            'commands' => [
                Console\HelloCommand::class,
                Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand::class,
            ]
        ]
    ],
];
```

./api/config/dev/console.php

```php
<?
use App\Console;
use Doctrine\ORM\Tools\Console\Command\SchemaTool;
use Doctrine\ORM\Tools\Console\Command;

return [
    'config' => [
        'console' => [
            'commands' => [
                Console\HelloCommand::class,
                Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand::class,
                SchemaTool\CreateCommand::class,
                SchemaTool\DropCommand::class,
                Command\InfoCommand::class,
            ]
        ]
    ],
];
```

Более лучшее решение - продвинутый инструмент для мержа (обьединения)
конфигурационных массивов в один большой массив.
./docs/13-doctrine/04-laminas-config-aggregator.md


## Уходим от использования deprecated классов
Данная строка кода даёт предупреждение о том, что используется устаревший класс
$cli->getHelperSet()->set(new EntityManagerHelper($entityManager), 'em');

```
Call to deprecated class
"Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper":
This class will be removed in ORM 3.0 without replacement.
```
Если посмотреть на сами команды doctrine то можно увидить что они большая их
часть принимает в конструктор $entityManagerProvider
это видно здесь:
```
ConsoleRunner.addCommands:

    Application $cli,
       ?EntityManagerProvider $entityManagerProvider = null  <<<
    ): void {
        ...
        new Command\InfoCommand($entityManagerProvider),
```
и здесь
```php
<?
namespace Doctrine\ORM\Tools\Console\Command;
class InfoCommand extends AbstractEntityManagerCommand {/*...*/}
// ...
abstract class AbstractEntityManagerCommand extends Command
{
    /** @var EntityManagerProvider|null */
    private $entityManagerProvider;

    public function __construct(
        ?EntityManagerProvider $entityManagerProvider = null  // <<<<<
    ) {
        parent::__construct();
        $this->entityManagerProvider = $entityManagerProvider;
    }
```
То есть нужно инстанцировать обьекты нужных нам команд передвая в них обёрту
вроде:

```php
<?
    $em = $container->get(EntityManagerInterface::class);
    return new SingleManagerProvider($em);
```
Помня что DI-контейнер у нас ипользуется с автовайрингом (autowiring)
т.е. при инстанцировани классов в обьекты, контейнер сам парсит конструкторы
запрашиваемых на инстанцирование классов и подставляет в аргументы конктруктора
обьекты нужных типов, которые так же собирает сам, по описанным правилам в
его же настройках.
  EntityManagerProvider - это интерфейс и для того чтобы di-контейнер мог
подставлять в конструкторы запрашиваемых у него классов конкретные обьекты надо
указать маппинг из какого конкретного класса нужно собирать обьект данного типа
(интерфейса) делается это вот так:
```php
<?
   return [
        //... маппинг какой конкретный класс исп-ть для данного интерфейса
        EntityManagerProvider::class => DI\get(
            SingleManagerProvider::class
        ),
        //..
  ];
```
[Здесь обьяснения по di-контейнерам]
(./docs/06-install-slime-symfony-cli/05-create-src-dir.md)

Но особенность этого процесса в том, что для аргументов у которых прописаны
значения по умолчанию подстановка обьекта не производится!
А у нас во всех командах прописан EntityManagerProvider как null.
А это значит что просто будет подставляться null и не будет инстанцирвоаться
нужный обьект для нами описанного правила

        EntityManagerProvider -> SingleManagerProvider

В этом убедился через написание своей собтсвенной команды:
./api/src/Console/DICheckCommand.php

Когда конструктор принимает конкретное значение и нет `?Type == null`
```
 public function __construct(EntityManagerProvider $entityManagerProvider)
```
тогда di-контейнер идёт по описанному правилу интерфейс-> класс и создаёт и
подставляет нужный обьект реализующий EntityManagerProvider,
```sh
docker compose run --rm api-php-cli composer app di-check
> php bin/app.php --ansi 'di-check'
DI USE SingleManagerProvider
EntityManagerProvider: has
```

а вот если изменить конструктор на такой
```
 public function __construct(?EntityManagerProvider $entityManagerProvider=null)
```
когда значение становиться "не обязательным" тогда передаётся null
```sh
docker compose run --rm api-php-cli composer app di-check
EntityManagerProvider: NULL
```

Если забыть прописать правило интерфейс-реализующий_класс тогда будет такая
ошибка:
```
PHP Fatal error:
Uncaught DI\Definition\Exception\InvalidDefinition:
Entry "App\Console\DICheckCommand" cannot be resolved:
Entry "Doctrine\ORM\Tools\Console\EntityManagerProvider" cannot be resolved:
the class is not instantiable
```


Выходит что для того чтобы избежать использование устаревшего класса
нужно прописать правила инстанцирования нужных нам команд с подстановкой в них
нашего entityManager-а собранного из di-контейнера

Убираем эту строку кода: из api/bin/app.php
```
// $cli->getHelperSet()->set(new EntityManagerHelper($entityManager), 'em');
```

./api/config/common/doctrine.php
```php
<?
return [
    // .. root level:
    Command\InfoCommand::class =>
    static function (ContainerInterface $c): Command\InfoCommand {
        return new Command\InfoCommand($c->get(SingleManagerProvider::class));
    },
];
```

```
/**
 * @psalm-import-type Params from DriverManager
 */
```

Исходники doctrine
./api/vendor/doctrine/orm/lib/Doctrine/ORM/Tools/Console/ConsoleRunner.php

