## Практика по автоматизированному конфигурированию удалённой машины(сервера)

Ипользование Ansible для нашего кода(сайта)
конфигурирование чистой VM для установки докера и серт-бота

Создавать наш сервер будем не вручную, а полностью автоматически и для этого
и будем использовать Ansible

Создаём отдельный каталог `provisioning` в нём и будет размещатся всё для этого

### Inventory Адреса удалённых хостов

Создадим простую `затоговку` под описание адреса сервера или серверов:
./provisioning/hosts.yml.dist

```yml
all:
  hosts:
    server:
      ansible_connection: ssh
      ansible_user: root
      ansible host: 0.0.0.0
      ansible port: 22
```

Это делается для удобства - чтобы она была всегда под рукой и можно было
просто скопировать эту заготовку переименов в рабочий файл с yml и заполнить
реальными ip адресами и данными для подключения к конкретному серверу.

[Ansible inventory-guide](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html#behavioral-parameters)

Далее Из корня проекта копируем заготовку и заполняем её реальным адресом сервера
```sh
cp ./provisioning/hosts.yml.dist ./provisioning/host.yml
```

Для успешного доступа ssh ключ уже должен быть установлен на удалённом сервере.
Если надо использовать конкретный ссш ключ а не дефолтный то можно указать так

      ansible_ssh_private_key_file: ~/.ssh/keys/some_project/id_rsa

в файле hosts.yml может быть описано сразу несколько серверов. вот так
```yml
all:
  hosts:
    server_1:                  # просто имя сервера?
      ansible_connection: ssh
      ansible_user: root
      ansible_host: 1.2.3.4
      ansible_port: 22
    server_2:
      ansible_connection: ssh
      ansible_user: root
      ansible_host: 1.2.3.5
      ansible_port: 22
```

при использовании такого файла ансибл просто пройдётся по всем этим серверам
и выполнит указанный playbook для каждого из описанного сервера


# Создание Ansible playbook для своего сайта

Далее описываем свой ansible `playbook` - это файл ./provisioning/site.yml

```yml
---
- name: Provision Site     # название playbook-а
  hosts: all               # к каким хостам применять данный playbook - все
  remote_user: root        # юзер от лица которого будут выполнятся команды
  tasks:                   # задачи которые нужно выполнить (состояние машины)
  # ...
```
если например playbook написан под конкретный сервер из стака то можно указать
явно что применять его надо только для конкретного сервера а не ко всем:

```yml
- name: PlayBook Name
  hosts: server_1
  # ...
```

Задачи(tasks) в playbook пишем на основе инструкций по установке докера

https://docs.docker.com/engine/install/debian/#install-using-the-repository

и letsencrypt-а (search keywords):

    how-to-secure-apache-with-let-s-encrypt-on-debian-11
    Setting Up the SSL Certificate

Суть написания задач(tasks) для playbook такая же как при ручной установке
поэтапно описываем все нужные шаги для установки. например:
- 1 apt update
- 2 install packages to allow apt to use a repository over HTTPS
- 3 add dockers official GPG key (sudo apt-key add -)
- 3.2 verify fingerprint of GPG key (через apt-key fingerprint)
- 4 Добавляем репозиторий через add-apt-repository
- 5 sudo apt-get update
- 6 sudo apt-get install docker-ce

Теперь всё это пишем в задачи плейбука

1 `apt update`
```yml
    - name: Update apt packages    # название задачи(task-a)
      apt:                         # так называемая секция "apt"
        update_cache: yes
```

2 нужные для работы пакеты
```sh
sudo apt-get install \
  apt-transport-https \
  ca-certificates \
  curl \
  software-properties-common
```
выглядеть это будет так:
```yml
    - name: Install dependencies
      apt:
        name:
          - apt-transport-https
          - ca-certificates
          - curl
          - software-properties-common
        state: present             # пакеты должны быть свежими и установленными
        update_cache: yes          # обновлять кэши apt
```

Добавление GPG-ключа и его верификация(проверка на подлиность)
```yml
    - name: Add GPG key
      apt_key:
        url: https://download.docker.com/linux/debian/gpg
        state: present
    - name: Verify fingerprint
      apt_key:
        id: OEBFCD88
        state: present
```

Добавление нужного репозитория
```yml
    - name: Set up the repository
      apt_repository:
        repo: deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian   bullseye stable
        state: present
        filename: docker
        update_cache: yes
```

apt-get update
```yml
    - name: Update apt packages
      apt:
        update_cache: yes
```

Установка Докера CE(CommunityEdition)
```yml
# tasks:               этот блок yml-кода - task - задача в списке tasks-ов
    # ...
    - name: Install Docker           # название задачи(task)
      apt:                           # apt - установить
        name: docker-ce              # имя пакета что надо устанавливать
        state: present               # состояние - должно быть установлено
        update_cache: yes            # обновлять кэш apt(?)
      notify: Start docker on boot   # запустить проверку доступности докера
```
Здесь описана установка докера через apt и проверка его доступности как службы.
Проверка доступности идёт через уведомление (nitify) к "Start docker on boot".
"Start docker on boot" - это имя обработчика, который должен быть запущен.
сам обработчик(handler) описывается отдельно в самом низу playbook-yml-файла.
(в корневом узле handlers).

Хэндлеры - это своего рода вспомогательные команды, которые должны выполняться
только при неких определённых условиях.

Т.е. фактически вышеописанным task-блоком(задачей) мы говорим:
- установи docker-ce
- и если докера не было и реально выполнялась установка тогда:
  запусти проверку доступен ли докер как служба.

Проверка доступности докера происходит в отдельном хэндлере с именем
`Start docker on boot` обращение(ссылка из task-a в handler) на этот хэндлер
идёт по этому конкретному имени после ключевого слова `notify`.
notify будет запускать хэндлер только когда задача реально выполнилась,
если же задача была пропущена(докер уже установлен) то и хэндле не запустится.

notify - уведомить т.е. задача(task) уведомляет обработчки(handler) о том,
что этот обработчик должен быть запущен. Одна задача может "уведомлять" сразу
несколько обработчиков т.е. notify может принимать как строку так и список.

Сам обработчик опишем в отдельной handlers внизу этого же файла. вот так:
```yml
  handlers:
    - name: Start docker on boot  # имя хэндлера по нему из task-а обращаемся
      systemd:                    # служба с именем
        name: docker              # докер должна быть
        state: started            # запущена и
        enabled: yes              # доступна
```
В блоке обработчика мы описываем проверить запущена ли служба docker-а (`systemd`)
и доступна ли она в системе(state:started).

Обработчик(Handler) - это своего рода вспомогательная команда.
Запускается хэндлер только получив уведомление(notify) от некой задачи.
сам по себе хэндлер не запускается. и пропущеная задача не шлёт уведомлений.

Пример: изменился конфиг службы nginx-а и после этого его надо перезапустить,
но если конфиг реально не изменился - то и перезапускать службу не надо.

То есть хэндлер - это способ выполнить что-то только после того как задача
реально была выполнена и в конце послала notify обработчику.

[playbooks_handlers](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_handlers.html)

Зачем этот хэндлер надо? Наш файл site.yml - это provision-скрипт который мы
будем запускать постоянно, а не сугу только один раз только лишь для установки.
Поэтому при первом запуске на чистой ВМке будет проходить установка докера,
а при последующих запусках этого же скрипта задачи по установке докера будут
просто пропускаться и соответственно notify проходить не будет т.к. задача
`Install Docker` будет пропущена. так как докер и так уже стоит, а значит выполнять
задачу по его установке не надо.


Установка старой версии docker-compose

```yml
    # old deprecated way to install docker-compose
    - name: Install Docker Compose # install file through simpe wget
      get-url:
        url: https://github.com/docker/compose/releases/download/1.25.0/docker-compose-{{ absible_system }}-{{ ansible_us}}
        dest: /usr/local/bin/docker-compose   # куда сохранять скаченный файл
        group: docker                         # добавить файл в группу docker
        mode: 'u+x,g+x'                       # назначить права на запуск
```
Здесь идёт прямое скачивание файла по ссылке(get-url) в указанное место(файл)
Добавление в группу docker делается потому, что docker работает через сокет-файл
доступ к которому есть только у пользователей с группой docker

### Автоочистка докер-обьектов

При разработке будет идти частый деплой на сервер новых докер-образов
и они будут накапливаться на сервере образуя мусор забивающий диск.
Со временем встанет вопрос очистки этих ненужных старых образов.
Для автоматизации этого действия добавим в крон задачу:
"Очистка докера от старых элементов"

```yml
    - name: Set periodic Docker prune
      cron:
        name: docker-prune
        job: 'docker system prune -f --filter “until=$((30*24))h"'
        minute: '1'
        hour: '1'
```

Команда для удаления всех старых докер-элементов(обрзы, контейнеры и прочее)
которые старше 30 дней (здесь указано значение в часах)
```sh
  docker system prune -f --filter "until=$((30*24))h"
```

Так же в самом конце playbook-файла добавим команды для очистки apt

Удаление старых зависимостей - старых уже не нужных пакетов и зависимостей
```yml
    - name: Remove useless packages
      apt:
        autoclen: yes

    - name: Remove useless dependencies
      apt:
        autoremove: yes
```
Делается это для того, чтобы система не засорялась всякими ненужными пакетами
и apt-кэшами


## Разделяем один большой playbook.yml файл на несколько частей.

Дальне нужно еще добавить задачи по установке certbot-а для работы с letsencrypt
Чтобы не создавать огромную простыню кода можно вынести задачи(task) в отдельные
yml-файлы

```
provisioning/
├── hosts.yml
├── hosts.yml.dist
├── site.yml
└── tasks
    └── setup_docker.yml
```

site.yml
```yml
- name: Provision Site
  hosts: all
  remote_user: root
  tasks:
    - import_tasks: tasks/setup_docker.yml    # добавляем внешний файл с задачами

  handlers:
    - name: Start docker on boot
      systemd:
        name: docker
        state: started
        enabled: yes
```

Здесь просто вынесли все задачи по установке докера в отдельный файл, для того
чтобы держать основной playbook.yml файл кратким и понятным

Дальше уже можно добавлять задачи по установке и настройке certbot-a сразу
уже в отдельный файл
[Next](./docs/05-deploy/04-certbot-tasks.md)

