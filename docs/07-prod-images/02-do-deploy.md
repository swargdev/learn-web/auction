## Деплой оптимизированных под прод докер-образов

Собираем прод-образы, пушим в гитлаб-реджестри и деплоим в прод

```sh
REGISTRY=registry.gitlab.com/swargdev/learn-web/auction IMAGE_TAG=2 make build
REGISTRY=registry.gitlab.com/swargdev/learn-web/auction IMAGE_TAG=2 make push
# deploy
HOST=root@10.144.52.181 PORT=22 OPT='-i ~/.ssh/keys/site-auction/id_rsa' \
REGISTRY='registry.gitlab.com/swargdev/learn-web/auction' \
BUILD_NUMBER=2 IMAGE_TAG=2 make deploy
```


Смотрим на роде состояние приложения
```sh
docker ps --format "table {{.Image}}\t{{.Ports}}\t{{.Names}}"
```

```
registry.gitlab.com/swargdev/learn-web/auction/*:2

IMAGE                    PORTS                     NAMES
/auction-api:2           80/tcp                    auction-api-1
/auction-api-php-cli:2   9000/tcp                  auction-api-php-cli-1
/auction-api-php-fpm:2   9000/tcp                  auction-api-php-fpm-1
/auction-frontend:2      80/tcp                    auction-frontend-1

/auction-gateway:2       0.0.0.0:80->80/tcp,       auction-gateway-1
                         :::80->80/tcp,
                         0.0.0.0:443->443/tcp,
                         :::443->443/tcp
```

## Check API
```sh
curl -D - https://api.auction.v6.rocks/
HTTP/2 200
server: nginx
date: Tue, 12 Sep 2023 07:00:05 GMT
content-type: application/json
strict-transport-security: max-age=31536000
content-security-policy: block-all-mixed-content

{}
```


## Check FRONTEND
```sh
curl -D - https://auction.v6.rocks/
```
```
HTTP/2 200
server: nginx
date: Tue, 12 Sep 2023 07:00:29 GMT
content-type: text/html; charset=uft-8
content-length: 501
last-modified: Sun, 10 Sep 2023 14:06:24 GMT
etag: "64fdcd60-1f5"
x-frame-options: SAMEORIGIN
accept-ranges: bytes
strict-transport-security: max-age=31536000
content-security-policy: block-all-mixed-content

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon* href=" /favicon.ico">
    <meta name="description" content="Auction">
    <link href="/app.css" rel="stylesheet" />
    <title>Auction</title>
  </head>

  <body>
    <div id="app" class="app">
      <div class="welcome">
        <hi>Auction</hi>
        <p>We will be here soon</p>
      </div>
    </div>
  </body>
</html>
```
