## Перенос сгенерированного приложения в корень frontend
переносим всё из ./frontend/react-app в ./frontend

Начинаем с очистки от лишнего - от того что мы не будем у себя
использовать

./frontend/.gitignore
```
# See https://help.github.com/articles/ignoring-files/ for more about ignoring files.

# dependencies
/node_modules
/.pnp
.pnp.js

# testing
/coverage

# production
/build

# misc
.DS_Store
.env.local
.env.development.local
.env.test.local
.env.production.local

npm-debug.log*
yarn-debug.log*
yarn-error.log*
```

у себя в проекте мы не будем использовать локальные и дев -енвайраменты:

```
.env.local
.env.development.local
.env.test.local
.env.production.local
```

Env-ы будем передавать через сам докер

Вместо `npm` будем использовать пакетный менеджер `yarn`
поэтому можно удалить npm-debug.log - их просто не будет

в общем из всего старого оставляем только это:

```
/node_modules
/coverage
/build
npm-debug.log*
yarn-debug.log*
yarn-error.log*
```

так же всё это копируем в `.dockerignore` чтобы это не попадало в докер-образ


## Удаляем лишнее из ./frontend/react-app/package.json

```json
{
  "name": "react-app",
  "version": "0.1.0",
  "private": true,
  "dependencies": {
    "@testing-library/jest-dom": "^5.17.0",
    "@testing-library/react": "^13.4.0",
    "@testing-library/user-event": "^13.5.0",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-scripts": "5.0.1",
    "web-vitals": "^2.1.4"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  },
  "eslintConfig": {
    "extends": [
      "react-app",
      "react-app/jest"
    ]
  },
  "browserslist": {
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ],
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ]
  }
}
```

scripts.eject - команда позволяющая извлечь некие файлы в код нашего проекта
Когда устанавливаем React-проект из шаблона то по умолчанию подразумевается что
вся работа будет идти в каталоге src/ где и генерируются все основные файлы
проекта. Но кроме этих файлов из src/ есть некие встроенные в React скрытые
служебные(библиотечные) файлы. В этих служебных файлах описывается внутренняя
логика работы нашего проекта.
И вот для получения доступа к этим файлам и нужна команда eject

eject из каталога node_modules скопирует все используемые служебные файлы
и скопирует их в src/
- настройки webpack, babel и прочих внутренних вещей нужных для работы испо-ых
библиотек.

эта вещь на не нужна поэтому можно просто удалить эту команду из scripts

```
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
  },
```

## Выделяем dev-dependencies

в composer.json у нас изначально было разделение зависимостей на две секции
для прода и для dev-окружения. в JS тоже так можно делать. но здесь это надо
делать вручную:

```
  "dependencies": {
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-scripts": "5.0.1",
    "web-vitals": "^2.1.4"
  },
  "devDependencies": {
    "@testing-library/jest-dom": "^5.17.0",
    "@testing-library/react": "^13.4.0",
    "@testing-library/user-event": "^13.5.0",
  },
```

Далее в devDependencies еще будем доустанавливать всякие вещи нужные именно
для разработки

удаляем файлы которые нам вообще не нужны:

./frontend/public/manifest.json


## PhpStorm JSX-синтаксис
эта IDE может ругаться на такой синтаксис тип не понимаю чё это
решается это простым переименование js в jsx



## Интеграция сгенерированного React-App в код проекта
Сначало вспомним как мы работаем с vendor в нашем API написанном на php
- `make init` - команда из Makefile инициализирующая весь проект как целое.
 для api эта команда вызвывает api-init которая и устанавливает все нужные
 библиотеки-зависимости описанные в `composer.json` файле, после накатывая все
 нужные миграции и фикстуры:

```Makefile
api-init: api-permissions api-composer-install api-wait-db api-migrations api-fixtures
```
- установка прав доступа
- установка зависимостей
- ожидание поднятия бд
- накатка миграций
- накатка фикстур


Для фронтенд принцип такой же. поэтому сделаем тоже самое и для него

Очистка всех ненужных каталогов(build)
```Makefile
frontend-clear:
	docker run --rm -v ${PWD}/frontend:/app -w /app alpine sh -c 'rm -rf build'
```
Например, когда будем запускать команду `yarn build` то из package.json и кода
нашего приложения будет собран каталог `build` c файлами готовыми для деплоя.
поэтому первое что нам нужно - удалять старый билд очищая место.

Установка всех пакетов и зависимостей: из package.json
```Makefile
frontend-yarn-install:
	docker compose run --rm frontend-node-cli yarn install

frontend-init: frontend-yarn-install
```

Дальше "подключаем" новые команды для фронтенда к основной команде `init`
Разбивая длинную строку по смыслу на несколько:

```Makefile
init: docker-down-clear \
  api-clear frontend-clear \
  docker-pull docker-build docker-up \
  api-init frontend-init
```

После этого простой командой `make init` теперь будет развёртываться в локальное
окружение кроме самого api еще и фронтенд


## frontend/public - шаблон для собрки готового проекта
Если теперь при таких настройках просто вызывать make init который в свою очередь
вызовет frontend-yarn-install и соответственно `yarn install` внутри контейнера
с фронтендом, а после зайти локально на заглушку фронтенд сайта - увидим пустую
страницу. Это происходит потому что копируя код из каталога public мы просто
копируем чистый шаблон-заготовку к которому не проиведено подключение js-скриптов
т.е. просто пустая страница без нашего фронтенд приложения на ReactJS

в этом можно убедиться глянув ./frontend/public/index.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="icon" href="%PUBLIC_URL%/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta
      name="description"
      content="Web site created using create-react-app"
    />
    <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />
    <title>React App</title>
  </head>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"></div>
    <!--
      This HTML file is a template.
      If you open it directly in the browser, you will see an empty page.

      You can add webfonts, meta tags, or analytics to this file.
      The build step will place the bundled scripts into the <body> tag.

      To begin the development, run `npm start` or `yarn start`.
      To create a production bundle, use `npm run build` or `yarn build`.
    -->
  </body>
</html>
```
как видим здесь нет вообще никаких подключений файлов извне ни css ни js
О том что эта страница просто шаблон написано и в коментарии в теге body

В этом и отличие от классического подхода.
Раньше мы просто поднимали nginx и указывали файл который ему надо раздавать
Теперь такое не сразботает. Готовые для деплоя файлы будут доступны только
после сборки(yarn build) в каталоге build.

Пробуем посмотреть как собирается проект:

```sh
docker compose run --rm frontend-node-cli yarn build

$ react-scripts build
Creating an optimized production build...
Compiled successfully.

File sizes after gzip:

  46.66 kB  build/static/js/main.7004057d.js
  1.77 kB   build/static/js/787.a4b45000.chunk.js
  513 B     build/static/css/main.f855e6bc.css

The project was built assuming it is hosted at /.
You can control this with the homepage field in your package.json.

The build folder is ready to be deployed.
You may serve it with a static server:

  npm install -g serve
  serve -s build

Find out more about deployment here:

  https://cra.link/deployment

Done in 9.82s.
```

Здесь через пакетный менеджер `yarn` вызывается команда `react-scripts build`
и реазультатом её работы будет создание каталога `build/static/` с css js файлами

-  build/static/js/main.7004057d.js
-  build/static/js/787.a4b45000.chunk.js
-  build/static/css/main.f855e6bc.css

смотрим что внутри каталога build

```
frontend/build/
├── asset-manifest.json
├── favicon.ico
├── index.html               << готовый к работе файл с подключением скриптов
├── logo192.png
├── logo512.png
├── manifest.json
├── robots.txt              << скопирован из каталога-шаблона public
└── static                  << каталог с подключаемыми к index.html файлами
    ├── css
    │   ├── main.f855e6bc.css
    │   └── main.f855e6bc.css.map
    ├── js
    │   ├── 787.a4b45000.chunk.js
    │   ├── 787.a4b45000.chunk.js.map
    │   ├── main.7004057d.js
    │   ├── main.7004057d.js.LICENSE.txt
    │   └── main.7004057d.js.map
    └── media
        └── logo.6ce24c58023cc2f8fd88fe9d219db6c6.svg
```
Теперь понятно почему при работе с React нужно деплоить и отображать не каталог
public который по сути просто пустой шаблон, а каталог build - ведь именно в нём
и содержится всё наше приложение подготовленое для прод-сервера.
И именно по шаблону public/index.html и будет строится результирующий
build/index.html

Таким образом для деплоя рекат-приложения на прод сервера нужно собирать и
копировать каталог build внутрь контейнера с nginx.

А вот для dev-окружения такой подход будет не удобным. работая с API мы делали
прокидывание исходного кода через volume прямо внутрь докер-контейнеров так
что любое измнение кода сразу изменяло поведение приложения.
Здесь же придётся для каждого измнения пересобирать контейнер через `yarn build`

Поэтому предусмотрен dev-сервер который можно будет поднять так же внутри
контейнера через команду `yarn start`
(в package.json в секции `scripts` `start: react-script start`)
и прокинуть порт наружу в локальную машину. Это и позволит иметь локальную
копию фронтенд приложение которое будет изменятся на каждое изменение кода


## Настраиваем Dev и Prod Сервера

Смотрим как у нас была настроена наша заглушка фронтенда

```sh
frontend/docker/
├── common
│   └── nginx
│       └── conf.d
│           └── default.conf
├── development
│   ├── nginx
│   │   └── Dockerfile
│   └── node
│       └── Dockerfile
└── production
    └── nginx
        └── Dockerfile
```

./frontend/docker/common/nginx/conf.d/default.conf
```nginx
server {
  listen 80;
  charset uft-8;
  index index.html;
  root /app/public;

  # deny attackers to open the site in the frames on other sites
  add_header X-Frame-Options "SAMEORIGIN";
}
```
Т.е. вся его работа - просто отображать файл index.html из каталога `/app/public`
внутри контейнера

Теперь же для dev-окружения нужно запускать сервер не через nginx а через
команду `yarn start` Это поднимет сервер на 3000 порту и в принципе его можно
прокинуть на наш 8080 порт и вообще удалить nginx из frontend/docker/development
но недостаток такого подхода в различии конфигураций для dev и для prod
ведь если мы будем локально работать напрямую с nodejs сервером без nginx,
а на проде у нас будет работать nginx то возникают всякие различия.
Например на проде мы сможем вписывать в конфиг nginx-а нужные нам http-заголовки
а в локальном dev-окружении - нет. В общем при таком подходе теряется возможность
тестировать работу полноценной системы сервера на nginx локально,
а затем уже проверив накатывать конфигурацию на прод-сервер.

Поэтому для dev-окружения мы и настроим проксирование запросов через nginx в
запускаемой nodejs серер. Принцип такой же как мы делали для своего `api`
у нас там nginx проксирует(перенаправляет) запросы на `php-fpm` так и здесь
будет nginx сервер проксирующий запросы на nodejs сервер который поднимает
команда `yarn start`

А значит конфиги nginx-а для dev и prod серверов будут отличаться, поэтому
уходим от использования общего конфига из `frontend/docker/common/nginx/conf.d`
размещая конфиги прямо в `frontend/docker/development/nginx/conf.d` и аналогично
для прода.

```sh
mv common/nginx/conf.d/ production/nginx/
cp -r production/nginx/conf.d/ development/nginx/
rm -r common/
```

```sh
./frontend/docker/
├── development
│   ├── nginx
│   │   ├── conf.d
│   │   │   └── default.conf
│   │   └── Dockerfile
│   └── node
│       └── Dockerfile
└── production
    └── nginx
        ├── conf.d
        │   └── default.conf
        └── Dockerfile
```

Для продакшена конфиг мы оставляем такой же как был в common:

./frontend/docker/production/nginx/conf.d/default.conf
```
server {
  listen 80;
  charset uft-8;
  index index.html;
  root /app/public;

  # deny attackers to open the site in the frames on other sites
  add_header X-Frame-Options "SAMEORIGIN";
}
```

А для dev-окружения настроим nginx для проксирования запросов на nodejs сервер

## Конфигурирование nginx + nodejs сервер

Первое что нужно - запустить nodejs сервер через `yarn start`
для этого в нашем dev файле `./docker-compose.yml` рядом с сервисом
`frontend-node-cli` добавить новый сервис(контейнер) в котором и будет идти
`yarn start` с запуском dev-сервера

Для этого копируем блок кода из сервиса `frontend-node-cli` и называем новый
как `frontend-node` в нём у нас и будет происходить запуск dev-сервера
```yaml
  frontend-node:
    build:
      context: frontend/docker/development/node            # (1)
    environment:
      - WDS_SOCKET_PORT=0                                  # (5)
    volumes:
      - ./frontend:/app                                    # (2)
    command: yarn start                                    # (3)
    tty: true                                              # (4)

  frontend-node-cli:
    build:
      context: frontend/docker/development/node
    volumes:
      - ./frontend:/app
```
- (1) И здесь мы используем тот же докер-файл
- (2) Так же монтируем весь каталог ./frontend внутрь докер-образа:
- (3) переопределяем команду с которой должен запускаться этот контейнер(сервис)
      для нас это `yarn start` которая и будет поднимать nodejs сервер
- (4) чтобы запускаемая в интерактивном режиме команда yarn start не завершалась
  сразу, а продолжала работать постоянно. т.е. чтобы сервер оставался работать
  флаг `tty:true` сделает так, чтобы фоновая задача запускаемая чз `yarn start`
  стала основной для докер контейнера и чтобы контейнер не вырубался сразу а
  оставался висеть.
- (5) эта настройка нужна для корректной работы c websocket для dev-окружения

Прописываем проксирование с nginx на nodejs
./frontend/docker/development/nginx/conf.d/default.conf
```nginx
server {
    listen 80;
    charset utf-8;
    index index.html;
    root /app;
    server_tokens off;

    add_header X-Frame-Options "SAMEORIGIN";

    location / {
        proxy_set_header  Host $host;
        proxy_pass        http://frontend-node:3000;
        proxy_redirect    off;
    }
}
```
это минимальный конфиг для работы проксирования с nginx на поднятый через nodejs
dev-сервер, работающий на 3000 порту в отдельном контейнере с dns именем
`frontend-node`

в этом dev-сервере есть способ отправлять браузеру уведомления об обновлении
файлов(страниц сайта, css и js-скриптов.
Это сделано для удобства локальной разработки чтобы на лету обновлялись
файлы фронтенд приложения (без необходимости нажимать F5 в браузере).
Работает это через websocket, поэтому чтобы это работало нужно еще добавить
проксирование запросов на websocket.

Здесь описан конфиг для проксирования websocket запросов
он отличается от проксирования обычных http-запросов
```nginx
    # websocket
    location /ws {
        # old location was /sockjs-node      << такой путь использовался раньше
        proxy_set_header  Host $host;
        proxy_set_header  Upgrade $http_upgrade;
        proxy_set_header  Connection "Upgrade";
        proxy_pass        http://frontend-node:3000;
        proxy_redirect    off;
    }
```

в итоге вся конфигурация:

```nginx
server {
  listen 80;
  charset utf-8;
  index index.html;
  root /app;
  server_tokens off;

  # deny attackers to open the site in the frames on other sites
  add_header X-Frame-Options "SAMEORIGIN";

  # websocket
  location /ws {
      proxy_set_header  Host $host;
      proxy_set_header  Upgrade $http_upgrade;
      proxy_set_header  Connection "Upgrade";
      proxy_pass        http://frontend-node:3000;
      proxy_redirect    off;
  }

  location / {
      proxy_set_header  Host $host;
      proxy_pass        http://frontend-node:3000;
      proxy_redirect    off;
  }
}
```

Но мы у себя в dev-окружении ходим на фронтент через шлюз(gateway)
И для того чтобы проксирование работало и через шлюз нужно прописать
проксирование и в нём.
Если не прописывать то такой конфиг будет работать только при прямом обращении
на этот фроненд-сервер т.е на сервис frontend-node.
Но он у нас вроде как не проброшен в локалку - наружу "торчит" только шлюз.

Просто добавляем location с `/ws` так же как добавили для конфига
выше
./gateway/docker/development/nginx/conf.d/frontend.conf
```nginx
server {
  listen 8080;
  server_tokens off;

  # websocket
  location /ws {
      # oldlocation /sockjs-node
      proxy_set_header  Host $host;
      proxy_set_header  Upgrade $http_upgrade;
      proxy_set_header  Connection "Upgrade";
      proxy_pass        http://frontend;
      proxy_redirect    off;
  }

  location / {
    proxy_set_header  Host $host;
    proxy_set_header  X-Real-IP $remote_addr;
    proxy_set_header  X-Forwarded-Proto http;
    proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header  X-Forwarded-Host $remote_addr;
    proxy_set_header  X-NginX-Proxy true;
    proxy_pass        http://frontend;
    proxy_ssl_session_reuse off;
    proxy_redirect off;
  }
}
```

Но обрати внимание `proxy_pass` указывается без 3000-го порта и не на
сервис с именем `frontend-node` где работает сам dev сервер, а именно на
сервис `frontend`:

      proxy_pass        http://frontend;


теперь можно попробовать перезапустить все сервисы

```sh
docker compose up --build -d
 ✔ Network site_default                Created                       0.4s
 ✔ Container site-api-postgres-1       Started                       4.4s
 ✔ Container site-api-php-cli-1        Started                       4.4s
 ✔ Container site-frontend-1           Started                       4.4s
 ✔ Container site-api-php-fpm-1        Started                       4.4s
 ✔ Container site-frontend-node-1      Started                       4.4s
 ✔ Container site-mailer-1             Started                       4.4s
 ✔ Container site-frontend-node-cli-1  Started                       4.4s
 ✔ Container site-api-1                Started                       0.7s
 ✔ Container site-gateway-1            Started
```

Интереса ради размеры собранных образов можно глянуть так:
```sh
docker images

REPOSITORY                 TAG            IMAGE ID            SIZE
site-api-php-cli           latest         c8982f3ea8e3        105MB
site-gateway               latest         97976faa36fe        21.9MB
site-api                   latest         a0c1e8df44e6        21.9MB
site-api-php-fpm           latest         a734c351eba4        82.7MB
site-frontend              latest         b8d5fbf563ad        21.9MB
site-frontend-node-cli     latest         4926de67dec6        132MB
site-frontend-node         latest         48b243e28d98        132MB
```

Когда все контейнеры-сервисы поднимаутся заходим на localhost:8080
(это адрес нашего локального фронтенда) смотрим исходный код страницы:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta
      name="description"
      content="Web site created using create-react-app"
    />
    <link rel="apple-touch-icon" href="/logo192.png" />
    <link rel="manifest" href="/manifest.json" />
    <title>React App</title>
  <script defer src="/static/js/bundle.js"></script></head>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"></div>
  </body>
</html>
```

Визуально страница тоже как бы пуская есть только
```html
    <div id="root"></div>
```
Но здесь на деле уже идёт подключение собранных js-скриптов:

```html
  <script defer src="/static/js/bundle.js"></script>
```
view-source:http://localhost:8080/static/js/bundle.js


Note: узнать точный адрес по которому работает websocket при неполадках можно
через консоль разработчика в браузере(F12):
```
Firefox can’t establish a connection to the server at ws://localhost:3000/ws.
```
Когда вебсокет не работет во вкладке `Networks` будет отображаться подобное:
(при этом нужно выделить вкладку `WS` внутри вкладки `Networks`)
```
GET localhost:3000 ws bundle.js:36565 (websocket) NS_ERROR_CONNECTION_REFUSED 0B
```

Когда вебсокет исправен будет нечто подобное:

```
101 GET localhost:8080 ws bundle.js:36565 (websocket) plain 181B
```


## Пробуем проверить обновление React приложения(страницы в браузере) в runtime
заходим в исходники и изменяем цвет страницы
./frontend/src/App.css

```css
.App {
  text-align: center;
}

/* ... */

.App-header {
  /* background-color: #282c34; */
  background-color: #ff0000; /* обновляекм и сохраняем новый цвет*/
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
}
/* ... */
```

Как только файл будет сохранён то измнения сразу сами автоматически отобразятся
на странице приложения, без какой либо её перезагрузки.
Другими словами dev-сервер на nodejs через websocket на каждое измнение
исходников сайта фронтенда будет слать уведомления, которое в браузере и будут
обновлять страницу


## Решаем проблему запуска dev-сервера внутри контейнера frontend-node

Если склонировать репозиторий на другой системе и запустить `make init`
для развёртывания локального dev-окружения то
- алиас `init` стриггерит запуск всех зависимостей, билд образов
- пройдёт команда `docker compose up --build up`
- произоёдёт запуск сервиса `frontend-node` при этом сами пакеты из package.json
  скорее всего еще не успеют установится в каталог `node_modules`

Другими словами при первом запуске `make init` сервис frontend-node упадёт с
ошибкой нечто вроде "не могу найти `node_modules`"

это решается добавлением ожидания перед запуском сервиса `frontend-node` чтобы
его запуск стартовал только после полной установке всех нужных зависимостей

Для этого можно сделать так:
- после установки всех зависимостей в каталог ./frontend/ создаётся файл `.ready`
т.е. просто прописать в Makefile создание обычного пустого файла ready.
Чтобы этот файл создавался когда всё будет готово для запуска frontend-node
- прописать к команде yarn start ожидание пока не появится файл `.ready`

1) прописываем задачу по созданию файла `.ready`
./Makefile
```Makefile
frontend-ready:
  docker run --rm -v ${PWD}/frontend:/app -w /app alpine touch .ready
```

А перед запуском проекта удаляем и каталог build и файл `.ready`(если они есть).
И так же добавляем к задаче `frontend-init` в самый конец вызов задачи-команды
`frontend-ready`:

```Makefile
frontend-clear:
	docker run --rm -v ${PWD}/frontend:/app -w /app alpine sh -c 'rm -rf .ready build'

frontend-init: frontend-yarn-install frontend-ready

frontend-yarn-install:
	docker compose run --rm frontend-node-cli yarn install
```

2) добавляем ожидание на появление файла `.ready` перед запуском `frontend-node`
./docker-compose.yml

```yml
  frontend-node:
    build:
      context: frontend/docker/development/node
    environment:
      - WDS_SOCKET_PORT=0
    volumes:
      - ./frontend:/app
    command: sh -c "until [ -f .ready ]; do sleep 1 ; done && yarn start"
    tty: true
```

Эта команда будет запущена внутри поднятого докер-контейнера суть в том чтобы
ждать пока не появится файл `.ready` и только после его появления запускать
`yarn start`
```sh
sh -c "until [ -f .ready ]; do sleep 1 ; done && yarn start"
```
дословно: пока файла `.ready` нет - ждать 1 секунду, как появится запустить yarn

Так же в этот же docker-compose.yml по аналоги с `api: depends_on: - api-php-fpm`
добавим и для фронтенд сервиса с запущенным в нём nginx дерективу-зависимость

было:
```yaml
  frontend:                    # сервис внутри которого поднимается nginx
    build:                     # первоначально раздающий frontend-файлы
      context: frontend/docker
      dockerfile: development/nginx/Dockerfile
    volumes:                    # теперь это не нужно, здесь мы разадвали
      - ./frontend:/app         # статические файлы заглуку html-страницы
```

в dev docker-compose.yml сервис frontend - содержит в себе только nginx
первоначальная задача которого была раздача статических файлов(public/index.html)
через него мы выводили заглушку нашего фронтенд приложения страницу с надписью

теперь у нас в dev-окружении раздача файлов происходит через сервер nodejs
который к тому же еще и websocket открывает для runtime обновления страницы в
браузере. А nginx теперь просто проксирует на него запросы из localhost:8080
напомню что nginx мы оставили для того, чтобы иметь возможность вписывать свои
http-заголовки как нам нужно будет это делать на prod-сервере.
nodejs сервер число для разработки и только - для удобства и скорости.

Обновляем сервис `frontend` добавляя зависимость от `frontend-node`
./docker-compose.yml
```yaml
  frontend:
    build:
      context: frontend/docker
      dockerfile: development/nginx/Dockerfile
    depends_on:
      - frontend-node
```
Такая зависимость(depends_on) будет говорить DockerCompose
- сначала запусти контейнер с frontend-node и только когда он стартанёт тогда
- запускай контейнер `frontend`

Если это не прописать то контейнер-сервис `frontend` стартанёт раньше чем
`frontend-node` и nginx из `frontend` просто не найдёт приложение куда мы
прописали проксировать запросы - и ругнувшить вырубиться отписав ошибку
что "по заданному адресу никого нет" т.е по http://frontend-node:3000
(Здесь frontend-node - это и есть dns-имя сервиса-контейреа котрый еще не успел
стартануть)


Всё готово можно проверить как это работает через `make init`

при запуске make init:
- сначала удалит все запущенные контейнеры из docker-compose.yml если есть такие
- очистит все временные файлы api:var/cache frontend:.ready build
- пересобирёт всё образы из Dockerfile-ов(используя имеющиеся кэши слоёв)
- запустит новые контейнеры

Причем сам контейнер `site-frontend-node-1` будет запущен сразу. Но внутри
контейнера сработает та наша команда на ожидание появления файла `.ready` и
сам nodejs сервер стартанёт только после появляения файла `.ready` что произойдёт
когда установятся все нужные для фронтенда зависимости в node_modules

утилита make продолжает свою работу дальше:
- запускает api-php-cli composer install (для нашего API)
- ждёт поднятия базы данных
- применяет миграции и фикстуры
- стартует установку зависимостей для фронтенда `yarn install`
- yarn install говорит пакеты установлены
- идёт запуск создание файла .ready через  `touch .ready`
- в итоге внутри контейнера `site-frontend-node-1` запускается команда
  yarn start, запускающая dev-сервер через nodejs
- теперь зайдя по адресу localhost:8080 видим сгенерированную фронтенд-страницу
  c логотипом ReactJS и надписью `Edit src/App.js and save to reload.`

Инфраструктура по развёртыванию фронтенд dev-сервера готова.

[make init output](./docs/20-setup-n-deploy-react/logs/02-configured-react-app.log)
