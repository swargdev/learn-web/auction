# Регистрация по соц-сети

Дополняем свой код для регистрации не только по email но и по соц.сети (v15)
реализуем UseCase JoinByNetwork
весь список UseCases-ов [здесь](./docs/12-auth/02-auth-use-cases.md)


Добавляем новые возможности:
- возможность в личном кабинете user-а сменить аккаунт соц-сети на другой
- возможность входить на сайт через аккаунт соц-сети


## Продумываем как должен работать `Join By Network`

Для содания User надо:
- передавать из соц-сети(network-a):
  email user-а, id юзера в этой соцсети, название самой соцсети
- так же нужно будет заполнять уже имеющиеся поля User-а: id, date,
- соц сеть должна предоставлять нашему сайту email юзера

Как и ранее UseCase - у нас это конкретный метод в классе
`App\Auth\Entity\User\User` Поэтому и начинаем с него.

## Отсутстве в php перегрузки методов-конструкторов и как с этим жить

В строго типизированных языках типа Java, C# есть возможность создавать несколько
конструкторов под разные варианты входных параметров(с разными сигнатурами)
в php так не сделать. Т.е. в php для одного класса может быть только один
конструктор поэтому такой финт на пхп не пройдёт:
```php
<?
class User
{
    public function __construct( // 1й конструктор с одним набором параметров
        Id $id,
        DateTimeImmutable $date,
        Email $email,
        string $passwordHash,
        Token $token
    ) {
       //...
    }

    public function __construct( // еще один конструктор в попытке
        Id $id,                  // работы в стиле сторогой типизации
        DateTimeImmutable $date, // и перегрузки конструкторов и методов
        Email $email,            // т.е. как в java - php так не может.
        Network $network,    // <<< новый обьект-значение с данными соцсети.
    ) {
       //...
    }
}
```

В php вместо этого используют подход называемый "именнованные конструкторы"
по сути именнованные конструкторы - это отдельные статические методы-фабрики
Это мы уже реализовывали в своём коде и конкретный тому пример - это
две "фабрики"(статические методы) в классе `App\Auth\Entity\User\Status`:

```php
<?
class Status
{
    private const WAIT = 'wait';
    private const ACTIVE = 'active';

    private string $value;

    private function __construct(string $value)  // конструктор "закрыт" от
    {                                            // внешнего мира - приватный
        $this->value = $value;                   // модификатор доступа
    }

    public static function wait(): Status // эти две фабрики (еще одна ниже)
    {                    //по сути вызывают один и тот же приватный конструктор
        return new Status(self::WAIT);    // заполняющая его разными значениями
    }

    public static function active(): Status // еще одна фабрика
    {
        return new Status(self::ACTIVE);    // создаёт сразу с активным статусом
    }
```
эти статические методы - фабрики по сути искуственные конструкторы.
В подобных случаях конструктор делают приватным - для защиты от случайных ошибок
неверного использования такого класса. Чтобы оставалась возможность только
правильного инстанцирования(создания new) обьектов - т.е. только через фабрики.

По этой же аналогии можно сделать статические методы принимающие разные входные
параметры и передающие их внутрь инстанцируемого класса. Что и сделаем с User


## Реализация
- изменяем конструктор класса User, оставляя в его принимаемых параметрах
  только общие для всех фабрик входные данные. Так же закрываем конструктор
  для доступа из вне меняя public на `private`


```php
<?
class User
{
    private Id $id;
    private DateTimeImmutable $date;
    private Email $email;
    private Status $status;
    // теперь эти два поля могут быть необязательными ?- говорит что поле может
    private ?string $passwordHash = null;              // null-ом
    private ?Token $joinConfirmToken = null;
    // здесь явно присваиваем null-ы для того, чтобы избавиться от ошибок
    // статического анализатора кода(psalm) иначе он будет ругаться
    private ArrayObject $networks; // об этом поле будет ниже

    // переписали конструктор, чтобы он принимал только общие для всех спобов
    // параметры
    private function __construct(
        Id $id, DateTimeImmutable $date, Email $email, Status $status,
        // обрати внимание здесь раньше был хэшированый пароль и токен - убрали
    ) {                                                    // их в фабрику ниже
        $this->id = $id;
        $this->date = $date;
        $this->email = $email;
        $this->status = $status;
    }

    //"именнованный конструктор" - фабрика для создания юзера через email
    public static function requestJoinByEmail( // метод статический User::jo...
        Id $id,
        DateTimeImmutable $date,
        Email $email,
        string $passwordHash,   // эти два параметра вместе с токеном
        Token $token            // теперь задаются через этот метод
    ): User {
        $user = new User($id, $date, $email, Status::wait());
        $this->passwordHash = $passwordHash;
        $this->joinConfirmToken = $token;
        return $user;
    }
```
дальше по аналогии делаем метод joinByNetwork:

```php
<?
class User {
    //...

    public static function joinByNetwork(
        Id $id,
        DateTimeImmutable $date,
        Email $email,
        NetworkIdentity $identity, // новые данные(соцсеть) вместо хэша и токена
    ): User {
        $user = new User($id, $date, $email, Status::active());// активен сразу!
        $user->networks->append($identity);// об этом будет ниже
        return $user;
    }
```


Теперь пишем отдельный хэндлер принимающий команду на регистрацию по соцсети
[JoinByNetwork\Command](./api/src/Auth/Command/JoinByNetwork/Handler.php)
В этой команде и будут передаваться данные о регистрации
```php
<?
namespace App\Auth\Command\JoinByEmail;

class Command
{
    public string $email = '';    // соцсеть полжна предоставлять email
    public string $network = '';  // название самой соцсети
    public string $identity = ''; // идишник юзера в этой соцсети
}
```
Принимать и обрабатывать эту команду будет конкретный обработчик(хэндлер)
Но для того, чтобы хранить инфу о соцсети прежде делаем класс обьекта-значения

[App\Auth\Entity\User\NetworkIdentity](./api/src/Auth/Test/Unit/Entity/User/NetworkIdentityTest.php):

```php
<?
namespace App\Auth\Entity\User;
use Webmozart\Assert\Assert;

class NetworkIdentity
{
    private string $network;
    private string $identity;

    public function __construct(string $network, string $identity)
    {
        Assert::notEmpty($network);         // проверяем что значение не пустые
        Assert::notEmpty($identity);        // иначе кидаем исключение
        $this->network = mb_strtolower($network);    // для надёжности в нижний
        $this->identity = mb_strtolower($identity);  // регистр
    }

    // понадобиться в будущем для проверки на попытку добавить ту же соцсеть
    public function isEqualTo(NetworkIdentity $network): bool
    {
        return
            $this->network === $network->network &&
            $this->identity === $network->identity;
    }

    public function getNetwork(): string
    {
        return $this->network;
    }

    public function getIdentity(): string
    {
        return $this->identity;
    }
}
```
сразу же пишем и тесты для этого обьекта-значения
[NetworkIdentityTest](./api/src/Auth/Test/Unit/Entity/User/NetworkIdentityTest.php)
в нём делаем простейшие проверки:
- возникнет ли исключение при попытке передать пустое значние и
- проверка работы проверки на равенство

Теперь уже можно реализовывать и сам наш UseCase `JoinByNetwork`
то есть хэндлер принимающий команду на регистрацию через соцсеть:

[JoinByNetwork\Handler](./api/src/Auth/Command/JoinByNetwork/Handler.php)
```php
<?
namespace App\Auth\Command\JoinByNetwork;
// use ...
class Handler
{
    private UserRepository $users;
    private Flusher $flusher;

    public function __construct(UserRepository $users, Flusher $flusher)
    {
        $this->users = $users;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        // создаём обьект-значение хранящий данные о пришедшей соцсети
        $identity = new NetworkIdentity($command->network, $command->identity);
        $email = new Email($command->email);

        // проверка в будущем через БД что такого юзера с такой соц сетью еще нет
        if ($this->users->hasByNetwork($identity)) {
            throw new \DomainException('User with this network already exists.');
        }

        // дополнительная проверка на случай если юзер с таким же емейлом уже
        // зареган в системе. - тип зайди через свой email-пароль или другую
        // ранее привязанную соцсеть
        if ($this->users->hasByEmail($email)) {
            throw new \DomainException('User with this email already exists.');
        }

        // проверки прошли можно регать - создаём инстанс Юзера
        $user = User::joinByNetwork(
            Id::generate(),
            new DateTimeImmutable(),
            $email,
            $identity
        );
        // Добавляем в репозиторий
        $this->users->add($user);
        // отправляем коммиты в БД одной транзакцией (о новом юзере)
        $this->flusher->flush();
    }
}
```

В наш интерфейс-заглушку добавляем новый метод hasByNetwork()
его задача - проверка наличия юзера в БД по данным о соцсети,
а точнее поиск конкретного юзера по данным о соцсети и возврат если такой есть.
```php
<?
namespace App\Auth\Entity\User;

interface UserRepository
{
    public function hasByEmail(Email $email): bool;
    public function hasByNetwork(NetworkIdentity $identity): ?User;
    public function findByConfirmToken(string $token): ?User;
    public function add(User $user): void;
}
```
этот метод `hasByNetwork(NetworkIdentity $identity): ?User;`
сейчас мы вводим для проверки что такого юзера еще нет в БД и можно регать.
Но далее этот же метод будет использовать контроллер, чтобы достать по данным о
соцсети юзера из нашей БД и если данные нашлись - залогинить юзера в системе.
А если же контроллер обнаруживает что такого еще нет - то он должен будет
вызвать метод-хэндлер `JoinByNetwork\Handler` и зарегать его как нового.

Дописываем Метод joinByNetwork в классе [User](./api/src/Auth/Entity/User/User.php)

```php
<?
class User
{
    private Id $id;
    // ..
    private ?Token $joinConfirmToken = null;
    private ArrayObject $networks; // - это "список" хрянящий все соцсети
    // ArrayObject - std php массив, дальше будет заменён на доктриновскую коллекцию

    private function __construct(
        Id $id, DateTimeImmutable $date, Email $email, Status $status,
    ) {
        $this->id = $id;
        $this->date = $date;
        $this->email = $email;
        $this->status = $status;
        $this->networks = new ArrayObject(); // <<< создаём коллекцию сразу
    }

    public function joinByNetwork(
        Id $id,
        DateTimeImmutable $date,
        Email $email,
        NetworkIdentity $identity,
    ): User {
        // тот же базовый конструктор, но уже сразу со статусом "активный"
        $user = new User($id, $date, $email, Status::active());
        // активный т.к. не нужно подтверждать себя по email
        $user->networks->append($identity);//добавление в массив нового элемента
        return $user;
    }
```

Далее пишем тест для этого класса User и его нового метода joinByNetwork -
который по сути отдельный UseCase который мы и реализуем.

```php
<?
namespace App\Auth\Test\Unit\Entity\User\User;

/**
 * @covers App\Auth\Entity\User\User;
 */
class JoinByNetworkTest extends TestCase
{
    public function testSuccess(): void
    {
        $user = User::joinByNetwork(
            $id = Id::generate(),
            $date = new DateTimeImmutable(),
            $email = new Email('email@example.com'),
            $network = new NetworkIdentity('vk', '000001')
        );

        self::assertEquals($id, $user->getId());
        self::assertEquals($date, $user->getDate());
        self::assertEquals($email, $user->getEmail());

        self::assertFalse($user->isWait());  // сразу уже активен и не ждёт
        self::assertTrue($user->isActive()); // подтверждения по email
        //здесь проверим работу стандартной php коллекции и возврат эл-та из неё
        self::assertCount(1, $networks = $user->getNetworks());
        // ArrayObject вернул тот же обьект что в него и передавали
        self::assertEquals($network, $networks[0] ?? null);
    }
}
```
Вот реализация геттера `$user->getNetworks()` из класса User:
```php
<?
class User
{
    //...
    /**
     * @return NetworkIdentity[]  - это аннотация в Doc-блоке для статического
     */                                              // анализатора кода(psalm)
    public function getNetworks(): array  // массив-копия а не сам ArrayObject
    {                                     // защита самого поля от модификаций
        /** @var NetworkIdentity[] */
        return $this->networks->getArrayCopy();
    }
}
```
Обрати внимание здесь стоит "защита от дурака"
И возвращается не сама коллекция с данными о соцсетях а её копия.
если бы мы здесь напрямую возвращали бы ArrayObject то некий джун мог бы
вызвать нечто вроде такого:

        $user->getNetworks()->append(...)

Тем самым добавить новые неучтённые данные о соцсети, которые далее могут
быть слиты в БД. Чтобы подобных вещей не происходило и используют подобные вещи.

Дальше исправляем старый код под новые фабрики User-а
в этом момогут
```sh
make test
make analyze
```



