﻿# Настройка NeoVim для отладки PHP кода через Xdebug

- Как это работает. Принципы и используемые технологии:

- Что конкретно надо для установки и настройки отладки через Xdebug в nvim
- Установка расширения vscode-php-debug
- Nvim - Конфиг для nvim-dap и его расширений
- Запуск nvim с передачей значения NVIM_XDEBUG_PATH_LOCAL
- Узнаём как передаётся имя сервера настроенное в PHP_IDE_CONFIG из контейнера

## Как это работает. Принципы и используемые технологии:

- Ныне есть такой стандарт как DAP - Debug-Adapter-Protocol
  основная идея DAP - это введеление слоя абстракции, который позволит разным
  редакторам кода и IDE использовать разные реализации дебаггеров.
  такой же принцип как идея в ооп "интерфейсы и реализующие их классы".
  Есть единый стандарт. И любой желающий может взять его и под него сделать
  своб реализацию отладчика, и далее подключить его через DAP в редактор кода
  умеющий работать с DAP (пример такого редактора кода vscode)

- Для NeoVim есть плагин [nvim-dap](https://github.com/mfussenegger/nvim-dap/):
 `Debug Adapter Protocol client implementation for Neovim`
  реализация DAP клиента для Neovim-a. То есть встраивание в nvim поддержки
  работы со внешними дебаггерами через стандартный протокол `DAP`.
  Этот DAP-client своего рода единая точка управления всеми возможными отладчиками
  для разных Языков Программирования(ЯП), которые можно установить как отдельные
  программы и подключив к nvim использовать для отладки кода прямо внутри nvim.

- Собственно сам отладчик, умеющий общаться по стандартному протоколу DAP,
  технически это отдельная внешняя программа, которую нужно запустить и настроить.
  Связь с этой программой(отладчиком) устанавливается через DAP-client встроенный
  внутрь редактора кода(nvim)

- Для VSCode написано множество расширений, в том числе и отладчики реализующие
  DAP протокол. Большинство этих расширений - это программы, которые можно
  запустить просто из командной строки как отдельный независимый процесс.
  Таким образом можно использовать функционал этих vscode-овских расширений
  внутри nvim-a через плагин `nvim-dap` (DAP-client встраиваемый в nvim)

- Основая трудность подключения нового функционала в nvim - конфигурация нужных
  вещей.


## Что конкретно надо для установки и настройки отладки через Xdebug в nvim

- vscode-extension (расширение) отладчик умеющий работать с Xdebug
  [PHP Debug Adapter for Visual Studio Code](https://github.com/xdebug/vscode-php-debug)
  похоже написан теми же кто создал и сам [Xdebug](https://github.com/xdebug/xdebug)

- плагин для Neovim [nvim-dap] и настройка интеграции с `vscode-php-debug`
  https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation#PHP

- Есть Альтернативный способ(узнал о нём позже)
    - сам xdebug работает по протоколу [DBGp](https://xdebug.org/docs/dbgp)
    - для этого протокола есть [vim-овский плагин](https://github.com/vim-vdebug/vdebug)



## Установка расширения vscode-php-debug

Есть несколько способов его установки.

1) Ручной через консоль прямо из репозитория с исходниками
```sh
git clone https://github.com/xdebug/vscode-php-debug.git
cd vscode-php-debug
npm install && npm run build
```
Такая установка самого дебаггера(написан на typescript(js)) со всеми его
зависимостями каталог `node_modules` займёт  ~200Mb

2) Установка через nvim-плагин [mason.nvim](https://github.com/williamboman/mason.nvim)

`mason.nvim` - Portable package manager for Neovim that runs everywhere Neovim runs.
Easily install and manage LSP servers, DAP servers, linters, and formatters.

По простому mason.nvim - это своего рода пакетный менеджер для удобной установки
доп "пакетов" всяких LSP, DAP-серверов, линтеров и прог по форматированияю кода.
Все пакеты этот плагин устанавливает сюда:

    ~/.local/share/nvim/mason/packages

Установка расширения `vscode-php-debug` через него займёт 7.4M
(значит походу `node_modules` используется единый для всей системы?)
каталог будет называется так: `php-debug-adapter`


Для установки внутри nvim: вызываешь команду `:Mason` нажимаешь `3` для перехода
в секцию `DAP`, далее поиском через `/php` находишь php-debug-adapter.
Чтобы убедиться что это именно то, что нужно выбери его в окне и нажми `enter`
При этом отобразятся все детали по этому "пакету":
```
  ◍ php-debug-adapter (keywords: php)
    PHP Debug Adapter 🐞⛔.

    installed version v1.33.0
    homepage          https://github.com/xdebug/vscode-php-debug
    languages         PHP
    categories        DAP
    executables       php-debug-adapter
```
Для установки достаточно нажать клавишу `i` - install
для последующих обновлений `u` - update

После установки так же добавится символическая ссылка к исполняемому файлу:
```sh
cd ~/.local/share/nvim/mason
ls -l bin/
# new
php-debug-adapter -> ~/.local/share/nvim/mason/packages/php-debug-adapter/php-debug-adapter
# already installed
phpactor -> ~/.local/share/nvim/mason/packages/phpactor/bin/phpactor
phpcbf -> ~/.local/share/nvim/mason/packages/phpcbf/phpcbf
php-cs-fixer -> ~/.local/share/nvim/mason/packages/php-cs-fixer/php-cs-fixer
```
(первый в списке остальные 3 - это другие полезные для работы с php пакеты)


## Nvim - Конфиг для nvim-dap и его расширений

```lua
return packer.startup(function(use)
  -- ...

  -- DAP
  use { "mfussenegger/nvim-dap" }        -- сам DAP-Client
  use { "rcarriga/nvim-dap-ui",          -- удобный UI для nvim-dap
    requires = {"mfussenegger/nvim-dap"}
  }
  use { "theHamsta/nvim-dap-virtual-text" } -- TODO config
  -- use { "nvim-telescope/telescope-dap.nvim" }

  -- ...
end
```
Доступные nvim-dap плагина команды:

- DapContinue          - если режим отладки еще не начат - запусткает его
- DapLoadLaunchJSON    - грузит настройки из `./.vscode/launch.json`
- DapRestartFrame
- DapSetLogLevel
- DapShowLog           - показать лог самого DAP-Client-a
- DapStepInto
- DapStepOver
- DapStepOut
- DapTerminate
- DapToggleBreakpoint



If you have not configured Xdebug, read Installation at
[vscode-php-debug](https://github.com/xdebug/vscode-php-debug#installation).

Add the adapter configuration:
```lua
dap.adapters.php = {
  type = 'executable',
  command = 'node',
  args = { '/path/to/vscode-php-debug/out/phpDebug.js' }
}

dap.configurations.php = {
  {
    type = 'php',
    request = 'launch',
    name = 'Listen for Xdebug',
    port = 9003
  }
}
```

Это настройка самого DAP-Client-а(`nvim-dap`) для совместной работы с
расширением `vscode-php-debug`:
~/.config/nvim/lua/user/dap/vscode-php-debug.lua
```lua
local dap_status_ok, dap = pcall(require, "dap")
if not dap_status_ok then return end

local mason_path = vim.fn.glob(vim.fn.stdpath("data")) .. "/mason/"
local php_dap_path = mason_path .. "packages/php-debug-adapter/"
local home = os.getenv("HOME")

dap.adapters.php = {
  type = 'executable',
  command = 'node',
  args = { php_dap_path .. '/extension/out/phpDebug.js' }
}

local xdebug_port        = os.getenv('NVIM_XDEBUG_PORT') or 9003
local xdebug_path_server = os.getenv('NVIM_XDEBUG_PATH_SERVER') or '/app/'
local xdebug_path_local  = os.getenv('NVIM_XDEBUG_PATH_LOCAL') or vim.fn.getcwd()

dap.configurations.php   = {
  {
    name = 'Start Listening for Xdebug', -- php-fpm
    type = 'php',
    request = 'launch',
    port = xdebug_port,

    localSourceRoot = xdebug_path_local,

    pathMappings = xdebug_path_local and xdebug_path_server
        --       in container     -> in local machine (project with sources)
        and { [xdebug_path_server] = xdebug_path_local, } or nil,
    -- pathMappings = {
    --   -- ['/app/'] = "${workspaceFolder}",
    -- }
  },
}
```

Пример еще одной конфигурации dap для php:
```lua
dap.configurations.php = {
    {
        type = 'php',
        request = 'launch',
        name = 'Listen for xdebug',
        port = '9003',
        log = true,
        serverSourceRoot = '/srv/www/',                      -- << Path mapping?
        localSourceRoot = '/home/user/dev/auction/api',
    },
}
```


## Запуск nvim с передачей значения NVIM_XDEBUG_PATH_LOCAL
Для запуска чтобы локальным корневым каталогом кода была директория `api`, а
не текущий рабочий каталог(по умолчанию берётся getcwd() запускай так:
```sh
NVIM_XDEBUG_PATH_LOCAL="$(pwd)/api" nvim api/public/index.php
```


Подробное описание поддерживаемых опций конфигураций расширения vscode-php-debug
через launch.json (для vscode)

`:DapLoadLaunchJSON` - TODO проверить как оно работает

Пример файла .vscode/launch.json:
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Start Listening for debugging",
            "type": "php",
            "request": "launch",
            "port": 9003,
            "pathMappings": {
                "/app/": "${workspaceFolder}"
            }
        }
    ]
}
```

Supported configuration options for PHP can be found under Supported
`launch.json settings` at the
[vscode-php-debug repo](https://github.com/xdebug/vscode-php-debug#supported-launchjson-settings).



## Update Makefile

Для отладки в связке `nvim + nvim-dap + vscode-php-debug + xdebug` нужно
настраивать правильный маппинг для путей к исходникам внутри проекта и внутри
докер-контейнера. На данный момент захардкодил корневой путь к исходникам
внутри докер-образа на `/app/` а полный путь к исходникам в локальной машине
передаётся в nvim через переменную окружения `NVIM_XDEBUG_PATH_LOCAL`
[Смотри настройку nvim](./docs/08-install-xdebug/06-nvim.md)
Поэтому для удобства сделал временные алиасы в Makefile для правильного запуска
подпроекта n

На данный момент для создания `Path mappings в nvim Добавил
Для отладки в связке `nvim + nvim-dap + vscode-php-debug + xdebug`
можно запускать nvim передавая ему полный путь к исходникам на машине

```sh
	NVIM_XDEBUG_PATH_LOCAL="$(pwd)/api" nvim api/public/index.php
```
Либо прописать алиасом в Makefile
```Makefile
# Launch nvim with right `Path mappings` for xdebug
n-api:
	NVIM_XDEBUG_PATH_LOCAL="$(shell pwd)/api" nvim api/public/index.php
```

На данный момент для запуска отладки использую возможность nvim-dap читать
VSCode-вские конфиг файлы `./.vscode/launch.json`.
Подтягивает он их от текущего каталога из которого запущен nvim
`:DapLoadLaunchJSON` Команда явно перечитать такой файл для проекта
(бывает само может прочесть) После этого описанные в этом файле настройки
отобразятся при включение прослушки(Start Listening) т.е. по команде
`:DapContinue` когда нет ни одной начатой дебаг-сессии.


Алиасы для запуска отладки как для php-fpm(web API) так и для php-cli скрипта

```Makefile
# Trigger debug requests for php-fpm (API)
debug-api:
	curl --cookie "XDEBUG_TRIGGER=1;" localhost:8081

# Trigger debug requests for php-cli
# Usage example: make debug-api-cli cmd=hello [arg1=name]
debug-api-cli:
	docker compose run --rm -e XDEBUG_TRIGGER=1 api-php-cli composer app $(cmd) $(arg1)
```

Посмотреть лог самого xdebug внутри контейнера api-php-fpm
```Makefile
debug-xdebug-logs:
	docker compose exec api-php-fpm tail -n 16 /tmp/xdebug.log
```

## Заметки

## Узнаём как передаётся имя сервера настроенное в PHP_IDE_CONFIG из контейнера

grep по логу xdebug-a [Лог Xdebug See](./docs/08-install-xdebug/07-work-with-xdebug.md)

Имя сервера настроенное в docker-compose.yml передаваемое через EnvVar
`PHP_IDE_CONFIG` передаётся в массиве $_SERVER как и все отстальные EnvVar-ы

```
[12] [Step Debug] <- context_get -i 27 -d 0 -c 1
[12] [Step Debug] ->
<response xmlns="urn:debugger_protocol_v1"
xmlns:xdebug="https://xdebug.org/dbgp/xdebug"
command="context_get"
transaction_id="27"
context="1">

<property name="PHP_IDE_CONFIG"
  fullname="$_SERVER[&quot;PHP_IDE_CONFIG&quot;]"         <<<<<<<<<<<<<
  type="string" size="14" encoding="base64">
  <![CDATA[c2VydmVyTmFtZT1BUEk=]]>                        << "serverName=API"
</property>
```

Насколько я понял настройка `PHP_IDE_CONFIG` и передача через EnvVar имени
сервера (`serverName=API`) делаем это в `docker-compose.yml` используется
только в PhpStorm, a в VSCode конфигах подобного функционала для launch.json нет.
(Либо просто я его еще не нашел)
Видимо в VSCode просто "раскидывают" файлы ./vscode/launch.json в директории
подпроектов (api, frontend) и VSCode сам их как-то подтягивает и прочитывает.

В PhpStorm получается мы прописываем отдельные настройки для нами же названных
серверов. и когда приходит первый ответ от Xdebug в PhpStorm то IDE вытягивает
из массива $_SERVER значение EnvVar PHP_IDE_CONFIG и маппит его к настроеному
нами же `Path Mappings` для этого сервера(serverName).
А в VSCode pathMappings просто можно раскидать для разных настроек поднятия
дебаг сессии.

Подобное для nvim можно сделал через `:DapLoadLaunchJSON` + .vscode/launch.json
Просто в момент включения "прослушки"(Start Listening) можно выбрать
одну из многих настроек прослушивания, и для каждого сервера описываем свою
настройку(маппинг исходников проект-контейнер)

./.vscode/launch.json:
```lua
dap.configurations.php = {
  {
    name = 'Start Listening for Xdebug(API)',
    // ...                             ^^^          vvv
    pathMappings = { ['/app/'] = "~/dev/php/auction/api" },
  },
  {
    name = 'Start Listening for Xdebug (Frontend)',
    // ...                              ^^^^^^^^    vvvvvvv
    pathMappings = { ['/app/'] = "~/dev/php/auction/frontend" },
  },
```
Возможные настройки для этого файла смотри здесь
[Документация vscode](https://code.visualstudio.com/docs/editor/debugging)
[Расширение vscode для отладки php]https://github.com/xdebug/vscode-php-debug



## Links:

https://github.com/mfussenegger/nvim-dap/
https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation#PHP

https://github.com/xdebug/vscode-php-debug
https://github.com/xdebug/xdebug

https://xdebug.org/docs/dbgp
https://github.com/vim-vdebug/vdebug

More general information on debugging with VS Code:
https://code.visualstudio.com/docs/editor/debugging
https://code.visualstudio.com/docs/editor/debugging#_launchjson-attributes
https://code.visualstudio.com/docs/editor/debugging#_multitarget-debugging
