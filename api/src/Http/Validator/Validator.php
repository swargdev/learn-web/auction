<?php

declare(strict_types=1);

namespace App\Http\Validator;

use App\Http\Validator\ValidationException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 *
 * 11-12-2023
 * @author Swarg
 */
class Validator
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate(object $object): void
    {
        $violations = $this->validator->validate($object);
        if ($violations->count() > 0) {
            throw new ValidationException($violations);
        }
    }
}
