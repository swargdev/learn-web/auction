Почему выбрали `yarn` в качестве пакетного менеджера? Чем плох `npm`?
2023-02-07

Лично встретил проблему, что на компьютере с Ubuntu+Docker и ноутбуке с
MacOS+Docker в том же проекте `NPM` генерировал разные несовместимые
`package-lock.json` файлы.
В нём команда `npm install` может в любое время изменять этот файл.
А для запуска на ссборочных серверах авторы сделали отдельную команду `npm ci`.

С `Yarn` таких неожиданностей нет.
Там `yarn.lock` работает идеально как `composer.lock`


## YARN warning "has unmet peer dependency". What do with this?

A peer dependency should be installed by yourself.
Usually, the purpose is to prevent version conflicts.

So, when you read a message such as:

`warning "react-scripts > eslint-config-react-app > eslint-plugin-flowtype@8.0.3"
has unmet peer dependency "@babel/plugin-syntax-flow@^7.14.5".`

It simply means that the dependency eslint-plugin-flowtype that you are
(indirectly) using (through react-scripts) requires that you add
`@babel/plugin-syntax-flow` also as dependency in your package.json.

