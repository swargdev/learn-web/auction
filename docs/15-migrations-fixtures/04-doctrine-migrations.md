## Doctrine-Migrations

Содержание:
- Как использовать и запускать Doctrine-Migrations в нашем коде                 :49
- Простейший способ прокинуть команды из Doctrine-Migrations в свой bin/app.php :88
- Краткий обзор команд Doctrine-Migrations какие есть и для чего нужны          :150
- Проброс EntityManager-а через DependencyFactory                               :176
- Как настраивать миграции                                                      :258
- Более продвинутый способ настройки миграций прямо через php-код               :293
- Настройка миграций для нашего проекта (DependencyFactory)                     :368
- Пример настройка миграций с HelperSet для старых версий библиотеки            :454
- Разделение команд Doctrine-Migrations для разных окружений(dev|prod)          :521
- run migrations:diff - создание первой миграции изучаем сгенерированный код    :694
- Коментарии кастомных типов сохраняемые в саму БД. что это и зачем             :790
- CREATE SCHEMA public исправление бага (06-fix-public-bug)                     :859
- Накатываем сгенерированную миграцию migrations:migrate                        :872
- изучаем состояние бд после накатывание миграции                               :932
- 07-migrations-dev-cmds Настройка Makefile под новые команды (+ wait4open)
- 08-migrations-prod-cmds
- 09-summary-orm+migrations
- 10-fixtures.md

https://www.doctrine-project.org/projects/doctrine-migrations/en/3.7/index.html

https://groups.google.com/group/doctrine-user
https://www.doctrine-project.org/slack
https://github.com/doctrine/migrations/issues
https://stackoverflow.com/questions/tagged/doctrine-migrations

https://www.doctrine-project.org/projects/doctrine-migrations/en/3.7/reference/introduction.html

The `Doctrine Migrations` project offers additional functionality on top of the
`DBAL` and `ORM` for versioning your database schema.
It makes it easy and safe to deploy changes to it
in a way that can be reviewed and tested before being deployed to production

Билб-ка `Doctrine-Migrations` позволяет:
- писать миграции с любыми своими нужными SQL-запросами
- интегрируется с самой Биб-кой Doctrine, позволяя автоматически генерить
  миграции на основе прописанного маппинга сущностей.
  Другими словами писать миграции руками будет не надо.

Подключение через композер:

```sh
composer require "doctrine/migrations"

docker compose run --rm api-php-cli composer require "doctrine/migrations"
```
[log](./docs/15-migrations-fixtures/log/doctrine-migrations.log)

на 12.2023 по умолчанию ставится версия 3.7



## Как использовать и запускать Doctrine-Migrations в нашем коде

https://www.doctrine-project.org/projects/doctrine-migrations/en/3.7/reference/configuration.html#advanced

Способы использования Doctrine-Migrations:
- так же как подключали саму библиотеку Doctrine-ORM

Для работы нужен EntityManager или его обьект Connection, для того чтобы
доктрин-мигратионс могла работать с нашей БД

и это можно сделать создав в корне проекта файл cli-cinfig.php И в нём
возвращать наш EntityManager из нашего DI-контейнера

```php
<?php

require 'vendor/autoload.php';

use Doctrine\DBAL\DriverManager;
use Doctrine\Migrations\Configuration\Connection\ExistingConnection;
use Doctrine\Migrations\Configuration\Migration\PhpFile;
use Doctrine\Migrations\DependencyFactory;

$config = new PhpFile('migrations.php'); // Or use one of the
// Doctrine\Migrations\Configuration\Configuration\* loaders

$conn = DriverManager::getConnection(
  ['driver' => 'pdo_sqlite', 'memory' => true]
);

return DependencyFactory::fromConnection(
  $config, new ExistingConnection($conn)
);
```

а можно просто в свой bin/app.php добавить доктриноские команды из пакета
migrations к своим командам в наше cli-приложение

## Простейший способ прокинуть команды из Doctrine-Migrations в свой bin/app.php

В простейшем случае это можно сделать через ConsoleRunner вот так:

./api/bin/app.php

```php
<?
$cli = new \Symfony\Component\Console\Application('Console');

$commands = $container->get('config')['console']['commands'];

/** @var Doctrine\ORM\EntityManagerInterface $entityManager */
$entityManager = $container->get(Doctrine\ORM\EntityManagerInterface::class);

// здесь передаём только инстанс своего приложения. смотри ниже
Doctrine\Migrations\Tools\Console\ConsoleRunner::addCommands($cli)


```

Смотрим Исходник что ConsoleRunner::addCommands добавляет:
[ConsoleRunner](/api/vendor/doctrine/migrations/lib/Doctrine/Migrations/Tools/Console/ConsoleRunner.php)
```php
<?
namespace Doctrine\Migrations\Tools\Console;

class ConsoleRunner {
    //...
    public static function addCommands(
        Application $cli,
        DependencyFactory|null $dependencyFactory = null // для передачи $em
    ): void {
        // здесь идёт стандартная регистрация команд для симвони-cli:

        // для этой группы команд не нужен EntityManager только Doctrine-DBAL
        $cli->addCommands([
            new CurrentCommand($dependencyFactory),
            new DumpSchemaCommand($dependencyFactory),
            new ExecuteCommand($dependencyFactory),
            new GenerateCommand($dependencyFactory),
            new LatestCommand($dependencyFactory),
            new MigrateCommand($dependencyFactory), // нужная нам команда
            new RollupCommand($dependencyFactory),
            new StatusCommand($dependencyFactory),
            new VersionCommand($dependencyFactory),
            new UpToDateCommand($dependencyFactory),
            new SyncMetadataCommand($dependencyFactory),
            new ListCommand($dependencyFactory),
        ]);
        // т.е. эти команды (выше) будут работать и без Doctrine-ORM(без $em)

        // это стопор дальше не идти когда нет EntityManager обёрнутого в фабрику
        if ($dependencyFactory === null || ! $dependencyFactory->hasSchemaProvider()) {
            return;
        }
        // эта команда зарегается только если есть EntityManager
        // т.е. когда к проекту подключен Doctrine-ORM
        $cli->add(new DiffCommand($dependencyFactory));
    }

```

## Краткий обзор команд Doctrine-Migrations какие есть и для чего нужны

- MigrateCommand - накатывать новые еще не применённые миграции к БД
- ExecuteCommand - поштучный запуск одной конкретной миграции (накат/откат)
  например через неё можно откатить последнюю накатанную миграцию.
  для этого передаём какую миграцию надо "исполнить" и флаг down - откатить.

другие команды тоже полезны, но не так часто применимы
статус - какие миграции есть, версии, какие еще не применились и проч.

Стоит заметить что здесь идёт разбиение команд на две группы до условия
`dependencyFactory` и после.
1я группа - универсальные команды - умеют работать напрямую через Doctrine-DBAL
(т.е через компонент подключения к бд по сути обёрткой над обычным хпхшном PDO)
а это значит миграции можно будет использовать в любом проекте где подключен
DBAL и даже без подключения Doctrine-ORM
Когда же мы подключаем `Doctrine-ORM` а значит у нас есть EntityManager
и тогда зарегается еще одна команда `DiffCommand`

`DiffCommand` - команда позволяющая сгенерировать миграции на основе разницы
маппинга сущностей (описанных нами в коде аттрибутами-аннотациями) и текущим,
реальным состоянием таблиц в БД.
Принцип работы тот же что у команды `orm:validate-schema` - через сравнение
схемы сущностей и схемы реальной таблицы БД.

## Проброс EntityManager-а через DependencyFactory

Для того чтобы команда DiffCommand стала доступна нужно добавить вот такой код:
(нужно пробросить EntityManager и конфиг миграций)

```php
<?
// ...
/** @var Doctrine\ORM\EntityManagerInterface $entityManager */
$entityManager = $container->get(Doctrine\ORM\EntityManagerInterface::class);

// здесь создаём конфигурацию прямо из кода, а не из файла как рекомендуют в доках
// об этом еще будет ниже
$conf = new Doctrine\Migrations\Configuration\Configuration();
$conf->addMigrationsDirectory(
  'App\Data\Migration', __DIR__ . '/../../src/Data/Migration'
);
$conf->setAllOrNothing(true);
$conf->setCheckDatabasePlatform(false); // зачем это будет ниже

$storageConf = new Doctrine\Migrations\Metadata\Storage\TableMetadataStorageConfiguration();
$storageConf->setTableName('migrations');

$conf->setMetadataStorageConfiguration($storageConf);

// создаём обёртку с зависимостями(dep) для передачи в Doctrine-Migrations
$dep = Doctrine\Migrations\DependencyFactory::fromEntityManager(
    new Doctrine\Migrations\Configuration\Migration\ExistingConfiguration($conf),
    new Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager($entityManager)
);

Doctrine\Migrations\Tools\Console\ConsoleRunner::addCommands($cli, $dep);
```
теперь команда DiffCommand успешно зарегестрируется и станет доступна:
проверяем запуская наше консольное приложение

```sh
docker compose run --rm api-php-cli composer app
```


```
Available commands:
  ...

 migrations
  migrations:current        Outputs the current version
  migrations:diff       <<  Generate a migration by comparing your current
                            database to your mapping information.

  migrations:dump-schema    Dump the schema for your database to a migration.
  migrations:execute        Execute one or more migration versions up or down
                            manually.
  migrations:generate       Generate a blank migration class.
  migrations:latest         Outputs the latest version
  migrations:list           Dissplay a list of all available migrations and
                            their status.
  migrations:migrate        Execute a migration to a specified version or the
                            latest available version.
  migrations:rollup         Rollup migrations by deleting all tracked versions
                            and insert the one version that exists.
  migrations:status         View the status of a set of migrations.
  migrations:up-to-date     Tells you if your schema is up-to-date.
  migrations:version        Manually add and delete migration versions from the
                            version table.

  migrations:sync-metadata-storage
                            Ensures that the metadata storage is at the latest version.

 orm
  ...
```

важный момент здесь в том, что есть команда migrations:diff,
ведь она появляется только при успешно переданном в пакет `Doctrine-Migrations`
EntityManager-е. А именно она нам на данный момент и нужна, чтобы сгенерить
миграцию по созданию таблиц БД



## Как настраивать миграции
настройка миграций под наш проект можно делать через файл `migrations.php`
в нём идёт описание того как должна идти генерация миграций и где их хранить


Пример из документации настройки через `migrations.php`
https://www.doctrine-project.org/projects/doctrine-migrations/en/3.7/reference/configuration.html#migrations-configuration
```php
<?php

return [
    'table_storage' => [
        'table_name' => 'doctrine_migration_versions', // название таблицы в БД
         // в этой таблице будут хранится уже накаченные(применённые) миграции
        'version_column_name' => 'version',
        'version_column_length' => 191,
        'executed_at_column_name' => 'executed_at',
        'execution_time_column_name' => 'execution_time',
    ],

    'migrations_paths' => [
        // namespace под который сохранять миграции -- и каталог где их хранить
        'MyProject\Migrations' => '/data/doctrine/migrations/lib/MyProject/Migrations',
        'MyProject\Component\Migrations' => './Component/MyProject/Migrations',
    ],

    'all_or_nothing' => true,
    'transactional' => true,
    'check_database_platform' => true,
    'organize_migrations' => 'none',
    'connection' => null,
    'em' => null,
];
```

### Более продвинутый способ настройки миграций прямо через php-код
https://www.doctrine-project.org/projects/doctrine-migrations/en/3.7/reference/custom-configuration.html

Для этого нужно создать обект `Doctrine\Migrations\Configuration\Configuration`
и настроить его. Пример из документации как это делается:

```php
#!/usr/bin/env php
<?php

require_once __DIR__.'/vendor/autoload.php';

use Doctrine\DBAL\DriverManager;
use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Configuration\Connection\ExistingConnection;
use Doctrine\Migrations\Configuration\Migration\ExistingConfiguration;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\Migrations\Metadata\Storage\TableMetadataStorageConfiguration;
use Doctrine\Migrations\Tools\Console\Command;
use Symfony\Component\Console\Application;

$dbParams = [
    'dbname' => 'migrations_docs_example',
    'user' => 'root',
    'password' => '',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
];

$connection = DriverManager::getConnection($dbParams);

$configuration = new Configuration($connection);

$configuration->addMigrationsDirectory(
  'MyProject\Migrations', // namespace
  '/data/doctrine/migrations-docs-example/lib/MyProject/Migrations'
);
$configuration->setAllOrNothing(true);
$configuration->setCheckDatabasePlatform(false);

$storageConfiguration = new TableMetadataStorageConfiguration();
// название таблицы в нашей бд где будут хранится применённые миграции
$storageConfiguration->setTableName('doctrine_migration_versions');

$configuration->setMetadataStorageConfiguration($storageConfiguration);

// это обёртка для зависимостей которые нужно передать в обработчики команд
$dependencyFactory = DependencyFactory::fromConnection(
    new ExistingConfiguration($configuration),
    new ExistingConnection($connection)
);

$cli = new Application('Doctrine Migrations');
$cli->setCatchExceptions(true);

$cli->addCommands(array(
  // стандартная регистрация всех нужных нам доктриновских команд
  // все они идут из пакета Doctrine\Migrations\Tools\Console\Command (см use)
    new Command\CurrentCommand($dependencyFactory),
    new Command\DiffCommand($dependencyFactory),
    new Command\DumpSchemaCommand($dependencyFactory),
    new Command\ExecuteCommand($dependencyFactory),
    new Command\GenerateCommand($dependencyFactory),
    new Command\LatestCommand($dependencyFactory),
    new Command\ListCommand($dependencyFactory),
    new Command\MigrateCommand($dependencyFactory),
    new Command\RollupCommand($dependencyFactory),
    new Command\StatusCommand($dependencyFactory),
    new Command\SyncMetadataCommand($dependencyFactory),
    new Command\UpToDateCommand($dependencyFactory),
    new Command\VersionCommand($dependencyFactory),
));

$cli->run();
```

## Настройка миграций для нашего проекта

- настраиваем DI-контейнер и DependencyFactory

Именно такую продвинутую настройку мы и будем использовать в своём коде.


Создаём каталог в котором будем хранить наши миграции
./api/src/Data/Migration/
этот путь прописывается через `addMigrationsDirectory`



```php
<?
// ...
/** @var Doctrine\ORM\EntityManagerInterface $entityManager */
$entityManager = $container->get(Doctrine\ORM\EntityManagerInterface::class);

// здесь создаём конфигурацию прямо из кода, а не из файла как рекомендуют в доках
// об этом еще будет ниже
$conf = new Doctrine\Migrations\Configuration\Configuration();
$conf->addMigrationsDirectory(
  'App\Data\Migration', // namespace Для генерации классов наших миграций
  __DIR__ . '/../../src/Data/Migration'
);
$conf->setAllOrNothing(true); // флаг "всё или ничего" оч важен для прода
$conf->setCheckDatabasePlatform(false);

$storageConf = new Doctrine\Migrations\Metadata\Storage\TableMetadataStorageConfiguration();
$storageConf->setTableName('migrations');// название таблицы в БД под миграции

$conf->setMetadataStorageConfiguration($storageConf);

// создаём обёртку с зависимостями(dep) для передачи в Doctrine-Migrations
$deps = Doctrine\Migrations\DependencyFactory::fromEntityManager(
    new Doctrine\Migrations\Configuration\Migration\ExistingConfiguration($conf),
    new Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager($entityManager)
);

// добавление новых команд в наше консольное приложение $cli
// с передачей зависимостей $deps
Doctrine\Migrations\Tools\Console\ConsoleRunner::addCommands($cli, $deps);
```


- `setAllOrNothing` == true накатывать миграции как транзакции
либо всю целиком либо не накатывать вообще ничего.
Этот флаг походит не для всех СУБД. Так как не все из них поддерживают работу
с изменением самих схем таблиц бд в виде транзакций(одной неделимой операции)

И вот именно здесь проявляется преимущество использования в нашем проекте
СУБД PostgreSQL. Так как эта бд полностью поддерживает такой режим работы.
В той же MySQL такого нет, т.к. там транзакции работают только над
операциями по работае с данными типа INSERT, UPDATE и проч. и в MySQL нельзя
работать с запросами на изменение таблиц(миграции) как с транзакциями:
т.е через такие операкии как CREATE_TABLE ALTER_TABLE DROP_TABLE и т.д.
 А вот PostgreSQL изначально поддерживать транзакции даже для запросов по
изменению самих таблиц в БД, а не только данных в них.

`setAllOrNothing == true` - флаг гарантирует надёжное накатывание измнений.
Т.е. позволяет исключить ситуации поломок и рассинхронизаций,
когда часть миграции накатилась а часть нет. То есть когда часть операций из
одной миграции(измнении таблиц в БД) выполнилась а другая часть упала с ошибкой
и бд повисла в промежуточном поломанном состоянии)
То есть отсутствие гарантий выполнения миграций(измнения таблиц в БД) оставляет
возможным ситуацию поломоки баз данных. В том числе в и проде на рабочих
серверах. Что само собой крайне не желательно. т.к исправить такую ситуацию
можно будет только руками, через прямой достук к БД заходя на сервер где она
стоит.То есть придётся чинить бд и убирать наполовину накатанную миграцию
(часть операций из одной миграции успевших примениться до некой ошибки)
И делать придётся это руками наепример через SQL-запросы т.к. автоматически
сделать это через миграции будет невозможно. т.к. СУБД будет в промежуточном
состоянии и откат миграции полностью недокаченной просто не пройдёт


Дальше создаём обёртку над зависимоятями класс `DependencyFactory`
для того чтобы прокинуть и конфиг с нашими настройками и EntityManager внутрь
команд доктрин-миграций.

```php
<?
$dep = Doctrine\Migrations\DependencyFactory::fromEntityManager(...)
```
./api/vendor/doctrine/migrations/lib/Doctrine/Migrations/DependencyFactory.php


## Пример настройка миграций с HelperSet для старых версий библиотеки

Привожу это чисто для сравнения. если вдруг придётся где-то редачить легаси-код

То есть здесь делается примерно тоже самое - проброс зависимостей(конфига и em
т.е. по сути настроек для работы с Doctrine-ORM.
```php
<?
$commands = $container->get('config')['console']['commands'];

$em = $container->get(EntityManagerInterface::class);
$connection = $em->getConnection();

$configuration = new Configuration($connection) ;
$configuration->setMigrationsDirectory(_DIR_ . '/../src/Data/Migration');
$configuration->setMigrationsNamespace('App\Data\Migration');
$configuration->setMigrationsTableName('migrations');
$configuration->setALLOrNothing (true) ;

$cli->getHelperSet()->set(new EntityManagerHelper($em), 'em');
$cli->getHelperSet()->set(
  new ConfigurationHelper($connection, $configuration),
  'configuration'
);

Doctrine\Migrations\Tools\Console\ConsoleRunner::addCommands($cli);

foreach ($commands as $name) {
/** @var Command $command */
  $command = $container->get($name);
  $cli->add($command);
}

$cli->run();

// --- ConsoleRunner.php
public static function addCommands(Application $cli): void
{
  $cli->addCommands([
    new MigrateCommand(),
    // ...
    ]);

  if (!cli->getHelperSet()->has('em')) {
    return;
  }
  $cli->add(new DiffCommand());
}

```

еще раз тот же кусок кода из примера выше. для новой (ныне актуальный) версии
```php
<?
  if ($dependencyFactory === null || ! $dependencyFactory->hasSchemaProvider()) {
      return;
  }
  $cli->add(new DiffCommand($dependencyFactory));
```






## Разделение команд Doctrine-Migrations для разных окружений(dev|prod)

При подключении Doctrine-ORM мы подключали команды в bin/app.php не сразу пачкой,
через ConsoleRunner, а только те из них которые нам будут реально нужны и причем
для каждого окружения(dev|prod) делая свой набор,
убирая всё лишние ненужные команды, распределяя только нужные для prod и dev
- ./api/config/common/console.php  (prod)
- ./api/config/dev/console.php     (dev)
Делали это прописывая имена классов комманд для создания их через DI-контейнер.

Так же зарегистрируем и команды для миграций.
Берём только нужные из команд под конкретное окружения и распределяем их по
конфиг-файлам в каталоге api/config/ common и dev

Удаляем ConsoleRunner:addCommands() и прописываем классы в конфигах

к тому же на вызов метода addCommands будет ругаться psalm говоря что идёт
использование внутренних частей библиотеки:

```
ERROR: InternalMethod - bin/app.php:43:1 - The method
Doctrine\Migrations\Tools\Console\ConsoleRunner::addCommands
is internal to Doctrine and Doctrine but called from root namespace
(see https://psalm.dev/175)

Doctrine\Migrations\Tools\Console\ConsoleRunner::addCommands($cli, $dep);
```

[PROD](/api/config/common/console.php)
```php
<?php

declare(strict_types=1);

use App\Console;
use Doctrine\Migrations;
// здесь у нас набор команд для прод-окружения
return [
    'config' => [
        'console' => [
            'commands' => [
                Console\HelloCommand::class, // наша первая команда
                Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand::class,

                // добавляем новые команды из Doctrine-Migrations:
                Migrations\Tools\Console\Command\ExecuteCommand::class,
                Migrations\Tools\Console\Command\MigrateCommand::class,
                Migrations\Tools\Console\Command\LatestCommand::class,
                Migrations\Tools\Console\Command\ListCommand::class,
                Migrations\Tools\Console\Command\StatusCommand::class,
                Migrations\Tools\Console\Command\UpToDateCommand::class,
            ]
        ]
    ],
];
```


[DEV](./api/config/dev/console.php)
```php
<?php

declare(strict_types=1);

use App\Console;
use Doctrine\ORM\Tools\Console\Command\SchemaTool;
use Doctrine\ORM\Tools\Console\Command;
use Doctrine\Migrations;

return [
    'config' => [
        'console' => [
            'commands' => [
                Console\DICheckCommand::class,
                SchemaTool\DropCommand::class,
                Command\InfoCommand::class,

                Migrations\Tools\Console\Command\DiffCommand::class,
                Migrations\Tools\Console\Command\GenerateCommand::class,
            ]
        ]
    ],

];
```

- DiffCommand - Для автоматической генерации миграций (частоиспользуемая команда)
- GenerateCommand - для генерации пустых классов-шаблонов миграций при ручном их
написании.(Это может понадобиться если надо будет сделать некий запрос на
модификацию неких данных, а не прям на постоянное использование)

Дальше нужно прописать для DI-контейнера как собирать DependencyFactory

./api/config/common/migrations.php

```php
<?php

declare(strict_types=1);

use Doctrine\Migrations;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\Migrations\Tools\Console\Command;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

return [
    // Здесь мы прописываем для DI-контейнера как собирать DependencyFactory
    // этот код будет вызываться каждый раз когда из контейнера будут просить
    // инстанс класса dependencyFactory - это нужно для проброса настроек и em
    // внутрь команд доктрин-мираций
    DependencyFactory::class => static function (ContainerInterface $container) {
        $em = $container->get(EntityManagerInterface::class);

        $conf = new Doctrine\Migrations\Configuration\Configuration();
        $conf->addMigrationsDirectory(
            'App\Data\Migration',
            __DIR__ . '/../../src/Data/Migration'
        );
        $conf->setAllOrNothing(true);
        $conf->setCheckDatabasePlatform(false);

        $sc = new Migrations\Metadata\Storage\TableMetadataStorageConfiguration();
        $sc->setTableName('migrations');

        $conf->setMetadataStorageConfiguration($sc);

        return DependencyFactory::fromEntityManager(
            new Migrations\Configuration\Migration\ExistingConfiguration($conf),
            new Migrations\Configuration\EntityManager\ExistingEntityManager($em)
        );
    },
    // инстансы команд так же будут собираться DI-контейнером и при их
    // инстанцировании(доставании из контейнера) будут передаваться нужные им
    // зависимости - по сути подключение к бд(вместе с em) и конфиг миграций

    Command\MigrateCommand::class => static function (ContainerInterface $ci) {
        /** @var Doctrine\Migrations\DependencyFactory $factory */
        $factory = $ci->get(DependencyFactory::class);
        return new Command\MigrateCommand($factory);
    },
    // .. и так же по аналогии все остальные нами добавленные команды
    Command\DiffCommand::class => static function (ContainerInterface $ci) {
        /** @var Doctrine\Migrations\DependencyFactory $factory */
        $factory = $ci->get(DependencyFactory::class);
        return new Command\DiffCommand($factory);
    },
];
```

Снова Проверяем доступные команды после измнения кода:

```sh
docker compose run --rm api-php-cli composer app
```

```
migrations
  migrations:diff
  migrations:execute
  migrations:generate
  migrations:latest
  migrations:list
  migrations:migrate
  migrations:status
  migrations:up-to-date
 orm
```
наличие команды diff здесь как проверка того что контейнер зависимостей отработал
как надо и DependencyFactory успешно передаётся в команды.



## run migrations:diff - создание первой миграции изучаем сгенерированный код

Генерируем первую миграцию на основе нами написанного маппинга(атрибутов в коде)

При этом произойдёт ровно тоже самое как при orm:validate-schema:
- построится первая схема на основе маппинга сущностей заданного аттрибутами в коде
- затем построится вторая схема на основе имеющихся в бд таблицах

```sh
docker compose run --rm api-php-cli composer app migrations:diff
```

```
> php bin/app.php --ansi 'migrations:diff'
 Generated new migration class to "/app/config/common/../../src/Data/Migration/Version20231203143514.php"

 To run just this migration for testing purposes, you can use migrations:execute --up 'App\\Data\\Migration\\Version20231203143514'

 To revert the migration you can use migrations:execute --down 'App\\Data\\Migration\\Version20231203143514'
```

Эта команда сгенерировала файл c php-классом:
./api/src/Data/Migration/Version20231203143514.php

```php
<?php

declare(strict_types=1);

namespace App\Data\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231203143514 extends AbstractMigration
{
    public function getDescription(): string { return ''; }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE auth_user_networks (id UUID NOT NULL, user_id UUID NOT NULL, network_name VARCHAR(16) NOT NULL, network_identity VARCHAR(16) NOT NULL, PRIMARY KEY(id))');
        //..
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE auth_user_networks DROP CONSTRAINT FK_3EA78C3BA76ED395');
        $this->addSql('DROP TABLE auth_user_networks');
        $this->addSql('DROP TABLE auth_users');
    }
```

При этом т.к. у нас в ./api/config/common/migrations.php прописана настройка:

        $conf->setCheckDatabasePlatform(false);

то этим мы говорим чтобы генерировались SQL-запросы независящие от СУБД
если не указать её то и в коде миграций будет вставка кода на проверку того,
чтобы выдавать ошибку при попытке накатить миграцию не на PostgreSQL СУБД

$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
'Migration can only be exe....')

Что можно подметить из сгенеренного класса с миграцией:
- наследуется от Doctrine\Migrations\AbstractMigration
- в имя класса входит текущее время.
- в методе up идёт накопление SQL Запросов через метод `addSql`
  для последующего запуска всех накопленных одной транзакцией
  то есть запросы не выполняются сразу через execute а именно собираются в один
  большой - транзакцию
- сгенерились SQL-запросы на создание таблицы CREATE TABLE auth_user_networks
- проставились индексы CREATE INDEX IDX_3EA78... ON auth_user_networks (user_id)
- создались уникальные индексы CREATE UNIQUE INDEX UNIQ_3EA ... ON auth_user_networks
- создалась вторая таблица auth_users
- проставился внешний ключ с одной таблицы на другую:

```sql
  ALTER TABLE auth_user_networks
  ADD CONSTRAINT FK_3EA78C3BA76ED395 FOREIGN KEY (user_id)
  REFERENCES auth_users (id)
  ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE
```

- добавились коментации с прописыванием наших кастомных типов к колонкам таблиц

```sql
COMMENT ON COLUMN auth_users.id IS \'(DC2Type:auth_user_id)\'
```


## Коментарии кастомных типов сохраняемые в саму БД. что это и зачем

Все эти коментарии нужны для внутренней работы Doctrine-Migrations а именно для
работы команды migrations:diff. этими коментариями прописывается в саму бд
словно метками какие из соответствующих полей таблиц обьекты надо бдует собирать
при вытягивании сущности из бд.

То есть если взять и удалить все эти строки с COMMENT то миграция всё равно будет
работать и её можно будет запустить и она создаст нужную нам таблицу. но вот
вносить дальнейшие измнения уже не сможет. т.к. никак не сможет определить
кастомные типы значений хранимых в колонках бд.таблиц

Т.е. при создании очередной миграции при измнении в сущностях вызвав команду diff
доктрин составит две схемы на основе аттрибутов(аннотаций) в коде увидит там
нами уже зареганные кастомные типы, дальше пойдёт в таблицу а там их не будет.

```
expected                      real:
tables:                       tables:
  auth_users:                   auth_users:
    columns:                       columns:
        id: auth_user_id              id: uuid      COMMENT auth_user_id
     email: auth_user_email        email: tring     COMMENT auth_user_email
      hash: string,null             hash: string, null
```
слева в ожидаемых - это типы из аттрибутов-аннотаций прописанные нами в коде
справа типы колонок из реальной таблицы в БД

Так вот если бы этих коментов не было то при каждом запуске команды diff
генерились бы запросы на измнение типов полей. т.к. они не соответствуют
тому что нужно. Именно поэтому и добавляются коментарии к полям таблиц в саму БД.
Это способ прописать наши кастомные типы, чтобы на их основе отслеживать измнения
и создавать правильные миграции.

Причем генерация их просиходит не по умолчанию сама собой, а именно потому
что мы это прописали в каждом классе наших кастомных сущностей через:
`requiresSQLCommentHint()`
./api/src/Auth/Entity/User/EmailType.php

```php
<?
use Doctrine\DBAL\Types\StringType;

/**
 * 16-10-2023
 * @author Swarg
 */
class EmailType extends StringType
{
    // ...

    // этот метод говорит доктрин нужно ли писать комент в поле для этого типа
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
    // вот это и говорит доктрин добавлять коментарии к полям таблицы в саму БД
        return true;
    }
```

Это важная деталь. если не добавить этот метот до коментарий сам не будет
добавляться при генерации миграций, а значит БД постоянно будет в
рассогласованном состоянии - т.е. diff постоянно будет видеть расхождения
маппинга сущностей и текущего состояния самих таблиц реальной бд.
Т.к в маппинге например будет прописан тип auth_user_email а в бд VARCHAR


Следующий момент по сгенерированному коду миграции

## CREATE SCHEMA public
баг избыточной строки в генерируемом php-коде миграций
детальное описание иобьяснения почему и что с этим делать
[здесь](./docs/15-migrations-fixtures/06-fix-public-bug.md)

Кратко решение - исправить права доступа к сгенереному файлу
```sh
sudo chown $USER:$USER api/src -R
```
и удалялить из метода down строку с `CREATE SCHEMA public`



## Накатываем сгенерированную миграцию migrations:migrate

```sh
docker compose run --rm api-php-cli composer app migrations:migrate
```
```
WARNING! You are about to execute a migration in database "app" that could result in schema changes and data loss. Are you sure you wish to continue? (yes/no) [yes]:
 > yes

[notice] Migrating up to App\Data\Migration\Version20231203143514
[notice] finished in 613.3ms, used 10M memory, 1 migrations executed, 16 sql queries

 [OK] Successfully migrated to version: App\Data\Migration\Version20231203143514
```

Смотрим что там накатилось через консоль

```sh
PGPASSWORD=secret psql -h localhost -p 54321 -U app -d app
```


```
psql (13.13 (Debian 13.13-0+deb11u1), server 13.12)
Type "help" for help.

app=# \dt
              List of relations
 Schema |        Name        | Type  | Owner
--------+--------------------+-------+-------
 public | auth_user_networks | table | app
 public | auth_users         | table | app
 public | migrations         | table | app
(3 rows)
```
здесь показывается что у нас в "схеме" public есть три таблицы (Name)

посмотреть всё содержимое таблицы migrations можно вот так:
```
app=# select * from migrations;

                 version                  |     executed_at     | execution_time
------------------------------------------+---------------------+----------------
 App\Data\Migration\Version20231203143514 | 2023-12-03 15:38:42 |            496
```

как видно накатанная нами только что миграция прописалась в бд.
- имя миграции: `App\Data\Migration\Version20231203143514`
- время когда была накатана: `2023-12-03 15:38:42`


причем если попытаться накатить миграцию снова через вызов той же команды получим
```
 [OK] Already at the latest version ("App\Data\Migration\Version20231203143514")
```
значит действительно механиз защиты от повторного запуска той же самой миграции
работает как и положено.
Теперь у нас всё готово для командной разработки:
- используя миграции у нас есть инструмент синхронизации локальных БД всех
участников команды. и когда надо изменить структуру(схему) БД достаточно будет
просто накатить новую миграцию и у каждого из команды локальная БД подтянется
до последнего актуального состояния. или можно будет раскатать на свежем сервере
всю нужную бд прям снуля поэтапно накатывая имеющиеся миграции на чистую БД.
Так же и на прод-сервере миграции помогут "двигать состояние БД впрёд" до нужного

Здесь смотрим на содержимое созданной таблицы через консольный клиент:
[что-там-накатилось](./docs/15-migrations-fixtures/05-psql-cli-look.md)


