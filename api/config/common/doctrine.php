<?php

declare(strict_types=1);

use App\Auth;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\ORMSetup;
use Doctrine\ORM\Tools\Console\Command;
// use Doctrine\ORM\Tools\Console\EntityManagerProvider;
use Doctrine\ORM\Tools\Console\EntityManagerProvider\SingleManagerProvider;
use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

return [

    EntityManagerInterface::class =>
    static function (ContainerInterface $container): EntityManagerInterface {
        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{
         *     metadata_dirs:string[],
         *     dev_mode:bool,
         *     proxy_dir:string,
         *     cache_dir:?string,
         *     types:array<string,class-string<Doctrine\DBAL\Types\Type>>,
         *     subscribers:string[],
         *     connection:array<string, mixed>
         * } $settings
         */
        $settings = $container->get('config')['doctrine'];

        $config = ORMSetup::createAttributeMetadataConfiguration(
            $settings['metadata_dirs'],
            $settings['dev_mode'],
            $settings['proxy_dir'],
            $settings['cache_dir']
                ? new FilesystemAdapter('', 0, $settings['cache_dir'])
                : new ArrayAdapter()
        );

        $config->setNamingStrategy(new UnderscoreNamingStrategy());

        foreach ($settings['types'] as $name => $class) {
            if (!Type::hasType($name)) {
                Type::addType($name, $class);
            }
        }

        $conn_params = $settings['connection'];
        /**
         * @psalm-suppress ArgumentTypeCoercion
         */
        $connection = DriverManager::getConnection($conn_params, $config);

        $eventManager = new EventManager();
        foreach ($settings['subscribers'] as $name) {
            /** @var Doctrine\Common\EventSubscriber $subscriber */
            $subscriber = $container->get($name);
            $eventManager->addEventSubscriber($subscriber);
        }

        return new EntityManager($connection, $config, $eventManager);
        // $em = new EntityManager($connection, $config);
        // $r = $em->getRepository(User::class);
    },

    Doctrine\DBAL\Connection::class =>
    static function (ContainerInterface $container): Connection {
        /** @var Doctrine\ORM\EntityManagerInterface $em */
        $em = $container->get(EntityManagerInterface::class);
        return $em->getConnection();
    },


    'config' => [
        'doctrine' => [
            'dev_mode' => false,
            'cache_dir' => __DIR__ . '/../../var/cache/doctrine/cache',
            'proxy_dir' => __DIR__ . '/../../var/cache/doctrine/proxy',
            'connection' => [
                'driver' => 'pdo_pgsql',
                'host' => getenv('DB_HOST'),
                'user' => getenv('DB_USER'),
                'password' => getenv('DB_PASSWORD'),
                'dbname' => getenv('DB_NAME'),
                'charset' => 'utf-8',
            ],
            'subscribers' => [],
            'metadata_dirs' => [
                __DIR__ . '/../../src/Auth/Entity',
            ],
            'types' => [
                Auth\Entity\User\IdType::NAME => Auth\Entity\User\IdType::class,
                Auth\Entity\User\EmailType::NAME => Auth\Entity\User\EmailType::class,
                Auth\Entity\User\StatusType::NAME => Auth\Entity\User\StatusType::class,
                Auth\Entity\User\RoleType::NAME => Auth\Entity\User\RoleType::class,
            ],
        ],
    ],

    // --------------------------------------------------------------------- \\
    //                    CLI Commands for bin/app.php
    // --------------------------------------------------------------------- \\

    SingleManagerProvider::class =>
    static function (ContainerInterface $container): SingleManagerProvider {
        // print("[DEBUG] DI-Container new SingleManagerProvider\n");
        /** @var Doctrine\ORM\EntityManagerInterface $em */
        $em = $container->get(EntityManagerInterface::class);
        return new SingleManagerProvider($em);
    },

    // role Interface -> implementing class
    // EntityManagerProvider::class => DI\get(SingleManagerProvider::class),

    Command\ValidateSchemaCommand::class =>
    static function (ContainerInterface $c): SymfonyCommand {
        /** @var Doctrine\ORM\Tools\Console\EntityManagerProvider $emp */
        $emp = $c->get(SingleManagerProvider::class);
        return new Command\ValidateSchemaCommand($emp);
    },

];
