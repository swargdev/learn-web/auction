## Установка и настройка Библиотеки Puppeteer

Мы наши E2E тесты запускаем изнутри докер-контейнера, а значит нам нужно сделать
так чтобы и браузер запускался тоже внутри докер-контейнера.
Если бы работали с e2e тестами и Puppeteer напрямую без докера, тогда при
запуске тестов прямо в локальной машине открывался бы браузер.

В общем в нашем докер-контейнере cucumber-node-cli наши тесты будут работать
через nodejs, и теперь нам нужно в этот же контейнер каким-то образом установить
туда браузер без UI - без пользовательского интерфейса

Браузеры Chome и Chromium позволяют запускать из в headless режиме - т.е. без UI
(для возможности работать в фоне не выводя ничего "на экран" т.е. для его работы
не нужно ставить внутрь докер-образа оконные окружения потипу X11 - X Win System)

План действий такой
- установить саму либу Puppeteer через yarn в package.json
- установить сам браузер в наш докер-образ cucumber-node-cli



Устанавливаем саму биб-ку `Puppeteer` и в доп. биб-ку `chai` для тест-assert-ов
```sh
docker compose run --rm cucumber-node-cli yarn add puppeteer chai
```

```sh
du -hcd 1 cucumber/node_modules/
```
`96MB`

После установки двух пакетов puppeteer и chai автоматически изменится файл
./cucumber/package.json
```json
{
  "license": "MIT",
  "scripts": {
    "e2e": "cucumber-js",
    "lint": "eslint --ext .js,.jsx features",
    "lint-fix": "eslint --fix --ext .js,.jsx features"
  },
  "dependencies": {
    "@cucumber/cucumber": "^10.1.0",
    "chai": "^4.3.10",
    "puppeteer": "^21.6.1"
  },
  "devDependencies": {
    "eslint": "^8.0.1",
    "eslint-config-standard": "^17.1.0",
    "eslint-plugin-import": "^2.25.2",
    "eslint-plugin-n": "^16.0.0",
    "eslint-plugin-promise": "^6.0.0"
  }
}
```


REFS
https://stackoverflow.com/questions/48230901/docker-alpine-with-node-js-and-chromium-headless-puppeter-failed-to-launch-c
