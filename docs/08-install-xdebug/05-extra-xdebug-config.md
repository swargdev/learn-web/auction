## Улучшение и обновление настроек Xdebug

https://stackoverflow.com/questions/73097246/how-to-add-xdebug-to-php8-1-fpm-alpine-docker-container

# Configure Xdebug
RUN echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.log=/var/www/html/xdebug/xdebug.log" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.discover_client_host=1" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.client_port=9000" >> /usr/local/etc/php/conf.d/xdebug.ini

Настройка для Xdebug
/usr/local/etc/php/conf.d/xdebug.ini
```
xdebug.start_with_request=yes
xdebug.mode=debug
xdebug.log=/var/www/html/xdebug/xdebug.log
xdebug.discover_client_host=1
xdebug.client_port=9000
```

https://stackoverflow.com/questions/65616198/xdebug-start-start-with-request-yes-without-error-if-the-debug-client-is-not-lis
