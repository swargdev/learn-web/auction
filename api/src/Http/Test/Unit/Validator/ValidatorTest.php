<?php

declare(strict_types=1);

namespace App\Http\Test\Unit\Validator;

use App\Http\Validator\Validator;
use App\Http\Validator\ValidationException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @covers Validator
 */
class ValidatorTest extends TestCase
{
    public function testValid(): void
    {
        $command = new \stdClass();

        $origin = $this->createMock(ValidatorInterface::class);
        $origin->expects(self::once())->method('validate')
            ->with(self::equalTo($command)) // с таким параметров
            ->willReturn(new ConstraintViolationList());// что должен вернуть

        $validator = new \App\Http\Validator\Validator($origin);

        $validator->validate($command);
    }

    public function testNotValid(): void
    {
        $command = new \stdClass();

        $origin = $this->createMock(ValidatorInterface::class);
        $origin->expects(self::once())->method('validate')
            ->with(self::equalTo($command))
            ->willReturn($violations = new ConstraintViolationList([
                $this->createMock(ConstraintViolation::class),
            ]));

        $validator = new Validator($origin);

        try {
            $validator->validate($command);
            self::fail('Expected exception is not thrown');
        } catch (\Exception $exception) {
            self::assertInstanceOf(ValidationException::class, $exception);
            /** @var ValidationException $exception*/
            self::assertEquals($violations, $exception->getViolations());
        }
    }
}
