## Анализ тестового покрытия нашего кода

- как создаётся анализ покрытия
- как просматривается
- зачем нужен и как работет изнутри

### Запускаем анализ покрытия кода

vendor/bin/phpunit можно запустить с флагом опеределяющим в каком виде собирать
статистику покрытия нашего кода тестами. Пример:

    --coverage-<формат-отчёта> <куда класть отчёты>

форматы могут быть разные например: text, html, xml (смотри phpunit --help)
```
Code Coverage:
                                Write code coverage report in:
  --coverage-clover <file>       Clover XML format to file
  --coverage-cobertura <file>    Cobertura XML format to file
  --coverage-crap4j <file>       Crap4J XML format to file
  --coverage-html <dir>          HTML format to directory
  --coverage-text=<file>         text format to file [default: standard output]
  --coverage-xml <dir>           XML format to directory

  --coverage-php <file>         Write serialized code coverage data to file
```

Отличие запуска phpunit с флагом --coverage-html от запуска без них в том, что
с ними будет запускаться еще и Xdebug, но не в режиме отладки(debugging) а в
режиме трассировани слежения за происходящими с кодом процессами. При этом
Xdebug просто собирает статистику какие участки кода выполнялись.
Далее в конце phpunit собирает от xdebug эту статистику форматирует в нужный
формат и выдаёт конечному пользователю.

Это нужно для того, чтобы можно было ознакомившись с отчётом оценить
что покрыто тестами(конкретно методы классов, и даже строки в этих методах) а
что не покрыто и нужно устранить пробелы

На сбор статистики о покрытии тестами влияет настрока phpunit.xml(10.3):

         requireCoverageMetadata="true"

в 8.5 версси у неё было другое название `forceCoversAnnotation`

Но для того чтобы phpunit мог собирать статистику по покрытиям нужно добавить
аннотацию @covers как для классов так и для методов.

./api/tests/Unit/Http/JsonResponseTest.php:
```php
<?
/**
 * @covers \App\Http\JsonResponse
 */
class JsonResponseTest extends TestCase
{
```
добавим алиас в scripts composer.json
```
"test-unit-coverage": "phpunit --colors=always --testsuite=unit --coverage-html var/coverage",
```
Алиас для команды "Собирать покрытие и сохранять в каталог var/coverage"

```Makefile
test-unit-coverage:
	docker compose run -e XDEBUG_MODE=coverage --rm api-php-cli \
      composer test-unit-coverage
```

Запускаем и натыкаемся на Ошибку что не указан `XDEBUG_MODE=coverage`
```sh
make test-unit-coverage
docker compose run --rm api-php-cli composer test-unit-coverage
```
```
> phpunit --colors=always --testsuite=unit --coverage-html var/coverage
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.
Runtime:       PHP 8.1.23
Configuration: /app/phpunit.xml
.......                                                             7 / 7 (100%)
Time: 00:00.010, Memory: 8.00 MB
There was 1 PHPUnit test runner warning:

1) XDEBUG_MODE=coverage or xdebug.mode=coverage has to be set

WARNINGS!
Tests: 7, Assertions: 21, Warnings: 1.
Script phpunit --colors=always --testsuite=unit --coverage-html var/coverage
handling the test-unit-coverage event returned with error code 1
```

Для исправления ошибки надо передать в контейнер нужную переменную окружения:
(смотри 2ю строку в конманде)
```sh
make test-unit-coverage
docker compose run -e XDEBUG_MODE=coverage --rm api-php-cli composer test-unit-coverage
```
```
> phpunit --colors=always --testsuite=unit --coverage-html var/coverage
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.
Runtime:       PHP 8.1.23 with Xdebug 3.2.2
Configuration: /app/phpunit.xml
.......                                                             7 / 7 (100%)
Time: 00:00.074, Memory: 10.00 MB
OK (7 tests, 21 assertions)
Generating code coverage report in HTML format ... done [00:00.020]
```

Открываем в браузере один из html-файлов и смотрим отчёт
- `./api/var/coverage/index.html`
- `./api/var/coverage/dashboard.html`

Dashboard - общая инфа сколько покрытых и не покрытых классов

```
                              Code Coverage
          Lines                   Functions and Methods    Classes and Traits
Total     53.85%(warning) 7/13    25.00% (danger) 1/4      33.33% (danger) 1/3
Console   0.00% (danger)  0/5     0.00% (danger)  0/2       0.00% (danger) 0/1
Http      87.50%(warning) 7/8     50.00% (danger) 1/2      50.00% (danger) 1/2
```
Здесь проценты под `Lines` - сколько линий кода покрыто тестами (`covered`)

Здесь название каталога `Http` - кликабельно можно войти и посмотреть что в нём

```
                              Code Coverage
          Lines                   Functions and Methods    Classes and Traits

Total 	87.50%(warning) 7/8       50.00%(danger)  1/2       50.00% 1/2
Action 	0.00% (danger)  0/1        0.00%(danger)  0/1        0.00% (danger) 0/1
JsonResponse.php 	100.00%(success) 7/7 100.00%(success)1/1 100.00% (success)1/1
```

Как видно каталог Action содержащий класс HomeAction вооьще не покрыт(0.0%)
так как мы для него юнит-тесты вообще не писали.

Можно даже войти в сам класс например здесь это JsonResponse и посмотреть какие
конкретные строки кода выполнялись во время теста адрес урле будет такой

./api/var/coverage/Http/JsonResponse.php.html

Строки кода в самих методов классов обозначаются разного рода цветами:
- зеленым - те строки кода которые выполнились хотябы один раз во время теста
- красным - те которые вообще ниразу не выполнялись


Чем отчёты о покрытии кода могут быть полезны.
- позволяет взглянуть на весь код свысока и увидить для каких классов нужно
  дописать тесты.
- есть мудрёные методы с ветвлениями по условиям(IF) и чтобы убедится что все
  ветви выполнились досточно будет посмотреть это в отчёте. То есть если во
  время выполнения теста строка кода не выполнилась - она будет красной иначе
  зелёной.

Аннотация `@covers <имя класса>` защищает от ошибок в алгоритмах phpunit и Xdebug
при опеределении какие классы, методы мы тестируем в конкретных своих тестах.
Может быть случай когда в тесте идёт вызов дополнительного своего класса
например
```php
$headers = new Headers();
$response = new JsonResponse(0, 200, $headers);
```
И если аннотацию @covers не указать явно то phpunit отметил для себя зелёным
не только наш класс JsonResponse который мы собтсвенно здесь и тестируем, но
еще и класс Headers зачтёт что он "выполнялся". А это может быть нам не нужно
поэтому явно указав
```
/* @covers \App\Http\JsonResponse */
class JsonResponseTest extends TestCase
```
мы чётко говорим здесь проверяется только один конкретный класс JsonResponse

аннотацию `@covers` ввели с phpunit v7+ по умолчанию она включена и говорит о том
что каждый тестовый метод должен через эту аннотацию указывать какой класс и
метод он тестирует. Задаётся это через атрибут в phpunit:
-  8: `forceCoversAnnotation="true"` - следить чтобы везде было и ругаться если нет
- 10+: `requireCoverageMetadata="true"` - тот же смысл

Аннотация `@covers` весьма удобна для юнит-тестов. т.к. обычно один юнит тест
проверяет один какой-то класс и его метод или методы.

Но не удобна для функциональных тестов, потому как для них придётся описывать
руками кучу классов которые используется внутри каждого функционального теста.
```
/* @covers \App\Http\СlassA
   @covers \App\Http\ClassN */
class MyFunctionalTest extends WebTestCase
```

Но и для функциональных тесто тоже нужа и важна возможность посмотреть какие
контроллеры, репозитории, сервисы используются в тестах а какие нет.

Решение: отключить строгое требование указывать `@covers` везде.
тогда можно будет подключить покрытие и для функциональных тестов.

         requireCoverageMetadata="false"

Теперь @covers можно и не прописывать.
Если аннотации бы не было, но было включено требование тогда бы считалось что
тесты ничего не покрывают(?).



Переделываем команду запуска тестов с покрытием не только для Unit но и для
Functional тестов. просто убираем --testsuite из composer.json:

      "test-coverage": "phpunit --colors=always --coverage-html var/coverage",

и просто переносим указание testsuite-а в `Makefile`:

```Makefile
api-test-unit:
	docker compose run --rm api-php-cli composer test -- --testsuite=unit

api-test-unit-coverage:
	docker compose run -e XDEBUG_MODE=coverage --rm api-php-cli composer test-coverage -- --testsuite=unit

api-test-functional:
	docker compose run --rm api-php-cli composer test -- --testsuite=functional

api-test-functional-coverage:
	docker compose run -e XDEBUG_MODE=coverage --rm api-php-cli composer test-coverage -- --testsuite=functional
```

Таким образом у нас появляются команды для простого запуска юнит и функ-тестов
и запуск этих же тестов со сбором статистики по покрытию кода (делается медленее)

И можно просматривать покрытие не только для юнит тестов но идля функциональных
Кстати Запустив функ тесты с покрытием и посмотрев отчёт увидем что кактлог
Http покрыт на все 100% а значит какждая строчка кода отработала в тестах.

То есть отключение строго режима позволяют собирать статистику покрытия кода
не только для унит-тестов но и для других типов тестов(функциональных например)
Когда работа идёт с целой пачкой классов и указывать руками их затруднительно
либо займёт много места


Статистика по покрытию собирается только для классов из прописанного каталога в
phpunit.xml:
```xml
<source restrictDeprecations="true" restrictNotices="true" restrictWarnings="true">
    <include>
        <directory suffix=".php">src</directory>
    </include>
</source>
```
т.е. для каталога `src`
https://docs.phpunit.de/en/10.3/configuration.html#the-source-element
https://docs.phpunit.de/en/10.3/configuration.html#the-include-element
source - блок для указания где искать исходники проекта
include - подключение каталога к исходникам

на phpunit 8 это писалось через filter/whitelist
```
<filter>
  <whitelist processUncoveredFilesFromWhitelist="true">
    <directory suffix=".php">src</directory>
  </whitelist>
</filter>
```
https://docs.phpunit.de/en/8.5/configuration.html#the-filter-element

Таким образом если надо чтобы и для конфиг-файлов добавлялась статистика -
то надо просто "добавить каталог config в исходники" в phpunit.xml-е



## Статистика покрытия для парсинга утилитами в pipeline и анализаторах кода

Для этого нужно просто использовать другой флаг для формирования отчёта не
в html а например в xml формате удобном для парсинага утилитами а не для просмотра
человеком.

```
  --coverage-clover <file>       Clover XML format to file
  --coverage-cobertura <file>    Cobertura XML format to file
  --coverage-crap4j <file>       Crap4J XML format to file
  --coverage-html <dir>          HTML format to directory
  --coverage-text=<file>         text format to file [default: standard output]
  --coverage-xml <dir>           XML format to directory
```

Теста со сбором статистики по покрытию кода работают примерно в 2 раза медленнее
простых тестов. Есть способ ускорить их работу через тонкую настройку Xdebug
с прописываеним для него доп параметров подключением кэша.




## Makefile команда автоочистки var каталога от мусора

Например от отчётов по покрытию тестов

```Makefile
init: docker-down-clear docker-pull docker-build docker-up api-clear api-init
# ...

api-clear:
	docker run --rm -v ${PWD}/api:/app -w /app alpine sh -c 'rm -rf var/*'
```
Используется такая команда `sh -c 'rm -rf var/*'` вместо `rm -rf var/*`
т.к. без ковычек маска-звёздочка поломается и не будет работать. поэтому передаём
запустить команду внутри шела доке-контейнера через `sh -c`

при этом команда `rm -rf var/*` удалит только видимые файлы, а файлы с точки
оставит т.е. такие как .gitignore

Команда для исправления прав доступа для каталога var "внутри контейнера"
```Makefile
api-permissions:
  docker run --rm -v ${PWD}/api:/app -w /app alpine chmod 777 var
```

```sh
make api-clear
docker run --rm -v /home/swarg/dev0/php/auction/api:/app -w /app alpine sh -c 'rm -rf var/*'
```
При первом запуске если нет образа alpine - подтянет его с репозитория
```
Unable to find image 'alpine:latest' locally
latest: Pulling from library/alpine
Digest: sha256:7144f7bab3d4c2648d7e59409f15ec52a18006a128c733fcff20d3a4a54ba44a
Status: Downloaded newer image for alpine:latest
```
Проверяем реально ли остаются скрытые файлы после `rm -rf var/*`
```sh
ls -a api/var/
.  ..  .gitignore  .phpcs.json  .phplint.json  .phpunit.cache
```


