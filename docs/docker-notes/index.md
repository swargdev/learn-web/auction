## Заметки по работе с докером и передачей в него EnvVar-ов

[Docker Env Docs](https://docs.docker.com/engine/reference/commandline/run/#env)

Если переменных много или нужно чтобы их значения не выводились в логе терминала
например для (credentional данных) можно задавать их через заданный файл
```sh
docker run --env-file ./env.list ubuntu bash
```

Пример из документации передать EnvVar-ы внутрь запускаемого контейнера и
показать их значения:
```sh
docker run --env VAR1=value1 --env VAR2=value2 ubuntu env | grep VAR
```

Пример запуска контейнера с передачей переменной окружения `XDEBUG_MODE`
```sh
docker compose run -e XDEBUG_MODE=coverage --rm api-php-cli composer test-unit-coverage
```

Пример запуска shell-команды внутри докер контейнера.
```sh
docker run --rm -v ${PWD}/api:/app -w /app alpine sh -c 'rm -rf var/*'
```
Здесь поднимиться контейнер из образа `alpine:latest`
(При этом если нет такого в системе - он скачается)
в него смонтируется каталог `api` из текущего каталога в `/app` внутри контейнера
и затем внутри этого контейнера установится рабочий каталог `/app` (-w)

`sh -c 'cmd'` - используется для того чтобы корректно передать команду с
спец символами внутрь контейнера. т.е. внутри контейнера поднимется shell `sh`
и внутри него указанная команда.(по умолчанию в alpine нет bash-а)
```
-v, --volume list                    Bind mount a volume
-w, --workdir string                 Working directory inside the container
```

## Перенаправить лог сборки образа в файл

redirect everything into the file:

```sh
docker build --no-cache --progress=plain  . &> build.log
docker compose build my-service-name --no-cache --progress=plain &> build.log
```
