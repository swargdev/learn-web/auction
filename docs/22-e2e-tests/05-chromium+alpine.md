## Установка Chromium-браузера в Docker-контейнер
на оф репозитории либы Puppeteer в каталоге docs есть файл `troubleshooting.md`
https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md
в нём есть описание того как можно установить chromium-браузер

> `Running Puppeteer in Docker`
https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md#running-puppeteer-in-docker

Там описано два способа установки:
1)  установка для обычных Debiab-Ubuntu образов:(Нам не подходит)

```Dockerfile
FROM node:14-slim

# Install latest chrome dev package and fonts to support major charsets
# Note: this installs the necessary libs to make the bundled version of
# Chromium that Puppeteer installs, work.
RUN apt-get update \
    && apt-get install -y wget gnupg \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-stable \
       fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst \
       fonts-freefont-ttf libxss1 \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*
# ...
```
Т.е. это слишком "тяжелый" способ установки, но есть и более подходящий
`Alpine` который мы у себя и спользуем:

> Running on Alpine

The [newest Chromium package]https://pkgs.alpinelinux.org/package/edge/community/x86_64/chromium
supported on Alpine is 100, which corresponds to
[Puppeteer v13.5.0](https://github.com/puppeteer/puppeteer/releases/tag/v13.5.0).

## Бразурер Chromium можно поставить на Alpine через пакетный менеджер `apk`
(как раз то что нужно)

```Dockerfile
FROM alpine

# Installs latest Chromium (100) package.
RUN apk add --no-cache \
      chromium \
      nss \
      freetype \
      harfbuzz \
      ca-certificates \
      ttf-freefont \
      nodejs \
      yarn
# ...

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

# Puppeteer v13.5.0 works with Chromium 100.
RUN yarn add puppeteer@13.5.0

# Add user so we don't need --no-sandbox.
RUN addgroup -S pptruser && adduser -S -G pptruser pptruser \
    && mkdir -p /home/pptruser/Downloads /app \
    && chown -R pptruser:pptruser /home/pptruser \
    && chown -R pptruser:pptruser /app

# Run everything after as non-privileged user.
USER pptruser
...
```
в примере этого докер-образа отдельно устанавливаются `nodejs` и `yarn` т.к.
берётся (FROM alpinie) обычный образ alpine, а не образ `node:apline` как у нас

Puppeteer при первом запуске может сам скачать нужный для его работы браузер
Но лучше будет самому поставить браузер и сообщить ему чтобы сам не качал

Для этого и используется эта переменная окружения:
```Dockerfile
# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser
```
Этим мы говорим Puppeteer что нужно работать с уже установленным браузером.
раньше еще указывали переменную `PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true`
сейчас в примере из доки её убрали

Дальше в целях безопасности идёт создание обычного пользователя
чтобы не запускать браузер и все тесты в нём от root
```Dockerfile
# Add user so we don't need --no-sandbox.
RUN addgroup -S pptruser && adduser -S -G pptruser pptruser \
    && mkdir -p /home/pptruser/Downloads /app \
    && chown -R pptruser:pptruser /home/pptruser \
    && chown -R pptruser:pptruser /app

# Run everything after as non-privileged user.
USER pptruser
```

Такая безопасность особенно актуальна когда Puppeteer+Chromium используются для
парсинга сторонних сайтов, на которых вполне могут быть потенциально опасные
скрипты


Дополняем наш Dockerfile до такого:

./cucumber/docker/development/node/Dockerfile
```Dockerfile
FROM node:18-alpine

RUN apk add --no-cache \
    chromium \
    nss \
    freetype \
    freetype-dev \
    harfbuzz \
    ca-certificates \
    ttf-freefont

# Tell Puppeteer to skip installing Chrome.  Use the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
    PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

# Add user so we don't need --no-sandbox.
# RUN addgroup -S node && adduser -S -G node node && \
RUN mkdir -p /home/node/Downloads /app \
    && chown -R node:node /home/node \
    && chown -R node:node /app

WORKDIR /app

USER node
```

Группа и пользователь `node` уже будут созданны в самом базовом образе
`node:18-alpine`:

> Official Docker Node Image Based on Alpine
https://github.com/nodejs/docker-node/blob/f416b53801a9d49d6ce6b2c038c8bc9ed93625dd/18/alpine3.18/Dockerfile
```Dockerfile
FROM alpine:3.18

ENV NODE_VERSION 18.19.0

RUN addgroup -g 1000 node \
    && adduser -u 1000 -G node -s /bin/sh -D node \
# ...
```



После обновления пересобираем образ "сервиса" `cucumber-node-cli`
```sh
	docker compose build cucumber-node-cli
```
(Можно просто чз `make docker-build`)

```sh
# (163/170) Installing chromium (120.0.6099.129-r0)
```

Интереса ради смотрим на размер образа с браузером Chromium
```sh
docker images
site-cucumber-node-cli latest d2681f594b44   35 seconds ago   755MB
```

- chromium-120.0.6099.129-r0 installed size:  219 MiB
- nss-3.94-r0 installed size:                3664 KiB
- freetype-2.13.2-r0 installed size:          664 KiB
- freetype-dev-2.13.2-r0 installed size:     1016 KiB
- ca-certificates-20230506-r0 installed size: 688 KiB
- font-freefont-20120503-r4 installed size:  6848 KiB
- harfbuzz-8.3.0-r0 installed size:          1236 KiB

~232 MB


Explanation for the `no-sandbox` flag in a quick introduction here
https://www.google.com/googlebooks/chrome/med_26.html
and for More in depth design document here
https://chromium.googlesource.com/chromium/src/+/master/docs/design/sandbox.md

Найдено здесь
https://github.com/Zenika/alpine-chrome


