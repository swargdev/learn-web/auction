- Библиотека willdurand/negotiation для парсинга Accept-Language
- Пишем свой Middleware/ContentLanguage на основе willdurand/negotiation

## Библиотека willdurand/negotiation для парсинга Accept-Language

Для распознования заголовка `Accept-Language` можно использовать готовую либу
https://packagist.org/?query=Negotiation

https://github.com/willdurand/Negotiation

Negotiation is a standalone library without any dependencies that allows you to
implement content negotiation in your application, whatever framework you use.
This library is based on RFC 7231.
Negotiation is easy to use, and extensively unit tested!

```sh
composer require willdurand/negotiation
docker compose run --rm api-php-cli composer require willdurand/negotiation
```

#### Language Negotiation UsageExample

```php
<?php

$negotiator = new \Negotiation\LanguageNegotiator();

$acceptLanguageHeader = 'en; q=0.1, fr; q=0.4, fu; q=0.9, de; q=0.2';
$priorities          = array('de', 'fu', 'en');

$bestLanguage = $negotiator->getBest($acceptLanguageHeader, $priorities);

$type = $bestLanguage->getType();
// $type == 'fu';

$quality = $bestLanguage->getQuality();
// $quality == 0.9

//The LanguageNegotiator returns an instance of AcceptLanguage.
```


Подключаем
```sh
docker compose run --rm api-php-cli composer require willdurand/negotiation

./composer.json has been updated
Running composer update willdurand/negotiation
Loading composer repositories with package information
Updating dependencies
Lock file operations: 1 install, 0 updates, 0 removals
  - Locking willdurand/negotiation (3.1.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
  - Downloading willdurand/negotiation (3.1.0)
  - Installing willdurand/negotiation (3.1.0): Extracting archive
Generating autoload files
No security vulnerability advisories found.
Using version ^3.1 for willdurand/negotiation


64M   api/vendor/
196K	api/vendor/willdurand/negotiation
```


## Пишем свой Middleware/ContentLanguage на основе willdurand/negotiation
Задача этого мидлвера парсить заголовок Accept-Language и распознавать из
разного рода опеределений в том числе из списка приоритета тот язык который
поддерживает наше приложение. И после распознования заменять значение этого
же заголовка со списка на конкретный язык. То есть подготавливать заголовок
превращая список вариантов из языков в один конкретный язык. Чтобы дальше
Middleware\TranslationLocale мог на его основе переключить язык перевода.


```php
<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Negotiation\LanguageNegotiator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ContentLanguage implements MiddlewareInterface
{
    private array $languages = [];// список поддерживаемых приложением языков

    /* будем передавать сюда список языков из фабрики DI-контейнера */
    public function __construct(array $languages = [])
    {
        $this->languages = $languages;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $locale = $this->detectFromHeader($request);

        // идея здесь в том чтобы переключать язык на нужный только если
        // есть заголовок Accept-Language, иначе использовать язык по умолчанию
        if (!empty($locale)) {
            $request = $request->withHeader('Accept-Language', $locale);
        }

        // вызов следующего по цепочке - это будет Middleware\TranslationLocale
        return $handler->handle($request);
    }

    // сама работа по определению языка из заголовка Accept-Language
    private function detectFromHeader(ServerRequestInterface $request): ?string
    {
        $accept = $request->getHeaderLine('Accept-Language');

        if (empty($accept)) {
            return null;
        }

        $default = $this->languages[0] ?? null; // первый в списке язык по умолч.
        $locale = null;

        try {
            // парсинг на основе библиотеки
            $negotiator = new LanguageNegotiator();
            $best = $negotiator->getBest($accept, $this->languages);
            if ($best instanceof \Negotiation\AcceptLanguage) {
                $locale = $best->getType(); // наиболее подходящий из списка
            }
        } catch (\Exception $exception) {
            // ignore
        }

        // если опеределить не удалось либо язык не является поддерживаемым
        if (empty($locale) || !in_array($locale, $this->languages, true)) {
            $locale = $default; // тогда использовать дефолтный язык
        }

        return $locale;
    }
}
```

```php
<?
use App\Http\Middleware\ContentLanguage;
use App\Http\Test\Unit\WebUTestCase; // это мой класс-помошник для Юнит тестирования
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @covers ContentLanguage
 */
class ContentLanguageTest extends WebUTestCase
{
    // метод помощник его задача создать мок-заглушку контроллера
    // и проверять какой язык указан в заголовке Accept-Language
    // и валить тест если передан язык не такой какой ожидается
    private function mkControllerExpLocale(string $exp): RequestHandlerInterface
    {
        $handler = $this->createStub(RequestHandlerInterface::class);

        $handler->method('handle')->willReturnCallback(
            static function (ServerRequestInterface $request) use ($exp)
            : ResponseInterface {
                $actual = $request->getHeaderLine('Accept-Language');

                self::assertEquals($exp, $actual); // workload

                return self::createResponse();
            }
        );
        return $handler;
    }


    // когда явно не задан заголовок Accept-Language просто ничего не делать
    // точнее не вызывать никакого переключения на другой язык.
    public function testDefault(): void
    {
        $middleware = new ContentLanguage(['en', 'ru']);

        $request = self::createRequest(); // вообще нет заголовка Accept-Language
        $handler = $this->mkControllerExpLocale(''); // язык по умолчанию

        $middleware->process($request, $handler);
    }

    // прямое явное и простое указание языка без списка
    public function testAccepted(): void
    {
        $middleware = new ContentLanguage(['en', 'ru']);

        $request = self::createRequest()->withHeader('Accept-Language', 'ru');
        $handler = $this->mkControllerExpLocale('ru');

        $middleware->process($request, $handler);
    }

    // указание списка с приоритетами
    public function testMulti(): void
    {
        $middleware = new ContentLanguage(['en', 'ru']);

        $request = self::createRequest()
            ->withHeader('Accept-Language', 'es;q=0.9, ru;q=0.8, *;q=0.5');

        $handler = $this->mkControllerExpLocale('ru');

        $middleware->process($request, $handler);
    }

    // язык не поддерживаемый нашим приложением - поставить язык по умолчанию
    public function testOther(): void
    {
        $middleware = new ContentLanguage(['en', 'ru']);

        $request = self::createRequest()->withHeader('Accept-Language', 'es');
        $handler = $this->mkControllerExpLocale('en');

        $middleware->process($request, $handler);
    }
}
```

Регистрация фабрики для передачи в обьект поддерживаемых приложением языков

./api/config/common/translator.php
```php
<?
    Middleware\ContentLanguage::class =>
    static function (ContainerInterface $container): Middleware\ContentLanguage {
        /**
         * @psalm-suppress MixedArrayAccess
         * @var string[] $allowed
         */
        $allowed = $container->get('config')['locales']['allowed'];
        return new Middleware\ContentLanguage($allowed);
    }
```

Возвращаю список языков в тест контроллера Join/RequestAction:
```php
<?
    public function testNotValidLang(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v1/auth/join', [
            'email' => 'not-email',
            'password' => '',
        ])->withHeader('Accept-Language', 'es;q=0.9, ru;q=0.8, *;q=0.5')); //<<<
        #...
    }

    // следующее что нужно сделать - перевод доменных исключений
    public function testExistingLang(): void
    {
        $this->markTestIncomplete('Waiting translation for domain exceptions');

        $response = $this->app()->handle(self::json('POST', '/v1/auth/join', [
            'email' => 'existing@app.test',
            'password' => 'new-password',
        ])->withHeader('Accept-Language', 'ru'));

        self::assertEquals(409, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        $data = Json::decode($body);

        self::assertEquals([
            'message' => 'Пользователь уже существует.',
        ], $data);
    }
```


