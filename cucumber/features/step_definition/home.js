const { Then } = require('@cucumber/cucumber');
const { expect } = require('chai');

Then('I see welcome block', async function () {
  // await this.page.waitForSelector('.welcome') by css-class
  await this.page.waitForSelector('[data-test=welcome]')
  const callbackGetText = function(el) { return el.textContent }
  // (el) = > el.textContent
  // const text = await this.page.$eval('.welcome h1', callbackGetText)
  const text = await this.page.$eval('[data-test=welcome] h1', callbackGetText)
  expect(text).to.eql('Auction')
});
