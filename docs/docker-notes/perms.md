## Возможны проблемы с загрузкой php-классов при отсутствии прав доступа

```sh
docker ps # to find CONTAINER_ID
docker exec -it CONTAINER_ID /bin/sh
```

find api/ -type d -print0 | xargs -0 chmod 755
find api/ -type f -print0 | xargs -0 chmod 644

на директории должны стоять права -x (-execute) чтобы php мог видить файлы в ней

как я проверял почему php не видит класс
```php
<?
# public/index.php
require __DIR__ . '/../vendor/autoload.php'; // загрузка классов

// =======
function ch(string $path): void {
    echo $path . "  "; var_dump(is_readable($path)); echo "<br>";
}
function chClass(string $class): void {
  echo $class . "  "; var_dump(class_exists($class)) echo "<br>";
}

ch(__DIR__); // /app/public
ch('../src');
ch('../src/Http/Middleware/ClearEmptyInput.php');

chClass('App\Http\Middleware\ClearEmptyInput');
return
// =======
#  ... $app = ...
```
