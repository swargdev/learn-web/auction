для 16й и 17й версии React для написания юнит-тестов под React-компоненты
активно используют библиотеку `Enzyme` https://github.com/enzymejs/enzyme


Вот выдержка из старой документации еще для React 16
`http://create-react-app.dev/docs/running-tests`:

if you'd like to test component in isolation from the child compoments they
render, we recommended using shallow() rendering API from `Enzyme`

И то как эту либу можно поставить:
```sh
npm install --save-dev enzyme enzyme-adapter-react-16 react-test-renderer
yarn add --save-dev enzyme enzyme-adapter-react-16 react-test-renderer
```

Но в библиотеку React 18 внесли изменения, которые по сути
сделали невозможным обновление Enzyme до 18 версии React-a
подробнее смотри здесь ./docs/21-test-n-liners.react/03-use-rtl-react18.md


## Здесь о том как писать юнит тесты для React 18 через RTL без Enzyme
не используя для этого библиотеку Enzyme, а используя RTL

Долгое время для тестирования UI-React-компонентов в стиле юнит-тестов а
не интеграционных тестов использовали б-ку Enzyme но с выходом React 18 её
похоронили "измнения в новой версии реакта".

Oct 8, 2022:
Enzyme is dead. (There will be no React 18 support)
you can read the article below for more info but in short:
the API changes in React 18 meant that releasing a React 18 Enzyme adapter
will not be possible
https://dev.to/wojtekmaj/enzyme-is-dead-now-what-ekl


Jul 25, 2023
Not official, but worth trying as a short-term solution until you can migrate
to something like RTL:

https://www.npmjs.com/package/@cfaester/enzyme-adapter-react-18


## React Testing Library(RTL)
https://testing-library.com/docs/react-testing-library/intro/

rewrite your remaining Enzyme-based tests to RTL.

> О том как перейти от Enzyme на RTL
https://testing-library.com/docs/react-testing-library/migrate-from-enzyme/
https://github.com/testing-library

Кратко:
говорят о том, что Enzyme был хорош многие годы и в него вкладывались много
разработчиков, контрибьютя улучшения. Но он работал на основе прямого
использования кода внутренней реализации биб-ки React, и для более надёжного
тестирования не зависящего внутренней реализации React и предлагают перейти
на RTL

Как утсановить `RTL` (CRA автоматически ставит эти зависимости сам)

```sh
npm install --save-dev @testing-library/react @testing-library/jest-dom
```
```sh
yarn add --dev @testing-library/react @testing-library/jest-dom
```

после отработки `CRA` эти библиотеки уже будут добавлены в package.json
поэтому устанавливать их не нужно - уже стоят.


О том как использовать RTL

## Import React Testing Library to your test

If you're using Jest (you can use other test frameworks),
then you only need to import the following modules into your test file:

```js
// import React so you can use JSX (React.createElement) in your test
import React from 'react'

/**
 * render: lets us render the component as React would
 * screen: a utility for finding elements the same way the user does
 */
import {render, screen} from '@testing-library/react'


// The test structure can be the same as you would write with Enzyme:
test('test title', () => { // test === it
  // Your tests come here...
})
```

```js
// I went from this...
test('...', () => {
  const wrapper = shallow(<MyComp dynamicValue="foo" />);
  expect(wrapper.find(FontAwesomeIcon).prop('icon')).toEqual(someIcon);
})

// To this
test('...', () => {
  const { container } = render(<MyComp dynamicValue="foo" />);
  expect(container.firstChild).toMatchSnapshot();
})
```



## Basic Enzyme to React Testing Library migration examples
https://testing-library.com/docs/react-testing-library/migrate-from-enzyme/#basic-enzyme-to-react-testing-library-migration-examples

Нет прямого и четкого один-к-одному способа для переписывания с Enzyme на RTL
и многие функции из Enzyme нужно отбросить:
(no more need for a `wrapper` variable or `wrapper.update()` calls, etc.)

Ниже пример сравнения написания тестов для Enzyme и RTL

Это исходный код UI-компонента Welcome который и будем тестировать:
```js
const Welcome = props => {
  const [values, setValues] = useState({
    firstName: props.firstName,
    lastName: props.lastName,
  })

  const handleChange = event => {
    setValues({...values, [event.target.name]: event.target.value})
  }

  return (
    <div>
      <h1> Welcome, {values.firstName} {values.lastName} </h1>

      <form name="userName">
        <label>
          First Name
          <input
            value={values.firstName} name="firstName" onChange={handleChange}
          />
        </label>

        <label>
          Last Name
          <input
            value={values.lastName} name="lastName" onChange={handleChange}
          />
        </label>
      </form>
    </div>
  )
}

export default Welcome
```

## Test 1: Render the component, and check if the h1 value is correct

> Enzyme test

```js
test('has correct welcome text', () => {
  const wrapper = shallow(<Welcome firstName="John" lastName="Doe" />)
  expect(wrapper.find('h1').text()).toEqual('Welcome, John Doe')
})
```

> React Testing library (RTL)

```js
test('has correct welcome text', () => {
  render(<Welcome firstName="John" lastName="Doe" />)
  expect(screen.getByRole('heading')).toHaveTextContent('Welcome, John Doe')
})
```
С виду оба теста похожи друг на друга
метод `shallow` из Enzyme не рендерит подкомпоненты,

поэтому метод `render` из RTL больше похож на метод `mount` из Enzyme.

В RTL не нужно присваивать результат рендеринга переменной (wrapper) -
Получить доступ к результату рендеринга можно через обьект `screen`.
Так же RTL сам автоматом чистит окружение после прохождения какждого теста,
а значит не нужно самому явно прописывать очистку в afterEach|beforeEach

в RTL getByRole('heading') - 'heading' is the accessible role of the h1 element.
You can learn more about them on the queries documentation page.
https://testing-library.com/docs/queries/byrole

One of the things people quickly learn to love about Testing Library is
how it encourages you to write more accessible applications
(because if it's not accessible, then it's harder to test).


## Test 2: Input texts must have correct value

In the component above, the input values are initialized with the
`props.firstName` and `props.lastName` values.
We need to check whether the value is correct or not.

Enzyme
```js
test('has correct input value', () => {
  const wrapper = shallow(<Welcome firstName="John" lastName="Doe" />)
  expect(wrapper.find('input[name="firstName"]').value).toEqual('John')
  expect(wrapper.find('input[name="lastName"]').value).toEqual('Doe')
})
```

React Testing Library
```js
test('has correct input value', () => {
  render(<Welcome firstName="John" lastName="Doe" />)
  expect(screen.getByRole('form')).toHaveFormValues({
    firstName: 'John',
    lastName: 'Doe',
  })
})
```

## How to shallow render a component?
О том как использовать поверхностный рендеринг подставляя вместо внутренних
компонентов заглушки
In general, you should avoid mocking out components.
However, if you need to, then try using `Jest`'s mocking feature.
For more information, see the FAQ.
https://testing-library.com/docs/react-testing-library/faq/

> how do I mock out components in tests?

In general, you should avoid mocking out components
(see the Guiding Principles section).
However, if you need to, then try to use Jest's mocking feature.
One case where I've found mocking to be especially useful is for animation
libraries. I don't want my tests to wait for animations to end.

```js
// создаём заглушки UI-компонентов CSSTransition и Transition
jest.mock('react-transition-group', () => {
  //       ^^^ имя библиотеки-модуля в котором нужно подменить функции
  const FakeTransition = jest.fn(({children}) => children)
  const FakeCSSTransition = jest.fn(props =>
    props.in ? <FakeTransition>{props.children}</FakeTransition> : null,
  )
  // мэппинг подмены старые имена из библиотеки новыми мок-нутыми функциями
  return {CSSTransition: FakeCSSTransition, Transition: FakeTransition}
})

test('you can mock things with jest.mock', () => {
  const {getByTestId, queryByTestId} = render(
    <HiddenMessage initialShow={true} />,
  )
  expect(queryByTestId('hidden-message')).toBeTruthy() // we just care it exists
  // hide the message
  fireEvent.click(getByTestId('toggle-message'))
  // in the real world, the CSSTransition component would take some time
  // before finishing the animation which would actually hide the message.
  // So we've mocked it out for our tests to make it happen instantly
  expect(queryByTestId('hidden-message')).toBeNull() // we just care it doesn't exist
})
```

Note that because they're Jest mock functions (jest.fn()),
you could also make assertions on those as well if you wanted.

Open full test for the full example.(будет ниже)

This looks like more work than `shallow` rendering (and it is),
but it gives you more confidence so long as your mock resembles the thing
you're mocking closely enough.

If you want to make things more like shallow rendering,
then you could do something more like this.
https://testing-library.com/docs/example-react-transition-group

Learn more about how Jest mocks work from my blog post:
"But really, what is a JavaScript mock?"
https://kentcdodds.com/blog/but-really-what-is-a-javascript-mock

Полноценный пример


## React Transition Group
https://testing-library.com/docs/example-react-transition-group

> Mock (RTL)
```js
import React, {useState} from 'react'
import {CSSTransition} from 'react-transition-group'
import {render, fireEvent} from '@testing-library/react'

function Fade({children, ...props}) {
  return (
    <CSSTransition {...props} timeout={1000} classNames="fade">
      {children}
    </CSSTransition>
  )
}

function HiddenMessage({initialShow}) {
  const [show, setShow] = useState(initialShow || false)
  const toggle = () => setShow(prevState => !prevState)
  return (
    <div>
      <button onClick={toggle}>Toggle</button>
      <Fade in={show}>
        <div>Hello world</div>
      </Fade>
    </div>
  )
}

jest.mock('react-transition-group', () => {
  const FakeTransition = jest.fn(({children}) => children)
  const FakeCSSTransition = jest.fn(props =>
    props.in ? <FakeTransition>{props.children}</FakeTransition> : null,
  )
  return {CSSTransition: FakeCSSTransition, Transition: FakeTransition}
})

test('you can mock things with jest.mock', () => {
  const {getByText, queryByText} = render(<HiddenMessage initialShow={true} />)
  expect(getByText('Hello world')).toBeTruthy() // we just care it exists
  // hide the message
  fireEvent.click(getByText('Toggle'))
  // in the real world, the CSSTransition component would take some time
  // before finishing the animation which would actually hide the message.
  // So we've mocked it out for our tests to make it happen instantly
  expect(queryByText('Hello World')).toBeNull() // we just care it doesn't exist
})
```

> Shallow (Enzyme)
```js
import React, {useState} from 'react'
import {CSSTransition} from 'react-transition-group'
import {render, fireEvent} from '@testing-library/react'

function Fade({children, ...props}) {
  return (
    <CSSTransition {...props} timeout={1000} classNames="fade">
      {children}
    </CSSTransition>
  )
}

function HiddenMessage({initialShow}) {
  const [show, setShow] = useState(initialShow || false)
  const toggle = () => setShow(prevState => !prevState)
  return (
    <div>
      <button onClick={toggle}>Toggle</button>
      <Fade in={show}>
        <div>Hello world</div>
      </Fade>
    </div>
  )
}

//  ------------------

jest.mock('react-transition-group', () => {
  const FakeCSSTransition = jest.fn(() => null)
  return {CSSTransition: FakeCSSTransition}
})

test('you can mock things with jest.mock', () => {
  const {getByText} = render(<HiddenMessage initialShow={true} />)
  const context = expect.any(Object)
  const children = expect.any(Object)
  const defaultProps = {children, timeout: 1000, className: 'fade'}
  expect(CSSTransition).toHaveBeenCalledWith(
    {in: true, ...defaultProps},
    context,
  )
  fireEvent.click(getByText(/toggle/i))
  expect(CSSTransition).toHaveBeenCalledWith(
    {in: false, ...defaultProps},
    expect.any(Object),
  )
})
```


### Question
Формулировка вопроса по той же проблеме как писать юнит-тесты на RTL для
родительстких компонентов, чтобы при этом не учитывались измнения в дочерних
(найдено на reddit-e без чёткого ответа)

 we have an application, that uses components from our design system component
library. We would like to create dom/markup snapshot tests, but we would like
to omit the components from the library and only test for the changes in our
own code. If we don't do it, we will get an error every time the used
components from the library are updated, even though our code remains the same.
Thus, it would be a false positive.

How can we do it in React 18? It seems like enzyme is outdated.
RTL suggests using Jest.mock(), but how can we mock some internal sub-components,
that we use? Is shallow renderer from react itself also outdated?

толкового ответа не было


## Пример того как создавать зуглушки в тестовом фреймворке Jest

Задача вызвать этот Parent компонет изолировано так, чтобы не вызвался
код из компонента ChildWillBeIsolated
```js
export const Parent: FC = ({items}) => {
   return (
      <>
        <ListComponent items={items} />
        <ChildWillBeIsolated />
      </>
   )
}
```

In react-testing-library there is no option for shallow rendering,
so technically you can't.
But that does not mean that you cannot isolate child components and test them.
What you can do is mocking the child component like:

```js
import React from "react";
import { Parent as Component } from "./";
import { ChildWillBeIsolated } from "../ChildWillBeIsolated";
import { render } from "@testing-library/react";

const items = [ { title: "A" id: 1 }, { title: "B" id: 2 } ]

// создание заглушки для компонента ChildWillBeIsolated
jest.mock("../ChildWillBeIsolated", () => {
  return {
    __esModule: true,
    default: () => {            // if you exporting component as default
      return <div/>;            // вместо оригинального кода выведет пустой div
    },
    ChildWillBeIsolated: () => { // if you exporting component as not default
      return <div/>;
    },
  };
});

it("renders without crashing", async () => {
  // при рендере внутри Component на место оригинального ChildWillBeIsolated
  // автоматически подставиться переопределённая через jest.mock заглушка
  // и соответственно оригианльный код этого компонента заменится на другой
  const wrapper = render(<Component items={items} />);
  expect(wrapper).toMatchSnapshot();
  wrapper.unmount();
});
```
This code above should not throw any error since you
mocked the child component's return value as <div/>
edited May 18, 2020

Тут важное замечание - это решение для старого реакта ~16 версии в 18 для
тестов не рекомендуют использовать wrapper, доступ можно получать через screen
вообще поход с wrapper пошел от биб-ки Enzyme


## Refs Enzyme vs RTL
https://www.npmjs.com/package/@cfaester/enzyme-adapter-react-18
[Support for React 18]( https://github.com/enzymejs/enzyme/issues/2524)

https://blog.allegro.tech/2022/02/why-we-should-rewrite-enzymejs-to-rtl-and-how-to-do-that.html
https://alejandrocelaya.blog/2022/05/13/my-experience-migrating-from-enzyme-to-react-testing-library/
https://webbylab.com/blog/guide-of-testing-react-components-with-hooks-mocks/
https://chariotsolutions.com/blog/post/migrating-from-enzyme-to-react-testing-library/

https://jestjs.io/docs/tutorial-react
