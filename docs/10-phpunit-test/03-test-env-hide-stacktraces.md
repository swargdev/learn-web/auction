## ErrorMiddleware - скрыть вывод деталей об ошибках(стектрейсы) в тестах

Создав новые функ тесты такие как NotFoundTest при запуске видем что выводится
длинный стектрейс об ошибке:

```sh
make test
docker compose run --rm api-php-cli composer test
```
```
> phpunit --colors=always
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.23
Configuration: /app/phpunit.xml

.405 Method Not Allowed
Type: Slim\Exception\HttpMethodNotAllowedException
Code: 405
Message: Method not allowed. Must be one of: GET
File: /app/vendor/slim/slim/Slim/Middleware/RoutingMiddleware.php
Line: 79
Trace: #0 /app/vendor/slim/slim/Slim/Routing/RouteRunner.php(56):
Slim\Middleware\RoutingMiddleware->performRouting(Object(Slim\Psr7\Request))
...
```

Мы специально делали вывод ошибок для своего debv-окружения через добавление
специального middleware с названием ErrorMiddleware и настраиваем его здесь:

`./api/config/middleware.php`:

    $app->addErrorMiddleware($config['debug'], true, true);

GotoDef покажет сигнатуру функции. 2й параметр здесь `logErrors` - выводить ошибки
```php
    public function addErrorMiddleware(
        bool $displayErrorDetails,
        bool $logErrors,                      // << выводить ошибки в консоль
        bool $logErrorDetails,
        ?LoggerInterface $logger = null
    ): ErrorMiddleware {
```
если прогулятся по исходникам классов ErrorMiddleware ErrorHandler то в конце
придём к тому, что увидим что детали об ошибка выводятся через вызов функции
`error_log($error)` - а это вывод напрямую в консоль. Нам такое в тестах не надо.

Для тестового окружения чтобы не выводились все стектрейсы при возникновении
исключений (В том числе наличие которых мы и проверяем в своих тестах)
нужно вторым параметром в addErrorMiddleware передавать не true а false

Так как для тестового окружения мы переопределяем APP_ENV=test в файле
`api/phpunti.xml`
```xml
    <php>
        <env name="APP_ENV" value="test" force="true" />
        <env name="APP_DEBUG" value="1" force="true" />
    </php>
```
то это значение можно использовать в коде своего конфига:

добавим еще одну переменную `env` в свой конфиг `./api/config/common/system.php`:
```php
<?php

declare(strict_types=1);

return [
    'config' => [
        'env' => (bool) getenv('APP_ENV') ?: 'prod', // создали новую настройку
        'debug' => (bool) getenv('APP_DEBUG'),
    ],
];
```
Теперь настройку при поднятии middleware будем передавать вот так:

    $app->addErrorMiddleware($config['debug'], $config['env'] != 'test', true);

то есть выводить ошибки везде кроме тестового окружения.
Таким условием когда `APP_ENV=test` то вторым параметром будет передаваться false
и при этом не будет логирования ошибок в консоль
