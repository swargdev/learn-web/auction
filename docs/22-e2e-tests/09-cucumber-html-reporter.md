## cucumber-html-reporter

http://github.com/gkushang/cucumber-html-reporter

Библиотека для генерации красивых html-отчётов на основе report.json из cucumber
с встроенными в них скриншотами в виде base64

Эта библиотека - более юзер-френдли способ просмотра отчётов сгенерированных
E2E тестами, отчёт можно будет открывать в браузере.
Годно для предоставления заказчику результатов выполнения тестов в виде
красивых Html-страниц, с подробными результатами прошедших тестов.

Здесь будет показано как его поставить к себе если он нужен
но в своём коде мы его себе ставить не будем.

# installation

```sh
npm install cucumber-html-reporter --save-dev
# or
yarn add --dev cucumber-html-reporter
```

при этом генерацию отчёта нужно будет запускать вручную cli-командой

но чтобы было что запускать нужно создать свой скрипт которы и будет генерировать
сам html отчёт

./cucumber/reporter.json
```js
var reporter = require('cucumber-html-reporter');

var options = {
    theme: 'bootstrap',
    jsonFile: 'var/report.json',
    output: 'var/report.html',
    reportSuiteAsScenarios: true,
    scenarioTimestamp: true,
    launchReport: true,
    metadata: {
        "App Version":"0.3.2",
        "Test Environment": "STAGING",
        "Browser": "Chrome  54.0.2840.98",
        "Platform": "Windows 10",
        "Parallel": "Scenarios",
        "Executed": "Remote"
    },
    failedSummaryReport: true,
    noInlineScreenshots: true,
    screenshotsDirectory: 'var/screenshots/',
    storeScreenshots: true
};

reporter.generate(options);
```


- noInlineScreenshots: true, - если не задать то скриншоты будут хранится внутри
одного большого html-файла в виде той же самой base64-строки, для нас будет
более удобно чтобы картинки сохранялись отдельно в каталог `var/screenshots/`

добавляем команду Запуска генерации: `node reporter.js`

```json
{
  "license": "MIT",
  "scripts": {
    "e2e": "cucumber-js --format json:var/report.json",
    "report": "node reporter.js",
    "lint": "eslint --ext .js,.jsx features",
    "lint-fix": "eslint --fix --ext .js,.jsx features"
  },
}
```

```Makefile
cucumber-report:
	docker compose run --rm cucumber-node-cli yarn report
```

Теперь нужно сделать так чтобы после запуска тестов test-e2e автоматом шла
генерация html-очёта:

При этом для нас важно чтобы отчёты генерились даже в случае провала задачи
cucumber-e2e. И для этого нужно переписать задачу test-e2e. В make есть
способ игнорировать ошибки команды так чтобы не прерывать поочерёдное выполнение
нескольких команд в списке когда одна из команд падает с ошибкой.
для этого нужно перед командой казать символ `-`. Но команда должна быть
внешняя поэтому `- make cucumber-e2e`.
В итоге даже если тесты завалятся то генерация отчёта всё равно будет запущена

```Makefile
test-e2e
	make api-fixtures
	make cucumber-clear
	- make cucumber-e2e
	make cucumber-report
```


