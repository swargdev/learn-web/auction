<?php

declare(strict_types=1);

namespace App\Auth\Command\JoinByEmail\Request;

use Symfony\Component\Validator\Constraints;

class Command
{
    #[Constraints\NotBlank]
    #[Constraints\Email]
    public string $email = '';

    #[Constraints\Length(min: 6)]
    #[Constraints\NotBlank]
    public string $password = '';
}
