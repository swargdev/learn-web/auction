После того как миграции работают можно автоматизировать все используемые
команды и обеспечить быстрый и удобный доступ к ним через короткие алиасы

Проблемы которые здесь решаем
- как обеспечить ожидание пока бд не запуститься и зачем это надо.
- обновляем Dev-докеробраз и Makefile


## Обновляем в Makefile алиас для длиной команды запуска миграций

```
docker compose run --rm api-php-cli composer app migrations:migrate
```

```Makefile
api-migrations:
	docker compose run --rm api-php-cli composer app migrations:migrate
```

Поместим проверку валидации схемы например в уже имеющийся линтер

```Makefile
check: lint analyze validate-schema test
...
api-validate-schema:
	docker compose run --rm api-php-cli composer app orm:validate-schema
```

теперь одной командой `make check` можно будет проверить абсолютно всё что
только можно - всё что есть в нашем проекте.
удобство в том, что после внесения изменений когда пора фиксировать(комитить)
и пушить в мастер - просто запустил мейк чек и проверил сразу всё одним махом.

Так же удобства ради сделаем автоматический запуск миграций при инициализации
проекта:

```Makefile
api-init: api-permissions api-composer-install api-migrations
```

Для того чтобы после вызова команды make init весь проект развернулся в том
числе и базаданных перешла в готовое к работе состояние - т.е прошли все нужные
миграции (накатились измнения для перевода БД в самое последнее актуальное
состояние)

здесь возникает проблема того что докер не будет дожидаться старта БД внутри
контейнера, просто выполнил команду и пойдёт дальше. тогда как сама СУБД
(у нас это PostgreSQL может достаточно долго загружаться, иногда до нескольких
секунд 3-5 sec)
Например если работа идёт с тяжелыми приложениями такими как RabbitMQ то их
запуск может быть порой от 5-10 секунд. это зависит от виртуальной машины на
на которой идёт запуск.
Наприемр для прода это критично т.к. если виртуалка на одноядерном процессоре с
малой памятью то и загрузка будет долгой.

именно потому что СУБД нужно время для того чтобы поднятся, и контейнер с ней
не будет запускаться молниеностно такая вешь не пройдёт:
т.к. в момент применения миграций вызова команды migrations:diff СУБД может просто
не успеть поднятся и команда выдаст ошибку подключения.
api-init: api-permissions api-composer-install api-migrations

решение проблемы "влоб" черед добавление паузы скажем в 10 секунд:
```Makefile
api-migrations:
    sleep 10
	docker compose run --rm api-php-cli composer app migrations:migrate
```

но такой способ крайне не удобен. т.к. на хорошем железе БД может стартовать за
1-2 секунды и ждать еще 8 как-то не оч. в то время как в CI на загруженых серверах
когда будем пропускать код через пайплайны Jenkins-а может случится что сервер
будет сильно загружен и времени просто не хватит на старт БД.

Решается через скрипты на подобии этих
[wait-for-it](https://github.com/vishnubob/wait-for-it)
[wait-for](https://github.com/eficode/wait-for)

пример запуска
```sh
wait-for-it.sh host:port [-s] [-t timeout] [-- command args]
```

суть в том что скрипт просто ждёт когда откроется заданный порт,
и можно указать timeout сколько максимум ждать (-t 60 - макс минуту).

вместо sleep используем этот скрипт
```Makefile
api-migrations:
    wait-for-it.sh api-postgres:5432 -t 30
	docker compose run --rm api-php-cli composer app migrations:migrate
```

но такой подход будет платформо-зависимый, годный только для linux
для того чтобы сделать его платформо-независимым нужно запускать его из докера.

<!-- v22 42:40 -->
а для этого нужно добавляеть его в собраемые образы.
[api-php-cli](./api/docker/development/php-cli/Dockerfile)
единственно что этому скрипту для работы нужны пакеты: bash и coreutils.

Чтобы не тянуть их в свои образы сделал свою версию на чистом Си.
https://gitlab.com/Swarg/wait4open

Но в отличии от скриптов это уже полноценная программа и её нужно компилировать.
Но так как в докер образах и для dev и для prod мы уже собираем нужные части
из исходников, то для компиляции всё нужное уже есть (Пакет g++).

Скопировал исходник в данный прокет:
./api/docker/common/wait4open.c

Дальше прописываю его сборку внутри докер образов

[DEV-Dockerfile](./api/docker/development/php-cli/Dockerfile)

```Dockerfile
FROM php:8.1-cli-alpine

COPY ./common/wait4open.c /tmp/w4o.c

RUN apk update && apk add --no-cache --virtual .build-deps \
    autoconf g++ make linux-headers \
    && pecl install xdebug-3.2.2 \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable xdebug \

    && gcc -pedantic -ansi -O2 /tmp/w4o.c -o /usr/local/bin/wait4open \
    && chmod +x /usr/local/bin/wait4open \

    && apk del -f .build-deps

#...
```
собираю сразу в исполняемую директорию: `/usr/local/bin/wait4open` (PATH)
делаю исполняемым `chmod +x /usr/local/bin/wait4open`

пересобрать конкретно только dev-образ:
```sh
docker compose build api-php-cli
```

Проверяем работает ли и видит ли сервис доступный внутри докер-компоуз сети:
```sh
docker compose run --rm api-php-cli wait4open -t 30 api-postgres:5432
Connecting api-postgres (IP: 172.21.0.4 Port: 5432) ... [SUCCESS]
```

Теперь можно добавлять новую команду в Makefile
```Makefile
api-wait-db:
	docker compose run --rm api-php-cli wait4open -t 30 api-postgres:5432
```
Запуск ожидания поднятия БД теперь будет происходить изнутри консольного образа
api-php-cli (это дев-окружение)

Теперь как надо. Перед накатыванием миграций(`api-migrations`) идёт ожидание
пока поднимится БД (`api-wait-db`)
```Makefile
api-init: api-permissions api-composer-install api-wait-db api-migrations
```

Теперь в итоге при инициализации api происходит
- api-permissions       - установка прав
- api-composer-install  - установка композера и всех зависимостей(библ-к пакетов)
- api-wait-db           - ожидание запуска базыданных
- api-migrations        - накатывание миграций(т.е. изменение состояния дб)

Автоматическое накатывание миграций для Dev-окружения готово.


Полный набор новых добавленых команд:

```Makefile
init: docker-down-clear docker-pull docker-build docker-up api-clear api-init
check: lint analyze validate-schema test
validate-schema: api-validate-schema

...

api-init: api-permissions api-composer-install api-wait-db api-migrations

...

api-wait-db:
	docker compose run --rm api-php-cli wait4open -t 30 api-postgres:5432

api-migrations:
	docker compose run --rm api-php-cli composer app migrations:migrate

api-validate-schema:
	docker compose run --rm api-php-cli composer app orm:validate-schema

```

