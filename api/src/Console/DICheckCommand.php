<?php

declare(strict_types=1);

namespace App\Console;

use Doctrine\ORM\Tools\Console\EntityManagerProvider;
use Symfony\Component\Console\Command\Command ;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Experimental for check di-container behavior
 */
class DICheckCommand extends Command
{
    /** @var EntityManagerProvider|null */
    private $entityManagerProvider;

    // public function __construct(EntityManagerProvider $entityManagerProvider)
    public function __construct(?EntityManagerProvider $entityManagerProvider = null)
    {
        parent::__construct();

        $this->entityManagerProvider = $entityManagerProvider;
    }

    protected function configure(): void
    {
        $this
            ->setName('di-check')
            ->setDescription('Check DI-Container');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $has = $this->entityManagerProvider ? 'has' : 'NULL';
        $output->writeln('<info>EntityManagerProvider: ' . $has . '</info>');

        return 0;
    }
}
