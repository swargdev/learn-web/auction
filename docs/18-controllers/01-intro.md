## Постановка задачи что есть и что будем делать.

Здесь реализуем контроллеры, через которые будем обращаться к нашему модулю Auth
Начнём с контроллеров, вызывающих методы команды JoinByEmail

Есть две команды для JoinByEmail:
```
./api/src/Auth/Command/JoinByEmail/
├── Request             1я команда - запрос регистрации
│   ├── Command.php
│   └── Handler.php
└── Confirm             2я команда подтверждение по токену
    ├── Command.php
    └── Handler.php
```
На данный момент есть только один контроллер `HomeAction`
- ./api/src/Http/Action/HomeAction.php
- `App\Http\Action\HomeAction`;

Он открывает главную страницу нашего API. (localhost:8081)
То есть после локального запуска всего проекта(make init)
заходя через браузер на адрес localhost:8081 (это и есть адрес нашего API)
мы и видем результат который выдваёт нам наш "экшен" HomeAction.

```sh
curl localhost:8081
{}                  -- это и есть результат работы - пока просто пустой Json
```

Теперь по аналогии с экшеном(контроллером) HomeAction нужно дописать все
остальные экшены, которые бы внутри себя использовали все наши комады из модуля
аутентификации:

`api/src/Auth/Command/`:
- JoinByEmail
- JoinByNetwork
- AttachNetwork
- ChangeEmail
- ChangePassword
- ChangeRole
- ResetPassword
- Remove

Какие тесты у нас уже есть:

Есть несколько функциональных тестов:
```
api/tests/Functional/
├── HomeTest.php        - проверяет главную страницу API
├── NotFoundTest.php    - проверка открытия несуществующей страницы
└── WebTestCase.php     - базовый класс с переиспользуемой в тестах логикой
```

тест проверяющий попытку запроса несуществующей страницы у нас такой:
```php
<?
class NotFoundTest extends WebTestCase
{
    public function testNotFound(): void
    {
        $response = $this->app()->handle(self::json('GET', '/not-found'));

        // пока просто проверяет код ответа и ничего больше
        self::assertEquals(404, $response->getStatusCode());
    }
}
```


## Дописываем NotFoundTest для HomeAction - контроллера

Но этот тест не полный т.к. кроме самого кода статуса http-ответа(404)
фреймворк возвращает еще и текст самой страницы (content)

А значит для более полной проверки нужно проверять в каком формате возвращается
ответ:


```php
<?
class NotFoundTest extends WebTestCase
{
    public function testNotFound(): void
    {
        $response = $this->app()->handle(self::json('GET', '/not-found'));

        self::assertEquals(404, $response->getStatusCode());
        // проверяем что тело ответа это json
        self::assertJson($body = (string)$response->getBody());

        /** @var array $data */
        // декодирование json-строки($body) в обьект-массив $data
        $data = json_decode($body, true, 512, JSON_THROW_ON_ERROR);

        // проверяем массивы на равенство
        self::assertEquals([
            'message' => '404 Not Found', // то что должно быть возвращено
        ], $data);
    }
}
```


Такой тест не пройдёт

```
docker compose run --rm api-php-cli composer test
...
There was 1 failure:

1) Test\Functional\NotFoundTest::testNotFound
Failed asserting that two arrays are equal.
--- Expected
+++ Actual
@@ @@
 Array (
     'message' => '404 Not Found'
+    'exception' => [...]                              <<< исключение
 )

/app/tests/Functional/NotFoundTest.php:20

FAILURES!
```
Потому что кроме самого ответа сервера в `message` отправляется и `exception`


## ErrorMiddleware.displayErrorDetails в ответе сервера

Смотрим на это через консольную программу `curl` явно указав http-заголовок
типа принимаемого нами контента
говоря тем самым нашему же серверу, что мы ожидаем только json а не text/html:

```sh
curl localhost:8081/not-found -H "Accept: application/json"
```
```json
{
    "message": "404 Not Found",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "Not found.",
            "file": "/app/vendor/slim/slim/Slim/Middleware/RoutingMiddleware.php",
            "line": 76
        }
    ]
}
```
Теперь понятно, что кроме самого сообщения об ошибке "404 Not Found"
выводится еще и служебная инф-я в json-поле `exception` со стректрейсом.
А выводится эта инфа прямо в теле json ответа сервера т.к. у нас настроено
логирование с выводом ошибок в коде.
Эта настройка задаётся при создании `Slim\Middlewar\ErrorMiddleware`:


./api/config/common/errors.php
```php
<?
    $middleware = new ErrorMiddleware(
        $callableResolver,
        $responseFactory,
        $config['display_details'], // << вот эта настройка зависит от APP_DEBUG
        true,
        true
    );
```

поле класса ErrorMiddleware `bool $displayErrorDetails` - и добавляет эту инфу.

Это удобно при локальной разработке.
Когда разрабатывается фронтенд то в веб-инспекторе(в инструментах разработчика
в браузере) можно будет посмотреть какие запросы отправляются по ajax и какие
ответы на эти запросы приходят.
Удобно видить в ответах возникающие ошибки, чтобы было проще понять что править.

Значит нам нужно немного подкорректировать тест для того чтобы можно было
проверить json-ответ на наличие `404 Not Found`

Как это можно сделать:
- просто выключить для тестов вывод вспомогательной информации
чтобы не добавлялся в json ответ сервера элемент exception.(не наш вариант)
- проверять не весь массив-json ответа а только его часть.
В частности просто глянуть есть ли в массиве(декодированном Json ответе)
ключ с именем `message` и какое у него значение.

## PhpUnit & AssertArraySubset

Раньше в PhpUnit был такой метод `assertArraySubset`
этот метод проверяет что проверяемый массив содержит в себе заданный подмассив.

разрабы часто использовали этот метод не правильно и постоянно долбили автора
PhpUnit вопросами как его использовать - достали так что он тупо удалил этот
метод из своей либы(2019год).

https://github.com/sebastianbergmann/phpunit/issues/3494

This is why I decided to deprecate assertArraySubset() in PHPUnit 8 and
to remove it in PHPUnit 9.
Anybody who thinks this functionality is useful is more
than welcome to take the code, put it into a separate project,
and package it as an extension for PHPUnit.


Теперь чтобы использовать этот метод нужно подключить либу-расширение для phpunit

```sh
composer require --dev dms/phpunit-arraysubset-asserts
```

https://packagist.org/packages/dms/phpunit-arraysubset-asserts
https://github.com/rdohms/phpunit-arraysubset-asserts


```sh
docker compose run --rm api-php-cli composer require --dev dms/phpunit-arraysubset-asserts
```

```
./composer.json has been updated
Running composer update dms/phpunit-arraysubset-asserts
Loading composer repositories with package information
Updating dependencies
Lock file operations: 1 install, 0 updates, 0 removals
  - Locking dms/phpunit-arraysubset-asserts (v0.5.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
  - Downloading dms/phpunit-arraysubset-asserts (v0.5.0)
  - Installing dms/phpunit-arraysubset-asserts (v0.5.0): Extracting archive
Generating autoload files
No security vulnerability advisories found.
Using version ^0.5.0 for dms/phpunit-arraysubset-asserts

60K	api/vendor/dms/
58M	api/vendor/
```


установив либу в проект, для того чтобы использовать нужный на метод
можно будет подключить trait из этой либы прямо в класс `NotFoundTest`

Trait - это набор методов которые можно "подключить" к классу
https://www.php.net/manual/en/language.oop5.traits.php
Нечто похожее на методы по умолчанию в java-интерфейсах
По сути просто сопособ добавить к классу набор дополнительныx методов

В самом пакете идёт README:
./api/vendor/dms/phpunit-arraysubset-asserts/README.md
там есть пример как подключать и использовать


```php
<?
use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;

class NotFoundTest extends WebTestCase
{
    use ArraySubsetAsserts; // подключение trait-а

    public function testNotFound(): void {
        #..

        self::assertArraySubset([ // использование метода через trait
            'message' => '404 Not Found',
        ], $data);
    }
```

Полезность этого метода в том, что он позволяет не проверять весь массив целиком
а только нужную часть. "Есть ли в заданном массиве указанный подмасссив"


## Проблема PHPUnitExtensions + phpactor

Здесь столкнулся с проблемой работы phpactor и хитроспелетений данной библ-ки-
расширения.
Насколько я понял в ней для обратной совместимости есть два варианта классов
`DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts`
содержащих нужный метод assertArraySubset

- ./api/vendor/dms/phpunit-arraysubset-asserts/src/ArraySubsetAsserts.php
- ./api/vendor/dms/phpunit-arraysubset-asserts/src/ArraySubsetAssertsEmpty.php

первый содержит нужный метод, второй тупо пустой.
и phpactor подсвечивает место вызова self::assertArraySubset тип не знаю я
такого метода - его нет. а при goto-def выбрасывает именно на пустой класс-
заглушку у которого нет нужного метода.
Хз как по хорошему исправить эту проблему. пока что просто добавил
одноимённый пустой метод assertArraySubset в WebTestCase чтобы его видел
phpactor И не выдвал ошибки при этом на деле сам этот метод будет перекрываться
trait-ом.



## Класс-помошник Json для упрощения декодирования тела ответа

```php
<?
class Json
{
    /**
     * @param string $data
     * @return array
     */
    public static function decode(string $data): array
    {
        /** @var array */
        return json_decode($data, true, 512, JSON_THROW_ON_ERROR);
    }
}
```


```php
<?
class NotFoundTest extends WebTestCase {
    use ArraySubsetAsserts;

    public function testNotFound(): void
    {
        $response = $this->app()->handle(self::json('GET', '/not-found'));

        self::assertEquals(404, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        $data = Json::decode($body); // заменяем хэлпером

        self::assertArraySubset([ 'message' => '404 Not Found' ], $data);
    }
```

Проверяем - тесты проходят


## Расширем метод WebTestCase:json добавляем тело для POST-запроса

Как будем тестировать остальные свои контроллеры?
смотрим как тестируется NotFoundTest

        $response = $this->app()->handle(self::json('GET', '/not-found'));

через метод `self::json` мы передаём метод запроса(GET) и адрес страницы

Для методов регистрации нужно будет передавать `POST` запросы в теле которого в
виде json-а передавать данные для регистрации

смотрим на метод self::json (Из базового класса от которого наследуемся)

```php
<?
class WebTestCase extends TestCase {

    protected static function json(string $method, string $path): ServerRequestInterface
    {
        return self::request($method, $path)
            ->withHeader('Accept', 'application/json')
            ->withHeader('Content-Type', 'application/json');
    }
```
Здесь у нас запрос собирается очень просто и не включает в себя никакое тело
запроса, которое нам нужно будет передавать для регистрации.

Переписываем так, чтобы можно было добавлять и тело запроса (для POST-Запросов):
```php
<?
class WebTestCase extends TestCase {

    protected static function json(
        string $method,
        string $path,
        array $body = [] // для данных передаваемых с POST-запросом
    ): ServerRequestInterface {
        $request = self::request($method, $path)
            ->withHeader('Accept', 'application/json')
            ->withHeader('Content-Type', 'application/json');

        // кодируем в виде json-а
        $request->getBody()->write(json_encode($body, JSON_THROW_ON_ERROR));

        return $request;
    }
```

