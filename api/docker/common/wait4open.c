/*
 * This file contains a completely self-contained source code for building the
 * wait4open program whose task is to wait for the specified host:port to open.
 *
 *
 * Example how to compile inside Dockerfile:
 *
 * FROM alpine
 * COPY ./src/main.c /tmp/w4o.c
 *
 * RUN apk update && apk add --no-cache --virtual .build-deps \
 *   g++ \
 *
 *   && gcc -pedantic -ansi -O2 /tmp/w4o.c -o /usr/local/bin/wait4open \
 *   && chmod +x /usr/local/bin/wait4open \
 *
 *   && apk del -f .build-deps
 *
 * ...
 *
 * @author Swarg 04-12-23
 *
 * */

/* explicity enabling POSIX features to compile with -pedantic -ansi */
#define _XOPEN_SOURCE 600

#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <netdb.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <netinet/in.h>

/* ------------------------------------------------------------------------- */

#define APP_NAME "wait4open"
#define APP_VER "0.1.0"

#define CODE_SUCCESS 0
#define CODE_ERROR 1
#define CODE_EXIT_BY_TIMEOUT 2

#define TIMEOUT_SECONDS 0
#define TIMEOUT_ENVVAR "W4O_TIMEOUT"

#define SLEEP_SECONDS 1
#define SLEEP_ENVVAR "W4O_SLEEP"

typedef struct w4o_peer {
  char *name;
  char *host;
  char *port;
  struct addrinfo *addr;
} w4o_peer_t;

/* */
static const char *USAGE = "Usage: %s [-v] [-q] [-d] "
                           "[-t <timeout_sec>] [-s <interval_sec>] "
                           "[<name>=]<host>:<port>\n"
                           "-q quite (by default output is verbose)\n"
                           "-d dry-run\n";

static const char *DRY_RUN =
    "[DRY-RUN] Just show what will be done without the work itself\n"
    "timeout seconds: %d\n"
    "sleep interval : %d\n"
    "name: %s\nhost: %s\nport: %s\naddr: %s\n";

/* ------------------------------------------------------------------------- */

/* static - means the local variables visible only in the current "module" */
static const char *bin_name;
static int opt_verbose = 1; /* -q to hide verbose ouput */
static int opt_dry_run = 0; /* -d */
static int timeout_sec = TIMEOUT_SECONDS;
static int sleep_sec = SLEEP_SECONDS;

/* ------------------------------------------------------------------------- */

void w4o_peer_free(w4o_peer_t *r);
w4o_peer_t *w4o_peer_new();
w4o_peer_t *parse_peer(const char *arg);

int opt_atoi(char *str, char opt_char, char *env, int has_opt, int def_val);

w4o_peer_t *parse_args(int argc, const char **argv);
w4o_peer_t *parse_peers(int argc, const char **argv);
int extract_parts(const char *composite, w4o_peer_t *peer);
int process_peer(const w4o_peer_t *peer, int timeout_sec, int sleep_sec);
int connect_to_addr(const struct addrinfo *a, const char *name);
struct addrinfo *configure_hints(struct addrinfo *a);
char *addrinfo2str(const struct addrinfo *a);

void show_dry_run(const w4o_peer_t *peer);

/**
 * EntryPoint
 */
int main(int argc, const char **argv)
{
  int ret;
  w4o_peer_t *peer;

  peer = parse_args(argc, argv);

  if (opt_dry_run) {
    show_dry_run(peer);
    exit(0);
  }

  ret = process_peer(peer, timeout_sec, sleep_sec);

  w4o_peer_free(peer);

  return ret;
}

/**
 * wait until the peer(host:port) becomes available rr until a timeout
 * timeout == 0 means that there is no waiting limit at all
 * that is, it will ping forever
 * retrun 0 - is success exit-code for system
 */
int process_peer(const w4o_peer_t *peer, int timeout_sec, int sleep_sec)
{
  struct addrinfo *addr;
  time_t curr_time, start_time;
  char *name;

  start_time = time(NULL);
  name = peer->name ? peer->name : peer->host;

  while (peer && peer->addr) {
    addr = peer->addr;
    while (addr) {

      if (connect_to_addr(addr, name) < 0) {
        /* failed, continue trying */
        addr = addr->ai_next; /* list of addrs */
      } else {
        return CODE_SUCCESS;
      }
    }

    if (timeout_sec) {
      int remaining;
      curr_time = time(NULL);

      if (start_time + timeout_sec <= curr_time) {
        fprintf(stderr, "Exit by timeout [%d].\n", timeout_sec);
        return CODE_EXIT_BY_TIMEOUT;
      }

      remaining = start_time + timeout_sec - curr_time;
      if (sleep_sec > remaining) {
        sleep_sec = remaining;
      }
    }

    sleep(sleep_sec);
  }
  return CODE_ERROR;
}

/**
 *
 */
void show_usage_and_exit()
{
  fprintf(stderr, USAGE, bin_name);
  exit(255);
}

static char *s_unknown = "?";

/**
 * just show how the arguments were parsed without waiting
 */
void show_dry_run(const w4o_peer_t *peer)
{
  char *name, *host, *port, *addr;
  struct addrinfo *ai = NULL;

  if (peer) {
    name = peer->name ? peer->name : "";
    host = peer->host;
    port = peer->port;
    addr = addrinfo2str(peer->addr);
    ai = peer->addr->ai_next;
  } else {
    name = host = port = addr = s_unknown; /* "?"; */
  }
  fprintf(stdout, DRY_RUN, timeout_sec, sleep_sec, name, host, port, addr);

  /* getaddrinfo can give a linked list with multiples ip addrs for one host */
  while (ai) {
    char *s = addrinfo2str(ai);
    fprintf(stdout, "addr: %s\n", s);
    free(s);
    ai = ai->ai_next;
  }
  if (addr != s_unknown) {
    free(addr);
  }

  exit(0);
}

/**
 * Parse Args: extract known options, and Peer
 */
w4o_peer_t *parse_args(int argc, const char **argv)
{
  w4o_peer_t *peer;
  char c;
  int has_opt_t = 0;
  int has_opt_s = 0;

  char *s_arg = getenv(SLEEP_ENVVAR);
  char *t_arg = getenv(TIMEOUT_ENVVAR);
  bin_name = argv[0];

  while ((c = getopt(argc, (char *const *)argv, "dhqvt:s:")) != -1) {
    switch (c) {
    case 'v':
      printf("%s version %s\n", APP_NAME, APP_VER);
      exit(0);

    case 'd': /* dry-run */
      opt_dry_run = 1;
      break;

    case 'q': /* quite */
      opt_verbose = 0;
      break;

    case 't': /* timeout */
      t_arg = optarg;
      has_opt_t = 1;
      break;

    case 's': /* sleep */
      s_arg = optarg;
      has_opt_s = 1;
      break;

    case '?': /* input error */
      if (optopt == 's') {
        fprintf(stderr, "Option -%c requires an argument.\n", optopt);
      } else if (isprint(optopt)) {
        fprintf(stderr, "Unknown option `-%c'.\n", optopt);
      } else {
        fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
      }
      show_usage_and_exit();

    case 'h':
    default:
      show_usage_and_exit();
    }
  }

  timeout_sec =
      opt_atoi(t_arg, 't', TIMEOUT_ENVVAR, has_opt_t, TIMEOUT_SECONDS);
  sleep_sec = opt_atoi(s_arg, 's', SLEEP_ENVVAR, has_opt_s, SLEEP_SECONDS);

  argc -= optind;
  argv += optind;

  if (argc < 1 || argc > 1) { /* TODO multiples peers */
    show_usage_and_exit();
  }

  peer = parse_peers(argc, argv);
  if (!peer) {
    fprintf(stderr, "Not found valid host:port in arguments\n");
    show_usage_and_exit();
  }

  return peer;
}

/**
 * find hots|ip:port from the reamin args
 * the prog-name and options have been "removed", start argc at 0
 */
w4o_peer_t *parse_peers(int argc, const char **argv)
{
  int i;
  w4o_peer_t *peer = NULL;

  i = 0;
  /* for (i = 0; i < argc; i++) { */
  peer = parse_peer(argv[i]);
  /* } */
  return peer;
}

/**
 * create substring by copy given str either to end_pos either to end of str
 *
 * end_pos - is an addr of the end char in str (optional)
 * if end_pos == NULL - copy to end of str
 */
char *mk_str_copy(const char *str, const char *end_pos)
{
  int len;
  char *copy = NULL;

  if (str == NULL) {
    return NULL;
  }
  len = strlen(str);

  if (end_pos != NULL) {
    int len0 = end_pos - str;
    if (len0 <= len) {
      len = len0;
    }
  }

  copy = malloc(len * sizeof(char) + 1);
  memcpy(copy, str, len);
  copy[len] = '\0';

  return copy;
}

/**
 *
 */
struct addrinfo *configure_hints(struct addrinfo *a)
{
  memset(a, 0, sizeof(*a));
  a->ai_family = PF_UNSPEC; /* AF_INET, AF_INET6 */
  a->ai_socktype = SOCK_STREAM;
  a->ai_protocol = IPPROTO_TCP;
  a->ai_flags |= AI_CANONNAME | AI_ADDRCONFIG;
  return a;
}

/*
 * build peer from arg
 * [name=]host:port
 */
w4o_peer_t *parse_peer(const char *arg)
{
  int err;
  struct addrinfo hints, *res;
  w4o_peer_t *peer = w4o_peer_new();

  extract_parts(arg, peer);

  configure_hints(&hints);

  err = getaddrinfo(peer->host, peer->port, &hints, &res);
  if (err != 0) {
    const char *errmsg;
    errmsg = (err == EAI_SYSTEM) ? strerror(errno) : gai_strerror(err);
    fprintf(stderr, "lookup: %s:%s:  %s\n", peer->host, peer->port, errmsg);

    w4o_peer_free(peer);

    return NULL;
  }

  peer->addr = res;

  return peer;
}

/**
 * parse composite argument to [name=]host:port
 */
int extract_parts(const char *composite, w4o_peer_t *peer)
{
  char *tok;
  const char *arg = composite;

  if ((tok = strchr(composite, '='))) {
    peer->name = mk_str_copy(composite, tok);
    composite = tok + 1;
  }

  if ((tok = strchr(composite, ':'))) {
    peer->host = mk_str_copy(composite, tok);
    peer->port = mk_str_copy(tok + 1, NULL);
  } else {
    fprintf(stderr, "Invailid argument (missing port): %s\n", arg);
    show_usage_and_exit();
  }
  return 1;
}

/**
 * arg to number
 * str - passed string value to convert
 * env - the env-var name
 * has_opt - opt defined via arguments
 */
int opt_atoi(char *str, char opt_char, char *env, int has_opt, int def_val)
{
  if (!str) {
    return def_val;
  }

  if (!(*str >= '0' && *str <= '9')) {
    char *hint;

    if (has_opt) {
      hint = calloc(16, sizeof(char));
      sprintf(hint, "argument to -%c", opt_char);
    } else {
      hint = calloc(12 + strlen(env), sizeof(char));
      sprintf(hint, "evn_var %s", env);
    }

    fprintf(stderr, "[WARNING]: %s [%s] is not a number. set to 0\n", hint,
            str);
    free(hint);
  }

  return atoi(str);
}

/**
 *
 */
w4o_peer_t *w4o_peer_new()
{
  w4o_peer_t *p = malloc(sizeof(w4o_peer_t));
  p->name = NULL;
  p->host = NULL;
  p->port = NULL;
  p->addr = NULL;

  return p;
}

/**
 *
 */
void w4o_peer_free(w4o_peer_t *p)
{
  if (p) {
    if (p->name) {
      free(p->name);
    }
    if (p->host) {
      free(p->host);
    }
    if (p->port) {
      free(p->port);
    }
    if (p->addr) {
      freeaddrinfo(p->addr);
    }
    free(p);
  }
}

/**
 * Make addrinfo readable
 */
char *addrinfo2str(const struct addrinfo *a)
{
  uint16_t port;
  int len = 64;
  char *ret = calloc(len + 1, sizeof(char));

  if (a == NULL) {
    snprintf(ret, len, "null");
  } else if (a->ai_family == AF_INET) {
    char ip[INET_ADDRSTRLEN];
    struct sockaddr_in *ai = (struct sockaddr_in *)a->ai_addr;
    port = htons(ai->sin_port);
    if (inet_ntop(a->ai_family, &(ai->sin_addr), ip, INET_ADDRSTRLEN)) {
      snprintf(ret, len, "IP: %s Port: %d", ip, port);
    }
  } else if (a->ai_family == AF_INET6) {
    char ip[INET6_ADDRSTRLEN];
    struct sockaddr_in6 *ai = (struct sockaddr_in6 *)a->ai_addr;
    port = htons(ai->sin6_port);
    if (inet_ntop(a->ai_family, &(ai->sin6_addr), ip, INET6_ADDRSTRLEN)) {
      snprintf(ret, len, "IP: %s Port: %d", ip, port);
    }
  } else {
    snprintf(ret, len, "Unknown addr-family: %d\n", a->ai_family);
  }

  return ret;
}

/**
 * try to connect to already resolved addrinfo
 */
int connect_to_addr(const struct addrinfo *ai, const char *name)
{
  int s, result;

  if ((s = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol)) < 0) {
    perror("socket");
    exit(127);
  }

  if (opt_verbose) {
    char *readable_addr = addrinfo2str(ai);
    printf("Connecting %s (%s) ... ", name, readable_addr);
    free(readable_addr);
  }

  if ((result = connect(s, ai->ai_addr, ai->ai_addrlen)) < 0) {
    if (opt_verbose)
      printf("[FAILED].\n");
  } else {
    if (opt_verbose) {
      printf("[SUCCESS].\n");
    } else {
      printf("PORT READY: %s\n", name);
    }

    close(s);
  }

  fflush(stdout);

  return result;
}
