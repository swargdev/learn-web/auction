## План:

- Введение, кратная теория
  - Выход на базу данных
  - принципы проектированияя приложений

- добавление БазыДанных в проект
  через образ postgres:13.12-apline в docker-compose.yml
  проброс порта(54321) для подключения из-вне (для dev-env)

- установка расширения pdo_pgsql для php.
  Чтобы php через PDO мог ходить в БД postgres
  - Оптимизация установки из исходников
  [pdo_pgsql](./docs/13-doctrine/02-pdo_pgsql-opti.md)

- установка Doctrine-ORM и настройка для работы с нашим DI-Контейнером
  - настройка подключения к бд (логин, пароль, имя бд, порт)
  - создание EntityManager через DI-Контейнер
  - подключение некоторых полезных cli команд Doctine к нашему api/bin/app.php
  [03-doctrine](./docs/13-doctrine/03-setup-doctrine-orm.md)

- подлючение библиотеки для продвиного обьединения конфиг-файлов
  [04-laminas-config-aggregator](./docs/13-doctrine/04-laminas-config-aggregator.md)


## Введение. Выход на базу данных, принцип проектированияя приложения

Логика работы модуля Auth(аутентификации и регистрации) реализована, продуманы
и реализованы, в виде прикладных комманд, все нужные операции для работы
с модулем.  Все они располагаются в api/src/Auth/Command. Все эти наши команды
взаимодействуют с нашими:
- доменными сущностями src/Auth/Entity/
- доменными сервисами src/Auth/Services/


Для хранения данных о пользователях и для управления этими данными была введена
сущность [User](./api/src/Auth/Entity/User/User.php)
Внутри класса этой сущности и происходит вся нужная работа над данными.
Сущность User у нас реализована в виде большого агрегата, то есть вместо того,
чтобы просто хранить данные в простых строковых(string) полях класса User мы
ввели обьекты значения такие как Id, Email, Token, Status, Role, NetworkIdentity
все это так же сущности более простые и так же хранящие в себе конкретные данные
в своих приватных полях. Доп сущности - классы обьектов-значений введены для
того, чтобы разгрузить сам класс User от большого количества кода для работы
с примитивными данными (строками). Например была введена сущность Email хранящая
адрес електронной почты. Причем весь код для проверки на валидность адреса и
работы с ним размещаются именно в самом этом классе Email, тем самым разгружая
от этого кода класс User. Такой подход позволяет держать код чистым, и облегчает
развитие и рост кодовой базы - так если надо будет ввести новую логику в виде
новых методов то просто добавляем в класс Email.
Приемущество такого подхода в том, что логика работы с данными "спрятана" и
изолирована как можно ближе к самим данным - в классы этих обьектов-значений.
Так например класс Token, хранящий само строковое значение токена и время его
истечение содержит в себе весь код для валидации и проверки этого токена
и в нашем классе User есть 3 поля для хранения токенов, которые используются в
разных случаях - для подтверждения регистрации, смены емейла и пароля. Причем
т.к. логика работы с данными токена хранится в его классе Token то её не надо
постоянно копировать в самом классе User, достаточно просто вызывать нужные
методы из класса Token.

Таким образом создание отдельных "типов"(классов обьектов-значений) разгрузило
сам класс User. По сути этот класс просто хранит эти обьекты и обеспечивает
их взаимодействие. То есть класс User - это полноценный агрегатор, который
агрегирует внутри себя - в своих приватных полях другие обьекты разных типов
такие как Id,Email,Token,Status и т.д

Теперь нужно сделать систему позволяющую сохранять и доставать все эти данные
из БазыДанных.

Обычно при использовании такого фреймворка как Laravel c ORM Eloquent,
(работа по принципу ActiveRecord-a) принцип работы другой -

#### DB First (Сначала база данных, потом код)

    DB --> Migrations --> Code

Есть готовая БазаДанных и к ней привязываются сущности

Сначала проектируется БД, продумываются поля, таблицы и связи между ними.
Дальше идёт создание таблиц либо вручную, либо с помощью миграций
и только потом уже придумываются некие сущности, которые привязываются к БД.

Здесь же мы работаем по принципу Code First

#### Code First

    Code --> Migrations --> DB

Сначала придумываем сущности, затем подключаем их к БД

Сначала написали весь нужный код, со всеми нужными сущностями и логикой и тестами
затем будем писать миграции и привязывать уже готовые сущности к базе данных

Плюсы такого подхода:
- если бы изначально работали сразу в Laravel ORM Eloquent c ActiveRecord -
подходом(привязка сущности к таблице) то у нас даже мысли бы не возникло
вводить свои типы(классы обьектов-значений) для полей User т.к. мы заранее
знали что они привязаны к конкретным полям таблиц и имеют конкретные типы значений
таких как string int и т.д. Т.е. зная что данные хранятся в плоских таблицах
нам бы и в голову не пришло бы делать вложенные обьекты-типы, которые мы уже
реализовали в нашем коде
 Программируя же сначало код, а потом только привязывая его к бд - тогда и нет
ограничений:
- можно придумывать любые обьекты-типы с нужной вложенностью
- писать нужный и удобный для нас код, а не тот который требует ДБ

То есть код писать приятнее и удобнее но есть и недостатки такого подхода.
Привязывать к БД такие обьекты тяжелее т.к. простые подходы с ActiveRecord уже
не подойдут и нужно нечто более комплесное.
В самых нестандартных случаях может быть нужно писать свою ORM систему
Object Relation Mapper - преобразователь позволяющий конвертировать обьекты
в базу данных и обратно. Но ныне уже есть целый ряд таких систем.
То есть кроме простого Eloquent есть и другие готовые библиотеки позволяющие
работасть с более комплексными обьектами:

## Doctrine https://doctrine-project.org

The Doctrine Project is the home to several PHP libraries primarily focused on
database storage and object mapping.
The core projects are the Object Relational Mapper (ORM)
and the Database Abstraction Layer (DBAL) it is built upon.

Doctine - это готовое решение позволяющее связывать свои сущности с базой данных.
Данная библиотека часто используется в проектах использующих Symfony

Здесь далее мы научимся подключать этут библиотеку к своему проекту и настроем
её так чтобы работа с ней была как можно проще, эффективнее и не такой
ресурсоёмкой.
Но до подключения самой ORM нужно добавить к проекту саму БазуДанных
То есть установить что-то из PostgreSQL, MySQL MariaDB и т.д.

Здесь мы выберим PostgreSQL
Причины:
- это открытая и свободно распостраняемая система
- нет и не будет лицензионных проблем. А значит не придётся в попыхах переводить
свой проект с какой-нить MySQL на MariaDB или другие форки MySQL, если возникнут
проблемы с лицензией.


## Основные отличия PostgreSQL от MySQL:

1) UUID в качестве первичных ключей
Постгресс более предпочтителен т.к. мы в своём коде активно используем UUID -
идентификаторы. А PostgreSQL имеет встроенный тип для поддержки таких UUID
идентификаторов в качестве первичных ключей.
в MySQL же такого нет и придётся писать некие конвертеры, чтобы преобразовывать
строковые значения UUID в бинарные(чтобы использовать как первичные ключи)
тогда как в PostgreSQL этого делать не надо - есть спец тип UUID и нативная его
поддержка.

2) PostgreSQL Построение вычисляемых индексов
Такие индексы бывают полезны при работе с json. Данные хранимые в Json можно
так же проиндексировать и использовать быстрый поиск по этим индексам

3) PostgreSQL поддерживает полнотекстовый поиск без всяких ограничений

Это основные причины того почему выбираем PostgreSQL Для своего проекта



## Установка БазыДанных PostgreSQL

Базу данных можно поставить сразу на локальную машину, а можно и через докер-
контейнер. И так как мы используем докер и для прода и для разработки(dev)
то и БД будем ставить через докер-контейнер

Тонкости работы с БД в проде:
- одни поднимают БД на докере
- другие ставят БД на отдельные настроенные хосты, без доп обёрток в виде докера

В проектах повышенные требования к надёжности и производительности лучше ставить
на железо а не через докер. Аргументы здесь опаска что БД будет медленее работать
через докер-волюмы, нежели напрямую с файловой системой


## Подключаем БД PostgresSQL к проекту

Добавляем новые образы для dev-а и prod-а в файлы docker-compose*.yml

в образе `postgres:13.12-apline` поставляется скрипт, который на основе
перменных окружения(EnvVar) при своём автозапуске проверяет:
- есть ли переменные, описывающие нужную БД, и если есть -
- проверяет есть ли уже сама бд, описанная этими переменными окружения
- если бд еще не создана - создаёт её сам автоматически.
- если БД уже создана, шаг её создания пропускается и уже имеющиеся данные не
  перезатераются.

То есть переменными POSTGRES_USER, POSTGRES_DB, POSTGRES_PASSWORD прописываем
данные на основе которых мы хотим чтобы была создана БД, и при старте контейнера
эта бд будет сама создана скриптом из готового докер-образа postgres.

Для того чтобы обеспечить персистентность - постоянную сохранность данных самих
файлов БД - подключаем волюм (volume) где СУБД PostgreSQL будет хранить свои
файлы. (Напомню что докер контейнеры без волюмов при перезапуске всегда развёрты
ваются в свежем окружении, а старая эмулируемая файлавая система и всё что было
в ней сохранено предыдущим запуском контейнера - теряется. Чтобы этого не
происходило и нужны volume.)

docker-compose.yml:
```
# ...
volumes:
  api-postgres:  # обьявление волюма по его имени
```

Монтирование волюма в конкретный каталог внутри докер-образа
имя-волюма : путь к монтированию
```
api-postgres:
  ...
  volumes:
    - api-postgres:/var/lib/postgresql/data
```

./docker-compose.yml
```yml
version: '3.7'
services:
  # ...

  api-postgres:
    image: postgres:13.12-alpine
    environment:                         # переменные окружения для создания БД
      POSTGRES_USER: app
      POSTGRES_PASSWORD: secret          # заглушка вместо настоящего пароля
      POSTGRES_DB: app                   # имя нужной нам БазыДанных
    volumes:
      - api-postgres:/var/lib/postgresql/data
    ports:
      - "54321:5432"                    # прокидываем порт из контейнера наружу

volumes:
  api-postgres:                        # волюм где будет хранится сами файлы бд
```

Порт 5432 - это порт по умолчанию на котором работает PostgreSQL
Для Dev-разработки нам может быть нужно ходить внутрь бд и смотреть что там
происходит.
Для этого и пробрасываем порт БД из контейнера наружу в локальную машину.

Теперь выполнив `make init` в корне проекта создадутся доп образы и можно
будет по указанному порту сходить и посмотреть что уже есть в БД.

## Взаимодействие с базой данных PostgreSQL стоящей в докер-контейнере
./docs/docker-notes/psql-notes/01-cli.md


## Настройка БД для продакшена

Для прода настройка будет похожей:

./docker-compose-production.yml
```yml
version: '3.7'
services:
  # ...

  api-postgres:
    image: postgres:13.12-alpine
    restart: always
    environment:
      POSTGRES_USER: app
      POSTGRES_PASSWORD: ${API_DB_PASSWORD}   # передача реального пароля
      POSTGRES_DB: app
    volumes:
      - api-postgres:/var/lib/postgresql/data

volumes:
  api-postgres:
```

Размер образа с БД
```sh
docker images
...
postgres  13.12-alpine   60382b4c56ba   9 days ago       232MB
```


## Настройка PHP для работы с БД PostgreSQL (pdo_pgsql)

Для работы с бд через PDO нужно установленное расширение-драйвер для postgresql

./api/docker/development/php-cli/Dockerfile
./api/docker/development/php-fpm/Dockerfile
```Dockerfile
FROM php:8.1-cli-alpine
# RUN apk ...    && pecl install xdebug-3.2.2 ...
# здесь идёт установка xdebug из PECL-а (сборка из исходников)

# установка дайвера pdo_pgsql напрямую через docker-php-ext-install а не из PECL
RUN apk add --no-cache postgresql-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo_pgsql
# ...
```

`docker-php-ext-install` - эта команды не подгружает уже готовые собранные
расширения(бинарники), а скачивает исходники и компилирует их под конкретную,
уже установленную версию php и под ту ОС и для того железа на котором будет
выполняться эта команда. В нашем случае php:8.1 и OC alpine
А для того чтобы эта команда смогла скомпилировать нужное нам расширение из
исходников нужно заранее подключить зависимости нужные для компиляции:
- `postgresql-dev` (библиотека) именно dev-пакеты для компиляции
- `docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql` - этой командой
идёт настройка компиляции нужного нам расширения.


Теперь можно пересобрать наши образы и проверить их работу
```sh
make init
```
Без оптимизации размеры образов сильно выросли
```sh
docker images
```
```
REPOSITORY           TAG            IMAGE ID       CREATED          SIZE
site-api-php-fpm     latest         2939c1ecea86   8 minutes ago    567MB
site-api-php-cli     latest         2bccdb080727   12 minutes ago   589MB
```

## Оптимизация размера образов. Убираем из образа всё лишнее
./docs/13-doctrine/02-pdo_pgsql-opti.md


Драйвер для подключения к БД готов и теперь можно ставить любую ORM


