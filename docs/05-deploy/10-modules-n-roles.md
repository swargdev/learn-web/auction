## Модульная структура каталога provisioning

Модульная структура каталога ./provisioning естественным путём вытекает из
необходимости поддерживать работу с серверами разных типов.

Для реализации модульной структуры в Ansible есть понятие роль(role).
Каждые пачки таких каталогов как envs, files, tasks и им подобные можно
разнести на отдельные модули, так называемые роли - и дать им уникальные имена.

Например создадим роль docker задача которой собрать в себе всё необходимое
для поддержание состояния на серверах на которых нужен докер.

./provisioning/roles/     каталог для ролей, в нём подкаталоги - имена ролей
  - docker                роль "docker"
      - handlers          подкаталог внутри роли - он автоматом будет включатся
                          в yml файл где указанна роль по её имени.
           main.yml       обязательный файл автоматически запускаемый для этой роли
      - tasks
          - main.yml

Все задачи(таски) для докера из provisioning/task/setup_docker.yml переносим
в роль "docker" в подкаталог tasks в файл main.yml - Этот файл будет запускаться
по умолчанию для этой роли под именем `docker`.
в этот ./provisioning/roles/docker/tasks/main.yml пойдут все задачи для работы
с докером и установкой команды крона для переодическая очистки докер-обьектов.

также если хочется можно разделять main.yml на несколько файлов и подтягивать
части другие yml-файлы через import_tasks.

у нас в докере есть хэндел, проверяющий доступность докера - его перенесём в
./provisioning/roles/docker/handlers/main.yml

Таким образом каталог `./provisioning/roles/docker/` - это отдельный модуль
для установки докера. И для серверов, где нужен докер будет достаточно
просто указать роль `docker` и все таски этой роли будут применятся "сами".

```
provisioning/roles/docker/
├── handlers
│   └── main.yml
└── tasks
    └── main.yml
```

Дальше создадим отдельный модуль для certbot `./provisioning/roles/certbot/`

```
provisioning/roles/certbot/
├── files                       # все конфиг файлы так же переносим внутрь роли
│   └── cli.ini
└── tasks
    └── main.yml
```

При этом весь наш site.yml playboot скрипт описывается теперь так:
```yml
---
- name: Provision Site       # задаём секцию с именем `Provision Site`
  hosts: site                # эта секция работает только с хостами группы site
  remote_user: root
  roles:                     # для сех хостов этой грыппы применять эти роли
    - docker                 # все задачи из каждой из этих ролей подтягиваются
    - certbot                # из соответствующего каталога .roles/*/tasks/main.yml
    - site                   # и будут применены при выполнении этого скрипта
```

Теперь перепишем файл с хостами из такого вида
./provisioning/hots.yml:
```yml
all:
  hosts:
    server:
      ansible_connection: ssh
      ansible_user: root
      ansible_host: 0.0.0.0       # Здесь конкретный адрес сервера куда деплоим
      ansible_port: 22
```

в более продвинутую структуру используя `children`:
```
all:
  children:                        # здесь указываем вложенные подкатегории
    site:                          # имя 1й категории (группы серверов)
      hosts:
        server:
          ansible_connection: ssh
          ansible_user: root
          ansible_host: 0.0.0.0
          ansible_port: 22
    db:                            # имя 2й категории
      hosts:
        server:
          ansible_connection: ssh
          ansible_user: root
          ansible_host: 0.0.0.0
          ansible_port: 22
    queue:                          # имя 3й категории
      hosts:
        server:
          ansible_connection: ssh
          ansible_user: root
          ansible_host: 0.0.0.0
          ansible_port: 22
```

этим мы можем сделать сколько надо разных групп хостов и группировать их
по соответствующим категориям и далее в плейбуке site.yml сделать разные задачи
под каждую из описанных категорий:

Пример возможного playbook-скрипта:
```yml
---
- name: Provision Site       # 1я секция
  hosts: site                # работает только с хостами из группы site в hosts.yml
  remote_user: root
  roles:                     # роли которые должны применятся для всех серверов
    - docker                 # данной секции т.е. из группы site
    - certbot
    - site

- name: Provision DB         # 2я секция - уже другие роли для других серверов
  hosts: db                  # работает только с хостами из группы db в host.yml
  remote_user: root
  roles:
    # - docker               # если базу данных ставим напрямую то докер и не нужен
    - db                     # отдельная роль по подьёму базы данных
    - db-master

- name: Provision DB         # 2я секция - уже другие роли для других серверов
  hosts: db-slave            # отдельные сервера с репликациями баз данных
  remote_user: root
  roles:
    - db                     # опять таки роль мы выносив в отдельный каталог
    - db-slave               # который размещаем в каталог roles
```

Таким образом роли очень удобный инструмент для разделения нашего provisioning
кода на отдельные части - модули, которые затем можно переиспользовать в своих
скриптах.
Удобство ролей еще и в том, что их можно подгружать через системы зависимостей,
выкладывать на свой гитлаб/гитхаб и подгружать по сети от туда.
Таким образом ролями можно делится и переиспользовать их.
Принцип повторного переиспользования кода.


Таким образом переписав provisioning по модульному принципу мы:
- переписали инвентарь хостов указав children.site.hosts  в файле
  ./provisioning/hosts.yml

- сделали отдельную роль `site` разместив в ней всё что по работе с сайтом
  (создание deploy-юзера и генерация новых сертификатов)
  причем в главном файле роли roles/site/main.yml просто идёт инклюд задач вот так:

```yml
- import_tasks: generate_certificates.yml
- import_tasks: create_user.yml
```


переменные нужные для генерации сетификатов перемещаем
из  `envs/certbot.yml` в `roles/site/defaults/main.yml`

просто убрав секцию по подключению файла с переменными из site.yml
```yml
  vars_files:
    - envs/certbot.yml
```
Путь roles/<name>/defaults/main.yml - для переменных по умолчанию
defaults mean “default variables for the roles”
and vars mean “other variables for the role”

- все скрипты касающиеся site переписали с hosts:all на host:site вот так:

./provisioning/authorized.yml:
```yml
- name: Add authorized key
  hosts: site                  # << поменяли с all на site
  remote_user: root
```

Это делается для того чтобы эти скрипты применялись только к серверам из группы
`site` и не применялись к серверам из других будущих групп, например `db`.

Так же могут быть и общие роли, например для apt-update apt-upgrade если надо
чтобы обновление применялось ко всем серверам.

Модульная структура с использованием ролей теперь выглядит так:
```
provisioning/
├── roles
│   ├── certbot
│   │   ├── files
│   │   │   └── cli.ini
│   │   └── tasks
│   │       └── main.yml
│   ├── docker
│   │   ├── handlers
│   │   │   └── main.yml
│   │   ├── site
│   │   │   └── tasks
│   │   └── tasks
│   │       └── main.yml
│   └── site
│       ├── defaults
│       │   └── main.yml
│       └── tasks
│           ├── create_user.yml
│           ├── generate_certificates.yml
│           └── main.yml
├── hosts.yml.dist
├── hosts.yml
├── authorize.yml
├── docker-login.yml
├── renew-certificates.yml
└── site.yml
```


