## laminas/laminas-config-aggregator библиотека для склейки конфиг-массивов
(старое название zend-config-aggregator)

https://docs.laminas.dev/laminas-config-aggregator/
https://docs.laminas.dev/laminas-config-aggregator/intro/

Позволяет склеивать конфигурационные массивы более продвинутым способом

composer require laminas/laminas-config-aggregator

```sh
docker compose run --rm api-php-cli composer require laminas/laminas-config-aggregator
```
Умеет читать конфиги из разных источников(провайдеров) не только из php-скриптов

Вот например провайдер для php-скриптов
`Laminas\ConfigAggregator\PhpFileProvider`

умеет еще работать и с ini, xml, yml, json и другими подобными провайдерами
в резаультате своей работы он соединяет все конфиги(массивы) в один общий
большой массив получить доступ к которому можно через
$aggregator->getMergedConfig()

Together with laminas-config, laminas-config-aggregator can be also used to load
configuration in different formats, including YAML, JSON, XML, or INI:

Принцип его работы такой же как мы делали без него
Сгребсти из разных файлов массивы и соединить все их в один общий.
Именно поэтому каждый конфиг файл  должен возвращать массив
(Using PhpFileProvider, each file should return a PHP array)

```php
<?
// db.global.php
return [
    'db' => [
        'dsn' => 'mysql:...',
    ],
];

// cache.global.php
return [
    'cache_storage' => 'redis',
    'redis' => [ ... ],
];
```
Result:
```
array(3) {
  'db' =>
      array(1) {
        'dsn' =>
        string(9) "mysql:..."
      }
  'cache_storage' => string(5) "redis",
      'redis' =>
          array(0) {
             ...
          }
}
```

Так же данная библиотека поддерживает продвинутые способы соединения этих самых
массивов и их элементов.


Перепишем наш основной конфиг файл где идёт сбока всех конфиг-файлов в один
общий, причем в зависимости от того какое окружение указано(APP_ENV)

Старая версия основанная на функции `array_replace_recursive`

./api/config/dependencies.php
```php
<?php
// здесь мы сгребали имена всех файлов в один массив
$files = array_merge(
    glob(__DIR__ . '/common/*.php') ?: [],
    glob(__DIR__ . '/' . (getenv('APP_ENV') ?: 'prod') . '/*.php') ?: []
);
// сдесь читали из всех имён файлов массивы в них прописанные
$configs = array_map(
    static function (string $file): array {
        return require $file;
    },
    $files
);
// обьединяли все массивы в один общий
return array_replace_recursive(...$configs);
```

То же самое но уже используя библиотеку laminas-config-aggregator

./api/config/dependencies.php
```php
<?
use Laminas\ConfigAggregator\ConfigAggregator;
use Laminas\ConfigAggregator\PhpFileProvider;
// говорим где брать файлы и какой формат используем (php)
$aggregator = new ConfigAggregator([
    new PhpFileProvider(__DIR__ . '/common/*.php'),
    new PhpFileProvider(__DIR__ . '/' . (getenv('APP_ENV') ?: 'prod') . '/*.php')
]);
// получаем один общий массив
return $aggregator->getMergedConfig();
```

Теперь можно убрать дублирование команд для dev-окружения
так как с этой библиотекой элементы массива-списка будут не перезаписывается,
а соединятся в один общий массив.

./api/config/dev/console.php


## Как переопределять ключи в массиве при их слиянии
По умолчанию при слиянии двух массивов в один общий элементы одноимённых
массивов склеиваются в один, а не перазаписываются как это делает функция
`array_replace_recursive`. Для того чтобы изменить это поведение, так чтобы
элементы в новом массиве переопеределили уже имеющиеся элементы в том же массиве.

```php
<?
$array_from_file_1 = [ config => [1, 2] ];

$array_from_file_2 = [ config => [3] ]

# по умолчанию даст
$merged_array = [ config => [1, 2, 3] ];

# а нужно
$merged_array = [ config => [3] ];
```

Делается это вот так:
```php
<?
return [
  'config' => [
    'console' => [
      'commands' => new \Laminas\StdLib\ArrayUtils\MergeReplaceKey([
        // здесь указываем новое значение которым нужно переопределить старое.
      ]),
    ]
  ]
]
```


