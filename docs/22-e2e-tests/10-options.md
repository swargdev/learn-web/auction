## Ускорение выполнения тестов через параллельный запуск

для этого используется флаг --parallel
в нём указываем число паралельно запускаемых браузеров
```js
{
  "license": "MIT",
  "scripts": {
    "e2e": "cucumber-js --parallel 2",
    "lint": "eslint --ext .js,.jsx features",
    "lint-fix": "eslint --fix --ext .js,.jsx features"
  },
```

## Прописываение таймаута

тесты могут падать и просто потому что для работы не хватит дефолтного
таймаута в 5 секунд.


```js
// пытаться два раза и таймаут не 5 а 30 секунд
const opts = { wrapperOptions: { retry: 2 }, timeout: 30000 }
When('I open {string} page', opts, async function (uri) {
  return await this.page.goto('http://gateway:8080' + uri);
});
```


