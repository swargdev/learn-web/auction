- Реализуем сервис JoinConfirmationSender для работы через Symfony/Mailer
- Проблема как указывать абсолютную ссылку для токена подтверждения
- Регистрация класса JoinConfirmationSender в DI-контейнере
- Переписываем MailerCheckCommand на использование JoinConfirmationSender
- Unit test-ы для JoinConfirmationSender делаем хитрный mock для метода send
- Линия рассуждений для написания теста под сервис JoinConfirmationSender
- Mock как способ проверить поведение нудного обьекта
- Весь код первого варианта теста JoinConfirmationSender
- Проверим реально ли тесты будут выявлять ошибки в коде
- Почему делали прокидывание адреса отправителя письма (from)
- Меняем относительные адреса на абсолютные с именем хоста.
- Вариант через конкатенацию строк
- FrontendUrlGenerator - cпецилизированный обьект для генерации ссылок
- Почему явная фабрика класса JoinConfirmationSender больше ну нужна
- Переписываем наш Unit-тест на использование FrontendUrlGenerator


## Реализуем сервис JoinConfirmationSender для работы через Symfony/Mailer

Интерфейс-заглушка до реализации:
```php
<?php

declare(strict_types=1);

namespace App\Auth\Service;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\Token;

interface JoinConfirmationSender
{
    public function send(Email $email, Token $token): void;
}
```

интерфейс меняем на класс. Хотя можно было бы и оставить сам интерфейс и просто
добавить новый класс реализующий данный интерфейс, и в настройках DI-контейнера
прописать чтобы да этого интерфейса создавался реализующий его класс.
Здесь же для простоты просто будем код писать сразу в классе вообще убрав
интерфейс-заглушку

Если бы мы не прописывали через слушателя события автоматическое добавление
адреса отправителя, то в конструкторе нужно было бы передавать как сам мейлер
так и адрес отправителя($from):

```php
<?
class JoinConfirmationSender
{
    private MailerInterface $mailer;
    private array $from;

    public function __construct(MailerInterface $mailer, array $from)
    {
        $this->mailer = $mailer;
        $this->from = $from;
    }
```
но так как адрес отправителя у нас будет подставляться сам достаточно просто:

```php
<?
class JoinConfirmationSender
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer, array $from)
    {
        $this->mailer = $mailer;
    }
```

NOTE
так же обрати внимание на новый синтаксис введённый с версии php 8.0
детально об этом [здесь](./docs/php-syntax/01-constructor-properties.md)

краткое обявление полей класса прямо в конструкторе:
```php
<?
final class JoinConfirmationSender
{
    // добавим модификатор области видимости (здесь это private)
    // говорим что это будет поле данного класса
    public function __construct(private readonly MailerInterface $mailer)
    {
        // при этом явно писать присвоение полю класс не нужно - сделается само
    }
```
Всё же пока не использую такой "модный" синтаксис для того чтобы оставить
привычную высокую читабельность кода. Пока впечатления такие что это хоть и
удобно за счёт своей краткости, но сильно снижает читаемость кода.
(самих полей класса не видно, их как бы нет, да и присвоение тоже скрыто от глаз)


в итоге сервис будет выглядить так:
```php
<?

namespace App\Auth\Service;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\Token;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email as MimeEmail;

class JoinConfirmationSender
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(Email $email, Token $token): void
    {
        $message = (new MimeEmail())
            // ->from(..)                     от кого подставится автоматически
            ->subject('Join Confirmation')           // тема письма
            ->to($email->getValue())                 // кому отправляем
        ->text('/join/confirm?'. http_build_query([  // текст письма - токен
            'token' => $token->getValue(),
        ]));

        $this->mailer->send($message);
    }
}
```

Вообще суть этого сервиса - отправить ссылку для подтверждения регистации
здесь пока пишем просто относиетльную ссылку.


## Проблема как указывать абсолютную ссылку для токена подтверждения

Вообще же эта ссылка должна вести не на api а на frontend и эта ссылка должна
быть абсолютной.
Для локальной разработки было бы допустимо написать

    ->text('http://localhost:8080/join/confirm?'. ...

и по этому адресу должна открываться страница Join Confirm в неё нужно будет
передвать токен подтверждения и этот фронтенд должен будет передвать этот токен
в API для подтверждения регистрации

Но это пойдёт только для dev-Локальной разработки т.к. для прода само собой
нужно будет вписывать настоящий адрес сайта.

Пока для первого теста по проверки отправки письма оставим пока относительный
путь.

## Регистрация класса JoinConfirmationSender в DI-контейнере

Для того чтобы использовать JoinConfirmationSender например в MailerCheckCommand
нужно зарегать класс этого сервиса в DI-контейнере. чтобы он мог при запросе
класса JoinConfirmationSender выдавать готовый к работе обьект(инстанс)

```php
<?php

declare(strict_types=1);

use App\Auth\Service\JoinConfirmationSender;
use Psr\Container\ContainerInterface;
use Symfony\Component\Mailer\MailerInterface;

return [

    JoinConfirmationSender::class =>
    static function (ContainerInterface $container): JoinConfirmationSender {
        /** var Mailer $mailer */
        $mailer = $container->get(MailerInterface::class);
        /**
          * @psalm-suppress MixedArrayAccess
          * @psalm-var array{from:array} $mailerConfig
          */
        $mailerConfig = $container->get('config')['mailer'];
        return new JoinConfirmationSender($mailer); //, $mailerConfig['from']);
    },
];
```

## Переписываем MailerCheckCommand на использование JoinConfirmationSender


```php
<?php

declare(strict_types=1);

namespace App\Console;

use Symfony\Component\Console\Command\Command ;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Component\Mime\Email;

class MailerCheckCommand extends Command
{
    private JoinConfirmationSender $sender;

    public function __construct(JoinConfirmationSender $sender)
    {
        parent::__construct();
        $this->sender = $sender;
    }

    protected function configure(): void
    {
        $this->setName('mailer:check');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('<info>Sending</info>');

        $this->sender->send(
            new Email('user@app.test'),
            new Token(Uuid:uuid4()->toString(), new DateTimeImmutable())
        );

        $output->writeln('<info>Done.</info>');

        return 0;
    }
}
```

Проверяем:

```sh
docker compose run --rm api-php-cli composer app mailer:check
> php bin/app.php --ansi 'mailer:check'
Sending
Done.
```

http://localhost:8082/#
```
From 	Auction <mail@app.test>
Subject 	Join Confirmation
To 	user@app.test

    Plain text
    Source

/join/confirm?token=96be25aa-f90d-4a34-86f0-bed26249b879
```

Работает как и задумано
Auction - имя сайта, подставляется к адресу From
тело письма содержит пока что относительную ссылку с созданым токеном:
`/join/confirm?token=96be25aa-f90d-4a34-86f0-bed26249b879`

а значит сервис отправки писем подтверждения регистрации уже работает и способен
слать их куда надо. Остнется только вписать prod-адрес почтового сервера(мейлера)

## Тесты для сервиса JoinConfirmationSender

внешние - функциональные или интеграционные тесты должны проверять отправку
писем используя внешние инструменты.
В частности через обращение к MailHog мейлеру, запросив у него дошедшие
до него из нашего кода письма и проверив тело письма на правильность(ч\з парсинг)


## Unit test-ы для JoinConfirmationSender делаем хитрный mock для метода send

юнит тесты для сервисов, в нашем случае для JoinConfirmationSender будут
отличаться от обычных юнит тестов для например для сущностей.
потому что сервис не имеет состояние как сущность или инстанс класса с полями.
А значит не хранит что и куда он там отправил и что делал.
просто делает свою работу и всё. А значит нельзя так же как для сущности проверить
отправку письма через простой вызов геттера "покажи что там у тебя внутри".
как это можно сделать для класса сущности хранящей своё состояние в приватном поле.

Поэтому простым stub-ом для написания юнит теста для JoinConfirmationSender
уже не отделаться. Нужно более хитрый способ.

## Линия рассуждений для написания теста под сервис JoinConfirmationSender

Рассуждения для написания тестов, как сделать проверку отправки почты?

Смотрим только на методы класса JoinConfirmationSender:

```php
<?
class JoinConfirmationSender
{
    public function __construct(MailerInterface $mailer) {}

    public function send(Email $email, Token $token): void {}
}
```

есть конструктор принимающий мейлер и есть метод отпраки токена на указанный адрес.
смотря на подобный интерфейс(внешний вид класса только методы и их сигнатуры)
уже можно предположить что должен делать обьект данного класса то есть  какое у
него назначение.

если в конструктор передаётся мейлер то значит именно через него и должно
отправляться письмо, а значит нужно проверить будет ли передаваться
в этот заданный мейлер, письмо и с какими конкретно значениями.

## Mock как способ проверить поведение нудного обьекта

А для этого обычно делается mock-"шпион" - это своего рода посредник, муляж
реального обьекта нужного класса или интерфейса, который можно подставить в
проверяемый обьект для проверки его работы. В частности в нашем случае этот мок
должен проверять какое письмо будет передаваться из проверяемого нами сервиса.


Приступаем к написанию теста (Заготовка)
./api/src/Auth/Test/Unit/Service/JoinConfirmationSenderTest.php
```php
<?php

declare(strict_types=1);

namespace App\Auth\Test\Unit\Service;

use App\Auth\Service\JoinConfirmationSender;
use PHPUnit\Framework\TestCase;

/**
 * @covers JoinConfirmationSender
 */
class JoinConfirmationSenderTest extends TestCase
{
    public function testSuccess(): void
    {
        $sender = new JoinConfirmationSender($mailer);
        $sender->send($to, $token);                // куда и что слать в письме
    }
}
```

Добавляем данные для отправки
```php
<?
    $from = [ email => 'test@app.test', name => 'Test'];
    $to = Email('user@app.test');
    $token = new Token(Uuid::uuid4()->toString(), new DateTimeImmutable());

    $sender = new JoinConfirmationSender($mailer);
    $sender->send($to, $token);
```

Остаётся добавить mailer который будет способен делать проверки его вызова
и через который можно будет получить инфу о том как его вызвали и что передали.

Здесь мы проверяем отправку ссылки на подтверждение,
а значит нужно будет её проверять(confirmUrl):


```php
<?
    # ...
    $token = new Token(Uuid::uuid4()->toString(), new DateTimeImmutable());
    $confirmUrl = '/join/confirm?token='. $token->getValue();
```

Создаём mock над `MailerInterface` для проверки правильности отправки писем.
mock - это своего рода хитрая заглушка с возможностью прослушать поведение
метода проверяемого обьекта.

```php
<?
$mailer = $this->createMock(MailerInterface::class);
// ожидаем что метод `send` будет вызван обязательно ровно один раз `::once()`
$mailer->expects(self::once())->method('send')
    ->willReturnCallback(
    // подменяем метод send на свою анонимную функцию для проверки переданного
    // $message - это тот обьект который будет отправлять внутри себя наш мейлер
    static function (MimeEmail $message) use ($to, $confirmUrl): void {
        self::assertEquals([], $message->getFrom()); // отправителя нет
        self::assertEquals($to->getValue(), $message->getTo()[0]->getAddress());
        self::assertEquals('Join Confirmation', $message->getSubject());
        self::assertEquals($confirmUrl, $message->getTextBody());
    });
```
здесь идёт проверка:
- (адрес отправителя - пустой т.к. видимо хук на событе отправки не отрабатывает)
- адрес получателя
- темы письма
- текcта письма (confirmUrl) - в нём должна быть ссылка с токеном

Так же обрати внимание на то как идёт проброс локальных перменных:
$to и $confirmUrl внурь анонимной функции передаётся через
`use ($to, $confirmUrl)` без этого работать не будет.

`$message->getTo()[0]->getAddress()` первый т.к. адресов может быть  много

вторым тестовым методом нужно проверять случай с ошибкой
но его можно и не писать т.к. при ошибке всегда будет просто бросаться
исключение `TransportExceptionInterface`

## Весь код первого варианта теста JoinConfirmationSender

```php
<?php

declare(strict_types=1);

namespace App\Auth\Test\Unit\Service;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\Token;
use App\Auth\Service\JoinConfirmationSender;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email as MimeEmail;

/**
 * @covers JoinConfirmationSender
 */
class JoinConfirmationSenderTest extends TestCase
{
    public function testSuccess(): void
    {
        $from = [ 'email' => 'test@app.test', 'name' => 'Test'];
        $to = new Email('user@app.test');
        $token = new Token(Uuid::uuid4()->toString(), new DateTimeImmutable());
        $confirmUrl = '/join/confirm?token=' . $token->getValue();

        $mailer = $this->createMock(MailerInterface::class);
        $mailer->expects(self::once())->method('send')
            ->willReturnCallback(
            static function (MimeEmail $message) use ($to, $confirmUrl): void {
                self::assertEquals($to->getValue(), $message->getTo()[0]->getAddress());
                self::assertEquals('Join Confirmation', $message->getSubject());
                self::assertEquals($confirmUrl, $message->getTextBody());
            });

        $sender = new JoinConfirmationSender($mailer);

        $sender->send($to, $token);
    }
}
```

Запуск теста
```sh
make test
docker compose run --rm api-php-cli composer test
> phpunit --colors=always
PHPUnit 10.4.1 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.26
Configuration: /app/phpunit.xml

........................................................... 65 / 67 ( 97%)
..                                                          67 / 67 (100%)

Time: 00:00.092, Memory: 14.00 MB

OK (67 tests, 121 assertions)
```


## Проверим реально ли тесты будут выявлять ошибки в коде:

задача тестов - сразу вявлять случайные ошибки в коде, например случайное
удаление строки. здесь эмулируем подобную ситуацию и проверяем реально ли тест
покажет что  что-то не так

./api/src/Auth/Service/JoinConfirmationSender.php

```php
<?
    public function send(Email $email, Token $token): void
    {
        $message = (new MimeEmail())
        ->subject('Join Confirmation')
        // ->to($email->getValue())                   //<< эмуляция ошибки "забыли"
        ->text('/join/confirm?' . http_build_query([
            'token' => $token->getValue(),
        ]));

        $this->mailer->send($message);
    }
```

```
make test
docker compose run --rm api-php-cli composer test
> phpunit --colors=always
PHPUnit 10.4.1 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.26
Configuration: /app/phpunit.xml

E................................................................ 65 / 67 ( 97%)
..                                                                67 / 67 (100%)

Time: 00:00.091, Memory: 14.00 MB

There was 1 error:

1) App\Auth\Test\Unit\Service\JoinConfirmationSenderTest::testSuccess
Error: Call to a member function getAddress() on null

/app/src/Auth/Test/Unit/Service/JoinConfirmationSenderTest.php:33
/app/src/Auth/Service/JoinConfirmationSender.php:30
/app/src/Auth/Test/Unit/Service/JoinConfirmationSenderTest.php:40

ERRORS!
Tests: 67, Assertions: 117, Errors: 1, Warnings: 1.
Script phpunit --colors=always handling the test event returned with error code 2
```

Да реально тест падает при попытке получить адрес т.к. он не задаётся в коде.
```php
<?
/* 33 */self::assertEquals($to->getValue(), $message->getTo()[0]->getAddress());
```


## Почему делали прокидывание адреса отправителя письма (from)

Адрес отправителя у нас настраивается один раз для всего проекта и больше не
меняется. Сервисов по отправки писем у нас будет несколько, и в каждом из них
постоянно писать from не удобно. Именно поэтому и добавляли хук на событие
отправки письма для того чтобы адрес прописывался автоматически.


## Меняем относительные адреса на абсолютные с именем хоста.

- добавляем в файлы docker-compose.yml новую переменную окружения
  под хранение адреса сайта(фронтенда) взятого из переменной окружения

      FRONTEND_URL: http://localhost:8080  - для docker-compose.yml (dev)
      FRONTEND_URL: ${FRONTEND_URL}        - для docker-compose-production.yml

Для прода нужно будет передовать реально доменное имя сайта.
по аналогии можно будет сделать docker-compose-stating.yml под stating-сервер
и передавать в него другой реальный доменный адрес сайта на котором и будет
крутиться этот staging-сервер.

Добавляем новый конфигурационный файл, куда и будем прописывать урл на сайт.
./api/config/common/frontend.php

```php
<?php

declare(strict_types=1);

return [
    'config' => [
        'frontend' => [
            'url' => getenv('FRONTEND_URL'),
        ],
    ],
];
```

## Вариант через конкатенацию строк

Простейший вариант использования этого урла из конфига:

```php
<?php
class JoinConfirmationSender
{
    private MailerInterface $mailer;
    private string $frontendUrl;

    public function __construct(MailerInterface $mailer, string $frontendUrl)
    {
        $this->mailer = $mailer;
        $this->frontendUrl = $frontendUrl; // сохряняем его приняв от DI-контейнера
    }

    public function send(Email $email, Token $token): void
    {
        $message = (new MimeEmail())
        ->subject('Join Confirmation')
        ->to($email->getValue())
        // просто добавляем адрес сайта в начала создаваемого адреса
        ->text($this->frontendUrl . '/join/confirm?' . http_build_query([
            'token' => $token->getValue(),
        ]));

        $this->mailer->send($message);
    }
}
```
И соответственно добавляем его в фабрику по которой DI-контейнер будет создавать
инстанс класса JoinConfirmationSender которому нужно будет передвать урл

./api/config/common/auth.php

```php
<?
return [

    JoinConfirmationSender::class =>
    static function (ContainerInterface $container): JoinConfirmationSender {
        /** @var MailerInterface $mailer */
        $mailer = $container->get(MailerInterface::class);
        /**
          * @psalm-suppress MixedArrayAccess
          * @psalm-var array{url:string} $frontendUrl
          */
        $frontendConfig = $container->get('config')['frontend'];

        return new JoinConfirmationSender($mailer, $frontendConfig['url']);
        //            передача урла в конструкто класса ^^^^^^^^^^^^^^^^^
    },
];
```

## FrontendUrlGenerator - cпецилизированный обьект для генерации ссылок

- создаём специализированный класс FrontendUrlGenerator,
  обьект которого будет генерировать абсолютный адрес для настроенного домена
  делая полные пути на основе относительных путей.
- вместо строк в конструкторы сервисов можно будет передавать уже этот обьект


```php
<?php
class JoinConfirmationSender
{
    private MailerInterface $mailer;
    private string $frontend;

    public function __construct(
        MailerInterface $mailer,
        FrontendUrlGenerator $frontend
    ) {
        $this->mailer = $mailer;
        $this->frontend = $frontend;
    }

    public function send(Email $email, Token $token): void
    {
        $message = (new MimeEmail())
        ->subject('Join Confirmation')
        ->to($email->getValue())
        ->text($this->frontend->generate('join/confirm', [
            'token' => $token->getValue(),
        ]));

        $this->mailer->send($message);
    }
}
```

Делаем сам класс `./api/src/Frontend/FrontendUrlGenerator.php`

```php
<?php

declare(strict_types=1);

namespace App\Frontend;

class FrontendUrlGenerator
{
    private string $baseUrl;

    // сюда через DI-контейнер будет приходит название домена(ссылка)
    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl; // ссылка на фронтенд сайта
    }

    /**
     * @param array<string,string> $params
     */
    public function generate(string $uri, array $params = []): string
    {
        return $this->baseUrl
            . ($uri ? '/' . $uri : '') // если не пустой uri - добавить к ссылке
            . ($params ? '?' . http_build_query($params) : '');
    }
}
```

Его задача просто собирать полные ссылки на основе переданного uri и query-str

регистрируем фабрику по инстанцированию этого класса в обьект
./api/config/common/frontend.php

```php
<?
return [

    FrontendUrlGenerator::class =>
    static function (ContainerInterface $container): FrontendUrlGenerator {
        /**
         * @psalm-suppress MixedArrayAccess
         * @psalm-var array{url:string} $config
         */
        $config = $container->get('config')['frontend'];

        return new FrontendUrlGenerator($config['url']);
    },

    'config' => [
        'frontend' => [
            'url' => getenv('FRONTEND_URL'),
        ],
    ],
];
```

Изменяем фабрику создания сервиса JoinConfirmationSender
на передачу в него не строки а обьекта генерирующего url-адрес

```php
<?
return [

    JoinConfirmationSender::class =>
    static function (ContainerInterface $container): JoinConfirmationSender {
        /** @var MailerInterface $mailer */
        $mailer = $container->get(MailerInterface::class);

        /** @var FrontendUrlGenerator $frontend */
        $frontend = $container->get(FrontendUrlGenerator::class);

        return new JoinConfirmationSender($mailer, $frontend);
    },
];
```

## Почему явная фабрика класса JoinConfirmationSender больше ну нужна

Но так как у нас здесь конструктор класса JoinConfirmationSender принимает
именно конкретные классы а не интерфейсы, то DI-контейнер вполне сам сможет
сообразить (через рефлексию) откуда брать и как собирать нужные обьекты.
То есть если из конструктора и так понятно что туда нужно передавать и откуда
это брать то такую адачу DI-контейнер способен решить сам без доп. фабрик.
А т.к. у нас нужные конструктору обьекты уже описаны:
- MailerInterface
- FrontendUrlGenerator
то это значит, что описание фабрики JoinConfirmationSender вообще не нужно.
то есть его можно полностью удалить и это будет работать под капотом
стандартного функционала DI-контейнера.

Фабрики нужны только для тех классов, конфигурирование которых хотябы немного
отличается от стандартного - например что-то подхватить из массива конфига.

Здесь же конструктор класса JoinConfirmationSender позволяет через автовайринг
подставлять заранее описанные обьекты нужных классов, а значит описывать фабрику
для него и ему подобных классов вообще не нужно.



## Переписываем наш Unit-тест на использование FrontendUrlGenerator

```php
<?php
class JoinConfirmationSenderTest extends TestCase
{
    public function testSuccess(): void
    {
        $from = [ 'email' => 'test@app.test', 'name' => 'Test'];
        $to = new Email('user@app.test');
        $token = new Token(Uuid::uuid4()->toString(), new DateTimeImmutable());
        $confirmUrl = '/join/confirm?token=' . $token->getValue();

        // -+-- заглушка (moсk) для генератора ссылок
        $frontend = $this->createMock(FrontendUrlGenerator::class);
        $frontend->expects($this->once())->method('generate')->with(
            $this->equalTo('join/confirm'),
            $this->equalTo(['token' => $token->getValue()]),
        )->willReturn($confirmUrl);
        // -+--

        $mailer = $this->createMock(MailerInterface::class);
        $mailer->expects(self::once())->method('send')
        ->willReturnCallback(static function (MimeEmail $message) use ($to, $confirmUrl): void {
                self::assertEquals([], $message->getFrom());
                self::assertEquals($to->getValue(), $message->getTo()[0]->getAddress());
                self::assertEquals('Join Confirmation', $message->getSubject());
                self::assertEquals($confirmUrl, $message->getTextBody());
            });

        $sender = new JoinConfirmationSender($mailer);

        $sender->send($to, $token);
    }
}
```

Здесь мы добавляем вторую заглушку для FrontendUrlGenerator
суть работы этой заглушки(мока):

        $frontend->expects($this->once())->method('generate')

один раз должен вызваться метод generate()
и в этот метод должны быть переданы два параметра

        ->with(
            $this->equalTo('join/confirm'),
            $this->equalTo(['token' => $token->getValue()]),
        )
и должен этот метод вернуть строку $confirmUrl:

        ->willReturn($confirmUrl);

Ну и возвращаему строку мы задаём вот так:

        $frontendUrl = 'http://test';
        $confirmUrl = $frontendUrl . '/join/confirm?token=' . $token->getValue();

Так же передаём frontend-генератор в конструктор обьекта-сервиса:

        $sender = new JoinConfirmationSender($mailer, $frontend);

Так же напишем тест для самого FrontendUrlGenerator
/api/src/Frontend/Test/FrontendUrlGeneratorTest.php
для трех случаев:
- собрать ссылку с пустыми параметрами
- собрать ссылку только с путём
- собрать ссылку и с путём и с query-параметрами
- собрать ссылку только с query-параметрами


Следующим шагом будет генерация писем в виде html-страниц
