<?php

declare(strict_types=1);

namespace App\Auth\Command\JoinByEmail\Confirm;

use Symfony\Component\Validator\Constraints;

class Command
{
    #[Constraints\NotBlank]
    public string $token = '';
}
