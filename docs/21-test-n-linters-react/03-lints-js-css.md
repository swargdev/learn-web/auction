## Устанавливаем линтеры для автоматизации проверки js и css кода

- eslint - для ECMAScript т.е. JS
- stylelint - для проверки CSS-кода


 для нашего api на php мы устанавливали
- линтер для проверки корректности написания кода и
- codesniffer для проверки стиля кода

Теперь такие же интструменты нам нужны для frontend-а на React - т.е. для
js и css

## ESLint http://eslint.org линтер для спеки ECMAScript которую реализует js

 Find and fix problems in your JavaScript code

ESLint statically analyzes your code to quickly find problems.
It is built into most text editors and you can run `ESLint` as part of your
continuous integration pipeline.

## https://eslint.org/docs/latest/use/getting-started

ESLint is a tool for identifying and reporting on patterns found in
`ECMAScript/JavaScript` code, with the goal of making code more consistent and
avoiding bugs.

ESLint is completely pluggable. Every single rule is a plugin and you can add
more at runtime. You can also add community plugins, configurations,
and parsers to extend the functionality of ESLint.

> Prerequisites

To use ESLint, you must have Node.js (^12.22.0, ^14.17.0, or >=16.0.0)
installed and built with SSL support.
(If you are using an official Node.js distribution, SSL is always built in.)

Узнать версию установленного nodejs можно так:
```sh
docker compose run --rm frontend-node-cli node -v
```
v18.19.0


> Quick start
You can install and configure ESLint using this command:
```sh
npm init @eslint/config
```

в старой доке команда была такая:


```sh
npm install eslint --save-dev
# or
yarn add eslint --dev
```

```sh
docker compose run --rm frontend-node-cli yarn add --dev eslint
docker compose run --rm frontend-node-cli npx eslint --init
```

You can also run this command directly using 'npm init @eslint/config'.
Need to install the following packages:
@eslint/create-config@0.4.6
Ok to proceed? (y) y

Дальше отвечаем на вопросы в интерактивном режиме

```
✔ How would you like to use ESLint? · style
✔ What type of modules does your project use? · esm
✔ Which framework does your project use? · react
✔ Does your project use TypeScript? · No / Yes                       << NO
✔ Where does your code run? · browser, node                         browser+node
✔ How would you like to define a style for your project? · guide
✔ Which style guide do you want to follow? · standard
✔ What format do you want your config file to be in? · JSON

Checking peerDependencies of eslint-config-standard@latest
The config that you've selected requires the following dependencies:

eslint-plugin-react@latest eslint-config-standard@latest eslint@^8.0.1 eslint-plugin-import@^2.25.2 eslint-plugin-n@^15.0.0 || ^16.0.0  eslint-plugin-promise@^6.0.0

✔ Would you like to install them now? · No / Yes                     << NO
Successfully created .eslintrc.json file in /app
npm notice
npm notice New patch version of npm available! 10.2.3 -> 10.2.5
npm notice Changelog: https://github.com/npm/cli/releases/tag/v10.2.5
npm notice Run npm install -g npm@10.2.5 to update!
npm notice
```

- Where does your code run? · browser, node
здесь выбираем оба т.к. мы будем писать и для браузера и в будущем для SSR
(server side render)

На последний вопрос отвечаем нет т.к. для нас важно ставить пакеты не через npm
а через Yarn
он пишет что для выбранной конфигурации нужны такие пакеты:
- eslint-plugin-react@latest
- eslint-config-standard@latest
- eslint@^8.0.1
- eslint-plugin-import@^2.25.2
- eslint-plugin-n@^15.0.0 || ^16.0.0
- eslint-plugin-promise@^6.0.0

Установку всех описанных зависимостей делаем так:
```sh
docker compose run --rm frontend-node-cli yarn add --dev \
eslint-plugin-react@latest \
eslint-config-standard@latest \
eslint@^8.0.1 \
eslint-plugin-import@^2.25.2 \
eslint-plugin-n@^16.0.0  \
eslint-plugin-promise@^6.0.0
```

```
success Saved lockfile.
success Saved 10 new dependencies.
info Direct dependencies
├─ eslint-config-standard@17.1.0
├─ eslint-plugin-n@16.5.0
├─ eslint-plugin-promise@6.1.1
└─ eslint-plugin-react@7.33.2
info All dependencies
├─ builtins@5.0.1
├─ eslint-compat-utils@0.1.2
├─ eslint-config-standard@17.1.0
├─ eslint-plugin-es-x@7.5.0
├─ eslint-plugin-n@16.5.0
├─ eslint-plugin-promise@6.1.1
├─ eslint-plugin-react@7.33.2
├─ get-tsconfig@4.7.2
├─ is-builtin-module@3.2.1
└─ resolve-pkg-maps@1.0.0
Done in 125.49s.
```


После опроса создаётся файл `./frontend/.eslintrc.json`
```json
{
    "env": {                 // в этой секции описано где будет запускаться код
        "browser": true,     // а значит будте разрешать доступ к window, document
        "es2021": true,      // формат кода ecmascript 2021
        "node": true
    },
    "extends": [
        "standard",
        "plugin:react/recommended"
    ],
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
    }
}
```
Здесь перечислены правила по которым будет работать ESLint

секция `env` - описывает специфику js-кода, где конкретно он бдует запсукаться
`extends` - это про расширения по работе с js-кодом:
- standard - это для стандартного код-стайла
- plugin:react/recommended - плагин для работы с реакт

идея секции `extends` в том, что данный файл конфигурации может быть расширен
другими конфиг-файлами через описание их именно в этой секции.
Т.е. можно подключать как свои так и чужие конфиг файлы.

секция `rules` - для переопределения правил провеки стиля кода

Добавляем команду `lint` и `lint-fix` в файл `./frontend/package.json`
```json
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject",
    "lint": "eslint --ext .js,.jsx src",
    "lint-fix": "eslint --fix --ext .js,.jsx src"
  },
```

алиас для Makefile
```Makefile
lint: api-lint frontend-lint
test: api-test api-fixtures frontend-test

# ...

frontend-lint:
	docker compose run --rm frontend-node-cli yarn lint

frontend-lint-fix:
	docker compose run --rm frontend-node-cli yarn lint-fix
```

lint-fix - запускать автоматическое исправление недостатков в кодстайле



Запускаем новую команду через алиас смотрим вывод  (убрал повторные ошибки)
```sh
make frontend-lint
docker compose run --rm frontend-node-cli yarn lint
yarn run v1.22.19
$ eslint --ext .js,.jsx src
Warning: React version not specified in eslint-plugin-react settings.
See https://github.com/jsx-eslint/eslint-plugin-react#configuration .

/app/src/App.jsx
   1:26  error  Extra semicolon                            semi
   5:13  error  Missing space before function parentheses  space-before-function-paren

/app/src/App.test.jsx
   1:26  error  Extra semicolon                               semi
   5:1   error  'jest' is not defined                         no-undef
   5:41  error  Component definition is missing display name  react/display-name
   6:1   error  Expected indentation of 2 spaces but found 4  indent
   9:1   error  'test' is not defined                         no-undef
  11:3   error  'expect' is not defined                       no-undef
  11:29  error  Strings must use singlequote                  quotes

/app/src/components/Welcome.jsx
   4:17  error  Missing space before function parentheses  space-before-function-paren

/app/src/components/Welcome.test.jsx
  1:26  error  Extra semicolon          semi
  5:1   error  'test' is not defined    no-undef
  8:3   error  'expect' is not defined  no-undef

/app/src/index.jsx
  17:18  error  Extra semicolon  semi

/app/src/reportWebVitals.js
  13:31  error  Extra semicolon  semi

/app/src/setupTests.js
  5:35  error  Extra semicolon  semi

✖ 50 problems (50 errors, 0 warnings)
  44 errors and 0 warnings potentially fixable with the `--fix` option.

error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
```

- Первое что говорит - надо указать версию библиотеки React
`Warning: React version not specified in eslint-plugin-react settings.`

дальше идёт полотно ошибко стиля кода

Файл `/app/src/App.jsx`

- `semi`
- 1:26  error  Extra semicolon
это про то что в стандартной рекомендации не нужно указывать `;` в конце строк

```js
import React from 'react';
import './App.css';//    ^
//...             ^
```

- `space-before-function-paren`
- 5:13 error Missing space before function parentheses
```js
function App() {
//          ^    в стандарте рекомендуюется так App ()
```

Можно это правило добавить в исключения:

- no-undef
- 9:1   error  `'test' is not defined`
говорит о том, что не может понять откуда берётся глобальное имя `test`
функция которую используем в своих тестах

```js
import React from 'react';
import { render, screen } from '@testing-library/react';
import Welcome from './Welcome';

test('renders app', () => {
//^^
  render(<Welcome />);
  const h1Element = screen.getByText(/Auction/i);
  expect(h1Element).toBeInTheDocument();
});
```
так происходит т.к. в нашем коде все функции и обьекты подключаются через import
а функция test не подключается, и стандартной такой в js нет.
На деле же эту функцию обьявляет фреймворк Jest.
- надо как-то указать ESLint-у что мы используем фреймворк Jest

quotes - это про то что в строках вместо одинарных используются двойные кавычки

Так же в выводе говорится о том что всё это можно исправить через запуск команды:
```
  44 errors and 0 warnings potentially fixable with the `--fix` option.
```

- Подключаем Jest (в env)
- Указываем версию React (settings)
- Добавляем исключения в правила стиля кода (rules)

```json
{
  "env": {
    "browser": true,
    "es2021": true,
    "node": true,
    "jest": true
  },
  "extends": [
    "standard",
    "plugin:react/recommended"
  ],
  "parserOptions": {
    "ecmaVersion": "latest",
    "sourceType": "module"
  },
  "plugins": [
    "react"
  ],
  "settings": {
    "react": {
      "version": "18"
    }
  },
  "rules": {
    "semi": "off",
    "space-before-function-paren": "off"
  }
}
```

Для мока-заглушки кидал такую ошибку:
Component definition is missing display name `react/display-name`

```js
jest.mock('./components/Welcome', () => (/* props */) => {
  return <span data-testid='WelcomeComponent' />
});

// так норм:
jest.mock('./components/Welcome', () => function mockWelcome(/* props */) {
  return <span data-testid='WelcomeComponent' />
});
```

После всех исправлений работает без ошибок:
```sh
make frontend-lint
docker compose run --rm frontend-node-cli yarn lint
yarn run v1.22.19
$ eslint --ext .js,.jsx src
Done in 1.11s.
```


## Линтер для CSS-кода - http://stylelint.io
тоже может быть весьма полезен чтобы выявлять ошибки опечатки или недочёты в css

https://stylelint.io/user-guide/get-started

> Linting CSS
1. Устанавливаем библиотеку

```sh
npm install --save-dev stylelint stylelint-config-standard
# or
yarn add --dev stylelint stylelint-config-standard
```

2. Создаём файл `.stylelintrc.json` в корне фронтенда
```json
{
  "extends": "stylelint-config-standard"
}
```

3. Run Stylelint on all the CSS files in your project:
Запускаем для казания какие файлы нужно проверять
```sh
npx stylelint "**/*.css"
```


```sh
docker compose run --rm frontend-node-cli yarn add --dev stylelint stylelint-config-standard
yarn add v1.22.19
[1/4] Resolving packages...
[2/4] Fetching packages...
[3/4] Linking dependencies...
[4/4] Building fresh packages...
success Saved lockfile.
success Saved 24 new dependencies.
info Direct dependencies
├─ stylelint-config-standard@36.0.0
└─ stylelint@16.1.0
info All dependencies
├─ @csstools/css-parser-algorithms@2.4.0
├─ @csstools/css-tokenizer@2.2.2
├─ @csstools/media-query-list-parser@2.1.6
├─ astral-regex@2.0.0
├─ css-functions-list@3.2.1
├─ env-paths@2.2.1
├─ fastest-levenshtein@1.0.16
├─ globjoin@0.1.4
├─ html-tags@3.3.1
├─ is-plain-object@5.0.0
├─ known-css-properties@0.29.0
├─ lodash.truncate@4.4.2
├─ mathml-tag-names@2.1.3
├─ meow@13.0.0
├─ postcss-resolve-nested-selector@0.1.1
├─ postcss-safe-parser@7.0.0
├─ slice-ansi@4.0.0
├─ stylelint-config-recommended@14.0.0
├─ stylelint-config-standard@36.0.0
├─ stylelint@16.1.0
├─ supports-hyperlinks@3.0.0
├─ svg-tags@1.0.0
├─ table@6.8.1
└─ write-file-atomic@5.0.1
Done in 105.25s.
```

для того чтобы скрыть ворнинги от yarn `warning "has unmet peer dependency"`:
```json
  "devDependencies": {
    "@babel/plugin-proposal-private-property-in-object": "^7.21.11",
    "@babel/plugin-syntax-flow": "^7.14.5",
    "@babel/plugin-transform-react-jsx": "^7.14.9",
    "@babel/core": "^7.0.0-0",
    //...
  }
```

Добавляем команду для запуска stylelint проверяющего стиль css-файлов
```json
  "scripts": {
    // ..
    "eslint": "eslint --ext .js,.jsx src",
    "eslint-fix": "eslint --fix --ext .js,.jsx src",
    "stylelint": "stylelint \"src/**/*.css\""
  },
```
Так же чтобы не путаться команду `lint` переименуем в `eslint`


Пробуем запустить линтер `make frontend-lint`
```sh
docker compose run --rm frontend-node-cli yarn stylelint
yarn run v1.22.19
$ stylelint "src/**/*.css"

src/index.css
 13:3  ✖  Unexpected vendor-prefix "-webkit-text-size-adjust"  property-no-vendor-prefix

src/components/Welcome.module.css
  3:3   ✖  Expected empty line before comment       comment-empty-line-before
  6:28  ✖  Expected modern color-function notation  color-function-notation
  6:42  ✖  Expected "0.05" to be "5%"               alpha-value-notation
 16:3   ✖  Expected "0 0 20px 0" to be "0 0 20px"   shorthand-property-no-redundant-values

5 problems (5 errors, 0 warnings)

error Command failed with exit code 2.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
```


"-webkit-text-size-adjust" -> "text-size-adjust" ->

- `Expected "0.05" to be "5%"               alpha-value-notation`
   описание шибки                           название правила
```css
  box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.05);
  box-shadow: 1px 1px 10px rgba(0, 0, 0, 5%);
```

Если надо переопределить правило это делается вот так
```json
{
  "extends": ["stylelint-config-standard"],
  "rules": {
    "alpha-value-notation": true
  }
}
```

src/components/Welcome.module.css
 5:28  ✖  Expected modern color-function notation  color-function-notation

```css
  box-shadow: 1px 1px 10px rgba(0, 0, 0, 5%);
```

Legacy notation:

    a { color: rgb(0, 0, 0, 0.5) }

Modern notation:

    a { color: rgb(0 0 0 / 50%) }



После ручного исправления всех ошибок проходит как надо
```sh
docker compose run --rm frontend-node-cli yarn stylelint
yarn run v1.22.19
$ stylelint "src/**/*.css"
Done in 0.74s.
```

