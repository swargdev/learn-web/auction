<?php

declare(strict_types=1);

namespace App\Auth\Test\Unit\Service;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\Token;
use App\Auth\Service\JoinConfirmationSender;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email as MimeEmail;
use Twig\Environment;

/**
 * @covers JoinConfirmationSender
 * 07-12-2023
 * @author Swarg
 */
class JoinConfirmationSenderTest extends TestCase
{
    public function testSuccess(): void
    {
        // $from = [ 'email' => 'test@app.test', 'name' => 'Test'];
        $to = new Email('user@app.test');
        $token = new Token(Uuid::uuid4()->toString(), new DateTimeImmutable());
        $confirmUrl = 'http://site/join/confirm?token=' . $token->getValue();
        $body = '<a href="' . $confirmUrl . '">' . $confirmUrl . '</a>';

        $twig = $this->createMock(Environment::class);
        $twig->expects(self::once())->method('render')->with(
            self::equalTo('auth/join/confirm.html.twig'),
            self::equalTo(['token' => $token]),
        )->willReturn($body);

        $mailer = $this->createMock(MailerInterface::class);
        $mailer->expects(self::once())->method('send')
        ->willReturnCallback(
            static function (MimeEmail $message) use ($to, $body): void {
                self::assertEquals([], $message->getFrom());
                self::assertEquals($to->getValue(), $message->getTo()[0]->getAddress());
                self::assertEquals('Join Confirmation', $message->getSubject());
                self::assertEquals($body, $message->getHtmlBody());
            }
        );

        $sender = new JoinConfirmationSender($mailer, $twig);

        $sender->send($to, $token);
    }
}
