## Установка отладчика xdebug и настройка для работы изнутри докер-образов

Задача: возможность в dev-окружении отлаживать php код
как для консоли (php-cli) так и для web-страниц API (php-fpm)
Решение - Xdebug
[Подробная документация о настройке отладки](https://xdebug.org/docs/step_debug)

Xdebug — Step Debugger and Debugging Aid for PHP
[Расширение для php-fpm](https://github.com/xdebug/xdebug)
[Документация](https://xdebug.org/docs/install)


## Устанавливаем расширение xdebug в образы api-php-fmp api-php-cli

Смотрим что у нас есть на данный момент:
./api/docker/development/php-fpm/Dockerfile
```Dockerfile
FROM php:8.1-fpm-alpine

RUN mv $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini

COPY ./common/php/conf.d /usr/local/etc/php/conf.d

WORKDIR /app
```
Здесь в этом Dockerfile для api-php-fpm делается:
- берём оригинальный образ
- переопределяем настройки
- задаём рабочий каталог

В Докерфайле для api-php-cli дополнительно к этому идёт подключение composer-а.
Composer туда устанавливается для возможности работать с ним через консоль:

./api/docker/development/php-cli/Dockerfile
```Dockerfile
FROM php:8.1-cli-alpine

RUN apk update && apk add unzip

RUN mv $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini

COPY ./common/php/conf.d /usr/local/etc/php/conf.d

ENV COMPOSER_ALLOW_SUPERUSER 1

RUN curl -sS https://getcomposer.org/installer | \
  php -- --install-dir=/bin --filename composer --quiet \
  && rm -rf /root/.composer/cache

WORKDIR /app
```

В продакшенских же образах идёт дополнительное подключение библиотеки opcache
```Dockerfile
RUN docker-php-ext-install opcache
```
Это команда устанавливает расширение php-opcache конкретно для текущей
используемой версии php.
(Как уже знаем это shell-скрипт устанавливающий расширение из исходников)

xdebug ставится из отдельного PECL-репозитория.
а это значит его надо вручную скомпилировать, установить и добавить в свой проект:
Делать это будем для обоих образов api-php-cli и api-php-fpm для того, чтобы
можно было отлаживать и веб-страницы нашего апи и консольные команды к php-cli

Добавляем команду на установку xdebug из исходников сразу после `FROM`:
```Dockerfile
FROM php:8.1-cli-alpine

RUN apk update && apk add autoconf g++ make && apk add --update linux-headers \
    && pecl install xdebug-3.2.2 \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable xdebug
...
```
Обычно хороший тон всегда указывать конкретные используемые версии пакетов.
Но здесь можно не указывать конкретную версию xdebug т.к. он автоматически будет
устанавливаться под конкретную версию php внутри образа.
и даже если что-то в нём сломается это отразится только на дев-окружение,
так как в продакшен образы мы его вообще не ставим.

Немного обьяснений что происходит в этой команде `RUN apl updae ...`:
здесь основаня команда установки это:
```sh
pecl install xdebug
```
Но для выполнения этой команды нужны доп пакеты: autoconf g++ и make
т.к. установка будет идти через компиляцию исходников

`rm -rf /tmp/pear` - очистка временной директории в которой шла сборка

`docker-php-ext-enable xdebug` - добавит отдельный файл docker-xdebug.ini
в директорию `/usr/local/etc/php/conf.d/` для настройки работы xdebug

## What is PECL? Что такое PECL?
PECL :: The PHP Extension Community Library
[pecl.php.net](https://pecl.php.net/)

PECL is a repository for PHP Extensions, providing a directory of all known
extensions and hosting facilities for downloading and development of
PHP extensions.

Аналогичным образом можно из PECL ставить и любые другие компоненты которые
имеют свои расширения в PECL но не имеют готовых расширений типа php-xdebug
например для работы с redis.

## Добаваляем конфиг xdebug.ini

Конфиг файл, в котором настраивается работа Xdebug-а:
Можно его запечь в сам образ прямо из докер файла например вот так:
```Dockerfile
# ...
# Configure Xdebug
RUN echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.log=/var/www/html/xdebug/xdebug.log" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.discover_client_host=1" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.client_port=9000" >> /usr/local/etc/php/conf.d/xdebug.ini
./api/docker/development/php/conf.d/xdebug.ini:
```

Но более универсальный способ разместить его в каталог с настройками окружений
а затем копировать его содержимое внутрь образа при его создании.

./api/docker/development/php/conf.d/xdebug.ini
```
xdebug.mode=debug
```

- `xdebug.start_with_request=yes`(Xdebug 3.x) или `xdebug.enable=1`(Xdebug 2.x)
  включить расширение на постоянку - при каждом запросе к php-скрипту.
  это сильно замедлит работу php интерпретатора в (~2-3раза)

- `xdebug.mode=debug`(Xdebug 3.x) или старое `xdebug.remote_enable`(Xdebgug 2.x)
  включать xdebug расширение только при специальном запросе на отладку.
  такой запрос отправляется через отдельную Cookie, наличие которой в
  Http-Заголовке проверяет Xdebug и включает режим отладки только когда она есть
  [Подробно здесь](./docs/08-install-xdebug/07-work-with-xdebug.md)

Прокидываем этот конфиг-файл в образы api-php-cli api-php-fpm через `COPY`
(Note. Два `COPY` из разных каталогов в один - своеобразный `merge` конфигов)
```Dockerfile
# ...
COPY ./common/php/conf.d /usr/local/etc/php/conf.d
COPY ./development/php/conf.d /usr/local/etc/php/conf.d        # <<<<
# ...
````

Пересобираем все наши дев-образы для локальной разработки как обычно через:
```sh
make init
```


Без указания конкретной версии билды образов валится с ошибкой:
```
12.79 configure: error: rtnetlink.h is required, install the linux-headers package: apk add --update linux-headers
12.81 ERROR: `/tmp/pear/temp/xdebug/configure --with-php-config=/usr/local/bin/php-config' failed
```
(В логе при этом показывает что пытается собрать версию 3.2.2)

При этом если указать конкретную версию 3.1.6 то сборка проходит успешно!

[Таблица совместимостей версий xdebug и php](https://xdebug.org/docs/compat)
[Номер Версии 3.2.x взял отсюда:](https://github.com/xdebug/xdebug/tags)
Версия 3.2.2 зеленая для php8.1 её и беру

Ошибку решил вот такой командой установки:
```Dockerfile
FROM php:8.1-fpm-alpine

RUN apk update && apk add autoconf g++ make && apk add --update linux-headers \
    && pecl install xdebug-3.2.2 \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable xdebug
```

Здесь указываю самую свежую на текущий момент версию ветки 3.2.x и доп установку
заголовков как подсказывал лог с ошибкой при компиляции Xdebug-а


## Проверка действительно ли Xdebug установился в php

```sh
docker compose run --rm api-php-cli php -v
```
```
PHP 8.1.23 (cli) (built: Sep  2 2023 07:56:24) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.1.23, Copyright (c) Zend Technologies
    with Xdebug v3.2.2, Copyright (c) 2002-2023, by Derick Rethans
```

`Note`:
Когда настраивал, то в начале использовал устаревшие Xdebug 2.x имена параметров
и при запросе версии выдало такое предупреждение:
```
Xdebug: [Config] The setting 'xdebug.remote_enable' has been renamed, see the upgrading guide at https://xdebug.org/docs/upgrade_guide#changed-xdebug.remote_enable (See: https://xdebug.org/docs/errors#CFG-C-CHANGED)
PHP 8.1.23 (cli) (built: Sep  2 2023 07:56:24) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.1.23, Copyright (c) Zend Technologies
    with Xdebug v3.2.2, Copyright (c) 2002-2023, by Derick Rethans
```

Наименования параметров конфигурации в версиях Xdebug 2.x и Xdebug 3.x отличаются
Подробнее, что было изменено смотри здесь
[документации](https://xdebug.org/docs/upgrade_guide#changed-xdebug.remote_enable)
[See:](https://xdebug.org/docs/errors#CFG-C-CHANGED)

