Кратко что сделаем в этом разделе:

Полностью реализуем отправку письма подтверждения регистрации:
- подключим и настроим Symfony\Mailer
- подключим эмулятор smtpсервер MailHog
  для удобной и быстрой проверки отправки писем и просмотра их содержимого
- подключим шаблонизатор twig, позволяющий делать письма в виде html-страниц
  и рендерить эти страницы прямо внутри php-кода.
- создам FrontendUrlGenerator для удобной генерации полных ссылк на фронтенд
- создам своё расширение FrontendUrlTwigExtention и свою собственную функцию
  frontend_url для вызова FrontendUrlGenerator изнутри twig-шаблонов.


Обзор:

01 intro
- Что сделано и что дальше
- Symfony Mailer Библиотека для отправки писем
- Как работать с Symfony Mailer
- SMTP-транспорт
- Зачем устанавливать эмулятор почтового сервера
- Mailhog - готовое решение для эмуляции smtp-сервера принимающего письма
- Установка MailHog в dev docker-compose.yml
- Проверка отправки эл. писем MailerCheckCommand
- Исследуем компонент Symfony/Mailer на то как использовать SMTP-транспорт

02 setup-mailer
- Подключаем Symfony/Mailer к нашему проекту
- Создание фабрики по сборке Symfony/Mailer
- MAILER_FROM_EMAIL - Автоматическая подстановка отправителя для всех писем

03 use-mailer:
- Реализуем сервис JoinConfirmationSender для работы через Symfony/Mailer
- Проблема как указывать абсолютную ссылку для токена подтверждения
- Регистрация класса JoinConfirmationSender в DI-контейнере
- Переписываем MailerCheckCommand на использование JoinConfirmationSender
- Unit test-ы для JoinConfirmationSender делаем хитрный mock для метода send
- Линия рассуждений для написания теста под сервис JoinConfirmationSender
- Mock как способ проверить поведение нудного обьекта
- Весь код первого варианта теста JoinConfirmationSender
- Проверим реально ли тесты будут выявлять ошибки в коде
- Почему делали прокидывание адреса отправителя письма (from)
- Меняем относительные адреса на абсолютные с именем хоста.
- Вариант через конкатенацию строк
- FrontendUrlGenerator - cпецилизированный обьект для генерации ссылок
- Почему явная фабрика класса JoinConfirmationSender больше ну нужна
- Переписываем наш Unit-тест на использование FrontendUrlGenerator

04 - twig:
- Подключение шаблонизатора twig
- Как использовать: Basic API Usage
- Установка twig-а
- Делаем шаблоны писем для рендеренга в Sender-e(Отправителе писем)
- Переписываем Sender на использование шаблонизатора Twig
- Рефакторинг JoinConfirmationSender-а своё расширение для twig и frontend_url
- Пишем своё расширение FrontendUrlTwigExtension
- Пишем тест для FrontendUrlTwigExtension
- Переписываем шаблон(представление) на использование функции frontend_url
- Реализуем остальные наши Sender-ы:


## Что уже сделано и что предстоит

На текущий момент вся работа с БД реализована
И теперь для модуля аутентификации осталось реализовать
- сервисы отправки и подтверждения токенов регистрации
- смены email
- восстановление пароля на email


Пока что в нашем коде только написаны интерфейсы под нужный функционал сервисов.
То есть описали то что сервисы аутентификации должны делать и через какие методы

./api/src/Auth/Service/JoinConfirmationSender.php

```php
<?php

declare(strict_types=1);

namespace App\Auth\Service;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\Token;

interface JoinConfirmationSender
{
    public function send(Email $email, Token $token): void;
}
```

Пример одного такого сервиса.
Что должен сделать этот сервис:
- принять email и токен для подтверждения регистрации
- сформировать html код для письма подтверждения регистрации
  (отправкляем не просто текст а красивое письмо стилизованное под проект.)
- отправить письмо на указанный email

Для этого понадобятся интсрументы(библиотеки):
- для отправки электронных писем
- шаблонизатор для вёрстки отправляемых html-писем.


Вообще в php есть стандартная процедура mail умеющая отправлять письма.
Но ныне письма отправленные через неё любой почтовый сервер воспримит как спам.
Из-за того что слишком часто эту стандартную процедуру использовали спамеры
для рассылки спама. То есть если через эту процедуру отправлять письма то они
будут просто оброшены спам-фильтром принимаюшего письмо сервисом.
Поэтому единственное решение - официальные и защищённые способы отправки писем.
Если нужно отправлять письма с адресов неким образом привязанных к нашему домену
то можно воспользоваться сервисами "подта для домена" от yandex или google.
Далее в подобном сервисе заводится своя электронная почта, и появляется возможность
через этот сервис слать свои письма используя специальный протокол подключения
к нашему почтовому сервису. Самый распостранённый такой протокол это SMTP.
его поддерживают большиство сервисов хоть как-то связанных с отправкой писем.
Для работы по протоколу smtp с неким реальным почтовым сервером можно взять
готовое решение(библиотеку). Одна из таких - symfony mailer


## Symfony Mailer Библиотека для отправки писем
https://symfony.com/doc/current/mailer.html
https://github.com/symfony/mailer

Symfony's Mailer & Mime components form a powerful system for creating and
sending emails - complete with support for multipart messages, Twig integration,
CSS inlining, file attachments and a lot more.

Подключается через композер (как и все остальные библиотеки)

```sh
composer require symfony/mailer

docker compose run --rm api-php-cli composer require symfony/mailer
```

NOTE: Родоночальницей этой библиотеки является другая старой библиотека,
с 2021 года не поддерживаемая:

https://swiftmailer.symfony.com/docs/introduction.html
Swift Mailer is a component based library for sending e-mails from PHP applications.

Swift Mailer will stop being maintained at the end of November 2021.

Please, move to Symfony Mailer at your earliest convenience.
Symfony Mailer is the next evolution of Swift Mailer.
It provides the same features with support for modern PHP code and support for
third-party providers.

## Как работать с Symfony Mailer

Документация по symfony/mailer достаточно обьёмная, но это потому что в основном
описывает как его использовать внутри проекта на symfony-фрейморке.
Мы же используем не симфони фрейморк, а легковесный slim. Поэтому и настройка
будет частично отличатся от той что даётся в документации для настройку его под
симфони-фреймворк.

https://symfony.com/doc/current/mailer.html#creating-sending-messages

to send an email, get a Mailer instance by type-hinting `MailerInterface`
and create an Email object:
```php
// src/Controller/MailerController.php
<?
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class MailerController extends AbstractController
{
    #[Route('/email')]
    public function sendEmail(MailerInterface $mailer): Response
    {
        $email = (new Email())
            ->from('hello@example.com')    // от имени кого отправяем письмо
            ->to('you@example.com')        // адрес получателя письма
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')              // темя письма
            ->text('Sending emails is fun again!')             // тело текстом
            ->html('<p>See Twig integration for better HTML integration!</p>');

        $mailer->send($email);

        // ...
    }
}
```

Сами письма отправляются через некий транспорт, бывают разные виды транспортов.
- sendmail - транспорт эмулирующий отправку писем,
  на деле сохраняет письма в файлы на диск
- smtp - транспорт отправляющий полноценные письма на SMTP-сервер.

Для работы по smtp-протоколу с потовым сервером нужны данные:
- самого почтового сервер, его dns-имя и порт к на какой ходить,
- логин-пароль для аутентификации на данном сервере
- метод шифрования при подключении к почтовому серверу(tls)

Все эти данные нужно передавать для настройки конкретного транспорта.
В частности это можно настроить например через переменные окружения и файл .env


https://symfony.com/doc/current/mailer.html#transport-setup

Emails are delivered via a "transport".
Out of the box, you can deliver emails over SMTP by configuring the DSN
in your `.env` file (the user, pass and port parameters are optional):

Пример настройки транспорта из документации
(этот пример для настройки мейлера под симфони-фреймворк)
```
# .env
MAILER_DSN=smtp://user:pass@smtp.example.com:port
```

```php
<?
// config/packages/mailer.php
use function Symfony\Component\DependencyInjection\Loader\Configurator\env;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $container): void {
    $container->extension('framework', [
        'mailer' => [
            'dsn' => env('MAILER_DSN'),
        ],
    ]);
};
```

Т.е. последовательность работы такая
- настроить транспорт для отправки письма (куда слать и от чьего имени)
- обьект создать письма `Symfony\Component\Mime\Email`
- передать его в мейлер `Symfony\Component\Mailer\MailerInterface`

в примере выше мейлер скрыт за интерфейсом и как обычно конкретный класс
прописывается в DI-контейнере, и там же идёт проброс настроек транспорта.



```php
<?
  $result = $mailer->send($email);
```

в Swift Mailer этот метод выводил количество успешно отправленых писем либо
0 если возникли ошибки. (можно будет подключить сюда логгер для учёта ошибок)
В Symfony Mailer при провале просто кидается исключение TransportExceptionInterface


Using a 3rd Party Transport
Есть возможность писать свой собтственный транспорт под специфическую систему,
если вдруг встроенного транспорта будет мало(возникнут проблемы с подключением
к своему почтовому сервису) и надо будет например ходить через API а не SMTP.

Так же есть уже целый набор готовых транспортов от стороних разработчиков.

Instead of using your own SMTP server or sendmail binary, you can send emails
via a third-party provider:
https://symfony.com/doc/current/mailer.html#using-a-3rd-party-transport

## SMTP-транспорт

Но обычно это не нужно, так как многие сервисы включая такие как `Mailgun`,
`UniOne` от unisender-а позволяют работать с ними не только по API но и по SMTP.
А это значит что SMTP транспорта должно хватать на все случаи.


## Зачем устанавливать эмулятор почтового сервера

Подключив библ-ку можно будет отсылать письма.
Но возникает проблема куда слать.

Для прода в коде нужно указывать реальные данные почтового сервера (логин-пароль).
- для локальной разработки это будет не удобно, да и если вписать реальные данные
прямо в код и отправить его в репозиторий, то секретные данные утекут и от нашего
имени и аккаунта начнут слать спам.
как вариант каждому разработчику из команды вписывать данные своего собственного
почтового сервера, специально для этого создав отдельный почтовый аакаунт.
Но такой подход создаёт неудобства для всех членов команды - надо вписывать
свои данные в какие-то env-файлы
- для функциональных и приёмочных тестов нужна будет проверка отправки писем,
и при запуске тестов будут слаться множество описаных в них писем с тестовыми
данными имён-заглушен не существующих почтовых адресов. Т.е. слать всё это дело
на реальный почтовый сервер не вариант - он либо расценит это как спам либо
будет ругаться адрес не существует либо просто отбрасывать письма.

Как варинат решения проблемы отправки писем в dev|test-окружении это использовать
не SMTP транспорт а например File-подобный Transport, когда письма просто
сохраняются в файлы(var/mail) и реально никуда не шлются.
Ну и контролить отправку писем просто визуально проверяя наличие писем в dir-е.
Но при таком подхоже будет возникать рассинхрон dev-prod Окружения:
в проде шлём через SMTP в dev через FileTransoprt локально всё работает в проде
что-то сломалось. Чем больше разницы между dev и prod - окружением тем выше
шанс пропустить возникшую ошибку или баг.

Лучшее Решение - это использовать одну и ту же систему - один и тот же SMTP-
транспорт везде и на dev тоже, но для dev ипользовать не настоящий почтовый
сервер, а эмулятор работающий по SMTP-протоколу.


## Mailhog - готовое решение для эмуляции smtp-сервера принимающего письма
https://github.com/mailhog/MailHog

Эмулятор почтового сервера,
- поддерживает работу по протоколу SMTP
- есть WEB-интерфейс (можно смотреть полученные им писльма из браузера)
- есть API по которому к нему можно обращаться из тестов
  и проверять отправку писем
- слушает 1025 SMTP порт (для приёма писем)
- слушает 8025

Можно не ставить его напрямую на локальную машину, а использовать Docker image:
https://registry.hub.docker.com/r/mailhog/mailhog/

https://github.com/mailhog/MailHog/blob/master/docs/APIv1.md

MailHog is an email testing tool for developers:
- Configure your application to use MailHog for SMTP delivery
- View messages in the web UI, or retrieve them with the JSON API
- Optionally release messages to real SMTP servers for delivery


## Установка MailHog в dev docker-compose.yml

Простейший вариант:
прописать проброс порта из докер-контейнера в локальную машину
Пробрасываем порт с 8025 в локальную машина на следующий свободный 8082

./docker-compose.yml
```yml
  ...
  api-postgres:
     ...

  mailer:
    image: mailhog/mailhog
    ports:
      - "8082:8025"

volumes:
  api-postgres:
```


Плюсы:
- удобно чисто для локальной разработки,
  можно зайти на почтовый сервер-эмулятор прямо из браузера

Минусы:
- не подходит на случай когда надо поднять staging-server,
  работащий на dev-поддомене своего сайта,
  причем возможно, уже по протоколу https а не smtp

Поэтому вместо простого прямого проброса портов в docker-compose.yml
можно пробросить порт через наш `gateway` (Для запуска через staging-server)

```yml
  ...
  api-postgres:
     ...

  mailer:
    image: mailhog/mailhog
    # 8082 see ./gateway/docker/development/nginx/conf.d/mailer.conf

```
Прописываем nginx-у слушать нужный нам `8082` порт (следующий свободный от 8081)

./gateway/docker/development/nginx/conf.d/mailer.conf
```nginx
server {
  listen 8082;
  server_tokens off;

  location / {
    proxy_set_header  Host $host;
    proxy_set_header  Upgrade $http_upgrade;
    proxy_set_header  Connection "Upgrade";

    proxy_pass        http://mailer:8025;
    proxy_redirect    off;
  }
}
```

Здесь идёт прописывание проксирования запросов двух разных типов
Web-интерфейс работающий на 8025 порту подключен к серверу двумя способами:
- обычный для отображения самой html-страницу
- web-socket (для динамичного обновления?)

Для проксирования web-socket и нужны настройки:

    proxy_set_header  Upgrade $http_upgrade;
    proxy_set_header  Connection "Upgrade";

Это нужно для того чтобы подключение по web-socket не отваливалось.
Если просто скопировать настройки проксирования из frontend или api то
подключение по web-socket-у работать с ними не будет.

Пример проксирования для API(не годится для проксирования web-socket-а)
```nginx
server {
  listen 8081;
  server_tokens off;

  location / {
    proxy_set_header  Host $host;
    proxy_set_header  X-Real-IP $remote_addr;
    proxy_set_header  X-Forwarded-Proto http;
    proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header  X-Forwarded-Host $remote_addr;
    proxy_set_header  X-NginX-Proxy true;
    proxy_pass        http://api;
    proxy_ssl_session_reuse off;
    proxy_redirect off;
  }
}
```


Теперь для того чтобы создать staging-сервер достаточно будет создать каталог
`gateway/docker/staging` на подобии `gateway/docker/prodaction`
и в нём прописать в конфиге чтобы мейлер работал либо по своему поддомену либо
по указанному порту, и например по протоколу https, как работают все остальные
сервисы которые у нас в продакшене проброшены через gateway.


```
gateway/docker/
├── common
│   └── nginx
│       └── snippets
│           ├── certbot.conf
│           ├── resolver.conf
│           └── ssl.conf
├── development
│   └── nginx
│       ├── conf.d
│       │   ├── api.conf
│       │   ├── default.conf
│       │   ├── frontend.conf
│       │   └── mailer.conf       << слушать 8082 слать на 8025 в докер-образ
│       └── Dockerfile
└── production
    └── nginx
        ├── conf.d
        │   ├── api.conf
        │   ├── default.conf
        │   └── frontend.conf
        └── Dockerfile
```

добавляем проброс порта из докера в локальную машину
docker-compose.yml
```yml
  gateway:
    build:
      context: gateway/docker
      dockerfile: development/nginx/Dockerfile
    ports:
      - '${PORT_FRONTEND}:8080'
      - '${PORT_API}:8081'
      - '${PORT_MAILER}:8082'                    # <<<
    depends_on:
      - frontend
      - api
```
Ну и значение порта в прописываем в файл `.env` равное 8082

Пересобираем образ api-gateway для dev-окружения: и перезапускаем сайт
```sh
docker compose build gateway
make down && make up
```
при этом так же подтянется и новый образ с mailhog-ом



## Проверка отправки эл. писем MailerCheckCommand

Можем проверить работу Symfony/Mailer и почтового сервера MailHog.

Так как у нас пока нет никаких контроллеров, отправляющих письма
можно сделать простую консольную команду для отпраки писем.
Всё прописываем напрямую без DI-контейнера.
Но сначала попробуем посмотреть и разобраться как работать с нужным нам
видом транспорта в этой библиотеке.
Методология поиска как использовать новую библиотеку
(например при переходе со старой на другую, новую)


### Исследуем компонент Symfony/Mailer на то как использовать SMTP-транспорт

На гитхабе биб-ки https://github.com/symfony/mailer
есть простой пример по создания транспорта одной строкой

```php
<?
$transport = Transport::fromDsn('smtp://localhost');
```
это называется создание транспорта через DSN-строку.
И оно нам не походит так как здесь идёт создание транспорта на основе одной
dsn-строки, в которой указываются все нужные для подключения днанные от
хоста-порта до логина-пароля.

пример из документации https://symfony.com/doc/current/mailer.html:
```
smtp 	smtp://user:pass@smtp.example.com:25
```
в нашем же проекте мы передаём это всё по отдельности в отдельных переменных
окружения(через секреты). поэтому будем строить транспорт по другому.

### Идём искать и смотреть исходник нужного нам транспорта

Ищем как же создать компонент для создания smtp-транспорта Symfony\Mailer.
для этого идём в исходники после установки этой билбиотеки в компонент mailer:
`./api/vendor/symfony/mailer/`

Видим целый набор всяких разных абстрактых классов с разного рода транспортами
и их частями:
```
./api/vendor/symfony/mailer/Transport/
├── Smtp/                                << нужный нам SMTP транспорт
├── AbstractApiTransport.php
├── AbstractHttpTransport.php
├── AbstractTransportFactory.php
├── AbstractTransport.php
├── Dsn.php
├── FailoverTransport.php
├── NativeTransportFactory.php
├── NullTransportFactory.php
├── NullTransport.php
├── RoundRobinTransport.php
├── SendmailTransportFactory.php
├── SendmailTransport.php
├── TransportFactoryInterface.php
├── TransportInterface.php
└── Transports.php
```

Смотрим дальше, что там в нём есть
```
./api/vendor/symfony/mailer/Transport/Smtp/
├── Auth/
└── Stream/
├── SmtpTransport.php             -- простой SMTP без шифрования
├── EsmtpTransport.php            -- (E)ncripted smtp с шифрованием
├── EsmtpTransportFactory.php
```
Берём именно `EsmtpTransport` так как нам нужно будет шиврование

Идём и смотрим на конструктор данного класса:

```php
<?
class EsmtpTransport extends SmtpTransport
{
    public function __construct(
        string $host = 'localhost',
        int $port = 0,
        bool $tls = null,
        EventDispatcherInterface $dispatcher = null, // симфонивсикй диспетчер
        LoggerInterface $logger = null, // PSR-овский логгер
        AbstractStream $stream = null,
        array $authenticators = null
    ) {
     # ...
    }
}
```
Здесь сразу можно приметить такие вещи:
- Psr\EventDispatcher\EventDispatcherInterface
- Psr\Log\LoggerInterface;

логен и диспетчер событий чисто стандартные PSR-овские интерфейсы, можно будет
использовать нужные их реализации. Например когда подключим свой логер к проекту.
EventDispatcherInterface - это диспетчер событий то есть можно будет навешивать
хуки на нужные нам события. Например для коректировки письма до его отправки
(Это будет полезно для заполнения отправителя из значения по умолчанию)

Узнав как создавать обьект что передвать в конструктор можно перейти к созданию
консольной команды для отправки тестового письма по Smtp-транспорту на MailHog
(чисто для dev-окружения проверить их совместную работу)


./api/src/Console/MailerCheckCommand.php
```php
<?php

declare(strict_types=1);

namespace App\Console;

use Symfony\Component\Console\Command\Command ;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Component\Mime\Email;

class MailerCheckCommand extends Command
{
    protected function configure(): void {
        $this->setName('mailer:check'); // имя команды
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('<info>Sending</info>');

        // здесь mailer - имя сервиса(докер-контейнера dns внутри докер-сети)
        // и 1025 - порт на котором слушает почтовый сервер MailHog
        $transport = (new EsmtpTransport('mailer', 1025, false))
        // этот почтовик чисто для проверок - принимает любые логин-пароли
            ->setUsername('user')->setPassword('pass');

        $mailer = new Mailer($transport);

        $email = (new Email())
            ->from('mail@app.test')        // от кого
            ->to('user@app.test')          // кому
            ->subject('Join Confirmation') // тема письма
            ->text('Confirm');             // тело

        $mailer->send($email); // кидает TransportExceptionInterface при ошибках

        $output->writeln('<info>Done.</info>');
        return 0;
    }
}
```

Регистрируем класс команды как обычно в ./api/config/dev/console.php:

```php
<?
return [
    // ...
    'config' => [
        'console' => [
            'commands' => [
                Console\FixturesLoadCommand::class,

                Console\MailerCheckCommand::class,   // <<
                //...
            ]
        ]
    ]
]
```

Запуск
```sh
docker compose run --rm api-php-cli composer app mailer:check
> php bin/app.php --ansi 'mailer:check'
Sending
Done.
```

Идём в браузер на `http://localhost:8082/#`

    откуда,       кому               тема-письма          когда пришло
    mail@app.test user@app.test     Join Confirmation    a few seconds ago

В том числе можно посмотреть подробно всю о письма в разделе `Source`
```
From 	mail@app.test
Subject 	Join Confirmation
To 	user@app.test

    Plain text
    Source

Content-Transfer-Encoding: quoted-printable
Content-Type: text/plain; charset=utf-8
Date: Wed, 06 Dec 2023 16:11:50 +0000
From: mail@app.test
MIME-Version: 1.0
Message-ID: <c7fbbf81b439504d30cc23706d94add9@app.test>
Received: from [127.0.0.1] by mailhog.example (MailHog)

          id RQQ2XHBhGC0Ed08N7xQrwwxg9V240P81ComXhWTVxkk=@mailhog.example; Wed, 06 Dec 2023 16:11:50 +0000
Return-Path: <mail@app.test>
Subject: Join Confirmation
To: user@app.test

Confirm
```


