## Functional Тесты Контроллеров(Action-ов)

- пишем функциональный тест проверяющий работу приложения в целом.
- проверяем какие json-ответы оно возврщает с главной страницы.

Для контроллера(экшена) `./api/src/Http/Action/HomeAction.php`
можно написать и простой юнит тест, и в этом юнит тесте
- инстанцировать класс HomeAction
- передать в сформированный обьект класса ServerRequestInterface.
- проверить возвращаемые из метода handle данные

Но на контроллер такого вида как HomeAction нет особого смысла писать юнит-тест.
т.к. контроллер работае как часть единого приложения -
где есть маршрутизация, контроль доступа и middleware.
Для такого рода компонентов уже надо писать функциональные тесты - проверяющие
работу приложение в целом. Здесь о том как писать такие тесты

## make check

Добавим такую команду обьединяющую все проверки нашего кода.
```Makefile
check: lint analyze test
```
Это будет полезно использовать перед коммитом изменений из локальной машины
в мастер вертку основной кодовой базы(удалённого репозитория)

## Пишем функциональный тест для HomeAction - контроллера.

создаём каталог под все наши функциональные тесты ./api/tests/Functional/
и в нём класс HomeTest.php с неймспейсом `Test\Functional`;

```php
<?php

declare(strict_types=1);

namespace Test\Functional;

use PHPUnit\Framework\TestCase;

class HomeTest extends TestCase
{
    public function testSuccess(): void
    {
    // этот тест должен проверять что главная страница нашего API "открывается"
    // то есть здесь нужно проверить работу всего нашего приложения
    }
}
```

Наше приложение запускается с точки входа `api/public/index.php`
- создаётся di-контейнер со всеми настройками
- на основе di-контейнера создаётся инстанс приложения
- запуск самого приложения `$app->run();`

Вот этот код:
```php
<?
/** @var \Psr\Container\ContainerInterface $container */
$container = require __DIR__ . '/../config/container.php';

/** @var \Slim\App $app */
$app = (require __DIR__ . '/../config/app.php')($container);
$app->run();
```
только надо будет подправить пути для require на один каталог выше вот так:

        $container = require __DIR__ . '/../../config/container.php';

Теперь по аналогии с юнит тестом где мы брали класс и инстанцировали из него
обьект чтобы вызвать его методы и проверить корректность их работы так же и
здесь в функ-ых тестах мы можем взять связку поднимающую всё наше приложение
и инстанцировать его в обьект который будем дальше тестировать.
Ну а так как для создания приложения надо выполнить 3 шага описанных выше
просто берём этот код из index.php и копируем в начало теста testSuccess

метод `$app->run()` производит марштутизацию c вызовом нужного контроллера
и печать ответа (отправка клиенту).

В тестах нам не нужно что-либо "печатать",а нужно только проверять данные.

Смотрим на сам метод run() в исходнике класса который наследует класс нашего
приложения `./api/vendor/slim/slim/Slim/App.php`
```php
<?
class App extends RouteCollectorProxy implements RequestHandlerInterface
{
    /**
     * Run application
     *
     * This method traverses the application middleware stack and then sends the
     * resultant Response object to the HTTP client.
     *
     * @param ServerRequestInterface|null $request
     * @return void
     */
    public function run(?ServerRequestInterface $request = null): void
    {
        if (!$request) {
            $serverRequestCreator = ServerRequestCreatorFactory::create();
            $request = $serverRequestCreator->createServerRequestFromGlobals();
        }

        $response = $this->handle($request);          // *1
        $responseEmitter = new ResponseEmitter();     // *2 кто будет выводить
        $responseEmitter->emit($response);            // *3
    }
}
```

- `*1` - Здесь мы можем увидить что внутри метода run() и идёт передача $request
   через метод handle c возвратом ответа $respose из этого метода
- `*2` - создание обьекта класса ResponseEmitter который будет выводить ответ
- `*3` - сама "печать" то есть отправка ответа(response) из контроллера клиенту
   если глянуть метод emit то там увидим просто "печать" через header и echo.

Таким образом можно не вызывать сам метод `run()` где идёт еще и печать ответа,
а только метод `handle` из строчки `*1` - то есть ту часть метода `run`, которая
отвечает за поиск соответствующего запросу контроллера и получение ответа от него

Дальше для эмуляции вызова главной страницы нашего API нужно послать некий
запрос `$request`

Будем использовать фабрику по создания реквеста для простоты, т.к. иначе придётся
передавать кучу пораметров в конструктор класса `Request`

    $request = (new ServerRequestFactory())->createServerRequest('GET', '/');

Таким запросом мы эмулируем получение главной страницы нашего API
Ну а саму проверку содержимого ответа делаем так же как в первом юнит-тесте:

        self::assertEquals('{}', $response->getBody()->getContents());
        self::assertEquals(200, $response->getStatusCode());

Но здесь есть одна тонкость в вызове метода

    $response->getBody()->getContents();

Первый вызов метода getContents() вернёт содержимое ответа, а последующие могут
уже и не возвращать ничего. т.к. ответ может быть "перематываемым" то есть
работать как стек - запрок getContent() как pop() т.е. "изъять содержимое"
(Проверить это можно через `$response->getBody()->isSeekable()` либо
вызвать метод `$response->getBody()->rewind()` и прочесть контент заново)
поэтому правильнее вместо getContents() использовать явное приведение типа:


        self::assertEquals('{}', (string)$response->getBody());

Когда делаем "каст" приведение типа к string внутри происходит вызов метода
toString() где и происходит автоматически то что там нужно без возможного
изьятия контента из обьекта `$response`

Проверяем наш тест

```sh
make test
docker compose run --rm api-php-cli composer test
> phpunit --colors=always
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.23
Configuration: /app/phpunit.xml

........                                                            8 / 8 (100%)

Time: 00:00.020, Memory: 8.00 MB

OK (8 tests, 23 assertions)
```

Как видим на один тест больше (до создания этого теста было 7)

Такой функциональный тест будет работать намного медленнее чем юнит-тесты т.к.
В Юнит тест обысно проверяет только один конкретный класс нашего кода.
Тогда как в Функциональных тестах просиходит загрузка всего нашего приложения:
- загрузка всех наших конфигов, создание di-контейнера
- загрузка всех классов как своих собственных так и используемых библиотек и
  фреймворка с его зависимостями т.е. подгрузка каталог `vendor`

В итоге весь функциональный тест будет выглядеть вот так:
```php
<?php

declare(strict_types=1);

namespace Test\Functional;

use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;

class HomeTest extends TestCase
{
    public function testSuccess(): void
    {
        /** @var \Psr\Container\ContainerInterface $container */
        $container = require __DIR__ . '/../../config/container.php';

        /** @var \Slim\App $app */
        $app = (require __DIR__ . '/../../config/app.php')($container);

        $request = (new ServerRequestFactory())
            ->createServerRequest('GET', '/');

        $response = $app->handle($request);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('{}', (string)$response->getBody());
    }
}
```

Перепишем этот код в более красивый вид -разделяя функционал в разные методы
```php
<?php
class HomeTest extends TestCase
{
    /**
     * Сам тест
     */
    public function testSuccess(): void
    {
        $response = $this->app()->handle(self::json('GET', '/'));

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('application/json', $response->getHeaderLine('Content-Type'));
        self::assertEquals('{}', (string)$response->getBody());
    }
    // все остальные методы этого тест-класса методы-помощники

    // создаём http-запрос на приёмку от веб-сервера только json-содержимого
    private static function json(string $method, string $path): ServerRequestInterface
    {
        return self::request($method, $path)
            ->withHeader('Accept', 'application/json')
            ->withHeader('Content-Type', 'application/json');
    }

    // формирует любые запросы по указанному методу(GET|POST) и url-пути
    private static function request(string $method, string $path): ServerRequestInterface
    {
        return (new ServerRequestFactory())->createServerRequest($method, $path);
    }

    // Создаёт наше приложение, с загрузкой всей конфигурации в di-контейнер
    private function app(): App
    {
        /** @var \Psr\Container\ContainerInterface $container */
        $container = require __DIR__ . '/../../config/container.php';

        /** @var \Slim\App $app */
        $app(require __DIR__ . '/../../config/app.php')($container);
        return $app;
    }
}
```

метод `json` `request` сделали static-ческими т.к. внутри себя они используют
фабрику - тоже статический метод

метод `app` тоже можно было бы сделать статическим, но этого сделать не даелаем
так как со временем будем подключать фикстуры(?) и всё что нужно для работу с
БазамиДанных. А при этом понадобится хранить доп состояние и кэшировать его

## Выносим общий переиспользуемый функционал в отдельный класс WebTestCase

./api/tests/Functional/WebTestCase.php:
```php
<?php

declare(strict_types=1);

namespace Test\Functional;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use Slim\Psr7\Factory\ServerRequestFactory;

class WebTestCase extends TestCase
{
    protected static function json(string $method, string $path): ServerRequestInterface
    {
        return self::request($method, $path)
            ->withHeader('Accept', 'application/json')
            ->withHeader('Content-Type', 'application/json');
    }

    protected function request(string $method, string $path): ServerRequestInterface
    {
        return (new ServerRequestFactory())->createServerRequest($method, $path);
    }

    protected function app(): App
    {
        /** @var \Slim\App $app */
        $app = (require __DIR__ . '/../../config/app.php')($this->container());
        return $app;
    }

    private function container():ContainerInterface
    {
        /** @var \Psr\Container\ContainerInterface $container */
        $container = require __DIR__ . '/../../config/container.php';
        return $container;
    }
}
```

Теперь код самого тестового класса используя наследование от WebTestCase будет:
./api/tests/Functional/HomeTest.php:
```php
<?php

declare(strict_types=1);

namespace Test\Functional;

class HomeTest extends WebTestCase
{
    /**
     * @covers HomeAction::handle
     */
    public function testSuccess(): void
    {
        $response = $this->app()->handle(self::json('GET', '/'));

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('application/json', $response->getHeaderLine('Content-Type'));
        self::assertEquals('{}', (string)$response->getBody());
    }
}
```
Проверяем работает ли наш тест
```sh
make test
docker compose run --rm api-php-cli composer test
> phpunit --colors=always
PHPUnit 10.3.4 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.23
Configuration: /app/phpunit.xml

........                                                            8 / 8 (100%)

Time: 00:00.020, Memory: 10.00 MB

OK (8 tests, 24 assertions)
```

Чтобы убедиться что тест вообще запускает можно подставить в последний ассерт
неправильное значение например '{X}' - тогда тест должен будет упасть.


## Об функция с побочными эффектами в Функциональных тестах

За счёт чего получили простоту написания функциональных тестов сравнимую с
простотой написания юнит тестов:

1) вынос создания всего своего приложения в отдельный файл api/config/app.php
  в этом файле идёт bootstrap-ипрование - т.е. создание нашего приложения из
  переданного di-контейнера со всем необходимым подключением middleware и роутов

  Если бы мы не сделали этот вынос кода по созданию приложения в отдельный файл
  а оставили его в api/public/index.php то пришлось бы копипастить весь этот код
  внутрь хелпер-класса WebTestCase

2) В нашем проекте мы не используем никаких singleton-ов, сервис-контейнеров,
   статических перезаписываемых переменных. То есть не используем в своём коде
   всё то, что может создавать конфликты.

В частности если в своих контроллерах(Action) использовать прямое обращение к
таким функциям как header('Content'); echo ''Ж

```php
<?
class HomeAction implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        header('Content...'); // прямой вывод заголовка
        // открытие сессий...
        echo ''; // прямой вывод результата вместо обьекта-враппера new Response()
    }
}
```
Так вот если бы писали контроллеры в таком стиле, то в своих тестах первый
контроллер бы отрабатывал нормально а дальше бы шел конфликт.
нечто вроде заголовок уже отправлен и т.д.
так же и с открытыми сессиями если бы мы их спользовали напрямую были бы конфликты
и ошибки - "нет такой сессии" и прочее


Таким образом когда мы не используем простые php функции с побочными эффектами
такие как header echo и прочее. то не имеет значения откуда мы запускаем свой
код из веба либо из консоли. Конкретный пример на нашем контроллере HomeAction
в нём нет никаких побочных функций которые напрямую что-либо выводят клиенту
входные данные приходят через `$request` а сформированный ответ передаётся через
специально созданного для этого обьект обёртку реализующий интерфейс
ResponseInterface. в нашем случае обёртка хранящее "состояние" - ответ
это класс JsonResponse наследуемый от Slim\Psr7\Response; в полях этих классов
(приватными переменными) и хранится ответ.
(Смотри исходники этих классов:)

```php
protected $headers;  // HeadersInterface
protected $body;     // StreamInterface
```

Сам же вывод ответа из обьекта-обёртки будет происходить в `$app->run();`
Мы же в своих тестах ничего не выводим а работаем напрямую с сформированным
содержанием обьектов-обёрток хранящих ответ. Такой подход позволяет избежать
парсинга выводимого через echo текста, работая с данными напрямую.


