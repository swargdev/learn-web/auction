<?php

declare(strict_types=1);

namespace App\Frontend\Test;

use App\Frontend\FrontendUrlGenerator;
use PHPUnit\Framework\TestCase;

/**
 * @covers FrontendUrlGenerator
 * 07-12-2023
 * @author Swarg
 */
class FrontendUrlGeneratorTest extends TestCase
{
    public function testEmpty(): void
    {
        $generator = new FrontendUrlGenerator('http://site');

        self::assertEquals('http://site', $generator->generate(''));
    }

    public function testPath(): void
    {
        $generator = new FrontendUrlGenerator('http://site');

        self::assertEquals('http://site/path', $generator->generate('path'));
    }

    public function testPathWithParams(): void
    {
        $generator = new FrontendUrlGenerator('http://site');

        $res = $generator->generate('path', ['x' => '1', 'y' => 'b']);
        self::assertEquals('http://site/path?x=1&y=b', $res);
    }

    public function testOnlyParams(): void
    {
        $generator = new FrontendUrlGenerator('http://site');

        $res = $generator->generate('', ['x' => '1', 'y' => 'b']);
        self::assertEquals('http://site?x=1&y=b', $res);
    }
}
