## Ansible PlayBook Tasks по установке certbot и генерации ssl-сертификатов

### Старый Способ для debian 10 с добавлением backports-репозитория

(Чисто для иллюстрации как можно использовать нестандартные репозитории)

Пример задачи для установки backports репозитория и его использования для
установки certbot-a из этого репозитория.
```yml
- name: Set up the repository
  apt_repository:
    repo: 'deb http://ftp.debian.org/debian buster-backports main'
    state: present
    filename: buster-backports      # в какой файл сохранить этот репозиторий
    update_cache: yes

- name: Install Certbot
  apt:
    name: certbot
    state: present
    default_release: buster-backports  # файл с координатами репозитория
    update_cache: yes
```
Здесь `default_release: buster-backports` - для того, чтобы установка пакета
происходила из конкретного репозитория который описан в этом конкретном файле
с именем `buster-backports`, который мы добавили в задаче `Set up the repository`


Для Debian 11 пакет certbot доступен из стандартного репозитория.
проверить это можно вот так
```sh
apt-cache policy certbot
certbot:
  Installed: (none)
  Candidate: 1.12.0-2
  Version table:
     1.12.0-2 500
        500 http://ftp.debian.org/debian bullseye/main amd64 Packages
```

Поэтому для Debian 11 задачу по установке certbot описываю так:
```yml
- name: Install Certbot
  apt:
    name:
      - certbot
      - python3-certbot-nginx
    state: present
    update_cache: yes
```


### Настройка конфигурации certbot-а

provisioning/tasks/setup_certbot.yml:
```yml
- name: Copy Certbot configuration
  copy:
    src: files/cli.ini                 # откуда брать конфиг файл
    dest: /etc/letsencrypt/cli.ini     # куда помещать конфиг файл
    owner: root
    group: root
    mode: 0644
```
Сам конфиг размещаем в `./provisioning/files/cli.ini` от корня проекта

./provisioning/files/cli.ini:
```
authenticator = webroot
webroot-path = /var/www/html
text = True
```

webroot - это тип запуска по которому должен работать certbot при обновлении
сертификатов, далее описываем в каком каталоге certbot должен генерировать
свои файлы для проверки подленности владения сайтом(доменом).
Так же для нашего сервера нужно сделать так чтобы сгенеренные certbot-ом файлы
были видны и доступны снаружи из интернета. Это у нас сделано в gateway для
продакшена и в snippets-ах указали тот же самый путь для certbot-a

./gateway/docker/common/nginx/snippets/certbot.conf:
```nginx
location /.well-known/acme-challenge {
    root /var/www/html;
}
```
Это как раз и сделано для того, чтобы глобальной сети наш сайт делал доступными
файлы по адресу `http://my-domain.com/.well-known/acme-challenge`  а на сервер
они находились при этом в `/var/www/html/`
т.е. /var/www/html/ - это каталог используемый certbot-ом при генерации
и ssl-сертификата.

Ну и так как этот конфиг файл cli.ini должен быть на удалённой машене то мы
для этого и делаем эту задачу `Copy Certbot configuration` где кроме копирования
идёт еще установка нужных прав и владельца на копируемый файл

далее еще в `setup_certbot.yml` как и для докера добавляем задачи по очистке
apt-кэша: `Remove useless packages`, `Remove useles dependencies`


## Генерация ssl-сертификатов

При первом запуске на чистом сервере сразу должна идти генерация сертификатов
Когда certbot работает по параметру запуска webroot

Если бы мы не указывали тип работы через webroot
т.е. задали ли бы файл ./provisioning/files/cli.ini  вот так:
```
text = True
```
то для первоначального запуска можно было бы использовать `--standalone`:

```yml
- name: Generate new certificate
  shell: "certbot certonly --standalone --noninteractive --agree-tos --email {{ certbot email }} -d {{ item.item }}"
  with_items: "{{ letsencrypt_certs.results }}"
  when: not item.stat.exists
```

когда указан флаг `--standalone` certbot поднимает свой собственный встроенный
cервер для подтверждения подленности при работе с letsencrypt по выдаче сертификата

но если в конфиге уже указан режим работы webroot то использовать этот флаг
--standalone не выйдет certbot просто выдаст ошибку "тип не могу переключится на
другой режим"
Отсюда возникает вопрос как сделать так чтобы certbot сработал правильно при
первом запуске playbook-a на чистом удалённом хосте(VMке) когда там еще нет
ни докера, ни nginx-а ни описанного нами gateway-я. Т.е. без сервера certbot
не сможет пройти аутентификацию, а значит и выдача сертификата не пройдёт.
Возможное решение - поднять каконибудь временный легковесный http-сервер
только на время подтверждения владения доменом при выдаче ssl-сертификата.

А так как в момент первоначальной генерации ssl-ключа на вмке наше веб-приложение
еще не раскатано то значит 80 порт еще свободен и уже установлен докер.
Поэтому можно просто установить контейнер с Apache-сервером пробросив в него
через volume нужный каталог.

```yml
- name: Up certbot standalone Apache
  shell: "docker run -d --name apache -v /var/www/html:/usr/local/apache2/htdocs/ -p 80:80 httpd:2.4"

- name: Generate new certificate
  shell: "certbot certonly --noninteractive --agree-tos --email {{ certbot email }} -d {{ item.item }}"
  with_items: "{{ letsencrypt_certs.results }}"
  when: not item.stat.exists

- name: Down certbot Apache
  shell: "docker rm -f apache"
```

Мы здесь перед генерацией сертификата(заметь без ключа `--standalone`) поднимаем
контейнер апач а после генерации - удаляем его.

Но это будет работать только когда provisioning скрипт через Ansible будет запущен
впервые, (когда нет никаких http-серверов которые бы занимали 80 порт)
То есть когда еще сам проект стоять не будет - то всё пройдёт гладко, запустится
контейнер с апачем сгенерятся сертифекаты и отключат этот апач-сервер.
Но уже после установки проекта развернётся gateway с nginx-сервером и займёт свой
80 порт. А значит таким же способом запустить повторную генерацию сертификата уже не получится. Конкретно команда:
```sh
docker run -d --name apache -v /var/www/html:/usr/local/apache2/htdocs/ -p 80:80 httpd:2.4
```
просто упадёт с ошибкой - "порт уже занят"
Решение - перед запуском задачи проверять занят ли 80 порт - и если нет - тогда
запускать контейнер с апатчем.

Вот задача с проверкой занят ли 80 порт
```yml
- name: Check if server is running
  wait_for:                   # ждём
    port: 80                  # соединения по порту 80
    timeout: 1                # сколько секунд ждать
  register: port_check
  ignore_errors: yes          # продолжить работу даже если порт закрыт
```
Это задача приостановит поток выполнения всех задач ожидая соединения на 80 порту
т.е. будет происходить проверка открыт ли порт. Ждать не более 1 секунды.
Обычно если задача падает с ошибкой то все последующие задачи не выполняются,
т.е. playbook падает с ошибкой и прекращает дальнейшее своё выполнение.
Поэтому если не поставить ignore_errors: yes|true то задача не дождавшись
соединения по 80 порту упадёт с ошибкой, а нам нужно просто проверить и пойти
дальше. Т.е. так мы игнорируем ошибку

Дальше мы используем спец фишку Ansible, которая позволяет делать задачи
зависимыми от неких условий. Потиму условий и ветвлений.
это в этом блоке прописано через `register: port_check` - эта директива
регистрирует переменную с именем port_check и после выполнения данной задачи
её значение будет заполнено результатом выполнения данной задачи.
и эту переменную можно использовать дальше в других задачах

используем значение `port_check` для условного запуска задачи вот так:

```yml
- name: Up certbot standalone Apache
  shell: "docker run -d --name apache -v /var/www/html:/usr/local/apache2/htdocs/ -p 80:80 httpd:2.4"
  when: port_check.failed == true # условие при котором запускать данную задачу
```
Таким обрзом данная задача будет запускаться только тогда когда порт свободен
эту же проверку вставляем в задачу по удалению контейнера с патчем


## Циклы в Ansible Task-ах with-items

кроме директивы условной проверки `when` в Ансибл есть еще и циклы

Пример Ансибл-Задачи с циклом
```yml
- name: Check if certificate already exists
  stat:
    path: /etc/letsencrypt/live/{{ item }}/cert.pem
  register: letsencrypt_certs                # куда помещать результат проверки
  with_items: "{{ certbot_hosts }}"
```

Здесь запускается цикл проверок на нали
stat - это директива для проверки существует ли файл на диске
stat.path - это путь к файлу который надо проверить
{{ item }} - это подстановка конкретного значения которое вклеится в полное имя
{{ item }}-ы беруться из списка {{ certbot_hosts }} которые мы можем задать сами

прописываются свои переменные вот так:

```yml
- name: Provision Site                      # имя playbook-а
  # ...
  vars:
    certboot_hosts:                         # задаём список строковых значений
      - demo-auction.my-domain.pro
      - api.demo-auction.my-domain.pro
  tasks:
    - import_tasks: tasks/generate_certificates.yml
```
Здесь мы и определяем подстановки значений для списка который будет перебираться
в цикле задачи `Check if certificate already exists`

Т.е. до задачи создали свою переменную значение которой строковый список, а далее
уже в нужной нам задаче мы можем перебрать элементы этого списка в цикле
перебирая по одному за раз обращаясь с очередному элементу по имени {{ item }}
и вклеивая его значение в результирующую строку пути к файлу наличие которого и
проверяем через `stat`

Ключевое слово `with_items` и позволяет огранизовать цикл внутри задачи
Результаты выполнения stat помещаются в переменную с именем заданным вот так

    register: letsencrypt_certs

то есть letsencrypt_certs - это имя переменной которая будет доступна дальше
по скрипту. Это тоже список и по нему тоже можно пройтись циклом чтобы например
в нашем случае сгенерировать сертификаты для тех доменов для которых их еще нет.

Это мы и делаем в этой задаче по генерации одного или нескольких сертификатов
```yml
- name: Generate new certificate
  shell: "certbot certonly --noninteractive --agree-tos --email {{ certbot email }} -d {{ item.item }}"
  with_items: "{{ letsencrypt_certs.results }}"    # список для перебора
  when: not item.stat.exists
```

`with_items: "{{ letsencrypt_certs.results }}"` - организуем цикл перебора
значений из списка `results` в переменной `letsencrypt_certs`

на каждой итерации цикла все элементы из списка results будут доступны в
{{ item.item }}
- первый item - это очередной элемент из списка results
- второй item - это элемент из цикла с stat-проверкой полное имя к ключу сертификата

Нам нужно генерировать только те сертификаты которых еще нет, поэтому и добавляем
условие `when: not item.stat.exists`
этим условим мы говорим - выполнять команду генерации `shell: "certbot ..."`
только для тех элементов из списка всех сертификатов которых еще нет.

Т.е. with_items - огранизуем цикл when - условие для каждой итерации внутри цикла

В общем таким образом у нас есть задачи позволяющие нам сгенерировать целый
список нужных сертификатов если их еще нет на сервере.



