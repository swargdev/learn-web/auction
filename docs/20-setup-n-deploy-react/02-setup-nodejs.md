## Подключение nodejs отдельным сервисом

В нашем проекте разработка идёт через docker-compose, а значит мы можем
использовать готовые образы с нужным нам языком. Поэтому мы будем использовать
официальный образ nodejs с https://hub.docker.com/_/node

создаём Dockerfile где и будет у нас содержаться NodeJS

./frontend/docker/development/node/Dockerfile
```Dockerfile
FROM node:18-alpine
WORKDIR /app
```

Этот докер-файл будет описывать еще один сервис для docker-compose.yml

когда создавали Dockerfile для написания API на php мы там сами руками
внутрь докер-образов подключали пакетный менеджер `composer`.
для nodejs такое делать не надо т.к. уже есть готовые образы с предуставновленными
в них nodejs

Добавляем в dev окружение новый сервис frontend-node-cli
для только что описанного нами Dockerfile-а
./docker-compose.yml
```yml
version: '3.7'
services:
  gateway:
    # ...

  frontend:
    build:
      context: frontend/docker
      dockerfile: development/nginx/Dockerfile
    volumes:
      - ./frontend:/app

  frontend-node-cli:
    build:
      context: frontend/docker/development/node
    volumes:
      - ./frontend:/app

  #...
```

Здесь в сервисе `frontend-node-cli` мы так же как и в composer прокидываем
внутрь образа каталог с исходным кодом нашего фронтенда.
Теперь через сервис `frontend-node-cli` мы сможем запускать команды для
добавления зависимостей, установки шаблона проекта и проч. точно так же
как это у нас быдо для бэкенда на основе пакетного менеджера composer

