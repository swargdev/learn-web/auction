<?php

declare(strict_types=1);

namespace App\Auth\Service;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\Token;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email as MimeEmail;
use Twig\Environment;

/**
 * 12-10-2023
 * @author Swarg
 */
class PasswordResetTokenSender
{
    private MailerInterface $mailer;
    private Environment $twig;

    public function __construct(MailerInterface $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function send(Email $email, Token $token): void
    {
        $view = 'auth/password/confirm.html.twig';

        $message = (new MimeEmail())
        ->subject('Password Reset')
        ->to($email->getValue())
        ->html($this->twig->render($view, [ 'token' => $token ]));

        $this->mailer->send($message);
    }
}
