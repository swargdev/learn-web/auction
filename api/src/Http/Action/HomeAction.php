<?php

declare(strict_types=1);

namespace App\Http\Action;

use App\Http\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use stdClass;

class HomeAction implements RequestHandlerInterface
{
    /**
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        // file_put_contents('php://stdout', 'Success');
        // file_put_contents('php://stderr', 'Error');
        // throw new \Slim\Exception\HttpNotFoundException($request);
        return new JsonResponse(new stdClass());
    }
}
