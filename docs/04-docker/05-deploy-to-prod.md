## Deply на продакшен

Шаги того что должно происходить при деплои(развёртывании приложения на сервере):

- создать новый каталогна удалённом сервере для деплоя
- закинуть docker-compose-production.yml
- настроить переменные окружения REGISTRY IMAGE_TAG - какой конкретно образ ставить
- запустить docker compose up -d с нужным yml-файлов - запустить приложение

Делаем консольную команду для Deploy-я через Makefile

Основная идея как это делать:

Через ssh-соединение делаем каталог site/ и копируем в него по сети
docker-compose-docker.yml рядом добавляем файл .env для докер-композа куда
кладём нужные EnvVar-ы

    site/
       docker-compose-production.yml
       .env
          REGISTRY=registry.my-domain.com
          IMAGE_TAG=master-1

       # затем поднимает приложение через консольную команду
       docker compose -f docker-compose-production.yml up -d

Минусы такого подхода
- при обновлении сайта до новой версии придётся удалять этот каталог site
  и перезаписывать всё заново. прописывать новый таг версии образа и перед
  подьёмом использовать

       docker compose -f docker-compose-production.yml down --remove-orphans

`--remove-orphans` - опция для удаления всех контейнеров начинающихся на один
и тот же префикс(Для того чтобы удалить все возможно переименнованные контейнеры)

Так же надо сначала скачать новые образы для развёртывания а потом уже
останавливать сайт. Чтобы сайт долго не простаивал и не ждал скачивания новых
образов. Поэтому шаги такие -
1) скачать новые образы
2) остановить всё что сейчас запущено
3) поднять скаченные образы

       docker compose -f docker-compose-production.yml pull
       docker compose -f docker-compose-production.yml down --remove-orphans
       docker compose -f docker-compose-production.yml up -d


--build флаг - атоматически сверять изменилось ли образы контейнеров не
перезапускать только те контейнеры из стака для которых есть изменённые обрзы.

       docker compose up --build -d

таким образом можно вообще сократить до (здесь down подразумевается в флаге --build)
```sh
docker compose -f docker-compose-production.yml pull
docker compose -f docker-compose-production.yml up --build --remove-orphans -d
```

Это позволит перезапускать контейнеры поштучка уменьшив время простоя сервера.


Можно пойти дальше и улучшить этот подход, когда деплой идёт в один и тот же каталог
Для возможности отката до прошлой версии можно к имени каталога добавлять номер
билда site_1/ site_2 и так далее. А чтобы эти номера не шли в имена префиксов
создаваемых контейнеров переоределить имя проекта через `COMPOSE_PROJECT_NAME`



    site_1/
       docker-compose-production.yml
       .env
          COMPOSE_PROJECT_NAME=site
          REGISTRY=registry.my-domain.com
          IMAGE_TAG=master-1

    site_2/
       docker-compose-production.yml
       .env
          COMPOSE_PROJECT_NAME=site
          REGISTRY=registry.my-domain.com
          IMAGE_TAG=master-2

поэтому при запуске из любой директории `docker compose` будет воспринимать это
как запуск одного и того же проекта - имя то задано через EnvVar-ы

Плюс такого подхода - безболезненный и очень быстрый откат до прошлой версии
в слушае ошибок при деплои новой.

Далее когда возникнет нужда запускать команды докера по крону нужно будет
обеспечить в скрипте постоянный путь к сайту - сделаем это через символические
ссылки:

    site/ -> size_2/

Например надо запускать такой крон-скрипт
```
* 1 * * cd /root/site/ && docker compose rm --rf api-php-cli processor.php
```

```sh
ln -s site_2 site
```
И далее уже при каждом деплои переопределять символическую ссылку на каталог
с последней версией сайта

Таким образос создав подобную команду для деплоя
```Makefile
deploy:
	ssh ${HOST} -p ${PORT} 'rm -rf site_${BUILD_NUMBER}'
	ssh ${HOST} -p ${PORT} 'mkdir site_${BUILD_NUMBER}'

	scp -P ${PORT} docker-compose-production-env.yml ${HOST}:site_${BUILD_NUMBER}/docker-compose-production.yml
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "COMPOSE_PROJECT_NAME=auction" >> .env'
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "REGISTRY=${REGISTRY}" >> .env'
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "IMAGE_TAG=${IMAGE_TAG}" >> .env'
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker compose -f docker-compose-production.yml pull'
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker compose -f docker-compose-production.yml up --build --remove-orphans -d'
	ssh ${HOST} -p ${PORT} 'rm -f site'
	ssh ${HOST} -p ${PORT} 'ln -sr site_${BUILD_NUMBER} site'
```

Достаточно иметь некий сервер с установленым на нём docker и docker-compose
и на него уже ножно деплоить своё приложение

