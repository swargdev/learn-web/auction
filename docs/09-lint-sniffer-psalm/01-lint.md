# Устанавливаем софт для контроля технического написания нашего кода

Установим библиотеки позволяющие
- производить анализ php-кода (работоспособность, стиль, строгая типизация)

1) Проверка того что все наши php-скрипты исправны, без опечаток и ошибок.

Задача: пропускать в продакшен только проверенный код,
чтобы исключить возможность пронкновения любых ошибок на продакшен сервера.
Ренение: lint-ер - консольный "проверяльщик" корректности php-кода

Пути возникновения ошибок:
- случайные нажатия или опечатки в большой кодовой базе, не заметили подствеку
  ошибки в IDE и не запустили перед комитом проверку `Code->Inspect Code...`
- частые мержы и rebase-ы своего репозитория
  могут приводить к ошибкам слияния и ребейза.

Например при слиянии могут просиходить конфликты которые нужно разрешать самому
и при большом количестве конфликтор случайно пропустили "ёлочки" в index git-a

Что такое "ёлочки" при разрешении конфиликтов при слияни git выдаёт такое:
```php
# ...
class JsonResponse extends Response
{
<<<<<<<
public function __construct ($data).
=======
public function __construct($data, int $status = 200)
>>>>>>>
{

    parent:: __construct(
    # ...
```

<<<<< VAR1 ====== VAR2 >>>>> - это интерфейс git-а которой он предаставляет
разработчику для того чтобы разработчки выбрал какой вариант из двух оставить.
Нужно самому выбрать по смыслу один кусок(строчку) кода и удалить всё лишнее.


## PHP встроенный lint-ер

В сам php встроен собственный lint-ер - режим при котором происходит проверка
корректности php-кода. Высвечивающий все найденные ошибки.
```sh
docker compose run --rm api-php-cli php -l bin/app.php
```
Когда нет ошибок выдаёт:
```
No syntax errors detected in bin/app.php
```

Пример того как он будет информировать об ошибках:
```
PHP Parse error:  syntax error, unexpected token "*" in bin/app.php on line 9

Parse error: syntax error, unexpected token "*" in bin/app.php on line 9
Errors parsing bin/app.php
```

Ограничение встроенного в php линтера в том, что он может проверять за раз
только один php-файл. Решение - брать все свои исходники и запускать линтер для
каждого отдельного файла в цикле. Пример shell-кода для этого:
```sh
find bin config public src -name "*.php" -print0 | xargs -0 -n1 php -l
```
- найти все файлы с расширением *.php в каталогах bin config public src
- запустить для каждого найденного файла lint-ер через команду `php -l`
  (то есть для каждого файла будет запускаться `php -l <путь к файлу>`


Можно добавить алиас в composer.json:
```json
{
"scripts": {
  "lint": "find bin config public src -name \"*.php\" -print0 | xargs -0 -n1 php -l",
  "app": "php bin/app.php --ansi"
}
}
```

Добавляем команды в Makefile Для удобного быстрого запуска lint-а
```Makefile
lint: api-lint

api-lint:
	docker compose run --rm api-php-cli composer lint
```


```sh
make lint
```
```
docker compose run --rm api-php-cli composer lint
> find bin config public src -name "*.php" -print0 | xargs -0 -n1 php -l
No syntax errors detected in bin/app.php
No syntax errors detected in config/middleware.php
No syntax errors detected in config/container.php
No syntax errors detected in config/dependencies.php
No syntax errors detected in config/routes.php
No syntax errors detected in config/common/console.php
No syntax errors detected in config/common/system.php
No syntax errors detected in config/app.php
No syntax errors detected in public/index.php
No syntax errors detected in src/Http/JsonResponse.php
No syntax errors detected in src/Http/Action/HomeAction.php
No syntax errors detected in src/Console/HelloCommand.php
```

Недостаток такого подхода - медленная проверка.
Если в проекте много файлов то проверка может происходить долго, т.к. проверка
каждый раз будет проверять все файлы проекта причем в один поток по очереди.
Решение:
- использовать кэширование и проверять только изменившиеся с прошлой
  проверки файлы.
- сделать многопоточную проверку - запускать сразу в несколько потоков
  может дать выйгрышь в скорости проверки на многоядерных ПК


## phplint - готовое решение для быстрой проврки php-кода
https://github.com/overtrue/phplint

Библиотека- обёртка над стандартным линтером `php -l`. возможности:
- запускать проверку в несколько потоков
- поддержка кэшей
- красивый отчёт о проделанной проверки

https://github.com/overtrue/phplint/blob/main/docs/installation.md

```sh
composer global require overtrue/phplint
```

Пример конфиг-файла `.phplint.yml`:
```yml
path: ./
jobs: 10                          # на сколько протоков запускать проверку
cache: build/phplint.cache        # где держим кэш
extensions:
  - php                           # *.php
exclude:
  - vendor                        # игнорировать и не проверять каталог vendor
```

Запускать линтер можно так:
```sh
./vendor/bin/phplint
```
Установить самую последнюю версию `overtrue/phplint`:
```sh
docker compose run --rm api-php-cli composer require overtrue/phplint --dev
```

composer.json:
```
    "require-dev": {
        "overtrue/phplint": "^9.0",                       # << A New Installed
        "roave/security-advisories": "dev-master"
    },

    "scripts": {
        "lint": "phplint --no-cache",                    # Chenge lint command
        "app": "php bin/app.php --ansi"
    }
```

```sh
du -hcd 1 api/vendorA
580K	api/vendor/slim
40K	api/vendor/fig
528K	api/vendor/php-di
852K	api/vendor/overtrue
12K	api/vendor/bin
236K	api/vendor/nikic
156K	api/vendor/laravel
28K	api/vendor/ralouphie
380K	api/vendor/psr
224K	api/vendor/composer
3.3M	api/vendor/symfony
6.3M	api/vendor
6.3M	total
```

если нужна конкретная версия можно просто прописать ей в dockercompose.json
затем вызвать команду
```sh
docker compose run --rm api-php-cli composer install
```
алиас для этой команды у нас `make api-composer-install`

Добавляем конфиг-файл для phplint `./api/.phplint.yml`:
```yml
path: ./    # конревой каталог т.е. запуск изнутри докер-образа то это /app/
jobs: 4
extensions:
  - php
exclude:
  - vendor
```

Запускаем lint - проверку кода
```sh
make lint
```
```
docker compose run --rm api-php-cli composer lint
> phplint --no-cache
phplint 9.0.4 by overtrue and contributors.

Runtime       : PHP 8.1.23
Configuration : /app/.phplint.yml
............
Time: < 1 sec, Memory: 4.0 MiB, Cache: 0 hit, 12 misses
 [OK] 12 files
```

## Добавляем каталог ./api/var/ - для хранения всех временных данных (кэши/логи)

mkdir ./api/var
./api/var/.gitignore:
```
*
!.gitignore
```
тако .gitignore означает - игнорировать всё из текущего каталога кроме .gitignore

Затем добавляем этот каталог в список исключений lint-ера и прописываем кэш
```
path: ./
jobs: 8
cache: var/.phplint.json
extensions:
  - php
exclude:
  - vendor
  - var
```

теперь можно убрать флаг --no-cache для phplint из composer.json
проверяем работает ли теперь кэш

```sh
make lint
docker compose run --rm api-php-cli composer lint
```
```
> phplint
phplint 9.0.4 by overtrue and contributors.
Runtime       : PHP 8.1.23
Configuration : /app/.phplint.yml
............                               # каждая точка - проверенный файл
Time: < 1 sec, Memory: 4.0 MiB, Cache: 0 hit, 12 misses
[OK] 12 files
````
Повторный запуск даст такой результат:
```
> phplint
phplint 9.0.4 by overtrue and contributors.
Runtime       : PHP 8.1.23
Configuration : /app/.phplint.yml
                                           # ни один файл не проверялся!
Time: < 1 sec, Memory: 4.0 MiB, Cache: 12 hits, 0 miss
[OK] 12 files
```
Здесь видем что все 12 файлов "попали в кэш" - т.е. не изменились и поэтому их
проверка не запускалась. Теперь если изменить код в одном файле и перезапустить
линтер - то покажет "одну точку".


