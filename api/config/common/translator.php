<?php

declare(strict_types=1);

use App\Http\Middleware;
use Psr\Container\ContainerInterface;
use Symfony\Component\Translation\Loader\PhpFileLoader;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Contracts\Translation\TranslatorInterface;

$symfony_validator_res = __DIR__ . '/../../vendor/symfony/validator/Resources/';

return [
    TranslatorInterface::class => DI\get(Translator::class),

    Translator::class =>
    static function (ContainerInterface $container): Translator {
        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{lang:string,resources:array<string[]>} $config
         */
        $config = $container->get('config')['translator'];

        $translator = new Translator($config['lang']);
        $translator->addLoader('php', new PhpFileLoader());
        $translator->addLoader('xlf', new XliffFileLoader());

        foreach ($config['resources'] as $resource) {
            $translator->addResource(...$resource);
        }

        return $translator;
    },


    Middleware\TranslatorLocale::class =>
    static function (ContainerInterface $container): Middleware\TranslatorLocale {
        /** @var Translator $translator */
        $translator = $container->get(Translator::class);
        /**
         * @psalm-suppress MixedArrayAccess
         * @var array{allowed:string[]} $config
         */
        $config = $container->get('config')['locales'];
        return new Middleware\TranslatorLocale($translator);
    },


    Middleware\ContentLanguage::class =>
    static function (ContainerInterface $container): Middleware\ContentLanguage {
        /**
         * @psalm-suppress MixedArrayAccess
         * @var string[] $allowed
         */
        $allowed = $container->get('config')['locales']['allowed'];
        return new Middleware\ContentLanguage($allowed);
    },


    'config' => [
        'translator' => [
            'lang' => 'en',
            // 'lang' => 'ru',
            'resources' => [
                [
                    'xlf',
                    $symfony_validator_res . 'translations/validators.ru.xlf',
                    'ru',
                    'validators',
                ],
                [
                    'php',
                    __DIR__ . '/../../translations/exceptions.ru.php',
                    'ru',
                    'exceptions',
                ],
            ],
        ],
        'locales' => [
            'allowed' => ['en', 'ru'],
        ],
    ],
];
