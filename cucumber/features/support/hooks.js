const { Before, After, Status, setDefaultTimeout } = require('@cucumber/cucumber')
const puppeteer = require('puppeteer')

setDefaultTimeout(10 * 1000)

Before({ timeout: 30000 }, async function () {
  this.browser = await puppeteer.launch({
    headless: 'new',
    args: [
      '--disable-dev-shm-usage',
      '--no-sandbox'
    ]
  })
  this.page = await this.browser.newPage()
  await this.page.setViewport({ width: 1280, height: 720 })
})

After(async function (testCase) {
  if (testCase.result.status === Status.FAILED) {
    const uri = testCase.pickle.uri
    const name = uri.replace(/^\/app\/features\//, '').replace(/\//g, '_') +
      '-' +
      testCase.pickle.name.toLowerCase().replace(/[^\w]/g, '_')
    await this.page.screenshot({ path: 'var/' + name + '.png', fullPage: true })
  }
  await this.page.close()
  await this.browser.close()
})
