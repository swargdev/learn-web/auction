## Делаем процедуру подтверждения регистрации по емейлу JoinByEmail\Confirm

Дорабатываем JoinByEmail чтобы можно было работать с токенами:

- Добавляем состояние в сущность User (в ожинании/активен)
- Добавляем токены со сроком действия и их валидацию

- Что уже есть на данный момент
```
Join By Email
  Request      << + сделано
    Command       +
    Handler       +
  Confirm              << сейчас будем делать "Подтвержение регистрации"
    Command       -
    Handler       -
```
```
./api/src/Auth/Command/
└── JoinByEmail
    └── Request
        ├── Command.php
        └── Handler.php
```
Написали регистрацию User-а через `App\Auth\Command\JoinByEmail\Request\Handler`
его метод handle - это только запрос на регистрацию, дальше нужно подтверждение
для подтверждения регистрации нужно чтобы у User где-то хранился текущий статус
и при создании User получал статус "не активен" - в "ожидании регистрации"

По нашей методологии разработки начинаем написание кода с тестов.
сначала добавляем саму проверку статуса на активность в
./api/src/Auth/Test/Unit/Entity/User/User/JoinByEmail/RequestTest.php

```php
<?
class RequestTest extends TestCase
{
    public function testSuccess(): void
    {
        $user = new User();
        // дальше идут ассерты с проверкой что все геттеры отрабатывают
        self::assertEquals($id, $user->getId());
        // ..

        // сюда добавляем новую проверку статуса  еще до создания самих методов
        self::assertTrue($user->isWait());    // ждёт подтвеждение
        self::assertFalse($user->isActive()); // регистрация подтверждена
    }
}
```

написав тесты идём в User-а реализовывать код статуса и новых методов

Простейший пример - статус - строка:
```php
<?
class User
{
    public const STATUS_WAIT = 'wait';         // <<< константы для хранения
    public const STATUS_ACTIVE = 'active';     // <<< разных статусов

    private Id $id;
    private DateTimeImmutable $date;
    private Email $email;
    private string $passwordHash;
    private ?Token $joinConfirmToken;
    private string $status;                     // <<< поле хранящее статус

    public function __construct(
        Id $id,
        DateTimeImmutable $date,
        Email $email,
        string $passwordHash,
        Token $token
    ) {
        $this->id = $id;
        $this->date = $date;
        $this->email = $email;
        $this->passwordHash = $passwordHash;
        $this->joinConfirmToken = $token;
        $this->status = self::STATUS_WAIT;      //  создаётся не подтверждённым
    }

    # ...

    public function isWait(): bool
    {
        // сравниваем приватное динамическое поле с публичным статическим
        return $this->status === self::STATUS_WAIT;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }
```

В таком коде нлохо то, что если статусов будет много то код User-класса
получиться загромождйный.
Тогда можно вынести статусы в отдельный класс Status и брать его отуда

        $this->status = Status::WAIT;

        return $this->status === Status::WAIT; // isWait()

Можно ввести и свой отдельный тип для класса Status (Класс обьекта-значения)
и не использовать строки для хранения статуса в юзере:

    private Status $status;   // Поле в User теперь не страка а тип Status

    $this->status = new Status(Status::WAIT);   // в конструкторе User-a

Более удобный способ без прямого вызова конструктора - сделать фабричные методы:

    $this->status = Status::wait();   // фабрика - статический метод для new

    return $this->status->isWait();   // isWait() делегация проверки из User-a

Что этим сделали:
- создали свой новый тип Status (Обьект-значение)
- удобную фабрику для создания нужных статосов        `status = Status::wait();`
- проверку статуса делегировали в сам обьект status:  `$this->status->isWait();`
- в итоге получили `чистый код` в самом классе сущности User
  так как вся работа со статусом теперь идёт внутри самомого класса Status.
  То есть в User-е теперь нет ничего низкоуровневого и "постороннего" сам класс
  User просто хранит инстанс класса Status и делегирует запросы в него,
  вообще никак не работая с внутренними представлениями класса Status.

Это называется "Разделение по ответственности":
- код разделяется по ответственности
- сами проверки размещаются в самом классе обьекта-значения(Status), а не там
  где он используется(User)
- Запросы к сущности (User) c проверкой состояния хранимого в обьекте значении
  просто делегируются внутрь обьектов-значений `$this->status->isWait();`
  а не выполняются напрямую в самой сущности: `$this->status->value == Status::WAIT`)
- внутри хранителя обьекта можно оставить только проверку на корректность
  вводимых данных.

Кажущийся недостаток такого подхода -
"слишком много классов, а к ним еще тесты надо писать"

Но с другой стороны если вообще не использовать подобные классы для значений
то все проверки придётся помещать внутрь самой сущности, а не внутрь класса
обьект-значения. А это приведёт к тому что:

- проверки на корретность данных надо будет размещать в самой сущности
- в тестах самой сущности нужно бдует реализовывать еще и
  тесты для проверок на корректность этих данных.

А это приведёт к тому что код внутри классов сущностей будет стремительно расти
и превращаться в трудночитаемый и перегруженый всякого рода проверками.


Класс Обьекта-Значения для хранения Статуса сущности User:
```php
<?php

declare(strict_types=1);

namespace App\Auth\Entity\User;

class Status
{
    private const WAIT = 'wait';     // константы приватные т.к. при таком
    private const ACTIVE = 'active'; // подходе проверки только через методы

    private string $value;

    // конструктор приватный т.к. мы планируем использовать только через фабрики
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    // фабрики для создания нужного статуса
    public static function wait(): Status
    {
        return new Status(self::WAIT);
    }
    public static function active(): Status
    {
        return new Status(self::WAIT);
    }

    // методы для проверки состояния к ним будет делегировать User
    public function isWait(): bool
    {
        return $this->value === self::WAIT;
    }

    public function isActive(): bool
    {
        return $this->value === self::ACTIVE;
    }
    // т.к. у нас здесь есть методы isWait и isActive то и get-тер не обязателен
    // но он еще понадобиться для сохранения его значения в БД
}
```

Что и как сделали.
- начали писать код с теста `JoinByEmail\RequestTest`
- прописали в этом тесте проверку того, что юзер создаётся "ожидающим"
- под тесты дописали новый код в User добавив в него именно те методы, которые
  нужны для проверки работы логики внутри этого конктерного теста
- теперь имея возможность доступа к этим методом `isWait` `isActive` снаружи
  код использующий сущность User вообще не должен знать как оно там внутри
  устроено. И это даёт свободу в реализации класса User. т.к. внешний код
  использующий этот класс для того, чтобы проверить правильность поведения
  сущности вообще никак не зависит и не должен знать об внутренней её реализации

Главное для нас во время написания теста - чтобы наша сущность удовлетворяла
чётко поставленному условию, и чтобы можно было легко проверить это условие.


Какая здесь идея:
Тесты это своего рода воплощение в коде технического задания - спецификации.
То есть в тестах мы воплощаем проерку требуемой бизнес-логики, которая должна
выполнятся для нашего кода.

Например для теста `JoinByEmail\RequestTest` логика такова(тех задание):
- User должен создаваться в режиме "ожидание подтверждения регистрации",
а значит это нам и нужно проверить - вот и описываем в тесте это через методы.

В самом же тесте нас вообще не волнует, как конкретно там внутри это реализовано.
Мы просто знаем, что есть два метода isWait и isActive которые доступны
у обьекта класса User и через них можно проверить конкретно то, что нам нужно
проверить и всё. А дальше спускаемся на "слой ниже" в класс сущности User и
приступаем к реализации нужного поведения уже в нём.

Добавив сам статус к юзеру теперь можно приступить к реализации самого UseCase-а


### Делаем UseCase JoinByEmail\Confirm для подтверждения email-а

Контекст:
- письмо подтверждения уже отправлено
- для подтверждения нужно перейти по указанному в письме адресу(по ссылке)
- переход по ссылке должен передаться в некий контроллер где и
  произойдёт вызов кода потверждения регистрации.

```
./api/src/Auth/Command/JoinByEmail/
├── Confirm          << UseCase подтверждения регистрации
│   ├── Command.php  << В команду контроллером передастся токен подтверждения
│   └── Handler.php  << Далее это конманда передаётся обработчку(хэндлеру)
└── Request
    ├── Command.php
    └── Handler.php
```

- переход по ссылке стриггерит контролер.
- из перехода контроллер вытащит токен подтверждения
- затем поместит токен в команду `JoinByEmail\Confirm\Command`
- и передаст на обработку в обработчик(хэндлер) `JoinByEmail\Confirm\Hander`.

Обработчик принимающий команду(с токеном внутри) на подтверждение регистрации
```php
<?
namespace App\Auth\Command\JoinByEmail\Confirm;
# use ...
class Handler
{
    private UserRepository $users; // зависимсти которые разрешит
    private Flusher $flusher;      // di-контейнер

    public function __construct(UserRepository $users, Flusher $flusher)
    {
        $this->users = users; $this->flusher = flusher;
    }

    public function handle(Command $command): void
    {
        // ищем в бд User-а с таким же токеном по значению токена
        if (!$user = $this->users->findByConfirmToken($command->token)) {
            // если не находим кидаем исключение
            throw new \DomainException('Incorrect token');
        }
        // токен найден - триггерим подтверждение регистрации с проверкой даты
        $user->confirmJoin($command->token, new DateTimeImmutable());
        // сохраняем новое состояние юзера в бд
        $this->flusher->flush();
    }
}
```
здесь мы по нужной нам логике ввели новый метод `findByConfirmToken`
чтобы он выполнял искал и возвращал нам юзера с нужным нам значением токена.

У нас пока интерфейс репозитория поэтому добавляем просто сигнатуру метода:
```php
<?
interface UserRepository
{
    public function hasByEmail(Email $email): bool;
    public function findByConfirmToken(string $token): ?User;// Заготовка метода
    public function add(User $user): void;
}
```

### find и get - Отличие в именовании методов как правильно и что это значит?

метод `findByConfirmToken(): ?User` - должен возвращать либо `User` либо `null`
метод `getByConfirmToken(): User` - либо вернуть найденого User либо кинуть
исключение если ничего не найдено.

Поэтому если используем методы `find`, то надо после них делать проверки
найдено ли искомое и если не найдено - самому кидать исключение.

Тест обработчика подтверждения регистрации

### UserBuilder фабрика для тестов

Так как в тестах очень часто нужно будет создавать сущность User и заполнять
её нужными данными для упрощение написания тестов создадим для этого специальный
класс UserBuilder который по сути хелпер для тестов,
создающий готового для проверок User-а с нужными нам состояниями.

        ./api/src/Auth/Test/Builder/UserBuilder.php

Вот так вот мы создавали юзера до использования UserBuilder
```php
<?
class ConfirmTest extends TestCase
{
    public function testSuccess(): void
    {
        $user = new User(
            $id = Id::generate(),
            $date = new DateTimeImmutable(),
            $email = new Email('mail@example.com'),
            $hash = 'hash',
            $token = new Token(Uuid::uuid4()->toString(), $date)
        );
        # ...
    }
}
```
Создание пользователя через фабрику UserBuilder:
```php
<?
class ConfirmTest extends TestCase
{
    public function testSuccess(): void
    {
        $user = (new UserBuilder())->build();

        # ... проверка по токену...
    }
}
```

Дальше нужно как-то передавать токен внутрь UserBuilder-а чтобы новый User
создавался с заданным токеном. Для этого:
```php
<?php
namespace App\Auth\Test\Builder;
# use ...
class UserBuilder
{
    # ....
    // создаём статический метод переопределяющий токен который "пойдёт" в юзера
    public function withJoinConfirmToken(Token $token): UserBuilder
    {
        $clone = clone $this; // здесь идёт клонирование текущего инстанса
        $clone->joinConfirmToken = $token; // просто переопределяем на свой
        return $clone;
    }
    #...
```

Теперь для передачи токена в билдер используем наш новый метод
```php
<?
class ConfirmTest extends TestCase
{
    public function testSuccess(): void
    {
        $user = (new UserBuilder())
        // срау вводим для удобства фабрику создания токенов в этот же Класс
            ->withJoinConfirmToken($token = $this->createToken())
            ->build();

        // ... проверка по токену...
    }

    // метод помошник для удобного создания токенов в тестах
    public function createToken(): Token
    {
        return new Token(
            Uuid::uuid4()->toString(),
            new \DateTimeImmutable('+1 day')
        );
    }
}
```

Теперь можем делать все необходимые проверки метода `User.confirmJoin`:
- testSuccess - подтверждение реально меняет статус в User
- testWrong   - токены с невалидными значением не работают
- testExpired - истекшие токены не работают
- testAlready - активация проходит только один раз для wait-User-ов

```php
<?
class ConfirmTest extends TestCase
{
    public function testWrong(): void
    {
        $user = (new UserBuilder())
            ->withJoinConfirmToken($token = $this->createToken())
            ->build();

        $this->expectExceptionMessage('Token is invalid.');
        // здесь мы просто ожидаем что вылетит исключение с заданным сообщением
        // то есть делаем проверку по строке в сообщении кидаемого исключения

        $user->confirmJoin(
            Uuid::uuid4()->toString(),
            $token->getExpires()->modify('-1 day')
        );
    }
```

Написав сам тест `JoinByEmail\ConfirmTest` можем переходить к написанию самого
метода User.joinConfirm

У нас есть:
- хэндлер вызывающий `$user->confirmJoin()` из handle
- тест этого хэндлера проверяющией все возможные варинты подверждения регистрации
- теперь можно идти писать сам метод в класс User

```php
<?
namespace App\Auth\Entity\User;
class User
{
    // ...
    public function confirmJoin(string $token, DateTimeImmutable $date): void
    {
        // если токен пустой - значит у Юзера регистрация уже подтверждена
        if ($this->joinConfirmToken == null) {
            throw new DomainException('Confirmation is not required.');
        }
        // тут можно было бы описать все нужные проверки токена:
        // - равенство значений токена
        // - время истечения действия токена
        // но мы держим код чистым и переносим их все внутрь обькта Token
        $this->joinConfirmToken->validate($token, $date);
        $this->status = Status::active();
        $this->joinConfirmToken = null;
    }
```
Так приняв решение вынести всю валидацию токена внутрь самого класса Token:
Сначала пишем тесты для нового метода validate:

```php
<?
namespace App\Auth\Test\Unit\Entity\User;
// use ...
class TokenTest extends TestCase
{
    // ... старые проверки токена

    /**
     * @doesNotPerformAssertions
     */
    public function testValidateSuccess(): void
    {
        $token = new Token(
            $value = Uuid::uuid4()->toString(),
            $expires = new DateTimeImmutable()
        );
        // no exceptions in success
        $token->validate($value, $expires->modify('-10 secs'));
    }
    // остальные проверки валидации токена
```

[Тесты одного класса в нескольких файлах](./docs/12-auth/05-test-subdirs.md)

Написав все тесы под метод validate идём писать сам этот метод

Создаём метод валидации внутри самого класса Token
(в этот метод класс User будет делегировать проверку на валидность токена)
```php
<?
namespace App\Auth\Entity\User;
class Token
{
    public function validate(string $value, DateTimeImmutable $date): void
    {
        if (!$this->isEqualTo($value)) {
            throw new \DomainException('Token is invalid.');
        }
        if ($this->isExpiredTo($date)) {
            throw new \DomainException('Token is expired.');
        }
    }

    private function isEqualTo(string $value): bool
    {
        return $this->value === $value;
    }

    private function isExpiredTo(DateTimeImmutable $date): bool
    {
        return $this->expires <= $date;
    }
```

Опять таки плюс такого подхода:
- логика проверки внутри самого токена
- токен с истекающим сроком действия может использоваться в разных местах кода
  (например при смене пароля) И теперь не надо будет копипастить везде коды
  проверки на его валидацию


```php
<?php
namespace App\Auth\Test\Builder;
# use ...
class UserBuilder
{
    # ....
    // метод для cоздавания активированных User-ов
    public function active(): UserBuilder
    {
        $clone = clone $this;
        $clone->active = true; // на основе этого значения будем активировать
        return $clone;
    }
    #...
```

Почти полностью сделали черновик регистрации по емейлу
осталось только добавить бд и мейлер для отправки писем
всё остальное все наши сущности уже работают как надо.

Единственно что мы никак не проверяем сами Хэндлеры:
рассчитывая только на статический анализатор кода(psalm)
```
./api/src/Auth/Command/JoinByEmail/
├── Confirm
│   ├── Command.php
│   └── Handler.php     < хэндлер подтверждения
└── Request
    ├── Command.php
    └── Handler.php     < хэндлер регистрации
```


```
./api/src/Auth/Test/Unit/
├── Entity
│   └── User                         -- часть namespace
│       ├── EmailTest.php
│       ├── IdTest.php
│       ├── TokenTest.php            -- можно по аналогии с User вынести в каталог
│       └── User                     -- тут по логике должен быть UserTest.php
│           └── JoinByEmail             но так как проверок для сущности User
│               ├── ConfirmTest.php     будет достаточно много все они выносятся
│               └── RequestTest.php     в разные файлы.
└── Service
    ├── PasswordHasherTest.php
    └── TokenizerTest.php
```

Тут для `Confirm` `Request` из `App\Auth\Test\Unit\Entity\User\JoinByEmail\`
важно осознать разницу в этих методах не идёт прямое тестирование самих хэндлеров
UseCase-ов:

- `./api/src/Auth/Command/JoinByEmail/Resuest/Handler.php`
- `./api/src/Auth/Command/JoinByEmail/Confirm/Handler.php`

Но при этом эмулируется основная логика их работы.


