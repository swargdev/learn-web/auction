<?php

declare(strict_types=1);

namespace App\Auth\Entity\User;

use Webmozart\Assert\Assert;

/**
 *
 * 13-10-2023
 * @author Swarg
 */
class Role
{
    public const USER = 'user';
    public const ADMIN = 'admin';

    private string $name;

    public function __construct(string $name)
    {
        Assert::oneOf($name, [
            self::USER,
            self::ADMIN,
        ]);

        $this->name = $name;
    }

    public static function user(): Role
    {
        return new Role(Role::USER);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
