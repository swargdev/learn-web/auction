## Создаём новый проект через Create-React-App

В доке дают команду для установки через npx
```sh
npx create-react-app my-app
```
стандартно обычно везде используется пакетный менеджер `npm`
`npx` - это спец скрипт не только устанавливающий пакет, но еще и запускающий
его на исполнение. т.е. это своего рода executer(исполнитель) переданных в него
пакетов.

Вообще можно генерить React-проект и через другие пакетные менеджеры `npm` `yarn`


Запускаем установку реакт-проекта
Вообще по логике нам нужно использовать вместо каталога `react-app` `./`
(текущий внутри контейнера), но у нас там уже есть файлы и на них будет ругаться
установщик поэтому и прописываем пока дополнительный каталог `react-app`:

```sh
docker compose run --rm frontend-node-cli npx create-react-app react-app
```



```
Need to install the following packages:
create-react-app@5.0.1
Ok to proceed? (y) y
npm WARN deprecated tar@2.2.2: This version of tar is no longer supported,
and will not receive security updates. Please upgrade asap.

Creating a new React app in /app/react-app.

Installing packages. This might take a couple of minutes.
Installing react, react-dom, and react-scripts with cra-template...
           ^^^^^  ^^^^^^^^^      ^^^^^^^^^^^^^      ^^^^^^^^^^^^
            (1)       (2)             (3)                (4)

added 1486 packages in 2m
Success! Created react-app at /app/react-app
Inside that directory, you can run several commands:

  npm start
    Starts the development server.

  npm run build
    Bundles the app into static files for production.

  npm test
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

We suggest that you begin by typing:

  cd react-app
  npm start

Happy hacking!
npm notice
npm notice New patch version of npm available! 10.2.3 -> 10.2.5
npm notice Changelog: https://github.com/npm/cli/releases/tag/v10.2.5
npm notice Run npm install -g npm@10.2.5 to update!
npm notice
```

Проект создан, установлены три библ-ки 1-3 и
(4) - шаблон на основе которого и был создан проект

```
Installing react, react-dom, and react-scripts with cra-template...
           ^^^^^  ^^^^^^^^^      ^^^^^^^^^^^^^      ^^^^^^^^^^^^
            (1)       (2)             (3)                (4)
```

Эти 3 библиотеки имеют зависимости, которые так же были установлены:

```
Installing template dependencies using npm...

added 69 packages, and changed 1 package in 21s
```

Так же были подключены пакеты нужные для тестов(см packages.json)
- @testing-library/jest-dom
- @testing-library/react
- @testing-library/user-event

Дальше он удалил cra-template на основе которого и создал проект:
`Removing template package using npm...`

В итоге эта команда создала нам полноценный React-проект создала такую
структуру каталогов:

frontend/react-app/
├── node_modules
│   ... 370Mb 860 каталогов, 39427 файлов
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
├── README.md
├── package.json
├── package-lock.json
└── src
    ├── App.css
    ├── App.js
    ├── App.test.js
    ├── index.css
    ├── index.js
    ├── logo.svg
    ├── reportWebVitals.js
    └── setupTests.js


Количество файлов узнавал вот так:
```sh
find frontend/react-app/node_modules/ -type f | wc -l
```

Так же сразу после установки проекта выводит список доступных команд
```sh
Inside that directory, you can run several commands:

  npm start
    Starts the development server.

  npm run build
    Bundles the app into static files for production.

  npm test
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

We suggest that you begin by typing:

  cd react-app
  npm start
```

- `npm start` для запуска dev-сервера
- `npm run build` - собрать проект для прода, при этом создаться каталог build
  в который поместиться css html js файлы готовые для деплоя на прод
- `npm test` запуск тестов
- `npm run eject` - для ручной конфигурации проекта
   извелкает скрытые файлы на видное место для возможности их настройки
- `npm start` - запустить dev-js сервер выводящий нашу страницу по адресу
   localhost:3000. Но мы запускаем nodejs через докер, поэтому чтобы это
   работало нужно еще порт прокинуть


## Изучим созданные файлы в frontend/react-app

- frontend/react-app/package.json - аналог composer.json здесь описаны
зависимости(dependencies) проекта.


```json
{
  "name": "react-app",
  "version": "0.1.0",
  "private": true,
  "dependencies": {
    "@testing-library/jest-dom": "^5.17.0",
    "@testing-library/react": "^13.4.0",
    "@testing-library/user-event": "^13.5.0",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-scripts": "5.0.1",
    "web-vitals": "^2.1.4"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  },
  "eslintConfig": {
    "extends": [
      "react-app",
      "react-app/jest"
    ]
  },
  "browserslist": {
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ],
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ]
  }
}
```

оригинальные зависимости
```
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-scripts": "5.0.1",
    "web-vitals": "^2.1.4"
```

тестовые зависимости

```
    "@testing-library/jest-dom": "^5.17.0",
    "@testing-library/react": "^13.4.0",
    "@testing-library/user-event": "^13.5.0",
```

те самые скрипты которые можно запускать через nodejs
```js
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  },
```

сгенерированный шаблоном каталог
./frontend/react-app/public/
- index.html демо-страница React-приложения

если посмотреть на  index.html визуально это просто заглушка
без какого-либо особого html-кода отображаюшего какие-либо страницы:


```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="icon" href="%PUBLIC_URL%/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta
      name="description"
      content="Web site created using create-react-app"
    />
    <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />
    <!--
      manifest.json provides metadata used when your web app is installed on a
      user's mobile device or desktop.
      See https://developers.google.com/web/fundamentals/web-app-manifest/
    -->
    <link rel="manifest" href="%PUBLIC_URL%/manifest.json" />
    <!--
      Notice the use of %PUBLIC_URL% in the tags above.
      It will be replaced with the URL of the `public` folder during the build.
      Only files inside the `public` folder can be referenced from the HTML.

      Unlike "/favicon.ico" or "favicon.ico", "%PUBLIC_URL%/favicon.ico" will
      work correctly both with client-side routing and a non-root public URL.
      Learn how to configure a non-root public URL by running `npm run build`.
    -->
    <title>React App</title>
  </head>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"></div>
    <!--
      This HTML file is a template.
      If you open it directly in the browser, you will see an empty page.

      You can add webfonts, meta tags, or analytics to this file.
      The build step will place the bundled scripts into the <body> tag.

      To begin the development, run `npm start` or `yarn start`.
      To create a production bundle, use `npm run build` or `yarn build`.
    -->
  </body>
</html>
```
Так же добавлены favicon.ico logo robots.txt И проч


Сам код на js содержится в каталоге src:

frontend/react-app/src
  ├── App.css
  ├── App.js
  ├── App.test.js
  ├── index.css
  ├── index.js
  ├── logo.svg
  ├── reportWebVitals.js
  └── setupTests.js


- `setupTests.js` - описывает как производить запуск тестов, и какие для этого
библиотеки и плагины использовать

В общем теперь наша задача переместить сгенерированный проект на уровень выше
внутрь калалога frontend из frontend/react-app
