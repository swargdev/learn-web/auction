## Добавление проверки code-style (соблюдения стиля кода)

Ныне повсеместно используется единый стандарт стиль написания кода.
Раньше многие программисты писали как хотели в разных стилях расставления
скобок после классов, функций, методов, пробелы в скобках и т.д.
Такой код трудно читать и поддерживать из-за того что внимание постоянно
отвлекается на прыгающее форматирование кода. Особенно если над проектом
работало несколько человек с разным стилем написания кода.


### PHP Standards Recommendations

Настал момент когда группа [PHP-FIG](https://php-fig.org),
состаящая из разработчиков разных php-фреймворков придумала рекомендации по
написанию php кода.

Одна из таких рекомендаций как раз по code-style.

Раньше это был PSR-2 (ныне устаревший)

https://php-fig.org/psr
```
DEPRECATED
NUM   TITLE
0     Autoloading Standard
2     Coding Style Guide     << устаревший набор рекомендаций по написанию кода
```

[PSR-2: Coding Style Guide](https://www.php-fig.org/psr/psr-2)
Deprecated - As of 2019-08-10 PSR-2 has been marked as deprecated.
PSR-12 is now recommended as an alternative.

PSR-2 устарел из-за того что вышли новые версии php 7 8 с новыми вещами которых
небыло в psr-2



[PSR-12: Extended Coding Style](https://www.php-fig.org/psr/psr-12/)
This specification extends, expands and replaces PSR-2,
the coding style guide and requires adherence to PSR-1, the basic coding standard.

Это расширенный список рекомендаций того как оформлять свой код.

В умных IDE PhpStorm NetBeans есть автоматическое слежение за соблюдением psr-12
c исправлением и авто форматированием кода под общепризнанный стандарт psr-12

PhpStorm: Code -> Reformat Code (Ctrl+Alt+L)  -- автоподгонка стиля под psr-12

Но так как не все программисты в команде могут использовать IDE и авто
форматирование кода под psr-12 то нужен иснтрумент для консольной проверки стиля


## PHP_CodeSniffer Библиотека для автоформатирования и проверки стиля кода
https://github.com/squizlabs/PHP_CodeSniffer

Это библиотека содержит в себе большой список стандартов
https://github.com/squizlabs/PHP_CodeSniffer/tree/master/src/Standards

Generic, MySource, PEAR, PSR1, PSR2, PSR12, Squiz, Zend

Устанавливается так же через composer requires --dev


Для установки для всей системы можно использовать такую команду
```sh
composer global require "squizlabs/php_codesniffer=*"
```
Для этой команды нужно чтобы composer был в PATH-е, проверить путь по умолчанию
вот так:
```sh
composer global config bin-dir --absolute
```

В нашем проекте установим как и другие пакеты локально в vendor
```json
{
    "require-dev": {
        "squizlabs/php_codesniffer": "3.*"
    }
}
```
запускать можно будет вот так:
```sh
./vendor/bin/phpcs -h    # code sniffer -- проверяет код по заданным настройкам
./vendor/bin/phpcbf -h   # автоматическое исправление стиля кода в исходнике
```

```sh
docker compose run --rm api-php-cli composer require --dev squizlabs/php_codesniffer
```
```sh
du -hcd 1 api/vendor/
```
Размер каталога vendor после установки:
```
580K  api/vendor/slim
40K   api/vendor/fig
528K	api/vendor/php-di
852K	api/vendor/overtrue
20K   api/vendor/bin
236K  api/vendor/nikic
156K  api/vendor/laravel
28K   api/vendor/ralouphie
380K  api/vendor/psr
224K  api/vendor/composer
3.3M  api/vendor/symfony
9.4M  api/vendor/squizlabs
16M	  api/vendor/
16M	total
```

Пропишем команды для запуска проверки и справления cs-check cs-fix
composer.json
```json
{
    "require-dev": {
        "overtrue/phplint": "^9.0",
        "squizlabs/php_codesniffer": "^3.7",              // <<< new
        "roave/security-advisories": "dev-master"
    },
    "scripts": {
        "lint": "phplint",
        "cs-check": "phpcs",                              // <<<
        "cs-fix": "phpcbf",                               // <<<
        "app": "php bin/app.php --ansi"
    }
}
```

Проверку стиля кода добавим в `make api-lint` к lint-у
```Makefile
api-lint:
	docker compose run --rm api-php-cli composer lint
	docker compose run --rm api-php-cli composer cs-check
```

Сгенерировать заготовку под конфиг можно вот так
```sh
docker compose run --rm api-php-cli vendor/bin/phpcs --config-set default_standard PSR12
```
```
Using config file: /app/vendor/squizlabs/php_codesniffer/CodeSniffer.conf

Config value "default_standard" added successfully
```

./api/phpcs.xml
```html
<?xml version="1.0" encoding="utf-8"?>
<ruleset name="App coding standard">
    <arg value="p"/>
    <arg value="color"/>
    <arg value="cache" value="var/.phpcs.json"/>

    <rule ref="PSR12"/>
    <rule ref="Generic.Arrays.DisallowLongArraySyntax"/>
    <rule ref="Squiz.WhiteSpace.SuperfluousWhitespace">
        <properties>
            <property name="ignoreBlankLines" value="false"/>
        </properties>
    </rule>

    <!-- каталоги которыу нужно проверять -->
    <file>bin</file>
    <file>config</file>
    <file>public</file>
    <file>src</file>
</ruleset>
```
- Generic.Arrays.DisallowLongArraySyntax - запрещать старый синстаксис массивов
  array(...)
- Squiz.WhiteSpace.SuperfluousWhitespace - переопределяем встроенное правило на
  длинну строк чтобы линтер не ругался на слишком длинные(больше 120) сктоки

Теперь вторая часть make lint выдаст такой вывод, если нет никаких ошибок в стиле
```sh
docker compose run --rm api-php-cli composer cs-check
> phpcs
............ 12 / 12 (100%)

Time: 55ms; Memory: 6MB
```

Автоматически исправить найденые недочёты в стиле кода:
```sh
docker compose run --rm api-php-cli composer cs-fix
```
Note: Не все ошибки могут быть исправлены автоматически

Теперь можно настроить hook который например перед коммитом в репозиторий
будет автоматически запускать `make lint` и если возникнут ошибки в коде или
стиле - не давать комитить до исправления. Или например при слиянии ветвей
запускать проверку и только когда всё в порядке разрешать слияние.


Итак теперь у нас есть два типа проверки кода
- lint - проверка корректности кода(на наличие ошибок в ЯП)
- cs-check - проверка стиля кода (только форматирование по рекомандациям PSR-12)


