## yarn warning has incorrect peer dependency - несовместимость версии

здесь заметка о том, как пытался исправить конфликт версий пакетов-библиотек
при установке prettier поверх установленного stylelint

как оказалось проблема в том, что для новой 16ой версий stylelint нет
пакета stylelint-config-prettier умеющего с ней работать
, при этом если ставить stylelint 14ой версии то начинает ругаться уже
установленные пакеты:
- stylelint-config-standard@36.0.0"
- stylelint-config-recommended@14.0.0"
- stylelint-prettier@5.0.0"
тип им нужна версия >=16

поэтому пока просто оставил ворнинг для stylelint-config-prettier@9.0.5
всё равно рано или поздно её обновят и до 16. и пока ошибкок работы с
stylelint+prettier не обнаружено



После установки prettier и его плагинов `yarn` выводит такое сообщение:
```
warning " > stylelint-config-prettier@9.0.5" has incorrect peer dependency "stylelint@>= 11.x < 15".
```
Это означает что пакет stylelint-config-standard@9.0.5 умеет работать только
с stylelint от 11 до 15 версии а у нас стоит 16( package.json)

```json
    "stylelint": "^16.1.0",
```

изменяем руками версию stylelint в package.json на старую
```json
    "stylelint": "^14.0.0",
```

убеждаемся перед обновлением что версия была 16
```sh
head -n 3 frontend/node_modules/stylelint/package.json
{
  "name": "stylelint",
  "version": "16.1.0",
```

https://classic.yarnpkg.com/en/docs/cli/install
после редактирования package.json и запускаем `yarn install`
```sh
docker compose run --rm frontend-node-cli yarn install
```
это подхавтит обновления из package.json и переустановит пакеты с указанными
версиями

можно обновить один конкретный пакет до нужной версии и через `yarn upgrade`
при этом можно "обновлять" версию и как вверх так и "вниз"
```sh
docker compose run --rm frontend-node-cli yarn upgrade stylelint@^14
```

После обновления убеждаемся что версия откатилась до нужной
```sh
head frontend/node_modules/stylelint/package.json
{
  "name": "stylelint",
  "version": "14.16.1",
```

Проверяем будут ли еще предупреждения от yarn
```sh
docker compose run --rm frontend-node-cli yarn install --check-files
```
```
warning " > stylelint-config-standard@36.0.0"
has incorrect peer dependency "stylelint@^16.1.0".

warning "stylelint-config-standard > stylelint-config-recommended@14.0.0"
has incorrect peer dependency "stylelint@^16.0.0".

warning " > stylelint-prettier@5.0.0"
has incorrect peer dependency "stylelint@>=16.0.0".
```
