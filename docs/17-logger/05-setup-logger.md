
- Подключение логера к проекту, переопеределение Slim\ErrorHandler
- Создаём свой LogErrorHandler вместо стандартного Slim\Handlers\ErrorHandler
- Проверяем запись лог-сообщений в /var/log/cli/test.log
- Логирование готово. Что это даёт
- Запись дополнительных параметров(context) в лог


## Подключение логера к проекту, переопеределение Slim\ErrorHandler

Всё готово для того чтобы переопределить дефолтный Slim\Handlers\ErrorHandler

Так как настройки по параметрам `config.debug` `config.env` убраны из фабрики
`api/test/middleware.php`:

    $app->addErrorMiddleware($config['debug'], $config['env'] != 'test', true);

и настройка происходит в фабрике логера api/common/logger.php.
параметры `config.debug` и `config.env` опеределяются в файле
./api/config/common/system.php:

```php
<?php

declare(strict_types=1);

return [
    'config' => [
        'env' => (bool) getenv('APP_ENV') ?: 'prod',
        'debug' => (bool) getenv('APP_DEBUG'),
    ],
];
```

Но теперь они вообще нигде не используются.
Теперь каждый пакет использует не общий `config.debug` а свой независимый от
других, свой собственный `config.*.debug`

Вот примеры:
twig.php:
```
    'config' => [
        'twig' => [
            'debug' => (bool)getenv('APP_DEBUG'),
```

logger.php:
```
    'config' => [
        'logger' => [
            'debug' => (bool)getenv('APP_DEBUG'),
```

Сделали это для того чтобы конфиги были самодостаточными и
не зависили друг от друга.

Поэтому теперь удаляем файл `api/common/system.php` - он больше ну нужен.



## Создаём свой LogErrorHandler вместо стандартного Slim\Handlers\ErrorHandler

./api/config/common/errors.php
```php
<?
    $middleware = new ErrorMiddleware(
        $callableResolver,
        $responseFactory,
        $config['display_details'],
        $config['log'],              // задаёт значение logErrors
        true
    );

    // первый шаг: просто прописываем стандартный хэндлер
    // так же как он и должен создаваться в стандартном коде
    $middleware->setDefaultErrorHandler(
        new ErrorHandler($callableResolver, $responseFactory)
    );
```

Теперь понятно где конкретно нужно переопределять стандартный ErrorHandler на
свой собственный класс(наследующийся от стандартного)

Создаём свой класс наследующийся от `Slim\Handlers\ErrorHandler`:
./api/src/ErrorHandler/LogErrorHandler.php
```php
<?php

declare(strict_types=1);

namespace App\ErrorHandler;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Log\LoggerInterface;
use Slim\Handlers\ErrorHandler;
use Slim\Interfaces\CallableResolverInterface;

/**
 * подавляем жалобы psalm-а на то что у родительского класса есть поля
 * требующие обязательного заполнения  и не заполненные в конструкторе:

 * @psalm-suppress PropertyNotSetInConstructor
 */
class LogErrorHandler extends ErrorHandler
{
    protected function writeToErrorLog(): void
    {
        //здесь в передаём текст сообщения из исключения
        $this->logger->error($this->exception->getMessage(), [
            // дополнительная полезная информация - само исключение и урл
            'exception' => $this->exception,           // стектрейс
            'url' => (string)$this->request->getUri(), // на какой странице(урл)
        ]);
    }
}
```
переопеределяем только один метод `writeToErrorLog`, даже конструктор оставляем
таким же как у родительского класса т.к. он в новой версии Slim может принимать
LoggerInterface.

Как помним вначале метода LoggerInterfac.error идёт просто строка.
затем можно передавать массив доп параметров которые должны быть сохранены в лог.

NOTE: Без аннотации подавления ошибки псалм выдаст такое:
```
ERROR: PropertyNotSetInConstructor -
src/ErrorHandler/LogErrorHandler.php:15:7 -
Property App\ErrorHandler\LogErrorHandler::$logErrors
is not defined in constructor of
App\ErrorHandler\LogErrorHandler or in any methods called in the constructor
(see https://psalm.dev/074)
```

Конечный вариант фабрики ErrorMiddleware с переопреределённым ErrorHandler-ом
```php
<?
use App\ErrorHandler\LogErrorHandler; // наш класс наследующийся от ErrorHandler
# ...

return [
    ErrorMiddleware::class =>
    static function (ContainerInterface $container): ErrorMiddleware {
        # ... 2x Interfaces

        $config = $container->get('config')['errors'];

        $middleware = new ErrorMiddleware(
            $callableResolver,
            $responseFactory,
            $config['display_details'],
            true,                   // убираем $config['log'], logErrors = true
            true
        );

        /** @var LoggerInterface $logger*/
        $logger = $container->get(LoggerInterface::class);

        $middleware->setDefaultErrorHandler(
            // свой класс вместо стандартного Slim\Handlers\ErrorHandler
            new LogErrorHandler($callableResolver, $responseFactory, $logger)
        );

        return $middleware;
    },
    'config' => [
        'errors' => [
            'display_details' => (bool)getenv('APP_DEBUG'),
            // удаляем 'log' т.к. теперь logErrors всегда true
            // а выводить ли вывод в StdErr задаётся через config.logger.stderr
        ],
    ],
]
```

По поводу удаления настройки `conf.errors.log` она у нас
передавалась из ErrorMiddleware в `ErrorHandler.logErrors`

./api/vendor/slim/slim/Slim/Handlers/ErrorHandler.php

```php
<?
class ErrorHandler implements ErrorHandlerInterface {
    # ...
    public function __invoke(...) {
        # ...
        if ($logErrors) { // т.е. если передать сюда false то          <<<<<<<<
            // для тест-окружения логи писаться не будут, а нам нужно писать
            $this->writeToErrorLog();
        }
    }
```
На данном этапе  т.к. logErrors всегда нужно true то удалив config.error.log
становится не нужным и переопеределение для test-окружения, поэтому удаляем
и сам файл `api/test/errors.php`. Оставляя только `api/common/errors.php`


## Проверяем запись лог-сообщений в /var/log/cli/test.log

```sh
make test
```

```sh
tree -a api/var/log
api/var/log
├── cli
│   └── test.log
└── .gitignore
```

Лог записался, смотрим содержимое
```sh
cat api/var/log/cli/test.log
[2023-12-08T16:19:37.126388+00:00] API.ERROR: Method not allowed. Must be one of: GET {"exception":"[object] (Slim\\Exception\\HttpMethodNotAllowedException(code: 405): Method not allowed. Must be one of: GET at /app/vendor/slim/slim/Slim/Middleware/RoutingMiddleware.php:79)","url":"/"} []
[2023-12-08T16:19:37.152439+00:00] API.ERROR: Not found. {"exception":"[object] (Slim\\Exception\\HttpNotFoundException(code: 404): Not found. at /app/vendor/slim/slim/Slim/Middleware/RoutingMiddleware.php:76)","url":"/not-found"} []
```

Сюда у нас выводятся исключения тех самых функциональных логов,
которые мы исследовали в самом началае этого раздела. То есть теперь вместо
того чтобы писать полные трейсы прямо на экран в момент выполнения тестов все
исключения ловятся логгером и сохраняются в заданный нами файл в `var/log/*/*.log`


## Логирование готово. Что это даёт

- для прода оно стандартно(для докера) пишет в потоки stderr stdout
  этот вывод докер сам сохраняет в каталог с инофой о контейнере
 `/var/lib/docker/containers/CID/CID-json.log`
- для дев и тест окружения логи пишутся и еще и в файлы в ./var/log/

Такое поведение логгирования будет полезно при настройке CI-Pipeline-а
например в Jenkins-подобных системах. Суть в том что в CI-пайплайне тесты
будут запускаться автоматически и нужно обеспечить сохранение результатов тестов
чтобы всегда можно было посмотреть что пошло не так и где именно.

Другими словами теперь проблем с поиском ошибок в функциональных тестах не будет.
(функ-х тестов контроллеров или других подобных компонентов)
теперь всегда можно зайти в лог файл и глянуть что и где поломалось


## Заметки об ошиках

Ошибка при попытке из api-php-fpm записать сообщение в лог файл при отсутствии
прав к каталогу `var/log/fpm-fcgi`.
Решается ручным созданием каталога и прописыванием прав доступа
```sh
sudo chmod 777 api/var/log/fpm-fcgi/
```

http://localhost:8081/123
```
Fatal error: Uncaught UnexpectedValueException:
There is no existing directory at "/app/config/dev/../../var/log/fpm-fcgi"
and it could not be created:
Permission denied in
/app/vendor/monolog/monolog/src/Monolog/Handler/StreamHandler.php:200
```

После исправления пробуем еще раз запросить не существующий на сайте адрес
http://localhost:8081/123
```
sudo cat api/var/log/fpm-fcgi/app.log
[2023-12-08T18:32:50.962707+00:00] API.ERROR: Not found.
{"exception":
  "[object] (Slim\\Exception\\HttpNotFoundException(code: 404): Not found. at
            /app/vendor/slim/slim/Slim/Middleware/RoutingMiddleware.php:76)",
"url":"http://localhost/123"  <<<
} []
```
как видим в лог пишется еще и url - сам адрес который вызвал исключение


## Запись дополнительных параметров(context) в лог

Любые данные которые мы передаём в лог вторым параметром будут сохранены в логе.
в оригинальном интерфейсе LoggerInterface второй арг называется $contenxt:

```
public function error(string|\Stringable $message, array $context = []): void;
                                                          ^^^^^^^
```

```php
<?
class LogErrorHandler extends ErrorHandler
{
    protected function writeToErrorLog(): void
    {
        $message = $this->exception->getMessage();
        $context = [ // доп параметры для записи в лог
            'exception' => $this->exception,
            'url' => (string)$this->request->getUri(),
        ];
        $this->logger->error($message, $context);
    }
}
```


Такое поведение - запись доп параметров в лог может быть очень полезным,
когда надо фильтровать логи и искать нужную информацию по каким-то критериям.
На данный момент мы передаём только исключение и url. В будущем же можем
прописать еще один доп параметр из http-requet-a(заголовка запроса) называемый
`X-REQUEST-ID` или `X-Correlation-ID`.
Это отдельная тема. суть в том чтобы при наличии нескольких сервисов и очереди
сообщений иметь возможность как-то прослеживать прохождение процесса во времени.
В нашей текущей системе мы можем просто вызвать docker compose logs и глянуть
логи того что происходило, какие сообщения проходили. При использовании очереди
сообщений уже не получится так просто взять и посмотреть как один процесс
проходил через всю систему. Потому что запрос(request) может сработать в одно
время(утром) задание в очереди привязанное к этом запросу сработало позже
скажем через час, и некий нужный емейл отправился из очереди еще через 5 минут.
А если в логе сообщений много всё это перемешивается и как выловить лог-сообщения
именно одного конкретного процесса? Вот для этого и спользуются такая вещь как
X-REQUEST-ID X-Correlation-ID. В Nginx для каждого http-запроса можно
генерировать свой уникальный REQUEST-ID элемент и передавать его в виде
http-заголовка. а дальше уже в том же writeToErrorLog из `$this->request`
можно будет доставать этот уникальный заголовок, назваемый X-REQUEST-ID и
передавать его для записи в лог под соответствующим именем. Тогда даже если будет
несколько логов для разных контейнеров, просто берём сообщение о прилетевшей
ошибке и этот уникальный X-REQUEST-ID-идентификатор и уже по нему (grep)
искать все связанные с ним запросы, чтобы проследить как проходил этот запрос
через все очереди, внутренние вызове сервисов и т.д
Более подробно об этом будет при подлючении к проекту очередей.


