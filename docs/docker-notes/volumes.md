## Volumes

запись в docker-compose.yml

```
volumes:
   api-postgres:
```
означает, что создаётся вольюм с настройками по умолчанию.
В случае docker-compose такие волюмы хранятся в каталоге
/var/lib/docker/volumes/site_api-postgres

убедиться и проверить это можно так:

```sh
docker ps
```
```
CONTAINER ID   IMAGE                 ...    NAMES
7e9a5ffd2b39   postgres:13.12-alpine ...    site-api-postgres-1
```

```sh
docker inspect 7e9a5ffd2b39 | grep volume
```
```
"Source": "/var/lib/docker/volumes/site_api-postgres/_data",
```

размер волюма
```sh
sudo du -hcd 1 /var/lib/docker/volumes/site_api-postgres/
48M	/var/lib/docker/volumes/site_api-postgres/_data
48M	/var/lib/docker/volumes/site_api-postgres/
48M	total
```
