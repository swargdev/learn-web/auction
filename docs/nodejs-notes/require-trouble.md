
```sh
docker compose run --rm cucumber-node-cli yarn e2e
yarn run v1.22.19
$ cucumber-js
Error [ERR_REQUIRE_ESM]: require() of ES Module /app/node_modules/wrap-ansi/index.js from /app/node_modules/cliui/build/index.cjs not supported.
Instead change the require of index.js in /app/node_modules/cliui/build/index.cjs to a dynamic import() which is available in all CommonJS modules.
    at Object.<anonymous> (/app/node_modules/cliui/build/index.cjs:293:14)
    at Object.<anonymous> (/app/node_modules/yargs/build/index.cjs:1:60678)
    at Object.<anonymous> (/app/node_modules/yargs/helpers/index.js:6:5)
    at Object.<anonymous> (/app/node_modules/@puppeteer/browsers/lib/cjs/CLI.js:48:19)
    at Object.<anonymous> (/app/node_modules/@puppeteer/browsers/lib/cjs/main.js:40:16)
    at Object.<anonymous> (/app/node_modules/puppeteer-core/lib/cjs/puppeteer/node/ChromeLauncher.js:25:20)
    at Object.<anonymous> (/app/node_modules/puppeteer-core/lib/cjs/puppeteer/node/node.js:32:14)
    at Object.<anonymous> (/app/node_modules/puppeteer-core/lib/cjs/puppeteer/puppeteer-core.js:36:14)
    at Object.<anonymous> (/app/node_modules/puppeteer/lib/cjs/puppeteer/puppeteer.js:33:14)
    at Object.<anonymous> (/app/features/support/hooks.js:2:19)
    at async getSupportCodeLibrary (/app/node_modules/@cucumber/cucumber/lib/api/support.js:19:9)
    at async runCucumber (/app/node_modules/@cucumber/cucumber/lib/api/run_cucumber.js:41:11)
    at async Cli.run (/app/node_modules/@cucumber/cucumber/lib/cli/index.js:56:29)
    at async Object.run [as default] (/app/node_modules/@cucumber/cucumber/lib/cli/run.js:29:18) {
  code: 'ERR_REQUIRE_ESM'
}
```



I have the same issue and this helped me to solve this problem

Add this to your package.json file

```json
"resolutions": {
  "wrap-ansi": "7.0.0",
  "string-width": "4.1.0"
}
```


```sh
docker compose run --rm cucumber-node-cli yarn install
yarn install v1.22.19
[1/4] Resolving packages...
warning Resolution field "string-width@4.1.0" is incompatible with requested version "string-width@^4.2.0"
warning Resolution field "string-width@4.1.0" is incompatible with requested version "string-width@^5.1.2"
warning Resolution field "wrap-ansi@7.0.0" is incompatible with requested version "wrap-ansi@^8.1.0"
warning Resolution field "string-width@4.1.0" is incompatible with requested version "string-width@^4.2.3"
warning Resolution field "string-width@4.1.0" is incompatible with requested version "string-width@^4.2.0"
[2/4] Fetching packages...
[3/4] Linking dependencies...
[4/4] Building fresh packages...
success Saved lockfile.
```

24-12-23

Update: Time later it started to bother me again this issue, so according some
blogs found in internet, I decided to discard using yarn and I switched back to
npm and it's working now because it seems this issue is related to a bug in
yarn. In some places recommend to upgrade yarn 1 to 3 but the upgrading process
come up with issues, so I prefer to keep with npm.
