## slim/slim-skeleton
```sh
docker compose run --rm api-php-cli \
  composer create-project slim/slim-skeleton slim
```

```sh
du -hdc 1 api/slim

24K 	api/slim/app
128K 	api/slim/src
64K   api/slim/tests
12K 	api/slim/public
12K 	api/slim/var
8.0K	api/slim/logs
16K 	api/slim/.github
28K 	api/slim/vendor/bin
28K 	api/slim/vendor/ralouphie
40K 	api/slim/vendor/fig
60K 	api/slim/vendor/symfony
76K 	api/slim/vendor/theseer
100K	api/slim/vendor/jangregor
152K	api/slim/vendor/doctrine
156K	api/slim/vendor/laravel
200K	api/slim/vendor/myclabs
224K	api/slim/vendor/webmozart
296K	api/slim/vendor/psr
372K	api/slim/vendor/phar-io
400K	api/slim/vendor/composer
580K	api/slim/vendor/phpspec
580K	api/slim/vendor/slim
600K	api/slim/vendor/php-di
624K	api/slim/vendor/phpdocumentor
760K	api/slim/vendor/monolog
1.2M	api/slim/vendor/sebastian
2.1M	api/slim/vendor/nikic
4.2M	api/slim/vendor/phpunit
9.4M	api/slim/vendor/squizlabs
22M 	api/slim/vendor/phpstan
44M 	api/slim/vendor/

44M 	api/slim/
```

```sh
tree api/slim/src/
```
```
api/slim/public/
└── index.php

api/slim/src/
├── Application
│   ├── Actions
│   │   ├── ActionError.php
│   │   ├── ActionPayload.php
│   │   ├── Action.php
│   │   └── User
│   │       ├── ListUsersAction.php
│   │       ├── UserAction.php
│   │       └── ViewUserAction.php
│   ├── Handlers
│   │   ├── HttpErrorHandler.php
│   │   └── ShutdownHandler.php
│   ├── Middleware
│   │   └── SessionMiddleware.php
│   ├── ResponseEmitter
│   │   └── ResponseEmitter.php
│   └── Settings
│       ├── SettingsInterface.php
│       └── Settings.php
├── Domain
│   ├── DomainException
│   │   ├── DomainException.php
│   │   └── DomainRecordNotFoundException.php
│   └── User
│       ├── UserNotFoundException.php
│       ├── User.php
│       └── UserRepository.php
└── Infrastructure
    └── Persistence
        └── User
            └── InMemoryUserRepository.php

api/slim/tests/
├── Application
│   └── Actions
│       ├── ActionTest.php
│       └── User
│           ├── ListUserActionTest.php
│           └── ViewUserActionTest.php
├── bootstrap.php
├── Domain
│   └── User
│       └── UserTest.php
├── Infrastructure
│   └── Persistence
│       └── User
│           └── InMemoryUserRepositoryTest.php
└── TestCase.php

api/slim/var
└── cache
```

cat api/slim/composer.json
```json
{
    "name": "slim/slim-skeleton",
    "description": "A Slim Framework skeleton application for rapid development",
    "keywords": [
        "microframework",
        "rest",
        "router",
        "psr7"
    ],
    "homepage": "http://github.com/slimphp/Slim-Skeleton",
    "license": "MIT",
    "authors": [
        {
            "name": "Josh Lockhart",
            "email": "info@joshlockhart.com",
            "homepage": "http://www.joshlockhart.com/"
        },
        {
            "name": "Pierre Berube",
            "email": "pierre@lgse.com",
            "homepage": "http://www.lgse.com/"
        }
    ],
    "require": {
        "php": "^7.4 || ^8.0",
        "ext-json": "*",
        "monolog/monolog": "^2.8",
        "php-di/php-di": "^6.4",
        "slim/psr7": "^1.5",
        "slim/slim": "^4.10"
    },
    "require-dev": {
        "jangregor/phpstan-prophecy": "^1.0.0",
        "phpspec/prophecy-phpunit": "^2.0",
        "phpstan/extension-installer": "^1.2.0",
        "phpstan/phpstan": "^1.8",
        "phpunit/phpunit": "^9.5.26",
        "squizlabs/php_codesniffer": "^3.7"
    },
    "config": {
        "allow-plugins": {
            "phpstan/extension-installer": true
        },
        "process-timeout": 0,
        "sort-packages": true
    },
    "autoload": {
        "psr-4": {
            "App\\": "src/"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "Tests\\": "tests/"
        }
    },
    "scripts": {
        "start": "php -S localhost:8080 -t public",
        "test": "phpunit"
    }
}
```
