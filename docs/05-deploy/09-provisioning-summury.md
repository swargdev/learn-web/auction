## Provisioning Summury

```
provisioning/
├── envs
│   └── certbot.yml                       # domains and email for gen-certs
├── files
│   └── cli.ini                           # config for certbot
├── tasks                                 # parts used in site.yml
│   ├── setup_docker.yml
│   ├── setup_certbot.yml
│   ├── generate_certificates.yml
│   └── create_user.yml                   # create deploy-user
├── authorize.yml                         # copy ssh keys to deploy-user
├── docker-login.yml                      # login & hashed-passwd for registry
├── hosts.yml                             # inventory list with server addr
├── hosts.yml.dist                        # template
├── renew-certificates.yml                # manual cmd for certbot renew
└── site.yml                              # main playbook script
```

site.yml:
```yml
---
- name: Provision Site
  hosts: all
  remote_user: root
  vars_files:
    - envs/certbot.yml
  tasks:
    - import_tasks: tasks/setup_docker.yml
    - import_tasks: tasks/setup_certbot.yml
    - import_tasks: tasks/generate_certificates.yml
    - import_tasks: tasks/create_user.yml
  handlers:
    - name: Start docker on boot
      systemd:
        name: docker
        state: started
        enabled: yes
```

На данном этапе наш основной playbook скрипт site.yml:
при первом запуске на чистом сервере:
- установит докер и настроит его для работы и проверит его работоспособность
- установит certbot
- сгенерирует список заданных ssl-сертификатов
- создат юзера deploy со случайно сгенерированным паролем

есть еще отдельные узкоспециализированные playbook-и:
- `authorize.yml` для копирования id_rsa.pub из локального компа в authorizedkeys
  для юзера deploy на удалённый сервер, чтобы можно было под ним заходить по ssh
- `docker-login.yml` - логин в реджести для того чтобы сохранить хэшированный
  пароль для последующих входах в registry для деплоя - при pull-e докер-обрзов
- `renew-certificates.yml` - для ручного пересоздания ssl-сертификатов

Таким образов всё подготовлено для деплоя от лица deploy-юзера а не root-а

Со временем код provisioning-а будет меняться, на данный момент у нас всё готово
и всё написано конкретно для подготовки VMок для деплоя на них конкретно только
сайта. Структура каталога provisioning плоская. Все задачи(таски) написаны в
одном экземпляре, при этом они обслуживают все хосты:

     hosts: all   # Из site.yml

которые у нас перечислены в одной пачке в файле hosts.yml
```yml
all:
  hosts:            # <<
    server:
      # ...
```

то если если будет несколько серверов

```yml
all:
  hosts:
    server_1:
      # ...
    server_2:
      # ...
    server_3:
      # ...
```

то все эти наши таски будут выполнятся на всех этих серверах одинаково.
То есть на даном этапе наша структура - плоская - работающая для одного типа
серверов (под деплой сайтов)
Со временем может возникнуть необходимость разбить наш сайт на несколько виртуалок
для того чтобы на одну виртуалку поместить gateway, на другую api, на третью
базу данных. А для этого придётся отказываться от плоской структуры в пользу
модульной в виде дерева. Модульная структура позволит обслуживать сразу несколько
серверов разных типов.

