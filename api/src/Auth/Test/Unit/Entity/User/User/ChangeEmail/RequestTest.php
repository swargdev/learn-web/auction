<?php

declare(strict_types=1);

namespace App\Auth\Test\Unit\Entity\User\User\ChangeEmail;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\Token;
use App\Auth\Test\Builder\UserBuilder;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

/**
 * @covers \App\Auth\Entity\User\User
 */
class RequestTest extends TestCase
{
    public function testSuccess(): void
    {
        $oldEmail = new Email('old-email@example.com');
        $newEmail = new Email('new-email@example.com');

        $user = (new UserBuilder())
            ->withEmail($oldEmail)
            ->active()
            ->build();

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 day'));
        $user->requestEmailChanging($token, $now, $newEmail);

        self::assertNotNull($user->getNewEmailToken());
        self::assertEquals($token, $user->getNewEmailToken());
        self::assertEquals($oldEmail, $user->getEmail());
        self::assertEquals($newEmail, $user->getNewEmail());
    }

    public function testSame(): void
    {
        $oldEmail = new Email('old-email@example.com');
        $newEmail = $oldEmail;

        $user = (new UserBuilder())
            ->withEmail($oldEmail)
            ->active()
            ->build();

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 day'));

        $this->expectExceptionMessage('Email is already same.');
        $user->requestEmailChanging($token, $now, $newEmail);
    }

    public function testAlready(): void
    {
        $user = (new UserBuilder())->active()->build();

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 day'));
        $newEmail = new Email('new-email@example.com');

        $user->requestEmailChanging($token, $now, $newEmail);

        $this->expectExceptionMessage('Changing is already requested.');
        $user->requestEmailChanging($token, $now, $newEmail);
    }

    public function testExpired(): void
    {
        $user = (new UserBuilder())->active()->build();

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 hour'));
        $newEmail = new Email('new-email@example.com');

        $user->requestEmailChanging($token, $now, $newEmail);

        $newDate = $now->modify('+2 hours');
        $newToken = $this->createToken($newDate->modify('+1 hour'));
        $user->requestEmailChanging($newToken, $newDate, $newEmail);

        self::assertEquals($newToken, $user->getNewEmailToken());
        self::assertEquals($newEmail, $user->getNewEmail());
    }

    public function testNotActive(): void
    {
        $user = (new UserBuilder())->build(); // not active!

        $now = new DateTimeImmutable();
        $token = $this->createToken($now->modify('+1 day'));
        $newEmail = new Email('new-email@example.com');

        $this->expectExceptionMessage('User is not active.');
        $user->requestEmailChanging($token, $now, $newEmail);
    }

    private function createToken(DateTimeImmutable $date): Token
    {
        return new Token(Uuid::uuid4()->toString(), $date);
    }
}
