<?php

declare(strict_types=1);

namespace Test\Functional;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use Slim\Psr7\Factory\ServerRequestFactory;

class WebTestCase extends TestCase
{
    private ?App $app = null;
    private ?MailerClient $mailer = null;

    // protected function setUp(): void
    // {
    //     parent::setUp();
    //     // to work inside transaction
    //     $this->getEntityManager()->getConnection()->beginTransaction();
    //     $this->getEntityManager()->getConnection()->setAutoCommit(false);
    // }
    //
    // protected function tearDown(): void
    // {
    //     // to work inside transaction
    //     $this->getEntityManager()->getConnection()->rollBack();
    //     parent::tearDown();
    // }

    /**
     * @param array<string,mixed> $body
     */
    protected static function json(
        string $method,
        string $path,
        array $body = []
    ): ServerRequestInterface {
        $request = self::request($method, $path)
            ->withHeader('Accept', 'application/json')
            ->withHeader('Content-Type', 'application/json');
        $request->getBody()->write(json_encode($body, JSON_THROW_ON_ERROR));

        return $request;
    }

    protected static function request(string $method, string $path): ServerRequestInterface
    {
        return (new ServerRequestFactory())->createServerRequest($method, $path);
    }

    protected function app(): App
    {
        if ($this->app === null) {
            /** @var \Slim\App $app */
            $this->app = (require __DIR__ . '/../../config/app.php')($this->container());
        }
        return $this->app;
    }

    private function container(): \Psr\Container\ContainerInterface
    {
        /** @var \Psr\Container\ContainerInterface $container */
        $container = require __DIR__ . '/../../config/container.php';
        return $container;
    }

    protected function mailer(): MailerClient
    {
        if ($this->mailer === null) {
            $this->mailer = new MailerClient();
        }
        return $this->mailer;
    }

    /**
     * this is stub of original mehtod from:
     * DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;
     * this is workaround to hide err diagnostics from phpactor
     *
     * intended to be overrided by trait
     *
     * Asserts that an array has a specified subset.
     *
     * @param array|ArrayAccess|mixed[] $subset
     * @param array|ArrayAccess|mixed[] $array
     */
    public static function assertArraySubset($subset, $array, bool $checkForObjectIdentity = false, string $message = ''): void
    {
    }


    protected function getEntityManager(): EntityManagerInterface
    {
        /** @var \Psr\Container\ContainerInterface $container */
        $container = $this->app()->getContainer();

        /** @var EntityManagerInterface $em */
        $em = $container->get(EntityManagerInterface::class);
        return $em;
    }

    /**
     * @param array<int|string,string> $fixtures
     */
    protected function loadFixtures(array $fixtures): void
    {
        /** @var \Psr\Container\ContainerInterface $container */
        $container = $this->app()->getContainer();
        $loader = new \Doctrine\Common\DataFixtures\Loader();

        foreach ($fixtures as $class) {
            /** @var AbstractFixture $fixture */
            $fixture = $container->get($class);
            $loader->addFixture($fixture);
        }

        /** @var EntityManagerInterface $em */
        $em = $container->get(EntityManagerInterface::class);

        $executor = new ORMExecutor($em, new ORMPurger($em));
        $executor->execute($loader->getFixtures());
    }
}
