# New user 'deploy'

для этого и создадим еще одну
задачу для ансибл `Create deploy user`

./provisioning/tasks/create_user.yml
```yml
---
- name: Create deploy user
  user:
    name: deploy
    password: '!'           # здесь мы говорим через '!' - сгенерируй случайный
    groups:
      - docker              # чтобы мог взаимодействовать с докером
    state: present          # указанный юзер должен сущетсвовать
```
password можно указать конкретный фиксированный пароль,
если нужен новый случайный можно сказать сгенерируй новый через '!' или '*'
но при случайной генерации пароля войти в систему через ssh не получится,
но т.к. мы будем заходить не по паролю а только по ssh-ключам то и норм.

site.yml
```yml
tasks:
    # ...
    - import_tasks: tasks/create_user.yml
```

## Добавляем свой ssh-ключ для входа в систему от лица deploy

Для этого сделаем отдельную команду, точнее playbook-скрипт, чтобы наш публичный
ssh-ключ с нашей локальной машины отправлялся в authorizedkeys
на нужный сервер в нужное место для конкретного указанного юзера (deploy)

/home/deploy/.ssh/authorizedkeys -- место куда будет отправлен ключ

Для этого в Ансибл есть спец ключевое слово `authorized_key`

./provisioning/authorize.yml:
```yml
---
- name: Add authorized key
  hosts: all
  remote_user: root
  tasks:
    - name: Add user authorized key
      authorized_key:                  # закинуть публичный ключ в нужное место
        user: deploy                             # какому юзеру закидывать ключ
        key: "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"         # откуда брать
```

В итоге такая задача скопирует публичный ключ с нашей машины по указанному пути
и загрузит его на удалённый сервер в каталог пользователя deploy так что после
этого можно будет заходить по ссш на этого пользователя.


[Next](./docs/05-deploy/08-docker-login.md)


