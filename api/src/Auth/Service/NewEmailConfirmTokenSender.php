<?php

declare(strict_types=1);

namespace App\Auth\Service;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\Token;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email as MimeEmail;
use Twig\Environment;

/**
 * 13-10-2023
 * @author Swarg
 */
class NewEmailConfirmTokenSender
{
    private MailerInterface $mailer;
    private Environment $twig;

    public function __construct(MailerInterface $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function send(Email $email, Token $token): void
    {
        $view = 'auth/email/confirm.html.twig';

        $message = (new MimeEmail())
        ->subject('New Email Confirmation')
        ->to($email->getValue())
        ->html($this->twig->render($view, [ 'token' => $token ]));

        $this->mailer->send($message);
    }
}
