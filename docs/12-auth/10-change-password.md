## UseCase Смена пароля - Change Password.

- проверка текущего пароля и обновление до нового пароля в случае успеха

Еще один случай использования(UseCase) - возможность сменить пароль из кабинета
пользователя. Залогиневшийся юзер помнит свой старый и просто хочет изменить его.

- юрез помнит свой пароль и уже авторизован(залогинен) в системе
- юзер переходит по ссылке в своём кабинете на форму смены пароля.
- защита от возможсти изменить пароль посторонними, которые имеют доступ к пк,
  на котором открыт сайт с уже вошедшим на него юзером, не зная самого пароля:
- при смене на новый нужно вводить текущий существующий пароль.

Нужен хэндлер проверяющий текущий пароль, с введённым юрезом (проверка хэшей)
и записывающий новый пароль в случае успеха.

Если вводится неправильный пароль - нужно выдать ошибку об этом.

Еще ситуация
- юзер регался через соцсеть, и предыдущего пароля у него вообще нет и не было.
- возникает проблема защиты от смены пароля посторонними лицами имеющими доступ
  к пк зареганного юзера
- защита от подобного - это запрет на смену пароля, но с возможностью восстановить
пароль через почту (т.е. через уже сделанный нами UseCase ResetPassword с токеном)

Команда с данными для смены пароля:

./api/src/Auth/Command/ChangePassword/Command.php
- id - идишник юзера
- current - текущий пароль для проверки подлинности юзера
- new - новый пароль

Обработчик этой команды:
./api/src/Auth/Command/ChangePassword/Handler.php

его задача проверять текущий пароль и если пароль правильный - сменять на новый

```php
<?
namespace App\Auth\Command\ChangePassword;
// use ...
class Handler
{
    private UserRepository $users;
    private PasswordHasher $hasher;
    private Flusher $flusher;

    public function __construct(
        UserRepository $users,
        PasswordHasher $hasher,
        Flusher $flusher
    ) {
        $this->users = $users;
        $this->hasher = $hasher;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        $user = $this->users->get(new Id($command->id));

        $user->changePassword(
            $command->current, // сырое значение текущего пароля
            $command->new,     // новый пароль на который надо сменить
            $this->hasher      // доменная служба хэширующая пароли
        );

        $this->flusher->flush();
    }
}
```
В этом коде мы снова придерживаемся 1го принципа GRASP Information Expert.
а именно - все проверки, которые можно поместить ближе к данным - помещаем к ним.
Здесь это проверки на валидность текущего пароля. И делаем мы эти проверки в
самом классе User, а не в теле метода handle.

Почему так? Разбор:

Если проверки валидности пароля делать прямо внутри метода handle
то есть вне сущности User вот таким образом:

```php
<?
//public function handle(Command $command): void
   $user = $this->users->get(new Id($command->id));
  if (!$this->hasher->validate($command->current, $user->getPasswordHash())) {
      throw new DomainException('Incorrent current password.');
  }
  $user->changePassword($this->hasher->hash($command->new));
```
- проверка хэша текущего пароля вне метода changePassword
- смена на новый пароль напрямую методом changePassword подобие setter-а

То в дальнейшем с таким кодом у нас бы возникл ряд проблем.
Такого подхода есть ряд недостатков:

1) такой подход открывает возможность ошибиться т.к.  возникает постоянная
  потребность везде перед changePassword копировать код с проверками.
Причем сюда еще надо будет добавлять еще условие - на регистрацию через соцсеть.
(Когда у юзера вообще нет текущего пароля - мы условились запрещать его изменять)
т.е. кто-то в команде может по ошибке или умылслу вызвать метод changePassword
передав в него новый хэш пяроля не произведя нужных проверок.

2) Код который мы пишем мы сразу же и проверяем через тесты, а т.к. проверки
у нас не в User а в ChangePassword\Handler то надо будет как-то писать тесты
вызывая уже класс Handler, а это будет сделать намного тяжелее чем для класса
User

3) Для проверок вне сущности нужно делать дополнительне методы-геттеры, для того
чтобы передавать данные в код "снаружи". А это уже утечка информации из класса
User. Вместо того чтобы сам класс User обрабатывал их у себя внётри их приходится
при таком подходе передавать куда-то наружу.

Поэтому разумным решением будет размещение всех нужных проверок внутри User
Это и есть тот самый принцип GRASP Information Expert которого мы и придерживаемся
поэтому и делигируем все проверки из обработчика внутрь самого класса User.
и передаём не одно значение нового пароля а всё что нужно для проверки и
хэширования нового пароля.(Обьект хэшер)

Приимущества которые даёт подход когда все проверки внутри сущности:
1) проще написать полноценный тест для метода changePassword
2) не надо писать отдельные тесты для хэндлера проверяющие код с проверками
 вне сущности User, а все эти условия if-ы можно будет прописать в самом юнит-
 тесте проверяющем метод changePassword
3) метод changePassword можно будет в дальнейшем использовать и в других местах
без нужды копипастить if-ы - условия с проверками старого пароля.



## Тесты. Выход на createStub

Теперь написав метод handle как обычно нужно переходить к написанию тестов
для метода changePassword, но новое для нас здесь то, что теперь внутрь теста
у нас передаётся не только данные, но еще и обьект доменной службы хэширующий
пароли - hasher. И у нас здесь возникает потребность сделать так, чтобы мы могли
в тестах:
- передать инстанс нужного нам обьекта - здесь это PasswordHasher
- могли подменять его значения на удобные для тестов


./api/src/Auth/Test/Unit/Entity/User/User/ChangePasswordTest.php

```php
<?
class ChangePasswordTest extends TestCase
{
    public function testSuccess(): void
    {
        // создаём активного юзера
        $user = (new UserBuilder())->active()->build();
        // вызываем для него метод смены пароля
        $user->changePassword(
            'old-password',
            'new-password',
            $hasher          // нужно как-то передать обьект хэшера
        );

        self::assertEquals($hash, $user->getPasswordHash());
    }
```

Какие есть варианты как можно поступить для того чтобы воссоздать обьект
`PasswordHasher` для передачи в метод `changePassword`:
- если бы у нас PasswordHasher был интерфейсом то его можно было бы
переопределить(реализовать) своим специальным для тестов классом неким
dummyHasher в который бы мы через конструктор могли бы передавать нужные нам
значение которые бы он возвращал. Пример:

        $hasher = new DummyHasher(true, 'new-hash')
первое значение например то что он бдует выдвать на метод $hasher->validate()
второе - значение выдаваемого хэша пароля из метода $hasher->hash()

Но у нас PasswordHasher - это не интерфейс а простой класс. варианты как его
можно подменить для тестирования(чтобы он выдавал не реальные а нужные нам для
тестов значения)

- сделать класс DummyHasher наследником PasswordHasher переопределяя нужные нам
методы validate и hash так чтобы они выдвали нужные нам результаты
Недосток - если у самого класса PasswordHasher есть конструктор с кучей параметров
то все их нужно будет как-то передавать из класса наследника
То есть работая с переопределением класса наталкиваемся на то что мешает старый
конструктор из класса предка.

- использовать уже готорые, специально придуманые вещи из тестовых фреймворков
позволяющие заменять нужные классы на свои реализации, меняя их поведение так
как нужно. В PhpUnit это такие методы как:
- createStub() - создание заглушки нужного класса
- createMock()


`createStub` позволяет прямо на лету внутри теста генерировать любой обьект
любого нужного класса, задавая ему нужное нам поведение. т.е. переопределяя то,
какие значения будут возращать методы созданного нами обьекта-заглушки.

Поэтому удобно работать с очень маленькими классами, у которых малое число
методов. Поэтому наши сервисы и пишем с минимальным количеством методов.
например Tokenizer - один метод, PasswordHasher - два метода - и ждя таких
классов очень легко создавать обьекты-заглушки(stub-ы) и использовать в разных
тестах.


## Как работать с createStub из PhpUnit


```php
<?
class ChangePasswordTest extends TestCase
{
    public function test():void
    {
        //создаём инстанс-заглушку нужного нам класса
        $hasher = $this->createStub(PasswordHasher::class);
        //...
    }
}
```

метод createStub сгенерит заглушку - обьект класса PasswordHasher.

под капотом при этом идёт генерация
- либо класса реализующего интерфейс - если мы передали интерфейс,

         class PasswordHasher123wer implements IPasswordHasher

- либо идёт создание класса наследника указанного нами класса

         class PasswordHasher123wer extends IPasswordHasher

Причем главная суть в том, что даётся возможность переопределить поведение
нового класса-заглушки.
Технически всё это работает через функцию `eval`.
Эта функция позволяет в рантайме выполнять строки как php-код:

```php
<?
eval('class PasswordHasher123wer extends PasswordHasher {
//use .. некий служебный trait
//далее идёт переопределение всех методы нужного нам класса,
}');
```
То есть в нашем случае для класса PasswordHasher это методы hash и validate
При этом значения из этих методов будут возвращатся из приватных переменных
которые можно установить самому.
Дальше когда такой eval(string_php_code) отработает - в системе появляется
новый класс в нём описанный, на основе которого через рефлексию(то есть в обход
стандартного конструктора) идёт создания обьекта из него:

```php
<?
eval('class PasswordHasher123wer extends PasswordHasher{...}');
$hasher = (new \ReflectionClass(PasswordHasher123wer::class))
    ->newInstanceWithoutConstructor();
```
Теперь т.к. $hasher - это инстанс нужного нам класса, то его можно передавать
в любой метод где ожидается сам класс PasswordHasher


Так же createStub еще вводит доп методы через которые можно настраивать поведение
созданного нами обьекта-заглушки - `method()`:

        $hasher->method('validate')->willReturn(true);

этим говорим метод с именем validate должен возвращать значение true
т.е. когда внутри тестов будет обращение к методу $hasher->validate он выдаст
значение, заданное в willReturn

        $hasher->method('hash')->willReturn('new-hash');

для метода $hasher->hash чтобы возвращал всегда заданную строку

И вот как это можно использовать в своём тесте в простейшем виде
```php
<?
class ChangePasswordTest extends TestCase
{
    public function test():void
    {
        $user = (new UserBuilder())->active()->build();

        $hasher = $this->createStub(PasswordHasher::class);
        $hasher->method('validate')->willReturn(true);
        $hasher->method('hash')->willReturn($hash = 'new-hash');

        $user->changePassword('old-password', 'new-password', $hasher);

        self::assertEquals($hash, $user->getPasswordHash());
    }
}
```


То есть если нам нужно было бы написать тест для обработчика в конструктор,
которого нужно передавать ряд обьектов например UserRepository, PasswordHasher,
Flusher то для всех их можно сделать обьекты-заглушки и тем самым это намного
упростит создание тестов для класса-хэндлера. Ну а дальше создавать инстанс
класса Handler передавая в него заглушки и вызывая в тестах единственный метод
handle передавая туда разные обьекты класса Command проверяя логику работы.

То есть заглушки актуальны и когда надо протестировать хэндлер


## createMock из PhpUnit
это уже более продвинутая вещь нежели createSubt
кроме метода `method`, через который можно настравивать поведение обьекта-стаба
создаётся доп метод `expects` - в этот метод нужно передавать правило о том,
как должен вызываться данный метод


```php
<?
    $hasher = $this->createMock(PasswordHasher::class);
    $hasher->expects($this->once()) // метод должен вызваться ровно один раз
           ->method('validate')->willReturn(false);

    $hasher->expects($this->atLeastOnce()); // должен вызываться минимум один раз
```
т.е. через expects ны говорим чтобы PhpUnit следил и проверял чтобы было нужное
количество вызовов нужных нам методов. чтобы он за этим следил и если метод не
будет вызван - выдать ошибку при работе теста

Таким образом createStub - просто заглушка, задача которой создать обьект
методы которого просто возвращают нужный нам результат. То есть это примитивная
подмена обьекта на заглушку

createMock - более умное расширение createStub-а, умеет проверять то, как будет
вызываеться созданный мок-обьект, с какими элементами-параметрами, сколько раз.

```php
<?
$hasher = $this->createMock(PasswordHasher::class);
$hasher->expects($this->once())->with()->method('validate')->willReturn(false);
```

В данных тестах достаточно простого Stub-а:


```php
<?
class ChangePasswordTest extends TestCase {
    // выносим создание заглушки для переиспользования
    private function createHasher(bool $valid, string $hash): PasswordHasher
    {
        $hasher = $this->createStub(PasswordHasher::class);
        $hasher->method('validate')->willReturn($valid);
        $hasher->method('hash')->willReturn($hash);

        return $hasher;//настроенный правильным образом обьект-заглушка
    }

    public function testSuccess(): void
    {
        $user = (new UserBuilder())->active()->build();
        // здесь hasher - это обьект заглушка намего класса PasswordHasher
        $hasher = $this->createHasher(true, $hash = 'new-hash');

        $user->changePassword(
            'old-password',
            'new-password',
            $hasher // передаём заглушку а не полноценный рабочий обьект сервиса
        );

        self::assertEquals($hash, $user->getPasswordHash());
    }

    // попытка передать неправильный пароль
    public function testWrongCurrent(): void
    {
        $user = (new UserBuilder())->active()->build();

        $hasher = $this->createHasher(false, 'new-hash');
        // должен выдать вот такую вот ошибку
        $this->expectExceptionMessage('Incorrent current password.');
        $user->changePassword(
            'wrong-old-password',
            'new-password',
            $hasher
        );
    }

    // проверка смены пароля для юзера у которого вообще нет пароля -
    // зареган и логиниться через соцсеть
    public function testByNetwork(): void
    {
        // можно просто взять кусок кода из JoinByNetworkTest:
        $user = User::joinByNetwork(
            Id::generate(),
            new DateTimeImmutable(),
            new Email('email@example.com'),
            new NetworkIdentity('vk', '000001')
        );

        $hasher = $this->createHasher(false, 'new-hash');

        $this->expectExceptionMessage('User does not have an old password.');
        $user->changePassword(
            'any-old-password',
            'new-password',
            $hasher
        );
    }
```

Здесь в тестовом методе `testByNetwork` мы приходим к тому что вместо копипаста
куска кода по созданию юзера "зареганного через соцсеть" хорошо бы написать доп
вариант для уже имеющегося у нас помощника `UserBuilder`. В таком духе:
```php
<?
    $user = (new UserBuilder())
        ->viaNetwork()  //создание зареганным через соцсеть (без пароля)
        ->build();
```
Чтобы в наших тестах удобно и быстро можно было создать инстанс юзера зареганного
через соцсеть (без пароля)
./api/src/Auth/Test/Builder/UserBuilder.php
```php
<?
class UserBuilder
{
    private Id $id;
    // ...
    private ?NetworkIdentity $networkIdentity = null;

    public function viaNetwork(NetworkIdentity $identity = null): UserBuilder
    {
        $clone = clone $this;
        // если не задана соцсеть генерить по умолчанию
        $clone->networkIdentity = $identity ?? new NetworkIdentity('vk', '0001');
        return $clone;
    }

    public function build(): User
    {
        // если задана соцсеть - создавать User-а зарегананного через неё
        if ($this->networkIdentity !== null) {
            return  User::joinByNetwork(// регистрация через соцсеть(активный)
                $this->id,
                $this->date,
                $this->email,
                // без пароля!
                $this->networkIdentity
            );
        }
        // уже существующие варианты: (обычный подход)
        $user = User::requestJoinByEmail(
            $this->id,
            $this->date,
            $this->email,
            $this->passwordHash,     // хэш пароля
            $this->joinConfirmToken
        );

        if ($this->active) { // активация
            $user->confirmJoin(
                $this->joinConfirmToken->getValue(),
                $this->joinConfirmToken->getExpires()->modify('-1 day'),
            );
        }

        return $user;
    }
```


теперь можно переписать тот же тестовый метод вот так:
```php
<?
class Test {
    public function testByNetwork(): void
    {
        $user = (new UserBuilder())
            ->viaNetwork()  //создание зареганным через соцсеть (без пароля)
            ->build();

        $hasher = $this->createHasher(false, 'new-hash');
        // ожидаем ошибку о том что у юзера нет пароля - запрет на смену.
        $this->expectExceptionMessage('User does not have an old password.');
        $user->changePassword(
            'any-old-password',
            'new-password',
            $hasher
        );
    }
```

Написав все возможные тесты пишем сам метод User.changePassword
```php
<?
class User {
    // ...
    public function changePassword(
        string $current, // текущий пароль
        string $new,     // новый пароль
        PasswordHasher $hasher // обьект сервис хэширующий пароли
    ): void {
        // случай у юзера нет пароля - регистрация через соцсеть.
        if ($this->passwordHash === null) {
            throw new DomainException('User does not have an old password.');
        }
        // проверка сходится ли указанный пароль с уже сущетсвующим в БД
        if (!$hasher->validate($current, $this->passwordHash)) {
            throw new DomainException('Incorrent current password.');
        }
        // обновление значение хэша нового пароля
        $this->passwordHash = $hasher->hash($new);
    }
}
```

## API и любые запросы

т.к. мы создаём API которое будет работать по json запросам, то надо расчитывать
на то, что запросы могут приходить любые - поэтому и делать все возможные
проверки и защиту от попыток поломать систему. То есть хоть у нас и будет свой
фронтенд работающий с нашим же api через json и например для смены пароля для
юзера зареганного через соцсети можно будет сделать сокрытие формы смены пароля
то надо понимать что могут быть и сторонние неофициальные клиенты подключающиеся
к нашему сайту, в том числе и возможно злономеренные. поэтому и нужно делать все
возможные проверки в том числе и на "неправильное" использование и попытки
сломать работу сайта. Т.е. тешить себя иллюзией спрячем на фронтенде и юзер не
дойдёт до этого в api - это наивность.




