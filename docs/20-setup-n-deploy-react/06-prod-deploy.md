## Создаём образы для деплоя React-App в prod

Для Dev-окружения мы, внутри контейнера `frontend-node` поднимаем nodejs dev-
сервер и спользуем отдельный контейнер `frontend` с nginx внутри него как прокси,
который перенаправляет запросы от контейнера `gateway` в контейнер `frontend-node`:
(еще раз этот прокси мы создали только для того чтобы через него дальше можно
было прописывать http-заголовки так же как будем это делать в продакшене)

В общем схема проксирования такая

    8080:8080              x:3000
    gateway -> frontend -> frontend-node
    nginx      nginx       nodejs listen 3000 port
               proxy

т.е. локально когда мы заходим на localhost:8080 то попадаем при этом
на порт который слушает контейнер-сервис `gateway`(см docker-compose.yml ниже)
это контейнер с установленным в нём nginx-ом, который настроен как шлюз
который проксирует все приходящие на него запросы в другие докер-контейнеры.
Распределение запросов идёт по портам. Если запросы идут на 8080 порт то
эти запросы перенаправляются внутри докер-сети на контейнер-сервис `frontend`.
и этот контейнер сожержит nginx который прокидывает запрос дальше до контейнера
`frontend-node` уже на его 3000 порт


nodejs dev-сервер мы описываем этим докерфайлом:
./frontend/docker/development/node/Dockerfile

```Dockerfile
FROM node:18-alpine
WORKDIR /app
```

этот докерфайл используется в docker-compose.yml в сервисах `frontend*`

```yml
version: '3.7'
services:
  gateway:
    build:
      context: gateway/docker
      dockerfile: development/nginx/Dockerfile
    ports:
      - '${PORT_FRONTEND}:8080'                     # forward to "frontend"
      - '${PORT_API}:8081'
      - '${PORT_MAILER}:8082'
    depends_on:
      - frontend
      - api

  # ....

  frontend:                                        # forward to "frontend-node"
    build:
      context: frontend/docker
      dockerfile: development/nginx/Dockerfile
      # ./frontend/docker/development/nginx/conf.d/default.conf
    depends_on:
      - frontend-node

  frontend-node:
    build:
      context: frontend/docker/development/node
    environment:
      - WDS_SOCKET_PORT=0
    volumes:
      - ./frontend:/app
    command: yarn start
    tty: true

  frontend-node-cli:
    build:
      context: frontend/docker/development/node
    volumes:
      - ./frontend:/app
```

то есть для создания таких контейнеров-сервисов как:

- frontend-node      - используем для dev-сервера на 3000м порту
- frontend-node-cli  - используем для cli-комманд потипу yarn install


Для прода нам не нужен девелоперский NodeJS сервер, и деплоить нам нужно
подготовленные для деплоя файлы из frontend/build которые собирает `yarn build`

Поэтому конфигурация прод-сервера будет оличаться от дев
но будет похожа на ту которая уже у нас быля для Nginx:

./frontend/docker/production/nginx/conf.d/default.conf
```nginx
server {
  listen 80;
  charset uft-8;
  index index.html;
  root /app/public;

  add_header X-Frame-Options "SAMEORIGIN";
}
```

работа так же будет идти из каталога `/app/public`
но теперь нам нужно будет копирать всё из ./frontend/build внутрь
`/app/public` внутри контейнера

Поэтому нам нужно изменить наш старый Докерфайл:

./frontend/docker/production/nginx/Dockerfile
```Dockerfile
FROM nginx:1.18-alpine

COPY ./docker/production/nginx/conf.d /etc/nginx/conf.d

WORKDIR /app

COPY ./public ./public
```

так чтобы в нём теперь проходила сборка через `yarn build` и копирование
всего собранного внутрь /app/public

Лучше всего это делать через мультистадийную сборку:
т.е. сначала берём тот же докеробраз с готовым nodejs и собираем в нём
затем собранное копируем в новый чистый образ уже на базе nginx:1.18-alpine
при этом копируем только наше собранное фронтед приложение без лишних
зависимостей: без node_modules и без самого nodejs и без yarn и без исходников

```Dockerfile
FROM node:18-alpine as builder

WORKDIR /app

COPY ./package.json ./yarn.lock ./
RUN yarn install

COPY ./ ./
RUN yarn build

###

FROM nginx:1.18-alpine

COPY ./docker/production/nginx/conf.d /etc/nginx/conf.d

WORKDIR /app

COPY --from=builder /app/build ./public
```

В общем первая часть as builder - это черновой образ куда будут размещены все
зависимости и все нужные для сборки пакеты, а второй образ - чистовой куда
будет размещено готовое для работы приложение содержащее сугубо только нужное
для его работы, даже без исходных кодов самого фронтенда и тем более без
node_modules и прочего софта который на проде уже не нужен(nodejs yarn)

в общем публикация статических фронтенд файлов идёт вместе с nginx без какого-
либо nodejs сервера, который мы используем у себя в dev-окружение чисто для
удобства и скорости разработки.
Принцип деплоя простой - собрать всё приложение в build и все эти файлы отдать
nginx чтобы он их статически разадавал клиентам


В конфиг для nginx можно добавить настройки для кэширования файлов браузерами
```nginx
    listen 80;
    charset utf-8;
    index index.html;
    root /app/public;
    server_tokens off;


    location ~* \.(?:ico|gif|jpe?g|png|woff2?|eot|otf|ttf|svg)$ {
        expires 4d;
        access_log off;
        add_header Cache-Control "public";
    }

    location ~* \.(?:css|js)$ {
        expires 7d;
        access_log off;
        add_header Cache-Control "public";
    }

    location / {
        add_header X-Frame-Options "SAMEORIGIN";
        try_files $uri $uri/ /index.html;
    }
```

`access_log off` - это отключение логирования доступа к данным файлам
иначе все запросы к этим файлам будут логироваться в лог-файл nginx-a
смысл в том, чтобы логировать запросы только к важным файлам, а не к статическим
asset-файлам приложения
вообще css и js при сборке будут дополнятся хвостами с Хэш суммами т.е. каждая
последующая сборка будет создавать файлы с новыми именами поэтому можно разрешать
хэшировать хоть на веки вечные. но здесь вопрос в том как будет ли это забивать
место которое отьедают браузеры у клиентов

кэшироение изображений при build-е не изменяет имён этих файлов, поэтому их
кэшировать нужно отдельно от css js иначе если разрешить кэшировать картинки и
затем обновить например favicon.ico то изменится она на браузерх пользователей
только когда пройдёт срок хранения старой закэшированно версии.


