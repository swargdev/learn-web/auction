## Пишем первый e2e-тест Cucumber-js

Примеры тестов
https://github.com/cucumber/cucumber-js-examples/

- проверка работы первоночальной главной страницы нашего сайта

всё что есть на этой странице это просто Wellcome-блок с выводом
надписи `Auction We will be here soon`

Пишем спеку в которой описываем что
- главная страница открывается
- нужный текст выводится

Создаём каталог и файл со Gherkin-спекой в нём:
./cucumber/docker/features/home.feature
```feature
Feature: View home page
  In order to check home page content
  As a guest user
  I want to be able to view home page

  Scenario: View home page content
    Given I am a guest user
    When I open home page
    Then I see welcome block
```

В этом файле мы пописываем всё то, что нужно протестировать в рамках
именно этой фичи(feature)

Первая часть - первые 4 строки чисто для понятности - нечто вроде комантария
эта часть никак не будет использоваться в самих тестах, но призвана облегчить
понимание о чем вообще идёт речь

Дальше после ключевого слова `Scenario` идёт пошаговое описание того что надо
проверить:
- дано: я гостевой юзер
- когда: я открываю домашнюю страницу
- тогда: я вижу welcome-блок


пробуем запустить Cucumber-js снова:

```sh
make test-e2e
docker compose run --rm cucumber-node-cli yarn e2e
yarn run v1.22.19
$ cucumber-js
UUU                                 # говорит о том что запустились 3 шага
                                    # U - undefined т.е. не реализованные
Failures:                           # дальше идёт подсказка как надо писать код

1) Scenario: View home page content # features/home.feature:6
   ? Given I am a guest user
       Undefined. Implement with the following snippet:

         Given('I am a guest user', function () {
           // Write code here that turns the phrase above into concrete actions
           return 'pending';
         });

   ? When I open home page
       Undefined. Implement with the following snippet:

         When('I open home page', function () {
           // Write code here that turns the phrase above into concrete actions
           return 'pending';
         });

   ? Then I see welcome block
       Undefined. Implement with the following snippet:

         Then('I see welcome block', function () {
           // Write code here that turns the phrase above into concrete actions
           return 'pending';
         });


1 scenario (1 undefined)
3 steps (3 undefined)
0m00.003s (executing steps: 0m00.000s)
```

В общем cucumber нам вывел заготовку e2e-теста своего рода заготовку для
дальнейшей реализации

для кода по опеределению шагов сценариев создаём каталог
./cucumber/features/step_definitions/
в и внего размещаем свой код:

```js
const {Given, When, Then } = require('@cucumber/cucumber');
// в старых версиях надо было указывать просто через require('cucumber');

Given('I am a guest user', function () {
 return 'pending';
});

When('I open home page', function () {
 return 'pending';
});

Then('I see welcome block', function () {
 return 'pending';
});
```

Как видно вся магия e2e-тестов полностью развеивается
шаги просто мапяться на те же самые предложения что описаны человеческим
языком в Gherkin-спеке из файла home.feature

возврат строки `pending` означает что тест еще не доделан

```sh
docker compose run --rm cucumber-node-cli yarn e2e
yarn run v1.22.19
$ cucumber-js
P--

Warnings:

1) Scenario: View home page content # features/home.feature:6
   ? Given I am a guest user # features/step_definition/home.js:3
       Pending         # он увидил строку pending и остановился дальше не пошел
   - When I open home page # features/step_definition/home.js:7
   - Then I see welcome block # features/step_definition/home.js:11

1 scenario (1 pending)
3 steps (1 pending, 2 skipped)
0m00.012s (executing steps: 0m00.000s)
```
Этот запуск означает что запустился сценарий из home.js для home.feature
выполнился первый шаг `I am a guest user` и т.к. из него вернулась строка
pending (не доделано) то дальше выполнение не пошло.

Теперь нам нужно вписать js-код который будет
- запускать браузер,
- переходить на главную страницу сайта
- проверять наличие Welcome-Блока


## Добавляем ESLint в образ с cucumber-node-cli

По сути `cucumber` у нас создан в виде отдельного под-проекта для E2E тестов
в отдельном каталоге ./cucumber:

```
.
├── api/                  подпроект с backend API на php
├── cucumber/         отельный подпроект для E2E-текстов
├── frontend/                        подпроект с ReactJS
├── gateway
├── provisioning/
├── docs/
├── docker-compose-production.yml
├── docker-compose.yml
├── Makefile
├── LICENCE.md
└── README.md
```

А значит ничто на не мешает отдельно для этого подпроекта на nodejs добавить
ESLint, чтобы проверять корректность написания js-кода как мы это делаем в
подпроекте frontend

./cucumber/package.json
```json
{
  "license": "MIT",
  "scripts": {
    "e2e": "cucumber-js",
    "lint": "eslint --ext .js,.jsx src",
    "lint-fix": "eslint --fix --ext .js,.jsx src"
  },
  "dependencies": {
    "@cucumber/cucumber": "^10.1.0"
  },
  "devDependencies": {
    "eslint": "^8.0.1",
    "eslint-config-standard": "^17.1.0",
    "eslint-plugin-import": "^2.25.2",
    "eslint-plugin-n": "^16.0.0",
    "eslint-plugin-promise": "^6.0.0"
  }
}
```

Минимальный конфиг для ESLint без подключения каких-либо плагинов
./cucumber/.eslintrc.json
```json
{
  "env": {
    "browser": true,
    "commonjs": true,
    "es2021": true,
    "node": true
  },
  "extends": [
    "standard"
  ],
  "parserOptions": {
    "ecmaVersion": "latest",
    "sourceType": "module"
  },
  "rules": {
    "semi": "off"
  }
}
```

Особенность js кода в этом cucumber под-проекте состоит в том, что этот
код мы запускаем напрямую в nodejs без какого-либо преобразования
в какие-либо адаптированные версии(например как в React через babel)
именно поэтому не будем использовать `import` для подключения модулей
будем использовать только `require` для подключение других js файлов.

Дальше можно еще будет прописать вызов линтера через Makefile:

```Makefile
lint: api-lint frontend-lint cucumber-lint

cucumber-lint:
	docker compose run --rm cucumber-node-cli yarn lint

cucumber-lint-fix:
	docker compose run --rm cucumber-node-cli yarn lint-fix
```

```sh
docker compose run --rm cucumber-node-cli yarn install
```

Так же здесь мы прописываем вызов линтера для e2e тестов и для `make lint`


## Выбор библиотеки эмулирующую работу браузера выход на Puppeteer

Теперь нам надо писать код эмулирующий открытие сайта в браузере
а значит нужна библиотека для эмуляции работы с сайтом через браузер
котрая будет эмулировать такие вещи как
- открытие страницы
- нажатие клавишь клавиатуры и мыши

Как вариант можно установить Selenium-сервер и к нему привязать любой
из нужных браузеров Firefox, Chrome, Opera и проч. и управлять
привязанными браузерами через Selenium.
Selenium - написан да Java, а значит будет долго запускаться и возможно
медленно работать по сути являясь лишней прослойкой между кодом наших
тестов и браузером.

Другой вариант - работать с браузером напрямую через его API
а именно через специально придуманный dev-tool протокол
http://chromedevtools.github.io/devtools-protocol/

этот протокол позволяет
- создавать вкладки(tab-ы)
- открывать страницы на вкадках
- взаимодействовать со страницей через эмуляцию ввода пользователя:
  клики, перемещения по странице и проч.

можно работать с этим протоколом напрямую со своего js-кода,
а можно взять готовое решение под эту задачу:

https://github.com/ChromeDevTools/awesome-chrome-devtools
Awesome tooling and resources in the Chrome DevTools &
DevTools Protocol ecosystem

[The big two automation libraries](https://github.com/ChromeDevTools/awesome-chrome-devtools#the-big-two-automation-libraries)

На этой вкладке есть ссылка на библиотеку для автоматизации:

- [Puppeteer](https://github.com/GoogleChrome/puppeteer/) - Node.js offering a
  high-level API to control headless Chrome over the DevTools Protocol.
  See also [awesome-puppeteer](https://github.com/transitive-bullshit/awesome-puppeteer).

- [Playwright](https://github.com/microsoft/playwright) - Library to automate
  Chromium, Firefox and WebKit with a single API.
  Available for Node.js, Python, .Net, Java.
  See also [awesome-playwright](https://github.com/mxschmitt/awesome-playwright).

Puppeteer(Кукловод) - библиотека позволяющая работать с DevTools Protocol через
javascript
http://pptr.dev - это сайт этой библиотеки, содержит в том числе документацию
https://devdocs.io/puppeteer/

Смотрим в документации как эмулировать тестирование через эту либу:

```js
import puppeteer from 'puppeteer';

(async () => {
  const browser = await puppeteer.launch();   // запуск браузера
  const page = await browser.newPage();       // создать новую вкладку
  await page.goto('https://example.com');     // переходим на нужный url-адрес
  // работа со страницей прямо внутри браузера
  await page.screenshot({path: 'screenshot.png'};
  await browser.close();
})();
```

